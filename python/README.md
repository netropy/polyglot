# polyglot/python

### Python Experiments, Exercises, Refcards, Notes

Topics:
- [data classes vs namedtuples](data classes vs namedtuples.md)
- [tail-recursion elimination, tail-call optimization](tail-recursion elimination, tail-call optimization.md)

Quick links: \
[Python sources](py/),
[Python tests](py/test/)

[Up](../README.md)
