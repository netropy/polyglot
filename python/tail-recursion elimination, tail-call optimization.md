# polyglot/python

#### Tail-Recursion Detection, Enforcement, and Elimination in Python

Per stated view of Python's (former) BDFL: Unlikely that the Python core
distribution will ever feature Trail Recursion Elimination:
http://neopythonic.blogspot.com/2009/04/tail-recursion-elimination.html
http://neopythonic.blogspot.com/2009/04/final-words-on-tail-calls.html

Main concern: Tail Call Optimization may be confusing to programmers if
stack frames are eliminated.

Some suggestions for function decorators detecting tail-recursion seem
experimental at best:
http://lambda-the-ultimate.org/node/1331
http://code.activestate.com/recipes/474088-tail-call-optimization-decorator
https://chrispenner.ca/posts/python-tail-recursion
