# polyglot/python

#### dataclasses.@dataclass decorator vs collections.namedtuple()

Data classes appear superior to namedtuples due to configurability (optional
immutability, field default values etc) and built-in support for type hints.

Why tuples?
- readability
- extendability

dataclasses.dataclass:
- decorated classes, creates new type: A(1, '2') != B(1, '2')
- supports static type checkers, use of PEP 526 type anotation for variables
- subclassable composition, no base classes or metaclasses used
- mutability, optional immutability
- more specific equality tests
- controllable init, hash, rich compares, and repr
- field objects, post-init processing, init-only variables
- not substitutable for tuples, not iterable, unpackable

collections.namedtuple(), typing.NamedTuple:
- instances are subclass of tuples: A(1, '2') == B(1, '2')
- immutable, hashable
- iterable, unpackable
- orderable
- substitutable for dictionary
- fast indexed access
- fast slicing
- somewhat compact
- namedtuple has no built-in support for type annotations
- but can add type annotations by subclassing: class Point(typing.NamedTuple)
- may subclass to add methods: class Point(namedtuple('Point', ['x', 'y']))


References, pros & cons of tuple vs namedtuple vs @dataclass (vs ...):
https://docs.python.org/3/library/dataclasses.html \
https://www.python.org/dev/peps/pep-0557/ \
https://attrs.readthedocs.io/en/stable/why.html \
https://github.com/ericvsmith/dataclasses/issues/19 \
https://www.dropbox.com/s/cxxz4d2hwj4hm4d/HettingerHolidaySF2017.pdf \
https://stackoverflow.com/questions/3357581/using-python-class-as-a-data-container \
