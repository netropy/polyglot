# -*- coding: utf-8 -*-
"""
Unit test example.

Have pytest find and run test in current directory tree:
    $ pytest [-v] [-s]
      -v (verbose), -s (do not capture stdout)

For pytest's test discovery rules, see:
    https://doc.pytest.org/en/latest/goodpractices.html

Uses PEP 484, 526 type annotations (>= Python 3.6).
"""

HELLO: str = 'Hello '
"""Variable with PEP 526 type annotation"""

def greeting(name: str) -> str:
    """Function with PEP 484 type annotation."""
    return HELLO + name

def test_greeting() -> None:
    """Tests argument type compliance."""
    name = 'Darling'
    assert greeting(name) == HELLO + name

def test_greeting_error() -> None:
    """Tests argument type error."""
    #val = 1
    # great, type-checking with mypy reports this error
    # test_ex_1_2.py:49: error: Argument 1 to "greeting" has incompatible type "int"; expected "str"
    #assert greeting(val) == 'Hello ' + val
