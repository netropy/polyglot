# polyglot/java

### Notes: java.util.stream

- supports functional, lazy, concurrent operations on streams of elements
- wide applicability: map-reduce transformations, grouping/joining of data,
  bulk operations
- 2 main types _Stream_ (interface) and _Collectors_ (utility class)
  introduced in
  [Java 8 Stream API](https://docs.oracle.com/javase/8/docs/api/java/util/stream/package-summary.html)
- moderately augemented (in Java 9, 10), see
  [Java 11 Stream API](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html)
- compared to
  [Scala 2.12 Stream](https://www.scala-lang.org/api/2.12.8/scala/collection/immutable/Stream$.html) /
  [Scala 2.13 LazyList](https://www.scala-lang.org/api/current/scala/collection/immutable/LazyList.html):
  - _-_ no _memoization_ (but be can coded or can explicitely collect to _List_)
  - _-_ limited (runtime-only) support for _immutability_, reduction often
    based on mutable (and concurrent) container
  - _+_ built-in support for _parallelism_ utilizing
    [ForkJoinPool.commonPool()](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/ForkJoinPool.html#commonPool()) or a [custom ThreadPool](https://www.baeldung.com/java-8-parallel-streams-custom-threadpool)
    - _largely declarative_ support for
      [concurrent map transformations](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#Parallelism)
    - _somewhat declarative_ support for
      [concurrent reduction](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#ConcurrentReduction)

### Notes: Stream Parallelism

Initially, set the
[Stream's characteristics](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/BaseStream.html)
according to the _subsequent aggregation operation_:
```
    .stream()
        .unordered()   // if element order is irrelevant for subsequent ops
        .sequential()  // if ops have side-effects on non-concurrent data
        .parallel()    // if ops are associative or update concurrent data
		...
```

Above modifiers are idempotent.  By default, streams are created _sequential_
(_stream()_ vs _parallelStream()_).  Streams can be tested by _isParallel()_.

(Note there's no method _ordered()_, which'd be ambiguous, but _sorted(...)_.)

#### [Non-]Mutable Reduction with Stream.reduce​(identity, accumulator)

[Stream.reduce​(T, BinaryOperator)](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/Stream.html#reduce(T,java.util.function.BinaryOperator))
performs a
[concurrent reduction](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#ConcurrentReduction)
if the
[stream is parallel](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/BaseStream.html#parallel()).

If the accumulator function _updates_ and returns the result, that is, for a
[mutable reduction](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#MutableReduction),
the result container MUST be _concurrent_ or _synchronized_, i.e., thread-safe
(unless the stream is sequential).

Otherwise, for a _non-mutual reduction_, the accumulator function must return
an immutable result type or a cloned instance to prevent concurrent result
modifications.

Unsurprisingly, the accumulator must be an associative function, for which the
passed identity value is the neutral element.

#### Mutable Reduction with Stream.collect(supplier, accumulator, combiner)

[Stream.collect(Supplier,BiConsumer,BiConsumer)](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/Stream.html#collect(java.util.function.Supplier,java.util.function.BiConsumer,java.util.function.BiConsumer))
performs a
[concurrent reduction](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#ConcurrentReduction)
if the
[stream is parallel](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/BaseStream.html#parallel())
-- on ANY mutable result type, whether concurrent or not.

NO additional synchronization is required.  When this method performs a
parallel reduction, it
- creates multiple result instances using the _supplier_,
- updates the results with stream elements using the _accumulator_, and
- then merges the results using the _combiner_ function.

Unsurprisingly, the passed accumulator and combiner functions must be
associative, stateless, and
[non-interfering](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#NonInterference).

#### Mutable Reduction with Stream.collect(collector)

[Stream.collect(Collector)](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/Stream.html#collect(java.util.stream.Collector))
only performs a
[concurrent reduction](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#ConcurrentReduction)
if:
- the [stream is parallel](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/BaseStream.html#parallel())
  AND
- the
  [collector is concurrent](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/Collector.Characteristics.html#CONCURRENT)
  AND
- the
  [stream is unordered](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/BaseStream.html#unordered())
  OR the
  [collector is unordered](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/Collector.Characteristics.html#UNORDERED)

#### Mutable Reduction with Stream.forEach(action)

[Stream.forEach​(Consumer)](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/Stream.html#forEach(java.util.function.Consumer))
performs a
[concurrent reduction](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#ConcurrentReduction)
if the
[stream is parallel](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/BaseStream.html#parallel());
otherwise, the
[mutable reduction](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#MutableReduction)
operation is sequential but _nondeterministic_.

For the parallel case, the result-collecting container MUST be _concurrent_ or
_synchronized_, i.e., thread-safe.

Unsurprisingly, the passed consumer action function must be associative and
commutative in the result, statefull, and
[non-interfering](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#NonInterference).

[forEachOrdered​(Consumer)](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/Stream.html#forEachOrdered(java.util.function.Consumer))
does a strictly sequential reduction on any input stream (sequential or
parallel), and keeps the encounter order if the stream has one.

[Up](./README.md)
