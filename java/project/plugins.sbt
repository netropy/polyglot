// Implementation of SBT's test interface for JUnit Jupiter
// https://mvnrepository.com/artifact/net.aichler/jupiter-interface
// https://github.com/maichler/sbt-jupiter-interface
// also requires ../build.sbt:
// libraryDependencies += "net.aichler" % "jupiter-interface" % "0.11.1" % Test
addSbtPlugin("net.aichler" % "sbt-jupiter-interface" % "0.11.1")
