# polyglot/java

### Java Experiments, Exercises, Refcards, Notes

_Background_: Wrote some of these notes and coding exercises for interviewing
senior Java/Scala developers (Java <= 11, Scala <= 2.13).

- [java.util.function, Lambda Literals](Java_Function_API_Notes.md) - notes
- [java.util.stream, Stream Parallelism](Java_Stream_API_Notes.md) - notes
- [java.util.{stream,function} APIs, Use from Scala](Java_v_Scala_Stream_Function_API_Notes.md) - notes
- see [../bigdata/local use case of {stream,function}](../bigdata/local/ex_1.q.local.md) - exercises
- [product types, tuple, Map.Entry, records (Java 14), data classes](Java_Product_Types_Tuple_MapEntry_Records_Data_Classes_Notes.md) - notes
- [API design: best practices, a bean class with all the trimmings, csv converters](ex_1.q.bean_class_csv.md) - exercises

Quick links: \
[Java sources](src/main/java/netropy/polyglot/testing/java),
[Java tests](src/test/java/netropy/polyglot/testing/java)

[Up](../README.md)
