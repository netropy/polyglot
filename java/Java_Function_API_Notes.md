# polyglot/java

### Notes: java.util.function, Lambda Literals

Java 8 introduced _43_ (!)
[predefined functional interfaces](https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html)
with respective single-abstract methods (plus default and static methods):
- Suppliers: nullary function
  - _Supplier\<T\>:_  ___T get()___ \
    _[Boolean|Int|Long|Double]Supplier_
- Consumers: _void_ result type
  - _Consumer\<T\>:_ ___void accept(T)___ \
    _[Int|Long|Double]Consumer_
  - _BiConsumer\<T,U\>:_ ___void accept(T,U)___ \
    _Obj[Int|Double|Long]Consumer\<T\>_
- Predicates: _boolean_ result type
  - _Predicate\<T\>:_ ___boolean test(T)___ \
    _[Int|Long|Double]Predicate_
  - _BiPredicate\<T,U\>:_ ___boolean test(T,U)___
- Operators: parameters' type == result type
  - _UnaryOperator\<T\>:_ ___T apply(T)___ \
    _[Int|Long|Double]UnaryOperator_
  - _BinaryOperator\<T\>:_ ___T apply(T,T)___ \
    _[Int|Long|Double]BinaryOperator_
- Functions: unary and binary functions
  - _Function\<T,R\>:_ ___R apply(T)___
    _[Int|Long|Double]Function\<R\>_ \
    _To[Int|Long|Double]Function\<T\>_ \
    _IntTo[Long|Double]Function_,
    _LongTo[Int|Double]Function_,
    _DoubleTo[Int|Long]Function_
  - _BiFunction\<T,U,R\>:_ ___R apply(T,U)___ \
    _To[Int|Long|Double]BiFunction\<T,U\>_

#### Criticism

- a semantic, _connotative naming scheme_ feels misguided when all the
  _relevant_ information is just in the _signatures_ (arity and types)
- having to _memorize_ an elaborate naming scheme for interfaces _and_ methods
  feels taxing
- searching for a suitable type among the
  [API doc's 43 alphabetically listed interfaces](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/function/package-summary.html)
  feels burdensome
- the only acceptable number of newly introduced APIs would have been ___42___
- false mnemonics: Java's camel-case naming scheme makes the specialized APIs
  _associate the object type_, instead of the _value_ type: \
  _...[Boolean|Int|Long|Double]..._
- Java's chosen naming scheme is not even regular:
  - _UnaryOperator_ vs _[Supplier|Consumer|Predicate|Function]_
  - _BinaryOperator_ vs _Bi[Consumer|Predicate|Function]_
  - _BooleanSupplier_ vs nothing
  - binary _Obj[Int|Double|Long]Consumer\<T\>_ vs nothing

[Up](./README.md)
