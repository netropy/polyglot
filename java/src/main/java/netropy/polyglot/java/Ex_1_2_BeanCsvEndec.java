package netropy.polyglot.java;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import netropy.polyglot.java.Ex_1_1_BeanClassLayout.Person;


/**
 * Ex 1.2 API design: Extend 1.1 for converter functions _Person \<-\> CSV_.
 */
public class Ex_1_2_BeanCsvEndec {

    /*
     * Discussion: Names & Signatures for Converter Methods
     *
     * Java APIs follow different patterns, historically:
     *
     * - exposed constructor:
     *     e.g. java.lang.Long:
     *     @deprecated Long(String) throws NumberFormatException
     *
     * - static "parse*", "valueOf", or "of*" factory methods:
     *     e.g. java.lang.Long:
     *     static long parseLong(String) throws NumberFormatException
     *     static Long valueOf(String) throws NumberFormatException
     *
     *     e.g. java.util.Optional:
     *     static <T> Optional<T> of​(T value)
     *     static <T> Optional<T> of​Nullable(T value)
     *
     * - instance and static "to*" or "get" methods:
     *     e.g. java.lang.Long:
     *     String Long.toString()
     *     static String toString​(long i)
     *
     *     e.g. java.util.Optional:
     *     T get()
     *
     * - external helper class with static "to*" or "*" methods:
     *     e.g. java.util.Arrays:
     *     static String toString​(Object[])
     *     static <T> Stream<T> stream​(T[] array)
     *
     * - external en-/decoder, [de-]serialization objects
     *     + decouples serialization format from class
     *     - selection by static type, risk of "type truncation"
     *     - not extendable to subtypes (vs Scala implicits)
     *     - if external class/package, depends on class being public
     *       features, constructor or factory or builder, mutator methods
     */

    /**
     * A simple Person <-> CSV converter (endec, codec, serde).
     * + as external utility: toCsv, parseCsv
     * + use of CharSequence, java.util.regex
     */
    static public class PersonCsv {

        static public String toCsv(Person p) {
            return
                "\"" + p.getName() + "\","
                + p.getAge() + ","
                + (p.getAddresses().length == 0
                   ? "" : "\"" + String.join("\",\"", p.getAddresses()) + "\"");
        }

        /*
         * Simple Demo Parser:
         * + based on Java's support for Regular Expression parsing
         * - no need to support whitespace lines, comment lines "#..."
         * - no need to deal with absent name string, escaped doublequotes
         * + optional single trailing comma, horizontal whitespace
         * - not much error handling, yield Person or throw RuntimeException
         * - no need for ParseException with position or line number
         *
         * Regexp Refresher:
         *   `\h`, `\v` = horizontal/vertical white space character classes
         *   `\d` = digits
         *   (X) = capturing group
         *   (?:X) = non-capturing group, does't count towards group total
         *   `*`, `?` = gready quantifiers zero-or-more, zero-or-one
         */

        // TODO: simplify char classes, drop [] for singleton classes

        static final String lineRegexp = ""
            + "(?:\\h*[\"]([^\"\\v]*)[\"]\\h*)"     // group: name
            + "(?:,\\h*(\\d+)\\h*)"                 // group: age
            + "((?:,\\h*[\"][^\"\\v]*[\"]\\h*)*)"   // group: addresses
            + ",?\\h*";                             // trailing?
        static final Pattern linePattern = Pattern.compile(lineRegexp);

        static final String quotedRegexp = "[\"]([^\"\\v]*)[\"]";
        static final Pattern quotedPattern = Pattern.compile(quotedRegexp);

        // simple demo parser
        static public Person parseCsv(CharSequence line) {

            final Matcher lineMatch = linePattern.matcher(line);
            if (!lineMatch.matches())
                throw new RuntimeException("csv parse error, line: " + line);

            // extract name, age, addressList
            assert lineMatch.groupCount() == 3;
            final String name = lineMatch.group(1);
            final int age = Integer.parseInt(lineMatch.group(2));
            final String addressList = lineMatch.group(3);

            // extract addresses
            final Matcher quotedMatch = quotedPattern.matcher(addressList);
            assert quotedMatch.groupCount() == 1;
            final ArrayList<String> addresses = new ArrayList<>();
            while (quotedMatch.find())
                addresses.add(quotedMatch.group(1));

            return new Person(name, age, addresses.toArray(new String[0]));
        }
    }
}
