package netropy.polyglot.java.typeclass;


// Design Tradeoff:
//
// ACCEPTABLE: define Ord as abstract class
// - cannot use a lambda as implementation since not a functional interface
// + can declare final those extra methods that are subtype-invariant
// - cannot have subtypes extending multiple abstract classes (e.g. Ord + Eq)
//
// BEST: define Ord as interface
// + functional interface (single abstract method), can assign lambda
// - cannot declare final those extra methods that are subtype-invariant
// + can have subtypes implementing multiple interfaces (e.g. Ord + Eq)

// a comparison type that defines a total ordering on values of a type
/*public*/ abstract class OrdC<T> {

    public abstract int compare(T x, T y);

    // default methods on Ord[T]: o.lt(x, y), o.lteq(x, y)
    public final Boolean lt(T x, T y) { return compare(x, y) < 0; }
    public final Boolean lteq(T x, T y) { return compare(x, y) <= 0; }

    // some default orderings on basic types:

    static public final OrdC<Integer> intOrd = new OrdC<>() {
            public int compare(Integer x, Integer y) {
                return (x == y) ? 0 : (x > y) ? 1 : -1;
            }
        };

    static public final OrdC<String> stringOrd = new OrdC<>() {
            public int compare(String s, String t) {
                return s.compareTo(t);
            }
        };
}

// a comparison function that defines a total ordering on values of a type
public interface Ord<T> {

    int compare(T x, T y);

    // default methods on Ord[T]: o.lt(x, y), o.lteq(x, y)
    default /*final*/ Boolean lt(T x, T y) { return compare(x, y) < 0; }
    default /*final*/ Boolean lteq(T x, T y) { return compare(x, y) <= 0; }

    // some default orderings on basic types:

    static final Ord<Integer> intOrd = (x, y) ->
        (x == y) ? 0 : (x > y) ? 1 : -1;

    static final Ord<String> stringOrd = (s, t) ->
        s.compareTo(t);

    // no Java support for a "rich type" offering methods on T: x < y, x <= y
}
