package netropy.polyglot.java.typeclass;


// a user-defined type, which happens to come without an ordering
//
// ACCEPTABLE design:
// * define extra features in a separate "*Helper" or "*s" (plural) class
// - must be imported explicitly (possibly as 'import static')
// + can be added externally, no modification of class needed
//
// BEST design:
// * define extra features as static fields/methods in the class
// + automatically picked up, no need for extra import
// - requires modification of class
//
public class Name {

    public final String first;
    public final String last;

    public Name(String first, String last) {
        this.first = first;
        this.last = last;
    }

    static public Name of(String first, String last) {
        return new Name(first, last);
    }

    // some orderings (or other features) on Name:

    // an ordering as lambda instance
    static public Ord<Name> byLast = (x, y) -> {
        // must qualify Ord.stringOrd since not subclass
        int cl = Ord.stringOrd.compare(x.last, y.last);
        return (cl != 0) ? cl : Ord.stringOrd.compare(x.first, y.first);
    };

    // another ordering as anonymous class instance
    static public Ord<Name> byFirst = new Ord<>() {
            @Override
            public int compare(Name x, Name y) {
                // no need to qualify Ord.stringOrd since subclass
                int cf = stringOrd.compare(x.first, y.first);
                return (cf != 0) ? cf : stringOrd.compare(x.last, y.last);
            }
        };

    // yet another ordering as an enum singleton (just for fun)
    static public enum ByLength implements Ord<Name> {
        INSTANCE {
            @Override
            public int compare(Name x, Name y) {
                int lx = x.first.length() + x.last.length();
                int ly = y.first.length() + y.last.length();
                return lx - ly;
            }
        }
    }
    static public Ord<Name> byLength = ByLength.INSTANCE;  // for convenience
}
