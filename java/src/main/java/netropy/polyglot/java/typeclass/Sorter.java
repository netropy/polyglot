package netropy.polyglot.java.typeclass;

import java.util.List;


// usage example: explicitly declare + pass ordering parameter
public class Sorter {

    // java.util.List is not designed as a functional but primarily mutable
    // datatype; this makes it hard to write insert(), isort() as recursive
    // and side-effect-free functions (unless one copies lists everywhere).
    // Also, Java gives no assurances about eliminating tail recursion.
    //
    // Hence, we have these functions loop over and mutate the List argument.
    // Yet, they also return the mutated list to support use in expressions.

    static private <T> List<T>
    insert(T x, List<T> xs, int pos, Ord<? super T> ord) {
        assert 0 <= pos && pos <= xs.size();

        while (pos < xs.size() && !ord.lteq(x, xs.get(pos)))
            pos++;
        xs.add(pos, x);
        return xs;
    }

    /** Sorts a list in-place under an ordering and also returns the list. */
    static public <T> List<T> isort(List<T> xs, Ord<? super T> ord) {
        assert xs != null;
        assert ord != null;

        for (int i = xs.size() - 2; i >= 0; i--) {
            T x = xs.remove(i);
            insert(x, xs, i, ord);  // ord passed explicitly (unlike Scala)
        }
        return xs;
    }
}
