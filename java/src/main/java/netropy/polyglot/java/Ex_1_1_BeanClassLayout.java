package netropy.polyglot.java;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;


/**
 * Ex 1.1 API design: Write an example data class -- with all the trimmings.
 *
 * This code illustrates some design items of
 *     [EffJ]: Joshua Bloch, Effective Java, 3rd Edition (2018)
 * @see https://www.pearson.com/us/higher-education/program/Bloch-Effective-Java-3rd-Edition/PGM1763855.html
 *
 * Item01: Consider static factory methods instead of constructors
 * Item10: Obey the general contract when overriding equals
 * Item11: Always override hashCode when you override equals
 * Item12: Always override toString
 * Item13: Override clone judiciously
 * Item14: Consider implementing Comparable
 * Item15: Minimize the accessibility of classes and members
 * Item16: In public classes, use accessors, not public fields
 * Item17: Minimize mutability
 * Item49: Check parameters for validity
 * Item56: Write doc comments for all exposed API elements
 * Item78: Synchronize access to shared mutable data
 * Item86: Implement Serializable with great caution
 * Item87: Consider using a custom serialized form
 */
class Ex_1_1_BeanClassLayout {

    /**
     * A data class as Bean: subclassable, cloneable etc.
     *
     * This class exhibits mutable state and referenced mutable state (array)
     * to illustrate when there's a need for
     * - synchronization (or volatile, AtomicReference) for thread-safety
     * - defensive copying in clone() and other methods
     *
     * Other than that, class+method design should reflect "best practices".
     */
    static public class Person
        implements Cloneable, Serializable, Comparable<Person> {

        /*
         * Implement Serializable?
         * - maybe mandated for object transmission or persistence
         * - extralinguistic mechanism for creating objects
         * - limits flexibility to change class implementation
         * - testing burden with class layout change
         * - implementation burden for subclasses
         * - always define a serial version UID: practical to use design date!
         * - do not change UID unless to break existing serialized instances
         */

        // [EffJ] Item86: Implement Serializable with great caution
        // [EffJ] Item87: Consider using a custom serialized form
        private static final long serialVersionUID = 20201212L;  // date

        /*
         * Class Parameters:
         * - see mutable vs immutable: .../guides/ImmutableDataStructures.md
         * - classes with public mutable fields are not generally thread-safe
         * - painful boilerplate code
         */

        // intentional variety: showcase {immutable, mutable} X {fields, types}
        // [EffJ] Item15: Minimize the accessibility of classes and members
        // [EffJ] Item16: In public classes, use accessors, not public fields
        // [EffJ] Item17: Minimize mutability
        // [EffJ] Item78: Synchronize access to shared mutable data
        private final String name;  // non-null
        private final int age;
        private volatile String[] addresses;  // atomic, non-null

        // [EffJ] Item49: Check parameters for validity
        protected Person(String name, int age, String[] addresses) {
            assert name != null;  // vs Objects.requireNonNull​(name)
            assert addresses != null;  // vs Objects.requireNonNull​(addresses)

            this.name = name;
            this.age = age;
            this.addresses = addresses.clone();  // defensive copy :-(
        }

        // [EffJ] Item01: Consider static factory methods instead of c'tors
        static public Person of(String name, int age, String[] addresses) {
            return new Person(name, age, addresses);
        }

        static public Person of(String name, int age, String addresses) {
            return new Person(name, age, new String[]{ addresses });
        }

        /*
         * Proper clone():
         *
         * + overriden as public
         * + has covariant return type
         * + omits throwing CloneNotSupportedException if possible,
         *   depends on composed types, subclasses
         * + obeys java.lang.Object.clone() semantics:
         *   different but equal
         * + follows java.lang.Object.clone() recommendations:
         *   independent, as possible: deep copy references to mutable state
         *
         * Cloneable vs copy constructors:
         * - implement clone()? complexity, maybe dictated by framework/usage
         * - copy constructors with factory? prone to type truncation
         *
         * Discussion: java.lang.Cloneable is broken
         *   Joshua Bloch: https://www.artima.com/intv/bloch.html#part13
         *   Ken Arnold: https://www.artima.com/intv/issues.html#part3
         * - risk-prone, extralinguistic object creation mechanism
         * - interface name prone to misspelling, better: 'Copyable'
         * - interface lacks declaration of method clone(),
         *   hence, method can only invoked through reflection
         * - clone() signature is not covariant,
         *   could be fixed by use of CRTP (or type class pattern):
         *     interface Copyable<T extends Copyable<T>> { T copy(); }
         * - use of checked CloneNotSupportedException,
         *   constraint violation, should be unchecked RuntimeException
         * - unclear sematics: deep vs shallow copy,
         *   deep copy recommended, but native T[].clone is shallow
         * - imposes class invariant: subclasses must implement clone(),
         *   not typesafe, unenforceable at runtime
         * - painful boilerplate code
         */

        // [EffJ] Item13: Override clone judiciously
        @Override
        public Person clone() {
            try {
                Person p = (Person)super.clone();  // shallow copy
                p.addresses = p.addresses.clone();  // defensive copy :-(
                return p;
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        }

        /*
         * Getters + Setters:
         * - some encoders need accessors + mutators to recognize Bean pattern
         * - hence, lack of support for immutable types
         * - downsides of exposing mutable fields, mutators...
         * - risks, holes from exposing fields of mutable type...
         * - painful boilerplate code
         */

        public String getName() { return name; }

        public int getAge() { return age; }

        // showcase mutable type
        public String[] getAddresses() {
            return addresses.clone();  // defensive copy :-(
        }

        // showcase mutable field + type
        public void setAddresses(String[] addresses) {
            assert addresses != null;  // vs Objects.requireNonNull​(addresses)
            this.addresses = addresses.clone();  // defensive copy :-(
        }

        /*
         * Proper hashCode(), equals(), toString(), compareTo​():
         *
         * + should be defined together ("Rule of Two", "Rule of Four")
         * + utilizes java.util.{Objects, Arrays, Comparator}
         * + provides values-oriented not identity-based toString()
         * + obeys java.lang.Object.hashCode semantics:
         *   runtime-constant, same hash if equal, distinct hash as possible
         * + obeys java.lang.Object.equals semantics:
         *   reflexive, symmetric, transitive, constant, unequals null
         * + follows java.lang.Comparable recommendations:
         *   lexicographical & natural ordering, total order, consistency
         * + asserts consistency of hashCode() with equals()
         * + asserts consistency of toString() with equals()
         * + asserts consistency of compareTo() with equals()
         * + fluent comparator construction: most clear and concise
         * + public COMPARATOR instance
         * - otherwise, painful boilerplate code
         */

        // [EffJ] Item11: Always override hashCode when you override equals
        @Override
        public int hashCode() {
            // ensure consistency of hashCode() with equals():
            // - deepEquals() semantics requires deepHashCode​()
            //   incorrect: Objects.hash(name, age, addresses)
            return Objects.hash(name, age, Arrays.deepHashCode​(addresses));
        }

        // [EffJ] Item10: Obey the general contract when overriding equals
        @Override
        public boolean equals(Object other) {
            if (other == this)
                return true;

            if (other == null || other.getClass() != this.getClass())
                return false;

            final Person o = (Person)other;
            final boolean result =
                Objects.equals(name, o.name)
                && (age == o.age)
                //&&  Objects.deepEquals(addresses, o.addresses);  // ok
                &&  Arrays.deepEquals(addresses, o.addresses);  // better

            // expensive asserts: production code would want to skip checking
            // consistency of hashCode(), toString(), compareTo() with equals()
            assert !result || hashCode() == o.hashCode();
            assert !result || toString().equals(o.toString());
            assert result == (compareTo(o) == 0);
            return result;
        }

        // [EffJ] Item12: Always override toString
        @Override
        public String toString() {
            // some IDEs generate toString() code; but it is more helpful
            // to have consistency of toString() with equals()
            // - use of deepToString() mirrors use of deepEquals()
            // - for identity, can always print System.identityHashCode(Object)
            return getClass().getName()
                + "(" + name + ", "
                + age + ", "
                + Arrays.deepToString​(addresses) + ")";
        }

        // [EffJ] Item14: Consider implementing Comparable
        @Override
        public int compareTo​(Person other) {
            return COMPARATOR.compare(this, other);
        }

        /**
         * A lexicographic-order Comparator by Persons' name, age, addresses.
         *
         * Notes:
         * - Comparator's fluent API makes composition most readable and
         *   ensures being consistent with equals.
         *
         * - support compareTo(null), analog to equals(null):
         *     Comparator.nullsFirst(...)   // null to be less than non-null
         *
         * - arrays are not Comparable by default:
         *     incompatible bounds, expects: Comparable<?>, found: String[]
         *     .thenComparing(Person::getAddresses)
         *   but Arrays::compare can be supplied as auxiliary comparator:
         *     .thenComparing(Person::getAddresses, Arrays::compare)
         *
         * - Arrays::compare is documented to use Comparator::nullsFirst.
         */
        static public final Comparator<Person> COMPARATOR =
            Comparator.nullsFirst​(  // null to be less than non-null
                Comparator.comparing(Person::getName)
                .thenComparingInt(Person::getAge)
                .thenComparing(Person::getAddresses, Arrays::compare));
    }
}
