package netropy.polyglot.java.experiments;

public class InterfacesAreAlwaysStatic {

    final static int s = 2;
    final int i = 2;

    // default methods in interfaces cannot access instance variables
    public interface InterfaceWithDefaultMethod {
        default int fun() {
            // non-static variable i cannot be referenced from a static context
            //return i; // compile error
            return s;  // ok
        }
    }
}
