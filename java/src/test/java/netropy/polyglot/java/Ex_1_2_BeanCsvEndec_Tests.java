package netropy.polyglot.java;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import netropy.polyglot.java.Ex_1_1_BeanClassLayout.Person;
import static netropy.polyglot.java.Ex_1_2_BeanCsvEndec.PersonCsv.toCsv;
import static netropy.polyglot.java.Ex_1_2_BeanCsvEndec.PersonCsv.parseCsv;

/**
 * Ex 1.2: ...
 */
public class Ex_1_2_BeanCsvEndec_Tests {

    // just import test data from prior test instance
    final Ex_1_1_BeanClassLayout_Tests data =
        new Ex_1_1_BeanClassLayout_Tests();
    final Person p0 = data.p0;
    final Person p1 = data.p1;

    @Test
    public void test_parseCsv() {
        String p0csv = "\"Jane Doe\",33";
        assertEquals(p0, parseCsv(p0csv));
        assertEquals(p0, parseCsv(p0csv + ","));
        assertEquals(p0, parseCsv(p0csv + " , "));

        String p1csv = "\"John Doe\",32,\"1234 Elm St\",\"5051 Main St\"";
        assertEquals(p1, parseCsv(p1csv));
        assertEquals(p1, parseCsv(p1csv + ","));
        assertEquals(p1, parseCsv(p1csv + " , "));
    }

    @Test
    public void test_toCsv_parseCsv() {
        assertEquals(toCsv(p0), toCsv(p0.clone()));
        assertEquals(toCsv(p1), toCsv(p1.clone()));

        assertEquals(p0, parseCsv(toCsv(p0)));
        assertEquals(p1, parseCsv(toCsv(p1)));
    }
}
