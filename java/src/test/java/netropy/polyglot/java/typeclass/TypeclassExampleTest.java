package netropy.polyglot.java.typeclass;

import java.util.List;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

// allowed here (but not in local scope):
import static netropy.polyglot.java.typeclass.Ord.intOrd;

public class TypeclassExampleTest {

    // test data
    static public final List<Integer> numbers =
        List.of(0, 2, 1);  // unmodifiable

    // test data: to be sorted by first name / last name / combined length
    static public final List<Name> names =
        List.of(  // unmodifiable
            Name.of("Jeremias", "Smythe"),
            Name.of("Jeremias", "Smith"),
            Name.of("John", "Smith"));

    @Test
    public void test_pass_imported_static_typeclass_instance_sort_int() {
        // not allowed here (only at file level): see above
        //import static netropy.polyglot.java.typeclass.Ord.intOrd;
        //
        // alternatively, can always alias a fully qualified name:
        //Ord<Integer> intOrd = Ord.intOrd;

        List<Integer> l = Sorter.isort(new ArrayList(numbers), intOrd);

        assertEquals(l, List.of(0, 1, 2));
    }

    @Test
    public void test_pass_qualified_typeclass_instance_sort_byLast() {
        List<Name> al = new ArrayList<>(names);  // modifiable list

        List<Name> l = Sorter.isort(al, Name.byLast);

        assertEquals(l, al);
        assertEquals(l, List.of(names.get(1), names.get(2), names.get(0)));
    }

    @Test
    public void test_pass_qualified_typeclass_instance_sort_byFirst() {
        List<Name> al = new ArrayList<>(names);  // modifiable list

        List<Name> l = Sorter.isort(al, Name.byFirst);

        assertEquals(l, al);
        assertEquals(l, List.of(names.get(1), names.get(0), names.get(2)));
    }

    @Test
    public void test_pass_qualified_typeclass_instance_sort_byLength() {
        List<Name> al = new ArrayList<>(names);  // modifiable list

        List<Name> l = Sorter.isort(al, Name.byLength);

        assertEquals(l, al);
        assertEquals(l, List.of(names.get(2), names.get(1), names.get(0)));
    }


    @Test
    public void test_call_default_typeclass_methods() {
        Ord<Name> byLast = Name.byLast;  // alias fully qualified name

        assert(byLast.lt(
                   Name.of("Jeremias", "Smith"),
                   Name.of("Jeremias", "Smythe")));
        assert(byLast.lteq(
                   Name.of("Jeremias", "Smith"),
                   Name.of("John", "Smith")));
    }

    // Typeclass Extension Methods: cannot express any of this sort in Java
    //
    //   Ord<Name> byLast = Name.byLast
    //   Name.of("Jeremias", "Smith") <(byLast) Name.of("Jeremias", "Smythe")
    //   Name.of("Jeremias", "Smith") <=(byLast) Name.of("John", "Smith")
    //
    // No support for infix operators, extension methods, context parameters,
    // or implicit conversions to a "rich type".
}
