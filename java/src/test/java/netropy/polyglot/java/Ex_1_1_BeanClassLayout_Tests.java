package netropy.polyglot.java;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import netropy.polyglot.java.Ex_1_1_BeanClassLayout.Person;
import static netropy.polyglot.java.Ex_1_1_BeanClassLayout.Person.COMPARATOR;

/**
 * Ex 1.1: ...
 */
public class Ex_1_1_BeanClassLayout_Tests {

    /*
     * Test Data:
     * - ideally generated (properties-based testing)
     * - ideally, test coverage of full space: names X ages X addresses
     * - for simplicity, just testing on 2 data points here
     */
    static public final String[] names = { "Jane Doe", "John Doe" };
    static public final int[] ages = { 33, 32 };
    static public final String[][] addresses = {
        {},
        { null },
        { "1234 Elm St" },
        { "1234 Elm St", null },
        { "1234 Elm St", "5051 Main St" },
    };
    public final Person p0 = Person.of(names[0], ages[0], addresses[0]);
    public final Person p1 = Person.of(names[1], ages[1], addresses[4]);
    public final Person p0c = p0.clone();
    public final Person p1c = p1.clone();

    @Test
    public void test_equals() {
        assert(!p0.equals(null));
        assert(!p0.equals(p1));
        assert(!p1.equals(p0));
        assert(p0.equals(p0c));
        assert(p0c.equals(p0));
        assert(p1.equals(p1c));
        assert(p1c.equals(p1));
    }

    @Test
    public void test_toString() {
        assertEquals(p0.toString(), p0c.toString());
        assertEquals(p1.toString(), p1c.toString());
    }

    @Test
    public void test_hashCode() {
        assert(p0.hashCode() == p0c.hashCode());
        assert(p1.hashCode() == p1c.hashCode());
    }

    @Test
    public void test_COMPARATOR() {
        assert(COMPARATOR.compare(null, p0) < 0);  // nullsFirst
        assert(COMPARATOR.compare(p0, null) > 0);
        assert(COMPARATOR.compare(p0, p1) < 1);
        assert(COMPARATOR.compare(p1, p0) > 1);
        assert(COMPARATOR.compare(p0, p0c) == 0);
        assert(COMPARATOR.compare(p0c, p0) == 0);
        assert(COMPARATOR.compare(p1, p1c) == 0);
        assert(COMPARATOR.compare(p1c, p1) == 0);
    }

    @Test
    public void test_compareTo() {
        //assert(null.compareTo(p0) == COMPARATOR.compare(null, p0));
        assert(p0.compareTo(null) == COMPARATOR.compare(p0, null));
        assert(p0.compareTo(p1) == COMPARATOR.compare(p0, p1));
        assert(p1.compareTo(p0) == COMPARATOR.compare(p1, p0));
        assert(p0.compareTo(p0c) == COMPARATOR.compare(p0, p0c));
        assert(p0c.compareTo(p0) == COMPARATOR.compare(p0c, p0));
        assert(p1.compareTo(p1c) == COMPARATOR.compare(p1, p1c));
        assert(p1c.compareTo(p1) == COMPARATOR.compare(p1c, p1));
    }
}
