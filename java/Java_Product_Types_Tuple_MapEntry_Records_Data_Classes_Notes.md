# polyglot/java

### Notes: Product Types, Tuple, Map.Entry, Records, Data Classes

#### Product Types, Tuples

The Java SE API has never offered _standardized_ types for generic tuples or
products.

This makes it harder to
- map, group, or pair data held in collections
- compare and hash paired data by value equality
- convert of `Collection<Tuple<K, V>>` data to `Map<K, V>` (see below)
- formulate relational operations on structured data, e.g., _project_, _join_
- express zip/unzip-style or general traversal functor algorithms, e.g.,
  `Collection<Tuple<...>> <-> Tuple<Collection<...>>`
- model data as
  [Algebraic Data Types](https://en.wikipedia.org/wiki/Algebraic_data_type).

#### Set, Map, and Map.Entry

[java.util.Map<K,​V>](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/Map.html)
does not _extend_
[Iterable](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/lang/Iterable.html),
[Collection](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/Collection.html), nor
[Set](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/Set.html),
but offers a modifiable _Set view_ of the mappings via method
[entrySet()](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/Map.html#entry(K,V)).

The returned key-value pairs of interface type
[java.util.Map.Entry<K,​V>](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/Map.Entry.html)
are mutable and obey _value equality_ over key and value.

_Map.Entry_ instances are strictly view objects, albeit modifiable, i.e.,
setting the value changes the underlying map.  However, _Map_ does not support
to _add_ _Map.Entry_ objects to it, not even by copying entry objects (as they
are bound to their map).

This makes _Map -> Set<Map.Entry>_ extraction a one-way road.

Of course, one can always manually extract each entry's key and value and put
them to a map.

Since Java 9, _Map_ offers a (default) _Map.Entry_ factory as static method
[<K,​ V> Map.entry(K,V)](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/Map.html#entry(K,V)),
along with a static _Map_ factory
[Map.ofEntries](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/Map.html#ofEntries(java.util.Map.Entry...)),
which returns an unmodifiable map containing the keys and values extracted
from the given entries.

However, map entries themselves cannot be added to or stored in a map.

This seems the closest the Java SE API ever got to offering a standardized,
structural _tuple_ or _pair_ type.

#### Records (Java 14), Data Class Proposal

Java's _Bean_ pattern imposes significant boilerplate compared to
[Kotlin Data Classes](https://kotlinlang.org/docs/reference/data-classes.html)
or [Scala case classes](https://docs.scala-lang.org/tour/case-classes.html).

Based on
[JEP 359](https://openjdk.java.net/jeps/359),
Java 14 introduced _Records_ as a
[preview feature](https://openjdk.java.net/jeps/12)
, with a new keyword
[`record`](https://docs.oracle.com/en/java/javase/14/language/records.html)
and the common base class
[java.lang.Records](https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/Record.html).

Records are _immutable_, _optionally serializable_, and offer _value
equality_ on instances of the same record type (_nominal tuples_).

Records overlap with a proposal for
[value/data classes](https://cr.openjdk.java.net/~briangoetz/amber/datum.html).
Also, records are well suited to support
[pattern macthing](https://cr.openjdk.java.net/~briangoetz/amber/pattern-match.html),
if introduced.

[Up](./README.md)
