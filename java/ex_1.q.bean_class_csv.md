# polyglot/java

### Java: API Design - Best Practices, Exercises, Code Experiments

#### 1.1 API design: Write an example data class -- with all the trimmings.

Less trivial than it may apear, write a data class _Person_ with 3 fields:
- _String name_, _int age_,
- a mutable field _String[] addresses_ (address format does not matter)

Ensure that class _Person_
- supports subclassing
- implements _Cloneable_, _Serializable_, and _Comparable\<T\>_
- provides value-based rather than identity-based equality
- applies "best practices" of Java API design & implementation

Reference, for example:
[Effective Java, 3rd Edition (2018), by Joshua Bloch](https://www.pearson.com/us/higher-education/program/Bloch-Effective-Java-3rd-Edition/PGM1763855.html).

Discuss the Java APIs _Cloneable_, _Serializable_, and _Comparable\<T\>_ on
requirements, recommendations, and options for implementation.

Provide a unit test.

See code comments for discussion.

[Java source](src/main/java/netropy/polyglot/java/Ex_1_1_BeanClassLayout.java)
[Java test](src/test/java/netropy/polyglot/java/Ex_1_1_BeanClassLayout_Tests.java)

#### 1.2 API design: Extend 1.1 for converter functions _Person <-> CSV_.

Comma-separated-values is a simple (and fragile) text format for data.

Discuss API options for how & where to provide bidirectional converters along
with a given data class.  (Often, converters are also called _serdes_, for
serializiers-deserializiers, or _codecs_, _endecs_, for encoder-decoders.)

Check name and signature patterns for converters in standard Java APIs.

Implement a simple, one-record-per-line CSV parser for _Person_ data:
- no need to support syntax for escaped quotes ("\""), comment lines (#...)
- no need for detailed error messages (unlike production code)
- try to allow for arbitrary horizontal whitesspace, a trailing comma

See code comments for discussion.

[Java source](src/main/java/netropy/polyglot/java/Ex_1_2_BeanCsvEndec.java)
[Java test](src/test/java/netropy/polyglot/java/Ex_1_2_BeanCsvEndec_Tests.java)

[Up](../README.md)
