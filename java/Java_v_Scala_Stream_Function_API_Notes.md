# polyglot/java

### Notes: java.util.{stream,function} APIs, Use from Scala

_Thoughts:_
- _+_ the
  [Java Stream API](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html)
  provides for _functional transformation_ of data, for which the
  [Java Collection Framework](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/package-summary.html)
  has not been designed
- _+_ Streams are a
  [fluent API](https://en.wikipedia.org/wiki/Fluent_interface):
  greatly improve _readability of code_ by pipelining (method chaining),
  factory methods, and standard patterns (transformation, aggregation)
- _+_ Streams encourage a _looser coupling_ of components by providing a
  single abstraction for suppliers and consumers of data
- _+_ Streams are _efficient_ as they support lazy evaluation, interleaving
  of operations, memory locality, and concurrency
- _+_ Java [Stream Parallelism](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#Parallelism)
  is largely _declarative_ and extends to:
  map-reduce transformations, grouping/joining of data, bulk operations
- ___=> Java's Stream API ranks among the most useful among the Java SE API
  additions___

_Comparative Thoughts:_
- _-_ the [Java Function API](https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html)
  is a pain compared to
  [Scala _Function*_ types](https://www.scala-lang.org/api/current/scala/index.html),
  _see below_
- _+_ Java Stream API differences to Scala's
  [Stream](https://www.scala-lang.org/api/2.12.8/scala/collection/immutable/Stream$.html) /
  [LazyList](https://www.scala-lang.org/api/current/scala/collection/immutable/LazyList.html)
  API: support for +parallelism, -memoization, (-immutability)
- _-_ Scala's APIs for parallel bulk operations require expertise
  e.g., parallel map operations with [Future.traverse](https://www.scala-lang.org/api/current/scala/concurrent/Future$.html#traverse[A,B,M[X]%3C:IterableOnce[X]](in:M[A])(fn:A=%3Escala.concurrent.Future[B])(implicitbf:scala.collection.BuildFrom[M[A],B,M[B]],implicitexecutor:scala.concurrent.ExecutionContext):scala.concurrent.Future[M[B]]),
  [parallel collections](https://docs.scala-lang.org/overviews/core/collections-migration-213.html)
- _+_ Scala (>=2.12) can pass [lambdas](https://docs.scala-lang.org/overviews/scala-book/anonymous-functions.html)
  to Java
  [single-abstract method interfaces (SAM)](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/FunctionalInterface.html),
  i.e., lambdas are interoperable
- _-_ [Java's Collection Framework](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/package-summary.html)
  is poorer than and many parts inferior to
  [Scala's Collection API](https://www.scala-lang.org/api/current/scala/collection/index.html)
  e.g., on immutable collections
- _+_ however, Scala-Java class interoperability,
  [mutual conversion of collection types](https://docs.scala-lang.org/overviews/collections-2.13/conversions-between-java-and-scala-collections.html),
  and
  [low-level stream construction](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#StreamSources)
is available
 - ___=> use of _java.util.stream_ from _Scala_ is a real possibility for
   declarative & efficient parallelism___

[Up](./README.md)
