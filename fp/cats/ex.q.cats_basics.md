# polyglot/fp/cats

### Exercises, Q&A, Code Experiments with CATS

Quick Links: \
[scala tests](src/test/scala/netropy/polyglot/fp/cats/)

### 1 Code: CATS _basics_

#### 1.1 Done: Check CATS compile status with Dotty/Scala3.

___2022-12___: Scala3 compile works for CATS 2.9.0

___2020-09___: No luck compiling CATS 2.2.0 with dotty 0.27.0-RC1:
```
  // failing/missing implicit conversions despite:
  import scala.language.implicitConversions  // must enable
  import cats.syntax.all._  // CATS doc recommends wildcard import
```

#### 1.2 Code: Try out and unit-test _Id[T]_ as Functor, Applicative etc.

Write simple unit tests for the CATS data type _Id_ used as a Functor,
Apply/Applicative, FlatMap/Monad, CoflatMap/Comonad, and Traverse.

See CATS API docs:
[Id](https://typelevel.org/cats/datatypes/id.html),
[type def](https://typelevel.org/cats/api/cats/index.html#Id[A]=A)

See chapter in (excellent) "Scala with Cats":
[The Identity Monad](https://www.scalawithcats.com/dist/scala-with-cats.html#sec:monads:identity)

Discuss API/usage issues in code comments.

No need for property checks of laws, given the type's trivial nature.

[scala tests](src/test/scala/netropy/polyglot/fp/cats/ex_1_2.Id_Tests.scala)

### 2 Code: Basic CATS effects _Writer, Reader, State_

#### 2.1 Code: Try out and property-check the CATS _Writer[W,A]_ Monad.

See CATS API docs:
[Writer](https://typelevel.org/cats/datatypes/writer.html)
[object](https://typelevel.org/cats/api/cats/data/package$$Writer$.html)
[type def](https://typelevel.org/cats/api/cats/data/index.html#Writer[S,A]=cats.data.package.WriterT[cats.Eval,S,A])

See chapter in (excellent) "Scala with Cats":
[The Writer Monad](https://www.scalawithcats.com/dist/scala-with-cats.html#writer-monad)

Discuss API/usage issues in code comments.

[scala tests](src/test/scala/netropy/polyglot/fp/cats/ex_2_1.Writer_Tests.scala)

#### 2.2 Code: Try out and property-check the CATS _Reader[R,A]_ Monad.

See CATS API docs:
[Reader](https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers]]),
[object](https://typelevel.org/cats/api/cats/data/package$$Reader$.html]]),
[type def](https://typelevel.org/cats/api/cats/data/index.html#Reader[-A,B]=cats.data.package.ReaderT[cats.Id,A,B])

See chapter in (excellent) "Scala with Cats":
[The Reader Monad](https://www.scalawithcats.com/dist/scala-with-cats.html#sec:monads:reader)

Discuss API/usage issues in code comments.

[scala tests](src/test/scala/netropy/polyglot/fp/cats/ex_2_2.Reader_Tests.scala)

#### 2.3 Code: Try out and property-check the CATS _State[S,A]_ Monad.

See CATS API docs:
[State](https://typelevel.org/cats/datatypes/state.html),
[object](https://typelevel.org/cats/api/cats/data/package$$State$.html),
[type def](https://typelevel.org/cats/api/cats/data/index.html#State[S,A]=cats.data.package.StateT[cats.Eval,S,A])

See chapter in (excellent) "Scala with Cats":
[The State Monad](https://www.scalawithcats.com/dist/scala-with-cats.html#the-state-monad)

Discuss API/usage issues in code comments.

[scala tests](src/test/scala/netropy/polyglot/fp/cats/ex_2_3.State_Tests.scala)

### 3 Code, Q&A: CATS conditionals _ifF, ifA, ifM_

_Context:_  The CATS functions
[ifF][iff],
[ifA][ifa] (deprecated), and
[ifM][ifm],
respectively, lift the _if-then-else_ conditional into the Functor,
Applicative (Apply), and Monad (FlatMap) context.

[iff]: https://www.javadoc.io/static/org.typelevel/cats-core_2.13/2.6.1/cats/Functor.html#ifF[A](fb:F[Boolean])(ifTrue:=%3EA,ifFalse:=%3EA):F[A]

[ifa]: https://www.javadoc.io/static/org.typelevel/cats-core_2.13/2.6.1/cats/Apply.html#ifA[A](fcond:F[Boolean])(ifTrue:F[A],ifFalse:F[A]):F[A]

[ifM]: https://www.javadoc.io/static/org.typelevel/cats-core_2.13/2.6.1/cats/FlatMap.html#ifM[B](fa:F[Boolean])(ifTrue:=%3EF[B],ifFalse:=%3EF[B]):F[B]

___2022-11, 2020-09:___ Here's how the
[API Doc](https://www.javadoc.io/doc/org.typelevel/cats-core_2.13/2.6.1/cats/index.html)
specified the functions (v2.2.0, as screenshot): \
![ifF](ex_3_1.ifF_cats2.2.0.png "ifF Cats 2.2.0") \
![ifA](ex_3_1.ifA_cats2.2.0.png "ifA Cats 2.2.0") \
![ifM](ex_3_1.ifM_cats2.2.0.png "ifM Cats 2.2.0")

#### 3.1 Done: Code-review the above CATS API doc.

_2020-09: The above scaladoc lacks clarity, simplicity, uniformity._

1. There's a doc bug: The _ifA: asInt3_ case
   ```
   scala> val b3: Option[Boolean] = Some(true)
   scala> val asInt3: Option[Int] = Apply[Option].ifA(b3)(Some(1), None)
   asInt2: Option[Int] = None
   ```
   actually evaluates to _Some(1)_ and should have been written as:
   ```
   scala> Apply[Option].ifA(Some(true))(Some(1), None)
   val res2: Option[Int] = Some(1)
   ```
1. The _ifA_ example should have used _List_ instead of _Option_ to show the
   _cartesian product_ taking effect then:
   ```
   scala> Apply[List].ifA(List(true,false,false))(List(1,2), List(3,4))
   val res0: List[Int] = List(1, 3, 3, 1, 4, 4, 2, 3, 3, 2, 4, 4)
   ```
1. For uniformity, the _ifM_ doc should have used _[A]_ as type parameter and
   given a code example instead of:
   ```
   def ifM[B](fb: F[Boolean])(ifTrue: => F[B], ifFalse: => F[B]): F[B]
   if lifted into monad.
   ```

_2022-11: function ifA got deprecated after we concluded this excercise._

```
-- Deprecation Warning: --------------------------------------------------------
  method ifA in trait Apply is deprecated since 2.6.2:
  Dangerous method, use ifM (a flatMap) or ifF (a map) instead
```

No explanation of "dangerous" is given, here: \
Was _ifA_ ill-defined in some usage context, e.g., nested Applicatives? \
Was _ifA_ just errorprone, e.g., hard to grasp in which order the arguments
are channeled?

#### 3.2 Code: Try out and unit-test the conditionals _ifF, ifA, ifM._

Discuss/document the functions' precise semantics in code comments.

Use a _multi-valued_ collection type (not just binary _Option_) to capture the
functions' combinatorial behaviour.

Also test for strictness in the parameters.

[scala tests](src/test/scala/netropy/polyglot/fp/cats/ex_3_2.ifF_ifA_ifM_Tests.scala)

#### 3.3 Q&A: Summarize the conditionals' signatures, semantics, and usage.

[Remarks](ex_3_3.r.conditionals.md)

[Up](../README.md)
