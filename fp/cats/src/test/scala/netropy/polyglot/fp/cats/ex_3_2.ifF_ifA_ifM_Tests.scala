package netropy.polyglot.fp.cats

import munit.ScalaCheckSuite
import org.scalacheck.Prop.forAll

import cats.{Functor, Apply, FlatMap}
//import cats.syntax.all._  // CATS implicits/givens, extension methods etc

/*
 * ex_3_2: Try out and unit-test the conditionals _ifF, ifA, ifM._
 *
 * A few, illustrative unit tests plus a property check for each conditional.
 *
 * Discussion:
 *
 * The functions ifF, ifA, ifM respectively lift the if-conditional into
 * Functor, Applicative (Apply), Monad (FlatMap).
 *
 * API docs:
 *
 * Deprecated:
 * "method ifA in trait Apply is deprecated (since 2.6.2):
 *  Dangerous method, use ifM (a flatMap) or ifF (a map) instead"
 *
 * The Scaladoc lacks clarity and has inaccuracies (as of CATS 2.2.0):
 * @see [[https://typelevel.org/cats/api/cats/Functor.html]]
 * @see [[https://typelevel.org/cats/api/cats/Apply.html]]
 * @see [[https://typelevel.org/cats/api/cats/FlatMap.html]]
 *
 * Signatures:
 *
 * ```
 *   ifF[A](fb: F[Boolean])(ifTrue: =>   A,  ifFalse: =>   A ): F[A]
 *   ifA[A](fb: F[Boolean])(ifTrue:    F[A], ifFalse:    F[A]): F[A]
 *   ifM[A](fb: F[Boolean])(ifTrue: => F[A], ifFalse: => F[A]): F[A]
 * ```
 *
 * Usage:
 *
 * The conditional combinators are curried.  This allows to store true/false
 * decisions in a context and later apply them to values in the context,
 * selecting and combining them according to the context.
 *
 * Strictness:
 *
 * ```
 *   ifF[A](b)(l, r):  strict in b, non-strict in l, r
 *   ifA[A](b)(l, r):  strict in all parameters b, l, r
 *   ifM[A](b)(l, r):  strict in b, non-strict in l, r
 * ```
 *
 * Semantics:
 *
 * ```
 *   Functor[F[_]].ifF(b)(l, r)
 *      == b.map(if (_) l else r)
 *
 *   Apply[F[_]].ifA(b)(l, r)
 *      == map3(l, r, b)((ll, rr, bb) => if (bb) ll else rr)
 *      == for { ll <- l; rr <- r; bb <- b } yield if (bb) ll else rr
 *      == l.flatMap(ll => r.flatMap(rr => b.map(bb => if (bb) ll else rr)))
 *
 *   FlatMap[F[_]].ifM(b)(l, r)
 *      == b.flatMap(if (_) l else r)
 * ```
 *
 * See below:
 * class ex_3_2_1_IfF_Tests
 * class ex_3_2_2_IfA_Tests
 * class ex_3_2_3_IfM_Tests
 */

/** ex_3_2_1: Try out and unit-test Functor combinator ifF.
  *
  * @see [[https://typelevel.org/cats/api/cats/Functor.html]]
  */
class ex_3_2_1_IfF_Tests
    extends ScalaCheckSuite
    with FunSuiteExt {

  // show usage: a Functor instance, the ifF combinator, stored decisions
  val lf: Functor[List] =
    Functor[List]
  val liff: List[Boolean] => (=> Int, => Int) => List[Int] =
    lf.ifF
  val liffTFT: (=> Int, => Int) => List[Int] =
    liff(List(true, false, true))

  test("test ifF strictness") {

    // strict in b, non-strict in l, r
    intercept[scala.NotImplementedError] {
      Functor[Option].ifF(???)(0, 1)
    }
    assertResult(Option.empty[Int]) {
      Functor[Option].ifF(None)(???, 1)
    }
    assertResult(Option.empty[Int]) {
      Functor[Option].ifF(None)(0, ???)
    }
    assertResult(Option.empty[Int]) {
      Functor[Option].ifF(None)(???, ???)
    }
  }

  test("test ifF[Option]") {

    // None if b == None, Some(l) if true, Some(r) if false
    assertResult(Option.empty[Int]) {
      Functor[Option].ifF(None)(0, 1)
    }
    assertResult(Option(0)) {
      Functor[Option].ifF(Some(true))(0, 1)
    }
    assertResult(Option(1)) {
      Functor[Option].ifF(Some(false))(0, 1)
    }
  }

  test("test ifF[List]") {

    // Nil if b == Nil
    assertResult(List.empty[Int]) {
      Functor[List].ifF(List())(0, 1)
    }

    // List(Boolean).map(if (_) l else r)
    assertResult(List(0)) {
      Functor[List].ifF(List(true))(0, 1)
    }
    assertResult(List(1)) {
      Functor[List].ifF(List(false))(0, 1)
    }

    // List(Boolean,Boolean).map(if (_) l else r)
    assertResult(List(0, 0)) {
      Functor[List].ifF(List(true, true))(0, 1)
    }
    assertResult(List(0, 1)) {
      Functor[List].ifF(List(true, false))(0, 1)
    }
    assertResult(List(1, 0)) {
      Functor[List].ifF(List(false, true))(0, 1)
    }
    assertResult(List(1, 1)) {
      Functor[List].ifF(List(false, false))(0, 1)
    }

    // List(Boolean,Boolean,Boolean).map(if (_) l else r)
    assertResult(List(0, 1, 0)) {
      Functor[List].ifF(List(true, false, true))(0, 1)
    }
    assertResult(List(0, 1, 1)) {
      Functor[List].ifF(List(true, false, false))(0, 1)
    }
  }

  test("test ifF(b)(l, r) == b.map(if (_) l else r)") {

    val (l, r) = (0, 1)
    forAll { (b: List[Boolean]) =>
      assertEquals(
        Functor[List].ifF(b)(l, r),
        b.map(if (_) l else r))
    }
  }
}

/** ex_3_2_2: Try out and unit-test Applicative combinator ifA (deprecated).
  *
  * DISABLED warning: method ifA in trait Apply is deprecated (since 2.6.2)
  *
  * @see [[https://typelevel.org/cats/api/cats/Apply.html]]
  */
@annotation.nowarn("cat=deprecation")
class ex_3_2_2_IfA_Tests
    extends ScalaCheckSuite
    with FunSuiteExt {

  // show usage: an Apply instance, the ifA combinator, stored decisions
  val la: Apply[List] =
    Apply[List]
  val lifa: List[Boolean] => (List[Int], List[Int]) => List[Int] =
    la.ifA
  val lifaTFT: (List[Int], List[Int]) => List[Int] =
    lifa(List(true, false, true))

  test("test ifA strictness") {

    // strict in all parameters
    intercept[scala.NotImplementedError] {
      Apply[Option].ifA(???)(None, None)
    }
    intercept[scala.NotImplementedError] {
      Apply[Option].ifA(None)(???, None)
    }
    intercept[scala.NotImplementedError] {
      Apply[Option].ifA(None)(None, ???)
    }
  }

  test("test ifA[Option]") {

    val l = Some(0)
    val r = Some(1)

    // None if b == None
    assertResult(Option.empty[Int]) {
      Apply[Option].ifA(None)(l, r)
    }
    assertResult(Option.empty[Int]) {
      Apply[Option].ifA(None)(None, r)
    }
    assertResult(Option.empty[Int]) {
      Apply[Option].ifA(None)(l, None)
    }

    // l if true
    assertResult(l: Option[Int]) {
      Apply[Option].ifA(Some(true))(l, r)
    }
    assertResult(l: Option[Int]) {
      Apply[Option].ifA(Some(true))(l, None)
    }
    assertResult(Option.empty[Int]) {
      Apply[Option].ifA(Some(true))(None, r)
    }

    // r if false
    assertResult(r: Option[Int]) {
      Apply[Option].ifA(Some(false))(l, r)
    }
    assertResult(Option.empty[Int]) {
      Apply[Option].ifA(Some(false))(l, None)
    }
    assertResult(r: Option[Int]) {
      Apply[Option].ifA(Some(false))(None, r)
    }
  }

  test("test ifA[List]") {

    val l = List(1, 2)
    val r = List(3, 4, 5)

    // Nil if b == Nil
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List())(l, r)
    }
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List())(Nil, r)
    }
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List())(l, Nil)
    }

    // Nil if (l X r) == Nil
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List(true))(Nil, r)
    }
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List(true))(l, Nil)
    }
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List(true))(Nil, Nil)
    }
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List(false))(Nil, r)
    }
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List(false))(l, Nil)
    }
    assertResult(List.empty[Int]) {
      Apply[List].ifA(List(false))(Nil, Nil)
    }

    // (l X r).map(_._1), (l X r).map(_._2)
    assertResult(List(1, 1, 1, 2, 2, 2)) {
      Apply[List].ifA(List(true))(l, r)
    }
    assertResult(List(3, 4, 5, 3, 4, 5)) {
      Apply[List].ifA(List(false))(l, r)
    }

    // (l X r X b).map { case (l, r, b) => if (b) l else r }
    assertResult(List(1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2)) {
      Apply[List].ifA(List(true, true))(l, r)
    }
    assertResult(List(1, 3, 1, 4, 1, 5, 2, 3, 2, 4, 2, 5)) {
      Apply[List].ifA(List(true, false))(l, r)
    }
    assertResult(List(3, 1, 4, 1, 5, 1, 3, 2, 4, 2, 5, 2)) {
      Apply[List].ifA(List(false, true))(l, r)
    }
    assertResult(List(3, 3, 4, 4, 5, 5, 3, 3, 4, 4, 5, 5)) {
      Apply[List].ifA(List(false, false))(l, r)
    }

    // (l X r X b).map { case (l, r, b) => if (b) l else r }
    assertResult(List(1, 3, 1, 1, 4, 1, 1, 5, 1, 2, 3, 2, 2, 4, 2, 2, 5, 2)) {
      Apply[List].ifA(List(true, false, true))(l, r)
    }
    assertResult(List(1, 3, 3, 1, 4, 4, 1, 5, 5, 2, 3, 3, 2, 4, 4, 2, 5, 5)) {
      Apply[List].ifA(List(true, false, false))(l, r)
    }
  }

  test("test ifA(b)(l, r) == for{x <- l;y <- r;z <- b} yield if (z) x else y") {

    val l = List(1, 2)
    val r = List(3, 4, 5)
    forAll { (b: List[Boolean]) =>
      assertEquals(
        Apply[List].ifA(b)(l, r),
        for {
          ll <- l
          rr <- r
          bb <- b
        } yield if (bb) ll else rr)
    }
  }
}

/** ex_3_2_3: Try out and unit-test Monad combinator ifM.
  *
  * @see [[https://typelevel.org/cats/api/cats/FlatMap.html]]
  */
class ex_3_2_3_IfM_Tests
    extends ScalaCheckSuite
    with FunSuiteExt {

  // show types
  val lm: FlatMap[List] =
    FlatMap[List]
  val lifm: List[Boolean] => (=> List[Int], => List[Int]) => List[Int] =
    lm.ifM
  val lifmTFT: (=> List[Int], => List[Int]) => List[Int] =
    lifm(List(true, false, true))

  test("test ifM strictness") {

    // strict in b, non-strict in l, r
    intercept[scala.NotImplementedError] {
      FlatMap[Option].ifM(???)(Some(0), Some(0))
    }
    assertResult(Option.empty[Int]) {
      FlatMap[Option].ifM(None)(???, Some(0))
    }
    assertResult(Option.empty[Int]) {
      FlatMap[Option].ifM(None)(Some(0), ???)
    }
    assertResult(Option.empty[Int]) {
      FlatMap[Option].ifM(None)(???, ???)
    }
  }

  test("test ifM[Option]") {

    val l = Some(0)
    val r = Some(1)

    // None if b == None
    assertResult(Option.empty[Int]) {
      FlatMap[Option].ifM(None)(l, r)
    }
    assertResult(Option.empty[Int]) {
      FlatMap[Option].ifM(None)(None, r)
    }
    assertResult(Option.empty[Int]) {
      FlatMap[Option].ifM(None)(l, None)
    }

    // l if true
    assertResult(l: Option[Int]) {
      FlatMap[Option].ifM(Some(true))(l, r)
    }
    assertResult(l: Option[Int]) {
      FlatMap[Option].ifM(Some(true))(l, None)
    }
    assertResult(Option.empty[Int]) {
      FlatMap[Option].ifM(Some(true))(None, r)
    }

    // r if false
    assertResult(r: Option[Int]) {
      FlatMap[Option].ifM(Some(false))(l, r)
    }
    assertResult(Option.empty[Int]) {
      FlatMap[Option].ifM(Some(false))(l, None)
    }
    assertResult(r: Option[Int]) {
      FlatMap[Option].ifM(Some(false))(None, r)
    }
  }

  test("test ifM[List]") {

    val l = List(1, 2)
    val r = List(3, 4, 5)

    // Nil if b == Nil
    assertResult(List.empty[Int]) {
      FlatMap[List].ifM(List())(l, r)
    }

    // l if true
    assertResult(List.empty[Int]) {
      FlatMap[List].ifM(List(true))(Nil, r)
    }
    assertResult(l) {
      FlatMap[List].ifM(List(true))(l, Nil)
    }
    assertResult(List.empty[Int]) {
      FlatMap[List].ifM(List(true))(Nil, Nil)
    }
    assertResult(l) {
      FlatMap[List].ifM(List(true))(l, r)
    }

    // r if false
    assertResult(r) {
      FlatMap[List].ifM(List(false))(Nil, r)
    }
    assertResult(List.empty[Int]) {
      FlatMap[List].ifM(List(false))(l, Nil)
    }
    assertResult(List.empty[Int]) {
      FlatMap[List].ifM(List(false))(Nil, Nil)
    }
    assertResult(r) {
      FlatMap[List].ifM(List(false))(l, r)
    }

    // List(Boolean,Boolean).flatMap(if (_) l else r)
    assertResult(l ::: l) {
      FlatMap[List].ifM(List(true, true))(l, r)
    }
    assertResult(l ::: r) {
      FlatMap[List].ifM(List(true, false))(l, r)
    }
    assertResult(r ::: l) {
      FlatMap[List].ifM(List(false, true))(l, r)
    }
    assertResult(r ::: r) {
      FlatMap[List].ifM(List(false, false))(l, r)
    }

    // List(Boolean,Boolean,Boolean).flatMap(if (_) l else r)
    assertResult(l ::: r ::: l) {
      FlatMap[List].ifM(List(true, false, true))(l, r)
    }
    assertResult(l ::: r ::: r) {
      FlatMap[List].ifM(List(true, false, false))(l, r)
    }
  }

  test("test ifM(b)(l, r) == b.flatMap(if (_) l else r)") {

    val l = List(1, 2)
    val r = List(3, 4, 5)
    forAll { (b: List[Boolean]) =>
      assertEquals(
        FlatMap[List].ifM(b)(l, r),
        b.flatMap(if (_) l else r))
    }
  }
}
