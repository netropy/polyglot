package netropy.polyglot.fp.cats

import munit.FunSuite

import cats.Id
//import cats.syntax.all._  // CATS implicits/givens, extension methods etc

/** ex_1_2: Try out and unit-test _Id[T]_ as Functor, Applicative etc.
  *
  * Just simple, illustrative unit tests, no need for property checks.
  *
  * @see [[https://typelevel.org/cats/datatypes/id.html]]
  * @see [[https://typelevel.org/cats/api/cats/index.html#Id[A]=A]]
  *
  * Discussion:
  *
  * - cats.Id[A] = A just a type alias, no c'tor
  *   elegant but also results in weak typing
  *   https://typelevel.org/cats/api/cats/index.html#Id[A]=A
  *
  * - Apply.ap[A, B](ff: F[(A) ⇒ B])(fa: F[A]): F[B]
  *   Note the swapped but more sensible order of parameters
  *     val id: Id[Int] = 1
  *     val g: Id[Int => Int] = _ + 1
  *     assertResult(2)(g.ap(id))
  *     //assertResult(2)(id.ap(g))  // wrong order
  *
  * - Irregular import names for implicit conversion:
  *   OK, canonical names:
  *     cats.syntax.functor.toFunctorOps
  *     cats.syntax.flatMap.toFlatMapOps
  *     cats.syntax.coflatMap.toCoflatMapOps
  *     cats.syntax.traverse.toTraverseOps
  *   but then
  *     //cats.syntax.apply.toApplyOps  // Oops, does not exist
  *     cats.syntax.apply.catsSyntaxApply  // need to use this one
  *
  * - Fixed: can flatMap(Id[Int]=>Int) and coflatMap(Int=>Id[Int])
  *     val g: Int => Id[Int] = f
  *     val h: Id[Int] => Int = f
  *     assertResult(2, id.flatMap(h))
  *     2022-12: ok for scalac 3.2.1, error with 2.13.10
  *       possibly compiler bug: https://github.com/scala/bug/issues/12622
  *     2020-09: compile error: cyclic aliasing or subtyping involving type Id
  *     assertResult(2)(id.coflatMap(g))
  */
class ex_1_2_Id_Tests
    extends FunSuite
    with FunSuiteExt {

  test("test Id as Functor") {

    // show id(1) == 1
    val id: Id[Int] = 1
    assertResult(1)(id + 0)

    // enable Functor.Ops
    import cats.syntax.functor.toFunctorOps

    // use Id as Functor
    assertResult(2)(id.map(_ + 1))
    assertResult(2)((1: Id[Int]).map(_ + 1))  // can lift 1 with ascription
  }

  test("test Id as Applicative") {

    val id: Id[Int] = 1
    val f: Int => Int = _ + 1
    val g: Id[Int => Int] = f

    // enable Apply.Ops
    //import cats.syntax.apply.toApplyOps  // canonical name, does not exist
    import cats.syntax.apply.catsSyntaxApply  // only this extension works
    //import cats.syntax.apply.catsSyntaxApplyOps  // not sure what these do
    //import cats.syntax.applicative.catsSyntaxApplicative
    //import cats.syntax.applicative.catsSyntaxApplicativeId

    // use Id as Applicative
    // Note order of params in Apply.ap[A, B](ff: F[(A) ⇒ B])(fa: F[A]): F[B]
    // this is why it is not:
    // assertResult(2)(id.ap(g))
    assertResult(2)(g.ap(id))
    assertResult(2)((f: Id[Int => Int]).ap(id))  // can lift f with ascription
  }

  test("test Id as Monad, Comonad") {

    val id: Id[Int] = 1
    val f: Int => Int = _ + 1
    val g: Int => Id[Int] = f
    val h: Id[Int] => Int = f

    // enable FlatMap.Ops, CoflatMap.Ops
    import cats.syntax.flatMap.toFlatMapOps
    import cats.syntax.coflatMap.toCoflatMapOps

    // use Id as Monad
    assertResult(2)(id.flatMap(f))
    assertResult(2)(id.flatMap(g))
    assertResult(2)(id.flatMap(h))

    // use Id as Comonad
    assertResult(2)(id.coflatMap(f))
    assertResult(2)(id.coflatMap(h))
    // scalac 3.2.1: ok
    // scalac 2.13.10: error: cyclic aliasing or subtyping involving type Id
    //assertResult(2)(id.coflatMap(g))
  }

  test("test Id as Traverse") {

    val id: Id[Int] = 1
    val f: (Int, Int) => String = (i,j) => s"$i$j"

    // enable Traverse.Ops
    import cats.syntax.traverse.toTraverseOps

    // use Id as Traversable
    assertResult((1, 0))(id.zipWithIndex)
    assertResult("10")(id.mapWithIndex(f))
  }
}
