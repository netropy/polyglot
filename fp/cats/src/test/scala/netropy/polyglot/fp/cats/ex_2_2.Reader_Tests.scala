package netropy.polyglot.fp.cats

import munit.ScalaCheckSuite
import org.scalacheck.Prop.forAll
import org.scalacheck.{Gen, Arbitrary}

import cats.data.Reader
//import cats.syntax.all._  // CATS implicits/givens, extension methods etc

/** ex_2_2: Try out and property-check the CATS _Reader[R,A]_ Monad.
  *
  * Notes:
  *
  * - Reader[R, A] wraps a function type R => A:
  *   of an injected "resource" R (or "environment" E) and a derived value A.
  *
  * - Core methods/functions:
  *     .run/.apply		evaluates the monad at a resource
  *     unit/pure		returns a given result
  *     get, ask		returns the resource as result
  *
  * - CATS specializes the type as: Reader[-A, B] = Kleisli[Id, A, B]
  *
  * - Argument order: Method f.ap(r) in Reader=Kleisli applies wrapped f to r.
  *
  * - Weird: No functions
  *     unit/pure(a) = Reader(_ => a)
  *     get/ask = Reader(identity)
  *   defined here, see below.
  *
  * - Note: value equality of Reader objects on wrapped function.
  *
  * - Straight-forward property checks using ScalaCheck std+custom generators.
  *
  * - Ok to have pattern duplication among {Writer,Reader,State}_Tests
  *   (factored out in monad experiments, see fp.effects).
  *
  * - See code comments below on CATS API / implicits issues.
  *
  * @see [[https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers]]
  * @see [[https://typelevel.org/cats/api/cats/data/package$$Reader$.html]]
  * @see [[https://typelevel.org/cats/api/cats/data/index.html#Reader[-A,B]=cats.data.package.ReaderT[cats.Id,A,B]]]
  */
class ex_2_2_Reader_Tests
    extends ScalaCheckSuite {

  // ----------------------------------------------------------------------
  // ScalaCheck Generators for Reader (summoning the arbitrary generator)
  // ----------------------------------------------------------------------

  val gen: Gen[Int => String] =
    Arbitrary.arbitrary[Int => String]

  val genR: Gen[Reader[Int, String]] =
    gen.map(Reader(_))

  val genRF: Gen[String => Reader[Int, String]] = for {
    f <- gen
  } yield ((s: String) => Reader(f.andThen(_ + s)))

  // ----------------------------------------------------------------------
  // Reader API Checks (see code comments on CATS API issues)
  // ----------------------------------------------------------------------

  test("check c'tor, equality, hash") {

    forAll(gen) { f =>
      forAll(gen) { g =>
        val rm0 = Reader(f)
        val rm1 = Reader(g)
        // CATS Reader: value equality on underlying function
        assert(rm0 == Reader(f))
        assert(rm1 == Reader(g))
        assert(f == g ^ rm0 != rm1)
        assert(rm0 != rm1 || rm0.## == rm1.##)
      }
    }
  }

  test("check .run, .apply: evaluates the monad at a resource") {

    forAll(gen) { f =>
      forAll { (i: Int) =>
        val rm = Reader(f)
        assertEquals(rm.run(i), f(i))
        assertEquals(rm.apply(i), f(i))
        assertEquals(rm(i), f(i))
      }
    }
  }

  /*
   * 2020-07: Lack of functions: Reader.pure (=unit), Reader.ask (=get)
   *
   * https://typelevel.org/cats/api/cats/index.html
   * https://www.javadoc.io/static/org.typelevel/cats-docs_2.13/2.9.0/index.html
   *
   * Problem: no such methods in cats.data.Reader: `pure/unit`, `ask/get`
   *   https://typelevel.org/cats/api/cats/data/package$$Reader$.html
   *   https://typelevel.org/cats/api/cats/data/Kleisli$.html
   *   https://typelevel.org/cats/api/cats/data/Kleisli.html
   *
   * For sure, Reader.apply() in the companion gives us `pure/unit`, `ask/get`:
   *     unit(a: => A) = Reader(_ => a)
   *     get = Reader(identity)
   *
   * But let's follow the compiler's suggestion to extend `pure` to Reader:
   *   """One of the following imports might fix the problem:
   *      import cats.implicits.catsSyntaxApplicativeId
   *      import cats.syntax.all.catsSyntaxApplicativeId
   *      import cats.syntax.applicative.catsSyntaxApplicativeId"""
   *
   * Problem: type-mismatch errors calling `pure`: really the Kleisli.pure?
   *   Kleisli[Id,Int,Int].pure(1)
   *   Reader[Int,Int].pure(1)
   *     Found:    (1 : Int)
   *     Required: cats.Applicative[F]
   *
   * Problem: Nope, confirmed, we really pick up the wrong `pure` method:
   *   Reader[Int,Int].pure(cats.Applicative[Id])(1)
   *   Reader[Int,Int].pure(cats.Applicative[Id]).apply(1)
   *     Found:    (1 : Int)
   *     Required: Int => Int
   *   https://typelevel.org/cats/api/cats/data/Kleisli$.html
   *   https://typelevel.org/cats/api/cats/syntax/ApplicativeIdOps.html
   *
   * => Solution: define Reader extension methods unit/pure, get/ask
   *
   * => Workaround: just for here, define helper functions unit/pure, get/ask
   */

  def unit[A](a: => A): Reader[Any, A] = Reader(_ => a)
  def get[R]: Reader[R, R] = Reader(identity)

  test("check unit (=pure): returns a given result") {

    forAll { (i: Int) =>
      assertEquals(unit(()).run(i), ())
    }
  }

  test("check get (=ask): returns the resource as result") {

    forAll { (i: Int) =>
      assertEquals(get.run(i), i)
    }
  }

  // ----------------------------------------------------------------------
  // Reader Functor/Monad Checks (no need to check as general laws)
  // ----------------------------------------------------------------------

  /*
   * 2022-12: Compile problems with implicit params followed by auto-apply
   *
   * Can auto-apply an object with an apply() method to an argument:
   *   scala> Reader(identity)(1)
   *   val res10: cats.Id[Any] = 1
   *
   * Cannot auto-apply after calling a method that takes an implicit/using:
   *   scala> Reader(identity).map(identity)(1)
   *   -- [E007] Type Mismatch Error:
   *      Found:    (1 : Int)
   *      Required: cats.Functor[cats.Id]
   *
   * Can again auto-apply when explicitly passing the implicit/using:
   *   scala> Reader(identity).map(identity)(cats.Functor[cats.Id])(1)
   *   val res11: cats.Id[Any] = 1
   *   scala> Reader(identity).map(identity)(using cats.Functor[cats.Id])(1)
   *   val res12: cats.Id[Any] = 1
   *
   * Can always explicitly call the object's apply() method:
   *   scala> Reader(identity).map(identity).apply(1)
   *   val res13: cats.Id[Any] = 1
   *
   * Can always invoke the Reader's run() value:
   *   scala> Reader(identity).map(identity).run(1)
   *   val res14: cats.Id[Any] = 1
   *
   * => Call run() or apply() explicitly, not worth debugging implicits, here.
   */

  test("check .map functor identity") {

    // fa: F[A], f: A => B
    // map(fa)(identity) == fa
    forAll(genR) { rm =>
      forAll { (i: Int) =>
        assertEquals(rm.map(identity).run(i), rm.run(i))
      }
    }
  }

  test("check .map functor composition") {

    // fa: F[A], f: A => B, g: B => C
    // map(map(fa)(f))(g) == map(fa)(f.andThen(g))
    forAll(genR) { rm =>
      forAll { (f: String => String, g: String => String) =>
        forAll { (i: Int) =>
          assertEquals(
            rm.map(f).map(g).run(i),
            rm.map(f.andThen(g)).run(i))
        }
      }
    }
  }

  test("check .map == flatMap + unit") {

    // probably redundant to check .consistency of map<->flatMap
    // a: A, fa: F[A], f: A => B
    // map(fa)(f) == flatMap(fa)(a => unit(f(a)))
    forAll(genR) { rm =>
      forAll { (f: String => String) =>
        forAll { (i: Int) =>
          assertEquals(
            rm.map(f).run(i),
            rm.flatMap(a => unit(f(a))).run(i))
        }
      }
    }
  }

  //
  // Note: CATS Reader = Kleisli method f.ap(r) takes Reader argument
  //
  test("check .ap == flatMap + unit") {

    // probably redundant to check .consistency of ap<->flatMap
    // a: A, fa: F[A], ff: F[A => B]
    // ap(ff)(fa) == flatMap(ff)(f => map(fa)(f))
    forAll { (f: String => String) =>
      forAll(genR) { rm =>
        forAll { (i: Int) =>
          assertEquals(
            unit(f).ap(rm).run(i),
            unit(f).flatMap(f => rm.map(f)).run(i))
        }
      }
    }
  }

  test("check .flatMap monad left identity") {

    // a: A, fa: F[A], f: A => F[B]
    // flatMap(unit(a))(f) == f(a)
    forAll(genRF) { rmf =>
      forAll { (s: String) =>
        forAll { (i: Int) =>
          assertEquals(
            unit(s).flatMap(rmf).run(i),
            rmf(s).run(i))
        }
      }
    }
  }

  test("check .flatMap monad right identity") {

    // a: A, fa: F[A], f: A => F[B]
    // flatMap(fa)(unit) == fa
    forAll(genR) { rm =>
      forAll { (i: Int) =>
        assertEquals(
          rm.flatMap(unit(_)).run(i),
          rm.run(i))
      }
    }
  }

  test("check .flatMap monad associativity") {

    // a: A, fa: F[A], f: A => F[B], g: B => F[C]
    // flatMap(fa)(a => flatMap(f(a))(g)) == flatMap(flatMap(fa)(f))(g)
    forAll(genR) { rm =>
      forAll(genRF, genRF) { (rmf0, rmf1) =>
        forAll { (i: Int) =>
          assertEquals(
            rm.flatMap(rmf0(_).flatMap(rmf1)).run(i),
            rm.flatMap(rmf0).flatMap(rmf1).run(i))
        }
      }
    }
  }
}
