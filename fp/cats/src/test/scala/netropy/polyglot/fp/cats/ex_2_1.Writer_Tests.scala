package netropy.polyglot.fp.cats

import munit.ScalaCheckSuite
import org.scalacheck.Prop.forAll
import org.scalacheck.{Gen, Arbitrary}

import cats.Monoid
import cats.data.Writer
//import cats.syntax.all._  // CATS implicits/givens, extension methods etc

/** ex_2_1: Try out and property-check the CATS _Writer[W,A]_ Monad.
  *
  * Notes:
  *
  * - Writer[W, A] wraps a tuple type (W, A):
  *   of an underlying, write-only "log" W (or L) and a value A (or V).
  *
  * - Core methods/functions:
  *     .run, .value, .written	returns the writer's value/log
  *     .reset, value/unit	deletes the log
  *     .tell, tell		appends/initializes the log
  *     .listen, listen		pairs the current log with the value
  *     .swap			exchanges value and log
  *
  * - CATS specializes the type as: Writer[L, V] = WriterT[Id, L, V]
  *
  * - Weird: Method .ap in Writer=WriterT takes function argument: w.ap(f);
  *   reverse of Reader f.ap(r).
  *
  * - Weird: No function unit/pure = Writer.value; defined here, see below.
  *
  * - Note: value equality of Writer objects on wrapped tuple.
  *
  * - Straight-forward property checks using ScalaCheck std+custom generators.
  *
  * - Ok to have pattern duplication among {Writer,Reader,State}_Tests
  *   (factored out in monad experiments, see fp.effects).
  *
  * See other code comments below.
  *
  * @see [[https://typelevel.org/cats/datatypes/writer.html]]
  * @see [[https://typelevel.org/cats/api/cats/data/package$$Writer$.html]]
  * @see [[https://typelevel.org/cats/api/cats/data/index.html#Writer[L,V]=cats.data.WriterT[cats.Id,L,V]]]
  */
class ex_2_1_Writer_Tests
    extends ScalaCheckSuite {

  // ----------------------------------------------------------------------
  // ScalaCheck Generators for Writer (summoning the arbitrary generator)
  // ----------------------------------------------------------------------

  val gen: Gen[(String, Int)] =
    Arbitrary.arbitrary[(String, Int)]

  val genW: Gen[Writer[String, Int]] =
    gen.map { case (s, i) => Writer(s, i) }  // Scala2+3

  val genWF: Gen[Int => Writer[String, Int]] = for {
    (s, i) <- gen
  } yield ((z: Int) =>
    Writer(s"s\n$z", i + z))

  // ----------------------------------------------------------------------
  // Writer API Checks
  // ----------------------------------------------------------------------

  test("check c'tor, equality, hash") {

    forAll(gen) { case (s0, i0) =>  // Scala2+3
      forAll(gen) { case (s1, i1) =>
        val wm0 = Writer(s0, i0)
        val wm1 = Writer(s1, i1)
        // CATS Writer: value equality on underlying tuple
        assert(wm0 == Writer(s0, i0))
        assert(wm1 == Writer(s1, i1))
        assert((s0, i0) == (s1, i1) ^ wm0 != wm1)
        assert(wm0 != wm1 || wm0.## == wm1.##)
      }
    }
  }

  test("check .run, .value, .written: returns the writer's value/log") {

    forAll(gen) { case (s, i) =>  // Scala2+3
      val wm = Writer(s, i)
      assertEquals(wm.run, (s, i))  // no .apply
      assertEquals(wm.value, i)
      assertEquals(wm.written, s)
    }
  }

  // alias for uniformity, see Reader_Tests
  def unit[W, A](a: A)(implicit mw: Monoid[W]): Writer[W, A] = Writer.value(a)

  test("check .reset, value, unit: deletes the log") {

    forAll(gen) { case (s, i) =>  // Scala2+3
      // implicit ev: Monoid[W]
      assertEquals(Writer(s, i).reset.run, ("", i))
      assertEquals(Writer.value[String, Int](i).run, ("", i))
      assertEquals(unit[String, Int](i).run, ("", i))
    }
  }

  test("check .tell, tell: appends/initializes the log") {

    forAll(gen) { case (s, i) =>  // Scala2+3
      // implicit ev: Semigroup[W]
      assertEquals(Writer(s, i).tell(s).run, (s + s, i))
      assertEquals(Writer.tell(s).run, (s, ()))
    }
  }

  test("check .listen, listen: pairs the current log with the value") {

    forAll(gen) { case (s, i) =>  // Scala2+3
      val wm = Writer(s, i)
      assertEquals(wm.listen.run, (s, (i, s)))
      assertEquals(Writer.listen(wm).run, (s, (i, s)))
    }
  }

  test("check .swap: exchanges value and log") {

    forAll(gen) { case (s, i) =>  // Scala2+3
      assertEquals(Writer(s, i).swap.run, (i, s))
    }
  }

  // ----------------------------------------------------------------------
  // Writer Functor/Monad Checks (no need to check as general laws)
  // ----------------------------------------------------------------------

  test("check .map functor identity") {

    // fa: F[A], f: A => B
    // map(fa)(identity) == fa
    forAll(genW) { wm =>
      assertEquals(wm.map(identity).run, wm.run)
    }
  }

  test("check .map functor composition") {

    // fa: F[A], f: A => B, g: B => C
    // map(map(fa)(f))(g) == map(fa)(f.andThen(g))
    forAll(genW) { wm =>
      forAll { (f: Int => Int, g: Int => Int) =>
        assertEquals(
          wm.map(f).map(g).run,
          wm.map(f.andThen(g)).run)
      }
    }
  }

  test("check .map == flatMap + unit") {

    // probably redundant to check .consistency of map<->flatMap
    // a: A, fa: F[A], f: A => FB
    // map(fa)(f) == flatMap(fa)(a => unit(f(a)))
    forAll(genW) { wm =>
      forAll { (f: Int => Int) =>
        assertEquals(
          wm.map(f).run,
          wm.flatMap(a => unit(f(a))).run)
      }
    }
  }

  //
  // Note: CATS Writer method w.ap(f) takes function, reverse of Reader f.ap(r)
  //
  test("check .ap == flatMap + unit") {

    // probably redundant to check .consistency of ap<->flatMap
    // a: A, fa: F[A], ff: F[A => B]
    // ap(ff)(fa) == flatMap(ff)(f => map(fa)(f))
    forAll { (f: Int => Int) =>
      forAll(genW) { wm =>
        assertEquals(
          wm.ap(unit[String, Int => Int](f)).run,
          unit[String, Int => Int](f).flatMap(f => wm.map(f)).run)
      }
    }
  }

  test("check .flatMap monad left identity") {

    // a: A, fa: F[A], f: A => F[B]
    // flatMap(unit(a))(f) == f(a)
    forAll(genWF) { wmf =>
      forAll { (i: Int) =>
        assertEquals(
          unit[String, Int](i).flatMap(wmf).run,  // implicit ev: Monoid[W]
          wmf(i).run)
      }
    }
  }

  test("check .flatMap monad right identity") {

    // a: A, fa: F[A], f: A => F[B]
    // flatMap(fa)(unit) == fa
    forAll(genW) { wm =>
      assertEquals(
        wm.flatMap(unit[String, Int]).run,  // implicit ev: Monoid[W]
        wm.run)
    }
  }

  test("check .flatMap monad associativity") {

    // a: A, fa: F[A], f: A => F[B], g: B => F[C]
    // flatMap(fa)(a => flatMap(f(a))(g)) == flatMap(flatMap(fa)(f))(g)
    forAll(genW) { wm =>
      forAll(genWF, genWF) { (wmf0, wmf1) =>
        assertEquals(
          wm.flatMap(wmf0(_).flatMap(wmf1)).run,
          wm.flatMap(wmf0).flatMap(wmf1).run)
      }
    }
  }
}
