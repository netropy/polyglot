package netropy.polyglot.fp.cats

// concurrent unit test below
import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

import munit.ScalaCheckSuite
import org.scalacheck.Prop.forAll
import org.scalacheck.{Gen, Arbitrary}

import cats.data.State
//import cats.syntax.all._  // CATS implicits/givens, extension methods etc

/** ex_2_3: Try out and property-check the CATS _State[S,A]_ Monad.
  *
  * Notes:
  *
  * - State[S, A] wraps a function type S => (S, A):
  *   of a carried & transformed "state" S, from which values A are generated.
  *
  * - Core methods/functions:
  *     .run*		evaluates the monad at an initial state
  *     get		extracts the state as the result
  *     inspect		extracts the state via a transformation function
  *     pure, unit	keeps the state and assigns a result
  *     set		assigns a state
  *     modify		updates the state via a transformation function
  *
  * - CATS specializes the type as: State[S, A] = IndexedStateT[Eval, S, S, A]
  *
  * - Weird: No method .ap in State = IndexedStateT, no f.ap(s) nor s.ap(f)
  *
  * - Weird: No function unit = State.pure; defined here, see below.
  *
  * - Weird: No value equality of State objects on wrapped function.
  *
  * - Weird: No method .apply = .run
  *
  * - Straight-forward property checks using ScalaCheck std+custom generators.
  *
  * - Ok to have pattern duplication among {Writer,Reader,State}_Tests
  *   (factored out in monad experiments, see fp.effects).
  *
  * - One concurrent test for the joy of a purely functional data structure :-)
  *
  * See other code comments below.
  *
  * @see [[https://typelevel.org/cats/datatypes/state.html]]
  * @see [[https://typelevel.org/cats/api/cats/data/package$$State$.html]]
  * @see [[https://typelevel.org/cats/api/cats/data/index.html#State[S,A]=cats.data.package.StateT[cats.Eval,S,A]]]
  */
class ex_2_3_State_Tests
    extends ScalaCheckSuite {

  // ----------------------------------------------------------------------
  // ScalaCheck Generators for State (summoning the arbitrary generator)
  // ----------------------------------------------------------------------

  val gen: Gen[Int => (Int, String)] =
    Arbitrary.arbitrary[Int => (Int, String)]

  val genS: Gen[State[Int, String]] =
    gen.map(State(_))

  val genSF: Gen[String => State[Int, String]] = for {
    f <- gen
  } yield ((z: String) =>
    State(f.andThen { case (s, a) => (s + z.length, a + z) } ))  // Scala2+3

  // ----------------------------------------------------------------------
  // State API Checks
  // ----------------------------------------------------------------------

  test("check c'tor, equality, hash") {

    forAll(gen) { f =>
      forAll(gen) { g =>
        val sm0 = State(f)
        val sm1 = State(g)
        // CATS State: no value equality on underlying function
        //assert(sm0 == State(f))
        //assert(sm1 == State(g))
        //assert(f == g ^ sm0 != sm1)
        assert(sm0 != sm1 || sm0.## == sm1.##)
      }
    }
  }

  test("check .run, .runS, .runA: evaluates the monad at an initial state") {

    forAll(gen) { f =>
      forAll { (i: Int) =>
        val sm = State(f)
        // no .apply; .run yields cats.Eval, .value forces evaluation
        val (state, value): (Int, String) = sm.run(i).value
        assertEquals((state, value), f(i))

        // let's run some repeated evaluations
        for (j <- 0 to 3) {
          assertEquals(sm.run(i).value, (state, value))
          assertEquals(sm.runS(i).value, state)
          assertEquals(sm.runA(i).value, value)
        }
      }
    }
  }

  test("check .run -- concurrently (functional data structure :-)") {

    // Just for the joy of a purely functional, non-blocking data structure...
    forAll(gen) { f =>
        val sm = State(f)

        // Good enough, here: Futures with sequentially awaited results
        // fails fast on concurrently executed asserts:
        //   java.util.concurrent.ExecutionException: Boxed Exception
        //   Caused by: munit.ComparisonFailException
        val n = 1000
        val d = Duration(1, "s")  // timeout per awaited result, not total
        val run: Vector[Future[Unit]] = Vector.tabulate(n) { i =>
          Future(assertEquals(sm.run(i).value, f(i)))
        }
        run.foreach(Await.result(_, d))  // note: Await.ready does not throw
    }
  }

  test("check get: extracts the state as the result") {

    forAll { (i: Int) =>
      assertEquals(State.get.run(i).value, (i, i))
    }
  }

  test("check inspect: extracts the state via a transformation function") {

    forAll { (i: Int) =>
      assertEquals(State.inspect((s: Int) => s"$s").run(i).value, (i, s"$i"))
    }
  }

  // alias for uniformity, see Reader_Tests
  def unit[S, A](a: A): State[S, A] = State.pure(a)

  test("check pure, unit: keeps the state and assigns a result") {

    forAll { (r: String, i: Int) =>
      assertEquals(State.pure(r).run(i).value, (i, r))
      assertEquals(unit[Int, String](r).run(i).value, (i, r))
    }
  }

  test("check set: assigns a state") {

    forAll { (i: Int, j: Int) =>
      assertEquals(State.set(j).run(i).value, (j, ()))
    }
  }

  test("check modify: updates the state via a transformation function") {

    forAll { (i: Int) =>
      assertEquals(State.modify((j: Int) => j + 1).run(i).value, (i + 1, ()))
    }
  }

  // ----------------------------------------------------------------------
  // State Functor/Monad Checks (no need to check as general laws)
  // ----------------------------------------------------------------------

  test("check .map functor identity") {

    // fa: F[A], f: A => B
    // map(fa)(identity) == fa
    forAll(genS) { sm =>
      forAll { (i: Int) =>
        assertEquals(sm.map(identity).run(i).value, sm.run(i).value)
      }
    }
  }

  test("check .map functor composition") {

    // fa: F[A], f: A => B, g: B => C
    // map(map(fa)(f))(g) == map(fa)(f.andThen(g))
    forAll(genS) { sm =>
      forAll { (f: String => String, g: String => String) =>
        forAll { (i: Int) =>
          assertEquals(
            sm.map(f).map(g).run(i).value,
            sm.map(f.andThen(g)).run(i).value)
        }
      }
    }
  }

  test("check .map == .flatMap + unit") {

    // probably redundant to check .consistency of map<->flatMap
    // a: A, fa: F[A], f: A => B
    // map(fa)(f) == flatMap(fa)(a => unit(f(a)))
    forAll(genS) { sm =>
      forAll { (f: String => String) =>
        forAll { (i: Int) =>
          assertEquals(
            sm.map(f).run(i).value,
            sm.flatMap(a => unit(f(a))).run(i).value)
        }
      }
    }
  }

  //
  // Note: CATS State = IndexedStateT has no method .ap, no f.ap(s) nor s.ap(f)
  //
  // test("check .ap == flatMap + unit") {
  //
  //   // probably redundant to check .consistency of ap<->flatMap
  //   // a: A, fa: F[A], ff: F[A => B]
  //   // ap(ff)(fa) == flatMap(ff)(f => map(fa)(f))
  //   forAll { (f: String => String) =>
  //     forAll(genS) { sm =>
  //       forAll { (i: Int) =>
  //         assertEquals(
  //           unit(f).ap(sm).run(i),
  //           unit(f).flatMap(f => sm.map(f)).run(i))
  //       }
  //     }
  //   }
  // }

  test("check .flatMap monad left identity") {

    // a: A, fa: F[A], f: A => F[B]
    // flatMap(unit(a))(f) == f(a)
    forAll(genSF) { smf =>
      forAll { (s: String) =>
        forAll { (i: Int) =>
          assertEquals(
            unit(s).flatMap(smf).run(i).value,
            smf(s).run(i).value)
        }
      }
    }
  }

  test("check .flatMap monad right identity") {

    // a: A, fa: F[A], f: A => F[B]
    // flatMap(fa)(unit) == fa
    forAll(genS) { sm =>
      forAll { (i: Int) =>
        assertEquals(
          sm.flatMap(unit).run(i).value,
          sm.run(i).value)
      }
    }
  }

  test("check .flatMap monad associativity") {

    // a: A, fa: F[A], f: A => F[B], g: B => F[C]
    // flatMap(fa)(a => flatMap(f(a))(g)) == flatMap(flatMap(fa)(f))(g)
    forAll(genS) { sm =>
      forAll(genSF, genSF) { (smf0, smf1) =>
        forAll { (i: Int) =>
          assertEquals(
            sm.flatMap(smf0(_).flatMap(smf1)).run(i).value,
            sm.flatMap(smf0).flatMap(smf1).run(i).value)
        }
      }
    }
  }
}
