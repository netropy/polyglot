package netropy.polyglot.fp.cats

import munit.FunSuite

/** A bit of sugar for MUnit tests. */
trait FunSuiteExt {
  this: FunSuite =>  // reassign this

  def assertResult[A](exp: => A)(act: => A) =
    assertEquals(act, exp)
}
