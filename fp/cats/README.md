# polyglot/fp/cats

### CATS: Code Experiments, Exercises, Notes

Projects:
- [Exercises, Q&A, Code Experiments with CATS](ex.q.cats_basics.md)

Quick links: \
[scala tests](src/test/scala/netropy/polyglot/fp/cats/)

CATS links: \
[website](https://typelevel.org/cats/),
[@github](https://github.com/typelevel/cats),
[@scaladex](https://index.scala-lang.org/typelevel/cats),
[apidocs](https://typelevel.org/cats/api/cats/index.html),
[apidocs 2.13](https://javadoc.io/doc/org.typelevel/cats-docs_2.13/latest/cats/index.html),
[apidocs 3](https://javadoc.io/doc/org.typelevel/cats-docs_3/latest/cats/index.html)

Excellent book (plain language, meaningful excercises, case studies): \
[Scala with Cats 2](https://www.scalawithcats.com)
(2020-07) by Noel Welsh, Dave Gurnell of
[underscore](https://underscore.io/books/scala-with-cats)

More resources: \
[CATS Resources](https://typelevel.org/cats/resources_for_learners.html),
[CATS Glossary](https://typelevel.org/cats/nomenclature.html),
[Scala Exercises: CATS](https://www.scala-exercises.org/cats)

Other CATS-related projects:
- [Cats Effect](https://typelevel.org/cats-effect):
  lightweight fibers for asynchronous, highly concurrent applications
- [Cats MTL](https://typelevel.org/cats-mtl):
  more transformer typeclasses for monads, applicatives, functors
- [Cats Collections](https://typelevel.org/cats-collections/index.html):
  data structures facilitating pure functional programming
- [Mouse](https://typelevel.org/mouse):
  enrichments for CATS and Scala standard library types
- [Cats Law](https://typelevel.org/cats/typeclasses/lawtesting.html):
  test framework based on
  [discipline](https://github.com/typelevel/discipline) and
  [ScalaCheck](https://github.com/typelevel/scalacheck)
- [Typelevel Projects](https://typelevel.org/projects/):
  projects from general functional programming to tooling

Tools: \
[sbt](https://www.scala-sbt.org),
[Maven](https://maven.apache.org),
[MUnit](https://scalameta.org/munit),
[ScalaCheck](https://www.scalacheck.org),
[MUnit-ScalaCheck](https://index.scala-lang.org/scalameta/munit/artifacts/munit-scalacheck)

[Up](../README.md)
