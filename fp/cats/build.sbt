name := "polyglot.fp.cats"
organization := "netropy"
description := "CATS, code experiments, exercises"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

//// OK: Scala 2.13 and 3
//scalaVersion := "2.13.10"
scalaVersion := "3.2.1"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  //"-Ytasty-reader",  // scala2
  //"-explaintypes",  // scala2
  //"-explain",  // scala3
)

// Resolvers (pre- or user-defined)
// https://www.scala-sbt.org/1.x/docs/Resolvers.html
//resolvers += DefaultMavenRepository  // inluded by default
//resolvers += Resolver.mavenLocal  // ~/.m2

// https://index.scala-lang.org/typelevel/cats
// https://mvnrepository.com/artifact/org.typelevel/cats-core
// https://typelevel.org/cats
// https://github.com/typelevel/cats
libraryDependencies += "org.typelevel" %% "cats-core" % "2.9.0"

// https://index.scala-lang.org/scalameta/munit/artifacts/munit-scalacheck
// https://mvnrepository.com/artifact/org.scalameta/munit-scalacheck
// https://scalameta.org/munit/docs/integrations/scalacheck.html
//
// scalac 3.2 + munit-scalacheck_3, scalac 2.13 + munit-scalacheck_2.13
libraryDependencies += "org.scalameta" %% "munit-scalacheck" % "0.7.29" % Test

// only use a single thread for building
//parallelExecution := false
// execute tests in the _current_ project serially (default parallel)
//Test / parallelExecution := false

// fork a new JVM for 'run' and 'Test/run'
//fork := true
// fork a new JVM for 'Test/run', but not 'run'
//Test / fork := true
// add a JVM option to use when forking a JVM for 'run'
//javaOptions += "-Xmx8G"

//Test / testOptions := Seq(Tests.Filter(s => s.endsWith("Test")))

// print the output of tests immediately instead of buffering
//Test / logBuffered := false
