# polyglot/fp

### Notes: Other Scala FP Libraries

- [Typelevel projects](https://typelevel.org/projects/),
  [@github](https://github.com/typelevel)
- [47 Degrees projects](https://47degrees.github.io/org/),
  [@github](https://github.com/47degrees)
- [doobie](https://tpolecat.github.io/doobie),
  [@github](https://github.com/tpolecat/doobie),
  [@mvnrepo](https://mvnrepository.com/artifact/org.tpolecat/doobie-core):
  pure functional JDBC layer for Scala
- Shapeless,
  [@github](https://github.com/milessabin/shapeless)
  [@mvnrepo](https://mvnrepository.com/artifact/com.chuusai/shapele)
  typeclass and dependent type based generic programming library
- [eff](https://atnos-org.github.io/eff),
  [@github](https://github.com/atnos-org/eff),
  [@mvnrepo](https://mvnrepository.com/artifact/org.atnos/eff),
  extensible effects, an alternative to monad transformers
- [FS2](https://fs2.io),
  [@github](https://github.com/typelevel/fs2),
  [@mvnrepo](https://mvnrepository.com/artifact/co.fs2):
  compositional, streaming I/O library for Scala
- [Monix](https://monix.io),
  [@github](https://github.com/monix),
  [@mvnrepo](https://mvnrepository.com/artifact/io.monix):
  asynchronous Programming for Scala and Scala.js
