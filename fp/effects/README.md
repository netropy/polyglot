# polyglot/fp/effects

TODO: fix

### CATS: Code Experiments, Exercises, Notes

- [_Effect_ types (StateM, ReaderM)](Effects/ex_1.q.effects.md)


Projects:
- [Exercises, Q&A, Code Experiments with CATS](ex.q.cats_basics.md)

Quick links: \
[scala tests](src/test/scala/netropy/polyglot/fp/cats/)

CATS links: \
[website](https://typelevel.org/cats/),
[@github](https://github.com/typelevel/cats),
[@scaladex](https://index.scala-lang.org/typelevel/cats),
[apidocs](https://typelevel.org/cats/api/cats/index.html),
[apidocs 2.13](https://javadoc.io/doc/org.typelevel/cats-docs_2.13/latest/cats/index.html),
[apidocs 3](https://javadoc.io/doc/org.typelevel/cats-docs_3/latest/cats/index.html)

Excellent book (plain language, meaningful excercises, case studies): \
[Scala with Cats](https://www.scalawithcats.com)
(2022-07) by Noel Welsh, Dave Gurnell of
[underscore](https://underscore.io/books/scala-with-cats)

Tools: \
[sbt](https://www.scala-sbt.org),
[Maven](https://maven.apache.org),
[MUnit](https://scalameta.org/munit),
[ScalaCheck](https://www.scalacheck.org),
[MUnit-ScalaCheck](https://index.scala-lang.org/scalameta/munit/artifacts/munit-scalacheck)

[Up](../README.md)
