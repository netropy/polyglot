package netropy.polyglot.fp.effects


/** Supports passing an updatable state through a computation.
  *
  * TODO: shorten/cleanup
  *
  * Available from CATS and other FP libaries:
  * [[https://typelevel.org/cats/datatypes/state.html]]
  * [[https://typelevel.org/cats/api/cats/data/package$$State$.html]]
  * [[https://typelevel.org/cats/api/cats/data/index.html#State[S,A]=cats.data.package.StateT[cats.Eval,S,A]]]
  *
  * This implementation, unit- and property tests as exercise.
  *
  * Refresher: The State Monad
  *
  * - Wraps a function `S => (S, A)` that transforms a state `s0 => s1` and
  *   produces a value `a`.
  *
  *   Example: a `StateM` instance encapsulating a random number generator
  *   with its running state `s`.  To convert the generated, raw `a` values
  *   to other types, custom mappings can be chained to the instance.  This
  *   keeps the usage site clear of the internal state or of any raw values.
  *
  * - Features:
  *   - has constructor: `StateM(s0 => (s1, a))`
  *   - has "constant state" ctor: `StateM.unit(a) = StateM(s => (s, a))`
  *   - has "get state" ctor: `StateM.get = StateM(s => (s, s))`
  *   - has "set state" ctor: `set[S](s) = StateM(_ => (s, ()))`
  *   - has "modify state" ctor: `modify(f) = get.flatMap(s => set(f(s)))`
  *   - has functor+monad functions `map`, `flatMap`
  *   - implements `Function1` for convenience, can apply: `(s1, a) = sm(s0)`
  *
  * - Form state-less computations: `map` the result value
  * TODO: shorten
  *   - `a => b`
  *     function maps the result value, for example: `b = f(a, env)`
  *
  *   - `sm1 = sm0.map( a => b )`
  *     `sm1 = sm0.map( a => f(a) ).map( b => g(b) )  // chain`
  *     each `map` accumulates in the result value
  *     subject to functor laws: identity, composition
  *
  * - Form state-ful computations: `flatMap` another state monad
  * TODO: shorten
  *   - `a => StateM(s0 => (s1, b))`
  *     monad function maps both, state and result: `(s0, a) => (s1, b)`
  *     for example: `s1 = g(s0, a, b, env)` and `b = f(s0, s1, a, env)`
  *
  *   - `sm2 = sm0.flatMap( a => StateM(s0 => (s1, b)) )`
  *     `sm3 = sm0.flatMap( a => sm1.flatMap( b => sm2 ) )  // nest`
  *     `sm3 = sm0.flatMap( a => sm1 ).flatMap( b => sm2 )  // == chain`
  *     each `flatMap` accumulates in both, the state and result value
  *     subject to monad laws: left/right identity, associativity
  *
  * - Specializations, Generalizations, Variations:
  *
  *   - taking 2 type parameters `StateM[S, +A]`, technically not a monad;
  *     can be made to meet monad by fixing one type with type projection
  *
  *   - pure state transformation: use `Unit` as value type
  *   - pure generation of a constant: state transformation is identity
  *
  *   - base on other context: transformer `StateT` provides `S => F[(S, A)]`
  *   - multiple types for states: `IndexedStateT` provides `SA => F[(SB, A)]`
  *
  *   - work with read-only state: -> Reader Monad
  *   - accumulate a value without using it on the way: -> Writer Monad
  *
  *   - categorical dual: -> Costate Comonad a.k.a. "Store" Comonad
  *     (a constant value and a modifiable accessor or "focus" function)
  *
  * Notes:
  *
  * - Verifying: `map` accumulates in the result `A`, not in the state `S`
  *
  *   In a cascade of `map` operations, the state-changing, user-defined
  *   function `run` is called exactly 1x.
  *
  *   Per Functor Identity and Composition Laws, `map` doesn't modify state:
  *     `m == m.map(identity)`
  *     `m.map(f).map(g) == m.map(f andThen g)`
  *
  *   Example:
  *   ```
  *     val m: Int => (Int, String) = s => (s + 1, s"${s}m")
  *     val f: String => String = _ + "f"
  *     val sm = StateM(m)
  *
  *     (a)  sm(1)                         ~> (2, "1m")   1x m(), 1x apply()
  *     (b)  sm.map(f)(1)                  ~> (2, "1mf")  1x m(), 2x apply()
  *     (c)  sm.map(f).map(f)(1)           ~> (2, "1mff") 1x m(), 3x apply()
  *     (d)  sm.map(f andThen f)(1)        ~> (2, "1mff") 1x m(), 2x apply()
  *   ```
  *
  * - Verifying: `flatMap` accumulates in both, the result `A` and state `S`
  *
  *   The monad function argument may change the state in addition to the
  *   user-defined function `run`.
  *
  *   Per Monad Left Identity and Associativity Law, the monad function
  *   passed to `flatMap` affects the state as if called directly on it:
  *     `f(x) =  StateM.unit(x).flatMap(f)`
  *     `m.flatMap(f(_).flatMap(g)) == m.flatMap(f).flatMap(g)`
  *
  *   Example:
  *   ```
  *     val m: Int => (Int, String) = s => (s + 1, s"${s}m")
  *     val g: String => StateM[Int, String] =
  *       a => StateM(s => (s * 2, s"$a${s}g"))
  *     val sm = StateM(m)
  *
  *     (a)  sm(1)                           ~> (2, "1m")     1x m(), 0x g()
  *     (b)  sm.flatMap(g)(1)                ~> (4, "1m2g")   1x m(), 1x g()
  *     (c)  sm.flatMap(g).flatMap(g)(1)     ~> (8, "1m2g4g") 1x m(), 2x g()
  *     (d)  sm.flatMap(g(_).flatMap(g))(1)  ~> (8, "1m2g4g") 1x m(), 2x g()
  *   ```
  *
  * - Question: What if State Monad wrapped a function `(S, A) => (S, B)`?
  *
  *   Idea: Looks more symmetrical.
  *         Why wrap a value-making but not a -taking function?
  *         Allow both, to incorporate and extract values into/from state.
  *         Allow both, to accumulate mappings "on the way down" and "up".
  *         Would include the special cases:
  *         - using `Unit`: `(S, A) => S`, `S => (S, B)`, `S => S`
  *         - bifunction:   `(S => S, A => B)`
  *
  *   - Why do CATS etc lack a (State) Monad with a value-taking function?
  *   - Let's try implement something like `State((S, A) => S)` ...
  *
  * - Experiment: Monad wrapping value-taking function `(S, A) => S`
  *
  *   Idea: Accumulate mappings `f(a)` in the value "on the way down".
  *   - like an inverse monad: `InvStateM[S, -A] <=> StateM[S, +A]`
  *   - but not all arrows reversed, i.e., not a Comonad
  *   - FAILED, not working for `flatMap`, unclear semantics
  *   ```
  *     case class InvStateM[S, -A](take: (S, A) => S) {
  *       def apply(s: S, a: A): S = take(s, a)
  *       def apply(sa: (S, A)): S = take.tupled(sa)
  *
  *       // commutes, map "on the way down" (but should this be `comap`?)
  *       def map[B](f: B => A): InvStateM[S, B] =
  *         InvStateM((s0, b) => this(s0, f(b)))
  *
  *       // does not commute: short of 2x `A` values
  *       def flatMap[B](g: B => InvStateM[S, A]): InvStateM[S, B] = ???
  *       // InvStateM((s0, b) => { ...g(b)(s0, a0) ... this(s1, a1) ... })
  *
  *       // hack, wrong signature: take a `StateM` to generate an `A`
  *       def mixedUpFlatMap[B](g: B => StateM[S, A]): InvStateM[S, B] =
  *         InvStateM((s0, b) => { val (s1, a) = g(b)(s0) ; this(s1, a) })
  *     }
  *   ```
  *
  * - Experiment: CoMonad wrapping value-taking function `(S, A) => S`
  *
  *   Idea: Maybe, all other arrows have to be reversed as well -> CoMonad?
  *   - 3 methods: counit = extract, cojoin = duplicate, coflatMap = extend
  *   - FAILED, not working for `coflatMap`, unclear semantics
  *   ```
  *     case class StateCoM[S, -A](take: (S, A) => S) {
  *       def apply(s: S, a: A): S = take(s, a)
  *       def apply(sa: (S, A)): S = take.tupled(sa)
  *
  *       // commutes (because Functor == CoFunctor?)
  *       def comap[B](f: B => A): StateCoM[S, B] =
  *         StateCoM((s0, b) => this(s0, f(b)))
  *
  *       // doesn't work: long on 2x `B` values
  *       def coflatMap[B](g: StateCoM[S, A] => B): StateCoM[S, B] = ???
  *       // StateCoM((s0, b) => { ... b1 = g(this) ... }"
  *     }
  *   ```
  *
  * - Why `S => (S, A)` is correct.
  *
  *   - Misguided motivation.  Original, wrapped function `S => (S, A)`
  *     already allows to incorporate + extract values into/from state:
  *        `flatMap` argument    `a => StateM(s0 => (s1, b))`
  *        supports symmetrical  `(s0, a) => (s1, b)`
  *
  *   => a single method/function signature can look deceptive
  *   => commutes: output value becomes the input to the next mapping
  *   => for proper categorical dual: "Store" Comonad
  *
  * @param run function that transforms a state extracting a value from it
  */
case class StateM[S, +A](run: S => (S, A))
    extends (S => (S, A)):  // Function1

  def apply(s: S): (S, A) = run(s)

  def map[B](f: A => B): StateM[S, B] =
    StateM(s0 => { val (s1, a) = this(s0); (s1, f(a)) })  // => (s1, b)
    // = flatMap(a => StateM.unit(f(a)))

  def flatMap[B](g: A => StateM[S, B]): StateM[S, B] =
    StateM(s0 => { val (s1, a) = this(s0); g(a)(s1) })  // => (s2, b)
    //StateM(s0 => { val (s1, a) = run(s0); g(a)(s1) })  // => (s2, b)

  // convenience methods returning just the final state/value
  def runS(s: S): S = run(s)._1
  def runA(s: S): A = run(s)._2

object StateM:
  // case class gives constructor: apply

  def unit[S, A](a: => A): StateM[S, A] =
    StateM((_, a))
    // = get[S].map(_ => a)

  def get[S]: StateM[S, S] =
    StateM(s => (s, s))
    // = StateM.unit( StateM[S, S](s => (s, s)) )(())._2
    // = StateM.unit(()).flatMap(_ => StateM[S, S](s => (s, s)))

  def inspect[S, B](f: S => B): StateM[S, B] =
    StateM(s => (s, f(s)))

  def set[S](s: => S): StateM[S, Unit] =
    StateM(_ => (s, ()))

  def modify[S](f: S => S): StateM[S, Unit] =
    get.flatMap(s => set(f(s)))
    // = for { s <- get; _ <- set(f(s)) } yield ()

  // common FP aliases
  def id[S]: StateM[S, S] = get[S]
  def pure[S, A](a: => A): StateM[S, A] = unit(a)
