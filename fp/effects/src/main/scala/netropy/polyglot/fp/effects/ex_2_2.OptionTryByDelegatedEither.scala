package netropy.polyglot.fp.effects

import util.Either
import scala.util.control.NonFatal

/** Monadic Option delegating to private Either. */
object ex_2_2_Option:

  case class Option[+A] private (private val e: Either[Unit, A]):

    // must add essential methods
    def map[B](f: A => B): Option[B] = Option(e.map(f))
    def flatMap[B](f: A => Option[B]): Option[B] = Option(e.flatMap(f(_).e))

  object Option:

    // c'tors
    val None: Option[Nothing] = Option(Left[Unit, Nothing](()))
    def Some[A](a: A): Option[A] = Option(Right[Nothing, A](a))
    def unit[A](a: A): Option[A] = Some(a)

    def apply[A](a: A): Option[A] =
      if (a == null) None else Some(a)

/** Monadic Try delegating to private Either. */
object ex_2_2_Try:

  case class Try[+A] private (private val e: Either[Throwable, A]):

    // must add essential methods
    def map[B](f: A => B): Try[B] = Try(e.map(f))
    def flatMap[B](f: A => Try[B]): Try[B] = Try(e.flatMap(f(_).e))

  object Try:

    // c'tors
    def Failure[A](t: Throwable): Try[A] = Try[A](Left[Throwable, Nothing](t))
    def Success[A](a: A): Try[A] = Try[A](Right[Nothing, A](a))
    def unit[A](a: A): Try[A] = Success(a)

    def apply[A](a: => A): Try[A] = try {
      Success(a)
    } catch {
      case NonFatal(e) => Failure(e)
    }
