package netropy.polyglot.fp.effects

import util.Either
import scala.util.control.NonFatal

/** Monadic Option opaquely aliasing Either (Scala3 feature). */
object ex_2_3_Option:

  opaque type Option[+A] = Either[Unit, A]

  // must add hidden methods with full signature
  extension[A] (x: Option[A])
    def map[B](f: A => B): Option[B] = x.map(f)
    def flatMap[B](f: A => Option[B]): Option[B] = x.flatMap(f)

  // c'tors
  val None: Option[Nothing] = Left[Unit, Nothing](())
  def Some[A](a: A): Option[A] = Right[Nothing, A](a)
  def unit[A](a: A): Option[A] = Some(a)

  def apply[A](a: A): Option[A] =
    if (a == null) None else Some(a)

/** Monadic Try opaquely aliasing Either (Scala3 feature). */
object ex_2_3_Try:

  opaque type Try[+A] = Either[Throwable, A]

  // must add hidden methods with full signature
  extension[A] (x: Try[A])
    def map[B](f: A => B): Try[B] = x.map(f)
    def flatMap[B](f: A => Try[B]): Try[B] = x.flatMap(f)

  // c'tors
  def Failure[A](t: Throwable): Try[A] = Left[Throwable, Nothing](t)
  def Success[A](a: A): Try[A] = Right[Nothing, A](a)
  def unit[A](a: A): Try[A] = Success(a)

  def apply[A](a: => A): Try[A] = try {
    Success(a)
  } catch {
    case NonFatal(e) => Failure(e)
  }
