package netropy.polyglot.fp.effects

import util.Either
import scala.util.control.NonFatal

/** Monadic Option transparently aliasing Either. */
object ex_2_1_Option:

  type Option[+A] = Either[Unit, A]

  // c'tors
  val None: Option[Nothing] = Left[Unit, Nothing](())
  def Some[A](a: A): Option[A] = Right[Nothing, A](a)
  def unit[A](a: A): Option[A] = Some(a)

  def apply[A](a: A): Option[A] =
    if (a == null) None else Some(a)

/** Monadic Try transparently aliasing Either. */
object ex_2_1_Try:

  type Try[+A] = Either[Throwable, A]

  // c'tors
  def Failure[A](t: Throwable): Try[A] = Left[Throwable, Nothing](t)
  def Success[A](a: A): Try[A] = Right[Nothing, A](a)
  def unit[A](a: A): Try[A] = Success(a)

  def apply[A](a: => A): Try[A] = try {
    Success(a)
  } catch {
    case NonFatal(e) => Failure(e)
  }
