package netropy.polyglot.fp.effects

/** Simple typeclass as required by WriterM to work with logs. */
trait Monoid[A]:
  def empty: A  // identity
  def append(as: A*): A  // associative

/** Provides computations with a shared, read-only environment.
  *
  * TODO: fix, adapt to Writer
  *
  * Available from CATS and other FP libaries:
  * [[https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers]]
  * [[https://typelevel.org/cats/api/cats/data/index.html#Writer[-A,B]=cats.data.package.WriterT[cats.Id,A,B]]]
  * [[https://typelevel.org/cats/api/cats/data/package$$Writer$.html]]
  *
  * Refresher: The Writer Monad
  *
  * - Wraps a function `R => A`.  Parametrizes computations over an `R`.
  *
  *   Type `R` is sometimes denoted as environment `E`.  Regardless of the
  *   connotation, the Writer Monad may just be viewed as a generic wrapper
  *   of unary functions.
  *
  *   Example: a `WriterM[A_withProperties, A]` implementation that injects a
  *   set of properties with an initial `A` into joined computations on `A`.
  *
  * - Features:
  *   - has constructor: `WriterM(r => a)`
  *   - has empty environment ctor: `WriterM.unit(a) = WriterM(_ => a)`
  *     can be used on arbitrary input, e.g., `()`
  *   - has "ask for environment" ctor: `WriterM.get = WriterM(r => r)`
  *   - has functor+monad functions `map`, `flatMap`
  *   - implements `Function1` for convenience, can apply: `a = rm(r)`
  *
  * - Form resource-less computations: `map` the result value
  * TODO: expand
  *     each `map` accumulates in the result value
  *     subject to functor laws: identity, composition
  *
  * - Form resource-ful computations: `flatMap` another writer monad
  * TODO: expand
  *     each `flatMap` accumulates in the result value and can access `R`
  *     subject to monad laws: left/right identity, associativity
  *
  * - Specializations, Generalizations, Variations:
  *
  *   - taking 2 type parameters `WriterM[R, +A]`, technically not a monad;
  *     can be made to meet monad by fixing one type with type projection
  *
  *   - work with updatable state: -> State Monad
  *   - writer monad behaves like a state monad with constant state, i.e.:
  *     `WriterM(r => a) =~= StateM(r => (r, a))` on the `WriterM` API portion
  *     => `WriterM` utilizes the `S => A` part of `StateM(S => (S, A))`
  *
  * @param run unary function
  */
case class WriterM[W: Monoid, A](written: W, value: A):

  def run: (W, A) = (written, value)
  def apply: (W, A) = run

  def map[B](f: A => B): WriterM[W, B] =
    WriterM(written, f(value))  // keep log

  def flatMap[B](g: A => WriterM[W, B]): WriterM[W, B] =
    val gw = g(value)
    val wm = summon[Monoid[W]]
    WriterM(wm.append(written, gw.written), gw.value)  // concatenate logs

  // TODO: unit-test FP aliases

  // common FP aliases
  def listen: WriterM[W, (A, W)] = map(_ => (value, written))

  def tell(w: W): WriterM[W, A] = flatMap(_ => WriterM(w, value))

  def bimap[W1: Monoid, A1](fw: W => W1, fa: A => A1): WriterM[W1, A1] =
    WriterM(fw(written), fa(value))

object WriterM:
  // case class gives constructor: apply

  def unit[W: Monoid, A](a: A): WriterM[W, A] =
    val wm = summon[Monoid[W]]
    WriterM(wm.empty, a)

  // TODO: unit-test FP aliases

  // common FP aliases
  def listen[W: Monoid, A](wm: WriterM[W, A]): WriterM[W, (A, W)] = wm.listen
  def tell[W: Monoid](w: W): WriterM[W, Unit] = WriterM(w, ())
  def value[W: Monoid, A](a: A): WriterM[W, A] = unit(a)
