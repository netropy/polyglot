package netropy.polyglot.fp.effects


/** Provides computations with a shared, read-only environment.
  *
  * TODO: shorten/cleanup
  *
  * Available from CATS and other FP libaries:
  * [[https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers]]
  * [[https://typelevel.org/cats/api/cats/data/index.html#Reader[-A,B]=cats.data.package.ReaderT[cats.Id,A,B]]]
  * [[https://typelevel.org/cats/api/cats/data/package$$Reader$.html]]
  *
  * This implementation, unit- and property tests as exercise.
  *
  * Refresher: The Reader Monad
  *
  * - Wraps a function `R => A`.  Parametrizes computations over an `R`.
  *
  *   Type `R` is sometimes denoted as environment `E`.  Regardless of the
  *   connotation, the Reader Monad may just be viewed as a generic wrapper
  *   of unary functions.
  *
  *   Example: a `ReaderM[A_withProperties, A]` implementation that injects a
  *   set of properties with an initial `A` into joined computations on `A`.
  *
  * - Features:
  *   - has constructor: `ReaderM(r => a)`
  *   - has empty environment ctor: `ReaderM.unit(a) = ReaderM(_ => a)`
  *     can be used on arbitrary input, e.g., `()`
  *   - has "ask for environment" ctor: `ReaderM.get = ReaderM(r => r)`
  *   - has functor+monad functions `map`, `flatMap`
  *   - implements `Function1` for convenience, can apply: `a = rm(r)`
  *
  * - Form resource-less computations: `map` the result value
  * TODO: expand
  *     each `map` accumulates in the result value
  *     subject to functor laws: identity, composition
  *
  * - Form resource-ful computations: `flatMap` another reader monad
  * TODO: expand
  *     each `flatMap` accumulates in the result value and can access `R`
  *     subject to monad laws: left/right identity, associativity
  *
  * - Specializations, Generalizations, Variations:
  *
  *   - taking 2 type parameters `ReaderM[R, +A]`, technically not a monad;
  *     can be made to meet monad by fixing one type with type projection
  *
  *   - work with updatable state: -> State Monad
  *   - reader monad behaves like a state monad with constant state, i.e.:
  *     `ReaderM(r => a) =~= StateM(r => (r, a))` on the `ReaderM` API portion
  *     => `ReaderM` utilizes the `S => A` part of `StateM(S => (S, A))`
  *
  * @param run unary function
  */
case class ReaderM[-R, +A](run: R => A)
    extends (R => A):  // Function1

  def apply(r: R): A = run(r)

  def map[B](f: A => B): ReaderM[R, B] =
    ReaderM(r => f(this(r)))
    // = ReaderM(r => f(run(r)))

  def flatMap[R1 <: R, B](g: A => ReaderM[R1, B]): ReaderM[R1, B] =
    ReaderM(r => g(this(r))(r))
    // = ReaderM(r => g(run(r)).run(r))

object ReaderM:
  // case class gives constructor: apply

  def unit[A](a: => A): ReaderM[Any, A] =
    ReaderM(_ => a)
    // = get[Any].map(_ => a)

  def get[R]: ReaderM[R, R] =
    ReaderM(identity)
    // = ReaderM.unit(ReaderM[R, R](identity))(())
    // = ReaderM.unit(()).flatMap(_ => ReaderM(identity))

  // common FP aliases
  def pure[A](a: => A): ReaderM[Any, A] = unit[A](a)
  def id[R]: ReaderM[R, R] = get[R]
  def ask[R]: ReaderM[R, R] = get[R]
