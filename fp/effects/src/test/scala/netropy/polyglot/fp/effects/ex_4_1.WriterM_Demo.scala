package netropy.polyglot.fp.effects

import munit.{FunSuite, Location}
import scala.collection.mutable.ArrayBuffer


/** Demo: non-tail-recursive version of fibonacci. */
class ex_4_1_1_RecFib
    extends FunSuite:

  // version with 2 recursive calls (to illustrate WriterM composition)

  def fib(n: Int): Int =
    require(n >= 0)
    n match
      case 0 => 0
      case 1 => 1
      case i => fib(i - 2) + fib(i - 1)

  // unit-test a few calls

  test("demo fibonacci") {

    assertEquals(fib(0), 0)
    assertEquals(fib(1), 1)
    assertEquals(fib(2), 1)
    assertEquals(fib(3), 2)
  }


/** Demo: log recursive fibonacci passing an global, mutable log. */
class ex_4_1_2_RecFib_GlobalMutableLog
    extends FunSuite:

  // for logging: mutable type with efficient 'append'

  val log = ArrayBuffer.empty[String]             // NOT THREAD-SAFE

  // keep computation, pass global & mutable log, append to log

  def fib(n: Int): Int =                          // nice: unchanged signature
    require(n >= 0)
    val r = n match
      case 0 => 0
      case 1 => 1
      case i => fib(i - 2) + fib(i - 1)           // nice: unchanged call site
    log += s"fib $n: $r"                          // append log
    r

   // unit-test a few calls, reset log, check result and log

  test("demo GlobalLog") {

    // must run these tests sequentially, must clear log before each test

    log.clearAndShrink()                          // error-prone: must reset
    assertEquals(fib(0), 0)                       // nice: unchanged call site
    assertEquals(log,
      ArrayBuffer("fib 0: 0"))

    log.clearAndShrink()                          // ...
    assertEquals(fib(1), 1)                       // ...
    assertEquals(log,
      ArrayBuffer("fib 1: 1"))

    log.clearAndShrink()                          // ...
    assertEquals(fib(2), 1)                       // ...
    assertEquals(log,
      ArrayBuffer("fib 0: 0", "fib 1: 1", "fib 2: 1"))

    log.clearAndShrink()                          // ...
    assertEquals(fib(3), 2)                       // ...
    assertEquals(log,
      ArrayBuffer("fib 1: 1", "fib 0: 0", "fib 1: 1", "fib 2: 1", "fib 3: 2"))
  }


/** Demo: log recursive fibonacci passing an implicit, mutable log. */
class ex_4_1_3_RecFib_ImplicitMutableLog
    extends FunSuite:

  // for logging: mutable type with efficient 'append'

  type Log = ArrayBuffer[String]

  // keep computation, pass implicit & mutable log, append to log

  def fib(n: Int)(using log: Log): Int =          // small change to signature
    require(n >= 0)
    val r = n match
      case 0 => 0
      case 1 => 1
      case i => fib(i - 2) + fib(i - 1)           // nice: unchanged call site
    log += s"fib $n: $r"                          // append log
    r

   // unit-test a few calls, reset log, check result and log

  test("demo ImplicitLog") {

    // option #1: use a single log instance; must then clear before each test
    // option #2: create new log for each test; must then memoize for check
    given l: Log = ArrayBuffer.empty              // OK: not thread-safe

    l.clearAndShrink()                            // error-prone: must reset
    assertEquals(fib(0), 0)                       // nice: unchanged call site
    assertEquals(l,
      ArrayBuffer("fib 0: 0"))

    l.clearAndShrink()                            // ...
    assertEquals(fib(1), 1)                       // ...
    assertEquals(l,
      ArrayBuffer("fib 1: 1"))

    l.clearAndShrink()                            // ...
    assertEquals(fib(2), 1)                       // ...
    assertEquals(l,
      ArrayBuffer("fib 0: 0", "fib 1: 1", "fib 2: 1"))

    l.clearAndShrink()                            // ...
    assertEquals(fib(3), 2)                       // ...
    assertEquals(l,
      ArrayBuffer("fib 1: 1", "fib 0: 0", "fib 1: 1", "fib 2: 1", "fib 3: 2"))
  }


/** Demo: log recursive fibonacci, changed to return explicit log. */
class ex_4_1_4_RecFib_ExplicitImmutableLog
    extends FunSuite:

   // for logging: immutable type with efficient 'append'

  type Log = Vector[String]

   // modify non-tail-recursive computation to return (result, log)

  def fib(n: Int): (Int, Log) =                   // changed signature
    require(n >= 0)
    val (r, l) = n match
      case 0 => (0, Vector.empty)
      case 1 => (1, Vector.empty)
      case i =>
        val (r2, l2) = fib(i - 2)                 // changed call site
        val (r1, l1) = fib(i - 1)                 // changed call site
        (r2 + r1, l2 ++ l1)
    (r, l :+ s"fib $n: $r")                       // changed return, append log

  // unit-test a few calls, check (result, log)

  test("demo ExplicitLog") {

    val (f0, l0) = fib(0)                         // changed call site
    assertEquals(f0, 0)
    assertEquals(l0,
      Vector("fib 0: 0"))

    val (f1, l1) = fib(1)                         // ...
    assertEquals(f1, 1)
    assertEquals(l1,
      Vector("fib 1: 1"))

    val (f2, l2) = fib(2)                         // ...
    assertEquals(f2, 1)
    assertEquals(l2,
      l0 ++ l1 ++ Vector("fib 2: 1"))             // nice: compose log

    val (f3, l3) = fib(3)                         // ...
    assertEquals(f3, 2)
    assertEquals(l3,
      l1 ++ l2 ++ Vector("fib 3: 2"))             // ...
  }


/** Demo: log recursive fibonacci using WriterM. */
class ex_4_1_5_RecFib_WriterM
    extends FunSuite:

   // for logging: immutable type with efficient 'append' + given as a monoid

  type Log = Vector[String]

  object MonoidV extends Monoid[Log]:
    def empty: Log = Vector.empty[String]
    def append(as: Log*): Log = as.fold(empty)(_ ++ _)

  given Monoid[Log] = MonoidV  // otherwise, pass '(using MonoidV)'

  // compose computation as a series of flatMap/map over WriterM

  def fib(n: Int): WriterM[Log, Int] =            // changed signature
    require(n >= 0)
    for                                           // need a for-yield
      r <- n match
        case 0 => WriterM.unit(0)                 // empty log
        case 1 => WriterM.unit(1)                 // empty log
        case i =>
          for                                     // need a nested for-yield
            r2 <- fib(i - 2)                      // non-empty log
            r1 <- fib(i - 1)                      // appends log
          yield r2 + r1                           // passes log
      _ <- WriterM.tell(Vector(s"fib $n: $r"))    // appends log
    yield r                                       // passes log

  // unit-test a few calls, check (log, result)

  test("demo WriterM") {

    val (l0, f0) = fib(0).run                     // changed call site
    assertEquals(f0, 0)
    assertEquals(l0,
      Vector("fib 0: 0"))

    val (l1, f1) = fib(1).run                     // ...
    assertEquals(f1, 1)
    assertEquals(l1,
      Vector("fib 1: 1"))

    val (l2, f2) = fib(2).run                     // ...
    assertEquals(f2, 1)
    assertEquals(l2,
      l0 ++ l1 ++ Vector("fib 2: 1"))             // nice: compose log

    val (l3, f3) = fib(3).run                     // ...
    assertEquals(f3, 2)
    assertEquals(l3,
      l1 ++ l2 ++ Vector("fib 3: 2"))             // ...
  }
