package netropy.polyglot.fp.effects

import org.scalacheck.{Gen, Arbitrary}

/** Property-checks the Try monad type. */
trait ex_1_2_Try_Tests[M[+_]]
    extends ex_1_1_Monad_Tests[M]:

  // ----------------------------------------------------------------------
  // Try-specifics under test (no need to introduce a typeclass)
  // ----------------------------------------------------------------------

  type Try[A] = M[A]
  def Failure[A](t: Throwable): Try[A]
  def Success[A](a: A): Try[A]

  // ----------------------------------------------------------------------
  // ScalaCheck generators (summoning the arbitrary generator)
  // ----------------------------------------------------------------------

  // ensure: types with value equality (as assumed by functor/monad laws)
  type A = Boolean
  type B = Short
  type C = Int

  // ensure: non-singleton Failure instances with value equality
  val t = new Throwable()
  def fail: Try[Nothing] = Failure(t)

  // ensure: generators genM, genMF, genMG cover all instances of M[_]
  def genA: Gen[A] = Arbitrary.arbitrary[A]
  def genM: Gen[M[A]] =
    Gen.frequency(2 -> genA.map(unit), 1 -> fail)
  def genF: Gen[A => B] = Arbitrary.arbitrary[A => B]
  def genG: Gen[B => C] = Arbitrary.arbitrary[B => C]
  def genMF: Gen[A => M[B]] =
    Gen.frequency(2 ->  genF.map(_.andThen(unit)), 1 -> ((_: A) => fail))
  def genMG: Gen[B => M[C]] =
    Gen.frequency(2 ->  genG.map(_.andThen(unit)), 1 -> ((_: B) => fail))

  // ----------------------------------------------------------------------
  // Unit tests with fixed test data (came first & easier to debug)
  // ----------------------------------------------------------------------

  val a: Boolean = true
  val e: Try[Boolean] = Failure(t)
  val s: Try[Boolean] = Success(a)
  val f: Boolean => Int = a => s"$a".length
  val g: Int => String = b => s"$b"
  val mf: Boolean => Try[Int] = a => Success(f(a))
  val mg: Int => Try[String] = b => Success(g(b))

  test("test M[_] value equality, c'tors") {
    assert(e == Failure(t))
    assert(s != Failure(t))
    assert(s == Success(a))
    assert(s == unit(a))
  }

  test("test map identity") {
    testMapIdentity(e)
    testMapIdentity(s)
  }

  test("test map composition") {
    testMapComposition(e, f, g)
    testMapComposition(s, f, g)
  }

  test("test map == flatMap + unit") {
    testMapFlatMap(e, f)
    testMapFlatMap(s, f)
  }

  test("test flatMap left identity") {
    testFlatMapLeftIdentity(a, mf)
  }

  test("test flatMap right identity") {
    testFlatMapRightIdentity(e)
    testFlatMapRightIdentity(s)
  }

  test("test flatMap associativity") {
    testFlatMapAssociativity(s, mf, mg)
    testFlatMapAssociativity(e, mf, mg)
  }
