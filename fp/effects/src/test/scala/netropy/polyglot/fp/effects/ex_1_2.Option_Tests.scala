package netropy.polyglot.fp.effects

import org.scalacheck.{Gen, Arbitrary}

/** Property-checks the Option monad type. */
trait ex_1_2_Option_Tests[M[+_]]
    extends ex_1_1_Monad_Tests[M]:

  // ----------------------------------------------------------------------
  // Option-specifics under test (no need to introduce a typeclass)
  // ----------------------------------------------------------------------

  type Option[A] = M[A]
  def None: Option[Nothing]
  def Some[A](a: A): Option[A]

  // ----------------------------------------------------------------------
  // ScalaCheck generators (summoning the arbitrary generator)
  // ----------------------------------------------------------------------

  // ensure: types with value equality (as assumed by functor/monad laws)
  type A = Boolean
  type B = Short
  type C = Int

  // ensure: generators genM, genMF, genMG cover all instances of M[_]
  def genA: Gen[A] = Arbitrary.arbitrary[A]
  def genM: Gen[M[A]] =
    Gen.frequency(2 -> genA.map(Some), 1 -> None)
  def genF: Gen[A => B] = Arbitrary.arbitrary[A => B]
  def genG: Gen[B => C] = Arbitrary.arbitrary[B => C]
  def genMF: Gen[A => M[B]] =
    Gen.frequency(2 ->  genF.map(_.andThen(Some)), 1 -> ((_: A) => None))
  def genMG: Gen[B => M[C]] =
    Gen.frequency(2 ->  genG.map(_.andThen(Some)), 1 -> ((_: B) => None))

  // ----------------------------------------------------------------------
  // Unit tests with fixed test data (as a start, easier to debug)
  // ----------------------------------------------------------------------

  val a: Boolean = true
  val n: Option[Boolean] = None
  val s: Option[Boolean] = Some(a)
  val f: Boolean => Int = a => s"$a".length
  val g: Int => String = b => s"$b"
  val mf: Boolean => Option[Int] = a => Some(f(a))
  val mg: Int => Option[String] = b => Some(g(b))

  test("test M[_] value equality, c'tors") {
    assert(n == None)
    assert(s != None)
    assert(s == Some(a))
    assert(s == unit(a))
  }

  test("test map identity") {
    testMapIdentity(n)
    testMapIdentity(s)
  }

  test("test map composition") {
    testMapComposition(n, f, g)
    testMapComposition(s, f, g)
  }

  test("test map == flatMap + unit") {
    testMapFlatMap(n, mf)
    testMapFlatMap(s, mf)
  }

  test("test flatMap left identity") {
    testFlatMapLeftIdentity(a, mf)
  }

  test("test flatMap right identity") {
    testFlatMapRightIdentity(n)
    testFlatMapRightIdentity(s)
  }

  test("test flatMap associativity") {
    testFlatMapAssociativity(s, mf, mg)
    testFlatMapAssociativity(n, mf, mg)
  }
