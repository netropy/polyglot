package netropy.polyglot.fp.effects

import munit.{Location, ScalaCheckSuite}
import org.scalacheck.{Gen, Prop}
import Prop.forAll

/** Property-checks a 2-ary monad type on abstract methods unit, map, flatMap.
  *
  * This trait provides property checks of the functor and monad laws (i.e.,
  * identity, composition, associativity), which are formulated over
  * - a Monad type taking 2 type arguments:             M[_, _]
  * - abstract, functor/monad-defining methods:         unit, map, flatMap
  * - arbitrary but fixed, abstract test data types:    A, B, ...
  *
  * Discussion:
  *
  * Note that we cannot re-use the 1-ary type monad property checks here
  * since the 2-ary monads here cannot be modeled as tuple type M[(_, _)]:
  * unit/map/flatMap work selectively on the component types (-> signatures).
  *
  * Another difference is that function-wrapping monads (e.g., Reader, State)
  * cannot be compared by value equality.   Hence, no property check for value
  * equality and hash, here.
  *
  * Instead, the function-wrapping monads are compared at input data points:
  *   forAll { (a: A) => assert(m.run(a) == n.run(a)) }
  * A new method comparing monads abstracts from this detail + difference:
  *   def assertEq[B](m: M[A, B], n: M[A, B]) ...
  *
  * As in ex_1_1, this unit-test suite follows an object-oriented design with
  * - this base trait providing the general tests of functor and monad laws
  * - derived traits adding monad-specific tests (e.g., for Writer, Reader)
  * - derived test classes delegating to a specific monad implementation.
  *
  * For more details, see doc comments in ex_1_1.MonadTests.scala
  */
trait ex_3_1_Monad2_Tests[M[_, _]]
    extends ScalaCheckSuite:

  // ----------------------------------------------------------------------
  // Abstract methods under test
  // ----------------------------------------------------------------------

  /*
   * Method unit[B] vs unit[A,B]
   *
   * `unit` lifts a value of B into the monad type M[A,B] -- but some monads
   * restrict type A, for example, to Monoids (associativity + identity):
   *   def unit[A, B](b: => B): M[A, B]            // Reader, State monad
   *   def unit[A: Monoid, B](b: => B): M[A, B]    // Writer monad
   *
   * To keep a single set of property checks covering either type signature:
   *
   * Since A in `M[A,B]` does not vary across property checks, an easy ad-hoc
   * solution is to drop A as a type parameter from `unit` and to defer to
   * the abstract test data type `this.A` below.  The derived, monad-specific
   * tests are then free to restrict and define A as they need.
   */
  def unit[B](b: => B): M[A, B]

  def map[A, B, C](m: M[A, B])(f: B => C): M[A, C]
  def flatMap[A, B, C](m: M[A, B])(mf: B => M[A, C]): M[A, C]

  // ----------------------------------------------------------------------
  // Abstract test data types and generators
  // ----------------------------------------------------------------------

  /*
   * Fixed but arbitrary types for use in functor and monad laws.
   *
   * Subclasses must ensure value equality as assumed by laws, for example:
   * Int, String, List etc; not: AnyRef/Object, Throwable
   */
  type A
  type B
  type C
  type D

  // generators for property checks
  def genB: Gen[B]
  def genF: Gen[B => C]
  def genG: Gen[C => D]
  def genM: Gen[M[A, B]]
  def genMF: Gen[B => M[A, C]]
  def genMG: Gen[C => M[A, D]]

  // compares two monads, since function-wrapping monads lack value equality
  def assertEq[B](m: M[A, B], n: M[A, B])(using loc: Location): Prop

  // ----------------------------------------------------------------------
  // Functor + Monad laws (over abstract test data types)
  // ----------------------------------------------------------------------

  def testMapIdentity
    (m: M[A, B])
    (using loc: Location): Prop =
    assertEq(
      map(m)(identity),
      m)

  def testMapComposition
    (m: M[A, B], f: B => C, g: C => D)
    (using loc: Location): Prop =
    assertEq(
      map(map(m)(f))(g),
      map(m)(f.andThen(g)))

  def testMapFlatMap  // possibly redundant
    (m: M[A, B], f: B => C)
    (using loc: Location): Prop =
    assertEq(
      map(m)(f),
      flatMap(m)(b => unit(f(b))))

  def testFlatMapLeftIdentity
    (b: B, mf: B => M[A, C])
    (using loc: Location): Prop =
    assertEq(
      flatMap(unit(b))(mf),
      mf(b))

  def testFlatMapRightIdentity
    (m: M[A, B])
    (using loc: Location): Prop =
    assertEq(
      flatMap(m)(unit),
      m)

  def testFlatMapAssociativity
    (m: M[A, B], mf: B => M[A, C], mg: C => M[A, D])
    (using loc: Location): Prop =
    assertEq(
      flatMap(flatMap(m)(mf))(mg),
      flatMap(m)(a => flatMap(mf(a))(mg)))

  // ----------------------------------------------------------------------
  // Functor + Monad property checks
  // ----------------------------------------------------------------------

  test("check M[_, _] value equality") {
    forAll(genB, genB) { (b0, b1) =>
      assertEq(unit(b0), unit(b0))
      if (b0 == b1) assertEq(unit(b0), unit(b1))
    }
  }

  test("check map identity") {
    forAll(genM) { m =>
      testMapIdentity(m)
    }
  }

  test("check map composition") {
    forAll(genM, genF, genG) { (m, f, g) =>
      testMapComposition(m, f, g)
    }
  }

  test("check map == flatMap + unit") {
    forAll(genM, genF) { (m, f) =>
      testMapFlatMap(m, f)
    }
  }

  test("check flatMap left identity") {
    forAll(genB, genMF) { (b, mf) =>
      testFlatMapLeftIdentity(b, mf)
    }
  }

  test("check flatMap right identity") {
    forAll(genM) { m =>
      testFlatMapRightIdentity(m)
    }
  }

  test("check flatMap associativity") {
    forAll(genM, genMF, genMG) { (m, mf, mg) =>
      testFlatMapAssociativity(m, mf, mg)
    }
  }
