package netropy.polyglot.fp.effects

import org.scalacheck.Prop.forAll

/** Unit-tests the ReaderM implementation. */
class ex_3_3_ReaderM_Tests
    extends ex_3_3_Reader_Tests[ReaderM]:

  // methods under test
  def from[A, B]
    (f: A => B): ReaderM[A, B] = ReaderM(f)  // c'tor
  def run[A, B]
    (m: ReaderM[A, B])(a: A): B = m.run(a)
  // TODO: def unit[A, B]
  def unit[B]
    (b: => B): ReaderM[A, B] = ReaderM.unit(b)
  def map[A, B, C]
    (m: ReaderM[A, B])(f: B => C): ReaderM[A, C] = m.map(f)
  def flatMap[A, B, C]
    (m: ReaderM[A, B])(mf: B => ReaderM[A, C]): ReaderM[A, C] = m.flatMap(mf)

  test("check run == apply == (...)") {
    forAll(genE) { e =>
      forAll(genA) { a =>
        val rm = from(e)
        assertEquals(rm.run(a), e(a))
        assertEquals(rm.apply(a), e(a))
        assertEquals(rm(a), e(a))
      }
    }
  }

  test("check get, unit") {
    forAll(genA) { a =>
      assertEquals(ReaderM.get(a), a)
      assertEquals(ReaderM.unit(())(a), ())
      assertEquals(ReaderM.get(a), ReaderM.unit(a)(()))
    }
  }
