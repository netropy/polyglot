package netropy.polyglot.fp.effects

import munit.{Location, ScalaCheckSuite}
import org.scalacheck.Gen
import org.scalacheck.Prop.forAll

/** Property-checks a 1-ary monad type on methods unit, map, flatMap.
  *
  * This trait provides property checks of the functor and monad laws (i.e.,
  * identity, composition, associativity), which are formulated over
  * - a Monad type taking 1 type argument:              M[_]
  * - abstract, functor/monad-defining methods:         unit, map, flatMap
  * - arbitrary but fixed, abstract test data types:    A, B, ...
  *
  * Discussion:
  *
  * This unit-test suite follows an object-oriented design with
  * - this base trait providing the general tests of functor and monad laws
  * - derived traits adding monad-specific tests (e.g., for Option, Try)
  * - derived test classes delegating to a specific monad implementation.
  *
  * This approach appeared less complicated and more flexible for testing the
  * self-written (1-ary, 2-ary) monads, here, compared to a functional design
  * using typeclasses with extension methods.
  *
  * Note that professional FP libs (e.g., CATS) model/implement many effects
  * in a more general way and have their own law-checking frameworks, anyway.
  */
trait ex_1_1_Monad_Tests[M[_]]
    extends ScalaCheckSuite:

  // ----------------------------------------------------------------------
  // Abstract methods under test
  // ----------------------------------------------------------------------

  def unit[A](a: A): M[A]
  def map[A, B](m: M[A])(f: A => B): M[B]
  def flatMap[A, B](m: M[A])(mf: A => M[B]): M[B]

  // ----------------------------------------------------------------------
  // Abstract test data types and generators
  // ----------------------------------------------------------------------

  /*
   * Fixed but arbitrary types for use in property checks
   *
   * Subclasses must ensure value equality as assumed by laws, for example:
   * Int, String, List etc; not: AnyRef/Object, Throwable
   */
  type A
  type B
  type C

  /*
   * Generators for property checks
   *
   * Subclasses must ensure that generators genM* cover all values of M[_],
   * for example: Int => {Some[Int], None}; Int => {Success[Int], Failure}
   */
  def genA: Gen[A]
  def genM: Gen[M[A]]
  def genF: Gen[A => B]
  def genG: Gen[B => C]
  def genMF: Gen[A => M[B]]
  def genMG: Gen[B => M[C]]

  // ----------------------------------------------------------------------
  // Functor + Monad laws (type-parameterized for added use in unit tests)
  // ----------------------------------------------------------------------

  def testMapIdentity[A]
    (m: M[A])(using loc: Location): Unit =
    assertEquals(
      map(m)(identity),
      m)

  def testMapComposition[A, B, C]
    (m: M[A], f: A => B, g: B => C)(using loc: Location): Unit =
    assertEquals(
      map(map(m)(f))(g),
      map(m)(f.andThen(g)))

  def testMapFlatMap[A, B]  // possibly redundant
    (m: M[A], f: A => B)(using loc: Location): Unit =
    assertEquals(
      map(m)(f),
      flatMap(m)(a => unit(f(a))))

  def testFlatMapLeftIdentity[A, B]
    (a: A, mf: A => M[B])(using loc: Location): Unit =
    assertEquals(
      flatMap(unit(a))(mf),
      mf(a))

  def testFlatMapRightIdentity[A]
    (m: M[A])(using loc: Location): Unit =
    assertEquals(flatMap(m)(unit(_)), m)

  def testFlatMapAssociativity[A, B, C]
    (m: M[A], mf: A => M[B], mg: B => M[C])(using loc: Location): Unit =
    assertEquals(
      flatMap(flatMap(m)(mf))(mg),
      flatMap(m)((a: A) => flatMap(mf(a))(mg)))

  // ----------------------------------------------------------------------
  // Functor + Monad property checks
  // ----------------------------------------------------------------------

  test("check M[_] value equality, hash") {
    forAll(genA, genA) { (x, y) =>
      assertEquals(unit(x), unit(x))
      assertEquals(unit(y), unit(y))
      assert(x == y ^ unit(x) != unit(y))
      assert(unit(x) != unit(y) || unit(x).## == unit(y).##)
    }
  }

  test("check map identity") {
    forAll(genM) { m =>
      testMapIdentity(m)
    }
  }

  test("check map composition") {
    forAll(genM, genF, genG) { (m, f, g) =>
      testMapComposition(m, f, g)
    }
  }

  test("check map == flatMap + unit") {
    forAll(genM, genF) { (m, f) =>
      testMapFlatMap(m, f)
    }
  }

  test("check flatMap left identity") {
    forAll(genA, genMF) { (a, mf) =>
      testFlatMapLeftIdentity(a, mf)
    }
  }

  test("check flatMap right identity") {
    forAll(genM) { m =>
      testFlatMapRightIdentity(m)
    }
  }

  test("check flatMap associativity") {
    forAll(genM, genMF, genMG) { (m, mf, mg) =>
      testFlatMapAssociativity(m, mf, mg)
    }
  }
