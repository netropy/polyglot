package netropy.polyglot.fp.effects

import ex_2_3_Option.Option
import ex_2_3_Try.Try

/** Property-checks the opaque Option-Either implementation. */
class ex_2_3_Option_Tests
    extends ex_1_2_Option_Tests[Option]:

  def map[A, B](m: Option[A])(f: A => B): Option[B] = m.map(f)
  def flatMap[A, B](m: Option[A])(mf: A => Option[B]): Option[B] = m.flatMap(mf)

  // c'tors: qualify to prevent recursion
  def unit[A](a: A): Option[A] = ex_2_3_Option.unit(a)
  def None: Option[Nothing] = ex_2_3_Option.None
  def Some[A](a: A): Option[A] = ex_2_3_Option.Some(a)

  test("test apply") {
    assert(None == ex_2_3_Option(null))
    assert(Some(true) == ex_2_3_Option(true))
  }

  def compile_test = {
    // works: flags as compile error: type Option[+A] is opaque
    // val o: Option[Int] = Right[Nothing, Int](0)  // required: Option[Int]
  }

/** Property-checks the opaque Try-Either implementation. */
class ex_2_3_Try_Tests
    extends ex_1_2_Try_Tests[Try]:

  def map[A, B](m: Try[A])(f: A => B): Try[B] = m.map(f)
  def flatMap[A, B](m: Try[A])(mf: A => Try[B]): Try[B] = m.flatMap(mf)

  // c'tors: qualify to prevent recursion
  def unit[A](a: A): Try[A] = ex_2_3_Try.unit(a)
  def Failure[A](t: Throwable): Try[A] = ex_2_3_Try.Failure(t)
  def Success[A](a: A): Try[A] = ex_2_3_Try.Success(a)

  test("test apply") {
    val t = new Throwable()  // single instance for value equality
    assert(Failure(t) == ex_2_3_Try(throw t))
    assert(Success(true) == ex_2_3_Try(true))
  }

  def compile_test = {
    // works: flags as compile error: type Try[+A] is opaque
    // val o: Try[Int] = Right[Nothing, Int](0)  // required: Try[Int]
  }
