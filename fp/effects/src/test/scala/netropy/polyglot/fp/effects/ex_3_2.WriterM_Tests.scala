package netropy.polyglot.fp.effects

import org.scalacheck.Prop.forAll

// TODO: used where?
object MonoidS extends Monoid[String]:
  def empty: String = ""
  def append(as: String*): String = as.fold(empty)(_ + _)


/** Unit-tests the WriterM implementation. */
class ex_3_2_WriterM_Tests
    extends ex_3_2_Writer_Tests[WriterM]:

  // methods under test
  // TODO: explain //def unit[A, B](b: => B): WriterM[A, B] = WriterM.unit(b)
  def from[A: Monoid, B]
    (w: (A, B)): WriterM[A, B] = WriterM(w._1, w._2)
  def run[A, B]
    (m: WriterM[A, B]): (A, B) = m.run
  def unit[B]
    (b: => B): WriterM[A, B] = WriterM.unit(b)
  def map[A, B, C]
    (m: WriterM[A, B])(f: B => C): WriterM[A, C] = m.map(f)
  def flatMap[A, B, C]
    (m: WriterM[A, B])(mf: B => WriterM[A, C]): WriterM[A, C] = m.flatMap(mf)

  test("check run == apply") {
    forAll(genE) { e =>
      val rm = from(e)
      assertEquals(rm.run, e)
      assertEquals(rm.apply, e)
    }
  }

/*
  test("check get, unit") {
    forAll(genA) { a =>
      assertEquals(WriterM.get, a)
      assertEquals(WriterM.unit(())(a), ())
      assertEquals(WriterM.get, WriterM.unit(a)(()))
    }
  }
 */
