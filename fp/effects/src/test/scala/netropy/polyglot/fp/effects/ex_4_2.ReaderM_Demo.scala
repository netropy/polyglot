package netropy.polyglot.fp.effects

import munit.{FunSuite, Location}
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/*
 * Demo #1: Dependency pass-through (vs injection)
 * def: A fixed-point number type taking an external scale factor.
 * use: Must pass through the scale factor needed at each conversion.
 * pro: modular, compile-safe
 * con: tedious, clutter, poor readability
 */

object ex_4_2_1_Fixp:                            // singleton, no parameters

  // internal representation: only stores the value, not the scale factor
  opaque type Fixp = Long

  // explicit conversions taking a scale factor -- assumed constant
  extension (x: Long) def toFixp(scale: Int): Fixp = x * scale
  extension (x: Fixp) def toLong(scale: Int): Long = x / scale

  // example operators
  extension (x: Fixp)
    def +(y: Fixp): Fixp = x + y
    def -(y: Fixp): Fixp = x - y
    def ==(y: Fixp): Boolean = x == y


class ex_4_2_1_Fixp_Test
    extends FunSuite:

  import ex_4_2_1_Fixp._

  val scale: Int = Random.between(10, 101)       // explicit runtime constant

  test("demo Fixp, passing explicit scale factor") {

    //val x: Fixp = 1L                           // no implicit conversions
    val x: Fixp = 1.toFixp(scale)                // pass scale everywhere...
    assertEquals(
      1.toFixp(scale).toLong(scale),
      1L)
    assertEquals(
      1.toFixp(scale),
      1.toFixp(scale))
    assertEquals(
      (1.toFixp(scale) + 0.toFixp(scale)).toLong(scale),
      1L)
    assertEquals(
      (1.toFixp(scale) - 0.toFixp(scale)).toLong(scale),
      1L)                                        // ...passed 11x, here
  }


/*
 * Demo #2: Dependency injection by term inference
 * def: A fixed-point number type taking an implicit scale factor.
 * use: Declare scale factor a 'given', no need to pass as argument.
 * pro: clean, good readbility, compile-safe, searchable 'given'
 * con: implicits (= term inference) unfamiliar to many OO programmers
 */

object ex_4_2_2_Fixp:                            // singleton, no parameters

  // internal representation: only stores the value, not the scale factor
  opaque type Fixp = Long

  // explicit conversions taking implicit scale factor -- assumed constant
  extension (x: Long) def toFixp(using scale: Int): Fixp = x * scale
  extension (x: Fixp) def toLong(using scale: Int): Long = x / scale

  // example operators
  extension (x: Fixp)
    def +(y: Fixp): Fixp = x + y
    def -(y: Fixp): Fixp = x - y
    def ==(y: Fixp): Boolean = x == y


class ex_4_2_2_Fixp_Test
    extends FunSuite:

  import ex_4_2_2_Fixp._

  given scale: Int = Random.between(10, 101)     // canonical runtime constant

  test("demo Fixp, using a given scale factor") {

    //val x: Fixp = 1L                           // no implicit conversions
    val x: Fixp = 1.toFixp                       // passing scale implicitly...
    assertEquals(1.toFixp.toLong, 1L)
    assertEquals(1.toFixp, 1.toFixp)
    assertEquals((1.toFixp + 0.toFixp).toLong, 1L)
    assertEquals((1.toFixp - 0.toFixp).toLong, 1L)
                                                // ...clean & compile-safe

    assertEquals(                               // can pass scale explicitly...
      1.toFixp(using scale).toLong(using scale),
      1L)                                       // ...if desired
  }


/*
 * Demo #3: Dependency injection by path-dependency
 * def: An inner fixed-point number type using a path-dependent scale factor.
 * use: Use an 'imported' scale factor, no arg to pass, clean & compile-safe.
 * pro: clean, good readbility, compile-safe, no use of implicits
 * con: path-dependent types/values/ops unfamiliar to many OO programmers
 */

case class ex_4_2_3_Fixp(val scale: Int):        // now a parameterized type!

  // internal representation: only stores the value, not the scale factor
  opaque type Fixp = Long

  // explicit conversions using outer scale factor parameter
  extension (x: Long) def toFixp: Fixp = x * scale
  extension (x: Fixp) def toLong: Long = x / scale

  // example operators
  extension (x: Fixp)
    def +(y: Fixp): Fixp = x + y
    def -(y: Fixp): Fixp = x - y
    def ==(y: Fixp): Boolean = x == y


class ex_4_2_3_Fixp_Test
    extends FunSuite:

  import ex_4_2_3_Fixp._

  val scale: Int = Random.between(10, 101)       // canonical runtime constant
  val fpn = ex_4_2_3_Fixp(scale)                 // instantiate
  import fpn._                                   // import path-dependent defs

  test("demo Fixp, using an outer scale factor") {

    //val x: Fixp = 1L  // no implicit conversion by design
    val x: Fixp = 1.toFixp
    assertEquals(1.toFixp.toLong, 1L)
    assertEquals(1.toFixp, 1.toFixp)
    assertEquals((1.toFixp + 0.toFixp).toLong, 1L)
    assertEquals((1.toFixp - 0.toFixp).toLong, 1L)
  }


/*
 * Demo #4: Dependency injection by Reader monad
 * def: A fixed-point number type with Reader monads hiding the scale factor.
 * use: Compose calculations in a for-yield, then evaluate with scale factor.
 * pro: compile-safe, no use of implicits
 * con: poor readbility
 */

object ex_4_2_4_Fixp:                            // singleton, no parameters

  // internal representation: only stores the value, not the scale factor
  opaque type Fixp = Long

  // explicit conversions wrapped in a Reader monad
  val toFixp: ReaderM[Int, Long => Fixp] = ReaderM(scale => l => l * scale)
  val toLong: ReaderM[Int, Fixp => Long] = ReaderM(scale => x => x / scale)

  // example operators
  extension (x: Fixp)
    def +(y: Fixp): Fixp = x + y
    def -(y: Fixp): Fixp = x - y
    def ==(y: Fixp): Boolean = x == y


class ex_4_2_4_Fixp_Test
    extends FunSuite:

  import ex_4_2_4_Fixp._

  val scale: Int = 100                           // TODO:...

  test("demo Fixp, using Reader monads: 1.toFixp") {

    //val x: Fixp = 1L                           // no implicit conversion here
    val x: Fixp =
      (for l2x <- toFixp yield l2x(1L))(scale)
  }

  test("demo Fixp, using Reader monads: _.toFixp.toLong") {

    val l2x2l: Long => ReaderM[Int, Long] =
      l =>
      for                                        // no use of scale...
        l2x <- toFixp
        x2l <- toLong
      yield x2l(l2x(l))                          // ... within for-yield
    assertEquals(l2x2l(1L)(scale), 1L)           // run reader inside assert
  }

  test("demo Fixp, using Reader monads: 1.toFixp == 1.toFixp") {

    val checkFixpEquals: ReaderM[Int, Unit] =
      for l2x <- toFixp
      yield assertEquals(l2x(1L), l2x(1L))
    checkFixpEquals(scale)                       // run assert inside reader
  }

  test("demo Fixp, using Reader monads: (1.toFixp + 0.toFixp).toLong") {

    val checkFixpSum: ReaderM[Int, Unit] =
      for
        l2x <- toFixp
        x2l <- toLong
        sum = l2x(1L) + l2x(0L)                  // value def inside for
      yield assertEquals(x2l(sum), 1L)
    checkFixpSum(scale)                          // run assert inside reader
  }

  test("demo Fixp, using Reader monads: (1.toFixp - 0.toFixp).toLong") {

    val checkFixpDiff: ReaderM[Int, Unit] =
      for
        l2x <- toFixp
        x2l <- toLong
      yield
        val diff = l2x(1L) - l2x(0L)             // value def inside yield
        assertEquals(x2l(diff), 1L)
    checkFixpDiff(scale)                         // run assert inside reader
  }
