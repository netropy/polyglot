package netropy.polyglot.fp.effects

import munit.Location
import org.scalacheck.{Gen, Arbitrary, Prop}
import Prop.forAll

trait ex_3_2_Writer_Tests[M[_, _]]
    extends ex_3_1_Monad2_Tests[M]:

  // ----------------------------------------------------------------------
  // Writer-methods under test (no need to introduce a typeclass)
  // ----------------------------------------------------------------------

  type Writer[A, B] = M[A, B]
  def from[A: Monoid, B](w: (A, B)): Writer[A, B]  // c'tor
  def run[A, B](m: Writer[A, B]): (A, B)  // get

  // ----------------------------------------------------------------------
  // ScalaCheck generators (summoning the arbitrary generator)
  // ----------------------------------------------------------------------

  // ensure: types with value equality (as assumed by functor/monad laws)
  // ensure: Writer Monad requires the Log type to be a Monoid (or Semigroup)
  type A = String
  type B = Int
  type C = Boolean
  type D = Long

  def genA: Gen[A] = Arbitrary.arbitrary[A]
  def genB: Gen[B] = Arbitrary.arbitrary[B]
  def genE: Gen[(A, B)] = Arbitrary.arbitrary[(A, B)]
  def genF: Gen[B => C] = Arbitrary.arbitrary[B => C]
  def genG: Gen[C => D] = Arbitrary.arbitrary[C => D]
  def genM: Gen[M[A, B]] = genE.map(from)
  def genMF: Gen[B => M[A, C]] =
    for { m <- genM; f <- genF } yield ((b: B) => map(m)(f))
  def genMG: Gen[C => M[A, D]] =
    for { m <- genM; f <- genF; g <- genG } yield ((c: C) => map(map(m)(f))(g))

  // compares two monads, tuple-wrapping monads have value equality
  def assertEq[B](m: M[A, B], n: M[A, B])(using loc: Location): Prop =
    assertEquals(m, n)
    Prop.passed

  // ----------------------------------------------------------------------
  // Given Monoid as required by (our) Writer Monad
  // ----------------------------------------------------------------------

  object MonoidS extends Monoid[A]:
    def empty: A = ""  // new A() creates new instances
    def append(as: A*): A = as.fold(empty)(_ + _)

  given Monoid[A] = MonoidS

/*
  // ----------------------------------------------------------------------
  // Writer property checks
  // ----------------------------------------------------------------------

  test("check run/from/apply/c'tor") {
    forAll(genE) { e =>
      forAll(genA) { a =>
        assertEquals(run(from(e))(a), e(a))
      }
    }
  }

  // TODO: check co/contra-variance

  // TODO: check Writer methods, see _old
 */
