package netropy.polyglot.fp.effects

import scala.Option  // redundant
import util.Try

/** Unit-tests the scala.Option implementation against the Option_Tests. */
class ex_1_3_ScalaOption_Tests
    extends ex_1_2_Option_Tests[Option]:

  def unit[A](a: A): Option[A] = Some(a)
  def map[A, B](m: Option[A])(f: A => B): Option[B] = m.map(f)
  def flatMap[A, B](m: Option[A])(mf: A => Option[B]): Option[B] = m.flatMap(mf)

  // c'tors: qualify to prevent recursion
  def None: Option[Nothing] = scala.None
  def Some[A](a: A): Option[A] = scala.Some(a)

  test("test apply") {
    assert(None == scala.Option(null))
    assert(Some(true) == scala.Option(true))
  }

/** Unit-tests the scala.util.Try implementation against the Try_Tests. */
class ex_1_3_ScalaTry_Tests
    extends ex_1_2_Try_Tests[Try]:

  def unit[A](a: A): Try[A] = Success(a)
  def map[A, B](m: Try[A])(f: A => B): Try[B] = m.map(f)
  def flatMap[A, B](m: Try[A])(mf: A => Try[B]): Try[B] = m.flatMap(mf)

  // c'tors: qualify to prevent recursion
  def Failure[A](t: Throwable): Try[A] = util.Failure(t)
  def Success[A](a: A): Try[A] = util.Success(a)

  test("test apply") {
    val t = new Throwable()  // single instance for value equality
    assert(Failure(t) == util.Try(throw t))
    assert(Success(true) == util.Try(true))
  }
