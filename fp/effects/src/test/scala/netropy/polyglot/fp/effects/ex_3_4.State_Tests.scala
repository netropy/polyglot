package netropy.polyglot.fp.effects

import munit.Location
import org.scalacheck.{Gen, Arbitrary, Prop}
import Prop.forAll

trait ex_3_4_State_Tests[M[_, _]]
    extends ex_3_1_Monad2_Tests[M]:

  // ----------------------------------------------------------------------
  // State-specifics under test (no need to introduce a typeclass)
  // ----------------------------------------------------------------------

  type State[A, B] = M[A, B]
  def from[A, B](f: A => (A, B)): State[A, B]  // c'tor
  def run[A, B](m: State[A, B])(a: A): (A, B)  // evaluate

  // ----------------------------------------------------------------------
  // ScalaCheck generators (summoning the arbitrary generator)
  // ----------------------------------------------------------------------

  // ensure: types with value equality (as assumed by functor/monad laws)
  type A = Int
  type B = String
  type C = Boolean
  type D = Long

  def genA: Gen[A] = Arbitrary.arbitrary[A]
  def genB: Gen[B] = Arbitrary.arbitrary[B]
  def genE: Gen[A => (A, B)] = Arbitrary.arbitrary[A => (A, B)]
  def genF: Gen[B => C] = Arbitrary.arbitrary[B => C]
  def genG: Gen[C => D] = Arbitrary.arbitrary[C => D]
  def genM: Gen[M[A, B]] = genE.map(from)
  def genMF: Gen[B => M[A, C]] =
    for { m <- genM; f <- genF } yield ((b: B) => map(m)(f))
  def genMG: Gen[C => M[A, D]] =
    for { m <- genM; f <- genF; g <- genG } yield ((c: C) => map(map(m)(f))(g))

  // compares two monads, since function-wrapping monads lack value equality
  def assertEq[B](m: M[A, B], n: M[A, B])(using loc: Location): Prop =
    forAll(genA) { a =>
      assertEquals(run(m)(a), run(n)(a))
    }

  // ----------------------------------------------------------------------
  // State property checks
  // ----------------------------------------------------------------------

  test("check run/from/apply/c'tor") {
    forAll(genE) { e =>
      forAll(genA) { a =>
        assertEquals(run(from(e))(a), e(a))
      }
    }
  }

  // TODO: check co/contra-variance

  // TODO: check Reader methods, see _old
