package netropy.polyglot.fp.effects

/** Unit-tests the StateM implementation. */
class ex_3_4_StateM_Tests
    extends ex_3_4_State_Tests[StateM]:

  // methods under test
  def from[A, B]
    (f: A => (A, B)): State[A, B] = StateM(f)  // c'tor
  def run[A, B]
    (m: StateM[A, B])(a: A): (A, B) = m.run(a)
  // TODO: def unit[A, B]
  def unit[B]
    (b: => B): StateM[A, B] = StateM.unit(b)
  def map[A, B, C]
    (m: StateM[A, B])(f: B => C): StateM[A, C] = m.map(f)
  def flatMap[A, B, C]
    (m: StateM[A, B])(mf: B => StateM[A, C]): StateM[A, C] = m.flatMap(mf)
