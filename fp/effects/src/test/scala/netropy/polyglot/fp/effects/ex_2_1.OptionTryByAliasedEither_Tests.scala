package netropy.polyglot.fp.effects

import ex_2_1_Option.Option
import ex_2_1_Try.Try

/** Property-checks the aliasing Option-Either implementation. */
class ex_2_1_Option_Tests
    extends ex_1_2_Option_Tests[Option]:

  def map[A, B](m: Option[A])(f: A => B): Option[B] = m.map(f)
  def flatMap[A, B](m: Option[A])(mf: A => Option[B]): Option[B] = m.flatMap(mf)

  // c'tors: qualify to prevent recursion
  def unit[A](a: A): Option[A] = ex_2_1_Option.unit(a)
  def None: Option[Nothing] = ex_2_1_Option.None
  def Some[A](a: A): Option[A] = ex_2_1_Option.Some(a)

  test("test apply") {
    assert(None == ex_2_1_Option(null))
    assert(Some(true) == ex_2_1_Option(true))
  }

  def compile_test = {
    // problem: compiles: type Option[+A] is transparent
    val o: Option[Int] = Right[Nothing, Int](0)
  }

/** Property-checks the aliasing Try-Either implementation. */
class ex_2_1_Try_Tests
    extends ex_1_2_Try_Tests[Try]:

  def map[A, B](m: Try[A])(f: A => B): Try[B] = m.map(f)
  def flatMap[A, B](m: Try[A])(mf: A => Try[B]): Try[B] = m.flatMap(mf)

  // c'tors: qualify to prevent recursion
  def unit[A](a: A): Try[A] = ex_2_1_Try.unit(a)
  def Failure[A](t: Throwable): Try[A] = ex_2_1_Try.Failure(t)
  def Success[A](a: A): Try[A] = ex_2_1_Try.Success(a)

  test("test apply") {
    val t = new Throwable()  // single instance for value equality
    assert(Failure(t) == ex_2_1_Try(throw t))
    assert(Success(true) == ex_2_1_Try(true))
  }

  def compile_test = {
    // problem: compiles: type Option[+A] is transparent
    val o: Try[Int] = Right[Nothing, Int](0)
  }
