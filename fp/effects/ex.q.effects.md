# polyglot/fp/effects

### Exercises, Q&A, Code Experiments with Functional Effect Types

Quick Links: \
[scala sources](src/main/scala/netropy/polyglot/fp/effects/),
[scala tests](src/test/scala/netropy/polyglot/fp/effects/)

This project involves experimenting with a couple of 1-ary and 2-ary monad
types and unit-testing different implementations.

Therefore, the first excercises make an effort to avoid the code/pattern
duplication in some property checks in
[../cats/](../cats/ex.q.cats_basics.md).


### 1 Code: Write a _simple_ property checks framework for 1-ary monad types

There exist professional test frameworks for automated law-checking of FP
libraries.  For example:
[Cats Law](https://typelevel.org/cats/typeclasses/lawtesting.html)
for _type classes_-centric FP designs.  Alas, _cats-law_ and its dependency
[discipline](https://github.com/typelevel/discipline)
come with a learning curve.

On the other hand, Scala's language feature of _abstract types_ supports a
really simple and direct _object-oriented_ test framework design.

___Outline:___
1. Take note of the Functor/Monad laws in
   [../](../README.md)
   and in the (duplicated) property checks for Writer/Reader/State in
   [../cats/](../cats/ex.q.cats_basics.md).
1. Factor out the general Functor/Monad property checks into a single base
   trait.
1. Collect any _monad-specific tests_ (e.g., for type _Option_) in a
   subtrait.
1. Finally, define a concrete subclass for each _implementation-under-test_
   (e.g., _scala.Option_).

#### 1.1 Code: Write a base trait for Functor/Monad property checks.

Sufficient to declare
- the type-under-test as a trait parameter,
- the sets of test data as abstract type members,
- test data generators as abstract functions over these types.

#### 1.2 Code: Write traits checking the monads _Option_ and _Try._

Can use subtraits (easiest) or self-typed traits ("cake pattern").

#### 1.4 Code: Write subclasses testing _scala.Option_ and _scala.util.Try._

These conrete test classes for the standard Scala types serve as unit tests
for the inherited property checks.


### 2 Code: Implement the monadic types _Option/Try_ in terms of _Either_

In Scala, a type can be expressed in terms of another by aliasing or by
delegation (or by subclassing).  Scala 3 offers the new language feature of
[opaque type aliases](https://docs.scala-lang.org/scala3/book/types-opaque-types.html).

___Outline:___
- Implement the monad types _Option_ and _Try_ as specializations of _Either._
- Compare the code for type aliasing vs delegation vs aliasing opqauely.

#### 2.1 Code: Implement _Option/Try_ by transparently aliasing _Either_

Quick Links: \
[scala sources](src/main/scala/netropy/polyglot/fp/effects/ex_1_2.OptionTryBy_Either_.scala),
[scala tests](src/test/scala/netropy/polyglot/fp/effects/ex_1_2.OptionTryBy_Either_.scala)

#### 2.2 Code: Implement _Option/Try_ by delegating to private _Either_

Quick Links: \
[scala sources](src/main/scala/netropy/polyglot/fp/effects/ex_1_2.OptionTryBy_Either_.scala),
[scala tests](src/test/scala/netropy/polyglot/fp/effects/ex_1_2.OptionTryBy_Either_.scala)

#### 2.3 Code: Implement _Option/Try_ by opaquely aliasing _Either_ (Scala3)

Quick Links: \
[scala sources](src/main/scala/netropy/polyglot/fp/effects/ex_1_2.OptionTryBy_Either_.scala),
[scala tests](src/test/scala/netropy/polyglot/fp/effects/ex_1_2.OptionTryBy_Either_.scala)

#### 2.4 Q&A: Rank the implementatons of _Option/Try_ in terms of _Either_

TODO: summarize


### 3 Code: Discuss, implement, and test the Writer/Reader/State monads

Motivation: Internalize the definition and usage of standard _effect_ types,
compare with CATS.

Quick Links: \
[scala sources](src/main/scala/netropy/polyglot/fp/effects/),
[scala tests](src/test/scala/netropy/polyglot/fp/effects/)

#### 3.1 TODO: Design a test framework...

TODO: add

#### 3.2 Implement and test the Writer monad.

TODO: fix

Discussion, implementation, unit-, property tests: \
[scala source](src/main/scala/netropy/polyglot/fp/effects/ex_2_2.WriterM.scala),
[scala tests](src/test/scala/netropy/polyglot/fp/effects/ex_2_2.WriterM_Tests.scala)

Resources: \
- read [CATS: Writer](https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers)
- <https://degoes.net/articles/zio-environment>
- <https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers>
- <https://wiki.haskell.org/All_About_Monads#The_Writer_monad>
- <https://www.javadoc.io/static/org.scalaz/scalaz_2.13/7.3.2/scalaz/MonadWriter.html>

#### 3.3 Implement and test the Reader monad.

Discussion, implementation, unit-, property tests: \
[scala source](src/main/scala/netropy/polyglot/fp/effects/ex_2_3.ReaderM.scala),
[scala tests](src/test/scala/netropy/polyglot/fp/effects/ex_2_3.ReaderM_Tests.scala)

Resources: \
- read [CATS: Reader](https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers)
- <https://degoes.net/articles/zio-environment>
- <https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers>
- <https://wiki.haskell.org/All_About_Monads#The_Reader_monad>
- <https://www.javadoc.io/static/org.scalaz/scalaz_2.13/7.3.2/scalaz/MonadReader.html>

#### 3.4 Implement and test the State monad.

Resources: \
[scala source](src/main/scala/netropy/polyglot/fp/effects/ex_2_4.StateM.scala),
[scala tests](src/test/scala/netropy/polyglot/fp/effects/ex_2_4.StateM_Tests.scala)

CATS, blogs, scalaz, Haskell wiki documentation:
- read [CATS: State](https://typelevel.org/cats/datatypes/state.html)
- <https://medium.com/@afsal.taj06/state-monad-unlocked-e946becf0f6f>
- <https://blog.tmorris.net/posts/the-state-monad-for-scala-users/index.html>
- <https://www.javadoc.io/static/org.scalaz/scalaz_2.13/7.3.2/scalaz/package$$State.html>
- <https://wiki.haskell.org/All_About_Monads#The_State_monad>
- <https://wiki.haskell.org/State_Monad>

### 4 Code, Q&A: Discuss the usage of the Writer/Reader/State monads

#### 4.1 TODO: Demo Writer ...

#### 4.2 TODO: Demo Reader ...

Consider this use case:
- A data type for
  [fixed-point numbers](https://en.wikipedia.org/wiki/Fixed-point_arithmetic).
  pairs an integral _value_ with a _scale_ factor.
- Arithmetic operations on fixed-point numbers work like fractions.  For
  example, adding fixed-point numbers requires to convert different scaling
  factors to a common factor before the operation.
- Here, as a special case: _scale_ is a __configured runtime constant__
  throughout the whole program, that is, the number of decimal places is not
  known at compile time but fixed at execution.

In this case, it'd be _wasteful_ to carry a constant _scale_ in each
fixed-point number -- just to have it available for conversions _toLong,_
_toDouble_ etc:
```
case class Fixp(val value: Long, val scale: Int):     // but scale is constant!
  def toLong: Long = value / scale                    // need scale here...
  def toDouble: Double = toLong.toDouble              // ...and here, indirectly
  def ==(y: Fixp): Boolean = value == y.value         // assert(scale == y.scale)
  def +(y: Fixp): Fixp = Fixp(value + y.value, scale) // assert(scale == y.scale)
```

__Task:__
- Design a data type _Fixp_ to work with a runtime constant _scale_ while
  being space-efficient and safe to use (inconsistent scaling factor in
  conversions from/to _Long_ etc).
- Assemble a few example computations to show the type's usage and to serve as
  a unit test; form expressions using 0..3 conversions to/from _Fixp_ as well
  as arithmetic operations.
- Try out different Scala language features and designs of _Fixp,_ including
  use of the _Reader_ monad.
- Summarize the pros & cons of the Scala designs, given this use case.

#### 4.3 TODO: Demo State ...

[Up](../README.md)
