## polyglot/fp/recursion

#### 2.5 Compare implementation against alternative/proposed code solutions.

_[Discuss the Rosetta (strict) Y Combinator version posted for Scala.  Compare
against solution from 2.4.  Optionally, also discuss alternative designs._

[(1)]: https://rosettacode.org/wiki/Y_combinator
[(2)]: https://mvanier.livejournal.com/2897.html
[(3)]: https://plato.stanford.edu/entries/lambda-calculus/#Com
[(4)]: https://en.wikipedia.org/wiki/Fixed-point_combinator
[(5)]: https://www.youtube.com/watch?v=9T8A89jgeTI

The Rosetta Y Combinator code suggested for
[Scala (as of 2020-08-08)](https://rosettacode.org/wiki/Y_combinator#Scala)
has its origin in a
[Scala Blog discussion 2008/2009](https://web.archive.org/web/20160709050901/http://scala-blogs.org/2008/09/y-combinator-in-scala.html).
See discussion of this code version, its refactoring, and of alternative
designs in:

[Scala Sources](../src/main/scala/netropy/polyglot/fp/recursion/Ex_2_5_FixPointS.scala),
[Scala Tests](../src/test/scala/netropy/polyglot/fp/recursion/Ex_2_FixPointSTests.scala)

[Up](ex_2.q.fixpoint_combinators.md)
