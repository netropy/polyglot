# polyglot/fp/recursion

Q&A of exercises below.

Code for 1.x exercises: \
[Scala Source](../src/main/scala/netropy/polyglot/fp/recursion/Ex_1_RecFacFibS.scala),
[Scala Test](../src/test/scala/netropy/polyglot/fp/recursion/Ex_1_RecFibFabSTests.scala),
[Java Source](../src/main/java/netropy/polyglot/fp/recursion/Ex_1_RecFacFibJ.java),
[Java Test](../src/test/java/netropy/polyglot/fp/recursion/Ex_1_RecFibFabJTests.java)

See code comments for language-specific notes.

#### 1.1 Write _factorial_, _fibonacci_ as iterative functions.

A bit tricky to get the loop counts right.

#### 1.2 Write _factorial_, _fibonacci_ as recursive functions.

Straight forward (ignoring robustness: preconditions, overflow etc).

#### 1.3 Check language support for recursive fields/values/closures.

See code for comments and compile error messages.

| feature | Scala | Java |
|---|:---:|:---:|
| recursive field | yes | no |
| recursive lazy field | yes | - |
| recursive local value | no | no |
| recursive lazy local value | yes | - |
| closures have own `this` | no | no |

Surprising that Java does not allow for recursive fields.  The JVM provides
for fields to have default values (`null`, `0`, `0.0`, and `false`) at
construction, before their initializer is run.  No such guarantee for local
variables.  Compiler (and probably also byte-code verifier) ensures locals
are initialized before use.

#### 1.4 Refactor _factorial_, _fibonacci_ as recursive function values.

Straight forward in Scala.

Java does not support fields or local values that are recursive.  As a
workaround, have lambdas delegate to (recursive) methods.

#### 1.5 Refactor as tail-recursive (or iterative) functions.

Straight forward in Scala.

Not supported in Java, have to code iterative solution.

[Up](ex_1.q.recursion.md)
