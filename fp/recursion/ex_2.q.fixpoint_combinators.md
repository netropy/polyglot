# polyglot/fp/recursion

### 2 Exercises, Experiments on Recursion and the _Y_ FixPoint Combinator

Motivation:
- Better understand the definition and significance of fixpoint combinators.
- Coding practice with lambdas, recursion, recursive types, subtyping
  solutions such as the _CRTP_ pattern (F-bound polymorphism), necessity of
  non-strict vs strict definitions.
- Comparing languages for ease-of-coding as to: lambdas, recursion, function
  types, implications of statically vs dynamically-typed, language means for
  by-name vs by-value evaluation.

Currently, code experiments and side-by-side comparison for Scala and Java.
See [(1)] for these and other languages.

Useful Reading:
- [(1)]: https://rosettacode.org/wiki/Y_combinator
  [(1)][]: Various language versions of the Y combinator (Rosetta Code)
- [(2)]: https://mvanier.livejournal.com/2897.html
  [(2)][]: _Scheme_ code & discussion of the Y (Mike's World-O-Programming
  Blog)
- [(3)]: https://plato.stanford.edu/entries/lambda-calculus/#Com
  [(3)][]: Excellent summary of _standard λ-terms and combinators_
  (Stanford Encyclopedia of Philosophy)
- [(4)]: https://en.wikipedia.org/wiki/Fixed-point_combinator
  [(4)][]: Details on the Y and other fixed-point combinators (Wikipedia)
- [(5)]: https://www.youtube.com/watch?v=9T8A89jgeTI
  [(5)][]: Introductory Video on the Y Combinator (Graham Hutton,
  Computerphile)

Progression of the exercises:
- Ex 2.1: Definitions, Y Combinator in Lambda Calculus.
- Ex 2.2: Derive non-recursive function operators `_FOp`.
- Ex 2.3: Derive recursive fixpoint generator `fixR`.
- Ex 2.4: Derive fixpoint combinator `fix`.
- Ex 2.5: Compare against alternative code solutions.

Q&A for each exercise in _[Remarks]_.  Language-specific notes in the
_[Sources]_.

Quick Links: \
[Scala Sources](../src/main/scala/netropy/polyglot/fp/recursion/),
[Java Sources](../src/main/java/netropy/polyglot/fp/recursion/),
[Scala Tests](../src/test/scala/netropy/polyglot/fp/recursion/),
[Java Tests](../src/test/java/netropy/polyglot/fp/recursion/)

#### 2.1 Explain the the _Y Combinator_ and its significance.

[Remarks](ex_2_1.r.md)

#### 2.2 Abstract out recursion, recover functions via recursive wrappers.

Take recursive functions `factorial`, `fibonacci` from exercise
[1 Recursion](ex_1.q.recursion.md)
and refactor.

[Remarks](ex_2_2.r.md)

#### 2.3 Refactor wrappers into recursive, fixpoint-generating function.

Take strict/non-strict, recursive wrappers for _factorial_, _fibonacci_ from
previous exercise and refactor.

[Remarks](ex_2_3.r.md)

#### 2.4 Refactor fixpoint-generating function into non-recursive combinator.

Take strict/nonstrict, recursive function `fixR` from previous exercise and
refactor.

Check against Rosetta (strict) Y Combinator version
[posted for Java](https://rosettacode.org/wiki/Y_combinator#Java_2).

[Remarks](ex_2_4.r.md)

#### 2.5 Compare implementation against alternative/proposed code solutions.

Discuss the Rosetta (strict) Y Combinator version
[posted for Scala](https://rosettacode.org/wiki/Y_combinator#Scala).

Compare against solution from 2.4.  Optionally,also discuss alternative
designs.

[Remarks](ex_2_5.r.md)

[Up](../README.md)
