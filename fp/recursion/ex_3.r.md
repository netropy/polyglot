# polyglot/fp/recursion

Q&A and code for exercises below.

### 3 Exercises on Recursion, _Tower of Hanoi_ on [Im-]Mutable Collections

The natural data type for formulating "Tower of Hanoi" is `Stack`.  This
follows directly from the first two constraints of the problem:
1. Only one disk can be moved at a time.
2. Each move takes an upper disk and places it on top of another stack.

The trait `Ex_3_TowerOfHanoiS` encapsulates a `Seq/Stack`-like data type and
a `move` algorithm for the ToH problem to allow for a single unit test suite
to run on multiple ToH implementations.  Since Scala collections do not offer
a `collection.Stack` abstraction (which would not add much of value) for
mutable and immutable stacks, the close type `collection.Seq` is used as type
bound for the abstract member type:
```
trait Ex_3_TowerOfHanoiS {

  /** Abstract type modeling a stack of items/discs of varying sizes. */
  type IStack <: scala.collection.Seq[Int]
```

___Implement ToH over a suitable, immutable collection type:___

Scala does not offer an immutable Stack type (deprecated in 2.11, removed in
2.13) since `collection.immutable.List` is best-suited as stack:  _O(1)_ time
`head/tail` decomposition and `::` composition can be used as efficient `top`,
`pop`, and `push` operations.

Immutability allows for writing a _move (disc)_ operation as an expression:
```
    ...
    val (t1, s1, d1) = move(n - 1, t0, s0.tail, s0.head :: d0)
    ...
```

[Scala Source](../src/main/scala/netropy/polyglot/fp/recursion/Ex_3_1_TowerOfHanoiS.scala)

___Implement ToH over a suitable, mutable collection type:___

Since Scala 2.13, class `collection.mutable.Stack` is based on the new (and
long-missed) `ArrayDeque`, which characteristically offers amortized _O(1)_
time insertion and removal from beginning or end.

Mutability suggests avoiding most of those general collection operations which
result in copying, e.g., `head/tail`, `appended[All]/prepended[All]`, pattern
matching etc.  Instead, `xxxInPlace` or update methods `+=`, `+=:`,
`append/prepend`, `++=`, `++=:` etc directly modify the given collection.

This requires writing a _move (disc)_ operation as a statement:
```
    d0.push(s0.pop())
    val (t1, s1, d1) = move(n - 1, t0, s0, d0)
```

[Scala Source](../src/main/scala/netropy/polyglot/fp/recursion/Ex_3_2_TowerOfHanoiS.scala)

___Require the weakest, necessary set of preconditions:___

Necessary preconditions of method `move` are the consistency of arguments `n`
and `src`:
```
  def move(n: Int, src: IStack, tmp: IStack, dst: IStack):
      (IStack, IStack, IStack) = {

    require(0 <= n && n <= src.size)
    require(isSorted(src))
	...
  }
```

The other input stacks `tmp` and `dst` do not have to be empty.  Method `move`
can allow for them to be non-empty as long as they are consistent and can
accommodate `n[-1]` items from `src`:
```
    require(isSorted(tmp))
    require(isSorted(dst))
    require(n < 1 || dst.isEmpty || src(n - 1) < dst.head)  // O(n) for Lists
    require(n < 2 || tmp.isEmpty || src(n - 2) < tmp.head)  // O(n) for Lists
```

___Ensure the strongest, reasonable set of postconditions:___

Immutability allows for the result values to be checked against the arguments
(which are available unmodified).  This makes for strong post-conditions,
since the output can be compared against a quick, alternative calculation
(using `splitAt`, which does not have to obey the ToH constraint).

As a consequence, the `ensuring`-clause for the immutable, `List`-based
implementation fails early, before any errors propagate up the recursion or to
the unit test:
```
      val (a, b) = src.splitAt(n)
      val c = a ::: dst
      assert(s == b, s"s = $s != $b = b")
      assert(t == tmp, s"t = $t != $tmp = tmp")
      assert(d == c, s"d = $d != $c = c")
```

Furthermore, these post-conditions (together with the preconditions) imply the
order-consistency of the output stacks, why `assert(isSorted(...))` checks
would be redundant.

In contrast, mutability of the input arguments implies that they are updated
in-place and, hence, the original input values are lost (they could be copied
for `ensuring`-purposes at a performance penalty).

Therefore, without imposing extra overhead, the only post-condition that the
`Stack`-based ToH version can check the order-consistence of the input=output
stacks:
```
      assert(isSorted(s))
      assert(isSorted(t))
      assert(isSorted(d))
```

___Provide (negative+positive) unit tests covering above versions:___

Separate unit test suites for immutable and mutable stack-based ToH versions
would result in unnecessary code duplication.  A single suite covering all
versions has to be parametrized over the `IStack` collection type, the factory
methods for creating test data to that type, and the ToH `move` function.

The easiest approach avoids multiple parameters with a higher-kinded type
(like `SeqFactory`) but just encapsulates above parameters and abstracts from
a ToH implementation by a single trait `Ex_3_TowerOfHanoiS` with an abstract
type member `IStack` (see above).  The unit test suite can then be defined as
self-type annotated trait and be mixed in with an implementation
(a.k.a. "cake" pattern for dependency injection).

This approach has the added benefit that most unit-test frameworks
(e.g. junit4) recognize inherited tests and properly report them per
implementing subtype.

[Scala Source](../src/main/scala/netropy/polyglot/fp/recursion/Ex_3_0_TowerOfHanoiS.scala),
[Scala Test](../src/test/scala/netropy/polyglot/fp/recursion/Ex_3_TowerOfHanoiS.scala)

___Summarize recursive ToH on immutable vs mutable collections:___

Immutable collections:
+ robustness, readability
+ efficient (if collection's design utilizes structural sharing)
- inefficient for some operations (e.g., index-access, appending to Lists),
  may be required by pre-/postconditions even if function body avoided them
+ largely carefree use of pattern matching, [de-]composition
+ possibly strong postconditions, input values available for checking results

Mutable collections:
+ efficient (due to in-place updates)
+ smaller yet also more opaque signatures (what param is input/output/both?)
- care is required to avoid inadvertent copying of mutable collections, e.g.,
  by use of pattern matching, [de-]composition etc
- possibly weaker postconditions, unmodified input values may no longer be
  available (unless copied at overhead for the purpose of _ensuring_)
- unit tests may have to clone input values against which to compare results

[Up](ex_1.q.recursion.md)
