# polyglot/fp/recursion

### 3 Exercises on Recursion, _Tower of Hanoi_ on [Im-]Mutable Collections

Motivation: Compare a recursive decomposition over immutable vs mutable
collection types, implications for pre- & post-conditions.

Well-known exercise with a nice, recursive decomposition:
[Tower of Hanoi (Wikipedia)](https://en.wikipedia.org/wiki/Tower_of_Hanoi)

Implement a recursive _ToH_ algorithm.  For additional insights:
- implement ToH over a suitable, _immutable_ collection type
- implement ToH over a suitable, _mutable_ collection type
- require the _weakest_, necessary set of preconditions
- ensure the _strongest_, reasonable set of postconditions
- provide (negative+positive) unit tests covering above versions
- summarize recursive ToH on immutable vs mutable collections

[Remarks](ex_3.r.md)

Quick Links: \
[Scala Sources](../src/main/scala/netropy/polyglot/fp/recursion/),
[Scala Tests](../src/test/scala/netropy/polyglot/fp/recursion/)

#### 3.1 Write a recursive _ToH_ function on an immutable collection type.

See Remarks, Quick Links above.

#### 3.2 Write a recursive _ToH_ function on a mutable collection type.

See Remarks, Quick Links above.

[Up](../README.md)
