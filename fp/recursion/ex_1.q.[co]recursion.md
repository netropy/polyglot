# polyglot/fp/recursion

### 1 Exercises, Experiments on Recursion and Corecursion

Compare languages for ease-of-coding with: lambdas, function types, recursion,
tail-recursion, corecursion, call-by-value vs call-by-name vs call-by-need.

Q&A for the exercises: \
[Remarks](ex_1.r.md)

Quick Links: \
[Scala Sources](src/main/scala/netropy/polyglot/fp/recursion/),
[Java Sources](src/main/java/netropy/polyglot/fp/recursion/),
[Scala Tests](src/test/scala/netropy/polyglot/fp/recursion/),
[Java Tests](src/test/java/netropy/polyglot/fp/recursion/)

Resources:
[Factorials](https://en.wikipedia.org/wiki/Factorial),
[Fibonacci numbers](https://en.wikipedia.org/wiki/Fibonacci_number)

Tasks:
- No need to consider arithmetic overflow, type `Int` Ok.
- Provide unit-tests.

#### 1.1 Write _factorial_, _fibonacci_ as iterative functions.

#### 1.2 Write _factorial_, _fibonacci_ as recursive functions.

#### 1.3 Check language support for recursive fields/values/closures.

Fields or local value/variable definitions are evaluated by value: only once
and before their 1st use.
- What recursive definitions allowed by a language?
- What if a field or a local value is a lambda?
  Do closures have a `this`, or can they capture their own reference?
- What other language (or stdlib) options support defining a value that is
  evaluated exactly once, memoized, and allowed to be recursive?

#### 1.4 Refactor _factorial_, _fibonacci_ as recursive function values.

Clarification: Try write as recursive definition of a field/value that is a
lambda, i.e., a closure that captures its own reference.  If not supported by
the language, fall back on a lambda that just wraps a recursive method.

#### 1.5 Refactor as tail-recursive (or desugared) functions.

Clarification: For languages that do not support tail-recursion, provide the
iterative, desugared version of the tail-recursive function.

[Up](../README.md)
