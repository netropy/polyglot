package netropy.polyglot.fp.recursion;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.IntUnaryOperator;


class Ex_1_RecFacFibJ {

    /*
     * Note: Leaving out pre-conditions (n >= 0), post-conditions.
     * Not considering arithmetic overflow; type `int` ok, here.
     */

    // ----------------------------------------------------------------------
    // Ex 1.1: Write factorial, fibonacci as iterative functions.
    // ----------------------------------------------------------------------

    static public int factorialI(int n) {
        int r = 1;
        for (int i = 1; i <= n; i++)  // tricky loop count
            r *= i;
        return r;
    }

    static public int fibonacciI(int n) {
        int r0 = 0;
        if (n == 0)
            return r0;

        int r1 = 1;
        for (int i = 1; i < n; i++) {  // tricky loop count
            int t = r0;
            r0 = r1;
            r1 += t;
        }
        return r1;
    }

    // ----------------------------------------------------------------------
    // Ex 1.2: Write factorial, fibonacci as recursive functions.
    // ----------------------------------------------------------------------

    static public int factorialR(int n) {
        return (n == 0) ? 1 : n * factorialR(n - 1);
    }

    static public int fibonacciR(int n) {
        return (n < 2) ? n : fibonacciR(n - 1) + fibonacciR(n - 2);
    }

    /*
     * Notes:
     * - These computations on int as IntUnaryOperator (no need for Long etc).
     */

    // ----------------------------------------------------------------------
    // Ex 1.3 Check language support for recursive fields/values/closures.
    // ----------------------------------------------------------------------

    /*
     * Notes: Java does not support recursive fields/values/closures.
     */

    static class Ignore {

        // Fields cannot be recursive.
        //
        // compile error: self-reference in initializer
        // static public Function<Integer, Integer> f = a -> f.apply(a);

        // Closures have no `this`.
        //
        // compile error: cannot find symbol: method apply
        // public Function<Integer, Integer> f = a -> this.apply(a);
        //
        // compile error: non-static variable this cannot be referenced from
        // a static context
        // static public Function<Integer, Integer> f = a -> this.apply(a);

        static void m() {

            // Local values/variables cannot be recursive.
            //
            // compile error: variable f might not have been initialized
            //Function<Integer, Integer> f = a -> f.apply(a);

            // Static class or enum types must not be local.
        }

        // No use to move code into enum singleton.
        //
        // enum G {
        //     // compile error: self-reference in initializer
        //     g(i -> G.g.f.apply(i));
        //     public Function<Integer, Integer> f;
        //     G(Function<Integer, Integer> f) { this.f = f; }
        // }
        // int j = G.g.f.apply(2);

        // No use to move code into lazy Supplier mutable field.
        //
        // compile error: self-reference in initializer
        // private Supplier<Function<Integer, Integer>> g = () -> {
        //     Function<Integer, Integer> val = i -> g.get().apply(i);
        //     g = () -> val;
        //     return val;
        // };
    }

    // ----------------------------------------------------------------------
    // Ex 1.4: Refactor factorial, fibonacci as recursive function values.
    // ----------------------------------------------------------------------

    static public IntUnaryOperator factorialRV =
        n -> factorialR(n);

    static public IntUnaryOperator fibonacciRV =
        n -> fibonacciR(n);

    /*
     * Note: Java does not support fields or local values that are recursive.
     * As a workaround, have lambdas delegate to (recursive) methods.
     */

    // ----------------------------------------------------------------------
    // Ex 1.5: Refactor as tail-recursive (or desugared) functions.
    // ----------------------------------------------------------------------

    static public int factorialDS(int n) {
        // mimic desugared version of a language supporting tail-recursion
        int r = 1;
        while (true) {
            if (n == 0)
                return r;
            r = n * r;
            n -= 1;
        }
    }

    static public int fibonacciDS(int n) {
        // mimic desugared version of a language supporting tail-recursion
        int r0 = 1;
        int r1 = 0;
        while (true) {
            switch (n) {
            case 0: return r1;
            case 1: return r0;
            default:
                int tmp = r0;
                r0 = r0 + r1;
                r1 = tmp;
                n -= 1;
            }
        }
    }

    /*
     * Notes:
     *
     * - No compile-support for tail-recursion in Java.
     *   Some blogs suggesting support for tail-recursion, e.g.:
     *   [https://medium.com/javarevisited/tail-recursion-in-java-abc24f56b56b]
     *
     * - So, giving here iterative version with local, mutable state.
     */
}
