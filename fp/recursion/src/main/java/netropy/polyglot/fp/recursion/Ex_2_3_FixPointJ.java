package netropy.polyglot.fp.recursion;

import java.util.function.Function;

import static netropy.polyglot.fp.recursion.Ex_2_2_FixPointJ.*;


public class Ex_2_3_FixPointJ {

    // ----------------------------------------------------------------------
    // Ex 3.1: Abstract out recursion into generic, recursive `fixRStrict`.
    // ----------------------------------------------------------------------

    static public <A, B> Function<A, B> fixRStrict(FOperatorStrict<A, B> f) {

        /*
         * Note: Directly passing `fixRStrict(f)` results in an endless loop.
         *
         * Evaluates `fixRStrict(f)` before outer `f` sees it:
         *   return f.apply(fixRStrict(f));
         *
         * Solution: pass as lambda with explicit argument.
         */

        return f.apply(a -> fixRStrict(f).apply(a));
    }

    /*
     * Note: Java knows no type aliases :-(  Defining a subtype may not help.
     *
     * Subtypes won't compose for
     * - consumer takes only subtype of `Function` (contravariant position),
     * - but producer yields general `Function` type (covariant position).
     *
     * public interface IntegerUnaryOp extends UnaryOperator<Integer>
     * {}
     *
     * error: Function<Integer,Integer> cannot be converted to IntegerUnaryOp
     * static public IntegerUnaryOp fixRStrictInt(IntegerFOperatorStrict f) {
     *     return f.apply(a -> fixRStrictInt(f).apply(a));
     * }
     */

    // ----------------------------------------------------------------------
    // Ex 3.2: Recover `F` from an `FOp` using recursive `fixRStrict`.
    // ----------------------------------------------------------------------

    static public Function<Integer, Integer> factorial_fixRStrict_FOpStrict =
        n -> fixRStrict(factorial_FOpStrict).apply(n);

    static public Function<Integer, Integer> fibonacci_fixRStrict_FOpStrict =
        n -> fixRStrict(fibonacci_FOpStrict).apply(n);

    /*
     * Note: Better define the operations as function values, built 1x only.
     *
     * static public Integer factorial_fixRStrict_FOpStrict(Integer n) {
     *     return fixRStrict(factorial_FOpStrict).apply(n);
     * }
     *  static public Integer fibonacci_fixRStrict_FOpStrict(Integer n) {
     *     return fixRStrict(fibonacci_FOpStrict).apply(n);
     * }
     */
}
