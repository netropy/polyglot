package netropy.polyglot.fp.recursion;

import java.util.function.Function;


public class Ex_2_2_FixPointJ {

    // ----------------------------------------------------------------------
    // Ex 2.1: Abstract out the recursive call as `FOperator` HOF.
    // ----------------------------------------------------------------------

    public interface FOperatorStrict<A, B>
        extends Function<Function<A, B>, Function<A, B>>
    {}

    public interface IntegerFOperatorStrict
        extends FOperatorStrict<Integer, Integer>
    {}

    static public IntegerFOperatorStrict factorial_FOpStrict =
        f -> n -> (n <= 1) ? 1 : (n * f.apply(n - 1));

    static public IntegerFOperatorStrict fibonacci_FOpStrict =
        f -> n -> (n < 2) ? n : (f.apply(n - 1) + f.apply(n - 2));

    /*
     * Notes:
     *
     * - Java offers no type aliases, can only define subtypes.
     * - From here on, all computations on Integer, as per Function<A, B>.
     *
     * - Can provide _FOpStrict as function value since no longer recursive.
     * - Java does not offer call-by-name, so _FOpStrict is strict in arg f.
     * - Therefore, _FOpStrict can only be wrapped by a recursive method,
     *   not by a function value (also, error: self-reference in initializer).
     *
     * - Java's syntax for function application diverges for function values:
     *     f(...) vs f.apply(...), f.applyAsInt(...)
     *
     * - Java's syntax for method references always requires a class name,
     *   why a lambda looks cleaner than a reference to a class-local method:
     *     .apply(m -> factorial_wfR_FOpStrict(m))            // cleanest
     *     .apply(factorial_wfR_FOpStrict)                    // not supported
     *     .apply(::factorial_wfR_FOpStrict)                  // not supported
     *     .apply(Ex_2_2_FixPointJ::factorial_wfR_FOpStrict)  // cluttered
     */

    // ----------------------------------------------------------------------
    // Ex 2.2: Recover `F` from an `FOp` per recursive wrapper function `wfR`.
    // ----------------------------------------------------------------------

    static public Integer factorial_wfR_FOpStrict(Integer n) {
        return factorial_FOpStrict
            .apply(m -> factorial_wfR_FOpStrict(m))
            .apply(n);
    }

    static public Integer fibonacci_wfR_FOpStrict(Integer n) {
        return fibonacci_FOpStrict
            .apply(m -> fibonacci_wfR_FOpStrict(m))
            .apply(n);
    }

    /*
     * Note: Cannot give recursive definitions of function values in Java.
     * Can only define these function values by wrapping recursive methods.
     *
     * error: self-reference in initializer
     *
     * static public Function<Integer, Integer> factorial_wfR_val_FOpStrict =
     *   factorial_FOpStrict.apply(factorial_wfR_val_FOpStrict);
     *
     * static public Function<Integer, Integer> factorial_wfR_val_FOpStrict =
     *   factorial_FOpStrict.apply(n -> factorial_wfR_val_FOpStrict.apply(n));
     */
}
