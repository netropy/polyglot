package netropy.polyglot.fp.recursion;

import java.util.function.Function;

import static netropy.polyglot.fp.recursion.Ex_2_2_FixPointJ.*;


public class Ex_2_4_FixPointJ {

    // ----------------------------------------------------------------------
    // Ex 4.1: Convert `fixRStrict` into a true combinator `fixStrict`.
    // ----------------------------------------------------------------------

    /** Type for a functional `g` that can be applied onto itself `g(g): F`. */
    static interface RecursiveFunction<F>
        extends Function<RecursiveFunction<F>, F>
    {}

    static public <A,B> Function<A,B> fixStrict(FOperatorStrict<A, B> f) {

        RecursiveFunction<Function<A,B>> g =
            w -> f.apply(x -> w.apply(w).apply(x));  // w -> f(x -> w(w)(x))
        return g.apply(g);
    }

    /*
     * Notes:
     *
     * - See Q&A on how to derive the Y combinator `fix` from the recursive
     *   function `fixR` from [[Ex_2_3_FixPointS]].
     *
     * - The `_fixStrict` variant must internally pass the strict operator
     *   as a lambda (eta per `(_)`).  Same as [[Ex_2_3_FixPointJ]].
     *
     * - Cannot provide the generic method `fixStrict` as a function value.
     *
     * - See Q&A: A difficulty is to define a recursive type for the functional
     *   `g: G<F>` that allows for self-application: `Function<G<F>, F>`.
     *   A solution is to combine subtyping with generics for a CRTP pattern
     *   (or F-bound type).  See definition of `RecursiveFunction` below.
     *
     * - Java does not allow for interfaces to be function-local :-(
     */

    // ----------------------------------------------------------------------
    // Ex 4.2: Recover `F` from an `FOpStrict` using combinator `fixStrict`.
    // ----------------------------------------------------------------------

    static public Function<Integer, Integer> factorial_fixStrict_FOpStrict =
        n -> fixStrict(factorial_FOpStrict).apply(n);

    static public Function<Integer, Integer> fibonacci_fixStrict_FOpStrict =
        n -> fixStrict(fibonacci_FOpStrict).apply(n);

    /*
     * Note: Better define the operations as function values, built 1x only.
     *
     * static public Integer factorial_fixStrict_FOpStrict(Integer n) {
     *     return fixStrict(factorial_FOpStrict).apply(n);
     * }
     * static public Integer fibonacci_fixStrict_FOpStrict(Integer n) {
     *     return fixStrict(fibonacci_FOpStrict).apply(n);
     * }
     */
}
