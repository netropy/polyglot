package netropy.polyglot.fp.recursion


object Ex_2_3_FixPointS {

  import Ex_2_2_FixPointS._

  // ----------------------------------------------------------------------
  // Ex 3.1: Abstract out recursion into generic, recursive `fixR`.
  // ----------------------------------------------------------------------

  def fixR[A, B](f: FOperator[A, B]): A => B =
    f(fixR(f))

  def fixRStrict[A, B](f: FOperatorStrict[A, B]): A => B =
    f(fixRStrict(f)(_))  // must pass as eta (_) to avoid endless expansion

  /*
   * Notes:
   *
   * - Offering both `fixR`, `fixRStrict` here as complementary usage options.
   *   Cannot overload by-name/by-value signatures, but can disambiguate them
   *   by name.  Their application is type-safe (see below).
   *
   * - The `_fixRStrict` variant must internally pass the strict operator
   *   as a lambda (eta per `(_)`) to avoid endless expansion.
   *
   * - Cannot provide the generic methods `fixR[Strict]` as function values.
   *   - Scala 2: values/functions cannot be polymorphic.
   *   - Scala 3: Rumors that values/functions may be polymorphic.
   *     Dotty has this PR merged, but documentation still missing (2020-08).
   *       No information yet on the final syntax:
   *       [[https://github.com/lampepfl/dotty/pull/4672]]
   *       [[https://github.com/lampepfl/dotty/issues/7594]]
   */

  // ----------------------------------------------------------------------
  // Ex 3.2: Recover `F` from an `FOp` using recursive `fixR`.
  // ----------------------------------------------------------------------

  val factorial_fixR_FOp: Int => Int =
    fixR(factorial_FOp)

  val factorial_fixRStrict_FOpStrict: Int => Int =
    fixRStrict(factorial_FOpStrict)

  val fibonacci_fixR_FOp: Int => Int =
    fixR(fibonacci_FOp)

  val fibonacci_fixRStrict_FOpStrict: Int => Int =
    fixRStrict(fibonacci_FOpStrict)

  /*
   * Notes:
   *
   * - The usage of functions `fixR` vs `fixRStrict` is type-safe, they only
   *   accept `_FOp` or `_FOpStrict` as argument, respectively.
   *
   * - Can provide all recovered, non-recursive `_fixR` functions as values.
   *   No need to define the `_fixRStrict` version as method, as in prior
   *   [[Ex_2_2_FixPointS]], as it passes the operator as a lambda, see above.
   */
}
