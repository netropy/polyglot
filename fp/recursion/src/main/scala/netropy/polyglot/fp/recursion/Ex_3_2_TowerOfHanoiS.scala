package netropy.polyglot.fp.recursion

import collection.mutable.Stack


class Ex_3_2_TowerOfHanoiS extends Ex_3_TowerOfHanoiS {

  // ----------------------------------------------------------------------
  // Ex 3.2: Write a recursive ToH function over a mutable collection
  // ----------------------------------------------------------------------

  // Since Scala 2.13, Stack is efficiently based on ArrayDeque :-)
  final type IStack = Stack[Int]

  final def iStack(is: Int*) = Stack(is: _*)

  final def clone(is: IStack): IStack = is.drop(-1)

  // some efficient, non-strict IndexedSeq recursion
  // avoid copying of mutable collections due to pattern-matching, tail etc
  final override def isSorted(s: IStack): Boolean = {
    @annotation.tailrec
    def go(i: Int): Boolean =
      if (i < 1) true
      else (s(i - 1) < s(i)) && go(i - 1) // && is non-strict
    go(s.size - 1)
  }

  final def move(n: Int, src: IStack, tmp: IStack, dst: IStack):
      (IStack, IStack, IStack) = {

    // weakest set of necessary preconditions, most of them O(n)
    require(0 <= n && n <= src.size)
    require(isSorted(src))
    require(isSorted(tmp))
    require(isSorted(dst))
    require(n < 1 || dst.isEmpty || src(n - 1) < dst.head)  // O(1) for Stack
    require(n < 2 || tmp.isEmpty || src(n - 2) < tmp.head)  // O(1) for Stack

    // move zero or one item, amortized O(1) for Stack/ArrayDeque
    // avoid copying of mutable collections due to head/tail, prepended etc
    n match {
      case 0 => (src, tmp, dst)
      case 1 =>  // redundant, yet nice
        dst.push(src.pop())
        (src, tmp, dst)
      case _ =>
        val (s0, d0, t0) = move(n - 1, src, dst, tmp)
        d0.push(s0.pop())
        val (t1, s1, d1) = move(n - 1, t0, s0, d0)
        (s1, t1, d1)
    }

  } ensuring { case (s, t, d) =>

      // strongest set of post-conditions, most of them O(n)
      // no unmodified input stacks against which to compare output:
      //   val (a, b) = src.splitAt(n)
      //   a ++= dst
      //   assert(s == b, s"s = $s != $b = b")
      //   assert(t == tmp, s"t = $t != $tmp = tmp")
      //   assert(d == a, s"d = $d != $a = a")
      // only option here is to check consistency:
      assert(isSorted(s))
      assert(isSorted(t))
      assert(isSorted(d))
      true
  }
}
