package netropy.polyglot.fp.recursion


object Ex_2_2_FixPointS {

  // ----------------------------------------------------------------------
  // Ex 2.1: Abstract out the recursive call as `FOperator` HOF.
  // ----------------------------------------------------------------------

  type FOperator[A, B] = (=> A => B) => A => B

  val factorial_FOp: FOperator[Int, Int] =
    f => n => if (n == 0) 1 else n * f(n - 1)

  val fibonacci_FOp: FOperator[Int, Int] =
    f => n => if (n < 2) n else f(n - 1) + f(n - 2)

  type FOperatorStrict[A, B] = (A => B) => A => B

  val factorial_FOpStrict: FOperatorStrict[Int, Int] =
    f => n => if (n == 0) 1 else n * f(n - 1)

  val fibonacci_FOpStrict: FOperatorStrict[Int, Int] =
    f => n => if (n < 2) n else f(n - 1) + f(n - 2)

  /*
   * Notes: Pass argument by-name, by-value, or offer both?
   *
   * - Scala supports by-name parameters enabling lazy evaluation and
   *   non-strictness.
   *
   * - Cannot overload by-name and by-value signatures if otherwise equal.
   *   But can disambiguate them by name, so offering both is an option.
   *
   * - For _FOperator[Strict], the usage implications are different for
   *   by-name and by-value signatures.  So, we try out both of them here.
   *   Yet, we treat the by-name version as default.  See note below.
   *
   * - Can provide _FOpStrict, _FOp as function values or as methods or both.
   *   Best to provide these non-recursive, stateless functions as values,
   *   no need to create an instance each time when passing them as a lambda.
   */

  // ----------------------------------------------------------------------
  // Ex 2.2: Recover `F` from an `FOp` per recursive wrapper function `wfR`.
  // ----------------------------------------------------------------------

  val factorial_wfR_FOp: Int => Int =
    factorial_FOp(factorial_wfR_FOp)

  def factorial_wfR_FOpStrict(n: Int): Int =
    factorial_FOpStrict(factorial_wfR_FOpStrict)(n)

  val fibonacci_wfR_FOp: Int => Int =
    fibonacci_FOp(fibonacci_wfR_FOp)

  def fibonacci_wfR_FOpStrict(n: Int): Int =
    fibonacci_FOpStrict(fibonacci_wfR_FOpStrict)(n)

  /*
   * Notes:
   *
   * - The usage implications for _FOpStrict vs _FOp are different:
   *
   *   - _FOpStrict: recursive wrapper can be method but not a function value:
   *
   *     + def factorial_wfR_FOpStrict(n: Int): Int = ...
   *       good: passing a method/lambda (lazy) by-value (eager)
   *
   *     - val factorial_wfR_val_FOpStrict: Int => Int = ...
   *       error: passing a function value (eager) by-value (eager)
   *       NullPointerException for n > 0, field is null while not yet defined
   *
   *   - _FOp: recursive wrapper can be method but is better a function value:
   *
   *     - def factorial_wfR_FOp(n: Int): Int = ...
   *       overkill: passing a method/lambda (lazy) by-name (lazy)
   *
   *     + val factorial_wfR_val_FOp: Int => Int = ...
   *       best: passing a function value (eager) by-name (lazy)
   *
   * - The choice of _FOpStrict vs _FOp also affects how subsequent Fixpoint
   *   function and combinator have to be implemented.
   */
}
