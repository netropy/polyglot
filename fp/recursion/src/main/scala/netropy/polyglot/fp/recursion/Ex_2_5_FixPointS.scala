package netropy.polyglot.fp.recursion


object Ex_2_5_FixPointS {

  import Ex_2_2_FixPointS._

  // ----------------------------------------------------------------------
  // 2.5.1 Discuss the (strict) Y Combinator version for Scala on Rosetta
  // ----------------------------------------------------------------------

  /*
   * This Rosetta Y Combinator code suggested for Scala (as of 2020-08-08):
   * [[https://rosettacode.org/wiki/Y_combinator#Scala]]
   */

  def Y[A, B](f: (A => B) => (A => B)): A => B = {
    case class W(wf: W => (A => B)) {
      def apply(w: W): A => B = wf(w)
    }
    val g: W => (A => B) = w => f(w(w))(_)
    g(W(g))
  }

  /*
   * Notes:
   * - W wraps a function: case class W(wf: W => (A => B))
   * - W emulates a self-calling function: ... def apply(w: W) ...
   * - then defines a wrapper function: val g = ... f(w(w)) ...
   * - then applies the wrapper function to itself: g(W(g))
   *
   * Review:
   * + This code unit-tests OK.
   * + Is above function Y a combinator? probably
   *   - W recursive type? yes         W bound as (type) parameter? no
   *   - W passed inside W? yes        W accessed inside W? yes: this.wf())
   *   - W instantiated inside W? no   W stored in a field inside W? no
   * - However...
   *   - W not extending Function1 but simulating one by defining `apply`
   *   - W is a custom data type: normally, not needed for mapping functions
   *   - W is used as a singleton: why a `class`? only instantiation: `W(g)`
   *   - W looks like a kludge: `g(W(g))`, what we want is `g(g)`
   *
   * ==> This code hard to understand.  Refactor to take apart...
   */

  // ----------------------------------------------------------------------
  // 2.5.2 Take apart the (strict) Y Combinator version from Rosetta
  // ----------------------------------------------------------------------

  /*
   * (Renamed) Rosetta Y Combinator: Try to simplify, address review items.
   */

  def fixStrictRosetta[A, B](f: (A => B) => (A => B)): A => B = {
    case class W(wf: W => (A => B)) {
      def apply(w: W): A => B = wf(w)
    }
    val g: W => (A => B) = w => f(w(w))(_)
    g(W(g))
  }

  /*
   * Refactoring:
   * - v1: remove cosmetics: apply(), parenthesis
   * - v2: make class a singleton, since just 1 `W` instance created!
   * - v3: remove singleton instance as parameter/argument, redundant
   * - v4: remove singleton instance, must evaluate lazily: val -> def
   */

  def fixStrictRosetta_v1[A, B](f: (A => B) => A => B): A => B = {
    case class W(wf: W => A => B)
    val g: W => A => B = w => f(w.wf(w))(_)
    g(W(g))
  }

  def fixStrictRosetta_v2[A, B](f: (A => B) => A => B): A => B = {
    object W {
      val wf: W.type => A => B = w => f(w.wf(w))(_)
    }
    W.wf(W)
  }

  def fixStrictRosetta_v3[A, B](f: (A => B) => A => B): A => B = {
    object W {
      val wf: A => B = f(wf)(_)
    }
    W.wf
  }

  def fixStrictRosetta_v4[A, B](f: (A => B) => A => B): A => B = {
    def wf: A => B = f(wf)(_)  // recursive use, val -> def
    wf
  }

  /*
   * Notes:
   * + v1: makes no attempt to "dress up" kludges: f(w.wf(w)), g(W(g))
   * + v2: still a combinator? yes, `w` is bound, no use of `this`
   * - v3: no longer combinator, recursive use, passing `wf` as argument
   *   hint: allowed because wf is a field, has value (null) before init
   * - v4: local wf must evaluate lazily, i.e., must be `def` or `lazy val`
   *   otherwise "forward reference extends over definition of value wf"
   *
   * ==> Better understanding of code, transition: combinator ~> HOF
   */

  // ----------------------------------------------------------------------
  // Ex 2.5.3: Evolve recursive fixpoint generator fixRStrict from Ex 2.3
  // ----------------------------------------------------------------------

  /*
   * Recursive HOF: Try dissolve recursive call fixRStrict(f)(_).
   */

  def fixRStrict[A, B](f: (A => B) => A => B): A => B =
    f(fixRStrict(f)(_))            // = f(a => fixRStrict(f)(a))

  /*
   * Refactoring:
   * - v1: make function local:       fixRStrict(f) ~> wf(f)
   * - v2: capture operator f:        wf(f) ~> wf
   * - v3: add outer eta expansion:   f(wf(_)) ~> f(wf(_))(_)
   * - v4: drop inner eta expansion:  f(wf(_))(_) ~>  f(wf)(_)
   */

  def fixLocalRStrict_v1[A, B](f: (A => B) => A => B): A => B = {
    def wf(f: (A => B) => A => B): A => B = f(wf(f)(_))
    wf(f)
  }

  def fixLocalRStrict_v2[A, B](f: (A => B) => A => B): A => B = {
    def wf: A => B = f(wf(_))      // = f(a => wf(a))
    wf
  }

  def fixLocalRStrict_v3[A, B](f: (A => B) => A => B): A => B = {
    def wf: A => B = f(wf(_))(_)   // can drop either eta (_) but not both
    wf
  }

  def fixLocalRStrict_v4[A, B](f: (A => B) => A => B): A => B = {
    def wf: A => B = f(wf)(_)      // = a => f(wf)(a)
    wf
  }

  /*
   * Notes:
   * + v2: shows inner `f` was never essential: f(wf(f)(_)) ~> f(wf(_))
   * + v3: shows "strict", must keep at least 1 eta, otherwise endless loop
   * + v4: no longer recursive _call_, passing method wf as argument (eta)
   * - v4: still not a combinator, wf is neither a parameter nor a value
   * - hint: local wf must evaluate lazily, i.e., must be `def` or `lazy val`
   *
   * ==> Better understanding of pattern: get a recursive X inside f: f(X)
   *     for expansion `f(f(...f(a => b)))`
   */

  // ----------------------------------------------------------------------
  // 2.5.4 Connect versions evolved from fixStrictRosetta and fixRStrict
  // ----------------------------------------------------------------------

  object ignore {

    /*
     * Identical:  fixLocalRStrict_v4 == fixStrictRosetta_v4
     */

    def fixLocalRStrict_v4[A, B](f: (A => B) => A => B): A => B = {
      def wf: A => B = f(wf)(_)
      wf
    }


    def fixStrictRosetta_v4[A, B](f: (A => B) => A => B): A => B = {
      def wf: A => B = f(wf)(_)
      wf
    }

    /*
     * Transformation:  HOF fixRStrict <=> fixStrictRosetta combinator
     */

    def fixRStrict[A, B](f: (A => B) => A => B): A => B =
      f(fixRStrict(f)(_))

    def fixStrictRosetta[A, B](f: (A => B) => (A => B)): A => B = {
      case class W(wf: W => (A => B)) {
        def apply(w: W): A => B = wf(w)
      }
      val g: W => (A => B) = w => f(w(w))(_)
      g(W(g))
    }
  }

  /*
   * Notes:
   * + v4: even simpler than fixRStrict? captures `f` as a closure
   *
   * ==> v4 was not an obvious guess, what other versions are we missing?
   */

  // ----------------------------------------------------------------------
  // 2.5.5 Discuss alternatives to using CRTP/F-bound types
  // ----------------------------------------------------------------------

  /*
   * Refresher:
   * The Y and Z combinators from Ex 2.4 based on an F-bound type are perfect:
   */

  def fix[A, B](f: (=> A => B) => A => B): A => B = {

    trait RecursiveF[F] extends (RecursiveF[F] => F)
    val g: RecursiveF[A => B] = w => f(w(w))
    g(g)
  }

  def fixStrict[A, B](f: (A => B) => A => B): A => B = {

    trait RecursiveF[F] extends (RecursiveF[F] => F)
    val g: RecursiveF[A => B] = w => f(w(w)(_))
    g(g)
  }

  /*
   * Nevertheless...
   *
   * - Replace F-bound type with Abstract Type Member + anonymous subclass?
   *   Should be possible as per:
   *   [[https://docs.scala-lang.org/tour/abstract-type-members.html]]
   *
   * - Use of `asInstanceOf` to cast away: W[W[W[_]]] -> W[W[_]] ?
   *   Possible but not better (some code experiments below).
   *   Note: Y in dynamically-typed languages does not need a recursive type.
   *
   * - Use of Scala3 `AnyKind` for subsuming W[W[W[_]]] -> W[AnyKind] ?
   *   Tried but no such luck.
   *   Intuition: `AnyKind` or `Any` no substitute for cast/recursive type.
   *
   * - Use of `implicit` to dress up kludge in fixStrictRosetta `g(W(g))` ?
   *   Might be possible to hide away `W`.
   *   But would further hurt readability.
   *
   * - The code/design space is vast: myriad ways to encode same function.
   */

  //
  // Code experiments: W[W[W[_]]] -> W[W[_]]
  //

  def fixStrict_v10[A, B](f: (A => B) => A => B): A => B = {
    //case class W[X <: W[W[_]]](wf: X => A => B)  // ok as well
    //case class W[X <: W[_]](wf: X => A => B)  // ok as well
    case class W[X](wf: X => A => B)
    val g: W[W[_]] => A => B = w => f(w.wf(w))(_)
    val h: W[W[W[_]]] = W(g)
    val i: W[W[_]] = h.asInstanceOf[W[W[_]]]  // cannot avoid
    val j: A => B = g(i)
    j
  }

  def fixStrict_v11[A, B](f: (A => B) => A => B): A => B = {
    case class W[X](wf: X => A => B)
    type G = W[Any]
    val g: G => A => B = w => f(w.wf(w))(_)
    val h: W[W[Any]] = W(g)
    val i: W[Any] = h.asInstanceOf[W[Any]]  // cannot avoid
    val j: A => B = g(i)
    j
  }

  // ----------------------------------------------------------------------
  // Recover factorial, fibonacci function versions for testing
  // ----------------------------------------------------------------------

  val factorial_fixStrictRosetta: Int => Int =
    fixStrictRosetta(factorial_FOpStrict)

  val fibonacci_fixStrictRosetta: Int => Int =
    fixStrictRosetta(fibonacci_FOpStrict)

  val factorial_fixStrictRosetta_v1: Int => Int =
    fixStrictRosetta_v1(factorial_FOpStrict)

  val fibonacci_fixStrictRosetta_v1: Int => Int =
    fixStrictRosetta_v1(fibonacci_FOpStrict)

  val factorial_fixStrictRosetta_v2: Int => Int =
    fixStrictRosetta_v2(factorial_FOpStrict)

  val fibonacci_fixStrictRosetta_v2: Int => Int =
    fixStrictRosetta_v2(fibonacci_FOpStrict)

  val factorial_fixStrictRosetta_v3: Int => Int =
    fixStrictRosetta_v3(factorial_FOpStrict)

  val fibonacci_fixStrictRosetta_v3: Int => Int =
    fixStrictRosetta_v3(fibonacci_FOpStrict)

  val factorial_fixStrictRosetta_v4: Int => Int =
    fixStrictRosetta_v4(factorial_FOpStrict)

  val fibonacci_fixStrictRosetta_v4: Int => Int =
    fixStrictRosetta_v4(fibonacci_FOpStrict)

  val factorial_fixLocalRStrict_v1: Int => Int =
    fixLocalRStrict_v1(factorial_FOpStrict)

  val fibonacci_fixLocalRStrict_v1: Int => Int =
    fixLocalRStrict_v1(fibonacci_FOpStrict)

  val factorial_fixLocalRStrict_v2: Int => Int =
    fixLocalRStrict_v2(factorial_FOpStrict)

  val fibonacci_fixLocalRStrict_v2: Int => Int =
    fixLocalRStrict_v2(fibonacci_FOpStrict)

  val factorial_fixLocalRStrict_v3: Int => Int =
    fixLocalRStrict_v3(factorial_FOpStrict)

  val fibonacci_fixLocalRStrict_v3: Int => Int =
    fixLocalRStrict_v3(fibonacci_FOpStrict)

  val factorial_fixLocalRStrict_v4: Int => Int =
    fixLocalRStrict_v4(factorial_FOpStrict)

  val fibonacci_fixLocalRStrict_v4: Int => Int =
    fixLocalRStrict_v4(fibonacci_FOpStrict)
}
