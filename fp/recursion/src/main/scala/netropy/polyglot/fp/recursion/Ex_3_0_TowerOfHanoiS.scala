package netropy.polyglot.fp.recursion

import collection.Seq  // not default, Scala 2.13


/** Encapsulates a data type and algorithm for the "Tower Of Hanoi" problem.
  *
  * Allows to parametrize unit tests over multiple ToH implementations.
  */
trait Ex_3_TowerOfHanoiS {

  /** Abstract type modeling a stack of items/discs of varying sizes. */
  type IStack <: Seq[Int]

  /** Creates a stack from given input. */
  def iStack(is: Int*): IStack

  /** Clones a stack. */
  def clone(is: IStack): IStack

  /** Whether the ToH condition is met: stack sorted by increasing size. */
  def isSorted(s: IStack): Boolean =
    // provide a non-strict default implementation
    Option(s)
      .filterNot(_.isEmpty)
      .map(_.view)
      .map(v => v.zip(v.tail).forall { case (x, y) => x < y })
      .getOrElse(true)

  /** Recursively moves the first `n` items from `src` via `tmp` to `dst`.
    *
    * Observes the "Tower of Hanoi" constraints:
    * - Only one disk can be moved at a time.
    * - Each move takes an upper disk and places it on top of another stack.
    * - No larger disk may be placed on top of a smaller disk.
    * Allows for input stacks to be non-empty, if consistent & accommodating.
    * @return the (src, tmp, dst) output stacks, ensured to be consistent
    */
  def move(n: Int, src: IStack, tmp: IStack, dst: IStack):
      (IStack, IStack, IStack)
}
