package netropy.polyglot.fp.recursion


object Ex_2_4_FixPointS {

  import Ex_2_2_FixPointS._

  // ----------------------------------------------------------------------
  // Ex 4.1: Convert `fixR` into a true combinator `fix`.
  // ----------------------------------------------------------------------

  /** Type for a functional `g` that can be applied onto itself `g(g): F`. */
  trait RecursiveF[F] extends (RecursiveF[F] => F)

  def fix[A, B](f: FOperator[A, B]): A => B = {

    val g: RecursiveF[A => B] =
      w => f(w(w))
    g(g)
  }

  def fixStrict[A, B](f: FOperatorStrict[A, B]): A => B = {

    val g: RecursiveF[A => B] =
      w => f(w(w)(_))  // must pass as eta (_) to avoid endless expansion
    g(g)
  }

  /*
   * Notes:
   *
   * - See Q&A on how to derive the Y combinator `fix` from the recursive
   *   function `fixR` from [[Ex_2_3_FixPointS]].
   *
   * - Offering both `fix` and `fixStrict` as complementary usage options.
   *   Their application is type-safe.  Same as [[Ex_2_3_FixPointS]].
   *
   * - The `_fixStrict` variant must internally pass the strict operator
   *   as a lambda (eta per `(_)`).  Same as [[Ex_2_3_FixPointS]].
   *
   * - Cannot provide the generic methods `fix[Strict]` as function values.
   *   Requires polymorphic function feature.  Same as [[Ex_2_3_FixPointS]].
   *
   * - See Q&A: A difficulty is to define a recursive type for the functional
   *   `g: G[F]` that allows for self-application: `type G[F] = G[F] => F`.
   *   A solution is to combine subtyping with generics for a CRTP pattern
   *   (or F-bound type).  See definition of `RecursiveF[F]` below.
   */

  // ----------------------------------------------------------------------
  // Ex 4.2: Recover `F` from an `FOp` using combinator `fix`.
  // ----------------------------------------------------------------------

  val factorial_fix_FOp: Int => Int =
    fix(factorial_FOp)

  val factorial_fixStrict_FOpStrict: Int => Int =
    fixStrict(factorial_FOpStrict)

  val fibonacci_fix_FOp: Int => Int =
    fix(fibonacci_FOp)

  val fibonacci_fixStrict_FOpStrict: Int => Int =
    fixStrict(fibonacci_FOpStrict)
}
