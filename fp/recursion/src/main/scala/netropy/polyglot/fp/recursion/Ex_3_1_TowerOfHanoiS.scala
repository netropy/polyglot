package netropy.polyglot.fp.recursion

import collection.immutable.List  // default, Scala 2.13


class Ex_3_1_TowerOfHanoiS extends Ex_3_TowerOfHanoiS {

  // ----------------------------------------------------------------------
  // Ex 3.1: Write a recursive ToH function over an immutable collection
  // ----------------------------------------------------------------------

  // List is well-suited for an immutable Stack
  final type IStack = List[Int]

  final def iStack(is: Int*): IStack = List(is: _*)

  final def clone(is: IStack): IStack = is.drop(-1)

  // some efficient, non-strict List recursion
  @annotation.tailrec
  final override def isSorted(s: IStack): Boolean = s match {
    case h0 :: h1 :: t => h0 < h1 && isSorted(h1 :: t)  // && is non-strict
    case h :: Nil => true
    case Nil => true
  }

  final def move(n: Int, src: IStack, tmp: IStack, dst: IStack):
      (IStack, IStack, IStack) = {

    // weakest set of necessary preconditions, most of them O(n)
    require(0 <= n && n <= src.size)
    require(isSorted(src))
    require(isSorted(tmp))
    require(isSorted(dst))
    require(n < 1 || dst.isEmpty || src(n - 1) < dst.head)  // O(n) for Lists
    require(n < 2 || tmp.isEmpty || src(n - 2) < tmp.head)  // O(n) for Lists

    // move zero or one item, O(1) for Lists
    n match {
      case 0 => (src, tmp, dst)
      case 1 => (src.tail, tmp, src.head :: dst)  // redundant, yet nice
      case _ =>
        val (s0, d0, t0) = move(n - 1, src, dst, tmp)
        val (t1, s1, d1) = move(n - 1, t0, s0.tail, s0.head :: d0)
        (s1, t1, d1)
    }

  } ensuring { case (s, t, d) =>

      // strongest set of post-conditions, most of them O(n)
      // have unmodified input stacks against which to compare output:
      val (a, b) = src.splitAt(n)
      val c = a ::: dst
      assert(s == b, s"s = $s != $b = b")
      assert(t == tmp, s"t = $t != $tmp = tmp")
      assert(d == c, s"d = $d != $c = c")
      // redundant, consistency implied by above post-, pre-conditions:
      //   assert(isSorted(s))
      //   assert(isSorted(t))
      //   assert(isSorted(d))
      true
  }
}
