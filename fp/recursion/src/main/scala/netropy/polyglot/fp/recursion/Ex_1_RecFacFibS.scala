package netropy.polyglot.fp.recursion

// TODO: adapt to Scala 2.13
//import collection.immutable.LazyList
//import collection.immutable.Stream


object Ex_1_RecFacFibS {

  // ----------------------------------------------------------------------
  // Ex 1.3 Check language support for recursive fields/values/closures.
  // ----------------------------------------------------------------------

  /*
   * Notes: Scala supports recursive fields, lazy values but not local values.
   */

  object Ignore {

    // OK: Recursive primitive fields compile (and work).
    val i: Int = i + 1

    // OK: Recursive lambda fields compile (and work).
    val f: Int => Int = a => f(a)

    // OK: Recursive lazy lambda fields compile (and work).
    lazy val g: Int => Int = a => g(a)

    // Closures have no `this`.
    //
    // compile error: Ex_1_RecFacFibS.Ignore.type does not take parameters
    // val g: Int => Int = a => this(a)

    def m = {

      // Local values/variables cannot be recursive, except for `lazy val`.
      //
      // compile error: forward reference extends over definition of
      // value/variable g
      // val g: Int => Int = a => g(a)
      // val g: Int => Int = (() => a => g(a))()
      // val g: Unit => Int => Int = _ => a => g(())(a)
      // val g: Int => Int = if (true) a => g(a) else identity
      // val g: Int => Int = { object W { val h: Int => Int = g(_) } ; W.h }

      // OK: Recursive lazy lambda fields compile (and work).
      lazy val h: Int => Int = a => h(a)
    }
  }

  /*
   * Note: Leaving out pre-conditions (n >= 0), post-conditions.
   * Not considering arithmetic overflow; type `Int` ok, here.
   */

  // ----------------------------------------------------------------------
  // Ex 1.1: Write factorial, fibonacci as iterative functions.
  // ----------------------------------------------------------------------

  def facI(n: Int): Int = {
    require(n >= 0)
    var r = 1
    for (i <- 1 to n)  // tricky loop count
      r *= i
    r
  }

  def fibI(n: Int): Int = {
    require(n >= 0)
    var (r0, r1) = (0, 1)
    if (n == 0)
      return r0

    for (i <- 1 until n) {  // tricky loop count
      val t = r0
      r0 = r1
      r1 += t
    }
    r1
  }

  // ----------------------------------------------------------------------
  // Ex 1.2: Write factorial, fibonacci as recursive functions.
  // ----------------------------------------------------------------------

  def facR(n: Int): Int =
    require(n >= 0)
    if (n == 0) 1 else n * facR(n - 1)

  def fibR(n: Int): Int =
    require(n >= 0)
    if (n < 2) n else fibR(n - 1) + fibR(n - 2)

  // ----------------------------------------------------------------------
  // Ex 1.5: Refactor as tail-recursive (or desugared) functions.
  // ----------------------------------------------------------------------

  /*
   * Note: Compile-support for tail-recursion in Scala: @annotation.tailrec
   * @tailrec only applicable to methods, not recursive function values
   */

  def facTR(n: Int): Int = {
    require(n >= 0)

    @annotation.tailrec
    def go(n: Int, r: Int): Int =
      if (n == 0) r else go(n - 1, n * r)
    go(n, 1)
  }

  def fibTR(n: Int): Int = {
    require(n >= 0)

    @annotation.tailrec
    def go(n: Int, r0: Int, r1: Int): Int = (n: @annotation.switch) match {
      case 0 => r1
      case 1 => r0
      case _ => go(n - 1, r0 + r1, r0)
    }
    go(n, 1, 0)
  }

  // ----------------------------------------------------------------------
  // Ex 1.6: Write factorial, fibonacci as fold operation on Range.
  // ----------------------------------------------------------------------

  def facF(n: Int): Int =
    (1 to n).fold(1)((v0, v1) => v0 * v1)

  def fibF(n: Int): Int =
    (1 to n).foldLeft((0, 1)){ case ((v0, v1), i) => (v1, v0 + v1) }._1

  // Scala3: no nested tuple deconstruction
  // (1 to n).foldLeft((0, 1))(((v0, v1), i) => (v1, v0 + v1))._1

  // ----------------------------------------------------------------------
  // Ex 1.6: Write factorial, fibonacci as corecursive, single-use Iterators.
  // ----------------------------------------------------------------------

  // Note: Since Iterators have internal state, we provide them as `def` (not
  // `val`) to generate them anew, despite the computation remaining the same.
  // For proper memoization, see use of LazyList below.

  /*
   * Using: direct corecursion with `++` concatenation of Iterators
   *
   * Note: unlike [Lazy]Lists, Iterators lack operators `::` `#::` or `+:`
   * workaround: lift a single value and concatenate with other iterator.
   */

  def fac0: Iterator[Int] = {
    def go(v: Int, i: Int): Iterator[Int] =
      Iterator.single(v) ++ go(v * i, i + 1)
    go(1, 1)
  }

  def fib0: Iterator[Int] = {
    def go(v0: Int, v1: Int): Iterator[Int] =
      Iterator.single(v0) ++ go(v1, v0 + v1)
    go(0, 1)
  }

  /*
   * Using: corecursion via combinator `iterate` taking a generator function
   *
   * Note: f is applied after start value, hence: { ... => ... }.map(_._1)
   */

  def fac1: Iterator[Int] =
    Iterator.iterate((1, 1)) { case (v, i) => (v * i, i + 1) }.map(_._1)

  def fib1: Iterator[Int] =
    Iterator.iterate((0, 1)) { case (v0, v1) => (v1, v0 + v1) }.map(_._1)

  /*
   * Using: corecursion via combinator `unfold` taking a state-update function
   *
   * Note: f is applied to the initial state, hence: (v, ...) => Some((v, ...))
   */

  def fac2: Iterator[Int] =
    Iterator.unfold((1, 1)) { case (v, i) => Some((v, (v * i, i + 1))) }

  def fib2: Iterator[Int] =
    Iterator.unfold((0, 1)) { case (v0, v1) => Some((v0, (v1, v0 + v1))) }

  // ----------------------------------------------------------------------
  // Ex 1.6: Write factorial, fibonacci as corecursive, collected LazyLists.
  // ----------------------------------------------------------------------

  /*
   * Using: direct corecursion with LazyList `#::` construction
   */

  def fac10: LazyList[Int] = {
    def go(v: Int, i: Int): LazyList[Int] = v #:: go(v * i, i + 1)
    go(1, 1)
  }

  def fib10: LazyList[Int] = {
    def go(v0: Int, v1: Int): LazyList[Int] = v0 #:: go(v1, v0 + v1)
    go(0, 1)
  }

  /*
   * Using: corecursion via combinator `iterate` taking a generator function
   *
   * Note: f is applied after start value, hence: { ... => ... }.map(_._1)
   */

  def fac11: LazyList[Int] =
    LazyList.iterate((1, 1)) { case (v, i) => (v * i, i + 1) }.map(_._1)

  def fib11: LazyList[Int] =
    LazyList.iterate((0, 1)) { case (v0, v1) => (v1, v0 + v1) }.map(_._1)

  /*
   * Using: corecursion via combinator `unfold` taking a state-update function
   *
   * Note: f is applied to the initial state, hence: (v, ...) => Some((v, ...))
   */

  def fac12: LazyList[Int] =
    LazyList.unfold((1, 1)) { case (v, i) => Some((v, (v * i, i + 1))) }

  def fib12: LazyList[Int] =
    LazyList.unfold((0, 1)) { case (v0, v1) => Some((v0, (v1, v0 + v1))) }

  // ----------------------------------------------------------------------
  // Ex 1.6: Write factorial, fibonacci as corecursive, memoized LazyLists.
  // ----------------------------------------------------------------------

  /*
   * Using: direct corecursion with LazyList `#::` construction
   */

  lazy val fac20: LazyList[Int] = {
    def go(v: Int, i: Int): LazyList[Int] = v #:: go(v * i, i + 1)
    go(1, 1)
  }

  lazy val fib20: LazyList[Int] = {
    def go(v0: Int, v1: Int): LazyList[Int] = v0 #:: go(v1, v0 + v1)
    go(0, 1)
  }

  /*
   * Using: corecursion via combinator `iterate` taking a generator function
   *
   * Note: f is applied after start value, hence: { ... => ... }.map(_._1)
   */

  lazy val fac21: LazyList[Int] =
    LazyList.iterate((1, 1)) { case (v, i) => (v * i, i + 1) }.map(_._1)

  lazy val fib21: LazyList[Int] =
    LazyList.iterate((0, 1)) { case (v0, v1) => (v1, v0 + v1) }.map(_._1)

  /*
   * Using: corecursion via combinator `unfold` taking a state-update function
   *
   * Note: f is applied to the initial state, hence: (v, ...) => Some((v, ...))
   */

  lazy val fac22: LazyList[Int] =
    LazyList.unfold((1, 1)) { case (v, i) => Some((v, (v * i, i + 1))) }

  lazy val fib22: LazyList[Int] =
    LazyList.unfold((0, 1)) { case (v0, v1) => Some((v0, (v1, v0 + v1))) }

  /*
   * Using: corecursion via combinators `zip` and `map`
   *
   * Note: construct the right stream with which to zip, then combine via map
   */

  lazy val fac30: LazyList[Int] =
    1 #:: fac30.zip(LazyList.from(1)).map { case (x, i) => x * i }

  lazy val fib30: LazyList[Int] =
    0 #:: 1 #:: fib30.zip(fib30.tail).map { case (x0, x1) => x0 + x1 }

  /*
   * Using: corecursion via combinators `zipWithIndex` and `map`
   *
   * Note: if the index stream is not right, can still adjust in map operation,
   * but less efficiently
   */

  lazy val fac31: LazyList[Int] =
    1 #:: fac31.zipWithIndex.map { case (x, i) => x * (i + 1) }

  lazy val fib31: LazyList[Int] =
    0 #:: 1 #:: fib31.zipWithIndex.map { case (x, i) => x + fib31(i + 1) }

  // Note: above code uses
  //   Scala3 tuple deconstruction:        (a, b) => a ... b
  //   Scala2+3 can use partial function:  { case (a, b) => a ... b }
  //            or use projectors:         ab => ab._1 ... ab._2
}
