package netropy.polyglot.fp.recursion

import org.junit.Test

class Ex_2_FixPointSTests
  extends Ex_1_RecFacFibSTests {

  @Test
  def ex_2_test_FOpStrict(): Unit = {

    import netropy.polyglot.fp.recursion.Ex_2_2_FixPointS._

    testFac(factorial_wfR_FOp, "factorial_wfR_FOp")
    testFac(factorial_wfR_FOpStrict, "factorial_wfR_FOpStrict")

    testFib(fibonacci_wfR_FOp, "fibonacci_wfR_FOp")
    testFib(fibonacci_wfR_FOpStrict, "fibonacci_wfR_FOpStrict")
  }

  @Test
  def ex_3_test_fixRStrict(): Unit = {

    import netropy.polyglot.fp.recursion.Ex_2_3_FixPointS._

    testFac(factorial_fixR_FOp, "factorial_fixR_FOp")
    testFac(factorial_fixRStrict_FOpStrict, "factorial_fixR_FOpStrict")

    testFib(fibonacci_fixR_FOp, "fibonacci_fixR_FOp")
    testFib(fibonacci_fixRStrict_FOpStrict, "fibonacci_fixR_FOpStrict")
  }

  @Test
  def ex_4_test_fixStrict(): Unit = {

    import netropy.polyglot.fp.recursion.Ex_2_4_FixPointS._

    testFac(factorial_fix_FOp, "factorial_fix_FOp")
    testFac(factorial_fixStrict_FOpStrict, "factorial_fix_FOpStrict")

    testFib(fibonacci_fix_FOp, "fibonacci_fix_FOp")
    testFib(fibonacci_fixStrict_FOpStrict, "fibonacci_fix_FOpStrict")
  }

  @Test
  def ex_5_test_fixAltStrict(): Unit = {

    import netropy.polyglot.fp.recursion.Ex_2_5_FixPointS._

    testFac(factorial_fixStrictRosetta,
      "factorial_fixStrictRosetta")
    testFib(fibonacci_fixStrictRosetta,
      "fibonacci_fixStrictRosetta")

    testFac(factorial_fixStrictRosetta_v1,
      "factorial_fixStrictRosetta_v1")
    testFib(fibonacci_fixStrictRosetta_v1,
      "fibonacci_fixStrictRosetta_v1")

    testFac(factorial_fixStrictRosetta_v2,
      "factorial_fixStrictRosetta_v2")
    testFib(fibonacci_fixStrictRosetta_v2,
      "fibonacci_fixStrictRosetta_v2")

    testFac(factorial_fixStrictRosetta_v3,
      "factorial_fixStrictRosetta_v3")
    testFib(fibonacci_fixStrictRosetta_v3,
      "fibonacci_fixStrictRosetta_v3")

    testFac(factorial_fixStrictRosetta_v4,
      "factorial_fixStrictRosetta_v4")
    testFib(fibonacci_fixStrictRosetta_v4,
      "fibonacci_fixStrictRosetta_v4")

    testFac(factorial_fixLocalRStrict_v1,
      "factorial_fixLocalRStrict_v1")
    testFib(fibonacci_fixLocalRStrict_v1,
      "fibonacci_fixLocalRStrict_v1")

    testFac(factorial_fixLocalRStrict_v2,
      "factorial_fixLocalRStrict_v2")
    testFib(fibonacci_fixLocalRStrict_v2,
      "fibonacci_fixLocalRStrict_v2")

    testFac(factorial_fixLocalRStrict_v3,
      "factorial_fixLocalRStrict_v3")
    testFib(fibonacci_fixLocalRStrict_v3,
      "fibonacci_fixLocalRStrict_v3")

    testFac(factorial_fixLocalRStrict_v4,
      "factorial_fixLocalRStrict_v4")
    testFib(fibonacci_fixLocalRStrict_v4,
      "fibonacci_fixLocalRStrict_v4")
  }
}
