package netropy.polyglot.fp.recursion

import org.junit.Test

import AssertExtJUnit4S.{assertResult, assertThrowing}

class Ex_1_RecFacFibSTests {

  import netropy.polyglot.fp.recursion.Ex_1_RecFacFibS._

  def testFac(f: Int => Int, s: String): Unit = {

    assertResult(1) { f(0) }
    assertResult(1) { f(1) }
    assertResult(2) { f(2) }
    assertResult(6) { f(3) }
    assertResult(5040) { f(7) }
  }

  def testFib(f: Int => Int, s: String): Unit = {

    assertResult(0) { f(0) }
    assertResult(1) { f(1) }
    assertResult(1) { f(2) }
    assertResult(2) { f(3) }
    assertResult(13) { f(7) }
  }

  @Test
  def ex_1_test_iterative(): Unit = {

    testFac(facI, "facI")
    testFib(fibI, "fibI")
  }

  @Test
  def ex_1_test_recursive(): Unit = {

    testFac(facR, "facR")
    testFib(fibR, "fibR")

    testFac(facF, "facF")
    testFib(fibF, "fibF")
  }

  @Test
  def ex_1_test_tailrecursive(): Unit = {

    testFac(facTR, "facTR")
    testFib(fibTR, "fibTR")
  }

  @Test
  def ex_1_test_corecursive_iterator(): Unit = {

    // Unlike LazyList, Iterator lacks a method apply(n) returning the nth
    // element (assuming there is any).  Equivalent expression:
    //   Iterator[A] => Int => A = i => n => i.drop(n).next()

    testFac(fac0.drop(_).next(), "facCoR")
    testFib(fib0.drop(_).next(), "fibTR")

    testFac(fac1.drop(_).next(), "facCoR")
    testFib(fib1.drop(_).next(), "fibTR")

    testFac(fac2.drop(_).next(), "facCoR")
    testFib(fib2.drop(_).next(), "fibTR")
  }

  @Test
  def ex_1_test_corecursive_lazylist(): Unit = {

    testFac(fac10(_), "facCoR")
    testFib(fib10(_), "fibTR")

    testFac(fac11(_), "facCoR")
    testFib(fib11(_), "fibTR")

    testFac(fac12(_), "facCoR")
    testFib(fib12(_), "fibTR")

    testFac(fac20(_), "facCoR")
    testFib(fib20(_), "fibTR")

    testFac(fac21(_), "facCoR")
    testFib(fib21(_), "fibTR")

    testFac(fac22(_), "facCoR")
    testFib(fib22(_), "fibTR")

    testFac(fac30(_), "facCoR")
    testFib(fib30(_), "fibTR")

    testFac(fac31(_), "facCoR")
    testFib(fib31(_), "fibTR")
  }
}
