package netropy.polyglot.fp.recursion

import org.junit.Assert.{assertEquals, assertThrows}
import org.junit.function.ThrowingRunnable

/** A bit of assert* sugar for JUnit4 tests. */
object AssertExtJUnit4S {

  def assertResult[A](exp: => A)(act: => A) =
    assertEquals(act, exp)

  def assertThrowing[T <: Throwable, A](ex: Class[T])(expr: => A): Unit =
    assertThrows(ex, () => expr)
}
