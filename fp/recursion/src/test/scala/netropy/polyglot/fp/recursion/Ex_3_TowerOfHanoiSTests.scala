package netropy.polyglot.fp.recursion

import org.junit.Test

import AssertExtJUnit4S.{assertResult, assertThrowing}

/* Abstract test suite for Tower Of Hanoi implementations.
 *
 * For code simplicity and for unit-test framework recognizing tests, easiest
 * - not to parametrize this test suite over IStack, SeqFactory, move etc
 * + but to self-annotate tests and mixin to implementation (a.k.a. "cake").
 */
trait Ex_3_TowerOfHanoiSTests {
  this: Ex_3_TowerOfHanoiS =>

  @Test
  def ex_3_test_illegal_n: Unit = {
    assertThrowing(classOf[IllegalArgumentException]) {
      move(1, iStack(), iStack(), iStack())
    }
    assertThrowing(classOf[IllegalArgumentException]) {
      move(-1, iStack(), iStack(), iStack())
    }
  }

  @Test
  def ex_3_test_illegal_src: Unit = {
    assertThrowing(classOf[IllegalArgumentException]) {
      move(1, iStack(2, 1), iStack(), iStack())
    }
  }

  @Test
  def ex_3_test_illegal_tmp: Unit = {
    assertThrowing(classOf[IllegalArgumentException]) {
      move(1, iStack(1), iStack(3, 2), iStack())
    }
    assertThrowing(classOf[IllegalArgumentException]) {
      move(2, iStack(1, 2), iStack(1), iStack())
    }
  }

  @Test
  def ex_3_test_illegal_dst: Unit = {
    assertThrowing(classOf[IllegalArgumentException]) {
      move(1, iStack(1), iStack(), iStack(3, 2))
    }
    assertThrowing(classOf[IllegalArgumentException]) {
      move(2, iStack(1, 2), iStack(), iStack(1))
    }
  }

  @Test
  def ex_3_test_s0: Unit = {
    val (s0, t0, d0) = (iStack(), iStack(), iStack())
    val (s1, t1, d1) = (clone(s0), clone(t0), clone(d0)) // for mutable coll.
    val (s2, t2, d2) = move(s0.size, s1, t1, d1)
    assertResult(s0) { s2 }
    assertResult(t0) { t2 }
    assertResult(d0) { d2 }
  }

  def ex_3_test_s(n: Int): Unit = {
    val nums = 1 to n
    val (s0, t0, d0) = (iStack(nums: _*), iStack(99), iStack(99))
    for (i <- nums) {
      val (s1, t1, d1) = (clone(s0), clone(t0), clone(d0))
      val (s2, t2, d2) = move(i, s1, t1, d1)
      val (a, b) = s0.splitAt(i)
      assertResult(b) { s2 }
      assertResult(t0) { t2 }
      assertResult(a ++ d0) { d2 }
    }
  }

  @Test
  def ex_3_test_s1: Unit = {
    ex_3_test_s(1)
  }

  @Test
  def ex_3_test_s2: Unit = {
    ex_3_test_s(2)
  }

  @Test
  def ex_3_test_s3: Unit = {
    ex_3_test_s(3)
  }

  @Test
  def ex_3_test_s4: Unit = {
    ex_3_test_s(4)
  }

  @Test
  def ex_3_test_s9: Unit = {
    ex_3_test_s(9)
  }
}

// Tests suites for Tower Of Hanoi implementations.

class Ex_3_1_TowerOfHanoiSTests
    extends Ex_3_1_TowerOfHanoiS with Ex_3_TowerOfHanoiSTests

class Ex_3_2_TowerOfHanoiSTests
    extends Ex_3_2_TowerOfHanoiS with Ex_3_TowerOfHanoiSTests
