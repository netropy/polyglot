package netropy.polyglot.fp.recursion;

import java.util.function.Function;

import org.junit.Test;

import static netropy.polyglot.fp.recursion.AssertExtJUnit4J.*;

public class Ex_1_RecFacFibJTests {

    /*
     * Note:
     *
     * Better not to define a (test) function over a specialized type (e.g.,
     * IntUnaryOperator) in contravariant position.  Requires conversions
     * from general type Function<Integer, Integer>.
     *
     * public void testFactorial(IntUnaryOperator f, String s) {...}
     * public void testFibonacci(IntUnaryOperator f, String s) {...}
     */

    public void testFactorial(Function<Integer, Integer> f, String s) {

        assertResult(1, f.apply(0));
        assertResult(1, f.apply(1));
        assertResult(2, f.apply(2));
        assertResult(6, f.apply(3));
        assertResult(5040, f.apply(7));
    }

    public void testFibonacci(Function<Integer, Integer> f, String s) {

        assertResult(0, f.apply(0));
        assertResult(1, f.apply(1));
        assertResult(1, f.apply(2));
        assertResult(2, f.apply(3));
        assertResult(13, f.apply(7));
    }

    @Test
    public void ex_1_test_iterative() {

        testFactorial(Ex_1_RecFacFibJ::factorialI, "factorialI");
        testFibonacci(Ex_1_RecFacFibJ::fibonacciI, "fibonacciI");
    }

    @Test
    public void ex_1_test_recursive() {

        testFactorial(Ex_1_RecFacFibJ::factorialR, "factorialR");
        testFibonacci(Ex_1_RecFacFibJ::fibonacciR, "fibonacciR");

        // convert to Function<Integer, Integer>
        testFactorial(n -> Ex_1_RecFacFibJ.factorialRV.applyAsInt(n),
                      "factorialRV");
        testFibonacci(n -> Ex_1_RecFacFibJ.fibonacciRV.applyAsInt(n),
                      "fibonacciRV");
    }

    @Test
    public void ex_1_test_desugared() {

        testFactorial(Ex_1_RecFacFibJ::factorialDS, "factorialDS");
        testFibonacci(Ex_1_RecFacFibJ::fibonacciDS, "fibonacciDS");
    }
}
