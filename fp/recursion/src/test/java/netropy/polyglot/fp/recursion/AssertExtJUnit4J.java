package netropy.polyglot.fp.recursion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import org.junit.function.ThrowingRunnable;

/** A bit of assert* sugar for JUnit4 tests. */
public class AssertExtJUnit4J {

    static public <A> void
    assertResult(A exp, A act) {
        assertEquals(exp, act);
    }

    static public <T extends Throwable> void
    assertThrowing(Class<T> ex, ThrowingRunnable act) {
	assertThrows(ex, act);
    }
}
