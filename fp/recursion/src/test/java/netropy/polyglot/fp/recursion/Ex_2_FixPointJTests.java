package netropy.polyglot.fp.recursion;

import org.junit.Test;

public class Ex_2_FixPointJTests
    extends Ex_1_RecFacFibJTests {

    @Test
    public void ex_2_test_FOpStrict() {

        testFactorial(Ex_2_2_FixPointJ::factorial_wfR_FOpStrict,
                      "factorial_wfR_FOpStrict");
        testFibonacci(Ex_2_2_FixPointJ::fibonacci_wfR_FOpStrict,
                      "fibonacci_wfR_FOpStrict");
    }

    @Test
    public void ex_3_test_fixRStrict_FOpStrict () {

        testFactorial(Ex_2_3_FixPointJ.factorial_fixRStrict_FOpStrict,
                      "factorial_fixRStrict_FOpStrict");
        testFibonacci(Ex_2_3_FixPointJ.fibonacci_fixRStrict_FOpStrict,
                      "fibonacci_fixRStrict_FOpStrict");
    }

    @Test
    public void ex_4_test_fixStrict_FOpStrict () {

        testFactorial(Ex_2_4_FixPointJ.factorial_fixStrict_FOpStrict,
                      "factorial_fixStrict_FOpStrict");
        testFibonacci(Ex_2_4_FixPointJ.fibonacci_fixStrict_FOpStrict,
                      "fibonacci_fixStrict_FOpStrict");
    }
}
