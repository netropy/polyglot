name := "polyglot.fp"
organization := "netropy"
description := "Functional Programming, code experiments, exercises"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

// Multi-Project Build - Aggregation

// 2023-01: Why does sbt need this re-declaration, here?  Different scope?
// Otherwise, when building from parent dir:
//   No project 'fp' in 'file:/home/mz/@mzmac/gitlab/polyglot/'
lazy val fp = (project in file("."))

lazy val cats = (project in file("cats"))
lazy val effects = (project in file("effects"))
lazy val lists = (project in file("lists"))
lazy val recursion = (project in file("recursion"))

lazy val root = (project in file("."))
  .aggregate(
    cats,
    effects,
    lists,
    recursion
  )
