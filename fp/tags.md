TAGS: Problem: \
lambda, closure, recursion, tail-recursion, iteration, factorial, fibonacci,
higher-order function, y combinator, fixpoint, lambda calculus,
applicative order, normal order, evaluation, strict, non-strict, eager, lazy,
by name, by value, tower of hanoi, mutable collections, immutable collections,
Reader Monad, State Monad

TAGS: Scala: \
scala3, scala2.13, lambda, closure, @annotation.tailrec, require, ensuring,
recursive method, recursive field, lazy val, abstract type, cake pattern,
collection.Seq, collection.immutable.List, collection.mutable.Stack

TAGS: Java: \
java11, lambda, closure, recursive method
