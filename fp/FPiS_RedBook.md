# polyglot/fp

### Functional Programming in Scala: Exercises, Code Experiments, Notes

A prolific _starting point_ for exercises and experiments have been
- the _"Red Book"_
  [Functional Programming in Scala](https://www.manning.com/books/functional-programming-in-scala),
  written by Paul Chiusano and Rúnar Bjarnason,
- along with its _"Blue Book"_
  [Companion Booklet to FPiS](http://blog.higher-order.com/blog/2015/03/06/a-companion-booklet-to-functional-programming-in-scala/)
  with chapter notes, hints, and answers.

BTW, thanks for signing the book, Paul and Rúnar @ Scale By the Bay:
![RedBookSigned](FPiS_RedBook_Signed2017.png "@ Scale By the Bay, 2017")

The exercises from the Red Book's first 9 chapters are also known as
[Scala Exercises](https://www.scala-exercises.org/fp_in_scala/),
published as by [47 Degrees](https://www.47deg.com) with the same numeration
(plus minor changes).

Resources: _Blue Book_ and _Scala Exercises_ repositories \
<https://github.com/fpinscala/fpinscala>,
<https://github.com/scala-exercises/scala-exercises>

---

The exercises and experiments here in [fp](../fp) and in [../adsp](../adsp),
expand on the _Red Book_ (chapters 03 to 12).  Occasionally, the given code
solutions differ from the answers in the _Blue Book_:
- [Recursion, Higher Order Functions, chapter 2 (incomplete)](./Recursion/ex_1.q.recursion.md)
- [LinkedLists, chapter 3 (with 10, 11, 12)](../adsp/LinkedLists/README.md)
- [Option, Either, Try, chapter 4 (with 10, 11, 12)]
- [LazyLists, Streams, chapter 5 (with 10, 11, 12)]
- [Parser Combinators, chapter 9]
- [LazyLists, Streams, chapter 5 (with 10, 11, 12)]
- [An alternative modeling of core FP types, chapters 10, 11, 12]

Some of the Red Book's core lessons are summarized and (critically) discussed
in [../guides](../guides):
- [When to use Functional/Persistent/Immutable Data Structures?](../guides/ImmutableDataStructures.md)
- [null/nil/None, Option type, Coalescing Operators, and Nil Punning](../guides/NullNilNone_Option_CoalescingOperators_NilPunning.md)
- [Scala, Java Method Signature Design](../guides/Scala_Java_MethodSignatureDesign.md)

Motivation:
- Adroit knowledge of core functional data structures.
- Understanding of usage, expressivenes and implementation of the core
  combinators.
- Grasp of core FP types (Monoid, Foldable, Functor, Applicative, Traversal,
  and Monad).
- Data type design with Scala's OOP vs FP vs GADT Scala3 language features.

[Up](README.md)
