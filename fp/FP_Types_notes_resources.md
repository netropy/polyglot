# polyglot/fp

### Functional Programming: Code Experiments, Exercises, Notes

Motivation: _Diagram_ summarizing the features and relationships between the
_major_ FP typeclasses.

Anybody done some "cheat sheets" with types, operations, and laws?
- Haskell: [Typeclassopedia](https://wiki.haskell.org/Typeclassopedia)
- scalaz: color-coded
  [overview diagram](https://github.com/Zelenya/scalaz-typeclasses-diagram)
  by [Zelenya](https://github.com/Zelenya)
- CATS: color-coded
  [overview diagram (older CATS version)](https://github.com/tpolecat/cats-infographic)
  by [tpolecat](https://github.com/tpolecat)
- CATS: no diagram on the [website](https://typelevel.org/cats/)
  or among the [API docs](https://typelevel.org/cats/api/cats/index.html)?
  (closed [request](https://github.com/typelevel/cats/issues/95))

Therefore: 10 Core FP types, _simplified to fit on a single page_
- [Refcard FP Types + Operations](Refcard_FP_types_ops.png)
- [Refcard FP Types + Laws](Refcard_FP_types_laws.png)

Sources:
[CATS Glossary](https://typelevel.org/cats/nomenclature.html),
[Functional Programming in Scala](https://www.manning.com/books/functional-programming-in-scala),
[learning Scalaz](http://eed3si9n.com/learning-scalaz/),
(simplifications + errors by myself).

[Up](README.md)
