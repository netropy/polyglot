# polyglot/fp/LinkedLists

### 2 (List) Recursion]

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/)

#### 2.1 Write functions `show`, `mkString`.

List traversal with (associative) string concatenation.  Experiment with
recursive and tail-recursive algorithms (as practical) and indicate versions
by function name suffixes `_{r,tr}`.

Try: 1) map to String + concat, 2) use a stateful builder object.


```
  /** Displays list elements in a string like "[]", "[a,]", "[a,b,]" etc. */
  def show[A]
      (as: List[A]): String

  /** Displays list elements in a string using start, end, and separator. */
  def mkString[A]
      (as: List[A], start: String, sep: String, end: String): String
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_2_1.show_mkString.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_2_1.show_mkString.scala)

#### 2.2 Write functions `setHead`, `setTail`, `drop`, `dropWhile`.

List manipulation with data sharing.  Experiment with recursive and
tail-recursive algorithms (as practical) and indicate versions by function
name suffixes `_{r,tr}`.

```
  /** Replaces the first element of a non-empty list. */
  def setHead[A]
      (as: List[A], head: A): List[A]

  /** Replaces the rest of a non-empty list. */
  def setTail[A]
      (as: List[A], tail: List[A]): List[A]

  /** Drops the first `n` elements from a list with `>= n` elements. */
  def drop[A]
      (as: List[A], n: Int): List[A]

  /** Drops the first elements from a list while satisfying a predicate. */
  def dropWhile[A]
      (as: List[A], p: A => Boolean): List[A]
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_2_2.setHead_setTail_drop_dropWhile.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_2_2.setHead_setTail_drop_dropWhile.scala)

#### 2.3 Write functions `last`, `init`, `take`, `takeWhile`, `filter`

List construction without possibility for data sharing.  Experiment with
recursive and tail-recursive algorithms (as practical) and indicate versions
by function name suffixes `_{r,tr}`.

```
  /** Returns the last element of a non-empty list. */
  def last[A]
      (as: List[A]): A

  /** Returns the initial part of a list without its last element. */
  def init[A]
      (as: List[A]): List[A]

  /** Takes the first `n` elements from a list with `>= n` elements. */
  def take[A]
      (as: List[A], n: Int): List[A]

  /** Returns a list retaining only initial elements which match p. */
  def takeWhile[A]
      (as: List[A])
      (p: A => Boolean): List[A]

  /** Returns a list of only those elements that satisfy the predicate. */
  def filter[A]
      (as: List[A], p: A => Boolean): List[A]
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_2_3.last_init_take_takeWhile_filter.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_2_3.last_init_take_takeWhile_filter.scala)

#### 2.4 Write functions `append`, `reverseAppend`, `reverse`, `append`, `copy`.

List construction with order reversal/preservation.  Experiment with recursive
and tail-recursive algorithms (as practical) and indicate versions by function
name suffixes `_{r,tr}`.

```
  /** Returns the concatenation of two lists (`a0 ::: a1`). */
  def append[A]
      (a0: List[A], a1: List[A]): List[A]

  /** Returns the reversed and appended list (`reversed(a0) ::: a1`). */
  def reverseAppend[A]
      (a0: List[A], a1: List[A]): List[A]

  /** Returns a list of the elements in reversed order. */
  def reverse[A]
      (as: List[A]): List[A]

  def append[A]
      (a0: List[A], a1: List[A]): List[A]

  /** Returns a shallow copy of a list. */
  def copy[A]
      (as: List[A]): List[A]
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_2_4.reverse_append_copy.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_2_4.reverse_append_copy.scala)

#### 2.5 Write functions `length`, `sum`, `product`, `toStrings`, `mapIncr`.

Recursive Map-/Reduce-style functions, some with order preservation.
Experiment with recursive and tail-recursive algorithms (as practical) and
indicate versions by function name suffixes `_{r,tr}`.

```
  /** Returns the length of the list. */
  def length[A]
      (as: List[A]): Int

  /** Returns the sum of the list of [[Int]]. */
  def sum
      (as: List[Int]): Int

  /** Returns the [[BigInt]] product of the list of [[Int]]. */
  def product
      (as: List[Int]): BigInt

  /** Replaces each element in the list with its [[String]] representation. */
  def toStrings[A]
      (as: List[A]): List[String]

  /** Increments each value in the list of [[Int]]s by `1`. */
  def mapIncr
      (is: List[Int]): List[Int]
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_2_5.length_sum_product_toStrings_mapIncr.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_2_5.length_sum_product_toStrings_mapIncr.scala)

#### 2.6 Write functions `exists`, `forall`, `count`, `find`, `get`.

Map-/Reduce-style functions with the opportunity of early termination.
Experiment with recursive and tail-recursive algorithms (as practical) and
indicate versions by function name suffixes `_{r,tr}`.

```
  /** Returns whether at least one element satisfies the predicate. */
  def exists_fl[A]
      (as: List[A])
      (p: A => Boolean): Boolean

  /** Returns whether all elements satisfies the predicate. */
  def forall_fl[A]
      (as: List[A])
      (p: A => Boolean): Boolean

  /** Returns the number of elements that satisfy the predicate. */
  def count_f[A]
      (as: List[A])
      (p: A => Boolean): Long

  /** Returns any element that matches the predicate, if one exists. */
  def find_fl[A]
      (as: List[A])
      (p: A => Boolean): Option[A]

  /** Returns the element at the index of the list, if within bounds. */
  def get_fl[A]
      (as: List[A], i: Int): Option[A]
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_2_6.exists_forall_count_find_get.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_2_6.exists_forall_count_find_get.scala)

#### 2.7 Implement functions `startsWith`, `endsWith`, `hasSubsequence`.

Prefix/Suffix/Subsequence-Test functions with early termination.  Experiment
with recursive and tail-recursive algorithms (as practical) and indicate
versions by function name suffixes `_{r,tr}`.

How can these functions be implemented _lazily_?

```
  /** Returns whether a list starts with the same elements as another list. */
  def startsWith[A]
      (as: List[A], prefix: List[A]): Boolean

  /** Returns whether a list ends with the same elements as another list. */
  def endsWith[A]
      (as: List[A], suffix: List[A]): Boolean

  /** Returns whether a list contains another list as a subsequence. */
  def hasSubsequence[A]
      (as: List[A], sub: List[A]): Boolean
```

Note that the Scala
[`collection.Seq`](https://www.scala-lang.org/api/current/scala/collection/Seq.html)
provides an additional, interesting set of functions for iterating over
subsequences:
```
  /** Iterates over the inits of this iterable collection. */
  def inits: Iterator[Seq[A]]

  /** Iterates over the tails of this iterable collection. */
  def tails: Iterator[Seq[A]]

  /** Iterates over combinations. */
  def combinations(n: Int): Iterator[Seq[A]]

  /** Iterates over distinct permutations. */
  def permutations: Iterator[Seq[A]]

  /** Partitions elements in fixed size iterable collections. */
  def grouped(size: Int): Iterator[Seq[A]]

  /** Groups elements in fixed size blocks by a "sliding window". */
  def sliding(size: Int, step: Int): Iterator[Seq[A]]
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_2_7.startsWith_endsWith_hasSubsequence.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_2_7.startsWith_endsWith_hasSubsequence.scala)

#### 2.8 Write function `zipAdd`.

Zip-like (Traversal) Functions.  Experiment with recursive and tail-recursive
algorithms (as practical) and indicate versions by function name suffixes
`_{r,tr}`.

```
  /** Adds corresponding elements of two Int lists, while there are any. */
  def zipAdd
      (as0: List[Int], as1: List[Int]): List[Int]
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_2_8.zipAdd.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_2_8.zipAdd.scala)

[Up](./README.md)
