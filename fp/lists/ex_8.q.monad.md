# polyglot/fp/LinkedLists

### 8 (List) Monad Functions

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/)

#### 8.1 Implement combinators `join`, `flatMap`, `compose` in terms of _fold{Right,Left}_.

Notes:
- `join` is better known (and better named) in Scala as `flatten`.
- `flatMap` is also known (and poorly named) in Haskell as `bind`.
- `compose` seems less used (a.k.a. Kleisli/Arrow composition); it nicely
  states the associative law for monads:
```
   compose(compose(f, g), h) == compose(f, compose(g, h))
```

Discuss how these combinators relate to each other.

Provide both, `fold{Right,Left}`-based implementations (with list reversal);
discuss whether difference.

```
  def join_{fr,fl}[A]
      (aas: List[List[A]]): List[A]

  /** Maps each list element to a list and flattens the result. */
  def flatMap_{fr,fl}[A, B]
      (as: List[A])
      (f: A => List[B]): List[B]

  /** Sequentially composes two flattened list mappings. */
  def compose_{fr,fl}[A, B, C]
      (f: A => List[B])
      (g: B => List[C]): A => List[C]
```

Resources: \
<https://typelevel.org/cats/typeclasses/monad.html>,
<https://typelevel.org/cats/api/cats/Monad.html>

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_8_1.join_flatMap_compose.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_8_1.join_flatMap_compose.scala)

#### 8.2 Write functions `mproduct`, `filter` in terms of _join_, _flatMap_, _compose_.

Shows that Monads can insert or drop list elements.
```
  /** Pairs each list element with each of its mapped values. */
  def mproduct_fm[A, B]
      (as: List[A])
      (f: A => List[B]): List[(A, B)]
```
For signature of function `filter`, see [ex 2.3](ex_2.q.recursion.md).

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_8_2.mproduct_filter.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_8_2.mproduct_filter.scala)

#### 8.3 Rewrite combinator `map` in terms of _flatMap_, _compose_.

Discuss why not also in terms of _join_.  Shows that the (list) Monad is also
a Functor.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_8_3.map.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_8_3.map.scala)

#### 8.4 Rewrite combinators `join`, `flatMap`, `compose` in terms of each other.

Only 3 rotating implementations needed to show equivalence (yet, writing each
combinator in terms of the other two makes a good exercise).

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_8_4.join_flatMap_compose.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_8_4.join_flatMap_compose.scala)

#### 8.5 Rewrite combinators `tuple`, `map2`, `ap` in terms of _flatMap_.

Shows that the (list) Monad is also an Applicative.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_8_5.tuple2_map2_ap.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_8_5.tuple2_map2_ap.scala)

[Up](./README.md)
