# polyglot/fp/LinkedLists

### 4 (List) Reducible API

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/)

#### 4.1 Implement combinators `reduce`, `reduceOption` in terms of a _fold_.

Which _fold_ combinator is best suited for _reduce_?  Provide overloaded
_reduce_ variants for _Cons_ besides _List_?

```
  /** Applies associative operator to elements of non-empty list.
    * @throws [[IllegalArgumentException]] if `as` is Nil
    */
  def reduce_f[A, A1 >: A]
      (as: List[A])
      (op: (A1, A1) => A1): A1

  /** Applies associative operator to elements of list. */
  def reduceOption_f[A, A1 >: A]
      (as: List[A])
      (op: (A1, A1) => A1): Option[A1]
```

No need here for directional variants `reduce{Left,Right}[Option]`.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_4_1.reduce.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_4_1.reduce.scala)

#### 4.2 Rewrite 3.x functions in terms of pure _reduce_, where applicable.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_4_2.sum.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_4_2.sum.scala)

[Up](./README.md)
