# polyglot/fp/LinkedLists

### 3 (List) Foldable API

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/)

#### 3.1 Implement combinators `foldLeft_{tr,r}`, `foldRight_{tr,r}`.

Implement the directional fold combinators _foldLeft_, _foldRight_:
- tail-recursive `foldLeft_tr`
- non-tail-recursive `foldRight_r`
- `foldRight_tr` in terms of tail-recursive `foldLeft_tr` (+prior functions)
- `foldLeft_r` in terms of non-tail-recursive `foldRight_r` (+prior functions)

```
  /** Applies binary operator to start value, list elements, left->right. */
  def foldLeft_{tr,r}[A, B]
      (z: B, as: List[A])
      (op: (B, A) => B): B

  /** Applies binary operator to start value, list elements, left<-right. */
  def foldRight_{tr,r}[A, B]
      (as: List[A], z: B)
      (op: (A, B) => B): B
```

Discuss the efficiency and stack-safety aspects of both `foldRight_{tr,r}`
versions.

Resources: \
["foldRight broken for large lists #3295"](https://github.com/scala/bug/issues/3295), \
["List.foldRight throws StackOverflowError #2818"](https://github.com/scala/bug/issues/2818), \
["Port List optimizations to the strawman."](https://github.com/scala/scala/commit/d9885d562ab6f17defb05c420f898e4a5c9d624a)

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_3_1.foldLeft_foldRight.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_3_1.foldLeft_foldRight.scala)

#### 3.2 Implement combinators `foldMap_{fl,fr}`, `fold_{fl,fr}` in terms of _fold{Left,Right}_.

Implement the non-directional fold combinators _foldMap_, _fold_ as variants:
- `foldMap_{fl,fr}` in terms of `fold{Left,Right}_tr`
- `fold_{fl,fr}` in terms of `foldMap_{fl,fr}`

Why would it be useful for a library to offer _both_, internally left- and
right-folding, stack-safe versions of each, _foldMap_ and _fold_?

```
  /** Maps each element to a Monoid B(f, op) and combines the results. */
  def foldMap_{fl,fr}[A, B]
      (as: List[A])
      (f: A => B)
      (z: B, op: (B, B) => B): B

  /** Applies the associative operator to list elements, neutral element. */
  def fold_{fl,fr}[A, A1 >: A]
      (as: List[A], z: A1)
      (op: (A1, A1) => A1): A1
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_3_2.fold_foldMap.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_3_2.fold_foldMap.scala)

#### 3.3 Rewrite functions `length`, `sum`, `product`, `toStrings`, `mapIncr`, `mkString` in terms of a _fold_.

See Ex 2 for signatures.

Which of these Map-/Reduce-style functions is best served by which fold
combinator?  Use function name suffixes `_{fl,fr,f}` to indicate directional
variant, and `_lazy` to indicate early return or non-strictness, see
[ex_3_x](ex_3_x.fpis.foldRight.md).


[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_3_3.toStrings_length_sum_product_mapIncr_mkString.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_3_3.toStrings_length_sum_product_mapIncr_mkString.scala)

#### 3.4 Rewrite functions `reverseAppend`, `reverse`, `append`, `copy` in terms of a _fold_.

See Ex 2 for signatures.

Which of these List-Reconstruction operations is best served by which fold
combinator?  Use function name suffixes `_{fl,fr,f}` to indicate directional
variant, and `_lazy` to indicate early return or non-strictness, see
[ex_3_x](ex_3_x.fpis.foldRight.md).

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_3_4.reverse_append_copy.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_3_4.reverse_append_copy.scala)

#### 3.5 Rewrite functions `drop`, `dropWhile`, `take`, `takeWhile`, `last`, `init`, `filter` in terms of a _fold_.

See Ex 2 for signatures.  Which of these Removal/Insertion list operations is
best served by which fold combinator?  Use function name suffixes `_{fl,fr,f}`
to indicate directional variant, and `_lazy` to indicate early return or
non-strictness, see
[ex_3_x](ex_3_x.fpis.foldRight.md).

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_3_5.drop_dropWhile_take_takeWhile_last_init_filter.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_3_5.drop_dropWhile_take_takeWhile_last_init_filter.scala)

#### 3.6 Rewrite functions `exists`, `forall`, `count`, `find`, `get` in terms of a _fold_.

See Ex 2 for signatures.  How well can these _lazy_ MapReduce-like functions
be implemented in terms of a fold combinator?  Use function name suffixes
`_{fl,fr,f}` to indicate directional variant, and `_lazy` to indicate early
return or non-strictness, see
[ex_3_x](ex_3_x.fpis.foldRight.md).

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_3_6.exists_forall_count_find_get.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_3_6.exists_forall_count_find_get.scala)

#### 3.7 Rewrite functions `startsWith`, `endsWith` in terms of a _fold_.

See Ex 2 for signatures.  How well can these _lazy_ prefix/suffix-test
functions be implemented in terms of a fold combinator?  Why seems
`hasSubsequence` not suited for fold?  Use function name suffixes `_{fl,fr,f}`
to indicate directional variant, and `_lazy` to indicate early return or
non-strictness, see
[ex_3_x](ex_3_x.fpis.foldRight.md).

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_3_7.startsWith_endsWith.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_3_7.startsWith_endsWith.scala)

#### 3.8 (Hard:) Implement combinators `foldLeft_{fr,fm}`, `foldRight_{fl,fm}` purely in terms of _fold{Right,Left,Map}_.

Questions:
- When analyzing exercises 3.1 and 3.4, have foldLeft/foldRight been _truly_
  expressed only in terms of the other?
- How can a list be reversed solely with foldRight?
- If not using an intermediate collection for list reversal, what other
  abstraction can be used to refer to the elements of a list?


Implement:
- _foldRight\_fl_, _foldLeft\_fr_ solely in terms of _foldLeft/foldRight_,
  respectively
- _foldRight\_fm_, _foldLeft\_fm_ solely in terms of _foldMap_

Discuss efficiency.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_3_8.foldLeft_foldRight.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_3_8.foldLeft_foldRight.scala)

[Up](./README.md)
