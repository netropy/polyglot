# polyglot/fp/lists

### Linked Lists: Functional Programming Exercises, Code Experiments, Notes

Motivation: Understanding of
- _functional LinkedLists_ with inherited combinators from Foldable, Functor,
  Applicative, Traversal, and Monad;
- the usage, expressivenes and implementation of the core combinators;
- data type design with Scala's OOP vs FP vs GADT Scala3 language features.

This project consists of a _list of exercises_ that expand on chapter 3 (and
10, 11, 12) of the _"Red Book"_,
[Functional Programming in Scala](https://www.manning.com/books/functional-programming-in-scala),
written by Paul Chiusano and Rúnar Bjarnason.

The exercises from the first 9 chapters are also known as
[Scala Exercises](https://www.scala-exercises.org/fp_in_scala/),
published as by [47 Degrees](https://www.47deg.com) with the same numeration
(plus minor changes).

See also the _"Blue Book"_
[Companion Booklet to FPiS](http://blog.higher-order.com/blog/2015/03/06/a-companion-booklet-to-functional-programming-in-scala/)
for chapter notes, hints, answers.

---

The _Red Book_ served as a great _starting point_ for the exercises here.  The
given code solutions occasionally differ from the answers in the _Blue Book_.

Additions:
- Compared modeling LinkedLists as: OOP vs FP vs FP-with-Nil-Punning vs Scala3
  `enum` data type.
- Mixed in exercises from chapters 10, 11, and 12 (on Foldable, Functor,
  Applicative, Traversal, Monad).
- Added combinators from the related CATS APIs.
- Alternative code versions with function name suffixes:
  - `_r`/`_tr` [non-] vs tail-recursive,
  - `_fr`/`_fl`/`_fm`/`_f` whether based on `fold{Right,Left,Map}`, `fold`
  - `_m`/`_mrd` Map[+Reduce], `_fm` flatMap-based etc,
  - insisted on stacksafe (`@tailrec`) solutions almost everywhere.

Note that the core FP types (Foldable, Functor, etc) are only discussed here
in the specific _List context_.  Future exercises under will cover the Red
Book's chapters 10, 11, 12.

[SE_ch3]: https://www.scala-exercises.org/fp_in_scala/functional_data_structures

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala3 sources](src/main/scala3/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/),
[Scala3 tests](src/test/scala3/netropy/polyglot/fp/lists/)

| FPiS / Scala Exercises | Here |
|---|---|
| [Ex 3.1 `match`][SE_ch3] | [A: `x: Int = 3`, 1st case's _values_ not matching input](src/test/scala/netropy/polyglot/fp/lists/ex_2_5.length_sum_product_toStrings_mapIncr.scala) |
| | [___Q: 1 Linked-List API Variants___](ex_1.q.apis.md) |
| [Ex 3.2 `tail`][SE_ch3] | [ex_1_1.list_type_oop.scala](src/main/scala/netropy/polyglot/fp/lists/ex_1_1.list_type_oop.scala) |
| [Ex 3.2 `tail`][SE_ch3] | [ex_1_2.list_type_fp.scala](src/main/scala/netropy/polyglot/fp/lists/ex_1_2.list_type_fp.scala) |
| [Ex 3.2 `tail`][SE_ch3] | [ex_1_3.list_type_fpnull.scala](src/main/scala/netropy/polyglot/fp/lists/ex_1_3.list_type_fpnull.scala) |
| [Ex 3.2 `tail`][SE_ch3] | [ex_1_4.list_type_enum.scala](src/main/scala3/netropy/polyglot/fp/lists/ex_1_4.list_type_enum.scala) |
| | [___Q: 2 (List) Recursion___](ex_2.q.recursion.md) |
| | [ex_2_1.show_mkString.scala](src/main/scala/netropy/polyglot/fp/lists/ex_2_1.show_mkString.scala) |
| [Ex 3.{3,4,5} `setHead`, `drop`, `dropWhile`][SE_ch3] | [ex_2_2.setHead_setTail_drop_dropWhile.scala](src/main/scala/netropy/polyglot/fp/lists/ex_2_2.setHead_setTail_drop_dropWhile.scala) |
| [Ex 3.6 `init`][SE_ch3] | [ex_2_3.last_init_take_takeWhile_filter.scala](src/main/scala/netropy/polyglot/fp/lists/ex_2_3.last_init_take_takeWhile_filter.scala) |
| | [ex_2_4.reverse_append_copy.scala](src/main/scala/netropy/polyglot/fp/lists/ex_2_4.reverse_append_copy.scala) |
| | [ex_2_5.length_sum_product_toStrings_mapIncr.scala](src/main/scala/netropy/polyglot/fp/lists/ex_2_5.length_sum_product_toStrings_mapIncr.scala) |
| | [ex_2_6.exists_forall_count_find_get.scala](src/main/scala/netropy/polyglot/fp/lists/ex_2_6.exists_forall_count_find_get.scala) |
| [Ex 3.24 `hasSubsequence`][SE_ch3] | [ex_2_7.startsWith_endsWith_hasSubsequence.scala](src/main/scala/netropy/polyglot/fp/lists/ex_2_7.startsWith_endsWith_hasSubsequence.scala) |
| | [ex_2_8.zipAdd.scala](src/main/scala/netropy/polyglot/fp/lists/ex_2_8.zipAdd.scala) |
| | [___Q: 3 (List) Foldable API___](ex_3.q.foldable.md) |
| [Ex 3.{10,13} `foldLeft`, `foldRightViaFoldLeft`][SE_ch3] | [ex_3_1.foldLeft_foldRight.scala](src/main/scala/netropy/polyglot/fp/lists/ex_3_1.foldLeft_foldRight.scala) |
| | [ex_3_2.fold_foldMap.scala](src/main/scala/netropy/polyglot/fp/lists/ex_3_2.fold_foldMap.scala) |
| [Ex 3.{7,8,x} `foldRight`](ex_3_x.fpis.foldRight.md) | [ex_3_x.fpis.foldRight](ex_3_x.fpis.foldRight.md) |
| [Ex 3.{9,11,16,17} `length`, `sum`, `product`, `add1`, `doubleToString`][SE_ch3] | [ex_3_3.toStrings_length_sum_product_mapIncr_mkString.scala](src/main/scala/netropy/polyglot/fp/lists/ex_3_3.toStrings_length_sum_product_mapIncr_mkString.scala) |
|  [Ex 3.{12,14} `reverse`, `append`][SE_ch3] | [ex_3_4.reverse_append_copy.scala](src/main/scala/netropy/polyglot/fp/lists/ex_3_4.reverse_append_copy.scala) |
| [Ex 3.19 `removeOdds`][SE_ch3] | [ex_3_5.drop_dropWhile_take_takeWhile_last_init_filter.scala](src/main/scala/netropy/polyglot/fp/lists/ex_3_5.drop_dropWhile_take_takeWhile_last_init_filter.scala) |
| | [ex_3_6.exists_forall_count_find_get.scala](src/main/scala/netropy/polyglot/fp/lists/ex_3_6.exists_forall_count_find_get.scala) |
| | [ex_3_7.startsWith_endsWith.scala](src/main/scala/netropy/polyglot/fp/lists/ex_3_7.startsWith_endsWith.scala) |
| [Ex 3.13 `foldRightViaFoldLeft`, `foldLeftViaFoldRight`][SE_ch3] | [ex_3_8.foldLeft_foldRight.scala](src/main/scala/netropy/polyglot/fp/lists/ex_3_8.foldLeft_foldRight.scala) |
| | [___Q: 4 (List) Reducible API___](ex_4.q.reducible.md) |
| | [ex_4_1.reduce.scala](src/main/scala/netropy/polyglot/fp/lists/ex_4_1.reduce.scala) |
| | [ex_4_2.sum.scala](src/main/scala/netropy/polyglot/fp/lists/ex_4_2.sum.scala) |
| | [___Q: 5 (List) Functor API___](ex_5.q.functor.md) |
| [Ex 3.18 `map`][SE_ch3] | [ex_5_1.map_lift_mapReverse.scala](src/main/scala/netropy/polyglot/fp/lists/ex_5_1.map_lift_mapReverse.scala) |
| | [ex_5_2.mapply_fproduct_tupleLeft_tupleRight_unzip.scala](src/main/scala/netropy/polyglot/fp/lists/ex_5_2.mapply_fproduct_tupleLeft_tupleRight_unzip.scala) |
| | [ex_5_3.mapIncr_toStrings.scala](src/main/scala/netropy/polyglot/fp/lists/ex_5_3.mapIncr_toStrings.scala) |
| | [ex_5_4.length_product_mkString.scala](src/main/scala/netropy/polyglot/fp/lists/ex_5_4.length_product_mkString.scala) |
| | [ex_5_5.exists_forall_count.scala](src/main/scala/netropy/polyglot/fp/lists/ex_5_5.exists_forall_count.scala) |
| | [___Q: 6 (List) Applicative API___](ex_6.q.applicative.md) |
| | [ex_6_1.tuple2_map2_ap.scala](src/main/scala/netropy/polyglot/fp/lists/ex_6_1.tuple2_map2_ap.scala) |
| | [ex_6_2.tuple3_map3_ap2.scala](src/main/scala/netropy/polyglot/fp/lists/ex_6_2.tuple3_map3_ap2.scala) |
| | [ex_6_3.tuple2_map2_ap.scala](src/main/scala/netropy/polyglot/fp/lists/ex_6_3.tuple2_map2_ap.scala) |
| | [___Q: 7 (List) Zipable (Traversal) Functions___](ex_7.q.zipable.md) |
| [Ex 3.23 `zipWith`][SE_ch3] | [ex_7_1.zipWith_zip.scala](src/main/scala/netropy/polyglot/fp/lists/ex_7_1.zipWith_zip.scala) |
| [Ex 3.22 `addPairwise`][SE_ch3] | [ex_7_2.zipAdd.scala](src/main/scala/netropy/polyglot/fp/lists/ex_7_2.zipAdd.scala) |
| | [___Q: 8 (List) Monad Functions___](ex_8.q.monad.md) |
| [Ex 3.{15,20} `concat`, `flatMap`][SE_ch3] | [ex_8_1.join_flatMap_compose.scala](src/main/scala/netropy/polyglot/fp/lists/ex_8_1.join_flatMap_compose.scala) |
| Ex 3.21 Use `flatMap` to implement `filter`. | [ex_8_2.mproduct_filter.scala](src/main/scala/netropy/polyglot/fp/lists/ex_8_2.mproduct_filter.scala) |
| | [ex_8_3.map.scala](src/main/scala/netropy/polyglot/fp/lists/ex_8_3.map.scala) |
| | [ex_8_4.join_flatMap_compose.scala](src/main/scala/netropy/polyglot/fp/lists/ex_8_4.join_flatMap_compose.scala) |
| | [ex_8_5.tuple2_map2_ap.scala](src/main/scala/netropy/polyglot/fp/lists/ex_8_5.tuple2_map2_ap.scala) |

Resources: _Blue Book_ and _Scala Exercises_ repositories \
<https://github.com/fpinscala/fpinscala> \
<https://github.com/scala-exercises/scala-exercises>

Problem/language/library features: \
[tags](tags.md)

[Up](./README.md)
