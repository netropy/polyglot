TAGS: Problem: \
API design,functional data type,singly linked lists,combinators,recursion,
tail-recursion,Foldable,Functor,Applicative,Traversal,Monad,lambda,closure,
head,tail,last,init,drop,dropWhile,take,takeWhile,filter,reverse,append,copy,
exists,forall,count,find,get,mkString,startsWith,endsWith,hasSubsequence,
foldLeft,foldRight,fold,foldMap,reduce,map,lift,mapReverse,mapply,fproduct,
tupleLeft,tupleRight,unzip,tuple2,map2,ap,apply,tuple3,map3,ap2,zipWith,zip,
join,zipL,zipR,zipWithL,zipWithR,apZip,zipWithIndex,mapWithIndex,flatten,
flatMap,compose,andThen,mproduct,monoid

TAGS: Scala: \
scala3, scala2.13, enum, lambdas, @annotation.tailrec
