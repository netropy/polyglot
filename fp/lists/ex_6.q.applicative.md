# polyglot/fp/LinkedLists

### 6 (List) Applicative API

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/)


#### 6.1 Implement combinators `tuple2`, `map2`, `ap` in terms of _map_ and _foldRight_.

Also provide `foldLeft`-based implementations (with reversal of result list);
discuss whether difference.

```
  /** Returns the List of all pairs (a, b), sequencing as, then bs. */
  def tuple2_{fr,fl}[A, B]
      (as: List[A], bs: List[B]): List[(A, B)]

  /** Applies the function to each pair (a, b), sequencing as, then bs. */
  def map2_{fr,fl}[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C): List[C]

  /** Applies each function to each value, sequencing fs, then as. */
  def ap_{fr,fl}[A, B]
      (fs: List[A => B])
      (as: List[A]): List[B]
```

Discuss whether the implementations of these functions share a common pattern.

Resources: \
<https://typelevel.org/cats/typeclasses/applicative.html>,
<https://typelevel.org/cats/api/cats/Applicative.html>

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_6_1.tuple2_map2_ap.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_6_1.tuple2_map2_ap.scala)

#### 6.2 Implement combinators `tuple3`, `map3`, `ap2` in terms of _tuple2_, _map2_, _ap_, and _map_.

```
  /** Returns the List of all (a, b, c), sequencing as, bs, then cs. */
  def tuple3_t2[A, B, C]
      (as: List[A], bs: List[B], cs: List[C]): List[(A, B, C)]

  /** Applies the function to each (a, b, c), sequencing as, bs, then cs. */
  def map3_m2[A, B, C, D]
      (as: List[A], bs: List[B], cs: List[C])
      (f: (A, B, C) => D): List[D]

  /** Applies each function to each (a, b), sequencing fs, as, then bs. */
  def ap2_ap[A, B, C]
      (fs: List[(A, B) => C])
      (as: List[A], bs: List[B]): List[C] = {
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_6_2.tuple3_map3_ap2.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_6_2.tuple3_map3_ap2.scala)

#### 6.3 Rewrite combinators `tuple2`, `map2`, `ap` in terms of each other.

Only 3 rotating implementations needed to show equivalence (yet, writing each
combinator in terms of the other two makes a good exercise).

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_6_3.tuple2_map2_ap.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_6_3.tuple2_map2_ap.scala)

[Up](./README.md)
