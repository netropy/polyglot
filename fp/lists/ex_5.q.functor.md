# polyglot/fp/LinkedLists

### 5 (List) Functor API

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/)

#### 5.1 Implement combinator `map` in terms of a _fold_.

Implement `map_fr` in terms of `foldRight`.  Also provide a `foldLeft`-based
variant (with reversal of result list); discuss whether difference.
```
  /** Applies the function to each list element. */
  def map_{fr,fl}[A, B]
      (as: List[A])
      (f: A => B): List[B]

  /** Lifts a function to operate on Lists. */
  def lift_m[A, B]
      (f: A => B): List[A] => List[B]
```

Discuss signature variants `map` and `lift`.

Implement another combinator `mapReverse` and discuss: 1) why it may be useful
and 2) why it cannot be part of the Functor API:
```
  /** Applies the function to each element, reversing the order. */
  def mapReverse_fl[A, B]
      (as: List[A])
      (f: A => B): List[B]
```

Resources: \
<https://typelevel.org/cats/typeclasses/functor.html>,
<https://typelevel.org/cats/api/cats/Functor.html>

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_5_1.map.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_5_1.map.scala)

#### 5.2 Implement functions `mapply`, `fproduct`, `tupleLeft`, `tupleRight`, `unzip` in terms of _map_.

Implement the other functions:
```
  /** Applies each unary function in the list to the given value. */
  def mapply_m[A, B]
      (a: A)
      (fs: List[A => B]): List[B]

  /** Pairs each list element with its mapped value by given function. */
  def fproduct_m[A, B]
      (as: List[A])
      (f: (A) => B): List[(A, B)]

  /** Pairs each list element with the given value on the left. */
  def tupleLeft_m[A, B]
      (as: List[A], b: B): List[(B, A)]

  /** Pairs each list element with the given value on the right. */
  def tupleRight_m[A, B]
      (as: List[A], b: B): List[(A, B)]
```

Implement `unzip` in terms of `foldRight` and `map`; discuss whether
difference:
```
  /** Unzips a list of pairs into a pair of lists. */
  def unzip_fr,m}[A, B]
      (ab: List[(A, B)]): (List[A], List[B])
```

Summarize what sort of list operations can and cannot be implemented just in
terms of Functor's `map`.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_5_2.mapply_fproduct_tupleLeft_tupleRight_unzip.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_5_2.mapply_fproduct_tupleLeft_tupleRight_unzip.scala)

#### 5.3 Rewrite 3.x functions in terms of pure _map_, where applicable.

Suffix function names with `_m`.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_5_3.mapIncr_toStrings.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_5_3.mapIncr_toStrings.scala)

#### 5.4 Rewrite (eager) 3.x functions in terms of _map_ and _reduce_, where applicable.

Suffix function names with `_mrd`.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_5_4.length_product_mkString.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_5_4.length_product_mkString.scala)

#### 5.5 Rewrite (lazy) 3.x functions in terms of _map_ and _reduce_, where applicable.

Suffix function names with `_mrd`.  Discuss differences to _fold_-based
variants.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_5_5.exists_forall_count.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_5_5.exists_forall_count.scala)

[Up](./README.md)
