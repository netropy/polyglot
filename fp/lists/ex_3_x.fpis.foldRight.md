# polyglot/fp/LinkedLists

FPiS (3.7) and Scala Exercise (3.x) w/o code.

#### FPiS: 3.7 Early return from _foldRight_?

This exercise in
[Functional Programming in Scala](https://www.manning.com/books/functional-programming-in-scala)
seems _questionable_:
```
  Exercise 3.7:
  Can product, implemented using foldRight, immediately halt the recursion and
  return 0.0 if it encounters a 0.0?  Why or why not?  Consider how any
  short-circuiting might work if you call foldRight with a large list. This is
  a deeper question that we’ll return to in chapter 5.
```

The
[expected answer](https://github.com/fpinscala/fpinscala/blob/master/answerkey/datastructures/07.answer.scala):
```
  No, this is not possible! The reason is because _before_ we ever call our
  function, f, we evaluate its argument, which in the case of foldRight
  means traversing the list all the way to the end. We need _non-strict_
  evaluation to support early termination---we discuss this in chapter 5.
```

___Discussion:___

This question and the expected answer seem dubious:

Why the question's insistence on `foldRight` if for associative multiplication
`foldLeft` or `fold` would apply?

Even with _foldRight_ traversing the entire list and starting from right, a
lambda could still skip multiplication and `return` directly from _product_:
```
  def product_f(as: List[Int]): Double =
    foldRight(as, 1.0){ (x, y) => if (x == 0) return 0.0; x * y }
```

The question's hint about "large list" seems to suggest that halting recursion
early might _somehow violate referential transparency_ as it could avoid an
otherwise occuring stack overflow.  Such an argument would be odd: a sensible
_foldRight_ implementation may chose to trade stack for heap memory.  Also,
`StackOverflowError` or `OutOfMemoryError` are not part of a function's
semantics but features of the respective execution environment (e.g., a JVM).

#### Scala Exercises: 3.x _foldRight_ trace?

The
[Scala Exercises, Chapter 3](https://www.scala-exercises.org/fp_in_scala/functional_data_structures)
replaced question 3.7 from FPiS with
```
  Exercise 3.x:
  Let's run through the steps that foldRight will follow in our new implementation of sum2:

  foldRight(Cons(1, Cons(2, Cons(3, Nil))), 0)((x, y) => x + y) shouldBe 6
  _ + foldRight(Cons(2, Cons(3, Nil)), 0)((x, y) => x + y) shouldBe 6
  _ + _ + foldRight(Cons(3, Nil), 0)((x, y) => x + y) shouldBe 6
  _ + _ + _ + foldRight(_, 0)((x, y) => x + y) shouldBe 6
  _ + _ + _ + _ shouldBe 6
```

This exercise would be _clearer_ if it _also_ showed the defining feature of
`foldRight`: the right-associative evaluation of the operation
```
  foldRight(Cons(1, Cons(2, Cons(3, Nil))), 0)((x, y) => x + y)
  1 + (foldRight(Cons(2, Cons(3, Nil)), 0)((x, y) => x + y))
  1 + (2 + (foldRight(Cons(3, Nil), 0)((x, y) => x + y)))
  1 + (2 + (3 + (foldRight(Nil, 0)((x, y) => x + y))))
  1 + (2 + (3 + (0)))
  1 + (2 + (3))
  1 + (5)
  6
```

#### FPiS 3.8 Passing _Nil_, _Cons_ to _foldRight_?

```
  Exercise 3.8:
  Now that we know how foldRight works, try to think about what happens when
  you pass Nil and Cons themselves to foldRight.

  foldRight(List(1, 2, 3), Nil: List[Int])(Cons(_, _))

  What do you think this says about the relationship between foldRight and the
  data constructors of List?
```

Combinator _foldRight_ applies the operation _f_ in the order of the List's
construction, so passing _(Cons, Nil)_ copies the list:
```
     f(2,    f(1,    f(0,   z)))
  Cons(2, Cons(1, Cons(0, Nil)))
```

___Corollary:___

A collection that offers _foldRight_ can be converted to a List that preserves
the collection's size and order of traversal.

However, this mapping to a List may loose structural information (for example,
folding an k-ary Tree to List).

[Up](./README.md)
