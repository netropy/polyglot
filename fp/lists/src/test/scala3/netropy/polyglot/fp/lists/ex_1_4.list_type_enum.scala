package netropy.polyglot.fp.lists

import munit.FunSuite
import netropy.polyglot.fp.FunSuiteExt  // sugar

import ex_1_4.List
import List.{Nil, Cons, unit, empty}

class Ex_1_4_Tests
    extends FunSuite
    with FunSuiteExt {

  def compile_test_ex_1_4_List_Nil_Cons_head_tail: Unit = {

    // match over all values (no upcasts needed)
    List(1) match {
      case Nil => ()
      case Cons(h, t) => ()
    }
    Nil match {
      case Nil => ()
      //case Cons(h, t) => ()  // unreachable case warning
    }
    Cons(1, Nil) match {
      case Nil => ()
      case Cons(h, t) => ()
    }
  }

  test("List|0|: List, Nil, Cons, head, tail, headOption, tailOption") {
    assertEquals(Nil, List())
    intercept[UnsupportedOperationException] {Nil.head }
    intercept[UnsupportedOperationException] {Nil.tail }
    assertEquals(None, Nil.headOption)
    assertEquals(None, Nil.tailOption)
  }

  test("List|2|: List, Nil, Cons, head, tail, headOption, tailOption") {
    intercept[IllegalArgumentException] {Cons(1, null) }
    assertEquals(Cons(1, Nil), List(1))
    assertEquals(1, List(1).head)
    assertEquals(Nil, List(1).tail)
    assertEquals(Some(1), List(1).headOption)
    assertEquals(Some(Nil), List(1).tailOption)
  }

  test("List|2|: List, Nil, Cons, head, tail, headOption, tailOption") {
    assertEquals(Cons(2, Cons(1, Nil)), List(2, 1))
    assertEquals(2, List(2, 1).head)
    assertEquals(List(1), List(2, 1).tail)
    assertEquals(Some(2), List(2, 1).headOption)
    assertEquals(Some(List(1)), List(2, 1).tailOption)
  }

  test("List: empty, unit") {
    assertEquals(List(), empty)
    assertEquals(List(1), unit(1))
  }

  test("enum features") {
    assertEquals(0, Nil.ordinal)
    assertEquals(1, Cons(2, Nil).ordinal)
  }
}
