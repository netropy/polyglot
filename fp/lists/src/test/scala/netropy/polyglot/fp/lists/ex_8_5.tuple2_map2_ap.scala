package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_8_5_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_6_1_Tests {

  import ex_8_5.{tuple2_fm, map2_fm, ap_fm}

  test_tuple2(tuple2_fm)

  test_map2(map2_fm)

  test_ap(ap_fm)
}
