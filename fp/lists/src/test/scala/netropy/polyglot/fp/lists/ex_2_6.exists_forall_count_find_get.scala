package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_2_6_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  private type A = Int
  private val ltOne: A => Boolean = _ < 1

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_exists
      (exists: List[A] => (A => Boolean) => Boolean)
      (implicit loc: Location): Unit =
    test("exists") {
      assertResult(false) { exists(List())(ltOne) }
      assertResult(true) { exists(List(0))(ltOne) }
      assertResult(false) { exists(List(1))(ltOne) }
      assertResult(true) { exists(List(0, 0))(ltOne) }
      assertResult(true) { exists(List(0, 1))(ltOne) }
      assertResult(true) { exists(List(1, 0))(ltOne) }
      assertResult(false) { exists(List(1, 2))(ltOne) }
    }

  def test_forall
      (forall: List[A] => (A => Boolean) => Boolean)
      (implicit loc: Location): Unit =
    test("forall") {
      assertResult(true) { forall(List())(ltOne) }
      assertResult(true) { forall(List(0))(ltOne) }
      assertResult(false) { forall(List(1))(ltOne) }
      assertResult(true) { forall(List(0, 0))(ltOne) }
      assertResult(false) { forall(List(0, 1))(ltOne) }
      assertResult(false) { forall(List(1, 0))(ltOne) }
      assertResult(false) { forall(List(1, 2))(ltOne) }
    }

  def test_count
      (count: List[A] => (A => Boolean) => Long)
      (implicit loc: Location): Unit =
    test("count") {
      assertResult(0L) { count(List())(ltOne) }
      assertResult(1L) { count(List(0))(ltOne) }
      assertResult(0L) { count(List(1))(ltOne) }
      assertResult(2L) { count(List(0, 0))(ltOne) }
      assertResult(1L) { count(List(0, 1))(ltOne) }
      assertResult(1L) { count(List(1, 0))(ltOne) }
      assertResult(0L) { count(List(1, 2))(ltOne) }
    }

  def test_find
      (find: List[A] => (A => Boolean) => Option[A])
      (implicit loc: Location): Unit =
    test("find") {
      assertResult(None) { find(List())(ltOne) }
      assertResult(Some(0)) { find(List(0))(ltOne) }
      assertResult(None) { find(List(1))(ltOne) }
      assertResult(Some(0)) { find(List(0, 0))(ltOne) }
      assertResult(Some(0)) { find(List(0, 1))(ltOne) }
      assertResult(Some(0)) { find(List(1, 0))(ltOne) }
      assertResult(None) { find(List(1, 2))(ltOne) }
    }

  def test_get
      (get: (List[A], Int) => Option[A])
      (implicit loc: Location): Unit =
    test("get") {
      assertResult(None) { get(List(), -1) }
      assertResult(None) { get(List(), 0) }
      assertResult(None) { get(List(), 1) }
      assertResult(None) { get(List(9), -1) }
      assertResult(Some(9)) { get(List(9), 0) }
      assertResult(None) { get(List(9), 1) }
      assertResult(None) { get(List(8, 9), -1) }
      assertResult(Some(8)) { get(List(8, 9), 0) }
      assertResult(Some(9)) { get(List(8, 9), 1) }
      assertResult(None) { get(List(8, 9), 2) }
    }
}

class Ex_2_6_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_6_Tests {

  import ex_2_6.{exists_tr, forall_tr, count_tr, find_tr, get_tr}

  test_exists(exists_tr)

  test_forall(forall_tr)

  test_count(count_tr)

  test_find(find_tr)

  test_get(get_tr)
}
