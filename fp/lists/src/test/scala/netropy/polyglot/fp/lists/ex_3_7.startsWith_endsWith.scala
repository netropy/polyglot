package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_3_7_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_7_Tests {

  import ex_3_7.{startsWith_fl, startsWith_fl_lazy,
    endsWith_fl, endsWith_fl_lazy}

  test_startsWith(startsWith_fl)

  test_startsWith(startsWith_fl_lazy)

  test_endsWith(endsWith_fl)

  test_endsWith(endsWith_fl_lazy)
}
