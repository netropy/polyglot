package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_5_5_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_6_Tests {

  import ex_5_5.{exists_mrd, forall_mrd, count_mrd}

  test_exists(exists_mrd)

  test_forall(forall_mrd)

  test_count(count_mrd)
}
