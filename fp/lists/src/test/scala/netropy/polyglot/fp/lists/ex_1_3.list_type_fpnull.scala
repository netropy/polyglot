package netropy.polyglot.fp.lists

import munit.FunSuite
import netropy.polyglot.fp.FunSuiteExt  // sugar

import ex_1_3.List
import List.{Nil, Cons, head, tail, headOption, tailOption, unit, empty}

class Ex_1_3_Tests
    extends FunSuite
    with FunSuiteExt {

  def compile_test_List_Nil_Cons(): Unit = {
    List(1) match {  // upcasts needed
      case Nil => ()
      case Cons(h, t) => ()
    }
    Nil match {  // upcasts needed
      case Nil => ()
      case Cons(h, t) => ()
    }
    Cons(1, Nil) match {  // upcasts needed
      case Nil => ()
      case Cons(h, t) => ()
    }
  }

  test("List|0|: List, Nil, Cons, head, tail, headOption, tailOption") {
    assertResult(null) { Nil }
    assertResult(Nil) { List() }
    intercept[IllegalArgumentException] { head(List()) }
    intercept[IllegalArgumentException] { tail(List()) }
    assertResult(None) { headOption(Nil) }
    assertResult(None) { tailOption(Nil) }
  }

  test("List|1|: List, Nil, Cons, head, tail, headOption, tailOption") {
    assertResult(Cons(1, Nil)) { Cons(1, null) } // passing null is valid now
    assertResult(Cons(1, Nil)) { List(1) }
    assertResult(1) { head(List(1)) }
    assertResult(Nil) { tail(List(1)) }
    assertResult(Some(1)) { headOption(List(1)) }
    assertResult(Some(Nil)) { tailOption(List(1)) }
  }

  test("List|2|: List, Nil, Cons, head, tail, headOption, tailOption") {
    assertResult(Cons(2, Cons(1, Nil))) { List(2, 1) }
    assertResult(2) { head(List(2, 1)) }
    assertResult(List(1)) { tail(List(2, 1)) }
    assertResult(Some(2)) { headOption(List(2, 1)) }
    assertResult(Some(List(1))) { tailOption(List(2, 1)) }
  }

  test("List: empty, unit") {
    assertResult(List()) { empty }
    assertResult(List(1)) { unit(1) }
  }
}
