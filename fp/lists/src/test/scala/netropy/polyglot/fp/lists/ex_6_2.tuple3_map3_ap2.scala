package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_6_2_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  // types and binary operators for testing
  case class A(i: Int)
  case class B(i: Int)
  case class C(i: Int)
  type D = String
  // dotty "0.26.0-RC1" compiler bug
  // scala.MatchError: ... (of class dotty.tools.dotc.core.Names$DerivedName)
  //private val (a1, a2, b1, b2, c1, c2) = (A(1), A(2), B(1), B(2), C(1), C(2))
  private val a0 = A(0)
  private val a1 = A(1)
  private val b0 = B(0)
  private val b1 = B(1)
  private val c0 = C(0)
  private val c1 = C(1)
  private val conc: (A, B, C) => D = (a, b, c) => s"a${a.i}b${b.i}c${c.i}"
  private val f0: (A, B) => D = (a, b) => s"f0a${a.i}b${b.i}"
  private val f1: (A, B) => D = (a, b) => s"f1a${a.i}b${b.i}"

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_tuple3
      (tuple3: (List[A], List[B], List[C]) => List[(A, B, C)])
      (implicit loc: Location): Unit =
    test("tuple3") {
      assertResult(List()) {
        tuple3(List(a0, a1), List(b0, b1), List())
      }
      assertResult(List()) {
        tuple3(List(a0, a1), List(), List(c0, c1))
      }
      assertResult(List()) {
        tuple3(List(), List(b0, b1), List(c0, c1))
      }
      assertResult(
        List((a0, b0, c0), (a0, b1, c0), (a1, b0, c0), (a1, b1, c0))) {
        tuple3(List(a0, a1), List(b0, b1), List(c0))
      }
      assertResult(
        List((a0, b0, c0), (a0, b0, c1), (a1, b0, c0), (a1, b0, c1))) {
        tuple3(List(a0, a1), List(b0), List(c0, c1))
      }
      assertResult(
        List((a0, b0, c0), (a0, b0, c1), (a0, b1, c0), (a0, b1, c1))) {
        tuple3(List(a0), List(b0, b1), List(c0, c1))
      }
      assertResult(
        List(
          (a0, b0, c0), (a0, b0, c1), (a0, b1, c0), (a0, b1, c1),
          (a1, b0, c0), (a1, b0, c1), (a1, b1, c0), (a1, b1, c1),
        )) {
        tuple3(List(a0, a1), List(b0, b1), List(c0, c1))
      }
    }

  def test_map3
      (map3:
        (List[A], List[B], List[C]) => ((A, B, C) => D) => List[D])
      (implicit loc: Location): Unit =
    test("map3") {
      assertResult(List()) {
        map3(List(a0, a1), List(b0, b1), List())(conc)
      }
      assertResult(List()) {
        map3(List(a0, a1), List(), List(c0, c1))(conc)
      }
      assertResult(List()) {
        map3(List(), List(b0, b1), List(c0, c1))(conc)
      }
      assertResult(List("a0b0c0", "a0b1c0", "a1b0c0", "a1b1c0")) {
        map3(List(a0, a1), List(b0, b1), List(c0))(conc)
      }
      assertResult(List("a0b0c0", "a0b0c1", "a1b0c0", "a1b0c1")) {
        map3(List(a0, a1), List(b0), List(c0, c1))(conc)
      }
      assertResult(List("a0b0c0", "a0b0c1", "a0b1c0", "a0b1c1")) {
        map3(List(a0), List(b0, b1), List(c0, c1))(conc)
      }
      assertResult(
        List(
          "a0b0c0", "a0b0c1", "a0b1c0", "a0b1c1",
          "a1b0c0", "a1b0c1", "a1b1c0", "a1b1c1")) {
        map3(List(a0, a1), List(b0, b1), List(c0, c1))(conc)
      }
    }

  def test_ap2
      (ap2:
        (List[(A, B) => D]) => (List[A], List[B]) => List[D])
      (implicit loc: Location): Unit =
    test("ap2") {
      assertResult(List()) {
        ap2(List(f0, f1))(List(a0, a1), List())
      }
      assertResult(List()) {
        ap2(List(f0, f1))(List(), List(b0, b1))
      }
      assertResult(List()) {
        ap2(List())(List(a0, a1), List(b0, b1))
      }
      assertResult(List("f0a0b0", "f0a1b0", "f1a0b0", "f1a1b0")) {
        ap2(List(f0, f1))(List(a0, a1), List(b0))
      }
      assertResult(List("f0a0b0", "f0a0b1", "f1a0b0", "f1a0b1")) {
        ap2(List(f0, f1))(List(a0), List(b0, b1))
      }
      assertResult(List("f0a0b0", "f0a0b1", "f0a1b0", "f0a1b1")) {
        ap2(List(f0))(List(a0, a1), List(b0, b1))
      }
      assertResult(
        List(
          "f0a0b0", "f0a0b1", "f0a1b0", "f0a1b1",
          "f1a0b0", "f1a0b1", "f1a1b0", "f1a1b1")) {
          ap2(List(f0, f1))(List(a0, a1), List(b0, b1))
        }
    }
}

class Ex_6_2_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_6_2_Tests {

  import ex_6_2.{tuple3_t2, map3_m2, ap2_ap}

  test_tuple3(tuple3_t2)

  test_map3(map3_m2)

  test_ap2(ap2_ap)
}
