package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_3_6_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_6_Tests {

  import ex_3_6.{exists_fl, exists_fl_lazy, forall_fl, forall_fl_lazy,
    count_f,
    find_fl, find_fl_lazy, get_fl, get_fl_lazy}

  test_exists(exists_fl)

  test_exists(exists_fl_lazy)

  test_forall(forall_fl)

  test_forall(forall_fl_lazy)

  test_count(count_f)

  test_find(find_fl)

  test_find(find_fl_lazy)

  test_get(get_fl)

  test_get(get_fl_lazy)
}
