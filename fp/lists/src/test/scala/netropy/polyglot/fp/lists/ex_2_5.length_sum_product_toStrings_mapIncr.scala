package netropy.polyglot.fp.lists

import scala.math.BigInt
import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_2_5_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List

  def test_length
      (length: List[Int] => Int)
      (implicit loc: Location): Unit =
    test("length") {
      assertResult(0) { length(List()) }
      assertResult(1) { length(List(1)) }
      assertResult(2) { length(List(1, 2)) }
      assertResult(3) { length(List(0, 2, 3)) }
      assertResult(4) { length(List(1, 2, 3, 4)) }
    }

  def test_sum
      (sum: List[Int] => Int)
      (implicit loc: Location): Unit =
    test("sum") {
      assertResult(0) { sum(List()) }
      assertResult(1) { sum(List(1)) }
      assertResult(3) { sum(List(1, 2)) }
      assertResult(5) { sum(List(0, 2, 3)) }
      assertResult(10) { sum(List(1, 2, 3, 4)) }
    }

  def test_product
      (product: List[Int] => BigInt)
      (implicit loc: Location): Unit =
    test("product") {
      assertResult(BigInt(1)) { product(List()) }
      assertResult(BigInt(1)) { product(List(1)) }
      assertResult(BigInt(2)) { product(List(1, 2)) }
      assertResult(BigInt(0)) { product(List(0, 2, 3)) }
      assertResult(BigInt(24)) { product(List(1, 2, 3, 4)) }
    }

  def test_toStrings
      (toStrings: List[Int] => List[String])
      (implicit loc: Location): Unit =
    test("toStrings") {
      assertResult(List()) { toStrings(List()) }
      assertResult(List("1")) { toStrings(List(1)) }
      assertResult(List("1", "2")) { toStrings(List(1, 2)) }
      assertResult(List("0", "2", "3")) { toStrings(List(0, 2, 3)) }
      assertResult(List("1", "2", "3", "4")) { toStrings(List(1, 2, 3, 4)) }
    }

  def test_mapIncr
      (mapIncr: List[Int] => List[Int])
      (implicit loc: Location): Unit =
    test("mapIncr") {
      assertResult(List()) { mapIncr(List()) }
      assertResult(List(2)) { mapIncr(List(1)) }
      assertResult(List(2, 3)) { mapIncr(List(1, 2)) }
      assertResult(List(1, 3, 4)) { mapIncr(List(0, 2, 3)) }
      assertResult(List(2, 3, 4, 5)) { mapIncr(List(1, 2, 3, 4)) }
    }
}

class Ex_2_5_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_5_Tests {

  test("fpis_ex_3_1_match") {
    import ex_1_3.List
    import List.{Nil, Cons}
    import ex_2_5.{sum_tr => sum}

    // https://www.scala-exercises.org/fp_in_scala/functional_data_structures
    // EXERCISE 3.1
    // What will be the result of the following match expression?
    val x = List(1, 2, 3, 4, 5) match {
      case Cons(x, Cons(2, Cons(4, _))) => x
      case Nil => 42
      case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
      case Cons(h, t) => h + sum(t)
      //case _ => 101  // dotty warning: only null is matched TODO
      case null => 101
    }

    // A: `x: Int = 3`, value `4` in 1st case's pattern not matching input `3`
    assertResult(3) { x }
  }

  import ex_2_5.{length_r, length_tr, sum_r, sum_tr, product_r, product_tr,
    toStrings_r, toStrings_tr, mapIncr_r, mapIncr_tr}

  test_length(length_r)
  test_length(length_tr)

  test_sum(sum_r)
  test_sum(sum_tr)

  test_product(product_r)
  test_product(product_tr)

  test_toStrings(toStrings_r)
  test_toStrings(toStrings_tr)

  test_mapIncr(mapIncr_r)
  test_mapIncr(mapIncr_tr)
}
