package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_3_2_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  /*
   * Notes: Types and binary operator for testing `fold`
   *
   * Sequence concatenation is an associative but non-commutative operation
   * and, hence, can be used to verify that fold preserves the input data's
   * left/right ordering in the output (unlike commutative unlike + or *).
   *
   * Type compliance of fold is verified by a using a supertype of the list
   * data for the neutral element.
   */

  private type A = Seq[Unit]
  private type B = Int
  private val length: A => B = _.length
  private val z: B = 0
  private val add: (B, B) => B = _ + _
  private type C = Vector[Int]  // subtype
  private type C1 = Seq[Int]  // covariant supertype
  private val c: C1 = Seq[Int]()  // neutral element
  private val concat: (C1, C1) => C1 = _ ++ _  // +associative, -commutative

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_fold
      (fold: (List[C], C1) => ((C1, C1) => C1) => C1)
      (implicit loc: Location): Unit =
    test("fold") {
      assertResult(Seq()) { fold(List(), c)(concat) }
      assertResult(Seq()) { fold(List(Vector()), c)(concat) }
      assertResult(Seq(1)) { fold(List(Vector(1)), c)(concat) }
      assertResult(Seq(1, 2)) { fold(List(Vector(1), Vector(2)), c)(concat) }
    }

  def test_foldMap
      (foldMap: List[A] => (A => B) => (B, (B, B) => B) => B)
      (implicit loc: Location): Unit =
    test("foldMap") {
      assertResult(0) { foldMap(List())(length)(z, add) }
      assertResult(0) { foldMap(List(Seq()))(length)(z, add) }
      assertResult(1) { foldMap(List(Seq(())))(length)(z, add) }
      assertResult(2) { foldMap(List(Seq((), ())))(length)(z, add) }
      assertResult(2) { foldMap(List(Seq((), ()), Seq()))(length)(z, add) }
      assertResult(3) { foldMap(List(Seq((), ()), Seq(())))(length)(z, add) }
    }
}

class Ex_3_2_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_3_2_Tests {

  import ex_3_2.{fold_fl, fold_fr, foldMap_fl, foldMap_fr}

  test_fold(fold_fl)

  test_fold(fold_fr)

  test_foldMap(foldMap_fl)

  test_foldMap(foldMap_fr)
}
