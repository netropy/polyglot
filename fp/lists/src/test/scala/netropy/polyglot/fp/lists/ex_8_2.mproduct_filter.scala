package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_8_2_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  // types and binary operators for testing
  private type A = Int
  private type B = Long
  private val as: List[A] = List((for (i <- 0 to 2) yield i): _*)
  private val mult: A => List[B] =
    a => List((for (i <- 1 to a) yield a.toLong): _*)

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_mproduct
      (mproduct: List[A] => (A => List[B]) => List[(A, B)])
      (implicit loc: Location): Unit =
    test("mproduct") {
      assertResult(List()) { mproduct(List())(mult) }
      assertResult(List((1, 1L), (2, 2L), (2, 2L))) { mproduct(as)(mult) }
    }
}

class List_ex_8_2_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_8_2_Tests
    with Ex_2_3_Tests {

  import ex_8_2.{mproduct_fm, mproduct_jm, mproduct_c,
    filter_fm, filter_jm, filter_c}

  test_mproduct(mproduct_fm)

  test_mproduct(mproduct_jm)

  test_mproduct(mproduct_c)

  test_filter(filter_fm)

  test_filter(filter_jm)

  test_filter(filter_c)
}
