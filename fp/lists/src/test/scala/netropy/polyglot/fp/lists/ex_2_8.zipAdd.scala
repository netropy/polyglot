package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_2_8_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  private type I = Int

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_zipAdd
      (zipAdd: (List[I], List[I]) => List[I])
      (implicit loc: Location): Unit =
    test("zipAdd") {
      assertResult(List()) { zipAdd(List(), List()) }
      assertResult(List()) { zipAdd(List(), List(5)) }
      assertResult(List()) { zipAdd(List(1), List()) }
      assertResult(List(6)) { zipAdd(List(1), List(5)) }
      assertResult(List(6)) { zipAdd(List(1), List(5, 6)) }
      assertResult(List(6)) { zipAdd(List(1, 2), List(5)) }
      assertResult(List(6, 8)) { zipAdd(List(1, 2), List(5, 6)) }
      assertResult(List(6, 8)) { zipAdd(List(1, 2), List(5, 6, 7)) }
      assertResult(List(6, 8)) { zipAdd(List(1, 2, 3), List(5, 6)) }
      assertResult(List(6, 8, 10)) { zipAdd(List(1, 2, 3), List(5, 6, 7)) }
    }
}

class Ex_2_8_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_8_Tests {

  import ex_2_8.{zipAdd_r, zipAdd_tr}

  test_zipAdd(zipAdd_r)

  test_zipAdd(zipAdd_tr)
}
