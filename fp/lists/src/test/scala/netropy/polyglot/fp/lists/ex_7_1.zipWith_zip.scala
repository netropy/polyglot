package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_7_1_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  private type I = Int
  private type L = Long
  private val add: (I, L) => L = (_ + _)

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_zipWith
      (zipWith: (List[I], List[L]) => ((I, L) => L) => List[L])
      (implicit loc: Location): Unit =
    test("zipWith") {
      assertResult(List()) { zipWith(List(), List())(add) }
      assertResult(List()) { zipWith(List(), List(5))(add) }
      assertResult(List()) { zipWith(List(1), List())(add) }
      assertResult(List(6L)) { zipWith(List(1), List(5))(add) }
      assertResult(List(6L)) { zipWith(List(1), List(5, 6))(add) }
      assertResult(List(6L)) { zipWith(List(1, 2), List(5))(add) }
      assertResult(List(6L, 8L)) { zipWith(List(1, 2), List(5, 6))(add) }
      assertResult(List(6L, 8L)) { zipWith(List(1, 2), List(5, 6, 7))(add) }
      assertResult(List(6L, 8L)) { zipWith(List(1, 2, 3), List(5, 6))(add) }
      assertResult(List(6L, 8L, 10L)) {
        zipWith(List(1, 2, 3), List(5, 6, 7))(add)
      }
    }

  def test_zip
      (zip: (List[I], List[L]) => List[(I, L)])
      (implicit loc: Location): Unit =
    test("zip") {
      assertResult(List()) { zip(List(), List()) }
      assertResult(List()) { zip(List(), List(5)) }
      assertResult(List()) { zip(List(1), List()) }
      assertResult(List((1, 5L))) { zip(List(1), List(5)) }
      assertResult(List((1, 5L))) { zip(List(1), List(5, 6)) }
      assertResult(List((1, 5L))) { zip(List(1, 2), List(5)) }
      assertResult(List((1, 5L), (2, 6L))) { zip(List(1, 2), List(5, 6)) }
      assertResult(List((1, 5L), (2, 6L))) { zip(List(1, 2), List(5, 6, 7)) }
      assertResult(List((1, 5L), (2, 6L))) { zip(List(1, 2, 3), List(5, 6)) }
      assertResult(List((1, 5L), (2, 6L), (3, 7L))) {
        zip(List(1, 2, 3), List(5, 6, 7))
      }
    }
}

class Ex_7_1_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_7_1_Tests {

  import ex_7_1.{zipWith_tr, zipWith_zm, zip_zw, zip_tr}

  test_zipWith(zipWith_tr)

  test_zipWith(zipWith_zm)

  test_zip(zip_zw)

  test_zip(zip_tr)
}
