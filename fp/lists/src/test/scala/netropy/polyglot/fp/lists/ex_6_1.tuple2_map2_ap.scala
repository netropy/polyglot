package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_6_1_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  // types and binary operators for testing
  private type A = Int
  private type B = Char
  private type C = String
  private val conc: (A, B) => C = (a, b) => b.toString * a
  private val plusa: A => B = a => ('a' + a).toChar
  private val plusA: A => B = a => ('A' + a).toChar

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_tuple2
      (tuple2: (List[A], List[B]) => List[(A, B)])
      (implicit loc: Location): Unit =
    test("tuple2") {
      assertResult(List()) {
        tuple2(List(1, 2), List())
      }
      assertResult(List()) {
        tuple2(List(), List('a', 'b'))
      }
      assertResult(List(1 -> 'a', 2 -> 'a')) {
        tuple2(List(1, 2), List('a'))
      }
      assertResult(List(1 -> 'a', 1 -> 'b')) {
        tuple2(List(1), List('a', 'b'))
      }
      assertResult(List(1 -> 'a', 1 -> 'b', 2 -> 'a', 2 -> 'b')) {
        tuple2(List(1, 2), List('a', 'b'))
      }
    }

  def test_map2
      (map2: (List[A], List[B]) => ((A, B) => C) => List[C])
      (implicit loc: Location): Unit =
    test("map2") {
      assertResult(List()) {
        map2(List(1, 2), List())(conc)
      }
      assertResult(List()) {
        map2(List(), List('a', 'b'))(conc)
      }
      assertResult(List("a", "aa")) {
        map2(List(1, 2), List('a'))(conc)
      }
      assertResult(List("a", "b")) {
        map2(List(1), List('a', 'b'))(conc)
      }
      assertResult(List("a", "b", "aa", "bb")) {
        map2(List(1, 2), List('a', 'b'))(conc)
      }
    }

  def test_ap
      (ap: List[A => B] => List[A] => List[B])
      (implicit loc: Location): Unit =
    test("ap") {
      assertResult(List()) {
        ap(List())(List(1, 2))
      }
      assertResult(List()) {
        ap(List(plusa, plusA))(List())
      }
      assertResult(List('b', 'c')) {
        ap(List(plusa))(List(1, 2))
      }
      assertResult(List('b', 'B')) {
        ap(List(plusa, plusA))(List(1))
      }
      assertResult(List('b', 'c', 'B', 'C')) {  // ap sequences f firs)
        ap(List(plusa, plusA))(List(1, 2))
      }
    }
}

class Ex_6_1_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_6_1_Tests {

  import ex_6_1.{tuple2_fr, map2_fr, ap_fr}

  test_tuple2(tuple2_fr)

  test_map2(map2_fr)

  test_ap(ap_fr)
}
