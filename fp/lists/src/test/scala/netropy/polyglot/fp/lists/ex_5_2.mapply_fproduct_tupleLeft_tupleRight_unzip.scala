package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_5_2_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  // types and binary operators for testing
  private type A = Int
  private type B = Long
  private val incr: A => B = _.toLong + 1
  private val decr: A => B = _.toLong - 1

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_mapply
      (mapply: A => List[A => B] => List[B])
      (implicit loc: Location): Unit =
    test("mapply") {
      assertResult(List()) { mapply(1)(List()) }
      assertResult(List(2L)) { mapply(1)(List(incr)) }
      assertResult(List(2L, 0L)) { mapply(1)(List(incr, decr)) }
    }

  def test_fproduct
      (fproduct: List[A] => (A => B) => List[(A, B)])
      (implicit loc: Location): Unit =
    test("fproduct") {
      assertResult(List()) { fproduct(List())(incr) }
      assertResult(List((1, 2L))) { fproduct(List(1))(incr) }
      assertResult(List((1, 2L), (2, 3L))) { fproduct(List(1, 2))(incr) }
    }

  def test_tupleLeft
      (tupleLeft: (List[A], B) => List[(B, A)])
      (implicit loc: Location): Unit =
    test("tupleLeft") {
      assertResult(List()) { tupleLeft(List(), 3L) }
      assertResult(List((3L, 1))) { tupleLeft(List(1), 3L) }
      assertResult(List((3L, 1), (3L, 2))) { tupleLeft(List(1, 2), 3L) }
    }

  def test_tupleRight
      (tupleRight: (List[A], B) => List[(A, B)])
      (implicit loc: Location): Unit =
    test("tupleRight") {
      assertResult(List()) { tupleRight(List(), 3L) }
      assertResult(List((1, 3L))) { tupleRight(List(1), 3L) }
      assertResult(List((1, 3L), (2, 3L))) { tupleRight(List(1, 2), 3L) }
    }

  def test_unzip
      (unzip: List[(A, B)] => (List[A], List[B]))
      (implicit loc: Location): Unit =
    test("unzip") {
      assertResult((List(), List())) { unzip(List()) }
      assertResult((List(1), List(2L))) { unzip(List((1, 2L))) }
      assertResult((List(1, 2), List(2L, 3L))) { unzip(List((1, 2L), (2, 3L))) }
    }
}

class Ex_5_2_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_5_2_Tests {

  import ex_5_2.{mapply_m, fproduct_m, tupleLeft_m, tupleRight_m,
    unzip_fr, unzip_m}

  test_mapply(mapply_m)

  test_fproduct(fproduct_m)

  test_tupleLeft(tupleLeft_m)

  test_tupleRight(tupleRight_m)

  test_unzip(unzip_fr)

  test_unzip(unzip_m)
}
