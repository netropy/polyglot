package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class List_ex_5_4_Tests
    extends FunSuite
    with FunSuiteExt
    with Ex_2_1_Tests
    with Ex_2_5_Tests {

  import ex_1_3.List
  import ex_5_4.{length_mrd, product_mrd, mkString_mrd}

  test_length(length_mrd)

  test_product(product_mrd)

  test_mkString(mkString_mrd)
}
