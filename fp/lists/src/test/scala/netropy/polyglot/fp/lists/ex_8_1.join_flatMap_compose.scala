package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_8_1_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  // filtering/doubling list elements verifies type and operation of `flatMap`
  private type A = Int
  private type B = Long
  private type C = String
  private def f(a: A): List[B] = if (a % 2 == 0) Nil else List(a.toLong)
  private def g(b: B): List[C] = if (b % 3 == 0) List(s"$b$b") else List(s"$b")

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_join
      (join: List[List[A]] => List[A])
      (implicit loc: Location): Unit =
    test("join") {
      assertResult(List()) { join(List()) }
      assertResult(List(1)) { join(List(List(1))) }
      assertResult(List(1, 2)) { join(List(List(1), List(2))) }
    }

  def test_flatMap
      (flatMap: List[A] => (A => List[B]) => List[B])
      (implicit loc: Location): Unit =
    test("flatMap") {
      assertResult(List()) { flatMap(List())(f) }
      assertResult(List(1L)) { flatMap(List(1))(f) }
      assertResult(List(1L)) { flatMap(List(1, 2))(f) }
      assertResult(List(1L, 3L)) { flatMap(List(1, 2, 3))(f) }
    }

  def test_compose
    (compose: (A => List[B]) => (B => List[C]) => (A => List[C]))
      (implicit loc: Location): Unit =
    test("compose") {
      assertResult(List("1")) { compose(f)(g)(1) }
      assertResult(List()) { compose(f)(g)(2) }
      assertResult(List("33")) { compose(f)(g)(3) }
    }
}


class Ex_8_1_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_8_1_Tests {

  import ex_8_1.{join_fr, join_fl, flatMap_fr, flatMap_fl, compose_fr, compose_fl}

  test_join(join_fr)

  test_join(join_fl)

  test_flatMap(flatMap_fr)

  test_flatMap(flatMap_fl)

  test_compose(compose_fr)

  test_compose(compose_fl)
}
