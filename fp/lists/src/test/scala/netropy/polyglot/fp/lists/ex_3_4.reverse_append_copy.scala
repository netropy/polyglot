package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class List_ex_3_4_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_4_Tests
    with Ex_2_3_Tests {

  import ex_3_4.{reverseAppend_fl, reverse_fl, append_fr, copy_fr}

  test_reverseAppend(reverseAppend_fl)

  test_reverse(reverse_fl)

  test_append(append_fr)

  test_copy(copy_fr)
}
