package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_2_7_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  private type I = Int

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_startsWith
      (startsWith: (List[I], List[I]) => Boolean)
      (implicit loc: Location): Unit =
    test("startsWith") {
      assert(startsWith(List(), List()))
      assert(!startsWith(List(), List(1)))

      assert(startsWith(List(1), List()))
      assert(startsWith(List(1), List(1)))
      assert(!startsWith(List(1), List(0)))
      assert(!startsWith(List(1), List(1, 2)))

      assert(startsWith(List(1, 2), List()))
      assert(!startsWith(List(1, 2), List(0)))
      assert(startsWith(List(1, 2), List(1)))
      assert(!startsWith(List(1, 2), List(2)))
      assert(startsWith(List(1, 2), List(1, 2)))
      assert(!startsWith(List(1, 2), List(0, 2)))
      assert(!startsWith(List(1, 2), List(1, 3)))
      assert(!startsWith(List(1, 2), List(1, 2, 3)))

      assert(startsWith(List(1, 2, 3, 4, 5), List()))
      assert(startsWith(List(1, 2, 3, 4, 5), List(1)))
      assert(startsWith(List(1, 2, 3, 4, 5), List(1, 2)))
      assert(startsWith(List(1, 2, 3, 4, 5), List(1, 2, 3)))
      assert(startsWith(List(1, 2, 3, 4, 5), List(1, 2, 3, 4)))
      assert(startsWith(List(1, 2, 3, 4, 5), List(1, 2, 3, 4, 5)))
      assert(!startsWith(List(1, 2, 3, 4, 5), List(0, 2, 3, 4, 5)))
      assert(!startsWith(List(1, 2, 3, 4, 5), List(1, 2, 3, 4, 6)))
      assert(!startsWith(List(1, 2, 3, 4, 5), List(1, 2, 3, 4, 5, 6)))
    }

  def test_endsWith
      (endsWith: (List[I], List[I]) => Boolean)
      (implicit loc: Location): Unit =
    test("endsWith") {
      assert(endsWith(List(), List()))
      assert(!endsWith(List(), List(1)))

      assert(endsWith(List(1), List()))
      assert(endsWith(List(1), List(1)))
      assert(!endsWith(List(1), List(0)))
      assert(!endsWith(List(1), List(1, 2)))

      assert(endsWith(List(1, 2), List()))
      assert(!endsWith(List(1, 2), List(0)))
      assert(!endsWith(List(1, 2), List(1)))
      assert(endsWith(List(1, 2), List(2)))
      assert(endsWith(List(1, 2), List(1, 2)))
      assert(!endsWith(List(1, 2), List(0, 2)))
      assert(!endsWith(List(1, 2), List(1, 3)))
      assert(!endsWith(List(1, 2), List(0, 1, 2)))

      assert(endsWith(List(1, 2, 3, 4, 5), List()))
      assert(endsWith(List(1, 2, 3, 4, 5), List(5)))
      assert(endsWith(List(1, 2, 3, 4, 5), List(4, 5)))
      assert(endsWith(List(1, 2, 3, 4, 5), List(3, 4, 5)))
      assert(endsWith(List(1, 2, 3, 4, 5), List(2, 3, 4, 5)))
      assert(endsWith(List(1, 2, 3, 4, 5), List(1, 2, 3, 4, 5)))
      assert(!endsWith(List(1, 2, 3, 4, 5), List(0, 2, 3, 4, 5)))
      assert(!endsWith(List(1, 2, 3, 4, 5), List(1, 2, 3, 4, 6)))
      assert(!endsWith(List(1, 2, 3, 4, 5), List(0, 1, 2, 3, 4, 5)))
    }

  def test_hasSubsequence
      (hasSubsequence: (List[I], List[I]) => Boolean)
      (implicit loc: Location): Unit =
    test("hasSubsequence") {
      assert(hasSubsequence(List(), List()))
      assert(!hasSubsequence(List(), List(1)))

      assert(hasSubsequence(List(1), List()))
      assert(hasSubsequence(List(1), List(1)))
      assert(!hasSubsequence(List(1), List(0)))
      assert(!hasSubsequence(List(1), List(1, 2)))

      assert(hasSubsequence(List(1, 2), List()))
      assert(!hasSubsequence(List(1, 2), List(0)))
      assert(hasSubsequence(List(1, 2), List(1)))
      assert(hasSubsequence(List(1, 2), List(2)))
      assert(hasSubsequence(List(1, 2), List(1, 2)))
      assert(!hasSubsequence(List(1, 2), List(0, 2)))
      assert(!hasSubsequence(List(1, 2), List(1, 3)))
      assert(!hasSubsequence(List(1, 2), List(0, 1, 2)))

      assert(hasSubsequence(List(1, 2, 3, 4, 5), List()))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(1)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(1, 2)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(1, 2, 3)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(1, 2, 3, 4)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(1, 2, 3, 4, 5)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(2)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(2, 3)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(2, 3, 4)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(2, 3, 4, 5)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(3)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(3, 4)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(3, 4, 5)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(4)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(4, 5)))
      assert(hasSubsequence(List(1, 2, 3, 4, 5), List(5)))
      assert(!hasSubsequence(List(1, 2, 3, 4, 5), List(3, 2)))
      assert(!hasSubsequence(List(1, 2, 3, 4, 5), List(0, 2, 3, 4, 5)))
      assert(!hasSubsequence(List(1, 2, 3, 4, 5), List(1, 2, 3, 4, 6)))
      assert(!hasSubsequence(List(1, 2, 3, 4, 5), List(0, 1, 2, 3, 4, 5)))
    }
}

class Ex_2_7_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_7_Tests {

  import ex_2_7.{startsWith_tr, endsWith_tr, hasSubsequence_tr}

  test_startsWith(startsWith_tr)

  test_endsWith(endsWith_tr)

  test_hasSubsequence(hasSubsequence_tr)
}
