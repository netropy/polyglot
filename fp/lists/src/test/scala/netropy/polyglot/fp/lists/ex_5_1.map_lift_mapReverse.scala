package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_5_1_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  // wrapping list elements easily verifies type and operation of `map`
  private type A = Int
  private type B = Seq[Int]  // unrelated, information-preserving type
  private def wrap(a: A): B = Seq(a)

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_map
      (map: List[A] => (A => B) => List[B])
      (implicit loc: Location): Unit =
    test("map") {
      assertResult(List()) { map(List())(wrap) }
      assertResult(List(Seq(1))) { map(List(1))(wrap) }
      assertResult(List(Seq(1), Seq(2))) { map(List(1, 2))(wrap) }
    }
}

class Ex_5_1_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_5_1_Tests {

  import ex_5_1.map_fr

  test_map(map_fr)
}
