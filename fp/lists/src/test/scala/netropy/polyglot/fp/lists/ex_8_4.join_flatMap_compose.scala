package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_8_4_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_8_1_Tests {

  import ex_8_4.{
    join_fm, join_c, flatMap_jm, flatMap_c, compose_fm, compose_jm}

  test_join(join_fm)

  test_join(join_c)

  test_flatMap(flatMap_jm)

  test_flatMap(flatMap_c)

  test_compose(compose_fm)

  test_compose(compose_jm)
}
