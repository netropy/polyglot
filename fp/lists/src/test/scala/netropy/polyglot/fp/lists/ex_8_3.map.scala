package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_8_3_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_5_1_Tests {

  import ex_8_3.map_fm

  test_map(map_fm)
}
