package netropy.polyglot.fp

import munit.{FunSuite, Compare}

/** Convenience methods for munit FunSuite tests as mixin.
  *
  * More curried and expectation-first equals-assertions to support usage:
  * ```
  *   assertEquals(actual, expected)  // = munit.Assertions
  *   assertEquals(actual) {
  *     ...expected...
  *   }
  *   assertResult(expected, actual)  // as in ScalaTest
  *   assertResult(expected) {
  *     ...actual...
  *   }
  * ```
  *
  * See design notes in netropy.polyglot.testing.munit.FunSuiteExt
  */
trait FunSuiteExt {
  this: FunSuite =>

  /** A _curried_ equals assertion taking the expected value _second_. */
  def assertEquals[A, B](act: A)(exp: B)(implicit c: Compare[A, B]): Unit =
    assertEquals(act, exp)

  /** An _uncurried_ equals assertion taking the expected value _first_. */
  def assertResult[A, B](exp: A, act: B)(implicit c: Compare[B, A]): Unit =
    assertEquals(act, exp)

  /** A _curried_ equals assertion taking the expected value _first_. */
  def assertResult[A, B](exp: => A)(act: B)(implicit c: Compare[B, A]): Unit =
    assertEquals(act, exp)
}
