package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_7_2_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_8_Tests {

  import ex_7_2.{zipAdd_tr}

  test_zipAdd(zipAdd_tr)
}
