package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_6_3_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_6_1_Tests {

  import ex_6_3.{tuple2_m2, tuple2_am, map2_pm, map2_am, ap_pm, ap_m2}

  test_tuple2(tuple2_m2)

  test_tuple2(tuple2_am)

  test_map2(map2_pm)

  test_map2(map2_am)

  test_ap(ap_pm)

  test_ap(ap_m2)
}
