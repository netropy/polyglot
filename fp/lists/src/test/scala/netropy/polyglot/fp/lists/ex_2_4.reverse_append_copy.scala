package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_2_4_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List

  def test_append
      (append: (List[Int], List[Int]) => List[Int])
      (implicit loc: Location): Unit =
    test("append") {
      assertResult(List()) { append(List(), List()) }
      assertResult(List(1)) { append(List(1), List()) }
      assertResult(List(4)) { append(List(), List(4)) }
      assertResult(List(1, 4)) { append(List(1), List(4)) }
      assertResult(List(1, 2)) { append(List(1, 2), List()) }
      assertResult(List(3, 4)) { append(List(), List(3, 4)) }
      assertResult(List(1, 2, 3)) { append(List(1, 2), List(3)) }
      assertResult(List(2, 3, 4)) { append(List(2), List(3, 4)) }
      assertResult(List(1, 2, 3, 4)) { append(List(1, 2), List(3, 4)) }
    }

  def test_reverseAppend
      (reverseAppend: (List[Int], List[Int]) => List[Int])
      (implicit loc: Location): Unit =
    test("reverseAppend") {
      assertResult(List()) { reverseAppend(List(), List()) }
      assertResult(List(1)) { reverseAppend(List(1), List()) }
      assertResult(List(4)) { reverseAppend(List(), List(4)) }
      assertResult(List(1, 4)) { reverseAppend(List(1), List(4)) }
      assertResult(List(2, 1)) { reverseAppend(List(1, 2), List()) }
      assertResult(List(3, 4)) { reverseAppend(List(), List(3, 4)) }
      assertResult(List(2, 1, 3)) { reverseAppend(List(1, 2), List(3)) }
      assertResult(List(2, 3, 4)) { reverseAppend(List(2), List(3, 4)) }
      assertResult(List(2, 1, 3, 4)) { reverseAppend(List(1, 2), List(3, 4)) }
    }

  def test_reverse
      (reverse: List[Int] => List[Int])
      (implicit loc: Location): Unit =
    test("reverse") {
      assertResult(List()) { reverse(List()) }
      assertResult(List(1)) { reverse(List(1)) }
      assertResult(List(2, 1)) { reverse(List(1, 2)) }
      assertResult(List(3, 2, 1)) { reverse(List(1, 2, 3)) }
    }

  def test_copy
      (copy: List[Int] => List[Int])
      (implicit loc: Location): Unit =
    test("copy") {
      assertResult(List()) { copy(List()) }
      assertResult(List(1)) { copy(List(1)) }
      assertResult(List(1, 2)) { copy(List(1, 2)) }
      assertResult(List(1, 2, 3)) { copy(List(1, 2, 3)) }
    }
}

class Ex_2_4_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_4_Tests {

  import ex_2_4.{reverse_tr, reverseAppend_tr, append_r, append_tr, copy_tr}

  test_append(append_r)

  test_reverseAppend(reverseAppend_tr)

  test_reverse(reverse_tr)

  test_append(append_tr)

  test_copy(copy_tr)
}
