package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_4_1_reduce_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  /*
   * Notes: Types and binary operator for testing `reduce` (same as `fold`)
   *
   * Sequence concatenation is an associative but non-commutative operation
   * and, hence, can be used to verify that reduce preserves the input data's
   * left/right ordering in the output (unlike commutative unlike + or *).
   *
   * Type compliance of reduce is verified by a using a supertype of the list
   * data for the operator.
   */

  // type arguments [C, C1] required by scala 2.13 for overloaded methods
  type C = Vector[Int]  // subtype
  type C1 = Seq[Int]  // covariant supertype
  private val concat: (C1, C1) => C1 = _ ++ _ // +assoc, -commutative

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_reduceCons
      (reduce: Cons[C] => ((C1, C1) => C1) => C1)
      (implicit loc: Location): Unit =
    test("reduceCons") {
      assertResult(Seq()) { reduce(Cons(Vector(), Nil))(concat) }
      assertResult(Seq(1)) { reduce(Cons(Vector(1), Nil))(concat) }
      assertResult(Seq(1, 2)) {
        reduce(Cons(Vector(1), List(Vector(2))))(concat)
      }
    }

  def test_reduceList
      (reduce: List[C] => ((C1, C1) => C1) => C1)
      (implicit loc: Location): Unit =
    test("reduceList") {
      intercept[IllegalArgumentException] { reduce(List())(concat) }
      assertResult(Seq()) { reduce(List(Vector()))(concat) }
      assertResult(Seq(1)) { reduce(List(Vector(1)))(concat) }
      assertResult(Seq(1, 2)) { reduce(List(Vector(1), Vector(2)))(concat) }
    }

  def test_reduceOption
      (reduce: List[C] => ((C1, C1) => C1) => Option[C1])
      (implicit loc: Location): Unit =
    test("reduceOption") {
      assertResult(None) { reduce(List())(concat) }
      assertResult(Some(Seq())) { reduce(List(Vector()))(concat) }
      assertResult(Some(Seq(1))) { reduce(List(Vector(1)))(concat) }
      assertResult(Some(Seq(1, 2))) {
        reduce(List(Vector(1), Vector(2)))(concat)
      }
    }
}

class Ex_4_1_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_4_1_reduce_Tests {

  import ex_4_1.{reduce_f, reduceOption_f}
  import ex_1_3.List
  import List.Cons

  // type arguments [C, C1] required by scala 2.13 for overloaded methods

  test_reduceCons(reduce_f[C, C1])

  test_reduceList(reduce_f[C, C1])

  test_reduceOption(reduceOption_f)
}
