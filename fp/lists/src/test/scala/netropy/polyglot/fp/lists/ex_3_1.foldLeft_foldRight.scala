package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_3_1_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List
  import List.{Nil, Cons}

  /*
   * Notes: Types and binary operators for testing `foldLeft`, `foldRight`
   *
   * List construction is a non-associative operation and, hence, can be used
   * to verify that foldLeft/foldRight adhere to their order of operator
   * application.  foldLeft, though, requires a list constructor that takes
   * the list argument first, which is sometimes called `snoc` (<> `cons`).
   *
   * To verify the non-associativity, compare against string concatenation:
   * ```
   *    snoc(snoc(nil, 2), 1) == cons(1, cons(2, nil))
   *    snoc(snoc(nil, 1), 2) != cons(1, cons(2, nil))
   *       (("" + "1") + "2") == ("1" + ("2" + ""))
   * ```
   *
   * Type compliance of foldLeft/foldRight is verified by using unrelated
   * types for the list data and the result.
   */

  private type I = Int
  private type LI = List[Int]
  private val cons: (I, LI) => LI = (i, is) => Cons(i, is)
  private val snoc: (LI, I) => LI = (is, i) => Cons(i, is)  // swapped

  // curried method expands to function: (A)(B)C ~> A => B => C

  def test_foldLeft
      (foldLeft: (LI, List[I]) => ((LI, I) => LI) => LI)
      (implicit loc: Location): Unit =
    test("foldLeft") {
      assertResult(List()) { foldLeft(List(), List())(snoc) }
      assertResult(List(0)) { foldLeft(List(0), List())(snoc) }
      assertResult(List(1)) { foldLeft(List(), List(1))(snoc) }
      assertResult(List(1, 0)) { foldLeft(List(0), List(1))(snoc) }
      assertResult(List(1, 2)) { foldLeft(List(), List(2, 1))(snoc) }
      assertResult(List(1, 2, 0)) { foldLeft(List(0), List(2, 1))(snoc) }
    }

  def test_foldRight
      (foldRight: (List[I], LI) => ((I, LI) => LI) => LI)
      (implicit loc: Location): Unit =
    test("foldRight") {
      assertResult(List()) { foldRight(List(), List())(cons) }
      assertResult(List(0)) { foldRight(List(), List(0))(cons) }
      assertResult(List(1)) { foldRight(List(1), List())(cons) }
      assertResult(List(1, 0)) { foldRight(List(1), List(0))(cons) }
      assertResult(List(1, 2)) { foldRight(List(1, 2), List())(cons) }
      assertResult(List(1, 2, 0)) { foldRight(List(1, 2), List(0))(cons) }
    }
}

class Ex_3_1_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_3_1_Tests {

  import ex_3_1.{foldLeft_tr, foldLeft_r, foldRight_tr, foldRight_r}

  test_foldLeft(foldLeft_tr)

  test_foldLeft(foldLeft_r)

  test_foldRight(foldRight_tr)

  test_foldRight(foldRight_r)
}
