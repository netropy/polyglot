package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_2_3_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List  // import List[A], List[A]() from ex_2_3
  import List.{Nil}

  def test_last
      (last: List[Int] => Int)
      (implicit loc: Location): Unit =
    test("last") {
      intercept[IllegalArgumentException] { last(List()) }
      assertResult(1) { last(List(1)) }
      assertResult(2) { last(List(1, 2)) }
      assertResult(3) { last(List(1, 2, 3)) }
    }

  def test_init
      (init: List[Int] => List[Int])
      (implicit loc: Location): Unit =
    test("init") {
      intercept[IllegalArgumentException] { init(List()) }
      assertResult(List()) { init(List(1)) }
      assertResult(List(1)) { init(List(1, 2)) }
      assertResult(List(1, 2)) { init(List(1, 2, 3)) }
    }

  def test_take
      (take: (List[Int], Int) => List[Int])
      (implicit loc: Location): Unit =
    test("take") {
      intercept[IllegalArgumentException] { take(Nil, -1) }
      intercept[IllegalArgumentException] { take(List(1), -2) }

      intercept[IllegalArgumentException] { take(Nil, 1) }
      intercept[IllegalArgumentException] { take(List(1), 2) }

      assertResult(List()) { take(Nil, 0) }
      assertResult(List(1)) { take(List(1), 1) }
      assertResult(List(1, 2)) { take(List(1, 2), 2) }

      assertResult(List()) { take(List(1), 0) }
      assertResult(List(1)) { take(List(1, 2), 1) }

      assertResult(List()) { take(List(1, 2), 0) }
    }

  def test_takeWhile
      (takeWhile: List[Int] => (Int => Boolean) => List[Int])
      (implicit loc: Location): Unit =
    test("takeWhile") {
      intercept[IllegalArgumentException] { takeWhile(Nil)(null) }
      intercept[IllegalArgumentException] { takeWhile(List(1))(null) }

      assertResult(List()) { takeWhile(List())((a: Any) => true) }
      assertResult(List()) { takeWhile(List())((a: Any) => false) }

      assertResult(List(1)) { takeWhile(List(1))((a: Any) => true) }
      assertResult(List()) { takeWhile(List(1))((a: Any) => false) }
      assertResult(List(1)) { takeWhile(List(1))((a: Any) => a == 1) }
      assertResult(List()) { takeWhile(List(1))((a: Any) => a != 1) }

      assertResult(List(1, 2)) { takeWhile(List(1, 2))((a: Any) => true) }
      assertResult(List()) { takeWhile(List(1, 2))((a: Any) => false) }
      assertResult(List(1)) { takeWhile(List(1, 2))((a: Any) => a == 1) }
      assertResult(List()) { takeWhile(List(1, 2))((a: Any) => a != 1) }
    }

  def test_filter
      (filter: (List[Int], Int => Boolean) => List[Int])
      (implicit loc: Location): Unit =
    test("filter") {
      assertResult(List()) {
        filter(List(), (a: Int) => true)
      }
      assertResult(List()) {
        filter(List(), (a: Int) => false)
      }
      assertResult(List(1)) {
        filter(List(1), (a: Int) => true)
      }
      assertResult(List()) {
        filter(List(1), (a: Int) => false)
      }
      assertResult(List(1, 2, 3, 4)) {
        filter(List(1, 2, 3, 4), (a: Int) => true)
      }
      assertResult(List()) {
        filter(List(1, 2, 3, 4), (a: Int) => false)
      }
      assertResult(List(2, 4)) {
        filter(List(1, 2, 3, 4), (a: Int) => a % 2 == 0)
      }
      assertResult(List(1, 3)) {
        filter(List(1, 2, 3, 4), (a: Int) => a % 2 != 0)
      }
    }
}

class Ex_2_3_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_3_Tests {

  import ex_2_3.{last_tr, init_r, take_r, takeWhile_r, filter_r}

  test_last(last_tr)

  test_init(init_r)

  test_take(take_r)

  test_takeWhile(takeWhile_r)

  test_filter(filter_r)
}
