package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_4_2_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_5_Tests {

  import ex_4_2.sum_rd

  test_sum(sum_rd)  // also covers sum_rd(Cons[Int])
}
