package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class List_ex_3_3_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_5_Tests
    with Ex_2_1_Tests {

  import ex_3_3.{length_f, sum_f, product_f, toStrings_fr, mapIncr_fr,
    mkString_fl}

  test_length(length_f)

  test_sum(sum_f)

  test_product(product_f)

  test_toStrings(toStrings_fr)

  test_mapIncr(mapIncr_fr)

  test_mkString(mkString_fl)
}
