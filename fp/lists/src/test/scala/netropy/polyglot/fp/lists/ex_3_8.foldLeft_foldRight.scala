package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class Ex_3_8_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_3_1_Tests {

  import ex_3_8.{foldLeft_fr, foldRight_fl, foldLeft_fm, foldRight_fm}

  test_foldLeft(foldLeft_fr)

  test_foldRight(foldRight_fl)

  test_foldLeft(foldLeft_fm)

  test_foldRight(foldRight_fm)
}
