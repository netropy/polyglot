package netropy.polyglot.fp.lists

import munit.FunSuite
import netropy.polyglot.fp.FunSuiteExt  // sugar

import ex_1_1.{List, Nil, Cons}
import List.{unit, empty}

class Ex_1_1_Tests
    extends FunSuite
    with FunSuiteExt {

  def compile_test_List_Nil_Cons(): Unit = {
    List(1) match {  // upcasts needed
      case Nil => ()
      case Cons(h, t) => ()
    }
    (Nil: List[Int]) match {  // upcasts needed
      case Nil => ()
      case Cons(h, t) => ()
    }
    (Cons(1, Nil): List[Int]) match {  // upcasts needed
      case Nil => ()
      case Cons(h, t) => ()
    }
  }

  test("List|0|: List, Nil, Cons, head, tail, headOption, tailOption") {
    assertResult(Nil) { List() }
    intercept[UnsupportedOperationException] { Nil.head }
    intercept[UnsupportedOperationException] { Nil.tail }
    assertResult(None) { Nil.headOption }
    assertResult(None) { Nil.tailOption }
  }

  test("List|1|: List, Nil, Cons, head, tail, headOption, tailOption") {
    intercept[IllegalArgumentException] { Cons(1, null) }
    assertResult(Cons(1, Nil)) { List(1) }
    assertResult(1) { List(1).head }
    assertResult(Nil) { List(1).tail }
    assertResult(Some(1)) { List(1).headOption }
    assertResult(Some(Nil)) { List(1).tailOption }
  }

  test("List|2|: List, Nil, Cons, head, tail, headOption, tailOption") {
    assertResult(Cons(2, Cons(1, Nil))) { List(2, 1) }
    assertResult(2) { List(2, 1).head }
    assertResult(List(1)) { List(2, 1).tail }
    assertResult(Some(2)) { List(2, 1).headOption }
    assertResult(Some(List(1))) { List(2, 1).tailOption }
  }

  test("List: empty, unit") {
    assertResult(List()) { empty }
    assertResult(List(1)) { unit(1) }
  }
}
