package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class List_ex_3_5_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_2_Tests
    with Ex_2_3_Tests {

  import ex_3_5.{
    drop_fl, drop_fl_lazy, dropWhile_fl, dropWhile_fl_lazy,
    take_fl, take_fl_lazy, takeWhile_fl, takeWhile_fl_lazy,
    last_fl, init_fl,
    filter_fr, filter_fl}

  test_drop(drop_fl)

  test_drop(drop_fl_lazy)

  test_dropWhile(dropWhile_fl)

  test_dropWhile(dropWhile_fl_lazy)

  test_take(take_fl)

  test_take(take_fl_lazy)

  test_takeWhile(takeWhile_fl)

  test_takeWhile(takeWhile_fl_lazy)

  test_last(last_fl)

  test_init(init_fl)

  test_filter(filter_fr)

  test_filter(filter_fl)
}
