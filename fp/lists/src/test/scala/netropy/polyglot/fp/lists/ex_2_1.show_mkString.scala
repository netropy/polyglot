package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_2_1_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List  // import List[A], List[A]() from ex_2_1

  def test_show
      (show: List[Int] => String)
      (implicit loc: Location) : Unit =
    test("show") {
      assertResult("[]") { show(List()) }
      assertResult("[1,]") { show(List(1)) }
      assertResult("[1,2,]") { show(List(1, 2)) }
      assertResult("[1,2,3,]") { show(List(1, 2, 3)) }
    }

  def test_mkString
      (mkString: (List[Int], String, String, String) => String)
      (implicit loc: Location): Unit =
    test("mkString") {
      assertResult("[]") { mkString(List(), "[", "|", "]") }
      assertResult("[1]") { mkString(List(1), "[", "|", "]") }
      assertResult("[1|2]") { mkString(List(1, 2), "[", "|", "]") }
      assertResult("[1|2|3]") { mkString(List(1, 2, 3), "[", "|", "]") }
    }
}

class Ex_2_1_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_1_Tests {

  import ex_2_1.{show_r, show_tr, mkString_r, mkString_tr}

  test_show(show_r)
  test_show(show_tr)

  test_mkString(mkString_r)
  test_mkString(mkString_tr)
}
