package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

trait Ex_2_2_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_3.List  // import List[A], List[A](), Nil from ex_2_2
  import List.{Nil}

  def test_setHead
      (setHead: (List[Int], Int) => List[Int])
      (implicit loc: Location): Unit =
    test("setHead") {
      intercept[IllegalArgumentException] { setHead(Nil, 1) }
      assertResult(List(2)) { setHead(List(1), 2) }
      assertResult(List(3, 1)) { setHead(List(2, 1), 3) }
      assertResult(List('a', 1): List[AnyVal]) {  // munit Compare needs upcast
        setHead(List(2, 1), 'a')  // type widens
      }
    }

  def test_setTail
      (setTail: (List[Int], List[Int]) => List[Int])
      (implicit loc: Location): Unit =
    test("setTail") {
      intercept[IllegalArgumentException] { setTail(Nil, List()) }
      assertResult(List(1, 2)) { setTail(List(1), List(2)) }
      assertResult(List(2)) { setTail(List(2, 1), Nil) }
      assertResult(List('a', 2): List[AnyVal]) {  // munit Compare needs upcast
        setTail(List('a'), List(2))  // type widens
      }
    }

  def test_drop
      (drop: (List[Int], Int) => List[Int])
      (implicit loc: Location): Unit =
    test("drop") {
      intercept[IllegalArgumentException] { drop(Nil, -1) }
      intercept[IllegalArgumentException] { drop(List(1), -2) }

      intercept[IllegalArgumentException] { drop(Nil, 1) }
      intercept[IllegalArgumentException] { drop(List(1), 2) }

      assertResult(List()) { drop(Nil, 0) }
      assertResult(List(1)) { drop(List(1), 0) }
      assertResult(List(1, 2)) { drop(List(1, 2), 0) }

      assertResult(List()) { drop(List(1), 1) }
      assertResult(List(2)) { drop(List(1, 2), 1) }

      assertResult(List()) { drop(List(1, 2), 2) }
    }

  def test_dropWhile
    (dropWhile: (List[Int], Int => Boolean) => List[Int])
    (implicit loc: Location): Unit =
    test("dropWhile") {
      intercept[IllegalArgumentException] { dropWhile(Nil, null) }
      intercept[IllegalArgumentException] { dropWhile(List(1), null) }

      assertResult(List()) { dropWhile(List(), (a: Any) => true) }
      assertResult(List()) { dropWhile(List(), (a: Any) => false) }

      assertResult(List()) { dropWhile(List(1), (a: Any) => true) }
      assertResult(List(1)) { dropWhile(List(1), (a: Any) => false) }
      assertResult(List()) { dropWhile(List(1), (a: Any) => a == 1) }
      assertResult(List(1)) { dropWhile(List(1), (a: Any) => a != 1) }

      assertResult(List()) { dropWhile(List(1, 2), (a: Any) => true) }
      assertResult(List(1, 2)) { dropWhile(List(1, 2), (a: Any) => false) }
      assertResult(List(2)) { dropWhile(List(1, 2), (a: Any) => a == 1) }
      assertResult(List(1, 2)) { dropWhile(List(1, 2), (a: Any) => a != 1) }
    }
}

class Ex_2_2_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_2_2_Tests {

  import ex_2_2.{setHead, setTail, drop_tr, dropWhile_tr}

  test_setHead(setHead)
  test_setTail(setTail)

  test_drop(drop_tr)
  test_dropWhile(dropWhile_tr)
}
