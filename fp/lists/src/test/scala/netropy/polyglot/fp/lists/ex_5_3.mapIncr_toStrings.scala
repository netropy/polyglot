package netropy.polyglot.fp.lists

import munit.{FunSuite, Location}
import netropy.polyglot.fp.FunSuiteExt  // sugar

class List_ex_5_3_Tests
    extends FunSuite
    with FunSuiteExt
    with Ex_2_5_Tests {

  import ex_1_3.List
  import ex_5_3.{mapIncr_m, toStrings_m}

  test_mapIncr(mapIncr_m)

  test_toStrings(toStrings_m)
}
