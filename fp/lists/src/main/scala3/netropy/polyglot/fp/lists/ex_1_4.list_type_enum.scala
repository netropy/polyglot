package netropy.polyglot.fp.lists


object ex_1_4 {

  /*
   * Notes: Scala3 `enum` OO/FP-style design of core List type.
   *
   * - List GADT as enum with implementing case values
   * - blended mix of singleton + parameterized enum values
   * - all accessors as enum methods, no external functions
   * - only pattern-matching, no requiring or down-casting of argument
   * - checking for null illegal argument in primary constructor (body)
   * - 2 natural scopes for everything: enum, companion
   *
   * - downside of enum hiding the subtypes of its data constructors:
   *   can no longer overload external functions to specific values.
   *
   * - total: 55 lines for code + minimal scaladoc
   *
   * See summary below on use of Scala3 enum for GADTs.
   */

  /** An immutable, singly-linked list (a.k.a. LinkedList). */
  enum List[+A] {

    case Nil
    case Cons(h: A, t: List[A])

    this match {
      case Cons(_, null) => throw IllegalArgumentException("Cons(_, null)")
      case _ => ()
    }

    /** Returns the first element of this non-empty list.
      * @throws [[UnsupportedOperationException]] if Nil
      */
    def head: A =
      this match {
        case Cons(h, t) => h
        case Nil => throw UnsupportedOperationException(s"$this.head")
      }

    /** Returns the rest of this non-empty list.
      * @throws [[UnsupportedOperationException]] if Nil
      */
    def tail: List[A] =
      this match {
        case Cons(h, t) => t
        case Nil => throw UnsupportedOperationException(s"$this.tail")
      }

    /** Returns an [[Option]] of the first element of this list. */
    def headOption: Option[A] =
      this match {
        case Cons(h, t) => Some(h)
        case Nil => None
      }

    /** Returns an [[Option]] of the rest of this list. */
    def tailOption: Option[List[A]] =
      this match {
        case Cons(h, t) => Some(t)
        case Nil => None
      }
  }

  object List {

    /** Creates a list from the sequence of values. */
    def apply[A](as: A*): List[A] =
      as.foldRight(empty[A])((a, l) => Cons(a, l))

    /** Also called `pure` for Applicatives (with `unit: List[Unit]`). */
    def unit[A](a: A): List[A] = Cons(a, Nil)

    def empty[A]: List[A] = Nil
  }
}
