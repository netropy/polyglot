package netropy.polyglot.fp.lists


object ex_8_3 {
  import ex_1_3.List
  import List.unit
  import ex_8_1.{flatMap_fr, compose_fr}

  /*
   * Notes: Functor Map via Monad primitives (redundant)
   *
   * `map_jm`: Cannot implement `map` just with `join`, hence both needed.
   */

  def map_fm[A, B]
      (as: List[A])
      (f: A => B): List[B] =
    flatMap_fr(as)(a => unit(f(a)))

  def map_c[A, B]
      (as: List[A])
      (f: A => B): List[B] =
    compose_fr(identity[List[A]])(a => unit(f(a)))(as)
}
