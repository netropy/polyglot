package netropy.polyglot.fp.lists


object ex_3_8 {
  import ex_1_3.List
  import ex_3_1.{foldLeft_tr, foldRight_r}
  import ex_3_2.{foldMap_fl}

  /*
   * Notes: foldLeft/Right purely in terms of foldRight/Left
   *
   * Ex_3_1 implemented foldLeft/Right in terms of foldRight/Left using the
   * tail-recursive helper function: `reverse_tr`.  Ex_3_4 then showed how to
   * implement reverse in terms of foldLeft: `reverse_fl`.
   *
   * Therefore, only foldRight has been truly expressed in terms of foldLeft:
   * - foldRight: foldLeft_tr + reverse_fl  // ok, all foldLeft
   * - foldLeft:  foldRight_r + reverse_??  // need a right-recursive reverse
   *
   * Composing closures:
   * -------------------
   *
   * Without an intermediate data structure or collection, the list elements
   * can always be stored in a chain of _closures_ `f_i` (`->` == invokes):
   * ```
   *   f_n -> f_n-1 -> ... -> f_1 -> f_0  // built from right
   *   f_0 <- f_1 <- ... <- f_n-1 <- f_n  // built from left
   * ```
   *
   * Each closure `f_i: B => B` captures:
   * - the current list element `a_i: A`
   * - the previous closure `f_i-1: B => B`
   * - the (curried) combine operation `op: A => B => B` passed to foldRight
   *   [`op: B => A => B` to foldLeft]
   *
   * Note that the partially applied `op(a, _)` [`op(_, a)`] yields `B => B`.
   *
   * So, `f_i` has a choice in which order to compose `op(a_i, _)` and `f_i-1`:
   * ```
   *   1) f_i = (op(a_i, _)) compose f_i-1  // b => op(a_i, f_i-1(b))
   *   2) f_i = (op(a_i, _)) andThen f_i-1  // b => f_i-1(op(a_i, b))
   * ```
   *
   * Note how 2) expands to applying `op` to descending `a_i`, `a_i-1`, ...:
   * ```
   *   1) b => ... op(a_i, op(a_i-1, ...))
   *   2) b => ... op(a_i-1, op(a_i, ...))
   * ```
   *
   * Therefore, 2) reverses the order of application of `op` to `a_i`.
   *
   * If we pass `f_0 = identity[B]` for lists `a_n ... a_1` and a_1 ... a_n`
   *
   * ```
   *   foldRight( List(a_n ... a_1), identity[B])(op) ==
   *     b => identity(op(a_1, ... op(a_n-1, op(a_n, b))))
   *
   *   foldleft( identity[B], List(a_1 ... a_n) )(op) ==
   *     b => identity(op(op(op(a_n, b), a_n-1), ... a_1))
   * ```
   * ```
   *   b => op(a_0, op(a_1, ... op(a_n-1, op(a_n, b))))  // built from right
   *   b => op(op(op(op(a_n, b), a_n-1), ... a_1), a_0)  // built from left
   * ```



   *
   * When the final closure `f_n`, as returned by the fold, is applied
   * - it invokes `op` on the start value `z` and `a_n`
   * - passes the result to`f_n-1`, which combines it with `a_n-1` and so on.
   *
   * Hence, this chain of closures invokes operation `op` on the list elements
   * `a_0 ... a_n` in reversed order:
   * ```
   *   op(op(op(z, a_n), a_n-1), ... a_0)  // as constructed by foldRight
   *   op(a_0, ... op(a_n-1, op(a_n, z)))  // as constructed by foldLeft
   * ```
   *
   *
   * Note how this closure-building fold is not recursive!  Closure `f_i`
   * knows the next instance `f_i-1` only as captured parameter `f`, not as a
   * defined method or function value.  (Similar to a fixpoint combinator.)
   *
   * Efficiency:
   * -----------
   *
   * Tail-call optimization should apply here: `f(op(...))` is the closure's
   * only and last call.
   *
   * If so, the nested `op` terms should evaluate as _iteration_ over `n..0`:
   * - foldLeft:  op(op(op(z, a_n), a_n-1), ... a_0)
   * - foldRight: op(a_0, ... op(a_n-1, op(a_n, z)))
   *
   * => `foldLeft_{fr,fl}` can be expected to be stack-safe.
   *
   * Using closures to store the list elements along with other captured
   * references likely has higher construction overhead and memory footprint
   * than collecting the elements in an efficiently growable `Seq`, such as
   * [[immutable.Vector]] or [[mutable.ArrayDeque]], and then use `reverse`
   * or `reverseIterator` for traversal.
   *
   * => `foldLeft_{fr,fl}` are likely less efficient than using a collection.
   */

  def foldRight_fl[A, B]
      (as: List[A], z: B)
      (op: (A, B) => B): B = {

    // syntax variants:
    // foldLeft_tr((b: B) => b, as)((f, a) => b => f(op(a, b)))(z)
    // foldLeft_tr(identity[B], as)((f, a) => b => f(op(a, b)))(z)  // dotty
    // foldLeft_tr(identity[B](_), as)((f, a) => f.compose(b => op(a, b)))(z)
    // foldLeft_tr(identity[B](_), as)((f, a) => f.compose(op(a, _)))(z)
    // foldLeft_tr(identity[B](_), as)((f, a) => (op(a, _)).andThen(f))(z)

    foldLeft_tr(identity[B](_), as)((f, a) => b => f(op(a, b)))(z)
  }

  def foldLeft_fr[A, B]
      (z: B, as: List[A])
      (op: (B, A) => B): B = {

    // syntax variants:
    // foldRight_r(as, (b: B) => b)((a, f) => b => f(op(b, a)))(z)
    // foldRight_r(as, identity[B])((a, f) => b => f(op(b, a)))(z)  // dotty
    // foldRight_r(as, identity[B](_))((a, f) => f.compose(b => op(b, a)))(z)
    // foldRight_r(as, identity[B](_))((a, f) => f.compose(op(_, a)))(z)
    // foldRight_r(as, identity[B](_))((a, f) => (op(_, a)).andThen(f))(z)

    foldRight_r(as, identity[B](_))((a, f) => b => f(op(b, a)))(z)
  }

  def foldRight_fm[A, B]
      (as: List[A], z: B)
      (op: (A, B) => B): B = {

    // syntax variants:
    //foldMap_fl(as)(a => b => op(a, b))(b => b, (f, g) => b => f(g(b)))(z)
    //foldMap_fl(as)(a => op(a, _))(identity[B], (f, g) => f.compose(g))(z)
    //foldMap_fl(as)(op.curried)(identity[B], _ compose _)(z)  // only foldR

    foldMap_fl(as)(a => op(a, _))(identity[B], _ compose _)(z)
  }

  def foldLeft_fm[A, B]
      (z: B, as: List[A])
      (op: (B, A) => B): B = {

    // syntax variants:
    //foldMap_fl(as)(a => b => op(b, a))(b => b, (f, g) => b => g(f(b)))(z)
    //foldMap_fl(as)(a => op(_, a))(identity[B], (f, g) => g.compose(f))(z)

    foldMap_fl(as)(a => op(_, a))(identity[B], _ andThen _)(z)
  }
}
