package netropy.polyglot.fp.lists

import scala.math.BigInt


object ex_2_5 {
  import ex_1_3.List
  import List.{Nil, Cons, head, tail}
  import ex_2_4.{reverse_tr}

  /*
   * Recursive MapReduce-style functions, some with order preservation
   * - right-/recursive and left-/tail-recursive versions
   *
   * `length_r`, `length_tr`, `sum_r`, `sum_tr`, `product_r`, `product_tr`
   * left-recursive reduce with associative operator, neutral element (monoid)
   *
   * `toStrings_r`, `toStrings_tr`, `mapIncr_r`, `mapIncr_tr`
   * right-recursive mapping with non-associative Cons, Nil
   * left-/tail-recursive version requires extra reversal pass (-> leftFold)
   */

  /** Returns the length of the list. */
  def length_r[A](as: List[A]): Int =
    if (as == Nil) 0 else 1 + length_r(tail(as))

  /** Returns the sum of the list of [[Int]]. */
  def sum_r(as: List[Int]): Int =
    if (as == Nil) 0 else head(as) + sum_r(tail(as))

  /** Returns the [[BigInt]] product of the list of [[Int]]. */
  def product_r(as: List[Int]): BigInt =
    as match {
      case Nil => 1
      case Cons(h, t) => h * product_r(t)
    }

  /** Replaces each element in the list with its [[String]] representation. */
  def toStrings_r[A](as: List[A]): List[String] =
    as match {
      case Nil => Nil
      case Cons(h, t) => Cons(h.toString, toStrings_r(t))
    }

  /** Increments each value in the list of [[Int]]s by `1`. */
  def mapIncr_r(is: List[Int]): List[Int] =
    is match {
      case Nil => Nil
      case Cons(h, t) => Cons(h + 1, mapIncr_r(t))
    }

  def length_tr[A](as: List[A]): Int = {

    @annotation.tailrec
    def go[A](as: List[A], z: Int): Int =
      as match {
        case Cons(h, t) => go(t, z + 1)
        case Nil => z
      }
    // associative '+' allows for either direction (after mapping a=>1)
    go(as, 0)  // neutral element
  }

  def sum_tr(as: List[Int]): Int = {

    @annotation.tailrec
    def go(as: List[Int], z: Int): Int =
      as match {
        case Cons(h, t) => go(t, h + z)
        case Nil => z
      }
    // associative '+' allows for either direction
    go(as, 0)  // neutral element
  }

  def product_tr(as: List[Int]): BigInt = {

    @annotation.tailrec
    def go(as: List[Int], z: BigInt): BigInt =
      as match {
        case Cons(h, t) => go(t, h * z)
        case Nil => z
      }
    // associative '*' allows for either direction
    go(as, 1)  // neutral element
  }

  def toStrings_tr[A](as: List[A]): List[String] = {

    @annotation.tailrec
    def go[A](src: List[A], dst: List[String]): List[String] =
      src match {
        case Cons(h, t) => go(t, Cons(h.toString, dst))
        case Nil => dst
      }
    // non-associative Cons requires reversal of left->right construction
    go(reverse_tr(as), Nil)  // neutral element
  }

  def mapIncr_tr(is: List[Int]): List[Int] = {

    @annotation.tailrec
    def go(src: List[Int], dst: List[Int]): List[Int] =
      src match {
        case Cons(h, t) => go(t, Cons(h + 1, dst))
        case Nil => dst
      }
    // non-associative Cons requires reversal of left->right construction
    go(reverse_tr(is), Nil)  // neutral element
  }
}
