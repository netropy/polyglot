package netropy.polyglot.fp.lists


object ex_1_2 {

  /*
   * Notes: Straight-forward Functional-style design of core List type.
   *
   * - List GADT as trait (or class) with subtyping case class/object
   * - accessors as external functions that require + pattern-match argument
   * - more `null` IllegalArgumentException exceptions to check/document
   * - types, subtypes, values, and accessors all in same scope
   * - total: 60 lines for code + minimal scaladoc
   */

  /** An immutable, singly-linked list (a.k.a. LinkedList). */
  sealed trait List[+A]

  case object Nil extends List[Nothing]

  case class Cons[+A](head: A, tail: List[A]) extends List[A] {
    require(tail != null)
  }

  object List {

    /** Creates a list from the sequence of values. */
    def apply[A](as: A*): List[A] =
      as.foldRight(empty[A])((a, l) => Cons(a, l))

    /** Also called `pure` for Applicatives (with `unit: List[Unit]`). */
    def unit[A](a: A): List[A] = Cons(a, Nil)

    def empty[A]: List[A] = Nil
  }

  /** Returns the first element of a non-empty list.
    * @throws [[IllegalArgumentException]] if `as` is Nil or null
    */
  def head[A](as: List[A]): A =
    as match {
      case Cons(h, t) => h
      case _ => throw new IllegalArgumentException(s"as=$as")
    }

  /** Returns the rest of a non-empty list.
    * @throws [[IllegalArgumentException]] if `as` is Nil or null
    */
  def tail[A](as: List[A]): List[A] =
    as match {
      case Cons(h, t) => t
      case _ => throw new IllegalArgumentException(s"as=$as")
    }

  /** Returns an [[Option]] of the first element of a list.
    * @throws [[IllegalArgumentException]] if `as` is null
    */
  def headOption[A](as: List[A]): Option[A] = {
    require(as != null)
    as match {
      case Cons(h, t) => Some(h)
      case Nil => None
    }
  }

  /** Returns an [[Option]] of the rest of a list.
    * @throws [[IllegalArgumentException]] if `as` is null
    */
  def tailOption[A](as: List[A]): Option[List[A]] = {
    require(as != null)
    as match {
      case Cons(h, t) => Some(t)
      case Nil => None
    }
  }
}
