package netropy.polyglot.fp.lists


object ex_5_5 {
  import ex_1_3.List
  import ex_4_1.reduceOption_f
  import ex_5_1.mapReverse_fl

  /*
   * Notes: Lazy Map/Reduce-style operations, inefficient
   *
   * `exists_mrd`, `forall_mrd`, `count_mrd`
   * - map+reduce slightly longer than prior version based on `foldMap`
   * - very inefficient: no longer lazy due to prior map pass
   * - inefficient: instantiates list of intermediate values (Boolean, Int)
   * + can use `mapReverse_fl` instead of `map_fr` due to `|`, `&`, `+`
   * - must support empty lists, use of reduceOption (or add neutral element)
   *
   * `find_mrd`, `get_mrd`
   * Not implemented as map/reduce, even less efficient than above functions.
   */

  def exists_mrd[A]
      (as: List[A])
      (p: A => Boolean): Boolean =
    reduceOption_f(mapReverse_fl(as)(p))(_ | _)
      .getOrElse(false)

  def forall_mrd[A]
      (as: List[A])
      (p: A => Boolean): Boolean =
    reduceOption_f(mapReverse_fl(as)(p))(_ & _)
      .getOrElse(true)

  def count_mrd[A]
      (as: List[A])
      (p: A => Boolean): Long =
    reduceOption_f(mapReverse_fl(as)(a => if (p(a)) 1L else 0L))(_ + _)
      .getOrElse(0)
}
