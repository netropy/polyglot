package netropy.polyglot.fp.lists


object ex_3_5 {
  import ex_1_3.List
  import List.{Nil, Cons, head, tail, empty}
  import ex_3_1.{foldLeft_tr, foldRight_tr}
  import ex_3_4.reverse_fl

  /*
   * Notes: Removal operations, not a good fit for folds
   *
   * This group of functions can be expressed as folds, but implementations
   * - require complex intermediary results with repeated tuple [un]packing;
   * - do not preserve laziness (see recursive versions) as internal iteration
   *   implies full traversal.
   *
   * However, explicit control flow can be used to abort traversal/folding.
   *
   * `drop_fl`, `dropWhile_fl`, `take_fl`, `takeWhile_fl`
   * Stateful traversal, [un]packing of intermediate results, complex lambdas,
   * no longer lazy, traverses entire list.
   *
   *  2022-12: TODO: migrate to Scala3, deprecated nonlocal return
   *
   * `drop_fl_lazy`, `dropWhile_fl_lazy`, `take_fl_lazy`, `takeWhile_fl_lazy`
   * Forcefully aborts traversal by use of explicit `return`.
   *
   * Scala 3.2: Nonlocal Returns from lambdas have been deprecated and
   * replaced with: returning { ... throwReturn(x) ... }
   * @see [[https://docs.scala-lang.org/scala3/reference/dropped-features/nonlocal-returns.html]]
   * @see [[https://www.scala-lang.org/api/3.x/scala/util/control/NonLocalReturns$.html]]
   *
   * `last_fl`
   * Single pass; however, repeatedly packs intermediary result.
   *
   * `init_fl`
   * Two passes, indirect use of foldLeft through reverse.  Element removal by
   * reversing input list, removal from left, and then reversing for result.
   *
   * `filter_fr`, `filter_fl`
   * Constructs intermediary list, either preceded or followed by a reverse.
   * foldLeft more efficient for removal since fewer elements in result list;
   * foldRight more efficient for insertion since fewer elements in input list.
   */

  def drop_fl[A]
      (a0: List[A], n: Int): List[A] = {

    // foldLeft: using tuple (counter, list) as intermediary result
    // no longer lazy: traverses the entire list
    require(n >= 0)
    val (n1, a1) = foldLeft_tr((n, a0), a0) {
      case ((0, as), _) => (0, as)
      case ((n0, as), _) => (n0 - 1, tail(as))
    }
    require(n1 == 0)  // argument n not larger than list's length
    a1
  }

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def drop_fl_lazy[A]
      (a0: List[A], n: Int): List[A] = {

    // foldLeft: using tuple (counter, list) as intermediary result
    // lazy: using force (explicit control flow) to abort traversal
    require(n >= 0)
    val (n1, a1) = foldLeft_tr((n, a0), a0) {
      case ((n0, as), _) =>
        if (n0 == 0)
          return as

        (n0 - 1, tail(as))
    }
    require(n1 == 0)  // argument n not larger than list's length
    a1
  }

  def dropWhile_fl[A]
      (a0: List[A], p: A => Boolean): List[A] = {

    // foldLeft: using tuple (boolean, list) as intermediary result
    // no longer lazy: traverses the entire list
    require(p != null)
    val (i, a1) = foldLeft_tr((true, a0), a0) {
      case ((b, as), a) =>
        if (b && p(a)) (true, tail(as))
        else (false, as)
    }
    a1
  }

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def dropWhile_fl_lazy[A]
      (a0: List[A], p: A => Boolean): List[A] = {

    // foldLeft: using list as intermediary result
    // lazy: using force (explicit control flow) to abort traversal
    require(p != null)
    val a1 = foldLeft_tr(a0, a0) {
      (as, a) =>
      if (!p(a))
        return as

      tail(as)
    }
    a1
  }

  def take_fl[A]
      (a0: List[A], n: Int): List[A] = {

    // foldLeft: using tuple (counter, list) as intermediary result
    // no longer lazy: traverses the entire list
    require(n >= 0)
    val (n1, a1) = foldLeft_tr((n, empty[A]), a0) {
      case ((0, as), a) => (0, as)
      case ((n0, as), a) => (n0 - 1, Cons(a, as))
    }
    require(n1 == 0)  // argument n not larger than list's length
    reverse_fl(a1)
  }

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def take_fl_lazy[A]
      (a0: List[A], n: Int): List[A] = {

    // foldLeft: using tuple (counter, list) as intermediary result
    // lazy: using force (explicit control flow) to abort traversal
    require(n >= 0)
    val (n1, a1) = foldLeft_tr((n, empty[A]), a0) {
      case ((n0, as), a) =>
        if (n0 == 0)
          return reverse_fl(as)

        (n0 - 1, Cons(a, as))
    }
    require(n1 == 0)  // argument n not larger than list's length
    a0  // take input list
  }

  def takeWhile_fl[A]
     (a0: List[A])
     (p: A => Boolean): List[A] = {

    // foldLeft: using tuple (boolean, list) as intermediary result
    // no longer lazy: traverses the entire list
    require(p != null)
    val (i, a1) = foldLeft_tr((true, empty[A]), a0) {
      case ((b, as), a) =>
        if (b && p(a)) (true, Cons(a, as))
        else (false, as)
    }
    reverse_fl(a1)
  }

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def takeWhile_fl_lazy[A]
     (a0: List[A])
     (p: A => Boolean): List[A] = {

    // foldLeft: using tuple (boolean, list) as intermediary result
    // lazy: using force (explicit control flow) to abort traversal
    require(p != null)
    val a1 = foldLeft_tr(empty[A], a0) {
      (as, a) =>
        if (!p(a))
          return reverse_fl(as)

      Cons(a, as)
    }
    a0  // take input list
  }

  def last_fl[A]
      (a0: List[A]): A = {

    // foldLeft: using Option (or empty/singleton list) as intermediary result
    require(a0 != Nil)
    foldLeft_tr(Option.empty[A], a0)((_, a) => Some(a)).get
  }

  def init_fl[A]
      (a0: List[A]): List[A] = {

    // foldLeft: used 2x indirectly through reverse_fl
    require(a0 != Nil)
    reverse_fl(tail(reverse_fl(a0)))
  }

  def filter_fr[A]
      (a0: List[A], p: A => Boolean): List[A] = {

    // foldRight: internally reverses the input list before filtering
    require(p != null)
    foldRight_tr(a0, empty[A]) { (a, as) =>
      if (p(a)) Cons(a, as)
      else as
    }
  }

  def filter_fl[A]
      (a0: List[A], p: A => Boolean): List[A] = {

    // foldLeft: reverses the result list (even when no element was removed)
    require(p != null)
    val a1 = foldLeft_tr(empty[A], a0) { (as, a) =>
      if (p(a)) Cons(a, as)
      else as
    }
    reverse_fl(a1)
  }
}
