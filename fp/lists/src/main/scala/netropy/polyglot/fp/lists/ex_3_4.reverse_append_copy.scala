package netropy.polyglot.fp.lists


object ex_3_4 {
  import ex_1_3.List
  import List.{Cons, empty}
  import ex_3_1.{foldLeft_tr, foldRight_tr}

  /*
   * Notes: List-reconstruction operations, natural fit for left/right folds
   *
   * Not surprisingly,
   * - left-recursive construction with order reversal requires foldLeft,
   * - right-recursive construction with order preservation demands foldRight.
   *
   * The non-directional folds have no application, here.
   */

  def reverseAppend_fl[A]
      (a0: List[A], a1: List[A]): List[A] =
    foldLeft_tr(a1, a0)((as, a) => Cons(a, as))  // op = "snoc"

  def reverse_fl[A]
      (a0: List[A]): List[A] =
    foldLeft_tr(empty[A], a0)((as, a) => Cons(a, as))  // op = "snoc"

  def append_fr[A]
      (a0: List[A], a1: List[A]): List[A] =
    foldRight_tr(a0, a1)(Cons(_, _))

  def copy_fr[A]
      (a0: List[A]): List[A] =
    foldRight_tr(a0, empty[A])(Cons(_, _))
}
