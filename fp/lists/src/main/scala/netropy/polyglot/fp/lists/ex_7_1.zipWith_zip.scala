package netropy.polyglot.fp.lists


/*
 * Exercise Series 7: (List) Zipable (Traversal) Functions
 */

object ex_7_1 {
  import ex_1_3.List
  import List.{Nil, Cons}
  import ex_3_4.reverse_fl
  import ex_5_1.map_fr

  /*
   * Notes:
   *
   * `zipWith_tr`, `zip_tr`
   * These combinators add new functionality to lists: The ability to
   * traverse two (or more) collections in lockstep.
   *
   * The input lists are not required to be of same length; the traversal just
   * ends as soon as one (or both) of the lists are out of elements.
   *
   * Contrast `zipWith`:
   * - the `fold` combinators process only a single input list;
   * - the Applicative functions `tuple2`, `map2`, `ap` combine multiple
   *   inputs but in the form of cartesian products.
   *
   * These zip-style functions derive from the core FP type `Traversal` or
   * `Traversable Functor` (see URLs below), which specializes `Functor` and
   * `Foldable` and uses `Applicative`.  Here, only the zip-related functions
   * are introduced.
   *
   * `zipWith_tr` is implemented as left-recursion with final list reversal,
   * which is more efficient than reversing both input lists, independently.
   *
   * `zip` and `zipWith` can be implemented in terms of another (see below).
   *
   * Tried here to give plain-english, list-specific descriptions/scaladocs.
   * See .../fp exercises for Traversal laws and relation to other types.
   *
   * @see [[https://typelevel.org/cats/typeclasses/traverse.html]]
   * @see [[https://typelevel.org/cats/api/cats/Traverse.html]]
   */

  /** Applies a function to each pair of elements, while there are any. */
  def zipWith_tr[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C) : List[C] = {

    @annotation.tailrec
    def go(r: List[C], as: List[A], bs: List[B]): List[C] =
      (as, bs) match {
        case (_, Nil) => r
        case (Nil, _) => r
        case (Cons(a, ta), Cons(b, tb)) => go(Cons(f(a, b), r), ta, tb)
      }
    reverse_fl(go(Nil, as, bs))
  }

  /** Pairs corresponding elements from each list, while there are any. */
  def zip_zw[A, B]
      (as: List[A], bs: List[B]): List[(A, B)] =
    zipWith_tr(as, bs)((_, _))

  /*
   * For Illustration:
   *
   * zipWith can be implemented in terms of zip (and map); involves 2 passes.
   */

  def zip_tr[A, B]
      (as: List[A], bs: List[B]): List[(A, B)] = {

    @annotation.tailrec
    def go(r: List[(A, B)], as: List[A], bs: List[B]): List[(A, B)] =
      (as, bs) match {
        case (_, Nil) => r
        case (Nil, _) => r
        case (Cons(a, ta), Cons(b, tb)) => go(Cons((a, b), r), ta, tb)
      }
    reverse_fl(go(Nil, as, bs))
  }

  def zipWith_zm[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C) : List[C] =
    map_fr(zip_tr(as, bs))(f.tupled)
}
