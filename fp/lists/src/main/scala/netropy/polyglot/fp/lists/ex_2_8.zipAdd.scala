package netropy.polyglot.fp.lists


object ex_2_8 {
  import ex_1_3.List
  import List.{Nil, Cons}
  import ex_2_4.reverse_tr

  /*
   * Zip-like (Traversal) Functions
   * - right-/recursive and left-/tail-recursive versions
   *
   * `zipAdd_r`, `zipAdd_tr`
   * traversal of 2 lists in lockstep
   * reversal of result list for left-/tail-recursive version
   */

  /** Adds corresponding elements of two Int lists, while there are any. */
  // not: @annotation.tailrec
  def zipAdd_r
      (as0: List[Int], as1: List[Int]): List[Int] =
    (as0, as1) match {
      case (Nil, _) => Nil
      case (_, Nil) => Nil
      case (Cons(h0, t0), Cons(h1, t1)) =>  Cons(h0 + h1, zipAdd_r(t0, t1))
    }

  def zipAdd_tr
      (as0: List[Int], as1: List[Int]): List[Int] = {

    @annotation.tailrec
    def go(a0: List[Int], a1: List[Int], z: List[Int]): List[Int] =
      (a0, a1) match {
        case (Cons(h0, t0), Cons(h1, t1)) => go(t0, t1, Cons(h0 + h1, z))
        case (_, _) => z
      }
    reverse_tr(go(as0, as1, Nil))
  }
}
