package netropy.polyglot.fp.lists

object ex_5_2 {
  import ex_1_3.List
  import List.{Cons, empty}
  import ex_5_1.map_fr
  import ex_3_1.foldRight_tr

  /*
   * Notes:
   *
   * What `map` (by itself) supports to do -- or not:
   * + apply a list of functions to a single value
   * + pair list elements with their mapped value
   * + pair list elements with an injected value
   * + project from tuples
   * + unzip a list of tuples
   * - form cartesian products combining elements of 2 or more lists
   * - apply higher-arity functions to values from 2 or more lists
   * - zip a tuple of lists into a list of tuples
   * - transform a `List[X[_]]` into `X[List[_]]` (X = Applicative)
   * - change the size or order of lists
   *
   * Tried here to give plain-english, list-specific descriptions/scaladocs.
   * See .../fp exercises for Functor laws and relation to other types.
   *
   * @see [[https://typelevel.org/cats/typeclasses/functor.html]]
   * @see [[https://typelevel.org/cats/api/cats/Functor.html]]
   */

  /** Applies each unary function in the list to the given value. */
  def mapply_m[A, B]
      (a: A)
      (fs: List[A => B]): List[B] =
    map_fr(fs)(_(a))

  /** Pairs each list element with its mapped value by given function. */
  def fproduct_m[A, B]
      (as: List[A])
      (f: A => B): List[(A, B)] =
    map_fr(as)(a => (a, f(a)))

  /** Pairs each list element with the given value on the left. */
  def tupleLeft_m[A, B]
      (as: List[A], b: B): List[(B, A)] =
    map_fr(as)((b, _))

  /** Pairs each list element with the given value on the right. */
  def tupleRight_m[A, B]
      (as: List[A], b: B): List[(A, B)] =
    map_fr(as)((_, b))

  /** Unzips a list of pairs into a pair of lists. */
  def unzip_fr[A, B]
      (ab: List[(A, B)]): (List[A], List[B]) =
    foldRight_tr(ab, (empty[A], empty[B])) {
      case (ab, (as, bs)) => (Cons(ab._1, as), Cons(ab._2, bs))
    }

  /*
   * Notes: For Illustration
   *
   * `unzip` can be implemented solely via map; requires 2 passes, hence
   *   less efficient than directly using 1 foldRight.
   */

  def unzip_m[A, B]
      (ab: List[(A, B)]): (List[A], List[B]) =
    (map_fr(ab)(_._1), map_fr(ab)(_._2))  // 2x traverse (and reversing)
}
