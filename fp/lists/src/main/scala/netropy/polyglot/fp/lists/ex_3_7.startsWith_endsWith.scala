package netropy.polyglot.fp.lists


object ex_3_7 {
  import ex_1_3.List
  import List.{Nil, Cons}
  import ex_2_4.reverse_tr
  import ex_3_1.foldLeft_tr

  /*
   * Notes: Lazy Prefix/Suffix-Test functions, not a good fit for folds
   *
   * This group of functions can be expressed as folds, but implementations
   * - require complex intermediary results with repeated tuple [un]packing;
   * - do not preserve laziness (see recursive versions) as internal iteration
   *   implies full traversal.
   *
   * However, explicit control flow can be used to abort traversal/folding.
   *
   * `startsWith_fl`
   * Stateful traversal, [un]packing of intermediate results, complex lambdas,
   * final test after foldLeft, no longer lazy, traverses entire list.
   *
   * `endsWith_fl`
   * Two passes, indirect use of foldLeft through reverse.
   *
   *  2022-12: TODO: migrate to Scala3, deprecated nonlocal return
   *
   * `startsWith_fl_lazy`, `endsWith_fl_lazy`
   * Forcefully aborts traversal by use of explicit `return`.
   *
   * Scala 3.2: Nonlocal Returns from lambdas have been deprecated and
   * replaced with: returning { ... throwReturn(x) ... }
   * @see [[https://docs.scala-lang.org/scala3/reference/dropped-features/nonlocal-returns.html]]
   * @see [[https://www.scala-lang.org/api/3.x/scala/util/control/NonLocalReturns$.html]]
   *
   * `hasSubsequence`
   * Cannot be easily implemented using a foldLeft, need access to tail.
   */

  def startsWith_fl[A]
      (as: List[A], prefix: List[A]): Boolean = {

    val (r, t) = foldLeft_tr((true, prefix), as) {
      case ((true, Cons(h, t)), a) => ((h == a), t)
      case ((r, t), a) => (r, t)
    }
    r && (t == Nil)
  }

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def startsWith_fl_lazy[A]
      (as: List[A], prefix: List[A]): Boolean = {

    val t =  foldLeft_tr(prefix, as) {
      case (Nil, _) =>
        return true
      case (Cons(h, t), a) =>
        if (h != a)
          return false

        t
    }
    t == Nil
  }

  def endsWith_fl[A]
      (as: List[A], suffix: List[A]): Boolean =
    startsWith_fl(reverse_tr(as), reverse_tr(suffix))

  def endsWith_fl_lazy[A]
      (as: List[A], suffix: List[A]): Boolean =
    startsWith_fl_lazy(reverse_tr(as), reverse_tr(suffix))
}
