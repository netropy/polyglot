package netropy.polyglot.fp.lists


object ex_2_2 {
  import ex_1_3.List
  import List.{Nil, Cons, head, tail}

  /*
   * Left-recursive list manipulation with data sharing
   *
   * Naturally tail-recursive.
   */

  /** Replaces the first element of a non-empty list. */
  def setHead[A]
      (as: List[A], head: A): List[A] =
    Cons(head, tail(as))

  /** Replaces the rest of a non-empty list. */
  def setTail[A]
      (as: List[A], tail: List[A]): List[A] =
    Cons(head(as), tail)

  /** Drops the first `n` elements from a list with `>= n` elements. */
  @annotation.tailrec
  def drop_tr[A]
      (as: List[A], n: Int): List[A] = {
    require(n >= 0)
    if (n == 0) as
    else drop_tr(tail(as), n - 1)
  }

  /** Drops the first elements from a list while satisfying a predicate. */
  @annotation.tailrec
  def dropWhile_tr[A]
      (as: List[A], p: A => Boolean): List[A] = {
    require(p != null)
    as match {
      case Cons(h, t) if (p(h)) => dropWhile_tr(t, p)
      case l => l
    }
  }
}
