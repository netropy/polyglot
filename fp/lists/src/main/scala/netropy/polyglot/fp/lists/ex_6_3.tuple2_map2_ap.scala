package netropy.polyglot.fp.lists


object ex_6_3 {
  import ex_1_3.List
  import ex_5_1.map_fr
  import ex_6_1.{tuple2_fr, map2_fr, ap_fr}

  /*
   * Notes: The Applicative primitives in terms of each other (redundant)
   */

  def tuple2_m2[A, B]
      (as: List[A], bs: List[B]): List[(A, B)] =
    map2_fr(as, bs)((_, _))  // nice :-)

  def tuple2_am[A, B]
      (as: List[A], bs: List[B]): List[(A, B)] =
    ap_fr(map_fr(as)(a => (b: B) => (a, b))) (bs)  // ap sequences f first

  def map2_pm[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C): List[C] =
    map_fr(tuple2_fr(as, bs))(ab => f(ab._1, ab._2))  // alt: case/scala3

  def map2_am[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C): List[C] =
    ap_fr(map_fr(as)(a => b => f(a, b)))(bs)  // ap sequences f first

  def ap_pm[A, B]
      (fs: List[A => B])
      (as: List[A]): List[B] =
    map_fr(tuple2_fr(fs, as))(fa => fa._1(fa._2))  // f 1st, alt: case/scala3

  def ap_m2[A, B]
      (fs: List[A => B])
      (as: List[A]): List[B] =
    map2_fr(fs, as)(_(_))  // nice :-)  f first
}
