package netropy.polyglot.fp.lists


/*
 * Exercise Series 3: (List) Foldable API
 */

object ex_3_1 {
  import ex_1_3.List
  import List.{Nil, Cons}
  import ex_2_4.reverse_tr

  /*
   * Notes: Directional Fold combinators
   *
   * The combinators `foldLeft`, `foldRight` parametrize over:  0) the list,
   * 1) the binary operation, and 2) the start value.
   *
   * These methods define the fundamental FP type `Foldable` (e.g., Cats
   * [[https://typelevel.org/cats/api/cats/Foldable.html]]).
   *
   * The direct, recursive implementation of
   * - foldLeft is tail-recursive (`foldLeft_tr`),
   * - foldRight is not tail-recursive (`foldRight_r`)
   *
   * By reversing the list using `reverse_tr`:
   * - foldRight can be implemented via foldLeft, thus, yielding a stack-safe
   *   implementation (`foldRight_tr`);
   * - foldLeft can be implemented via stack-unsafe foldRight (`foldLeft_r`).
   *
   * See performance & stack-safety discussion below.
   */

  /** Applies binary operator to start value, list elements, left->right. */
  @annotation.tailrec
  def foldLeft_tr[A, B]
      (z: B, as: List[A])
      (op: (B, A) => B): B =
    as match {
      case Cons(h, t) => foldLeft_tr(op(z, h), t)(op)
      case Nil => z
    }

  /** Applies binary operator to start value, list elements, left<-right. */
  def foldRight_tr[A, B]
      (as: List[A], z: B)
      (op: (A, B) => B): B =
    foldLeft_tr(z, reverse_tr(as))((l, a) => op(a, l))

  // for comparison: the non-tail-recursive, stack-unsafe implementations

  // not: @annotation.tailrec,
  def foldRight_r[A, B]
      (as: List[A], z: B)
      (op: (A, B) => B): B =
    as match {
      case Cons(h, t) => op(h, foldRight_r(t, z)(op))
      case Nil => z
    }

  def foldLeft_r[A, B]
      (z: B, as: List[A])
      (op: (B, A) => B): B =
    foldRight_r(reverse_tr(as), z)((a, l) => op(l, a))

  /*
   * Notes: Efficiency of the list reversal step in foldRight_tr
   *
   * The call to reverse _appears_ as an extra operation in foldRight_fl that
   * is absent in foldRight_r.  However, both must traverse the input list
   * and hold element values/references in memory for access in reverse order:
   * - the _r version in call frames on the JVM's call stack
   * - the _fl version in newly created objects in heap memory.
   *
   * Performance considerations:
   * - If the temporary, heap-allocated list of _fl is short-lived enough,
   *   the costs for object-creation and -collection might be negligible.
   * - Stack-memory is not free in terms of GC.
   * - Compiling the recursion into a loop should yield a substantial
   *   performance gain over the repeated subroutine calls.
   *
   *
   * Should `foldRight` reverse and be stack-safe?  Yes, of course!
   *
   * Surprisingly, back in 2010, @odersky _disagreed_ with List.foldRight's
   * implementation applying reverse and be stack-safe:
   * - [[https://github.com/scala/bug/issues/3295]]
   *   "foldRight broken for large lists #3295"
   * - [[https://github.com/scala/bug/issues/2818]]
   *   "List.foldRight throws StackOverflowError #2818"
   *
   * However, implementation has been changed for stack-safety in 2017:
   * [[https://github.com/scala/scala/commit/d9885d562ab6f17defb05c420f898e4a5c9d624a]]
   *
   * No longer StackOverflow errors with current Scala 2.11, 2.12, 2.13:
   * scala> (1 to 3000000).toList.foldRight(0)(_ + _)
   * res26: Int = -1124226208
   */
}
