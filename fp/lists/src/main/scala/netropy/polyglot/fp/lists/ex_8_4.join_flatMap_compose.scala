package netropy.polyglot.fp.lists


object ex_8_4 {
  import ex_1_3.List
  import List.unit
  import ex_5_1.map_fr
  import ex_8_1.{join_fr, flatMap_fr, compose_fr}

  /*
   * Notes: The Monad primitives in terms of each other (redundant)
   */

  def join_fm[A]
      (aas: List[List[A]]): List[A] =
    flatMap_fr(aas)(identity)

  def join_c[A]
      (aas: List[List[A]]): List[A] =
    compose_fr(identity[List[List[A]]])(identity[List[A]])(aas)

  def flatMap_jm[A, B]
      (as: List[A])
      (f: A => List[B]): List[B] =
    join_fr(map_fr(as)(f))

  def flatMap_c[A, B]
      (as: List[A])
      (f: A => List[B]): List[B] =
    compose_fr(identity[List[A]])(f(_))(as)

  def compose_fm[A, B, C]
      (f: A => List[B])(g: B => List[C]): A => List[C] =
    a => flatMap_fr(f(a))(g(_))

  def compose_jm[A, B, C]
      (f: A => List[B])(g: B => List[C]): A => List[C] =
    a => join_fr(map_fr(f(a))(g(_)))
}
