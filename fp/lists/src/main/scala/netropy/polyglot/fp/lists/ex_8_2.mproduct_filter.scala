package netropy.polyglot.fp.lists


object ex_8_2 {
  import ex_1_3.List
  import List.{Nil, unit}
  import ex_5_1.map_fr
  import ex_8_1.{flatMap_fr, join_fr, compose_fr}

  /** Pairs each list element with each of its mapped values. */
  def mproduct_fm[A, B]
      (as: List[A])
      (f: A => List[B]): List[(A, B)] =
    flatMap_fr(as)(a => map_fr(f(a))((a, _)))

  def mproduct_jm[A, B]
      (as: List[A])
      (f: A => List[B]): List[(A, B)] =
    join_fr(map_fr(as)(a => map_fr(f(a))((a, _))))

  def mproduct_c[A, B]
      (as: List[A])
      (f: A => List[B]): List[(A, B)] =
    compose_fr(identity[List[A]])(a => map_fr(f(a))((a, _)))(as)

  def filter_fm[A]
      (a0: List[A], p: A => Boolean): List[A] = {
    require(p != null)
    flatMap_fr(a0)(a => if (p(a)) List(a) else Nil)
  }

  def filter_jm[A]
      (a0: List[A], p: A => Boolean): List[A] = {
    require(p != null)
    join_fr(map_fr(a0)(a => if (p(a)) List(a) else Nil))
  }

  def filter_c[A]
      (a0: List[A], p: A => Boolean): List[A] = {
    require(p != null)
    compose_fr(identity[List[A]])(a => if (p(a)) unit(a) else Nil)(a0)
  }
}
