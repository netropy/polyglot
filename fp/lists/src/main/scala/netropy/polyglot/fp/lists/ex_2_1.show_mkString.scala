package netropy.polyglot.fp.lists

import scala.collection.mutable.StringBuilder


/*
 * Exercise Series 2: (List) Recursion
 */

object ex_2_1 {
  import ex_1_3.List
  import List.{Nil, Cons}

  /*
   * Recursive and tail-recursive list traversal
   * - with (associative) string concatenation
   * - using a stateful builder object
   * - right-/recursive and left-/tail-recursive versions
   */

  /** Displays list elements in a string like "[]", "[a,]", "[a,b,]" etc. */
  def show_r[A](as: List[A]): String = {

    // no `foldLeft` yet defined on this List type
    // not: @annotation.tailrec
    def go[A](src: List[A]): String = {
      src match {
        case Cons(h, t) => s"$h," + go(t)
        case Nil => ""
      }
    }
    "[" + go(as) + "]"
  }

  def show_tr[A](as: List[A]): String = {

    // no `foldLeft` yet defined on this List type
    @annotation.tailrec
    def go[A](src: List[A], dst: String): String = {
      src match {
        case Cons(h, t) => go(t, dst + h + ",")
        case Nil => dst
      }
    }
    go(as, "[") + "]"
  }

  /** Displays list elements in a string using start, end, and separator. */
  def mkString_r[A]
      (as: List[A], start: String, sep: String, end: String): String = {

    // no `foldLeft` yet defined on this List type
    // not: @annotation.tailrec
    def go[A](src: List[A]): String =
      src match {
        case Cons(h, t) => sep + h + go(t)
        case Nil => ""
      }

    val s = as match {
      case Nil => ""
      case Cons(h, Nil) => h.toString
      case Cons(h, t) => h.toString + go(t)
    }
    start + s + end
  }

  def mkString_tr[A]
      (as: List[A], start: String, sep: String, end: String): String = {

    val sb = new StringBuilder

    // no `foreach` yet defined on this List type
    @annotation.tailrec
    def go[A](src: List[A]): Unit =
      src match {
        case Cons(h, t) => sb ++= sep ++= h.toString ; go(t)
        case Nil => ()
      }

    sb ++= start
    as match {
      case Nil => ()
      case Cons(h, Nil) => sb ++= h.toString
      case Cons(h, t) => sb ++= h.toString ; go(t)
    }
    sb ++= end
    sb.toString
  }
}
