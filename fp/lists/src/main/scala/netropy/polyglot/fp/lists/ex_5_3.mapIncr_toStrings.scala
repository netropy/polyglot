package netropy.polyglot.fp.lists


object ex_5_3 {
  import ex_1_3.List
  import ex_5_1.map_fr

  /*
   * Notes: Pure Map operations
   */

  def mapIncr_m
      (as: List[Int]): List[Int] =
    map_fr(as)(_ + 1)

  def toStrings_m[A]
      (as: List[A]): List[String] =
    map_fr(as)(_.toString)
}
