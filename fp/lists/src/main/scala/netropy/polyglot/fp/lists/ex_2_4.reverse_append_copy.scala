package netropy.polyglot.fp.lists


object ex_2_4 {
  import ex_1_3.List
  import List.{Nil, Cons}

  /*
   * Left-/right-recursive list construction, order reversal/preservation
   * - recursive and tail-recursive versions
   *
   * Pattern:
   * - preserves order:  Cons(, recur(...))
   * - reverses order:   recur(, Cons(...))
   */

  /** Returns the concatenation of two lists (`a0 ::: a1`). */
  // not: @annotation.tailrec
  def append_r[A](a0: List[A], a1: List[A]): List[A] =
    a0 match {
      case Cons(h, t) => Cons(h, append_r(t, a1))
      case Nil => a1
    }

  /** Returns the reversed and appended list (`reversed(a0) ::: a1`). */
  @annotation.tailrec
  def reverseAppend_tr[A](a0: List[A], a1: List[A]): List[A] =
    a0 match {
      case Cons(h, t) => reverseAppend_tr(t, Cons(h, a1))
      case Nil => a1
    }

  /** Returns a list of the elements in reversed order. */
  def reverse_tr[A](as: List[A]): List[A] =
    reverseAppend_tr(as, Nil)

  def append_tr[A](a0: List[A], a1: List[A]): List[A] =
    reverseAppend_tr(reverse_tr(a0), a1)

  /** Returns a shallow copy of a list. */
  def copy_tr[A](as: List[A]): List[A] =
    append_tr(as, Nil)
}
