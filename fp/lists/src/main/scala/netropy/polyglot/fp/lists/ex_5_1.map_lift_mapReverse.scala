package netropy.polyglot.fp.lists


/*
 * Exercise Series 5: (List) Functor API
 */

object ex_5_1 {
  import ex_1_3.List
  import List.{Cons, empty}
  import ex_3_1.foldRight_tr

  /*
   * Notes: The Map combinator
   *
   * The `map` combinator applies a unary function to each element in a list
   * and returns the result in a list that preserves the original order.
   *
   * Analog to List constructor `unit`, which lifts a value into (a) List,
   * `map` can be viewed as lifting an unary function for list application
   * (shown as curried functions):
   * ```
   *     map:  List[A] => (A => B) => List[B]   = as => f => lift(f)(as)
   *     lift: (A => B) => List[A] => List[B]   = f => as => map(as)(f)
   * ```
   *
   * `map` (or `lift`) defines the core FP type `Functor`.
   *
   * Tried here to give plain-english, list-specific descriptions/scaladocs.
   * See .../fp exercises for Functor laws and relation to other types.
   *
   * @see [[https://typelevel.org/cats/typeclasses/functor.html]]
   * @see [[https://typelevel.org/cats/api/cats/Functor.html]]
   */

  /** Applies the function to each list element. */
  def map_fr[A, B]
      (as: List[A])
      (f: A => B): List[B] =
    foldRight_tr(as, empty[B])((a, as) => Cons(f(a), as))

  /** Lifts a function to operate on Lists. */
  def lift_m[A, B]
      (f: A => B): List[A] => List[B] =
    as => map_fr(as)(f)

  /*
   * Notes: For Illustration
   *
   * `map` can be implemented directly on foldLeft with reversing the result.
   *   No advantage, since size of |input| == |result| list:
   *   `reverse(foldLeft(, as)) == foldLeft(, reverse(as)) == foldRight(as,)`
   *
   * `mapReverse` supports use cases where the reversed order is wanted or
   *   where the order does not matter (commutative + associative ops).
   *   NOT part of Functor, whose methods must preserve order and size
   *   (mapReserve violates both functor laws: identity and composition).
   */

  import ex_3_1.foldLeft_tr
  import ex_3_4.reverse_fl

  def map_fl[A, B]
      (as: List[A])
      (f: A => B): List[B] =
    reverse_fl(foldLeft_tr(empty[B], as)((as, a) => Cons(f(a), as)))

  /** Applies the function to each element, reversing the order. */
  def mapReverse_fl[A, B]
      (as: List[A])
      (f: A => B): List[B] =
    foldLeft_tr(empty[B], as)((as, a) => Cons(f(a), as))
}
