package netropy.polyglot.fp.lists


object ex_1_3 {

  /*
   * Notes: Scoped, null-based, functional-style design of core List type.
   *
   * - List GADT as public trait (or class), subtyping values in companion
   * - Nil := null (Nil punning), fewer argument checking/documentation
   * - accessors require & down-cast argument, or pattern-match
   * - all subtypes, values, and accessors in companion scope
   * - total: 48 lines for code + minimal scaladoc
   */

  /** An immutable, singly-linked list (a.k.a. LinkedList). */
  sealed trait List[+A]

  object List {

    val Nil: List[Nothing] = null // alias `null` as explicitly allowed value

    case class Cons[+A](head: A, tail: List[A]) extends List[A]

    /** Returns the first element of a non-empty list.
      * @throws [[IllegalArgumentException]] if `as` is null
      */
    def head[A](as: List[A]): A = {
      require(as != null)
      as.asInstanceOf[Cons[A]].head
    }

    /** Returns the rest of a non-empty list.
      * @throws [[IllegalArgumentException]] if `as` is null
      */
    def tail[A](as: List[A]): List[A] = {
      require(as != null)
      as.asInstanceOf[Cons[A]].tail
    }

    /** Returns an [[Option]] of the first element of a list. */
    def headOption[A](as: List[A]): Option[A] =
      as match {
        case Cons(h, t) => Some(h)
        case Nil => None
      }

    /** Returns an [[Option]] of the rest of a list. */
    def tailOption[A](as: List[A]): Option[List[A]] =
      as match {
        case Cons(h, t) => Some(t)
        case Nil => None
      }

    /** Creates a list from the sequence of values. */
    def apply[A](as: A*): List[A] =
      as.foldRight(empty[A])((a, l) => Cons(a, l))

    /** Also called `pure` for Applicatives (with `unit: List[Unit]`). */
    def unit[A](a: A): List[A] = Cons(a, Nil)

    def empty[A]: List[A] = Nil
  }
}
