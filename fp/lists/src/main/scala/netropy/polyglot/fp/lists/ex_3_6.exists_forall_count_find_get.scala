package netropy.polyglot.fp.lists

import scala.math.BigInt


object ex_3_6 {
  import ex_1_3.List
  import List.Nil
  import ex_3_1.foldLeft_tr
  import ex_3_2.foldMap_fl

  /*
   * Notes: Lazy MapReduce-like functions, not a good fit for folds
   *
   * This group of functions can be expressed as folds, but implementations
   * do not preserve laziness (see recursive versions) as internal iteration
   * implies full traversal.
   *
   * Some laziness can be restored using non-strict operators, for example:
   * ```
   *     foldMap_fl(as)(p)(false, _ || _)   // evaluates p on all elems
   *     foldLeft_tr(false, as)(_ || p(_))  // evaluates p only while false
   * ```
   *
   * Alternatively, laziness can be forced using explicit control flow to
   * abort traversal/folding.
   *
   * `exists_fl`, `forall_fl`, `find_fl`
   * Can use non-strict operations: Boolean `||` and `&&`, Option `orElse`.
   * Then, the predicate is only called on elements from left while needed.
   *
   *  2022-12: TODO: migrate to Scala3, deprecated nonlocal return
   *
   * `exists_fl_lazy`, `forall_fl_lazy`, `find_fl_lazy`
   * Forcefully aborts traversal by use of explicit `return`.
   *
   * Scala 3.2: Nonlocal Returns from lambdas have been deprecated and
   * replaced with: returning { ... throwReturn(x) ... }
   * @see [[https://docs.scala-lang.org/scala3/reference/dropped-features/nonlocal-returns.html]]
   * @see [[https://www.scala-lang.org/api/3.x/scala/util/control/NonLocalReturns$.html]]
   *
   * `count_f`
   * Needs to test all elements and, hence, utilizes `foldMap`.
   *
   * `get_fl`
   * Keeps incrementing the position even after finding the element.
   *
   * `get_fl_lazy`
   * Forcefully aborts traversal by use of explicit `return`.
   */

  def exists_fl[A]
      (as: List[A])
      (p: A => Boolean): Boolean =
    foldLeft_tr(false, as)(_ || p(_))

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def exists_fl_lazy[A]
      (as: List[A])
      (p: A => Boolean): Boolean = {

    foldLeft_tr(false, as) { (z, a) =>
      if (p(a))
        return true

      z
    }
  }

  def forall_fl[A]
      (as: List[A])
      (p: A => Boolean): Boolean =
    foldLeft_tr(true, as)(_ && p(_))

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def forall_fl_lazy[A]
      (as: List[A])
      (p: A => Boolean): Boolean = {

    foldLeft_tr(true, as) { (z, a) =>
      if (!p(a))
        return false

      z
    }
  }

  def count_f[A]
      (as: List[A])
      (p: A => Boolean): Long =
    foldMap_fl(as)(a => if (p(a)) 1L else 0L)(0L, _ + _)

  def find_fl[A]
      (as: List[A])
      (p: A => Boolean): Option[A] =
    foldLeft_tr(Option.empty[A], as)((o, a) => o.orElse(Option.when(p(a))(a)))

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def find_fl_lazy[A]
      (as: List[A])
      (p: A => Boolean): Option[A] = {

    foldLeft_tr(Option.empty[A], as) {
      (o, a) =>
      if (p(a))
        return Some(a)

      o
    }
  }

  def get_fl[A]
      (as: List[A], i: Int): Option[A] = {

    val (j, o) = foldLeft_tr((0, Option.empty[A]), as) {
      case ((j, o), a) => if (j == i) (j + 1, Some(a)) else (j + 1, o)
    }
    o
  }

  // 2022-12: TODO: migrate to Scala3, deprecated nonlocal return
  def get_fl_lazy[A]
      (as: List[A], i: Int): Option[A] = {

    if (as == Nil || i < 0)
      return Option.empty[A]

    val j = foldLeft_tr(i, as) {
      (j, a) =>
      if (j == 0)
        return Some(a)

      j - 1
    }
    assert(j >= 0)
    Option.empty[A]
  }
}
