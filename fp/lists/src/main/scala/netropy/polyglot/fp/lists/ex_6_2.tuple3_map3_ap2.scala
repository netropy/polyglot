package netropy.polyglot.fp.lists


object ex_6_2 {
  import ex_1_3.List
  import ex_5_1.map_fr
  import ex_6_1.{tuple2_fr, map2_fr, ap_fr}

  /*
   * Notes: Higher-Arity combinators, arity 2 gives N
   */

  /** Returns the List of all (a, b, c), sequencing as, bs, then cs. */
  def tuple3_t2[A, B, C]
      (as: List[A], bs: List[B], cs: List[C]): List[(A, B, C)] =
    map_fr(tuple2_fr(as, tuple2_fr(bs, cs))) {
      case (a, (b, c)) => (a, b, c)  // alt: scala3/._[12] tuple destruct
    }

  /** Applies the function to each (a, b, c), sequencing as, bs, then cs. */
  def map3_m2[A, B, C, D]
      (as: List[A], bs: List[B], cs: List[C])
      (f: (A, B, C) => D): List[D] =
    map2_fr(as, map2_fr(bs, cs)((_, _))) {
      case (a, (b, c)) => f(a, b, c)  // alt: scala3/._[12] tuple destruct
    }

  /** Applies each function to each (a, b), sequencing fs, as, then bs. */
  def ap2_ap[A, B, C]
      (fs: List[(A, B) => C])
      (as: List[A], bs: List[B]): List[C] = {
    // easier to express this `ap` as `map`
    // val fabs = ap_fr[(A, B) => C, A => B => C](List(_.curried))(fs)
    val fabs: List[A => B => C] = map_fr(fs)(_.curried)
    ap_fr(ap_fr(fabs)(as))(bs)
  }
}
