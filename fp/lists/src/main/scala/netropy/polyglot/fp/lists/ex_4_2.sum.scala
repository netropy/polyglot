package netropy.polyglot.fp.lists

import scala.math.BigInt


object ex_4_2 {
  import ex_1_3.List
  import List.Cons
  import ex_4_1.reduce_f

  /*
   * Notes: Strict, pure Reduce operation
   *
   * `sum` can use `reduce` due to associative `+`.
   * The overloaded variant for `List` must ensure to support empty lists.
   *
   * The other reduce-style functions require prior mapping (see next).
   */

  def sum_rd(as: Cons[Int]): Int =
    reduce_f(as)(_ + _)

  def sum_rd(as: List[Int]): Int =
    sum_rd(Cons(0, as))
}
