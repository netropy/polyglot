package netropy.polyglot.fp.lists


object ex_5_4 {
  import ex_1_3.List
  import List.{Nil, Cons}
  import ex_4_1.reduce_f
  import ex_4_2.sum_rd
  import ex_5_1.{map_fr, mapReverse_fl}
  import ex_5_3.toStrings_m

  /*
   * Notes: Map/Reduce-style operations
   *
   * `length_mrd`, `product_mrd`
   * - map+reduce slightly longer than prior version based on `foldMap`
   * - inefficient: instantiates list of intermediate values
   * - must support empty lists, add neutral element or use `reduceOption`
   * + can use `mapReverse_fl` instead of `map_fr` due to `+`, `*`
   *
   * mkString_mrd
   * + map+reduce shorter, clearer than prior version based on `foldLeft`
   * + no dependency between fold direction <> separator/concat pattern
   * - less efficient: instantiates list of intermediate strings
   */

  def length_mrd[A]
      (as: List[A]): Int =
    sum_rd(mapReverse_fl(as)(_ => 1))

  def product_mrd
      (as: List[Int]): BigInt =
    reduce_f(mapReverse_fl(Cons(1, as))(BigInt(_)))(_ * _)  // neutral element

  def mkString_mrd[A]
      (as: List[A], start: String, sep: String, end: String): String = {

    val s = as match {
      case Cons(_, t) => reduce_f(toStrings_m(as))(_ + sep + _)
      case Nil => ""
    }
    start + s + end
  }
}
