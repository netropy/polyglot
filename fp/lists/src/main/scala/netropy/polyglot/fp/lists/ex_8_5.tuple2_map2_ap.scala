package netropy.polyglot.fp.lists


object ex_8_5 {
  import ex_1_3.List
  import ex_8_1.flatMap_fr
  import ex_8_3.map_fm

  /*
   * Notes: The Applicative primitives in terms Monad (redundant)
   */

  def tuple2_fm[A, B]
      (as: List[A], bs: List[B]): List[(A, B)] =
    flatMap_fr(as)(a => map_fm(bs)((a, _)))

  def map2_fm[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C): List[C] =
    flatMap_fr(as)(a => map_fm(bs)(f(a, _)))

  def ap_fm[A, B]
      (fs: List[A => B])
      (as: List[A]): List[B] =
    flatMap_fr(fs)(f => map_fm(as)(f(_)))  // ap sequences f first
}
