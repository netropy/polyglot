package netropy.polyglot.fp.lists


/*
 * Exercise Series 8: (List) Monad Functions
 */

object ex_8_1 {
  import ex_1_3.List
  import List.empty
  import ex_3_1.foldRight_tr
  import ex_3_4.append_fr
  import ex_5_1.map_fr

  /*
   * Notes:
   *
   * `join`, `flatMap`, `compose`
   * These combinators are _equivalent_: subsequent exercise to implement
   * them in terms of another.  Together with List's `unit` (a.k.a. `pure`)
   * constructor, they define the core FP type `Monad`.
   *
   * Here, implementations given in terms of foldRight and foldLeft.
   *
   * Tried here to give plain-english, list-specific descriptions/scaladocs.
   * For Monad laws and relation to other types, see .../fp exercises.
   *
   * @see [[https://typelevel.org/cats/typeclasses/monad.html]]
   * @see [[https://typelevel.org/cats/api/cats/Monad.html]]
   */

  /** Flattens a nested list of lists into a single-layer list. */
  def join_fr[A]
      (aas: List[List[A]]): List[A] =
    foldRight_tr(aas, empty[A])(append_fr)

  /** Maps each list element to a list and flattens the result. */
  def flatMap_fr[A, B]
      (as: List[A])
      (f: A => List[B]): List[B] =
    foldRight_tr(as, empty[B])((a, bs) => append_fr(f(a), bs))

  /** Sequentially composes two flattened list mappings. */
  def compose_fr[A, B, C]
      (f: A => List[B])
      (g: B => List[C]): A => List[C] =
    a => foldRight_tr(f(a), empty[C])((b, cs) => append_fr(g(b), cs))

  /*
   * Notes: For Illustration, possibly also Efficiency
   *
   * Functions can be implemented directly on foldLeft and reversing result.
   *
   * There may gains in efficiency vs foldRight (which reverses the input):
   * - `join_fl`: (n * m) reversals vs (n + n * m) with foldRight?
   * - `flatMap_fl`: more efficient for filtering ops, less for inserting,
   *   due to deferral of input list reversal to result?
   */

  import ex_3_1.foldLeft_tr
  import ex_3_4.{reverseAppend_fl, reverse_fl}

  def join_fl[A]
      (aas: List[List[A]]): List[A] =
    reverse_fl(
      foldLeft_tr(empty[A], aas)((a0, a1) => reverseAppend_fl(a1, a0))
    )

  def flatMap_fl[A, B]
      (as: List[A])
      (f: A => List[B]): List[B] =
    reverse_fl(
      foldLeft_tr(empty[B], as)((bs, a) => reverseAppend_fl(f(a), bs))
    )

  def compose_fl[A, B, C]
      (f: A => List[B])
      (g: B => List[C]): A => List[C] =
    a => foldLeft_tr(empty[C], f(a))((cs, b) => reverseAppend_fl(g(b), cs))
}
