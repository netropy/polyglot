package netropy.polyglot.fp.lists


object ex_2_7 {
  import ex_1_3.List
  import List.{Nil, Cons}
  import ex_2_4.reverse_tr

  /*
   * Lazy prefix/suffix/subsequence test functions
   * - non-strict `||`, `&&` preserve tail-recursion
   *
   * `startsWith_tr`, `hasSubsequence_tr`
   * naturally tail-recursive, left-recursion on 2 lists
   * can place the tests on the left or right side of `. => .`
   *
   * `endsWith_tr`
   * simple & efficient to just reverse both list for left-recursion
   */

  /** Returns whether a list starts with the same elements as another list. */
  @annotation.tailrec
  def startsWith_tr[A]
      (as: List[A], prefix: List[A]): Boolean =
    (as, prefix) match {
      case (Cons(h0, t0), Cons(h1, t1)) => (h0 == h1) && startsWith_tr(t0, t1)
      case (_, Nil) => true
      case _ => false
      // BUG: Scala 3.1.0, incorrect 'Unreachable case' warning:
      //case (_, _) => assert(as == Nil && prefix != Nil); false
      // needlesly specific, but no Scala 3.1.0 'Unreachable case' warning:
      //case (Nil, _) => assert(as == Nil && prefix != Nil); false
      // old, incorrect Dotty compiler warning: case _ => only matches null
      //case _ => assert(as == Nil && prefix != Nil); false
    }

  /** Returns whether a list contains another list as a subsequence. */
  @annotation.tailrec
  def hasSubsequence_tr[A]
      (as: List[A], sub: List[A]): Boolean =
    (sub == Nil) || {
      as match {
        case Cons(h, t) => startsWith_tr(as, sub) || hasSubsequence_tr(t, sub)
        case Nil => false
      }
    }

  /** Returns whether a list ends with the same elements as another list. */
  def endsWith_tr[A]
      (as: List[A], suffix: List[A]): Boolean =
    startsWith_tr(reverse_tr(as), reverse_tr(suffix))
}
