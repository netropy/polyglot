package netropy.polyglot.fp.lists


object ex_7_2 {
  import ex_1_3.List
  import ex_7_1.zipWith_tr

  /*
   * Notes: Zipable use case
   */

  /** Returns a list of sums of the corresponding elements from two lists. */
  def zipAdd_tr
      (as0: List[Int], as1: List[Int]): List[Int] =
    zipWith_tr(as0, as1)(_ + _)
}
