package netropy.polyglot.fp.lists


/*
 * Exercise Series 1: Linked List API Variants
 */

object ex_1_1 {

  /*
   * Notes: Straight-forward OO-style design of core List type.
   *
   * - List GADT as abstract class (or trait), subtyping case class/object
   * - accessors defined + implemented as methods, no external functions
   * - no need for argument checking, down-casting, or pattern-matching
   * - single location for documentation in trait
   * - 4 natural scopes: trait, companion, case class, case object
   * - total: 52 lines for code + minimal scaladoc
   */

  /** An immutable, singly-linked list (a.k.a. LinkedList). */
  sealed abstract class List[+A] {

    /** Returns the first element of this non-empty list.
      * @throws [[UnsupportedOperationException]] if Nil
      */
    def head: A

    /** Returns the rest of this non-empty list.
      * @throws [[UnsupportedOperationException]] if Nil
      */
    def tail: List[A]

    /** Returns an [[Option]] of the first element of this list. */
    def headOption: Option[A]

    /** Returns an [[Option]] of the rest of this list. */
    def tailOption: Option[List[A]]
  }

  case object Nil extends List[Nothing] {

    def head: Nothing =
      throw new UnsupportedOperationException(s"$this.head")

    def tail: List[Nothing] =
      throw new UnsupportedOperationException(s"$this.tail")

    def headOption: Option[Nothing] = None

    def tailOption: Option[List[Nothing]] = None
  }

  case class Cons[+A](head: A, tail: List[A]) extends List[A] {
    require(tail != null)

    def headOption: Option[A] = Some(head)

    def tailOption: Option[List[A]] = Some(tail)
  }

  object List {

    /** Creates a list from the sequence of values. */
    def apply[A](as: A*): List[A] =
      as.foldRight(empty[A])((a, l) => Cons(a, l))

    /** Also called `pure` for Applicatives (with `unit: List[Unit]`). */
    def unit[A](a: A): List[A] = Cons(a, Nil)

    def empty[A]: List[A] = Nil
  }
}
