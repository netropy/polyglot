package netropy.polyglot.fp.lists


/*
 * Exercise Series 6: (List) Applicative API
 */

object ex_6_1 {
  import ex_1_3.List
  import List.empty
  import ex_3_1.foldRight_tr
  import ex_3_4.append_fr
  import ex_5_1.map_fr

  /*
   * Notes:
   *
   * `tuple2`, `map2`, and `ap` (for "apply") are core functions that allow
   * to form Cartesian Products from multiple lists.  This allows to lift
   * functions of higher arities and apply them to multiple collections.
   *
   * `ap` is often defined with reversed arguments: (List[A])(List[A => B]).
   * Note, though, that the functions are still sequenced before the values.
   *
   * Above three functions are _equivalent_: subsequent exercise to implement
   * them in terms of another.  Here, all implementations given on foldRight.
   *
   * These methods, together with List's `unit` (a.k.a. `pure`) constructor,
   * define the fundamental FP type `Applicative`.
   *
   * Tried here to give plain-english, list-specific descriptions/scaladocs.
   * See .../fp exercises for Applicative laws and relation to other types.
   *
   * @see [[https://typelevel.org/cats/typeclasses/applicative.html]]
   * @see [[https://typelevel.org/cats/api/cats/Applicative.html]]
   */

  /** Returns the List of all pairs (a, b), sequencing as, then bs. */
  def tuple2_fr[A, B]
      (as: List[A], bs: List[B]): List[(A, B)] = {
    val abs: List[List[(A, B)]] = map_fr(as)(a => map_fr(bs)((a, _)))
    foldRight_tr(abs, empty[(A, B)])((ab0, ab1) => append_fr(ab0, ab1))
  }

  /** Applies the function to each pair (a, b), sequencing as, then bs. */
  def map2_fr[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C): List[C] = {
    val cs: List[List[C]] = map_fr(as)(a => map_fr(bs)(f(a, _)))
    foldRight_tr(cs, empty[C])((c0, c1) => append_fr(c0, c1))
  }

  /** Applies each function to each value, sequencing fs, then as. */
  def ap_fr[A, B]
      (fs: List[A => B])
      (as: List[A]): List[B] = {
    val bs: List[List[B]] = map_fr(fs)(f => map_fr(as)(f(_)))
    foldRight_tr(bs, empty[B])((b0, b1) => append_fr(b0, b1))
  }

  /*
   * Notes:
   *
   * The implementations of the above functions share the same pattern:
   * formation of nested `List[List[_]]`, subsequent flattening to `List[_]`.
   *
   * This structural similarity suggests an equivalence (see ex_6_3) and
   * common abstraction (see ex_8_1).
   */

  /*
   * For illustration:
   *
   * Above functions can also be implemented directly using `foldLeft` and
   * reversing the result list last.  There's a performance disadvantage,
   * though, since the output list generally is larger than the inputs.
   */

  import ex_3_1.foldLeft_tr
  import ex_3_4.{reverseAppend_fl, reverse_fl}
  import ex_5_1.map_fl

  def tuple2_fl[A, B]
      (as: List[A], bs: List[B]): List[(A, B)] = {

    val abs: List[List[(A, B)]] = map_fl(as)(a => map_fl(bs)((a, _)))
    reverse_fl(
      foldLeft_tr(empty[(A, B)], abs)
        ((ab0, ab1) => reverseAppend_fl(ab1, ab0))
    )
  }

  def map2_fl[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C): List[C] = {

    val cs: List[List[C]] = map_fl(as)(a => map_fl(bs)(f(a, _)))
    reverse_fl(
      foldLeft_tr(empty[C], cs)
        ((c0, c1) => reverseAppend_fl(c1, c0))
    )
  }

  def ap_fl[A, B]
      (fs: List[A => B])
      (as: List[A]): List[B] = {

    val bs: List[List[B]] = map_fl(fs)(f => map_fl(as)(f(_)))
    reverse_fl(
      foldLeft_tr(empty[B], bs)
        ((b0, b1) => reverseAppend_fl(b1, b0))
    )
  }
}
