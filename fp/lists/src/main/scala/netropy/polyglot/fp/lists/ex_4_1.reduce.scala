package netropy.polyglot.fp.lists


/*
 * Exercise Series 4: (List) Reducible API
 */

object ex_4_1 {
  import ex_1_3.List
  import List.{Nil, Cons, head, tail}
  import ex_3_1.foldLeft_tr

  /*
   * Notes: (Strict) Reduce combinator
   *
   * The `reduce` combinator applies an associative binary operator to the
   * elements of a non-empty collection without requiring a neutral element.
   *
   * This API takes advantage of `Cons` subtyping `List` (see APIs 1_1_1-3):
   * Overloading reduce for `List` and `Cons` allows of minor optimizations,
   * only the List-based variant must check for empty lists and throw IAE.
   *
   * Alternatively, `reduceOption` accepts empty lists.
   *
   * Internally, reduce passes `head` as start value and therefore must use
   * foldLeft.
   */

  /** Applies associative operator to elements of non-empty list. */
  def reduce_f[A, A1 >: A]
      (as: Cons[A])
      (op: (A1, A1) => A1): A1 =
    foldLeft_tr(as.head: A1, as.tail)(op)  // preserve head :: tail

  /** Applies associative operator to elements of non-empty list.
    * @throws [[IllegalArgumentException]] if `as` is Nil
    */
  def reduce_f[A, A1 >: A]
      (as: List[A])
      (op: (A1, A1) => A1): A1 =
    foldLeft_tr(head(as): A1, tail(as))(op)  // head(Nil), tail(Nil) throw

  /** Applies associative operator to elements of list. */
  def reduceOption_f[A, A1 >: A]
      (as: List[A])
      (op: (A1, A1) => A1): Option[A1] =
    as match {
      case Cons(h, t) => Some(foldLeft_tr(h: A1, t)(op))  // preserve h :: t
      case Nil => None
    }

  /*
   * Notes: No directional Reduce Combinator Variants
   *
   * Not given here are the directional variants reduceLeft/reduceRight that
   * map to foldLeft/foldRight and do not require an associative operator:
   * ```
   *     // Applies binary operator to list elements, left->right:
   *     def reduceLeft_fl[A, A1 >: A]
   *         (as: Cons[A])
   *         (op: (A1, A) => A1): A1
   *     def reduceLeft_fl[A, A1 >: A]
   *         (as: List[A])
   *         (op: (A1, A) => A1): A1
   *     def reduceLeftOption_fl[A, A1 >: A]
   *         (as: List[A])
   *         (op: (A1, A) => A1): Option[A1]
   *
   *     // Applies binary operator to list elements, left<-right:
   *     def reduceRight_fr[A, A1 >: A]
   *         (as: Cons[A])
   *         (op: (A, A1) => A1): A1
   *     def reduceRight_fr[A, A1 >: A]
   *         (as: List[A])
   *         (op: (A, A1) => A1): A1
   *     def reduceRightOption_fr[A, A1 >: A]
   *         (as: List[A])
   *         (op: (A, A1) => A1): Option[A1]
   * ```
   */
}
