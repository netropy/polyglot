package netropy.polyglot.fp.lists

import scala.math.BigInt


object ex_2_6 {
  import ex_1_3.List
  import List.{Nil, Cons}
  import ex_2_4.reverse_tr

  /*
   * Lazy MapReduce-like functions
   * - non-strict `||`, `&&`, `if-then-else` preserve tail-recursion
   *
   * `exists_tr`, `forall_tr`, `find_tr`, `get_tr`
   * lazy, left-recursive reduce
   * non-strict: the passed predicate is only called from left while needed
   *
   * `count_f`
   * left-recursive reduce, needs to test all elements
   */

  /** Returns whether at least one element satisfies the predicate. */
  @annotation.tailrec
  def exists_tr[A]
      (as: List[A])
      (p: A => Boolean): Boolean =
    as match {
      case Cons(h, t) => p(h) || exists_tr(t)(p)
      case Nil => false
    }

  /** Returns whether all elements satisfies the predicate. */
  @annotation.tailrec
  def forall_tr[A]
      (as: List[A])
      (p: A => Boolean): Boolean =
    as match {
      case Cons(h, t) => p(h) && forall_tr(t)(p)
      case Nil => true
    }

  /** Returns the number of elements that satisfy the predicate. */
  def count_tr[A]
      (as: List[A])
      (p: A => Boolean): Long = {

    @annotation.tailrec
    def go(as: List[A], r: Long): Long =
      as match {
        case Cons(h, t) => go(t, if (p(h)) r + 1 else r)
        case Nil => r
      }
    go(as, 0L)
  }

  /** Returns an element that matches the predicate, if one exists. */
  @annotation.tailrec
  def find_tr[A]
      (as: List[A])
      (p: A => Boolean): Option[A] =
    as match {
      case Cons(h, t) => if (p(h)) Some(h) else find_tr(t)(p)
      case Nil => Option.empty[A]
    }

  /** Returns the element at the index of the list, if within bounds. */
  def get_tr[A]
      (as: List[A], i: Int): Option[A] = {

    @annotation.tailrec
    def go(as: List[A], j: Int): Option[A] =
      as match {
        case Cons(h, t) => if (j == i) Some(h) else go(t, j + 1)
        case Nil => Option.empty[A]
      }
    if (i < 0) Option.empty[A] else go(as, 0)
  }
}
