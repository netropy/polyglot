package netropy.polyglot.fp.lists


object ex_3_2 {
  import ex_1_3.List
  import ex_3_1.foldLeft_tr
  import ex_3_1.foldRight_tr

  /*
   * Notes: Non-Directional Fold combinators, using a Monoid
   *
   * If the binary operation passed to a fold operation is _associative_ and
   * the start value is a _neutral element_, i.e., a Monoid, folding from
   * left or from right yields the same result.
   *
   * These non-directional combinators `fold`, `foldMap` pursue that case:
   * - `fold` directly combines the list elements (and neutral element),
   * - `foldMap` first maps them to a Monoid type and then combines them.
   *
   * Stack-safe: `foldMap_fl`, `fold_fl` delegate to `foldLeft_tr`.
   */

  /** Maps each element to a Monoid B(f, op) and combines the results. */
  def foldMap_fl[A, B]
      (as: List[A])
      (f: A => B)
      (z: B, op: (B, B) => B): B =
    foldLeft_tr(z, as)((l, a) => op(l, f(a)))

  /** Applies the associative operator to list elements, neutral element. */
  def fold_fl[A, A1 >: A]
      (as: List[A], z: A1)
      (op: (A1, A1) => A1): A1 =
    foldMap_fl[A, A1](as)(identity[A1])(z, op)  // nice :-)

  /*
   * Also offering above folds based on foldRight.  No signature change.
   *
   * Helpful for testing fold operators' usage, swap variant: _fl <-> _fr
   * to check whether directional assumptions on type, behaviour were made.
   */

  def foldMap_fr[A, B]
      (as: List[A])
      (f: A => B)
      (z: B, op: (B, B) => B): B =
    foldRight_tr(as, z)((a, l) => op(f(a), l))

  def fold_fr[A, A1 >: A]
      (as: List[A], z: A1)
      (op: (A1, A1) => A1): A1 =
    foldMap_fr[A, A1](as)(identity[A1])(z, op)
}
