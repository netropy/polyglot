package netropy.polyglot.fp.lists


object ex_2_3 {
  import ex_1_3.List
  import List.{Nil, Cons, head, tail}

  /*
   * Recursive list construction without possibility for data sharing
   * - tail-recursive versions would require list reversal
   * - see ex_2_4, ex_3_5
   */

  /** Returns the last element of a non-empty list. */
  @annotation.tailrec
  def last_tr[A](as: List[A]): A = {
    require(as != Nil)
    as match {
      case Cons(h, Nil) => h
      case Cons(h, t) => last_tr(t)
    }
  }

  /** Returns the initial part of a list without its last element. */
  // not: @annotation.tailrec
  def init_r[A](as: List[A]): List[A] = {
    require(as != Nil)
    as match {
      case Cons(h, Nil) => Nil
      case Cons(h, t) => Cons(h, init_r(t))
    }
  }

  /** Takes the first `n` elements from a list with `>= n` elements. */
  // not: @annotation.tailrec
  def take_r[A]
      (as: List[A], n: Int): List[A] = {
    require(n >= 0)
    if (n == 0) Nil
    else Cons(head(as), take_r(tail(as), n - 1))
  }

  /** Returns a list retaining only initial elements which match p. */
  // not: @annotation.tailrec
  def takeWhile_r[A]
     (as: List[A])
     (p: A => Boolean): List[A] = {
    require(p != null)
    as match {
      case Cons(h, t) if (p(h)) => Cons(h, takeWhile_r(t)(p))
      case _ => Nil
    }
  }

  /** Returns a list of only those elements that satisfy the predicate. */
  // not: @annotation.tailrec
  def filter_r[A]
      (as: List[A], p: A => Boolean): List[A] = {
    require(p != null)
    as match {
      case Cons(h, t) if p(h) => Cons(h, filter_r(t, p))
      case Cons(h, t) => filter_r(t, p)
      case Nil => Nil
    }
  }
}
