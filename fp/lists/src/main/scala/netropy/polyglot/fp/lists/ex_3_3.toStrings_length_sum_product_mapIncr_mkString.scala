package netropy.polyglot.fp.lists

import scala.math.BigInt


object ex_3_3 {
  import ex_1_3.List
  import List.{Nil, Cons, empty}
  import ex_3_1.{foldLeft_tr, foldRight_tr}
  import ex_3_2.{fold_fl, foldMap_fl}

  /*
   * Notes: Map-/Reduce-style functions, natural fit for folds
   *
   * However, there may be subtleties regarding directionality:
   * - `fold` if associative reduce-like operations with neutral element,
   * - `foldMap` if the associative operation works on a different type,
   * - `foldLeft` if there's an operator that works with input from left,
   * - `foldRight` if the original order must be preserved.
   *
   * See `mkString_fl` for directional subtlety: The operator is no longer
   * associative since the result string must start with separator string.
   */

  def length_f[A]
      (as: List[A]): Int =
    // associative '+' allows for either direction (post mapping a=>1)
    // foldMap: buest suited
    // foldLeft, foldRight: applicable
    // fold: not applicable by operator's type signature: =>Int
    foldMap_fl(as)(_ => 1)(0, _ + _)

  def sum_f
      (as: List[Int]): Int =
    // associative '+' allows for accumulation in either direction
    // fold: buest suited
    // foldMap, foldLeft, foldRight: applicable
    fold_fl(as, 0)(_ + _)

  def product_f
      (as: List[Int]): BigInt =
    // associative '*' allows for accumulation in either direction
    // foldMap: buest suited
    // foldLeft, foldRight: applicable
    // fold: not applicable by operator type signature: =>BigInt (not >: Int)
    foldMap_fl(as)(BigInt(_))(BigInt(1), _ * _)

  def toStrings_fr[A]
      (as: List[A]): List[String] =
    // non-associative Cons requires left<-right construction
    // foldLeft, fold, foldMap: not applicable
    foldRight_tr(as, empty[String])((a, l) => Cons(a.toString, l))

  def mapIncr_fr[A]
      (as: List[Int]): List[Int] =
    // non-associative Cons requires left<-right construction
    // foldLeft, fold, foldMap: not applicable
    foldRight_tr(as, empty[Int])((a, l) => Cons(a + 1, l))

  def mkString_fl[A]
      (as: List[A], start: String, sep: String, end: String): String = {

    // string '+' is associative, however: separator requires direction
    // foldleft:  applicable if concatenating as: (a, s) => sep + a + s
    // foldRight: applicable if concatenating as: (s, a) => s + sep + a)
    // foldMap: not applicable due to different separator concat patterns
    // fold: not applicable by operator's type signature: =>String
    val s = as match {
      case Nil => ""
      case Cons(h, Nil) => h.toString
      case Cons(h, t) =>
        // head elem requires fold to produce string starting with s"$sep..."
        h.toString + foldLeft_tr("", t)(_ + sep + _)
        //h.toString + foldRight_tr(t, "")(sep + _ + _)
    }
    start + s + end
  }
}
