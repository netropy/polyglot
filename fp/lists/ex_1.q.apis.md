# polyglot/fp/LinkedLists

### 1 Compare API Definitions for Linked-List as Functional Datastructure

___Motivation:___

Typically, the definition of a data type List (or other GADTs) is given as a
3-liner: a type constructor, two data constructors.

The design space is much larger, though -- which this exercise is to explore a
bit.

Starting with the big decisions:  Write List as a
- stand-alone type (i.e., not as a member of an existing OO type hierarchy or
  set of typeclasses),
- functional data structure (immutability, ideally with use of data sharing).

There's still to be decided:
- Model List as OO type or FP type?
  - Define subtypes for the values?  Expose them?
  - Model core features as methods or functions?  Utilize overriding or
    pattern-matching?
  - Confine value, function definitions to class/companion scope or export
    them at package-level?
  - Use Scala3's `enum` for singleton and parameterized values?  Use for
    values only or also add behaviour?
- Nil-Punning: Disallow, ignore, or utilize `null` as a value?
- Error handling: `require` arguments and throw exceptions or map to monadic
  values?  Which exception or error types?
- How to access data: directly call fields/methods, support for pattern
  matching, or through accessor functions, or all above?
- Expose type's primary constructor? Which constructors to add to companion?

Later, when adding more methods/functions:  Allow for
- non-strict APIs, utilize by-name parameters?
- partial application, multiple parameter lists?
- recursion, tail-recursion?

(Some Scala style guides forbid some of the above for the project or company.)

___Task:___

Exploring some of the design space, define a linked list data type as:
1. Traditional OOP type with core accessor methods and constructors
2. Functional type & data constructors with a core set of external functions
3. Functional type, but all values/functions scoped, use of `null` as value
4. Scala3 `enum` type with scoped methods/functions

Provide essential scaladoc (i.e., document exceptions so that a simple
lines-of-code count also indicates usage complexity).

Discuss the designs 1..4).

Unit-test (and so document) the error behaviour of functions `tail`, `head`
for designs 1..4), see: \
[Exercise 3.2, FPiS](https://www.scala-exercises.org/fp_in_scala/functional_data_structures)

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala3 sources](src/main/scala3/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/),
[Scala3 tests](src/test/scala3/netropy/polyglot/fp/lists/)


#### 1.1 Write a traditional, OO-style List type.

See discussion in the code comments.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_1_1.list_type_oop.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_1_1.list_type_oop.scala)

#### 1.2 Write a traditional, FP-style List type.

See discussion in the code comments.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_1_2.list_type_fp.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_1_2.list_type_fp.scala)

#### 1.3 Write an FP-style, scoped List type with use of `null` as value.

See discussion in the code comments.

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_1_3.list_type_fpnull.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_1_3.list_type_fpnull.scala)

#### 1.4 Write a Scala3 `enum` List type.

See discussion in the code and test comments, and the remarks on Scala3/Dotty
enum's for defining GADTs.

[Remarks](ex_1_4.r.md),
[Scala3 source](src/main/scala3/netropy/polyglot/fp/lists/ex_1_4.list_type_enum.scala),
[Scala3 test](src/test/scala3/netropy/polyglot/fp/lists/ex_1_4.list_type_enum.scala)

[Up](./README.md)
