# polyglot/fp/LinkedLists

### 7 (List) Zipable (Traversal) Functions

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/fp/lists/),
[Scala tests](src/test/scala/netropy/polyglot/fp/lists/)

#### 7.1 Implement combinators `zipWith`, `zip`.

Implement the combinators directly and in terms or each other:
- tail-recursive `zipWith_tr`, `zip_zw` in terms of _zipWith_
- tail-recursive `zip_tr`, `zipWith_zm` in terms of _zip_ and _map_

```
  /** Applies a function to each pair of elements, while there are any. */
  def zipWith_{tr,zm}[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, B) => C) : List[C]

  /** Pairs corresponding elements from each list, while there are any. */
  def zip_{zw,tr}[A, B]
      (as: List[A], bs: List[B]): List[(A, B)] =
    zipWith_tr(as, bs)((_, _))
```

Discuss how these zip-style functions relate to the core FP type _Traversal_.

Resources: \
<https://typelevel.org/cats/typeclasses/traverse.html>,
<https://typelevel.org/cats/api/cats/Traverse.html>

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_7_1.zipWith_zip.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_7_1.zipWith_zip.scala)

#### 7.2 Implement function `zipAdd`.

```
  /** Returns a list of sums of the corresponding elements from two lists. */
  def zipAdd_tr
      (as0: List[Int], as1: List[Int]): List[Int]
```

[Scala source](src/main/scala/netropy/polyglot/fp/lists/ex_7_2.zipAdd.scala),
[Scala test](src/test/scala/netropy/polyglot/fp/lists/ex_7_2.zipAdd.scala)

#### 7.3 Implement functions `zipL`, `zipR`, `zipWithL`, `zipWithR`, `apZip`, `zipWithIndex`, `mapWithIndex`.

TODO: Consider implementing the other zip-style functions:
left/right length-preserving zips, allowing for missing elements
```
  def zipL[A, B]
      (as: List[A], bs: List[B]): List[(A, Option[B])]

  def zipR[A, B]
      (as: List[A], bs: List[B]): List[(Option[A], B)]

  def zipWithL[A, B, C]
      (as: List[A], bs: List[B])
      (f: (A, Option[B]) => C): List[C]

  def zipWithR[A, B, C]
      (as: List[A], bs: List[B])
      (f: (Option[A], B) => C): List[C]
```

TODO: Consider implementing the other zip-style functions:
zips that pair each element with its index, mapped value
```
  /** Pairs each list element with its mapped value by given function. */
  def apZip[A, B]
      (as: List[A])
	  (f: List[A] => List[B]): List[(A, B)]

  /** Zips each element with its index. */
  def zipWithIndex[A]
      (as: List[A]): List[(A, Int)]
    // see fpis 12.11, factoring out our mapAccum function, uses state monad

  /** Maps each list element along with its index. */
  def mapWithIndex[A, B]
      (as: List[A])(f: (A, Int) => B): List[B]
```

[Up](./README.md)
