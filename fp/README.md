# polyglot/fp

### Functional Programming: Code Experiments, Exercises, Notes

Refcard: _1+10 curried function signatures to know by heart_
```
// types F[_], G[_], A, B, C
unit:       A                               => F[A]     // pure
map:        F[A] => (A => B)                => F[B]
map2:       F[A] => F[B] => (A => B => C)   => F[C]
ap:         F[A => B] => F[A]               => F[B]     // apply
flatMap:    F[A] => (A => F[B])             => F[B]
traverse:   F[A] => (A => G[B])             => G[F[B]]  // G: Applicative
sequence:   F[G[A]]                         => G[F[A]]  // G: Applicative
foldRight:  F[A] => B => ((A, B) => B)      => B
foldLeft:   B => F[A] => ((B, A) => B)      => B
foldMap:    F[A] => (A => B)                => B        // B: Monoid
reduce:     F[A]                            => A        // A: Semigroup
```

TODO: add: unfold?, apply, unapply? zip, unzip?

Diagrams: _10 Core FP Types to Know by Heart_, simplified to fit on single page
- [Refcard FP Types + Operations](Refcard_FP_types_ops.png)
- [Refcard FP Types + Laws](Refcard_FP_types_laws.png)
- [Resources, Diagrams on CATS etc](FP_Types_notes_resources.md) TODO cleanup
- [style guide comments](../guides/README.md) TODO: fix link
  immutable data structures, _null_ vs _Option_ etc

TODO: list */README.md

Notes, projects, exercises:
- [./cats](cats/README.md),
  code, notes on CATS, links to related libs
- [Functional Programming in Scala (a.k.a. "Red Book")](FPiS_RedBook.md)
- [LinkedLists (FP in Scala)](lists/README.md)
- [Recursion, Higher Order Functions](recursion/ex_1.q.recursion.md)
- [FixPoint Combinators (Y, Z)](recursion/ex_2.q.fixpoint_combinators.md)
- [Recursion on \[Im-\]Mutable Collections](recursion/ex_3.q.recursion.md)
- [_Effect_ types (StateM, ReaderM)](effects/ex_1.q.effects.md)
- [CATS: code experiments, exercises, notes](cats/)
- [Notes: Other Scala FP Libraries](Notes_other_Scala_libs.md)

Other projects:
- [../bigdata](../bigdata/README.md):
  code, notes on Spark, Hadoop, map-reduce in Java/Scala, circe JSON lib etc

Used Tools: \
[sbt](https://www.scala-sbt.org),
[Maven](https://maven.apache.org),
[MUnit](https://scalameta.org/munit),
[ScalaCheck](https://www.scalacheck.org),
[MUnit-ScalaCheck](https://index.scala-lang.org/scalameta/munit/artifacts/munit-scalacheck)

Problem/language/library features: \
[tags](tags.md)

[Up](../README.md)
