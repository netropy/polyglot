# polyglot

### Root Project Code Example: Multi-Project Build with sbt, Gradle, Maven

This root project serves as code example for a multi-project build with:
[sbt](https://www.scala-sbt.org),
[Maven](https://maven.apache.org),
[Gradle](https://gradle.org)

Multi-project builds make use of one or both:
- _project aggregation_: propagating build actions from a root/parent project
  to listed sub-projects (with their inter-dependencies);
- _project inheritance_: sharing of configurations or build rules from the
  root/parent project with sub-projects.

Aggregation is implemented for all build tools here: sbt, Gradle, and Maven.

Project inheritance is only given for Maven.  Gradle's and sbt's preferred
mechanism for sharing configuration/behaviour appears a bit more complicated
and uses _configuration injection_ or the placing of Scala files under
_project/_.

___Usage:___

The common tasks `{sbt,gradlew,mvn} {clean,test}` propagate to sub-projects.

However, must invoke for each: `sbt <task>`, `gradlew <task>`, `mvn <task>`.

Currently, not all sub-projects have build files for each build tool: Some
still lack features required by the sub-project (e.g. support for _dotty_ or
building of C/C++ sources).

_TODO:_ Have build tools invoke each other for complete coverage by each tool.

[Up](./README.md)
