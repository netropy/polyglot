package netropy.polyglot.bigdata.spark;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;


/**
 * Ex 1.1 Spark API usage example: RDD, Dataset, DataFrame with a data class.
 */
class Ex_1_1_BeanClassUse {

    /*
     * No need for setup/teardown, instance initializer good enough.
     */

    final SparkSession spark = SparkSession
        .builder()
        .appName("Spark Ex_1_1_BeanClassUse Test")
        .master("local[*]")
        .getOrCreate();

    /*
     * Define a data class (as Bean).
     */

    static public class Person
        implements Serializable {  // required

        // Recommended: Bean pattern (helps use with Spark SQL API, Encoders)

        private String name;
        private int age;

        public Person(String name, int age) {  //
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        // Required: Setters (for Encoders to recognize Bean pattern)

        public void setName(String name) {
            this.name = name;
        }

        public void setAge(int age) {
            this.age = age;
        }

        // Optional: serialization UID, hashCode, equals

        private static final long serialVersionUID = 20201212L;

        @Override
        public int hashCode() {
            return Objects.hash(name, age);
        }

        @Override
        public boolean equals(Object other) {
            if (other == this)
                return true;

            if (other == null || other.getClass() != this.getClass())
                return false;

            Person o = (Person)other;
            return Objects.equals(name, o.name)
                && (age == o.age);
        }
    }

    /*
     * Create local collections List<Bean/Row>.
     */

    final List<Person> personList =
        Arrays.asList(new Person("John Doe", 33), new Person("Jane Doe", 32));

    final List<Row> personRowList = personList
        // Java Collections do NOT offer map, only Streams do
        .stream()
        .map(p -> RowFactory.create(p.name, p.age))
        .collect(Collectors.toList());

    /*
     * Create RDDs<Bean/Row> from List.
     */

    final JavaSparkContext javaSparkContext =
        new JavaSparkContext(spark.sparkContext());

    final JavaRDD<Person> personJavaRDD0 = javaSparkContext
        .parallelize(personList);

    /*
     * Load RDDs<Bean/Row> from text (or binary) files.
     */

    final JavaRDD<String[]> personJavaRDD1 = javaSparkContext
        .textFile("../test_data/persons.csv")
        .map(s -> s.split(","));

    // shows conversion as expression lambda (static function, alternatively)
    final JavaRDD<Person> personJavaRDD2 = personJavaRDD1
        .map(args -> {
                int age = Integer.parseInt(args[1]);
                return new Person(args[0], age);
             });

    /*
     * Create Datasets<Bean/Row> from List, RDD.
     */

    // loading objects into Spark SQL requires encoders
    final Encoder<Person> encoder = Encoders.bean(Person.class);

    final Dataset<Person> personDs0 =
        spark.createDataset(personList, encoder);

    final Dataset<Person> personDs1 =
        spark.createDataset(personJavaRDD0.rdd(), encoder);

    {
        // Note: deconstruction NOT equal
        // assert(personDs0.collect().equals(personList));
        // assert(personDs1.javaRDD().equals(personJavaRDD0));
    }

    /*
     * Create DataFrames from List, RDD, Dataset.
     */

    final Dataset<Row> personDF0 =
        spark.createDataFrame(personList, Person.class);

    // parsing Rows requires a schema:
    //
    // 1) can get from case class:
    //    final StructType personSchema = Encoders.bean(Person.class).schema();
    //    NOT working: possibly wrong field order
    //      The value (33) of the type (java.lang.Integer) cannot be converted
    //      to the string type
    //
    // 2) or construct programmatically
    final boolean nullable = true;
    final Metadata empty = Metadata.empty();
    final StructType personSchema =
        new StructType(
            new StructField[] {
                new StructField("name", DataTypes.StringType, nullable, empty),
                new StructField("age", DataTypes.IntegerType, !nullable, empty)
            });

    final Dataset<Row> personDF1 =
        spark.createDataFrame(personRowList, personSchema);

    final Dataset<Row> personDF2 =
        spark.createDataFrame(personJavaRDD0.rdd(), Person.class);

    final Dataset<Row> personDF3 =
        personDs1.toDF();

    {
        // Note: deconstruction NOT equal
        // assert(personDF0.collect().equals(personList));
        // assert(personDF2.javaRDD().equals(personJavaRDD0));
        // assert(personDF3.as(encoder).equals(personDs1));
    }

    /*
     * Load DataFrames from csv, json[l], parquet, orc, avro.
     */

    // faster csv parsing: provide a schema (or disable inferSchema option)
    final Dataset<Row> personDF4 = spark.read()
        .schema(personSchema)
        //.option("sep", ",")  // default
        //.option("header", "false")  // default
        //.option("inferSchema", "false")  // default
        .csv("../test_data/persons.csv");

    final Dataset<Row> personDF5 = spark.read()
        //.option("multiLine", "false")  // default: JSON Line format
        .option("allowComments", "true")
        .json("../test_data/persons.jsonl");

    final Dataset<Row> personDF6 = spark.read()
        .option("multiLine", "true")  // one-record-per-file JSON format
        .option("allowComments", "true")
        .json("../test_data/persons.json");

    final Dataset<Row> personDF7 = spark.read()
        .parquet("../test_data/persons.parquet");

    final Dataset<Row> personDF8 = spark.read()
        .orc("../test_data/persons.orc");

    final Dataset<Row> personDF9 = spark.read()
        .format("avro").load("../test_data/persons.avro");

    /*
     * Convert DataFrame -> Dataset
     */

    final Dataset<Person> personDs2 =
        personDF4.as(encoder);
}
