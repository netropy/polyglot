package netropy.polyglot.bigdata.spark

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, Encoders, Row, SparkSession}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField,
  StructType}


/**
  * Ex 1.1 Spark API usage example: RDD, Dataset, DataFrame with a data class.
  */
object ex_1_1_CaseClassUse {

  /*
   * No need for injection/setup/teardown, instance initializer good enough.
   */

  val spark = SparkSession
    .builder()
    .appName("Spark ex_1_1_CaseClassUse Test")
    .master("local[*]")
    .getOrCreate()

  /*
   * Define a data class (as case class).
   */

  case class Person(name: String, age: Int)

  /*
   * Create local collections Seq[CaseClass/Row].
   */

  val persons = Seq(Person("John Doe", 33), Person("Jane Doe", 32))

  val personRows = persons.map(p => Row(p.name, p.age))

  /*
   * Create RDDs[CaseClass/Row] from Seq.
   */

  val personRDD0: RDD[Person] = spark.sparkContext
    .parallelize(persons)

  /*
   * Load RDDs[CaseClass/Row] from text (or binary) files.
   */

  val personRDD1: RDD[Array[String]] = spark.sparkContext
    .textFile("../test_data/persons.csv")
    .map(s => s.split(","))

  val personRDD2: RDD[Person] = personRDD1
    .map(args => Person(args(0), args(1).toInt))

  /*
   * Create Datasets[CaseClass/Row] from Seq, RDD.
   */

  // loading objects into Spark SQL requires encoders:
  //
  // 1) infer encoder using implicit conversion:
  //    import spark.implicits._
  //    val personDs0: Dataset[Person] = persons.toDS()
  //
  // 2) or provide encoder instance
  val /*implicit*/ encoder = Encoders.product[Person]

  val personDs0: Dataset[Person] = spark.createDataset(persons)(encoder)

  val personDs1: Dataset[Person] = spark.createDataset(personRDD0)(encoder)

  {
    // Note: deconstruction NOT equal
    // assert(personDs0.collect() == persons)
    // assert(personDs1.rdd == personRDD0)
  }

  /*
   * Create DataFrames from Seq, RDD, Dataset.
   */

  val personDF0: DataFrame = spark.createDataFrame(persons)


  // parsing Rows requires a schema:
  //
  // 1) can get from case class:
  //    val personSchema = Encoders.product[Person].schema
  //
  // 2) or construct programmatically
  val personSchema =
    StructType(Array(
      StructField("name", StringType, nullable = true),
      StructField("age", IntegerType, nullable = false)))

  val personDF1: DataFrame = {
    import scala.collection.JavaConverters._  // Scala 2.12
    //import scala.jdk.CollectionConverters._  // Scala 2.13
    spark.createDataFrame(personRows.asJava, personSchema)
  }

  val personDF2: DataFrame = spark.createDataFrame(personRDD0)

  val personDF3: DataFrame = personDs1.toDF()

  {
    // Note: deconstruction NOT equal
    // assert(personDF0.collect() == persons)
    // assert(personDF2.rdd == personRDD0)
    // assert(personDF3.as(encoder) == personDs1)
  }

  /*
   * Load DataFrames from csv, json[l], parquet, orc, avro.
   */

  // faster csv parsing: provide a schema (or disable inferSchema option)
  val personDF4: DataFrame = spark.read
    .schema(personSchema)
    //.option("sep", ",")  // default
    //.option("header", "false")  // default
    //.option("inferSchema", "false")  // default
    .csv("../test_data/persons.csv")

  val personDF5: DataFrame = spark.read
    //.option("multiLine", "false")  // default: JSON Lines format
    .option("allowComments", "true")
    .json("../test_data/persons.jsonl")

  val personDF6: DataFrame = spark.read
    .option("multiLine", "true")  // one-record-per-file JSON format
    .option("allowComments", "true")
    .json("../test_data/persons.json")

  val personDF7: DataFrame = spark.read
    .parquet("../test_data/persons.parquet")

  val personDF8: DataFrame = spark.read
    .orc("../test_data/persons.orc")

  val personDF9: DataFrame = spark.read
    .format("avro").load("../test_data/persons.avro")

  /*
   * Convert DataFrame => Dataset
   */

  val personDs2: Dataset[Person] = personDF4.as(encoder)
}
