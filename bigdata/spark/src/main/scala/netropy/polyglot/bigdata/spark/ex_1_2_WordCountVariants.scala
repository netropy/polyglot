package netropy.polyglot.bigdata.spark

import scala.reflect.ClassTag
import scala.collection.Map  // as used by Spark
import scala.collection.immutable.HashMap

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SparkSession, Dataset, DataFrame, Encoders}
import org.apache.spark.sql.functions.count


/**
  * Ex 1.2 Explore the Spark API: variants of 'wordCount'.
  */
object ex_1_2_WordCountVariants {

  /*
   * No need for injection/setup/teardown, instance initializer good enough.
   */

  val spark = SparkSession
    .builder()
    .appName("Spark ex_1_2_WordCountVariants Test")
    .master("local[*]")
    .getOrCreate()

  val sparkContext = spark.sparkContext

  /*
   * See discussion of wordCount_v00..v17 variants: .../ex_1_2.r.wordcount.md
   */

  def wordCount_v00(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .countByValue()  // builds the map in the driver's memory

  def wordCount_v01(inputTextFile: String): Map[String, Long] = {

    def merge(m: Map[String, Long], s: String, l: Long): Map[String, Long] =
      m.updated(s, m.getOrElse(s, 0L) + l)

    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .aggregate(HashMap.empty: Map[String, Long])(
        (m, s) => merge(m, s, 1L),
        (m, n) => m.foldLeft(n) { case (n, (s, l)) => merge(n, s, l) }
      )
  }

  def wordCount_v02(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .keyBy(identity)  // RDD[(String, String)]
      .countByKey()  // builds the map in the driver's memory

  def wordCount_v03(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .keyBy(identity)  // RDD[(String, String)]
      .mapValues(_ => 1L)
      .reduceByKeyLocally(_ + _)  // builds the map in the driver's memory

  def wordCount_v04(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .keyBy(identity)                             // RDD[(String, String)]
      .mapValues(_ => 1L)
      .foldByKey(0L)(_ + _)  // RDD[(String, Long)]
      .collectAsMap()

  def wordCount_v05(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .keyBy(identity)                             // RDD[(String, String)]
      .aggregateByKey(0L)((n, _) => n + 1, _ + _)  // RDD[(String, Long)]
      .collectAsMap()

  def wordCount_v06(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .keyBy(identity)                             // RDD[(String, String)]
      .combineByKeyWithClassTag[Long](             // param types needed (2.12)
        (_: String) => 1L,                         // createCombiner on 1st val
        (n: Long, _: String) => n + 1,             // mergeValue
        (n: Long, o: Long) => (n + o)              // mergeCombiners
      )(ClassTag.Long)                             // RDD[(String, Long)]
      .collectAsMap()

  def wordCount_v07(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .groupBy(identity)         // RDD[(String, Iterator(String)] <-- overhead
      .mapValues(_.size.toLong)  // RDD[(String, Int->Long)] <-- precision
      .collectAsMap()

  def wordCount_v08(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .map((_, 1L))          // RDD[(String, Long)]
      .foldByKey(0L)(_ + _)  // associative combine function w/ neutral element
      .collectAsMap()

  def wordCount_v09(inputTextFile: String): Map[String, Long] =
    sparkContext.textFile(inputTextFile)
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .map((_, 1L))        // RDD[(String, Long)]
      .reduceByKey(_ + _)  // associative and commutative reduce function
      .collectAsMap()

  def wordCount_v10(inputTextFile: String): Map[String, Long] = {

    import spark.implicits._  // encoders
    spark.read.textFile(inputTextFile)  // Dataset[String]
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .groupByKey(identity)  // KeyValueGroupedDataset[String, String]
      .mapValues(_ => 1L)
      .reduceGroups(_ + _)
      .collect()
      .toMap
  }

  def wordCount_v11(inputTextFile: String): Map[String, Long] = {

    import spark.implicits._  // encoders
    spark.read.textFile(inputTextFile)  // Dataset[String]
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .groupByKey(identity)  // KeyValueGroupedDataset[String, String]
      .mapGroups((k, vs) => (k, vs.size.toLong))
      .collect()
      .toMap
  }

  def wordCount_v12(inputTextFile: String): Map[String, Long] = {

    import spark.implicits._  // encoders
    spark.read.textFile(inputTextFile)  // Dataset[String]
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .groupByKey(identity)  // KeyValueGroupedDataset[String, String]
      .count
      .collect()  // Array[(String, Long)], alternative: collectAsList()
      .toMap      // <-- 2G Scala collection size limit anyway
  }

  def wordCount_v13(inputTextFile: String): Map[String, Long] = {

    /*
     * Issues with: KeyValueGroupedDataset.agg(count) -> AnalysisException
     *
     * Symptom: org.apache.spark.sql.AnalysisException
     *
     * Problem:
     *   ds.groupByKey(identity)
     *      // returns KeyValueGroupedDataset with columns [value, value]
     *     .agg(count("value"))
     *      // "Reference 'value' is ambiguous, could be: value, value."
     *
     * Workaround:
     *     .agg(count("*"))
     *      // returns Dataset[String, Long]
     *
     * Not allowed:
     *     .agg(count("ALL value"))
     *      // "cannot resolve '`ALL value`' given input columns: [value, value]"
     *
     * Inconsistency with SQL, all of these work:
     *   "SELECT value, COUNT(*)         FROM ... GROUP BY value"  // = all
     *   "SELECT value, COUNT(value)     FROM ... GROUP BY value"  // = non-null
     *   "SELECT value, COUNT(ALL value) FROM ... GROUP BY value"  // = all
     *
     * ==> Questionable behaviour of "fixed" Spark bug:
     *   https://issues.apache.org/jira/browse/SPARK-25942
     *   https://github.com/apache/spark/pull/22944
     */

    import spark.implicits._  // encoders
    spark.read.textFile(inputTextFile)  // Dataset[String]
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .groupByKey(identity)    // KeyValueGroupedDataset[String, String]
      //.agg(count("value"))   // !!!sql.AnalysisException!!!
      .agg(count("*"))         // Dataset[String, Long]
      .collect()
      .toMap
  }

  def wordCount_v14(inputTextFile: String): Map[String, Long] = {

    import spark.implicits._  // encoders
    spark.read.textFile(inputTextFile)  // Dataset[String]
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .groupBy("value")   // RelationalGroupedDataset with 1 column
      .count              // DataFrame with 2 columns
      .as[(String, Long)]
      .collect()
      .toMap
  }

  def wordCount_v15(inputTextFile: String): Map[String, Long] = {

    import spark.implicits._  // encoders
    spark.read.textFile(inputTextFile)  // Dataset[String]
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .toDF                     // DataFrame with 1 column
      .groupBy("value")         // RelationalGroupedDataset with 1 column
      .agg("value" -> "count")  // DataFrame with 2 columns
      .as[(String, Long)]
      .collect()
      .toMap
  }

  def wordCount_v16(inputTextFile: String): Map[String, Long] = {

    //spark.catalog.dropTempView("words")
    import spark.implicits._  // encoders
    spark.read.textFile(inputTextFile)  // Dataset[String]
      .flatMap(_.split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .toDF("word")                      // rename column, DURABLE
      .createOrReplaceTempView("words")  // local to df's spark session

    spark.sql("SELECT word, COUNT(*) FROM words GROUP BY word")  // DF
     .as[(String, Long)]
      .collect()
      .toMap
  }

  def wordCount_v17(inputTextFile: String): Map[String, Long] = {

    //spark.catalog.dropTempView("words")
    import spark.implicits._  // encoders
    spark.read.text(inputTextFile)  // DataFrame
      .withColumnRenamed("value", "word0")  // EPHEMERAL, flatMap resets name
      //.toDF("word0")                      // same, EPHEMERAL
      .flatMap(r =>
        r.getAs[String]("word0")
          .split("[\\s\\p{Punct}]").filter(_.nonEmpty))
      .withColumnRenamed("value", "word")   // DURABLE
      //.toDF("word")                       // same, DURABLE
      .createOrReplaceTempView("words")     // local to df's spark session

    spark.sql("SELECT word, COUNT(ALL word) FROM words GROUP BY word")  // DF
      .as[(String, Long)]
      .collect()
      .toMap
  }
}
