package netropy.polyglot.bigdata.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.count;


/**
 * Ex 1.1 Spark API usage example: RDD, Dataset, DataFrame with a data class.
 */
public class Ex_1_1_BeanClassUse_Tests {

    // Surefire only runs test methods that meet _all_ of these criteria:
    // 1. name starts with _exactly_ `test` followed by a non-empty suffix
    // 2. method is parameterless
    // 3. method has a return type of _exactly_ `Unit` (or `void`)

    final Ex_1_1_BeanClassUse impl = new Ex_1_1_BeanClassUse();

    static final int nRows = 2;
    static final int nCols = 2;

    void verify(JavaRDD<?> rdd) {
        assert(nRows == rdd.count());
    }

    void verify(Dataset<?> ds) {
        assert(nRows == ds.count());
        assert(nCols == ds.columns().length);

        // code example: invoke `count` as aggregation argument on DataFrame
        assert(nRows == ds.agg(count("*")).head().getLong(0));

        // code example: invoke `count` as SQL function on registerd view
        ds.createOrReplaceTempView("ds");  // session-local
        assert(nRows
               == impl.spark.sql("select count(*) from ds").head().getLong(0));
    }

    public void test_all() {
        verify(impl.personJavaRDD0);
        verify(impl.personJavaRDD1);
        verify(impl.personJavaRDD2);

        verify(impl.personDs0);
        verify(impl.personDs1);
        verify(impl.personDs2);

        verify(impl.personDF0);
        verify(impl.personDF1);
        verify(impl.personDF2);
        verify(impl.personDF3);
        verify(impl.personDF4);
        verify(impl.personDF5);
        verify(impl.personDF6);
        verify(impl.personDF7);
        verify(impl.personDF8);
        verify(impl.personDF9);
    }
}
