package netropy.polyglot.bigdata.spark

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions.count


/**
  * Ex 1.1 Spark API usage example: RDD, Dataset, DataFrame with a data class.
  *
  * Surefire: Tests must reside in a Scala 'class', not an 'object'.
  */
class Ex_1_1_CaseClassUse_Tests {

  // Surefire only runs test methods that meet _all_ of these criteria:
  // 1. name starts with _exactly_ `test` followed by a non-empty suffix
  // 2. method is parameterless
  // 3. method has a return type of _exactly_ `Unit` (or `void`)

  import ex_1_1_CaseClassUse.{ spark,
    personRDD0,
    personRDD1,
    personRDD2,
    personDs0,
    personDs1,
    personDs2,
    personDF0,
    personDF1,
    personDF2,
    personDF3,
    personDF4,
    personDF5,
    personDF6,
    personDF7,
    personDF8,
    personDF9,
  }

  val nRows = 2
  val nCols = 2

  def verify(rdd: RDD[_]): Unit = {
    assert(nRows == rdd.count())
  }

  def verify(ds: Dataset[_]): Unit = {
    assert(nRows == ds.count())
    //assert(nCols == ds.columns().length)

    // code example: invoke `count` as aggregation argument on DataFrame
    assert(nRows == ds.agg(count("*")).head().getLong(0))

    // code example: invoke `count` as SQL function on registerd view
    ds.createOrReplaceTempView("ds")  // session-local
    assert(nRows == spark.sql("select count(*) from ds").head().getLong(0))
  }

  def test_all(): Unit = {

    verify(personRDD0)
    verify(personRDD1)
    verify(personRDD2)

    verify(personDs0)
    verify(personDs1)
    verify(personDs2)

    verify(personDF0)
    verify(personDF1)
    verify(personDF2)
    verify(personDF3)
    verify(personDF4)
    verify(personDF5)
    verify(personDF6)
    verify(personDF7)
    verify(personDF8)
    verify(personDF9)
  }
}
