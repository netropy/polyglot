package netropy.polyglot.bigdata.spark

import scala.collection.Map  // as used by Spark

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, DataFrame, Encoders}


/**
  * Ex 1.2 Explore the Spark API: variants of 'wordCount'.
  *
  * Surefire: Tests must reside in a Scala 'class', not an 'object'.
  */
class Ex_1_2_WordCountVariants_Tests {

  // Surefire only runs test methods that meet _all_ of these criteria:
  // 1. name starts with _exactly_ `test` followed by a non-empty suffix
  // 2. method is parameterless
  // 3. method has a return type of _exactly_ `Unit` (or `void`)

  import ex_1_2_WordCountVariants.{ spark,
    wordCount_v00,
    wordCount_v01,
    wordCount_v02,
    wordCount_v03,
    wordCount_v04,
    wordCount_v05,
    wordCount_v06,
    wordCount_v07,
    wordCount_v08,
    wordCount_v09,
    wordCount_v10,
    wordCount_v11,
    wordCount_v12,
    wordCount_v13,
    wordCount_v14,
    wordCount_v15,
    wordCount_v16,
    wordCount_v17,
  }

  /*
   * 3 Variants to convert: Dataset[Row] -> ... -> Map[(String, Long)]
   *
   *   _v0: easiest, robust
   *   _v1: easy, possibly fastest
   *   _v2: fragile, relies on field->column order
   */
  private def wordCountMap(df: DataFrame) = wordCountMap_v0(df)

  private def wordCountMap_v0(df: DataFrame): Map[String, Long] = {
    // DataFrame -> RDD[Row] -> RDD[(S,L)] -> Map[(S,L)]
    df.rdd
      .map(r => (r.getAs[String]("word"), r.getAs[Long]("count")))
      .collectAsMap()
  }

  private def wordCountMap_v1(df: DataFrame): Map[String, Long] = {

    if (df.isEmpty)
      return Map.empty[String, Long]

    // DataFrame -> Dataset[(S,L)] -> RDD[(S,L)] -> Map[(S,L)]
    df.select(
        df("word").as[String](Encoders.STRING),   // encoders can be implicit
        df("count").as[Long](Encoders.scalaLong))
      .rdd
      .collectAsMap()
  }

  private def wordCountMap_v2(df: DataFrame): Map[String, Long] = {

    if (df.isEmpty)
      return Map.empty[String, Long]

    // DataFrame -> Dataset[(L,S)] -> [(S,L)] -> Array[(S,L)] -> Map[(S,L)]
    // df.as[(Long, String)]                     // with implicit encoders
    df.as(Encoders.product[(Long, String)])      // with explicit encoders
      .map(_.swap)(Encoders.product[(String, Long)])
      .collect()
      .toMap
  }

  // function under test
  type wordCount = String => Map[String, Long]

  def test_wordCount(wc: wordCount, inputTextFile: String): Unit = {
    val resultMap = wc(inputTextFile)

    // Parse newline-delimited JSON Lines format: // https://jsonlines.org/
    //
    // Problem with one-record-per-file JSON format:
    //   case-insensitivity column name clashes, since field -> column
    //   could try:
    //     sqlContext.conf.set("spark.sql.caseSensitive", "true")
    //   possibly still a problem with special chars
    //
    val goldenJsonFile = inputTextFile + ".jsonl"
    val goldenDF: DataFrame = spark.read
      //.option("multiLine", "false")  // default: JSON Lines format
      .option("allowComments", "true")
      .json(goldenJsonFile)

    // convert to map, verify result
    val goldenMap = wordCountMap(goldenDF)
    val pass = (goldenMap == resultMap)
    if (!pass) {
      Console.err.println("ERROR: goldenMap != resultMap:")
      Console.err.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
      Console.err.println(s"$goldenMap")
      Console.err.println("----------------------------------------")
      Console.err.println(s"$resultMap")
      Console.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    }
    assert(pass, "goldenMap != resultMap, see Console.err for details")
  }

  // Local File Paths (must be same path on all cluster nodes):
  //
  // absolute path:
  //     "file:///Users/..."
  //     "file:/Users/..."
  //     "/Users/..."
  // relative path:
  //     "../test_data/empty.txt"
  // java.net.URISyntaxException: Relative path in absolute URI
  //     "file:../test_data/empty.txt"
  //
  val empty = "../test_data/empty.txt"
  val kublakhan = "../test_data/kublakhan.txt"

  def test_all_empty(): Unit = {

    test_wordCount(wordCount_v00, empty)
    test_wordCount(wordCount_v01, empty)
    test_wordCount(wordCount_v02, empty)
    test_wordCount(wordCount_v03, empty)
    test_wordCount(wordCount_v04, empty)
    test_wordCount(wordCount_v05, empty)
    test_wordCount(wordCount_v06, empty)
    test_wordCount(wordCount_v07, empty)
    test_wordCount(wordCount_v08, empty)
    test_wordCount(wordCount_v09, empty)
    test_wordCount(wordCount_v10, empty)
    test_wordCount(wordCount_v11, empty)
    test_wordCount(wordCount_v12, empty)
    test_wordCount(wordCount_v13, empty)
    test_wordCount(wordCount_v14, empty)
    test_wordCount(wordCount_v15, empty)
    test_wordCount(wordCount_v16, empty)
    test_wordCount(wordCount_v17, empty)
  }

  def test_all_kublakhan(): Unit = {

    test_wordCount(wordCount_v00, kublakhan)
    test_wordCount(wordCount_v01, kublakhan)
    test_wordCount(wordCount_v02, kublakhan)
    test_wordCount(wordCount_v03, kublakhan)
    test_wordCount(wordCount_v04, kublakhan)
    test_wordCount(wordCount_v05, kublakhan)
    test_wordCount(wordCount_v06, kublakhan)
    test_wordCount(wordCount_v07, kublakhan)
    test_wordCount(wordCount_v08, kublakhan)
    test_wordCount(wordCount_v09, kublakhan)
    test_wordCount(wordCount_v10, kublakhan)
    test_wordCount(wordCount_v11, kublakhan)
    test_wordCount(wordCount_v12, kublakhan)
    test_wordCount(wordCount_v13, kublakhan)
    test_wordCount(wordCount_v14, kublakhan)
    test_wordCount(wordCount_v15, kublakhan)
    test_wordCount(wordCount_v16, kublakhan)
    test_wordCount(wordCount_v17, kublakhan)
  }
}
