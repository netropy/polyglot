# polyglot/bigdata/spark

### Notes: Spark API (2.x)

___TOC:___ \
[SparkSessions, SparkContext](#sparksessions-sparkcontext) \
[SparkSession API](#sparksession-api) \
[Datasets, DataFrames](#datasets-dataframes) \
[Dataset, DataFrame API](#dataset-dataframe-api) \
[SparkContext API](#sparkcontext-api) \
[RDDs](#rdds) \
[RDD API](#rdd-api) \
[Closures](#closures) \
[Broadcast Variable and Accumulator API](#broadcast-variable-and-accumulator-api) \
[Parallelism, Partitions and Shuffle Operations](#parallelism-partitions-and-shuffle-operations)

#### SparkSessions, SparkContext

- [org.apache.spark.sql.SparkSession](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/sql/SparkSession.html):
  entry point to programming with the _Dataset_ and _DataFrame_ APIs
- [org.apache.spark.SparkContext](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/SparkContext.html):
  entry point to programming with _RDDs_ and low-level Spark API
- [org.apache.spark.api.java.JavaSparkContext](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/api/java/JavaSparkContext.html):
  a Java-friendly wrapper, works with Java collections
- _SparkContext_ represents a connection to a Spark cluster
  - is a JVM singleton
- _SparkSession_ isolates settings, environment, schema definitions
  - internally creates _SparkConfig_
  - internally creates a SparkContext (if none exists yet)
  - multiple SparkSessions can share the same SparkContext
- since Spark 2.x: SparkSession combines most APIs of former
  - _SparkContext_ (demoted from being the root object in Spark 1.x)
  - Spark 1.x context classes: _StreamingContext_, _SQLContext_ (deprecated),
    _HiveContext_ (removed)
- _spark-shell:_ offers predefined default instances
  - `spark` (SparkSession)
  - `sc` (SparkContext)
- _programmatically:_ create a session via _SparkSession.builder_, for example
  ```
    import org.apache.spark.sql.SparkSession
    val spark = SparkSession.builder
      .appName("SparkSessionExample")
      .master("local[4]")
      .config("spark.sql.warehouse.dir", "target/spark-warehouse")
      .enableHiveSupport()
      .getOrCreate()
  ```

#### SparkSession API

- manage:
  ```
  sparkContext: SparkContext       // the underlying cluster connection
  newSession(): SparkSession       // isolated sql+config, shared SparkContext
  close(): Unit, stop(): Unit      // to manually stop underlying SparkContext

  conf: RuntimeConfig              // get/set runtime configuration options
  ```
- create:
  ```
  emptyDataset: Dataset[T]         // empty typed collection
  createDataset(...): Dataset[T]   // from a List, Seq, or RDD
  range(...): Dataset[Long]        // single column `id`, per start, end, step

  emptyDataFrame: DataFrame        // empty Dataset[Row], no columns or rows
  createDataFrame(...): DataFrame  // from a List, Seq, or RDD + schema info
  implicits extends SQLImplicits   // to convert Scala objects into DataFrames
  ```
- read:
  ```
  read: DataFrameReader            // to read bounded data from a source
  readStream: DataStreamReader     // to read unbounded data from a source
  ```
- sql:
  ```
  sql(...): DataFrame              // execute a sql query
  table(...): DataFrame            // access the table or view
  udf: UDFRegistration             // register user-defined functions
  catalog: Catalog                 // access metastore, query/alter schema
  ```

#### Datasets, DataFrames

- a [Dataset\[T\]](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/sql/Dataset.html)
  is a strongly typed, immutable, partitioned, distributed, and fault-tolerant
  collection of elements
  - _T_ is often a domain-specific type in form of a _case class_ or a _bean_
- a _DataFrame_ is an untyped view of a Dataset as a relational table:
  - `type DataFrame = Dataset[Row]` (or just `Dataset<Row>` in Java)
  - over type
    [Row](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/sql/Row.html)
    and with columns identified by name and as
    [Column](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/sql/Column.html)
  - hence, DataFrames _are_ Datasets (all properties apply)
  - DataFrames are not typesafe, errors can materialize as RuntimeException
  - mutual conversion between DF and Ds: _df.as[T]_ and _ds.toDF_
- Datasets
  - are implemented on top of RDDs: _<ds>.rdd_
  - can represent static/bounded data as well as streaming/unbounded data
  - can be created from [non-]streaming sources: _sparkSession.read[Stream]_
  - can be saved to [non-]streaming external storage: _dataSet.write[Stream]_
- Datasets can be transformed in parallel with functional or relational
  operations
  - _Transformations_ are _lazy_ operations that produce new Datasets
  - _Actions_ trigger computation and return results to the driver program
- internally, a Dataset represents a logical plan describing a computation
  - optimized and translated into a physical plan upon invocation of an action
  - Encoder objects map the domain type T to Spark's internal type system
  - see Dataset methods _schema_, _explain_

#### Dataset, DataFrame API

- see [Dataset API](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/sql/Dataset.html)
- _Transformations_ producing new Datasets (or DataFrames), e.g.:
  ```
    - toDF: DataFrame, as[T]: Dataset[T], toJSON: Dataset[String]
    - map, flatMap, select (columns), drop, withColumn[Renamed]
    - filter, where, limit, sample, distinct, dropDuplicates
    - intersect[All], union[All], except[All], join[With], crossJoin
    - agg, groupBy[Key]: {Relational,KeyValue}GroupedDataset
    - sort, orderBy
    - coalesce, repartition[ByRange], mapPartitions, sortWithinPartitions
  ```
  plus predefined SQL
  [functions](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/sql/functions$.html)
  for DataFrame operations
- _Actions_ triggering computation, e.g.:
  ```
    - head, take[AsList], tail, collect, toLocalIterator
    - count, isEmpty, reduce
    - show, summary, foreach
  ```
- _Other Functions_, e.g.:
  ```
    - [un]persist, cache, checkpoint, localCheckpoint
    - isLocal, isStreaming, printSchema, schema, columns, explain, hint
  ```

#### SparkContext API

- create, return, stop the SparkContext, e.g.:
  ```
    - SparkContext.getOrCreate, SparkContext, stop
  ```
- create _RDDs_ from local collections in the driver (parallelize), or load
  _RDDs_ from external storage ([shared] file systems, data sources), e.g.:
  ```
    - emptyRDD, makeRDD, parallelize, range, union
    - textFile, wholeTextFiles
    - binary{Files,Records}, [newAPI]hadoop{File,RDD}, sequenceFile, objectFile
    - getPersistentRDDs, getRDDStorageInfo
  ```
- create _Broadcast Variables_, create or register _Accumulators_, e.g.:
  ```
    - broadcast
    - {collection,double,long}Accumulator, register
  ```
- manage logging, applications, jobs, cluster, e.g.:
  ```
    - setLogLevel
    - addFile, addJar, files, jars, listFiles, listJars
    - applicationId, appName, master, deployMode, isLocal, isStopped
    - getLocalProperty, setLocalProperty, clearCallSite, setCallSite
    - cancel{[All]Jobs,JobGroup,Stage}, killTaskAttempt
    - run[Approximate]Job, submitJob
    - setJobDescription, clearJobGroup, setJobGroup
    - getCheckpointDir, setCheckpointDir
    - getConf, defaultMinPartitions, defaultParallelism
    - request[Total]Executors, killExecutor[s], getExecutorMemoryStatus
    - sparkUser, startTime, version, uiWebUrl
  ```

#### RDDs

- An
  [RDD\[T\] (Resilient Distributed Dataset)](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/rdd/RDD.html)
  - is a distributed (or local-parallel) collection of objects
  - is immutable, built by lazy, parallel transformations
  - is fault-tolerant, automatically rebuilt on failure
  - can be persisted to memory (cached) and external storage
  - is a logical view of partitioned data
  - partitions spread across a cluster, are processed independently
- RDDs form the basic abstraction in Spark; they have become a low-level API
  with the introduction of the _Dataset_ API in Spark 1.6 (based on RDDs)
- internally, each RDD is characterized by five main properties:
  - a list of partitions
  - a function for computing each split
  - a list of dependencies on other RDDs
  - optionally, a Partitioner for (hash-partitioned) key-value RDDs
  - optionally, a list of preferred (block) locations to compute each split on
- extra functionality is available through extensions or subclasses, e.g.:
  - for RDDs of (key, value) pairs: [PairRDDFunctions](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/rdd/PairRDDFunctions.html)
  - for RDDs of Doubles: [DoubleRDDFunctions](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/rdd/DoubleRDDFunctions.html)
  - for RDDs that can be saved as SequenceFiles: [SequenceFileRDDFunctions](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/rdd/SequenceFileRDDFunctions.html)
  - for RRDs running SQL querys on a JDBC connection: [JdbcRDD](https://spark.apache.org/docs/2.4.7/api/scala/org/apache/spark/rdd/JdbcRDD.html)
  - for custom RDDs, abstract class `RDD` can be subclassed
- like Datasets, operations on RDDs are grouped into parallel operations:
  - _Transformations_ are _lazy_ operations that produce new RDDs
  - _Actions_ trigger computation and return results to the driver program
- _RDD Persistence_:
  - by default, a transformed RDD is recomputed each time an Action is run
    on it
  - can persist an RDD in memory across operations (_persist_ or _cache_)
  - cache is fault-tolerant: lost partitions will automatically be recomputed
  - can also persist RDDs on disk or replicated them across multiple nodes
  - see available [StorageLevels](https://spark.apache.org/docs/2.4.7/rdd-programming-guide.html#rdd-persistence)

#### RDD API

- RDD
  [Transformations](https://spark.apache.org/docs/2.4.7/rdd-programming-guide.html#transformations)
  producing new RDDs, e.g.:
  ```
    - map, map[Partitions[WithIndex]], flatMap, filter
	- sample, distinct, randomSplit
    - union (++), intersection, subtract
	- cartesian, zip[{Partitions,WithIndex,WithUniqueId}]
    - pipe
	- groupBy, keyBy
    - sortBy, coalesce, repartition[AndSortWithinPartitions], glom
  ```
- additional transformations for PairRDDs, e.g.:
  ```
	- keys, values
    - mapValues, flatMap, sampleByKey[Exact]
    - subtractByKey
    - {aggregate,fold,combine,group,reduce,sort}ByKey
    - join, {left,right,full}OuterJoin
    - cogroup, groupWith
  ```
- RDD and PairRDD [Actions](https://spark.apache.org/docs/2.4.7/rdd-programming-guide.html#actions)
  triggering computation, e.g.:
  ```
    - aggregate, fold, reduce[ByKeyLocally], collect[AsMap], toLocalIterator
	- isEmpty, count[[ByKey]Approx], count[ByValue[Approx]]
    - first, take[{Sample,Ordered}], top, max, min, lookup
    - foreach
    - saveAs{Text,Sequence,[NewAPI]Hadoop,Object}File
  ```
- Spark vs Scala/FP:
  - aggregate ~ fold
  - aggregate (of each partition, then across )
- _Other Functions_, e.g.:
  ```
    - partitions, getNumPartitions
    - cache, [un]persist, cache, getStorageLevel
    - checkpoint, localCheckpoint, isCheckpointed, getCheckpointFile
	- name, id, context, sparkContext, toJavaRDD, dependencies
  ```

#### Closures

- _Pitfall_: closures with side-effects result in undefined behavior
- lambdas or functions passed to Spark operations are run as local closures:
  - => all captured values and objects must be serializable
  - => in cluster mode, closures are _deserialized_, hence, _copies_
  - => executors require the code of any (indirectly) called method as well
  - => executors have their own stdout, so any I/O is not shown in the driver
  - => testing in local mode, may not flag broken serialization, side-effects
  - ==> preferred: pure, self-contained functions as lambdas or static methods
  - ==> for driver I/O: use _collect_ (with _head_, _take_, _sample_ etc)

#### Broadcast Variable and Accumulator API

- _Shared Variables_:
  - no support for general, read-write shared variables
  - support for read-only shared variables: _Broadcast Variables_
  - support for update-only shared variables: _Accumulators_
- [Broadcast Variables](https://spark.apache.org/docs/2.4.7/rdd-programming-guide.html#broadcast-variables):
  - are read-only variable cached on each node rather than shipped with tasks
  - are automatically broadcasted by Spark's distributed "shuffle" between
    stages, as needed by the tasks within each stage
  - are cached in serialized form, deserialized before running each task
  - hence, only useful to explicitly create broadcast variables when
    - tasks across multiple stages need the same data or
    - caching the data in deserialized form is important
  - create, release, or permanently delete a broadcast variable:
    ```
      SparkContext.broadcast, Broadcast.unpersist, Broadcast.destroy
	```
- [Accumulators](https://spark.apache.org/docs/2.4.7/rdd-programming-guide.html#accumulators):
  - are variables that tasks can update through associative and commutative
    operations with identity element (_.add_, _.merge_, _.reset_)
  - only the driver can read them (_.value_)
  - support for
    - numeric types double and long (ops: reset to zero, sum, count, average)
    - lists (ops: reset to empty, add element, merge another list)
    - user-defined types (implement: reset, isZero, add, merge, copy, value)
    - can accumulate inputs of type IN and produce output of type OUT
  - accumulator updates are performed when called from
    - actions: only applied once, restarted tasks will not update the value
    - transformations: may be applied more than once if tasks or job stages
      are re-executed
  - the Web UI displays named accumulator values for stages/tasks
  - create accumulators or register with a name:
    ```
      SparkContext.{collection,double,long}Accumulator, SparkContext.register
	```

#### Parallelism, Partitions and Shuffle Operations

- RDDs are a logical view to data held in _Partitions_:
  - partitions are the main unit of parallelism: 1 task for each partition
  - _#partitions = \<rdd\>.getNumPartitions = \<rdd\>.partitions.size_
  - important tuning parameter, good: #partitions = 2..4 * #CPUs in cluster
  - repartitioning can be expensive (serialize, move, deserialize data)
- certain operations trigger the Spark runtime to perform a
  [Shuffle](https://spark.apache.org/docs/2.4.7/rdd-programming-guide.html#shuffle-operations)
  to re-distribute, co-locate, and re-group data
- example: _reduceByKey_
  - for each key, all its values are to be combined by an associative and
    commutative reduce function
  - the values for a key may reside on other partitions or remote machines
  - therefore, the Spark runtime needs to perform an _all-to-all_ operation:
    read from all partitions to find all the values for all keys
  - then co-locate values across partitions to compute the result for each key
- non-deterministic effects:
  - shuffle does not determine the ordering of elements within partitions
  - for predictably ordered data following shuffle:
    - _mapPartitions_ (for example: _...(_.toBuffer.sorted.iterator)_)
    - _repartitionAndSortWithinPartitions_ (for _PairRDDs_)
    - _sortBy_ (make a globally ordered RDD)
- Shuffle is a complex and expensive operation:
  - involves heap memory, disk I/O, sorting, data serialization, network I/O
  - spawns map- and reduce-like tasks to organize and to aggregate the data
  - due to retention, long-running jobs may consume lots of disk space
  - see _spark.local.dir_ and tuning options in the
    [Spark Configuration Guide](https://spark.apache.org/docs/2.4.7/configuration.html#shuffle-behavior)
- operations which can cause a shuffle:
  - _repartition_ (increase number of partitions, full shuffle)
  - _coalesce_ (decrease number of partitions, minimizes shuffles)
  - _*ByKey_ operations (except for counting)
  - _*join_ and _cogroup_ operations
- details:
  [Parallelized Collections](https://spark.apache.org/docs/2.4.7/rdd-programming-guide.html#parallelized-collections),
  [Apache Spark Partitioning](https://medium.com/@adrianchang/apache-spark-partitioning-e9faab369d14), and
  [Spark Partitions](https://luminousmen.com/post/spark-partitions),
  which has this useful diagram:
  ![Shuffle Partitioning](spark-partitions-3.luminousmen.com.jpg
  "Shuffle Partitioning by luminousmen.com")

[Prev](./Spark_Notes_0_Overview.md),
[Up](../README.md),
[Next](./Spark_Notes_2_API_Tips.md)
