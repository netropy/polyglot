# polyglot/bigdata/spark

### 1 Spark API Usage Exercises

#### 1.1 Spark API usage example: RDD, Dataset, DataFrame with a data class.

Show Spark's Scala and Java API usage for a data class (bean, case class):
- create local collections [data class/Row]
- create RDDs [data class/Row] from local collections
- load RDDs [data class/Row] from text (or binary) files
- create Datasets [data class/Row] from local collection, RDD
- create DataFrames from local collection, RDD, Dataset
- load DataFrames from csv, json[l], parquet, orc, avro
- convert DataFrame -> Dataset [data class]

Verify all collections and conversions with a unit test.

[Scala source](src/main/scala/netropy/polyglot/bigdata/spark/ex_1_1_CaseClassUse.scala),
[Java source](src/main/scala/netropy/polyglot/bigdata/spark/Ex_1_1_BeanClassUse.java),
[Scala test](src/test/scala/netropy/polyglot/bigdata/spark/ex_1_1_CaseClassUse.scala),
[Java test](src/main/java/netropy/polyglot/bigdata/spark/Ex_1_1_BeanClassUse_Tests.java)

#### 1.2 Explore the Spark API: variants of 'word counts'.

Try to come up with as many different implementations as supported by the
Spark API for of a function that
- splits input text into words
- returns a map: word -> count

Discuss the code variants and the used Spark APIs.  Summarize into lessons.

Verify all implementations with a unit test.

[Remarks](ex_1_2.r.wordcount.md),
[Scala source](src/main/scala/netropy/polyglot/bigdata/spark/ex_1_2_WordCountVariants.scala)
[Scala test](src/test/scala/netropy/polyglot/bigdata/spark/ex_1_2_WordCountVariants.scala)

#### 1.3 Spark 2.x Mini Exercises, Code Gists

Assume this list of random integers was large: _List(5, 99, 3, 348, 99, 10)_.

Compute the following operations and print their result:
1. the size of the collection
2. the (approximate) sum of all numbers
3. all even numbers
4. every 2nd number
5. the first two numbers
6. the last two numbers
7. the two smallest numbers
8. the two largest numbers
9. the largest even number squared

Run in the Spark shell (or a Notebook).  Compare code for Spark _RDD_ and
_Dataset_ APIs.  Discuss code variants and mark on operations that are
computed in the driver.

[Remarks with code gists](ex_1_3.r.mini_code_exercises.md)

[Up](../README.md)
