# polyglot/bigdata/spark

### Code Gists, Tips: Core & SQL API

___TOC:___ \
[Resources](#resources) \
[Summary: API Lessons](#summary-api-lessons) \
[Create sample data \[Int\]](#create-sample-data-int) \
[Show first N](#show-first-n) \
[Take first N as local collection \[(String, Int)\]](#take-first-n-as-local-collection-string-int) \
[Collect as local collection \[(String, Int)\]](#collect-as-local-collection-string-int) \
[Collect as local iterator \[(String, Int)\]](#collect-as-local-iterator-string-int) \
[Take largest/smallest N elements \[(String, Int)\]](#take-largestsmallest-n-elements-string-int) \
[Find min/avg/max value \[(String, Int)\]](#find-minavgmax-value-string-int)

___Notation:___

_Ds_ := _Dataset[T]_ \
_DF_ := _DataFrame_ = _Dataset[Row]_ \
_SC_ := _SparkContext_

#### Resources

- [Spark SQL Guide](https://spark.apache.org/docs/latest/sql-data-sources.html)
- [Spark By Examples](https://sparkbyexamples.com/)
- [org.apache.spark.sql.SparkSession](https://spark.apache.org/docs/latest/api/scala/org/apache/spark/sql/SparkSession.html):
- [org.apache.spark.sql.Dataset](https://spark.apache.org/docs/latest/api/scala/org/apache/spark/sql/Dataset.html)
- [org.apache.spark.sql.functions](https://spark.apache.org/docs/latest/api/scala/org/apache/spark/sql/functions$.html)
- [org.apache.spark.rdd.RDD](https://spark.apache.org/docs/latest/api/scala/org/apache/spark/rdd/RDD.html)

#### Summary: API Tips

- stay on _Dataset_ vs untyped _DataFrame_ for as long as possible: safer to
  transform
- stay on _non-local_ _RDD_ operations (vs driver ops) for as long as
  possible: collection size and use of parallelism
- beware of driver bottleneck, avoid building large result sets:
  _RDD: collect(), reduce()_; _SC: collectionAccumulator(), register(acc)_
- prefer associative (and commutative) aggregation over explicit
  _Ds.groupByKey_ or _Ds.groupBy_: grouping can be expensive due to shuffle
- prefer _RDD.reduceByKey_ (with commutative & associative op) over
  _RDD.foldByKey_ (with associative op): simpler and possibly faster
- _Ds_ lacks _RDD.zipWithIndex_ equivalent, ill-suited for positional-indexed
  operations
- name cached _RDDs_ and _Accumulators_ for better Spark UI diagnostics:
  _SC.register(acc, name)_, _RDD.setName(name)_
- consider _RDD.persist(StorageLevel.MEMORY\_AND\_DISK)_ over
  _.cache() (== MEMORY\_ONLY)_ to avoid recomputation when dropped a partition
- prefer _narrow_ over _wide joins_: _RDDs_ sharing a _Partitioner_

See: \
[1 Spark API Usage Exercises](ex_1.q.spark.md), \
[1.2 Explore the Spark API: variants of 'wordCount'](ex_1_2.r.wordcount.md), \
[1.3 Spark 2.x Mini Exercises, Code Gists](ex_1_3.r.mini_code_exercises.md)

[Imran Rashid: 5 Apache Spark Tips in 5 Minutes (2015)](https://www.slideshare.net/cloudera/5-apache-spark-tips-in-5-minutes),

#### Create sample data _[Int]_

___Notation:___

_Ds_ := _Dataset[T]_ \
_DF_ := _DataFrame_ = _Dataset[Row]_

Ds, DF, RDD: quickest per _SparkSession.range_ with conversion
```
scala> val ds = spark.range(10)
ds: org.apache.spark.sql.Dataset[Long] = [id: bigint]

scala> val df = ds.toDF
df: org.apache.spark.sql.DataFrame = [id: bigint]

scala> val rdd = ds.rdd
rdd: org.apache.spark.rdd.RDD[Long] = MapPartitionsRDD[10] at rdd at <console>:25

scala> rdd.toDF
res: org.apache.spark.sql.DataFrame = [value: bigint]

scala> rdd.toDS
res: org.apache.spark.sql.Dataset[Long] = [value: bigint]
```

#### Create sample data from local collection _[(String, Int)]_

Ds, DF, RDD: create directly or via conversion from another
```
scala> val seq = Seq("aa" -> 1, "bb" -> 2)
seq: Seq[(String, Int)] = List((aa,1), (bb,2))

scala> val rdd = spark.sparkContext.parallelize(seq)
rdd: org.apache.spark.rdd.RDD[(String, Int)] = ParallelCollectionRDD[27] at parallelize at <console>:25

scala> val ds = spark.createDataset(seq)
ds: org.apache.spark.sql.Dataset[(String, Int)] = [_1: string, _2: int]

scala> val df = spark.createDataFrame(seq)
df: org.apache.spark.sql.DataFrame = [_1: string, _2: int]

scala> val df1 = df.toDF("name", "num")
df1: org.apache.spark.sql.DataFrame = [name: string, num: int]
```

#### Show first _N_

Ds, DF: _show(numRows)_
```
scala> spark.range(100).show
+---+
| id|
+---+
|  0|
...
| 19|
+---+
only showing top 20 rows

scala> spark.range(100).show(50)
+---+
| id|
+---+
|  0|
...
| 49|
+---+
only showing top 50 rows
```

Ds, DF: _show(numRows, truncate, vertical)_
```
scala> df1.show
+----+---+
|name|num|
+----+---+
|  aa|  1|
|  bb|  2|
+----+---+

scala> df1.show(5, 80, true)
-RECORD 0---
 name | aa
 num  | 1
-RECORD 1---
 name | bb
 num  | 2
```

RDD: _println_ will NOT work in cluster mode, use _show_
```
scala> rdd.foreach(println)
(bb,2)
(aa,1)

scala> rdd.toDS.show
+---+---+
| _1| _2|
+---+---+
| aa|  1|
| bb|  2|
+---+---+
```

#### Take first _N_ as local collection _[(String, Int)]_

Ds, DF: as _Array_ or Java _List_
```
scala> ds.take
take   takeAsList

scala> ds.take(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> df.take(5)
res: Array[org.apache.spark.sql.Row] = Array([aa,1], [bb,2])

scala> ds.takeAsList(5)
res: java.util.List[(String, Int)] = [(aa,1), (bb,2)]

scala> df.takeAsList(5)
res: java.util.List[org.apache.spark.sql.Row] = [[aa,1], [bb,2]]
```

Ds, DF: _first == head == take_
```
scala> ds.first
res: (String, Int) = (aa,1)

scala> ds.head
res: (String, Int) = (aa,1)

scala> ds.head(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))
```

RDD: as Array
```
scala> rdd.take(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> rdd.takeOrdered(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))
```

#### Collect as local collection _[(String, Int)]_

Ds, DF: as _Array_ or Java _List_
```
scala> ds.collect
collect   collectAsList

scala> ds.collect
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> ds.collectAsList
res: java.util.List[(String, Int)] = [(aa,1), (bb,2)]

scala> df.collect
res: Array[org.apache.spark.sql.Row] = Array([aa,1], [bb,2])

scala> df.collectAsList
res: java.util.List[org.apache.spark.sql.Row] = [[aa,1], [bb,2]]
```

RDD: as _Array_ or Scala _Map_
```
scala> rdd.collect
collect   collectAsMap   collectAsync

scala> rdd.collectAsMap
res: scala.collection.Map[String,Int] = Map(bb -> 2, aa -> 1)

scala> rdd.collect
res: Array[(String, Int)] = Array((aa,1), (bb,2))
```

#### Collect as local iterator _[(String, Int)]_

Ds, DF: as Java Iterator
```
scala> ds.toLocalIterator
res: java.util.Iterator[(String, Int)] = IteratorWrapper(<iterator>)

scala> df.toLocalIterator
res: java.util.Iterator[org.apache.spark.sql.Row] = IteratorWrapper(<iterator>)
```

RDD: as Scala Iterator
```
scala> rdd.toLocalIterator
res: Iterator[(String, Int)] = <iterator>

scala> rdd.toLocalIterator.to
to         toIndexedSeq   toList   toSet      toTraversable
toArray    toIterable     toMap    toStream   toVector
toBuffer   toIterator     toSeq    toString
```

#### Take largest/smallest _N_ elements _[(String, Int)]_

Ds, DF: _sort == orderBy_
```
scala> ds.sort("_1", "_2").take(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> ds.orderBy("_1", "_2").take(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> ds.orderBy($"_1".asc, $"_2".asc).take(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> ds.orderBy($"_1".desc, $"_2".desc).take(5)
res: Array[(String, Int)] = Array((bb,2), (aa,1))

scala> // import org.apache.spark.sql.functions._

scala> ds.sort(asc("_1"), asc("_2")).take(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> ds.sort(desc("_1"), desc("_2")).take(5)
res: Array[(String, Int)] = Array((bb,2), (aa,1))
```

RDD: _max_, _min_, _top_, _takeOrdered_, _sortBy_
```
scala> rdd.max
res: (String, Int) = (bb,2)

scala> rdd.min
res: (String, Int) = (aa,1)

scala> rdd.top(5)
res: Array[(String, Int)] = Array((bb,2), (aa,1))

scala> rdd.takeOrdered(5)(math.Ordering[(String, Int)])
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> rdd.takeOrdered(5)(math.Ordering[(String, Int)].reverse)
res: Array[(String, Int)] = Array((bb,2), (aa,1))

scala> rdd.sortBy(_._1).take(5)
res: Array[(String, Int)] = Array((aa,1), (bb,2))

scala> rdd.sortBy(_._1, false).take(5)
res: Array[(String, Int)] = Array((bb,2), (aa,1))
```

#### Find min/avg/max value _[(String, Int)]_

Ds, DF: _agg_ (yields DF), _min_, _max_, _avg_
```
scala> ds.agg(min("_1"), avg("_1"), max("_1")).show
+-------+-------+-------+
|min(_1)|avg(_1)|max(_1)|
+-------+-------+-------+
|     aa|   null|     bb|
+-------+-------+-------+

scala> ds.agg(min("_2"), avg("_2"), max("_2")).show
+-------+-------+-------+
|min(_2)|avg(_2)|max(_2)|
+-------+-------+-------+
|      1|    1.5|      2|
+-------+-------+-------+
```

RDD: _map_, _min_, _max_, _reduce_
```
scala> (rdd.map(_._2).min, rdd.map(_._2).max)
res: (Int, Int) = (1,2)

scala> rdd.map(si => (si._2, si._2)).reduce((i, j) => (i._1 min j._1, i._2 max j._2))
res: (Int, Int) = (1,2)
```

[Prev](./Spark_Notes_1_Core_and_SQL_API.md),
[Up](../README.md),
[Next](./Spark_Notes_3_SQL_Tips.md)
