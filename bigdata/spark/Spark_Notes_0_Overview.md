# polyglot/bigdata/spark

### Notes: Spark Overview (2.x)

___TOC:___ \
[References](#resources) \
[Features](#features) \
[Quick Start: Apache Spark](#quick-start-apache-spark) \
[spark-submit, spark-shell, pyspark](#spark-submit-spark-shell-pyspark) \
[Spark Web UI](#spark-web-ui) \
[Concepts & Terminology](#concepts-terminology)

#### References

- [Apache Spark](https://spark.apache.org/docs/latest/)
- [Docs](https://spark.apache.org/docs/latest/),
  [Scala/Java/Python/R/SQL API Docs](https://spark.apache.org/docs/latest/api/)
- [Apache Spark Examples](https://spark.apache.org/examples.html),
  [Spark By Examples](https://sparkbyexamples.com/)
- [Databricks](https://docs.databricks.com/),
  [eBooks, Primers etc](https://databricks.com/resources/)
- [Spark: The Definitive Guide](https://www.oreilly.com/library/view/spark-the-definitive/9781491912201/),
  [Learning Spark, 2nd Edition](https://www.oreilly.com/library/view/learning-spark-2nd/9781492050032/)
- githubs:
  [Apache Spark](https://github.com/apache/spark),
  [Jacek Laskowski: Spark, Delta Lake, Kafka](https://github.com/jaceklaskowski)

#### Features

Apache Spark is an open source analytical processing engine for large-scale
distributed data processing and machine learning applications.

- general-purpose, in-memory, fault-tolerant, distributed, cached, persisted
- support for cluster managers: Spark, Yarn, Mesos, Kubernetes
- support for data sources: text/binary, csv, json, xml, parquet, avro et al
- support for storage: HDFS, DBFS, AWS S3, Azure, NoSQL, RDMSs, JDBC et al
- functional: immutable, lazy, parallel, familiar combinators
- relational: ANSI SQL, programmatic queries and schemas
- #lines of code (github/spark, 2020-12): 970k Scala, 125k Java, 105k Python

Spark modules (built-in libraries), external packages:
- [Core](https://spark.apache.org/downloads.html)
- [SQL](https://spark.apache.org/sql/)
- [Streaming](https://spark.apache.org/streaming/)
- [MLlib](https://spark.apache.org/mllib/)
- [GraphX](https://spark.apache.org/graphx/) (Pregel API)
- [thrid-party](https://spark.apache.org/third-party-projects.html)

[Spark History](https://spark.apache.org/news):
- 2009 created by Matei Zaharia at UC Berkeley AMPLab (~4 years after Hadoop)
- 2013 Apache top-level project, creators founded Databricks Inc
- 2014: Spark 1.0, 2016: Spark 2.0, 2020: Spark 3.0

#### Quick Start: Apache Spark

- download and untar [Apache bundle](https://spark.apache.org/downloads.html):
  _{spark version}{+/- hadoop}{scala version}_
- alternatively: OS's package manager, other distributions, 3rd party tools
- might have to edit `$SPARK_HOME/conf/spark-env.sh` for Hadoop, JAVA_HOME
- quick test: run `$SPARK_HOME/bin/run-example SparkPi 10`

_Seems broken_: build local HTML API doc (there's no Apache Spark doc bundle):
- _$ git clone https://github.com/apache/spark.git; cd spark_
- _$ ./build/sbt unidoc_
- _$ mv ./target/scala-2.12/unidoc ..._

#### spark-submit, spark-shell, pyspark

- launch a Scala, Java, Python application (usage: `-h, --help`):
  ```
    $ spark-submit [--class <main>] [--jars <extras>] <app.jar/app.py> [<args>]

    $ spark-submit --class org.apache.spark.examples.SparkPi "$SPARK_HOME/examples/jars/spark-examples_2.12-2.4.7.jar" 10
    $ spark-submit --class org.apache.spark.examples.JavaSparkPi "$SPARK_HOME/examples/jars/spark-examples_2.12-2.4.7.jar" 10
    $ spark-submit "$SPARK_HOME/examples/src/main/python/pi.py" 10
  ```
- option: where to find a master (Cluster Manager)
  ```
    [--master <url>]                     # default: local[*]
       local,local[N],local[*]           # run locally with 1/N/#cpu threads
       spark://HOST[:PORT]               # run on a Spark standalone cluster
       {yarn,mesos,k8s}[://HOST[:PORT]]  # run on Yarn/Mesos/Kubernetes cluster
  ```
- option: where to run the driver
  ```
    [--deploy-mode <mode>]               # default: client
       cluster                           # on a worker node
       client                            # within the spark-submit proces
  ```
- run Spark interactively:
  ```
    $ spark-shell [<similar options as above>]
      scala> :help
      scala> :help <cmd>
      scala> spark                       # default SparkSession
      scala> spark.<tab>                 # show features
      scala> Ctrl-D                      # :quit

    $ pyspark [<similar options as above>]
      >>> help()
      >>> help(<cmd>)
      >>> spark                          # default SparkSession
      >>> dir(spark)                     # show features
      >>> Ctrl-D                         # quit()
  ```
- details:
  [Submitting Applications](https://spark.apache.org/docs/latest/submitting-applications.html)

#### Spark Web UI

- Spark/PySpark Shell Application UI: http://localhost:4040, _[4041, ...]_
- Resource Manager: http://localhost:9870
- Spark JobTracker: http://localhost:8088
- node-specific info: http://localhost:8042
- note: to access these URLs, the Spark application should in running state
  (otherwise, start a Spark History server to access UI at any the time)

#### Concepts & Terminology

Spark Programming Model:
- collections:
  - distributed, partitioned, immutable, fault-tolerant, persistable
  - _RDDs_ (Resilient Distributed Datasets, basic abstraction, functional)
  - _Datasets_ (strongly typed, relational)
  - _DataFrames_ (untyped, i.e., _Row_, relational)
- computations:
  - declarative functional or relational
  - executed as highly optimized + parallelized data flow DAGs

Client:
- _Application:_ a running driver program and the executors on the cluster
- _Application jar:_ the (self-contained) application code, often as "uber
  jar" with its dependencies (should not include Hadoop or Spark libraries)
- _Driver Program:_ the "master" program or _main()_ function that creates the
  SparkContext (and SparkSessions) and initiates computations
  - _SparkContext:_ an API object representing a connection to a Spark cluster
    and serving as entry point to programming with Spark low-level APIs
  - _SparkSession:_ an API object sharing a SparkContext and serving as the
    entry point to programming with Spark SQL APIs

Cluster:
- _Cluster Manager:_ an external service for acquiring and managing resources
  on the cluster
  - see above `[--master <url>]`
  - _Standalone:_ a simple, easy-to-use cluster manager included with Spark
  - _Apache Mesos:_ can run Hadoop MapReduce and Spark applications
  - _Hadoop YARN:_ a widely used resource manager from Hadoop 2
  - _Kubernetes:_ an open-source system for automating deployment, scaling, and
    management of containerized applications
  - _local:_ not really a cluster manager, runs Spark on a laptop/computer
- _Deploy Mode:_
  - see above `[--deploy-mode <mode>]`
  - where the driver process is run, inside (_cluster_) vs outside (_client_)
    of the cluster
- _Worker Node:_ any node that can run application code in the cluster
- _Executor:_ a process launched for a single application on a worker node,
  runs tasks and keeps data in memory or disk storage
- _Task:_ a unit of work that will be sent to one executor
- _Stage:_ a set of tasks that depend on each other
- _Job:_ a parallel computation divided into stages of multiple tasks, which
  is spawned in response to a  Spark action (_save_, _collect_)

Details:
[Spark Standalone Mode](https://spark.apache.org/docs/latest/spark-standalone.html),
[Spark Cluster Mode Overview](https://spark.apache.org/docs/latest/cluster-overview.html)
with diagram: \
![Spark Cluster Mode Overview](spark-cluster-overview.spark.apache.org.png
"Spark Cluster Mode Overview by spark.apache.org")

[Up](../README.md), [Next](./Spark_Notes_1_Core_and_SQL_API.md)
