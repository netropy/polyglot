TAGS: Problem: \
big data, analytical processing, large-scale, distributed, parallel,
in-memory, fault-tolerant, machine learning, word count, grouped data,
aggregation, map, reduce, fold, reduce, count, sort, shuffle, functional,
immutable, lazy, relational, SQL, data sources, data class, csv, json, yaml,
xml, avro, orc, parquet, cluster manager, spark, standalone, yarn, mesos,
kubernetes

TAGS: Scala: \
scala2.12, scala2.13, lambda, lazy, iterator, view, stream,
scala.collection.immutable, scala.collection.mutable, scala.io.Source,
HashMap, SortedMap, groupBy, groupMapReduce, foldLeft, case class

TAGS: Java: \
java11, lambda, bean class, java.util, java.util.concurrent, java.io,
java.nio, java.util.function, java.util.stream, Stream, Collectors, Function,
ConcurrentHashMap, ConcurrentSkipListMap, Map, HashMap, SortedMap, TreeMap,
BufferedReader

TAGS: JSON \
one-record-per-string json, jsonl, newline-delimited json lines, json parsing,
json libraries, jq, circe, io.circe, circe.syntax, circe.Json, circe.parser,
Play JSON, Json4s, Jawn, ninny-json, uJson, µPickle, scala.util.parsing.json,
scala-parser-combinators, JSON-java, org.json, json.JSONObject,
json.JSONTokener, json.JSONException, Gson, Jackson, JSON-P, JSON.simple

TAGS: Spark: \
modules, core, sql, streaming, mllib, graphx, spark-submit, spark-shell,
pyspark, master, cluster manager, application, driver program, deploy-mode,
application ui, web ui, resource manager, job tracker, worker node, executor,
task, stage, job, APIs, SparkContext, JavaSparkContext, SparkConf, RDD,
JavaRDD, resilient distributed dataset, partitioned data, SQLContext,
HiveContext, SparkSession, Dataset, DataFrame, Encoder, DataFrameReader, sql,
table, udf, catalog, PairRDDFunctions, SequenceFileRDDFunctions, JdbcRDD,
transformations, lazy operations, actions, persistence, cache, checkpoint,
closures, lambdas, broadcast variable, accumulators, partitions, shuffle

TAGS: Hadoop: \
apache hadoop ecosystem, hadoop2, hadoop1, cluster manager, yarn, hive, hdfs,
hdfs2, hadoop distributed file system, hadoop cluster, map/reduce, M/R, map,
mapper, reduce, reducer, sort, shuffle, combiner, partitioner, jobtracker,
tasktracker, job, task, driver, input split, sequence file input format, text
input format, name node, primary secondary namenode, data node, master node,
job tracker, task tracker, storage node, compute node

TAGS: Flink: \
features, components, sql, table api, datastream api, dataset api, datastream
processfunction api
