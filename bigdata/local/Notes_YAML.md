# polyglot/bigdata/local

Also see style summary: [YAML Best Practices](../../guides/YAML.md)

### Notes: YAML

___TOC:___ \
YAML Basics \
Linter: yamllint (Python[3]) \
Lib: eo-yaml (Java)

YAML is a
- human-friendly, flow+block-style, Unicode-based data serialization language
- used for configuration files (less for messaging, object persistence etc)
- cross language: support for common, native data types
- contains [JSON](https://www.json.org)
  as a subset (reported to be much slower to [de-]serialize than JSON)
- overlaps with [XML](https://www.w3.org/XML),
  subsets of YAML and XML can be
  [mapped to another](https://yaml.org/xml)
- recursive acronym: "YAML Ain't Markup Language" (initially "Yet Another
  Markup Language")

Latest version:
[YAML 1.2](https://yaml.org/spec/1.2/spec.html)
(2009) has
[211 grammar rules](https://github.com/yaml/yaml-grammar):
_"Fully comprehending the YAML grammar is quite an undertaking for most
mortals. Creating a fully compliant parser has proven almost impossible."_

___References:___

https://yaml.org - official website \
https://github.com/yaml - lang spec, implementations, test suite, tools etc

#### YAML Basics

##### Elements:
- line comment `#` (hash, pound)
- sequences (list, array, vector, tuple), like `- foobar` (dash+space)
- mappings (dictionary, object) `key: value` (colon+space, value indicator)
- scalars: string+number literals, unquoted strings like `True`, `False` etc
- structures (indentation, separation, complex key mappings, repeated nodes)
- tags starting with `!` (meta information) and `!!` (type annotation)
- directives (instructions to the YAML processor)

##### Flow-style (inline, folded): like JSON with extensions
- flow sequence: `[`_\<comma-separated-entries\>_`]`
- flow mappings: `{`_\<comma-separated-entries\>_`}`
- flow scalar: plain/unquoted, single-quoted, double-quoted scalar
  - plain/unquoted: `foo`
    - leading and trailing white space is stripped
    - must not contain `: ` (colon+space), ` #` (space+hash) \
    - limitations on starting with `[] {} > | * & ! % \ @ , ?` etc
  - single-quoted: `'foo'`
    - no need to escape `\`, `"`
  - double-quoted: `"foo"`
    - must escape `\\`, `\"`
    - can have punctiation, white space, line breaks are folded
    - can have generic + unicode escapes `\t`, `\x..`, `\u....`, `\U........`

##### Block-style: one entry per line at same indentation
- block sequence entry: `- ` (dash+space, nested series entry indicator)
- block mapping: `: ` (colon+space, value indicator)
- block scalar:
  - literal style `|` (pipe) or folded style `>` (angle bracket)
  - optional strip/keep chomp modifiers `-` or `+`

##### Scalars:
- string literals (with general+unicode escapes in double-quoted scalars)
- number literals (floating points, decimal/hexidecimal/octal integers)
- unquoted `null` or `~` (tilde)
- booleans `True`, `On`, `Yes` and `False`, `Off`, `No`

##### Structures:
- document start (separator): `---`
- document end (terminator): `...`
- complex key mapping (sequence-sequence): `? ` (QM+space, key indicator)
- repeated nodes: defined by anchor property `&` (ampersand), used by alias
  indicator `*` (asterisk)

___Notes:___
- YAML is case sensitive
- _not allowed: tabs for indentation_
- can have any of: _sequence|mappings of sequence|mappings|scalars_
- a type tag `!!foo` expands to URL "tag:yaml.org,2002:foo", see
  [language-independent types for YAML](https://yaml.org/type/)

#### Linter: yamllint (Python[3]) - support for YAML 1.2

[yamllint](https://github.com/adrienverge/yamllint),
[yamllint documentation](https://yamllint.readthedocs.io/en/stable),
[yamllint online validator](https://www.yamllint.com)

```
  $ yamllint -h
  usage: yamllint [-h] [-] [-c CONFIG_FILE | -d CONFIG_DATA]
                  [-f {parsable,standard,colored,github,auto}] [-s]
                  [--no-warnings] [-v]
                  [FILE_OR_DIR ...]

  A linter for YAML files. yamllint does not only check for syntax validity,
  but for weirdnesses like key repetition and cosmetic problems such as lines
  length, trailing spaces, indentation, etc.
```

#### Lib: eo-yaml (Java) - support for YAML 1.2, Java 8+

[eo-yaml](https://github.com/decorators-squad/eo-yaml)

```
    <dependency>
        <groupId>com.amihaiemil.web</groupId>
        <artifactId>eo-yaml</artifactId>
        <version>5.1.9</version>
    </dependency>
```

Not seen any other, serious YAML 1.2 parser lib for the JVM/Java/Scala.

[Up](../README.md)
