package netropy.polyglot.bigdata.local

import scala.collection.immutable.{Map, SortedMap}
import scala.collection.mutable.HashMap

/**
  * Ex 1.2 Write a local, scalable 'word counts' in Scala.
  */
object ex_1_2_WordCountsScala {

  /*
   * Code variants for 'wordCounts' in Scala.
   *
   * Why is lazy evaluation strongly desired here?
   *   + word counts can be computed as 1-pass map/reduce
   *   - realization of intermediate collections limited by memory
   *   => support for (arbitrarily) large input files
   *
   * Signature: lazy => sorted
   *   + Iterator: +lazy, +expressive, +general
   *       def wordCounts(lines: Iterator[String]): SortedMap[String, Long]
   *   - View: not needed, 2.12: lazy `.mapValues`, 2.13: `.groupMapReduce`
   *   - Stream: not wanted, no need for memoization in 1-pass algorithm
   *   * scala.collection.parallel: too special, 2.13: separate module
   *
   * 4 Code Variants:
   *
   * wordCounts_v0: as Scala 2.12 code, utilizes TraversableLike.groupBy
   *   + reasonably lazy
   *   + stays on `Iterator` for as long as possible
   *   + uses `.toIterable` whose default implementation is lazy (vs .toSeq),
   *     no guarantee, though, left to Iterator implementation (scaladoc)
   *   - uses `.mapValues`, lazy in 2.12, will be strict in future versions
   *   - use of `_.size.toLong` not ideal, reflects collection size limits
   *   + converts to `SortedMap` last
   *
   * wordCounts_v1: as Scala 2.13 code, utilizes IterableOps.groupMapReduce
   *   + reasonably lazy, same as v1
   *   + uses `groupMapReduce`, clarity & allows for internal optimization
   *   + use of `.to(SortedMap)`, nice Scala 2.13 API
   *
   * wordCounts_v2: as Iterator `foldLeft` building up an `immutable.Map`
   *   + maximally lazy, stays on Iterator all the way
   *
   * wordCounts_v3: as Iterator `for` loop updating a `mutable.HashMap`
   *   + maximally lazy, same as v2
   *   + local use of mutable collection, probably fastest
   */

  // Scala 2.12:

  @deprecated("uses deprecated collection API methods", "2.13")
  def wordCounts_v0(lines: Iterator[String]): SortedMap[String, Long] =
    SortedMap.empty[String, Long] ++        // strict
    lines                                   // lazy...
      .flatMap(_.split("[\\s\\p{Punct}]"))
      .filter(_.nonEmpty)
      .toIterable                           // likely lazy, no Iterator.groupBy
                                            // deprecated in 2.13
      .groupBy(identity)                    // strict
      .mapValues(_.size.toLong)             // lazy in 2.12, deprecated in 2.13

  // Scala 2.13:

  def wordCounts_v1(lines: Iterator[String]): SortedMap[String, Long] =
    lines                                        // lazy...
      .flatMap(_.split("[\\s\\p{Punct}]"))
      .filter(_.nonEmpty)
      .iterator.to(Iterable)                     // likely lazy (<-> .toSeq)
      .groupMapReduce(identity)(_ => 1L)(_ + _)  // strict, new in Scala 2.13
      .to(SortedMap)                             // strict, new in Scala 2.13

  def wordCounts_v2(lines: Iterator[String]): SortedMap[String, Long] =
    lines
      .flatMap(_.split("[\\s\\p{Punct}]"))
      .filter(_.nonEmpty)
      .foldLeft(Map.empty[String, Long])(
        (m, w) => m.updatedWith(w)(_.map(_ + 1L).orElse(Some(1L))))
      .to(SortedMap)  // strict

  def wordCounts_v3(lines: Iterator[String]): SortedMap[String, Long] = {
    val m = HashMap.empty[String, Long]
    for (w <- lines.flatMap(_.split("[\\s\\p{Punct}]")).filter(_.nonEmpty)) {
      m.updateWith(w)(_.map(_ + 1L).orElse(Some(1L)))
    }
    m.to(SortedMap)  // strict
  }
}
