package netropy.polyglot.bigdata.local;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Ex 1.3 Write a local, scalable 'word counts' in Java.
 */
class Ex_1_3_WordCountsJava {

    /*
     * Code variants for 'wordCounts' in Java.
     *
     * Why is lazy evaluation strongly desired here?
     *   + word counts can be computed as 1-pass map/reduce
     *   - realization of intermediate collections limited by memory
     *   => support for (arbitrarily) large input files
     *
     * Signature: lazy -> sorted
     *   + Stream: +lazy, +expressive, +functional, +parallel
     *       Function<Stream<String>, SortedMap<String, Long>>
     *   * alternatives:
     *     Spliterator: +lazy, -expressive, +internal iteration, +/-parallel
     *     or Iterator: +lazy, -expressive, must wrap for map/reduce as Stream
     *       Iterator -> [[Iterable] -> Spliterator] -> Stream
     *
     * 11 Code Variants:
     *
     * See notes on the concepts and terminology of the java.util.stream API:
     * .../polyglot/modules/java/Java_Stream_Function_API_Notes.md
     *
     * Summary:
     *   + most concise:         wordCounts_v00, utilizes Collector[s] API
     *   + most parallel:        wordCounts_v05, concurrent reduction+sorting
     *   + simple & sequential:  wordCounts_v09, imperative, Streams+Map only
     *   + simple & parallel:    wordCounts_v10, imperative, Streams+Map only
     *   - for illustration:     other versions
     *
     * wordCounts_v00: processes input as Stream, composes 3 Collectors
     *   + collectingAndThen() and groupingBy()/counting() for value reduction
     *   + lazy, + concise
     *   + parallelizable reduction, - sequential sorting
     *
     * wordCounts_v01: unpacks v0, replaces collectingAndThen(), counting()
     * wordCounts_v02: unpacks v1, replaces identity(), summingLong()
     * wordCounts_v03: unpacks v2, replaces reducing()
     *   + expansions of v0: + better readability, - less concise
     *   + parallelizable reduction, - sequential sorting
     *
     * wordCounts_v04: unpacks v3, replaces groupingBy​() -> toMap(), reducing()
     *   + use of Collectors.toMap: + simple and + general collector API
     *   + parallelizable reduction
     *
     * wordCounts_v05: parallelizes v4, replaces HashMap/TreeMap with concurrent
     *   + lazy, - concise
     *   + parallel reduction using toConcurrentMap with ConcurrentHashMap
     *   + parallel sorting using toConcurrentMap with ConcurrentSkipListMap
     *
     * wordCounts_v06: unpacks v4, replaces Collectors.toMap
     *   + lazy, - concise
     *   + parallel mutable reduction by collect(), - sequential sorting
     *   + parallelism w/o use of synchronization or concurrent datastructure!
     *   - possibly less efficient than mutable reduction
     *
     * wordCounts_v07: non-mutable reduction of v6, replaces Stream.collect
     *   + lazy, - concise, - cloning of collections, - INEFFICIENT
     *   + parallel non-mutable reduction by reduce(), - sequential sorting
     *   - sequential reduce() due to taking identity value instead of factory!
     *
     * wordCounts_v08: unpacks v6, replaces Stream.collect
     *   + lazy, - concise
     *   - sequential mutable reduction by reduce(), - sequential sorting
     *   - sequential reduce() due to taking identity value instead of factory!
     *
     * wordCounts_v09: unpacks v8, replaces Stream.reduce: foreach + side effect
     *   + lazy, + simple, + concise, imperative
     *   - sequential reduction, - sequential sorting
     *
     * wordCounts_v...: more variants possible, for example, use of
     *   Collectors.groupingByConcurrent, Spliterator, Stream.map
     */

    /** Splits a String into words as monadic function for Stream. */
    static Stream<String> split(String line) {
        // alternative: java.util.regex.Pattern
        //   Stream<String> splitAsStream​(CharSequence input)
        return Arrays
            .stream(line.split("[\\s\\p{Punct}]"))
            .filter(w -> !w.isEmpty());
    }

    static public SortedMap<String, Long> wordCounts_v00(Stream<String> lines) {

        return lines
            .unordered()                         // useful, order irrelevant
            .parallel()                          // useful, no side-effects
            .flatMap(Ex_1_3_WordCountsJava::split)
            .collect(
                Collectors.collectingAndThen(    // adapter, adds a finisher
                    Collectors.groupingBy​(       // group-by map collector
                        Function.identity(),     // classifier, key creator
                        Collectors.counting()),  // downstream, value reducer
                    TreeMap::new));              // finisher, result adapter
    }

    static public SortedMap<String, Long> wordCounts_v01(Stream<String> lines) {

        final Map<String, Long> wordCounts = lines
            .unordered()                         // useful, order irrelevant
            .parallel()                          // useful, no side-effects
            .flatMap(Ex_1_3_WordCountsJava::split)
            .collect(
                Collectors.groupingBy​(           // group-by map collector
                    Function.identity(),         // classifier, key creator
                    Collectors.summingLong(      // value map-reducer
                        w -> 1L)));

        return new TreeMap<>(wordCounts);
    }

    static public SortedMap<String, Long> wordCounts_v02(Stream<String> lines) {

        final Map<String, Long> wordCounts = lines
            .unordered()                         // useful, order irrelevant
            .parallel()                          // useful, no side-effects
            .flatMap(Ex_1_3_WordCountsJava::split)
            .collect(
                Collectors.groupingBy​(           // group-by map collector
                    w -> w,                      // classifier, key creator
                    Collectors.reducing(         // value reducer
                        0L, w -> 1L, Long::sum)));

        return new TreeMap<>(wordCounts);
    }

    static public SortedMap<String, Long> wordCounts_v03(Stream<String> lines) {

        final Map<String, Long> wordCounts = lines
            .unordered()                         // useful, order irrelevant
            .parallel()                          // useful, no side-effects
            .flatMap(Ex_1_3_WordCountsJava::split)
            .collect(
                Collectors.groupingBy​(           // group-by map collector
                    w -> w,                      // classifier, key creator
                    Collectors.mapping(          // value map-reducer
                        w -> 1L,                 // value mapper
                        Collectors.reducing(     // value reducer
                            0L, Long::sum))));

        return new TreeMap<>(wordCounts);
    }

    static public SortedMap<String, Long> wordCounts_v04(Stream<String> lines) {

        final Map<String, Long> wordCounts = lines
            .unordered()                         // useful, order irrelevant
            .parallel()                          // useful, no side-effects
            .flatMap(Ex_1_3_WordCountsJava::split)
            .collect(
                Collectors.toMap(                // general map collector
                    w -> w,                      // key mapper
                    w -> 1L,                     // value mapper
                    Long::sum,                   // merge function, reducer
                    HashMap::new));              // map factory (optional)

        return new TreeMap<>(wordCounts);
    }

    static public SortedMap<String, Long> wordCounts_v05(Stream<String> lines) {

        final ConcurrentMap<String, Long> wordCounts = lines
            .parallel()                          // required for concurrency
            .unordered()                         // useful, order irrelevant
            .flatMap(Ex_1_3_WordCountsJava::split)
            .collect(
                Collectors.toConcurrentMap(      // required for concurrency
                    w -> w,                      // key mapper, identity
                    w -> 1L,                     // value mapper
                    Long::sum,                   // merge function
                    ConcurrentHashMap::new));    // map factory (optional)

        final SortedMap<String, Long> sorted = wordCounts
            .entrySet()                          // unique keys! see merge...
            .parallelStream()                    // required for concurrency
            //.unordered()                       // redundant, unordered source
            .collect(
                Collectors.toConcurrentMap(      // required for concurrency
                    Map.Entry::getKey,           // key mapper, projection
                    Map.Entry::getValue,         // value mapper, projection
                    // merge function is not called but must return value
                    (l0, l1) -> { assert false; return null; },
                    ConcurrentSkipListMap::new));  // ordered & concurrent map

        return sorted;
    }

    static public SortedMap<String, Long> wordCounts_v06(Stream<String> lines) {

        final Map<String, Long> wordCounts = lines
            .parallel()                          // useful, no side-effects
            .unordered()                         // useful, order irrelevant
            .flatMap(Ex_1_3_WordCountsJava::split)
            .collect(
                // supplier: creates a mutable result container
                HashMap::new,
                // accumulator: associative, non-interfering, stateless fold
                (m, w) -> m.merge(w, 1L, Long::sum),
                // combiner: compatible with accumulator, must fold 2nd -> 1st
                (m0, m1) -> {
                    for (Map.Entry<String, Long> e : m1.entrySet())
                        m0.merge(e.getKey(), e.getValue(), Long::sum);
                });

        return new TreeMap<>(wordCounts);
    }

    static public SortedMap<String, Long> wordCounts_v07(Stream<String> lines) {

        final Map<String, Long> wordCounts = lines
            .parallel()                          // useful, no side-effects
            .unordered()                         // useful, order irrelevant
            .flatMap(Ex_1_3_WordCountsJava::split)
            .reduce(
                // identity value for the combiner (not a Supplier)
                new HashMap<String, Long>(),
                // accumulator: associative, non-interfering, stateless fold
                (m, w) -> {
                    // as Java does not offer efficient immutable collections
                    HashMap<String, Long> c = new HashMap<>(m);  // CLONE!
                    c.merge(w, 1L, Long::sum);
                    return c;
                },
                // combiner: compatible with accumulator, must return merged
                (m0, m1) -> {
                    // as Java does not offer efficient immutable collections
                    HashMap<String, Long> c = new HashMap<>(m1);  // CLONE!
                    for (Map.Entry<String, Long> e : m0.entrySet())
                        c.merge(e.getKey(), e.getValue(), Long::sum);
                    return c;
                });

        return new TreeMap<>(wordCounts);
    }

    static public SortedMap<String, Long> wordCounts_v08(Stream<String> lines) {

        final Map<String, Long> wordCounts = lines
            .sequential()                        // required, side-effects!
            //.parallel()                        // ->concurrent modification!
            .unordered()                         // useful, order irrelevant
            .flatMap(Ex_1_3_WordCountsJava::split)
            .reduce(
                // identity value for the combiner (not a Supplier)
                new HashMap<String, Long>(),
                // accumulator: associative, non-interfering, stateless fold
                (m, w) -> { m.merge(w, 1L, Long::sum); return m; },
                // combiner: compatible with accumulator, must return merged
                (m0, m1) -> {
                    for (Map.Entry<String, Long> e : m0.entrySet())
                        m1.merge(e.getKey(), e.getValue(), Long::sum);
                    return m1;
                });

        return new TreeMap<>(wordCounts);
    }

    static public SortedMap<String, Long> wordCounts_v09(Stream<String> lines) {

        final Map<String, Long> wordCounts = new HashMap<>();  // non-concurrent
        lines
            .sequential()                        // required, side-effects!
            //.parallel()                        // ->concurrent modification!
            .unordered()                         // useful, order irrelevant
            .flatMap(Ex_1_3_WordCountsJava::split)
            .forEach(w -> wordCounts.merge(w, 1L, Long::sum));

        return new TreeMap<>(wordCounts);
    }

    static public SortedMap<String, Long> wordCounts_v10(Stream<String> lines) {

        final Map<String, Long> wordCounts = new ConcurrentHashMap<>();
        lines
            .parallel()                          // ok, concurrent result map
            .unordered()                         // useful, order irrelevant
            .flatMap(Ex_1_3_WordCountsJava::split)
            .forEach(w -> wordCounts.merge(w, 1L, Long::sum));

        return new TreeMap<>(wordCounts);
    }
}
