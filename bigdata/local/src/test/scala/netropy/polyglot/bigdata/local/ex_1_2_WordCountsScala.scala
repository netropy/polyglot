package netropy.polyglot.bigdata.local

import scala.collection.immutable.SortedMap
import scala.collection.mutable.StringBuilder
import scala.io.Source

/*
 * Demonstrating `circe` for generating and parsing JSON in Scala.
 *
 * @see https://circe.github.io/circe/
 */
import io.circe  // json en-/decoding, see below

/**
  * Ex 1.2 Write a local, scalable 'word counts' in Scala.
  */
// Surefire: Tests must reside in a Scala 'class', not an 'object'.
class Ex_1_2_WordCountVariantsScala_Tests {

  // Surefire only runs test methods that meet _all_ of these criteria:
  // 1. name starts with _exactly_ `test` followed by a non-empty suffix
  // 2. method is parameterless
  // 3. method has a return type of _exactly_ `Unit` (or `void`)

  // function under test
  type wordCounts = Iterator[String] => SortedMap[String, Long]

  def test_wordCounts(wc: wordCounts, inputTextFile: String): Unit = {

    // skipping error handling (not using/loaning resource)
    val inputSource = Source.fromFile(inputTextFile)
    val resultMap = wc(inputSource.getLines())
    inputSource.close()

    /*
     * Converts a Map -> circe.Json object
     */
    val printResultAsJson = false
    if (printResultAsJson) {
      import circe.syntax._
      val jsonMap: circe.Json = resultMap.asJson
      println(s"----------------------------------------")
      println(s"$jsonMap")
      println(s"----------------------------------------")
    }

    /*
     * Parses a file -> single String -> circe.Json object
     */
    val goldenJsonFile = inputTextFile + ".json"
    val goldenJson: circe.Json = {
      // skipping error handling (not using/loaning resource)
      val goldenJsonSource = Source.fromFile(goldenJsonFile)

      /*
       * Parses file -> single String, filters out comment lines
       */
      val goldenJsonString = goldenJsonSource
        // shortcut if not allowing for comment lines:
        //   .foldLeft(new StringBuilder())((sb, c) => sb.append(c)).toString
        .getLines()
        .filterNot(_.trim.startsWith("//"))
        .foldLeft(new StringBuilder())((sb, l) => sb ++= l).toString
      goldenJsonSource.close()

      /*
       * Parses as one-record-per-string JSON format (https://www.json.org/).
       */
      import circe.parser._
      parse(goldenJsonString).toTry.get
    }

    // expecting a map
    val goldenMap = goldenJson.as[SortedMap[String, Long]].toTry.get

    val pass = (goldenMap == resultMap)
    if (!pass) {
      Console.err.println("ERROR: goldenMap != resultMap:")
      Console.err.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
      Console.err.println(s"$goldenMap")
      Console.err.println("----------------------------------------")
      Console.err.println(s"$resultMap")
      Console.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    }
    assert(pass, "goldenMap != resultMap, see Console.err for details")
  }

  import ex_1_2_WordCountsScala.{wordCounts_v0, wordCounts_v1, wordCounts_v2,
    wordCounts_v3}

  val kublakhan = "../test_data/kublakhan.txt"

  @deprecated("uses deprecated collection API methods", "2.13")
  def test_v0(): Unit =
    test_wordCounts(wordCounts_v0, kublakhan)

  def test_v1(): Unit =
    test_wordCounts(wordCounts_v1, kublakhan)

  def test_v2(): Unit =
    test_wordCounts(wordCounts_v2, kublakhan)

  def test_v3(): Unit =
    test_wordCounts(wordCounts_v3, kublakhan)
}
