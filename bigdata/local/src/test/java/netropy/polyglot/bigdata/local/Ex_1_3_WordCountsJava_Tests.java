package netropy.polyglot.bigdata.local;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.JSONException;


/**
 * Ex 1.3 Write a local, scalable 'word counts' in Java.
 */
public class Ex_1_3_WordCountsJava_Tests {

    // Surefire only runs test methods that meet _all_ of these criteria:
    // 1. name starts with _exactly_ `test` followed by a non-empty suffix
    // 2. method is parameterless
    // 3. method has a return type of _exactly_ `Unit` (or `void`)

    // function under test
    interface WordCounts
        extends Function<Stream<String>, SortedMap<String, Long>>
    {}

    void test_wordCounts(WordCounts wc, String inputTextFile)
        throws IOException, JSONException {

        // streams whose source is an IO channel require closing
        final SortedMap<String, Long> resultMap;
        try (Stream<String> lines = Files.lines​(Path.of(inputTextFile))) {
            resultMap = wc.apply(lines);
        }

        //
        // Converts result Map -> JSONObject -> pretty String (indent=2)
        //
        final boolean printResultAsJson = false;
        if (printResultAsJson) {
            JSONObject jsonMap = new JSONObject(resultMap);
            System.out.println("----------------------------------------");
            System.out.println(jsonMap.toString(2));
            System.out.println("----------------------------------------");
        }

        //
        // Parses golden file -> JSONTokener -> JSONObject
        //
        final Path goldenJsonFile = Path.of(inputTextFile + ".json");
        final JSONObject goldenJson;
        try (BufferedReader reader = Files.newBufferedReader​(goldenJsonFile)) {

            // skips 1st line if a comment - good enough
            assert reader.markSupported();
            reader.mark(1024);
            String line = reader.readLine().trim();
            if (!line.startsWith("//"))
                reader.reset();

            // parses file as one-record JSON format
            goldenJson = new JSONObject(new JSONTokener(reader));
        }

        //
        // Converts golden JSONObject -> SortedMap<String, Long>
        //
        SortedMap<String, Long> goldenMap = goldenJson.toMap().entrySet()
            .stream()
            .collect(
                Collectors.toMap(
                    e -> e.getKey(),
                    e -> ((Integer)e.getValue()).longValue(),  // map value
                    Long::sum,
                    TreeMap::new)
                );

        boolean pass = goldenMap.equals(resultMap);
        if (!pass) {
            System.err.println("ERROR: goldenMap != resultMap:");
            System.err.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            System.err.println(goldenMap.toString());
            System.err.println("----------------------------------------");
            System.err.println(resultMap.toString());
            System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        }
        assert pass : "goldenMap != resultMap, see System.err for details";
    }

    static final String kublakhan = "../test_data/kublakhan.txt";

    public void test_v00() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v00, kublakhan);
    }

    public void test_v01() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v01, kublakhan);
    }

    public void test_v02() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v02, kublakhan);
    }

    public void test_v03() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v03, kublakhan);
    }

    public void test_v04() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v04, kublakhan);
    }

    public void test_v05() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v05, kublakhan);
    }

    public void test_v06() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v06, kublakhan);
    }

    public void test_v07() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v07, kublakhan);
    }

    public void test_v08() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v08, kublakhan);
    }

    public void test_v09() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v09, kublakhan);
    }

    public void test_v10() throws IOException, JSONException {
        test_wordCounts(Ex_1_3_WordCountsJava::wordCounts_v10, kublakhan);
    }
}
