# polyglot/bigdata/local

### Local (Non-Cluster) Code Exercises

#### 1.1 Code-review this local version of a 'word counts' program.

[Spark's](https://spark.apache.org/)
code examples show this
[local, non-cluster version](https://github.com/apache/spark/blob/master/examples/src/main/scala/org/apache/spark/examples/DFSReadWriteTest.scala)
of a _word counts_ program:
```
  private def readFile(filename: String): List[String] = {
    val lineIter: Iterator[String] = Source.fromFile(filename).getLines()
    val lineList: List[String] = lineIter.toList
    lineList
  }

  def runLocalWordCount(fileContents: List[String]): Int = {
    fileContents
	  .flatMap(_.split(" "))
      .flatMap(_.split("\t"))
      .filter(_.nonEmpty)
      .groupBy(w => w)
      .mapValues(_.size)
      .values
      .sum
  }
```

Discuss the code from the viewpoint of a standalone Scala program.

[Remarks](ex_1_1.r.wordcount.md)

#### 1.2 Write a local, scalable 'word counts' in Scala.

Write a function that
- splits input text into words
- returns a map: word -> count
- supports (arbitrarily) large input text

Provide a unit test that
- reads from a JSON file a reference ("golden") word counts map
- verifies the result map of above function(s) against golden.

Discuss
- method signatures, code variants
- use of Scala 2.13 vs 2.12 collection API
- use of lazy evaluation, functional parallelism
- JSON parsing and conversion in Scala

See code comments for discussion.

[Scala source](src/main/scala/netropy/polyglot/bigdata/local/ex_1_2_WordCountsScala.scala),
[Scala test](src/test/scala/netropy/polyglot/bigdata/local/ex_1_2_WordCountsScala.scala)

#### 1.3 Write a local, scalable 'word counts' in Java.

Same as 1.2 but for Java.  Discuss use of the Java _java.util.stream_ API.

See code comments for discussion.

See [Notes: java.util.{stream,function}](../../java/Java_Stream_Function_API_Notes.md)

[Java source](src/main/java/netropy/polyglot/bigdata/local/ex_1_3_WordCountsJava.java),
[Java test](src/test/java/netropy/polyglot/bigdata/local/ex_1_3_WordCountsJava.java)

[Up](../README.md)
