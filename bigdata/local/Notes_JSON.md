# polyglot/bigdata/local

### Notes: JSON Processing in Scala, Java et al

Searching for "json" in 2021... \
[Scaladex](https://index.scala-lang.org/search?topics=json): 104 libraries \
[MvnRepository](https://mvnrepository.com/search?q=json): 5861 matches \
[GitHub](https://github.com/topics/json): 20,610 public repositories

Notes & references on selected Scala and Java JSON libraries.

___TOC:___ \
JSON File Formats \
JSON Lib Metrics, Catalogues \
Tool: _jq_ (CLI) \
Lib: _circe_ (Scala) \
Lib: _Play JSON_ (Scala) \
Lib: _Json4s_ (Scala) \
Lib: _Jawn_ (Scala) \
Lib: _ninny-json_ (Scala) \
Lib: _uJson_, _µPickle_ (Scala) \
Lib: _scala.util.parsing.json_ \
Lib: _JSON-java_ \
Lib: _Gson_ (Java) \
Lib: _Jackson_ (Java) \
Lib: _JSON-P_ (Java) \
Lib: _JSON.simple_ (Java)

#### JSON File Formats

JSON text files come in two formats:
- [.json: one-record-per-file JSON format](https://www.json.org/)
  - lightweight, human-readable, language-independent data-interchange format
  - _object:_ an unordered set of name/value pairs
  - _array:_ an ordered collection of values
  - _value:_ a string in double quotes, a number, a true/false/null literal,
    an object, or an array
- [.jsonl: newline-delimited JSON Lines format](https://jsonlines.org/)
  - each line is a valid JSON value, mostly an object or an array
  - line Separator is '\n'; also supports '\r\n' since ignoring trailing white
    space when parsing JSON values
  - assumes UTF-8 encoding; also supports ASCII escape sequences for unicode
    characters to make for plain ASCII files

#### JSON Lib Metrics, Catalogues

Not enough time for a systematic effort, here.

However, it _would_ be informative to compare (below's) JSON libraries along:
1. parsing _options_ and _tolerance_ vs _Spark's json DataFrameReader_
1. learning effort, size of the API, quality of documentation
1. support for default+custom mappings of AST <-> _case classes_, _beans_
1. support for _None/Null_-handling with Scala _Option_, Java _Optional_
1. support for Scala _2.{11,12,13}_ and _3.x_
1. deserialization performance

[Spark json DataFrameReader](https://spark.apache.org/docs/latest/api/java/org/apache/spark/sql/DataFrameReader.html#json-java.lang.String...-)
offers a rich set of parsing options, such as:
- _multiLine_, _allowComments_, _allowUnquotedFieldNames_,
  _allowSingleQuotes_, _allowNumericLeadingZeros_,
- _mode={PERMISSIVE,DROPMALFORMED,FAILFAST}_ et al.

Helpful catalogues:
[json.org](https://www.json.org/),
[scaladex: json](https://index.scala-lang.org/search?topics=json)

Discussion of some libs:
[couchbase: json ](https://docs.couchbase.com/scala-sdk/current/howtos/json.html),
[manuel.bernhardt: json (2015)](https://manuel.bernhardt.io/2015/11/06/a-quick-tour-of-json-libraries-in-scala)

#### Tool: _jq_ (CLI) - command-line JSON processor

Link:
[jq](https://stedolan.github.io/jq/)

Useful tool to slice, filter, and transform structured data -- like _sed_,
_awk_, _grep_ for text.

```
  $ jq --help
  jq - commandline JSON processor [version 1.6]

  Usage:
    jq [options] <jq filter> [file...]
    jq [options] --args <jq filter> [strings...]
    jq [options] --jsonargs <jq filter> [JSON_TEXTS...]

  jq is a tool for processing JSON inputs, applying the given filter to
  its JSON text inputs and producing the filter's results as JSON on
  standard output.
  ...
```

#### Lib: _circe_ (Scala) - functional, reliable, reported as being fast

Pronounced SUR-see.  Group of libraries/projects.

Links:
[circe](https://circe.github.io/circe),
[github](https://github.com/circe),
[scaladex](https://index.scala-lang.org/circe),
[mvn](https://mvnrepository.com/artifact/io.circe)

Tried out _circe-{core,generic,parser}_ on
[this code exercise](ex_1.q.local.md):
positive impression, convenient and reliable.

Dependency:
[Cats Core](https://typelevel.org/cats).

History: a fork of
[argonaut](http://argonaut.io/)
with significant improvements (e.g. replacing use of scalaz with cats).

Long list of adopters.  About ~50 circe companion libraries/projects for yaml,
golden file testing, JSON schema validation etc.

[This ad-hoc benchmark](https://www.lihaoyi.com/post/uJsonfastflexibleandintuitiveJSONforScala.html#zero-overhead-serialization)
reports circe as ~2x faster than Play JSON.

Example: Use of (local) implicits for conversion to/from JSON
```
    import io.circe;  // json en-/decoding

    // converts a Map -> circe.Json object
	val map: Map[String, Int] = ...
    import circe.syntax._
    val jsonMap: circe.Json = map.asJson

    // parses as one-record JSON string -> circe.Json object
	format (https://www.json.org/)
    val jsonStr: String = ...
    import circe.parser._
    val jsonObj: circe.Json = parse(jsonStr).toTry.get
```

[Codec derivation](https://circe.github.io/circe/):
_"circe does not use macros or provide any kind of automatic derivation in the
core project. Instead [...] circe includes a subproject (generic) that
provides generic codec derivation using
[Shapeless](https://github.com/milessabin/shapeless)."_

[Encoding and decoding](https://circe.github.io/circe/codec.html):
_"An Encoder[A] instance provides a function that will convert any A to a
Json, and a Decoder[A] takes a Json value to either an exception or an A."_

_"circe provides implicit instances of these type classes for many types from
the Scala standard library, including Int, String, and others. It also
provides instances for List[A], Option[A], and other generic types, but only
if A has an Encoder instance."_

[Semi-automatic Derivation](https://circe.github.io/circe/codecs/semiauto-derivation.html):
provide specific _Encoder_ or _Decoder_ instances:
```
  import io.circe._, io.circe.generic.semiauto._
  case class Foo(a: Int, b: String, c: Boolean)
  implicit val fooDecoder: Decoder[Foo] = deriveDecoder
  implicit val fooEncoder: Encoder[Foo] = deriveEncoder
```

Core type classes:
```
trait Encoder[A] {
  def apply(a: A): Json
}
trait Decoder[A] {
  def apply(c: HCursor): Decoder.Result[A]
  def tryDecode(c: ACursor): Decoder.Result[A]
}
```

#### Lib: _Play JSON_ (Scala) - functional, uses Jackson, uses macros

Links:
[Play JSON](https://www.playframework.com/documentation/2.8.x/ScalaJson),
[github](https://github.com/playframework/play-json),
[scaladex](https://index.scala-lang.org/playframework/play-json/play-json),
[mvn](https://mvnrepository.com/artifact/com.typesafe.play/play-json)

_Play JSON_ is a standalone Scala JSON library, originally developed as part
of the
[Play Framework](https://www.playframework.com).
It uses
[Jackson](https://github.com/FasterXML/jackson)
for JSON parsing.

- _+_ Actively maintained, published by
  [typesafe/lightbend](https://www.lightbend.com)
- _-_ The [API](https://www.playframework.com/documentation/2.8.x/api/scala/play/api/libs/json/index.html)
  appears large (its true surface area might be smaller, though): \
  _32_ traits / abstract classes, _18_ [case] classes, _27_ [case] objects
- _+_ Conversion objects <-> JSON via typeclasses and extension methods. \
  [Readme](https://github.com/playframework/play-json):
  _"To convert a Scala object to and from JSON, we use Json.toJson[T: Writes]
  and Json.fromJson[T: Reads] respectively. Play JSON provides the Reads and
  Writes typeclasses to define how to read or write specific types."_
- _-_ Uses Scala2 Macros -- adaptation and candidate release for Scala 3 \
  [Readme](https://github.com/playframework/play-json):
  _"Usually you don't need to traverse JSON AST directly. Play JSON comes
  equipped with some convenient macros to convert to and from case classes."_
  ```
    case class Resident(name: String, age: Int, role: Option[String])
    implicit val residentReads = Json.reads[Resident]  // macro, adds fromJson
    implicit val residentWrites = Json.writes[Resident]  // macro, adds toJson
    implicit val residentFormat = Json.format[Resident]  // macro, adds both
  ```
- _+_ Supports customized JSON
  [Reads/Writes/Format Combinators](https://www.playframework.com/documentation/2.8.x/ScalaJsonCombinators).
  for core type classes:
  ```
    trait Reads[A] { def reads(json: JsValue): JsResult[A] }
    trait Writes[A] { def writes(o: A): JsValue }
  ```
- _+_ Supports customized JSON

#### Lib: _Json4s_ (Scala) - functional, ships a Jackson parser

Group of libraries/projects.

Links:
[Json4s](https://json4s.org),
[github](https://github.com/playframework/play-json),
[scaladex](https://index.scala-lang.org/json4s),
[mvn](https://mvnrepository.com/artifact/org.json4s)

_Json4s_ is a standalone package based on the
[Json AST library and parser](https://github.com/lift/framework/tree/master/core/json)
developed as part of the
[Lift Web Framework](http://liftweb.net).

Per [Readme](https://github.com/json4s/json4s):
- _"... aims to provide a single AST to be used by other scala json
  libraries."_
- ships a "native" parser and an alternative parser based on
  [Jackson](https://github.com/FasterXML/jackson).
- rich list of features: \
  fast JSON parser, LINQ style queries, can extract values from parsed JSON
  into _case classes_, diff & merge, DSL to produce valid JSON, XPath-like
  expressions and HOFs to manipulate JSON, pretty and compact printing, XML
  conversions et al.

Core type classes:
```
  trait Reader[T] { def read(value: JValue): T }
  trait Writer[-T] { def write(obj: T): JValue }
```

Deserialization ("extraction") JSON -> case class instance appears to utilize
reflection (rather than macros).

If constructor parameter names cannot match json field names, _back ticks_ can
be used to deal with characters not allowed in Scala identifiers.  Pre-defined
method supports snake-case to camel-case translation (_camelizeKeys_).

#### Lib: _Jawn_ (Scala) - functional, Scala 2.x, Dotty, lightweight AST

Group of libraries/projects.

Links:
[github](https://github.com/typelevel/jawn),
[scaladex](https://index.scala-lang.org/typelevel/jawn),
[mvn jawn-ast](https://mvnrepository.com/artifact/org.typelevel/jawn-ast),
[mvn jawn-parser](https://mvnrepository.com/artifact/org.typelevel/jawn-parser),
[mvn jawn-util](https://mvnrepository.com/artifact/org.typelevel/jawn-util)

Main packages:
- _jawn-ast_ "small, somewhat anemic AST"
- _jawn-parser_ "fast, generic JSON parser"
_ _jawn-util_ "few helpful utilities"

Some connection (shared heritage? integration?) with other projects, for
example: Argonaut, circe, Json4s, Play, Rojoma, Spray.

- _+_ support for Scala 2.x, 3.0
- _+_ promising: current packages are published under the
  [typelevel.org](https://typelevel.org)
  domain (why not listed as project on their website?).  (Various older
  packages hosted under other domains, e.g.,
  [org.jsawn](https://mvnrepository.com/artifact/org.jsawn).)
- _-_ little high-level, usage information per quick look at some
  [scaladoc](https://github.com/typelevel/jawn/blob/master/parser/src/main/scala/jawn/Parser.scala)

#### Lib: _ninny-json_ (Scala) - discussion of _null_ vs _Option.None_

Links:
[github](https://github.com/kag0/ninny-json),
[SF Scala meetup](https://www.youtube.com/watch?v=3v-5Q4NLdNE)

Experimental code by Nathaniel Fischer: _"JSON typeclasses that know the
difference between null and absent fields."_

[Discussion](https://github.com/kag0/ninny-json#what-do-libraries-do-today)
of major JSON libs handling _Option_ differently for _Reads_ and _Writes_,
writing _Option.None_ to _null_ etc.

[API proposal for core type classes](https://github.com/kag0/ninny-json#what-are-we-proposing).

#### Lib: _uJson_, _µPickle_ (Scala) - discussion of read-transform-writes

_uJson_ (pronounced micro-json) is a standalone library for JSON string
manipulation.  Serves as a back-end for the _uPickle_ serialization library.

Links:
[uPickle, uJson, uPack](https://www.lihaoyi.com/upickle),
[github](https://github.com/lihaoyi/upickle),
[scaladex](https://index.scala-lang.org/lihaoyi/upickle-pprint)
[mvn upickle](https://mvnrepository.com/artifact/com.lihaoyi/upickle)
[mvn ujson](https://mvnrepository.com/artifact/com.lihaoyi/ujson)

Blogs: \
[uJson: fast, flexible and intuitive JSON for Scala](https://www.lihaoyi.com/post/uJsonfastflexibleandintuitiveJSONforScala.html) \
[How to work with JSON in Scala](https://www.lihaoyi.com/post/HowtoworkwithJSONinScala.html)

Reported features: high Performance, simple JSON Processing API, customizable,
ScalaJS support.

The uJson API exposes mutable, JSON AST-like data types next with (procedural)
_read/write_ methods:
```
  package object ujson{
    def read(s: Transformable): Js.Value
    def copy(t: Js.Value): Js.Value
    def write(t: Js.Value, indent: Int = -1): String
    def writeTo(t: Js.Value, out: java.io.Writer, indent: Int = -1): Unit
    ...
  }
```

uPickle seems to be formulated over (object-oriented rather than
typeclass-based?) _Reader/Writer_ types.

[Code example](https://www.lihaoyi.com/post/uJsonfastflexibleandintuitiveJSONforScala.html#intuitiveness)
of 1) parsing a string into a JSON value, 2) changing a data member, and 3)
writing back the value as JSON string: for Python, Ruby, JS, Scala uJson vs
Argonaut, circe, Play JSON.  Clumsiness of Scala libraries is attributed to
use of immutable JSON AST types.  _Hmm, not sure about that generalization._

#### Lib: _scala.util.parsing.json_ - obsolete, Scala2 (but great code example)

Package
[scala.util.parsing](https://www.scala-lang.org/api/2.12.13/scala-parser-combinators/scala/util/parsing/index.html)
was shipped with Scala up to version _2.12_ and has since been maintained as
separate _Scala Standard Parser Combinator Library_, called:
[scala-parser-combinators](https://index.scala-lang.org/scala/scala-parser-combinators/scala-parser-combinators).

Contained package
[scala.util.parsing.json](https://www.javadoc.io/static/org.scala-lang.modules/scala-parser-combinators_2.13/1.1.2/scala/util/parsing/json/index.html)
offered combinators for JSON, although with this caveat: \
  _"This package was never intended for production use; it's really more of a
  code sample demonstrating how to use parser combinators."_

Library _scala-parser-combinators_ shipped package _.json_ up to version
[1.1.2](https://index.scala-lang.org/scala/scala-parser-combinators/scala-parser-combinators/1.1.2)
(as Scala _2.{11,12,13}_ consumable).  Milestone release
[1.2.0-M1](https://index.scala-lang.org/scala/scala-parser-combinators/scala-parser-combinators/1.2.0-M1),
has package _.json_ removed entirely.

#### Lib: _JSON-java_ - json.org reference implementation, in maintenance mode

Links:
[github](https://github.com/stleary/JSON-java),
[API doc](https://stleary.github.io/JSON-java/index.html),
[mvn](https://mvnrepository.com/artifact/org.json/json)

Stated
[project goals](https://github.com/stleary/JSON-java/wiki/FAQ#what-is-the-goal-of-this-project):
- continue in maintenance mode for stability, availability
- fix reported bugs, ensure to compile against new Java versions
- maintain compatibility with RFC 8259 and ECMA-404

Example: map/file/json object with just name/Integer attributes
```
    //import java.io.{BufferedReader,IOException};
	//import java.nio.file.{Path,Files};
	//import java.util.Map;
	//import org.json.{JSONObject,JSONTokener,JSONException};

    // converts Map -> JSONObject -> pretty String (indent=2)
    Map<String, Integer> map = ...;
    JSONObject jsonMap = new JSONObject(map);
    System.out.println(jsonMap.toString(2));

    // parses one-record JSON file -> JSONTokener -> JSONObject
    Path jsonFile = Path.of("file.json");
    JSONObject jsonObj;
    try (BufferedReader reader = Files.newBufferedReader​(jsonFile)) {
        json = new JSONObject(new JSONTokener(reader));
    }

    // converts JSONObject -> Map
    Map<String, Integer> map = jsonObj.toMap();
```

#### Lib: _Gson_ (Java) - from Google, not an officially supported product

Links:
[github](https://github.com/google/gson),
[mvn](https://mvnrepository.com/artifact/com.google.code.gson/gson)

Promising.  [Design goals](https://github.com/google/gson#goals):
- simple _toJson()_, _fromJson()_ converter methods
- conversion of pre-existing, unmodifiable objects to/from JSON
- support arbitrarily complex objects (deep inheritance, generic types)

#### Lib: _Jackson_ (Java) - multi-format serialization, said to be fast

Links:
[github](https://github.com/FasterXML/jackson),
[mvn jackson-core](https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core),
[mvn jackson-databind](https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind),
[mvn jackson-annotations](https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations)

Framework for parsing, generating, transforming, querying of: json, xml, yaml
avro, csv, protobuf, etc.

#### Lib: _JSON-P_ (Java) - Jakarta (JavaEE) JSON Processing

Links:
[JSON-P](https://eclipse-ee4j.github.io/jsonp/),
[mvn json-api](https://mvnrepository.com/artifact/jakarta.json/jakarta.json-api),
[mvn jakarta impl](https://mvnrepository.com/artifact/org.glassfish/jakarta.json)

Part of [Jakarta EE](https://jakarta.ee) APIs with
[Glassfish](https://glassfish.org) as default provider.

#### Lib: _JSON.simple_ (Java) - obsolete, once seen used in many tutorials

Links:
[JSON.simple](https://code.google.com/archive/p/json-simple/),
[github](https://github.com/fangyidong/json-simple),
[mvn json-simple](https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple)

Last updates seem from over 8 years ago.

[Up](../README.md)
