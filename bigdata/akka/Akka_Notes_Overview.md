# polyglot/bigdata/akka

### Notes: Akka Overview

___TOC:___ \
[Resources](#resources) \
[Features](#features) \
[Concepts & Terminology](#concepts-terminology)

#### Resources

- [Akka](https://akka.io/)
- [Docs](https://doc.akka.io/docs/akka/current/),
  [Akka Modules](https://akka.io/docs/),
  [Getting Started](https://doc.akka.io/docs/akka/current/typed/guide/index.html),
  [Actors Quickstart](https://akka.io/try-akka/)
- Lightbend:
  [Akka Tutorials](https://developer.lightbend.com/start/?group=akka),
  [Guides, Webinars](https://developer.lightbend.com/guides/),
  [Akka Platform](https://www.lightbend.com/akka-platform),
  [Akka Platform Guide](https://developer.lightbend.com/docs/akka-platform-guide/)
- [Akka API Scaladoc](https://doc.akka.io/api/akka/current/akka/index.html),
  [Akka API Javadoc](https://doc.akka.io/japi/akka/current/akka/package-summary.html)
- githubs:
  [Akka](https://github.com/akka/akka),
  [Akka Project](https://github.com/akka),
  [Lightbend Akka Projects](https://github.com/lightbend?q=akka)

#### Features


- #lines of code (github/akka/akka, 2021-03): 458k Scala, 317k Java

------------------------------------------------------------

Akka is a free and open-source toolkit and runtime simplifying the
construction of concurrent and distributed applications on the JVM. Akka
supports multiple programming models for concurrency, but it emphasizes
actor-based concurrency,.

https://akka.io/
Build powerful reactive, concurrent, and distributed applications more easily

Akka is a toolkit for building highly concurrent, distributed, and resilient message-driven applications for Java and Scala

Akka is the implementation of the Actor Model on the JVM. 

------------------------------------------------------------

https://akka.io/
Simpler Concurrent & Distributed Systems

Actors and Streams let you build systems that scale up, using the resources of a server more efficiently, and out, using multiple servers.
resilience
Resilient by Design

Building on the principles of The Reactive Manifesto Akka allows you to write systems that self-heal and stay responsive in the face of failures.
speed
High Performance

Up to 50 million msg/sec on a single machine. Small memory footprint; ~2.5 million actors per GB of heap.
cloud
Elastic & Decentralized

Distributed systems without single points of failure. Load balancing and adaptive routing across nodes. Event Sourcing and CQRS with Cluster Sharding. Distributed Data for eventual consistency using CRDTs.
streams
Reactive Streaming Data

Asynchronous non-blocking stream processing with backpressure. Fully async and streaming HTTP server and client provides a great platform for building microservices. Streaming integrations with Alpakka.

------------------------------------------------------------

https://doc.akka.io/docs/akka/current/typed/guide/introduction.html
To help you deal with these realities, Akka provides:
- Multi-threaded behavior without the use of low-level concurrency constructs like atomics or locks — relieving you from even thinking about memory visibility issues.
- Transparent remote communication between systems and their components — relieving you from writing and maintaining difficult networking code.
- A clustered, high-availability architecture that is elastic, scales in or out, on demand — enabling you to deliver a truly reactive system.

------------------------------------------------------------

Overview of Akka libraries and modules
https://doc.akka.io/docs/akka/current/typed/guide/modules.html

The following capabilities are included with Akka OSS and are introduced later on this page:

Actor library
Remoting
Cluster
Cluster Sharding
Cluster Singleton
Persistence
Distributed Data
Streams
HTTP
With a Lightbend Platform Subscription, you can use Akka Enhancements that includes

------------------------------------------------------------

https://en.wikipedia.org/wiki/Akka_(toolkit)

Distinguishing features

The key points distinguishing applications based on Akka actors are:

    Concurrency is message-based and asynchronous: typically no mutable data are shared and no synchronization primitives are used; Akka implements the actor model.
    The way actors interact is the same whether they are on the same host or separate hosts, communicating directly or through routing facilities, running on a few threads or many threads, etc. Such details may be altered at deployment time through a configuration mechanism, allowing a program to be scaled up (to make use of more powerful servers) and out (to make use of more servers) without modification.
    Actors are arranged hierarchically with regard to program failures, which are treated as events to be handled by an actor's supervisor (regardless of which actor sent the message triggering the failure). In contrast to Erlang, Akka enforces parental supervision, which means that each actor is created and supervised by its parent actor.

Akka has a modular structure, with a core module providing actors. Other modules are available to add features such as network distribution of actors, cluster support, Command and Event Sourcing, integration with various third-party systems (e.g. Apache Camel, ZeroMQ), and even support for other concurrency models such as Futures and Agents. 

======================================================================

Apache Akka is a distributed data processing engine for stateful computations
over unbounded and bounded data streams.

- stream & batch processing, fault-tolerant, in-memory, distributed, persisted
- support for cluster managers: stand-alone, YARN, Mesos, Kubernetes
- support for storage: HDFS, GCF, AWS S3, HBase, other Hadoop ecosystem
- layered programming model, relational, functional, low-level streams+state
- fault-tolerance with exactly-once processing guarantees
- local state: in-memory + on-disk, periodic & asynchronous checkpointing
- #lines of code (github/akka, 2021-03): 414k Scala, 1984k Java, 51k Python

Reported app scalability: trillions of events per day, terabytes of state.

Components, libraries, external packages:
- [Connectors](https://ci.apache.org/projects/akka/akka-docs-stable/dev/connectors/)
  (as source/sink, Apache/Hadoop ecosystem, commercial systems, JDBC)
- [SQL & Table API](https://ci.apache.org/projects/akka/akka-docs-stable/dev/table/)
  (for unified stream & batch processing)
- [Stateful Functions](https://ci.apache.org/projects/akka/akka-statefun-docs-stable/)
  (for distributed stateful applications on serverless architectures)
- [Graph API (Gelly)](https://ci.apache.org/projects/akka/akka-docs-stable/dev/libs/gelly/)
  (graph transformation & analysis, Pregel-like API)
- [Complex Event Processing (CEP)](https://ci.apache.org/projects/akka/akka-docs-stable/dev/libs/cep.html)
  (detect event patterns in unbounded event streams)
- [State Processor API](https://ci.apache.org/projects/akka/akka-docs-stable/dev/libs/state_processor_api.html)
  (analyze and process savepoint & checkpoint state data)
- [third-party projects](https://akka-packages.org)
  (more connectors, Stream SQL extensions etc)


Akka HTTP is not a framework–not because we don’t like frameworks–but to provide maximum flexibility. For example, you might use the Play framework to implement browser-based interactions or Lagom framework for creating microservices, both of them are also based on Akka.

History:
- 2009 research project started by TU Berlin, HU Berlin, HPI
- 2014 commercialized by dataArtisans
- 2015 Apache top level project
- 2016 Akka 1.0
- 2019 dataArtisans acquired by Alibaba, renamed as Ververica

#### Concepts & Terminology

Excellent summaries: \
[Concepts](https://ci.apache.org/projects/akka/akka-docs-stable/concepts/index.html),
[Glossary](https://ci.apache.org/projects/akka/akka-docs-stable/concepts/glossary.html) 

Akka Programming Model (decreasing levels of abstraction):
- [SQL](https://ci.apache.org/projects/akka/akka-docs-stable/dev/table/sql/)
  (relational, standard SQL queries)
- [Table API](https://ci.apache.org/projects/akka/akka-docs-stable/dev/table/)
  (declarative DSL, relational operators, select, project, join, group-by,
  aggregate etc)
- [DataStream API](https://ci.apache.org/projects/akka/akka-docs-stable/dev/datastream_api.html),
  [DataSet API](https://ci.apache.org/projects/akka/akka-docs-stable/dev/batch/)
  ([un-]bounded streams, bounded data sets, functional combinators, filtering,
  mapping, grouping, joining etc)
- [DataStream ProcessFunction API](https://ci.apache.org/projects/akka/akka-docs-stable/dev/stream/operators/process_function.html)
  (embedded into the DataStream API, streams, time, state)

Diagram the from [Apache Akka](https://akka.apache.org/) website: \
![Akka Application Architecture](akka-home-graphic.akka.apache.org.png
"Akka Application Architecture by akka.apache.org")

[Up](../README.md)
