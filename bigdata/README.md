# polyglot/bigdata

### Cluster/Cloud/Streaming/Batch - Experiments, Exercises, Notes

_Motivation:_ Writing correct
- _concurrent & scalable_ applications is hard;
- _distributed & resilient & elastic_ applications is extremly hard (*).

(*) _too hard_, according to
[What is Akka?](https://doc.akka.io/docs/akka/2.4/intro/what-is-akka.html).

_History_: Some of these notes and coding exercises originate from a previous
job role of interviewing developers on Spark <= 2.x, Hadoop/Hive <= 2.x.

_TODO:_
- update notes for version [Spark 3.x](https://spark.apache.org/releases/)
- add notes for [Delta Lake 0.x](https://github.com/delta-io/delta/releases)
- add notes for [Akka 2.x](https://akka.io)
- add notes for [Google Dataflow](https://cloud.google.com/dataflow)
- get overview of [Google Cloud products](https://cloud.google.com/products)

#### Spark - Experiments, Exercises, Notes

- [Spark Overview (2.x)](spark/Spark_Notes_0_Overview.md) - notes
- [Spark API (2.x)](spark/Spark_Notes_1_Core_and_SQL_API.md) - notes
- [Spark Core & SQL API](spark/Spark_Notes_2_API_Tips.md) - code gists, tips
- [Ex 1.x Spark](spark/ex_1.q.spark.md) - API exercises in Scala, Java

#### Flink - Notes

- [Flink Overview](flink/Flink_Notes_Overview.md) - notes

#### Hadoop & Co - Notes, Exercises

- [Hadoop Ecosystem](hadoop/Hadoop_Ecosystem_Notes.md) - notes
- [Hadoop 2, HDFS](hadoop/Hadoop2_HDFS_Notes.md) - notes
- [Ex 1.x Hadoop](hadoop/ex_1.q.hadoop_code_explainer.md) - API exercises

#### Local Streaming, Functional Parallelism, Data Formats - Exercises, Notes

- [JSON Processing in Scala, Java et al](local/Notes_JSON.md) - notes
- [YAML](local/Notes_YAML.md) - notes
- [Ex 1.x Local](local/ex_1.q.local.md) - scalable _map-reduce_ in Scala, Java

#### Links

Covered problem/language/library features: \
[tags](tags.md)

Quick links: \
[Spark](spark) :
  [Scala sources](spark/src/main/scala/netropy/polyglot/bigdata/spark),
  [Java sources](spark/src/main/java/netropy/polyglot/bigdata/spark),
  [Scala tests](spark/src/test/scala/netropy/polyglot/bigdata/spark),
  [Java tests](spark/src/test/java/netropy/polyglot/bigdata/spark) \
[Hadoop](hadoop) :
  [Java sources](hadoop/src/main/java/netropy/polyglot/bigdata/hadoop) \
[Local](local) :
  [Scala sources](local/src/main/scala/netropy/polyglot/bigdata/local),
  [Java sources](local/src/main/java/netropy/polyglot/bigdata/local),
  [Scala tests](local/src/test/scala/netropy/polyglot/bigdata/local),
  [Java tests](local/src/test/java/netropy/polyglot/bigdata/local)

Used Tools: \
[Maven](https://maven.apache.org)

[Up](../README.md)
