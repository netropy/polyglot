# polyglot/bigdata/flink

### Notes: Flink Overview

___TOC:___ \
[Resources](#resources) \
[Features](#features) \
[Concepts & Terminology](#concepts-terminology)

#### Resources

- [Apache Flink](https://flink.apache.org)
- [Docs](https://ci.apache.org/projects/flink/flink-docs-stable/),
  [Local Installation](https://ci.apache.org/projects/flink/flink-docs-stable/try-flink/local_installation.html),
  [Training, First Steps](https://ci.apache.org/projects/flink/flink-docs-stable/learn-flink/)
- [Scala/Java/Python API Docs](https://ci.apache.org/projects/flink/flink-docs-stable/api/)
  (Scaladoc page inop?)
- githubs:
  [Apache Flink](https://github.com/apache/flink),
  [Ververica](https://github.com/ververica),
  [dataArtisans](https://github.com/dataArtisans)

#### Features

Apache Flink is a distributed data processing engine for stateful computations
over unbounded and bounded data streams.

- stream & batch processing, fault-tolerant, in-memory, distributed, persisted
- support for cluster managers: stand-alone, YARN, Mesos, Kubernetes
- support for storage: HDFS, GCF, AWS S3, HBase, other Hadoop ecosystem
- layered programming model, relational, functional, low-level streams+state
- fault-tolerance with exactly-once processing guarantees
- local state: in-memory + on-disk, periodic & asynchronous checkpointing
- #lines of code (github/flink, 2021-03): 414k Scala, 1984k Java, 51k Python

Reported app scalability: trillions of events per day, terabytes of state.

Components, libraries, external packages:
- [Connectors](https://ci.apache.org/projects/flink/flink-docs-stable/dev/connectors/)
  (as source/sink, Apache/Hadoop ecosystem, commercial systems, JDBC)
- [SQL & Table API](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/)
  (for unified stream & batch processing)
- [Stateful Functions](https://ci.apache.org/projects/flink/flink-statefun-docs-stable/)
  (for distributed stateful applications on serverless architectures)
- [Graph API (Gelly)](https://ci.apache.org/projects/flink/flink-docs-stable/dev/libs/gelly/)
  (graph transformation & analysis, Pregel-like API)
- [Complex Event Processing (CEP)](https://ci.apache.org/projects/flink/flink-docs-stable/dev/libs/cep.html)
  (detect event patterns in unbounded event streams)
- [State Processor API](https://ci.apache.org/projects/flink/flink-docs-stable/dev/libs/state_processor_api.html)
  (analyze and process savepoint & checkpoint state data)
- [third-party projects](https://flink-packages.org)
  (more connectors, Stream SQL extensions etc)

History:
- 2009 research project started by TU Berlin, HU Berlin, HPI
- 2014 commercialized by dataArtisans
- 2015 Apache top level project
- 2016 Flink 1.0
- 2019 dataArtisans acquired by Alibaba, renamed as Ververica

#### Concepts & Terminology

Excellent summaries: \
[Concepts](https://ci.apache.org/projects/flink/flink-docs-stable/concepts/index.html),
[Glossary](https://ci.apache.org/projects/flink/flink-docs-stable/concepts/glossary.html) 

Flink Programming Model (decreasing levels of abstraction):
- [SQL](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/sql/)
  (relational, standard SQL queries)
- [Table API](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/)
  (declarative DSL, relational operators, select, project, join, group-by,
  aggregate etc)
- [DataStream API](https://ci.apache.org/projects/flink/flink-docs-stable/dev/datastream_api.html),
  [DataSet API](https://ci.apache.org/projects/flink/flink-docs-stable/dev/batch/)
  ([un-]bounded streams, bounded data sets, functional combinators, filtering,
  mapping, grouping, joining etc)
- [DataStream ProcessFunction API](https://ci.apache.org/projects/flink/flink-docs-stable/dev/stream/operators/process_function.html)
  (embedded into the DataStream API, streams, time, state)

Diagram the from [Apache Flink](https://flink.apache.org/) website: \
![Flink Application Architecture](flink-home-graphic.flink.apache.org.png
"Flink Application Architecture by flink.apache.org")

[Up](../README.md)
