# polyglot/bigdata/hadoop

#### Notes: Hadoop 2, HDFS

___Hadoop 2.x with YARN:___
- supports diverse workloads vs Hadoop 1.x
- interactive queries over large data with Hive on Tez
- realtime data processing with Apache Storm
- scalable NoSQL datastore like HBase
- in-memory datastore like Spark
- high-availability (ZooKeeper)
- HDFS 2
- Hadoop 1.x: processing large amount of data in batch
  - single use system, batch apps, Hive, Pig, Cascading, HDFS 1
  - only Java processing

___Concepts and Terminology:___
- Hadoop Cluster = set of machines (nodes) running _MapReduce_ and _HDFS_
  - though, alternative file systems can be used: FTP, MapR
- _JobTracker_: clients submit MR jobs to the JobTracker
  - resides on a master node (1 per cluster), assigns MR tasks to other nodes
- _TaskTracker_: receives MR tasks from & reports progress back to JobTracker
  - runs on each node, responsible for instantiating MR tasks
  - heartbeat signal between JobTracker and TaskTracker
- _Job_: a program with the ability of complete execution of MR over a dataset
- _Task_: the execution of a single Mapper or Reducer over a slice of data,
  - _#task attempts >= #completed tasks_ due to failure, speculative execution
- _Driver_: code running on the client machine, configures and submits the job

___MapReduce Processing Model:___
- a clean abstraction for programmers
- a framework for distributed and parallel data transformations
- job typically consists of phases: _Map -> Sort -> Shuffle -> Reduce_
- _Map[per]_: code reading, transforming, sorting and filtering of data
  - attempted to run on nodes which hold data portion locally
  - reads a key/value pair, outputs zero or more key/value pairs: \
    _map(in_key, in_value) -> (inter_key, inter_value) list_
- _Shuffle and Sort_ phase: sorting and transfer of map outputs to the Reducer
  - starts for slow running mappers a new mapper instance on separate node,
    takes first result, and kills remaining instances (speculative execution)
  - combines intermediate values into a list per intermediate key
  - passes the intermediate keys+value lists in sorted key order to Reducers
- _Reducer_: code combining and aggregating the output from the Mappers
  - run after _map_ and _shuffle and sort_ phases
  - combines tuples into smaller set of tuples
  - job configuration specifies whether single or multiple Reducers
- _Combiner_: optional mini­reduce pass on the output from a local Mapper
  - can reduce the network traffic by pre-aggregating data before sending
- _Partitioner_: controls the partitioning of the keys of the map-outputs
  - the key is used to derive the partition, typically by a hash function
  - the total number of partitions is the same as the number of reduce tasks
  - hence, controls to which reduce task a record is sent
- developer is expected to at least provide Mapper, Reducer, and Driver code

___Technicalities:___
- _InputSplit_: mappers are handed chunks of the input files (split by size)
- _SequenceFileInputFormat_
  - compressed binary file format to read files in sequence
  - for passing the output data of one MR job as input to another MR job
- _TextInputFormat_:
  - each line is a record, value is content, key is the byte offset of line
- old Hadoop API (0.20): _org.apache.hadoop.mapred_
- new Hadoop API (1.X or 2.X): _org.apache.hadoop.mapreduce_
- old + new API: key/value pairs are _pushed_ to the mapper and reducer
- new API: also supports _pull_ style of iteration
  - mapper or reducer can pull records from within map() or reduce()
  - useful for processing records in batches

___Hadoop Distributed File System (HDFS):___
- distributed, scalable, rack-aware, portable file-system written in Java
- runs on top of the file systems of the underlying OSs
- scales typically to 10x PB
- stores large files, typically GB to TB, across multiple machines
- designed for mostly immutable files, rather than for concurrent writes
  - MapR: alternative, full random-access read/write file system
- HDFS cluster: typically a single NameNode plus multiple DataNodes
- _NameNode_ is single point for storage and management of metadata
  - hosts the file system index for HDFS
  - is critical, redundancy (and backup/failover) options are available
  - can become performance-bottleneck (especially large number of small files)
  - heartbeat signal between NameNode and DataNodes
- _DataNodes_ serve up blocks of data via a specific network block protocol
  - reliability by data replication across multiple hosts
  - default replication value = 3 (two on same rack, one on different rack)
  - _DataNodes_ rebalance data, move copies around
- HDFS Federation: multiple namespaces served by separate namenodes

___HDFS File Access:___
- `hadoop`, `hdfs` CLIs: copy files _local <-> HDFS_
- through the native Java API, the Thrift API with language bindings
- through the HDFS-UI webapp over HTTP
- through 3rd-party network client libraries, file system bridges
- linux, unix: can be mounted as FUSE (_file://_ URL? loss of locality?)

___Typical Hadoop Cluster Configurations:___
- prereq: ssh is set up on all nodes in the cluster
- small Hadoop cluster: single _master node_, multiple _worker nodes_
- _master node_ typically runs a JobTracker, TaskTracker, NameNode and
  DataNode
- _worker node_ typically acts as both, a DataNode and TaskTracker, although:
  - _storage node_: can have data-only worker nodes
  - _compute node_: can have compute-only worker nodes
- larger Hadoop cluster: primary NameNode, secondary NameNode
  - actually, 2nd saves snapshots, not really automatic failover behaviour

___References:___ \
https://hadoop.apache.org/docs/r1.2.1/mapred_tutorial.html \
https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html \
https://hadoop.apache.org/docs/r2.7.2/hadoop-project-dist/hadoop-hdfs/HdfsDesign.html \
https://www.cloudera.com/content/cloudera/en/documentation/hadoop-tutorial/CDH5/Hadoop-Tutorial.html \
https://hadoopbeforestarting.blogspot.com/2012/12/difference-between-hadoop-old-api-and.html \
https://www.geeksforgeeks.org/hadoop-ecosystem/ \
https://en.wikipedia.org/wiki/Apache_Hadoop

[Up](../README.md)
