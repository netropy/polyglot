# polyglot/bigdata/hadoop

#### Notes: Hadoop Ecosystem

___Apache Hadoop Ecosystem:___
- Hadoop: batch-processing platform for the petabyte scale
- HDFS: distributed file system with high-throughput access
- YARN: framework ("Cluster OS") for job scheduling and resource management
- MapReduce: YARN-based system for parallel processing of large data sets
- Tez: newer YARN-based data-flow programming framework generalizing MapReduce
- Hive: data warehouse providing SQL data query via Tez, Spark, MapReduce
- Pig: high-level, data-flow language and parallel computation framework
- Solr, Lucene: distributed searching, indexing, and load-balanced querying
- HBase: distributed, columnar/key-value NoSQL database for large tables
- Cassandra: scalable, fault-tolerant multi-master database
- Ozone: scalable, redundant, and distributed object store for Hadoop
- Mahout: scalable machine learning and data mining library
- Submarine: unified platform for machine learning and deep learning workloads
- Zookeeper: fault-tolerant coordination service for distributed applications
- Chukwa: data collection system for managing large distributed systems
- Flume: distributed service for collecting, aggregating, and moving log data
- Sqoop: transferring bulk data between Hadoop and structured datastores
- Avro: row-oriented RPC and data serialization framework
- Oozie: workflow scheduler system to manage Hadoop jobs
- Ambari: web-based tool for provisioning, managing of Hadoop clusters
- Zeppelin: web-based notebook for interactive data analytics with SQL, Scala
- _Spark_: fast, unified analytics engine for large-scale data processing

OLAP Processing – Cubes on Hadoop

Apache Kylin:
•  Distributed analytics engine that provides SQL and OLAP on Hadoop
Hadoop YARN – Data Operating System


___Spark vs Rest: no longer a contest?___

___References:___ \
https://hadoop.apache.org \
https://tez.apache.org \
https://hive.apache.org \
https://pig.apache.org \
https://en.wikipedia.org/wiki/Apache_Hadoop \
https://www.geeksforgeeks.org/hadoop-ecosystem/ \
https://www.xplenty.com/blog/apache-spark-vs-tez-comparison \
https://spark.apache.org \
https://databricks.com

[Up](../README.md)
