package netropy.polyglot.bigdata.hadoop;

import java.io.IOException;
import java.util.Arrays;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/** A Hadoop M/R job consists of: the Mapper, the Reducer, the Driver code. */
public class Ex_1_3_Hadoop2_WordCount {

    /**
     * Tokenizes a line, mapping each word to a `1` value.
     *
     * Comparing multiple Java code variants here:
     *
     * map_v0(): parses a line using StringTokenizer + while(){}
     * + simple enough
     * - StringTokenizer labeled as a "legacy" class (not deprecated yet)
     * - happens to use instance variable for holding current word
     *
     * map_v1(): parses a line via String.split("\\s") as List
     * - more complex API with java.util.regex.Pattern
     * - must skip empty Strings, even with split("\\s+") or trim()
     * + uses local variable for holding current word in iteration
     *
     * map_v2(): parses a line via String.split("\\s") as Stream
     * + can now filter(), forEach() with lambda
     * - checked exceptions a problem with java.util.function interfaces
     * + uses no variable for holding current word in iteration
     */
    public static class TokenizerMapper
        extends Mapper<Object, Text, Text, IntWritable> {

        // mutable value but used as a constant here
        static private final IntWritable one = new IntWritable(1);

        // mutable value, often pulled out of map() to reuse the instance
        private final Text word = new Text();

        public void map(
            Object key, Text value, Context context)
            throws IOException, InterruptedException {

            map_v0(key, value, context);  // select code variant v0..2
        }

        public void map_v0(
            Object key, Text value, Context context)
            throws IOException, InterruptedException {

            // ugly: StringTokenizer is legacy API
            final StringTokenizer str = new StringTokenizer(value.toString());
            while (str.hasMoreTokens()) {
                word.set(str.nextToken());
                context.write(word, one);
            }

        }

        public void map_v1(
            Object key, Text value, Context context)
            throws IOException, InterruptedException {

            // better: use a local variable outside of loop
            final Text word = new Text();

            // ugly: tokenizing by split("\\s+")... may yield empty Strings
            for (String w : Arrays.asList(value.toString().split("\\s"))) {
                // ugly: List<E> does not offer filter()
                if (!w.isEmpty()) {
                    word.set(w);
                    context.write(word, one);
                }
            }
        }

        public void map_v2(
            Object key, Text value, Context context)
            throws IOException, InterruptedException {

            Arrays.stream(value.toString().split("\\s"))
                .filter(w -> !w.isEmpty())
                .forEach(w -> {
                        // ugly: Consumer<T> doesn't support checked exceptions
                        try {
                            // ok: use a local variable inside of loop
                            context.write(new Text(w), one);
                        } catch(IOException e) {
                            throw new RuntimeException(e);
                        } catch(InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }
    }

    /**
     * Reduces the values per key by sum.
     *
     * Comparing multiple Java code variants here:
     *
     * reduce_v0(): uses external iteration (for/while)
     * - happens to use instance variable for holding final result
     * - cannot do sum as expression or at least using forEach()
     *
     * reduce_v1(): uses internal iteration (forEach) with lambda
     * - uses instance variable for holding partial int sum
     * - fragile (must not forget to reset to 0), updated by side-effect
     *
     * reduce_v2(): uses internal iteration (forEach) with lambda
     * + uses local variable for holding IntWritable sum (effectively final)
     * - lambda calls holder's get()/set() for each sum update
     */
    public static class IntSumReducer
        extends Reducer<Text, IntWritable, Text, IntWritable> {

        // mutable value, often pulled out of reduce() to reuse the instance
        private final IntWritable result = new IntWritable();

        public void reduce(
            Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {

            reduce_v0(key, values, context);  // select code variant v0..2
        }

        public void reduce_v0(
            Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {

            int sum = 0;
            // ugly: cannot sum as expression or by forEach() with side-effect
            for (IntWritable v : values) {
                sum += v.get();
            }
            result.set(sum);
            context.write(key, result);
        }

        // ugly: need instance variable just for purpose of int sum from lambda
        private int sum = 0;
        public void reduce_v1(
            Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {

            sum = 0;
            values.forEach(v -> sum += v.get());
            result.set(sum);
            context.write(key, result);
        }

        public void reduce_v2(
            Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {

            final IntWritable sum = new IntWritable(0);
            // ugly: instance get+set just for purpose of int sum
            values.forEach(v -> sum.set(sum.get() + v.get()));
            context.write(key, sum);
        }
    }

    /**
     * Configures and submits the M/R job.
     *
     * This Driver code runs on the client machine.
     */
    public static void main(String[] args)
        throws Exception {

        // ugly: lots of boilerplate code for config
        final Configuration conf = new Configuration();
        final Job job = Job.getInstance(conf, "hadoop2 wordcount");
        job.setJarByClass(Ex_1_3_Hadoop2_WordCount.class);
        job.setMapperClass(TokenizerMapper.class);
        // optimization: combine (k,v) locally before global shuffle/reduce
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
