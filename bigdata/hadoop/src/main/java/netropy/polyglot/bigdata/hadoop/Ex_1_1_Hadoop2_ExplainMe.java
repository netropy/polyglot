package netropy.polyglot.bigdata.hadoop;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Ex_1_1_Hadoop2_ExplainMe {

    public static class MyMapper
        extends Mapper<Object, Text, Text, IntWritable> {

        private static IntWritable anIntHolder = new IntWritable(1);
        private Text aTextHolder = new Text();

        public void map(
            Object key, Text value, Context context)
            throws IOException, InterruptedException {

            for (String w : Arrays.asList(value.toString().split("\\s"))) {
                if (!w.isEmpty()) {
                    aTextHolder.set(w);
                    context.write(aTextHolder, anIntHolder);
                }
            }
        }
    }

    public static class MyReducer
        extends Reducer<Text, IntWritable, Text, IntWritable> {

        private IntWritable anIntHolder = new IntWritable();

        public void reduce(
            Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {

            int anIntValue = 0;
            for (IntWritable v : values) {
                anIntValue += v.get();
            }
            anIntHolder.set(anIntValue);
            context.write(key, anIntHolder);
        }
    }

    public static void main(String[] args)
        throws Exception {

        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "hadoop2 explainme");
        job.setJarByClass(Ex_1_1_Hadoop2_ExplainMe.class);
        job.setMapperClass(MyMapper.class);
        job.setCombinerClass(MyReducer.class);
        job.setReducerClass(MyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
