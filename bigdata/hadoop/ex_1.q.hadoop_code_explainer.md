# polyglot/bigdata/hadoop

#### 1.1 Explain this (slightly) obfuscated Hadoop (2.x) program.

This Hadoop2 program just happens to lack meaningful names or code comments: \
[ExplainMe](src/main/java/netropy/polyglot/bigdata/hadoop/Ex_1_1_Hadoop2_ExplainMe.java)

- What does the program do?
- Explain the building blocks.

[Remarks](ex_1.r.hadoop_code_explainer.md)

#### 1.2 Show how to run the Ex 1.1 Hadoop program for a quick test.

[Remarks](ex_1_2.r.hadoop_code_quick_test.md)

#### 1.3 Improve the Ex 1.1 Hadoop code.

- Give a quick code review for comments/questions.
- Change the code for meaningful names, add code comments.
- Discuss code variants.

Improved code: \
[Java source](src/main/java/netropy/polyglot/bigdata/hadoop/Ex_1_3_Hadoop2_WordCount.java)

[Up](../README.md)
