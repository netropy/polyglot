# polyglot/c

#### 2 C's Type System: Classification

C's type system has grown surprisingly complex since (compared to other
languages).  The construction of _derived types_ over a rich set of _basic
types_ combined with _type qualifiers_ gives rise to an extensive __type
algebra__.

In addition, technical groupings of types are in use under different, not
necessarily disjoint criteria; for example: _character_ and _integer types_,
_object_ vs _function_ types etc.

___Task:___

Recount C's type system and common groupings of types as per
[cppreference.com](https://en.cppreference.com/w/c/language/type).

[Remarks](ex_2.r.c_types_classification.md)

[Up](./README.md)
