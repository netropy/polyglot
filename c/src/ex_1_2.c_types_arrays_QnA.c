// $ gcc--std=c11 -Wno-unused ex_1_2_c_types_arrays_QnA.c
// $ g++ --std=c++11 -Wno-unused ex_1_2_c_types_arrays_QnA.c

int ex_1_2_c_types_arrays_QnA() {

  // Q: Declare an array of 3 int:
  {
    // A:
    int a[3];
  }

  // Q: Declare an array of 3 pointers to int:
  {
    // A:
    int *a[3];
  }

  // Q: Declare a pointer to array of 3 int:
  {
    // A:
    int (*a)[3];
  }

  // Q: Declare a pointer to array of 3 pointers to int:
  {
    // A:
    int *(*a)[3];
    int *((*b)[3]);
    a = b;  // same!
  }

  // Q: declare a pointer to array of int unknown length:
  {
    // A:
    int (*a)[];
    sizeof(int (*)[]);  // can use type directly
  }

  return 0;
}
