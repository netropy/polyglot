// $ gcc --std=c11 -Wno-unused ex_1_1_c_types_basics_QnA.c
// $ g++ --std=c++11 -Wno-unused ex_1_1_c_types_basics_QnA.c

#include <stdbool.h>
#include <complex.h>

int ex_1_1_c_types_basics_QnA() {

  // Q: Explain the types of the variables:
  {
    int** a, b;

    // A: a = pointer to pointer to int, b = int
  }

  // Q: Declare a pointer to pointer to pointer to double
  {
    // A: declare a variable or pass the type to sizeof
    double*** a;
    sizeof(double***);  // can use type directly
  }

  // Q: Which of these variables have the same type?
  {
    int a = 0;
    typedef int INT;
    INT b = 0;
    double c = 0;

    // A: INT is just a type alias to int, double is different type
  }

  // Q: Which of these statements is legal C, C++ code?
  {
    bool a = true;  // C: per #include <stdbool.h>
    int b = 1;
    long c = 1L;
    float d = 1.1F;
    double e = 1.1;
    long double f = 1.1L;
    double complex g = 1.1 + 2.2I;  // not C++, C: per #include <complex.h>
    a = b;
    b = c;
    c = d;
    d = e;
    e = f;
    f = g;

    // A: C:   all assignments
    //    C is a weakly typed language
    //
    // A: C++: all assignments except for the last `f = g`
    //    '_Complex double' to 'long double' is not permitted in C++
    //    C++ inherits some but not all of the implicit type conversions

    // C: stdlib support for `bool` per #include <stdbool.h>, otherwise
    //    _Bool a = 1;

    // C: stdlib support for `complex` per #include <complex.h>, otherwise
    //    double _Complex e = 1.1 + 2.2I;

    // C++: stdlib support for `std::complex` per #include <complex>, otherwise
    //    double _Complex g = 1.1 + 2.2I;
    //    using namespace std::complex_literals;  // since C++17
    //    std::complex<double> h = 1.1 + 2.2i;    // per ""i literals
  }

  // Q: Which of these variables have the same type?
  {
    int a = 0;
    const int b = 0;
    volatile int c = 0;
    const volatile int d = 0;
    volatile const int e = 0;

    // A: variables d and e
    //    the const/volatile modifiers yield a different type
  }

  // Q: What types have a, b?
  {
    int a = 0;
    static int b = 0;

    // A: same type: int, difference is the storage class of the objects
  }

  // Q: Explain the types of a, ..., e; which ones are different?
  {
    const int * a = 0;
    int const * b = 0;
    int * const c = 0;
    const int * const d = 0;
    int const * const e = 0;

    // A: pointee is an int
    {
      const int * a = 0;       // pointee is const
      int const * b = 0;       // pointee is const
      int * const c = 0;       // pointer is const
      const int * const d = 0; // pointer and pointee are const
      int const * const e = 0; // pointer and pointee are const
    }
  }

  // Q: Declare a const pointer to a const pointer to const int:
  {
    // A:
    const int * const * const a = 0;
    sizeof(const int * const * const);  // can use type directly

    // Q: Which form do you prefer, b) or c)?
    const int * const * const b = 0;
    int const * const * const c = 0;

    // A: form c), can always append `const` as a suffix
  }

  return 0;
}
