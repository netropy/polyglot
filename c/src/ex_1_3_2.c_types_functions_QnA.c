// $ gcc --std=c11 -Wno-unused ex_1_3_2_c_types_functions_QnA.c
// $ g++ --std=c++11 -Wno-unused ex_1_3_2_c_types_functions_QnA.c

int ex_1_3_2_c_types_functions_QnA() {

  // Q: Explain this type (a real-world use case):
  {
    // #include <signal.h>
    void (*signal(int sig, void (*func)(int)))(int);

    // A: declares a function 'signal' that:
    //    - takes an int ('sig')
    //      and a pointer to a void function ('func') taking an int
    //    - returns a pointer to a void function taking an int
    //      (the previous value)
  }

  return 0;
}
