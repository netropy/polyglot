// $ gcc --std=c11 -Wno-unused ex_1_4_c_types_functions_arrays_QnA.c
// $ g++ --std=c++11 -Wno-unused ex_1_4_c_types_functions_arrays_QnA.c

int ex_1_4_c_types_functions_arrays_QnA() {

  // Q: Declare a function taking no argument and returning an array of 3 int
  {
    // A: error: function cannot return array type 'int [3]'
    //    int a()[3];
    //    int a()[];
    //    error: star modifier used outside of function prototype
    //    int a()[*];

    // A: must return a pointer
    int *a();      // could be confused with C++ (old) initializer syntax
    int *b(void);  // preferred
    int *(c());
  }

  // Q: _Define_ a variable of type:
  //    array of 3 pointers to functions taking an int and returning int
  {
    // A: both are correct
    int (*a[3])(int) = { 0 };
    int (*b[3])(int) = { 0, 0, 0 };
  }

  // Q: Declare a variable of type:
  //    pointer to function taking an array of 3 int and returning int
  {
    int (*a)(int[3]);
  }

  // Q: What (other) syntax variants exists for declaring this variable?
  {
    // A: these patterns are all equivalent, since arrays 'decay' to pointer
    int (*a)(int[3]);
    int (*b)(int[]);
    int (*c)(int[*]); // star modifier = pointer to Variable Length Array
    int (*d)(int*);

    // Q: Show that array arguments decay to pointer types
    // A:
    int i[1] = { 1 } ;
    (*a)(i);
    (*b)(i);
    (*c)(i);
    (*d)(i);
  }

  // Q: Declare a variable of type:
  //    pointer to function taking and returning an array of 3 int
  {
    int *(*a)(int[3]);  // asymmetric, yet conveys more information
    int *(*b)(int*);
  }

  return 0;
}
