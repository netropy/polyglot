// $ gcc --std=c11 -Wno-unused ex_1_5_c_types_structs_QnA.c
// $ g++ --std=c++11 -Wno-unused ex_1_5_c_types_structs_QnA.c

int ex_1_5_c_types_structs_QnA() {

  // Q: Would this code block compile in C, C++?  Explain each line.
  {
    struct A {} a;       // defines a struct tag with a variable
    typedef struct A A;  // defines a type alias
    struct A b;          // defines struct variable
    A c;                 // defines struct variable
    a = b = c;           // copy-assignments

    // A: yes, legal C, C++; struct types have their own namespace in C
  }

  // Q: Would any of the code lines break C, C++ compile?  Explain.
  {
    struct A {} a0, a1;
    struct B {} b0, b1;
    typedef struct A C;
    C c0, c1;
    struct D { long b; long c; } d0, d1;
    a0 = a1;
    //b0 = a1;  // structurally equal but different types
    c0 = c1;
    c0 = a1;    // same type
    d0 = d1;
    //d0 = a1;  // structurally incompatible, different types
  }

  // Q: Would this block compile in C, C++?  Explain.
  {
    typedef struct A {} A;
    struct B { long b; long c; };
    A f0(A a);
    struct B f1(struct B b);

    // A: yes, functions can take and return structs by value (copies)
  }

  return 0;
}
