// $ gcc --std=c11 -Wno-unused ex_1_3_1_c_types_functions_QnA.c
// $ g++ --std=c++11 -Wno-unused ex_1_3_1_c_types_functions_QnA.c

int ex_1_3_1_c_types_functions_QnA() {

  // Q: Explain these declarations; what is their type in C, C++?
  {
    void f3();
    void f4(void);

    // A: all legal, they declare function names: f3, f4

    // Q: Do f3, f4 have a type? If so what is it?

    // A: f3, f4 are of function type: void(void)
    // A: C++, more strictly than C, separates function from object types
    //   legal C, not C++, invalid application of 'sizeof' to a function type
    //     sizeof(void (void));
    //     sizeof(void ());
    //   not legal C++, '(' for function-style cast or type construction
    //     decltype(void (void));
    //   legal C++: warning: declaration does not declare anything
    //     decltype(void ());
  }

  // Q: Functions can be invoked by their name; is there another way?
  {
    void f3();
    f3();

    // A: Can also declare and invoke pointers to function
    void (*a)() = &f3;
    (*a)();
  }

  // Q: Which of these statements are legal C, C++?
  {
    void f3();
    void (*a)();
    void (*b)(void) = f3;
    a = f3;
    a = &f3;
    b = a;
    (*a)();
    b();

    // A: all of them, implicit conversions defined from/to function pointers
  }

  // Q: Declare variables of type:
  //    - pointer to function taking no arguments returning an int
  //    - pointer to function taking an int argument returning no value
  {
    // A:
    int (*a)();
    void (*b)(int);
  }

  // Q: Declare a variable of type:
  //    pointer to int-returning function
  //      taking pointer to int-returning function
  {
    // A:
    int (*a)(int (*)());
  }

  // Q: Declare a variable of type:
  //    pointer to function taking int
  //      and returning pointer to int function taking int
  {
    // A:
    int (*(*a)(int))(int);
  }

  // Q: Declare a variable of type:
  //    pointer to function taking int
  //      and returning pointer to int-returning function
  //        taking pointer to int-returning function
  {
    // A:
    int (*(*a)(int))(int (*)());
  }

  // Q: Explain this type (a real-world use case):
  {
    // #include <signal.h>
    void (*signal(int sig, void (*func)(int)))(int);

    // A: declares a function 'signal' that:
    //    - takes an int ('sig')
    //      and a pointer to a void function ('func') taking an int
    //    - returns a pointer to a void function taking an int
    //      (the previous value)
  }

  return 0;
}

//void f3(void) {}
//void f4() {}
