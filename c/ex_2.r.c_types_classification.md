# polyglot/c

#### 1.x C Types: Classification

The C type system (simplified):
- `void`
- basic types:
  - `char`
  - signed integer types: `signed char`, `[signed] [short,,long,long long] [int]`
  - unsigned integer types: `_Bool`, `unsigned char`, `unsigned [short,,long,long long] [int]`
  - real floating types: `float`, `double`, `long double`
  - complex (+imaginary) types: `{float,double,long double} _Complex`
- enumerated types
- derived types:
  - array types
  - structure types
  - union types
  - function types
  - pointer types
  - atomic types: `_Atomic` integer and pointer size/difference types

Combinations of _type qualifiers_ (where allowed) with above types:
- `const`, `volatile`, `restrict`

Standard _type aliases_:
- fixed-width integer types: `intN_t`, `uintN_t` et al from `<stdint.h>`
- wide character types: `char{16,32}_t` from `<wchar.h>` (C++ only: `wchar_t`, `char8_t`)
- boolean type: `bool` from `<stdbool.h>` (C23: `true`, `false` of type `_Bool`)
- complex types: `complex`, `imaginary` from `<complex.h>`
- size and pointer difference types: `size_t`, `ptrdiff_t` from `<stddef.h>`
- atomic type aliases: `atomic_XXX` from `<stdatomic.h>`

More _type groups_:
- object types: all types that aren't function types
- character types: char, signed char, unsigned char
- integer types: char, signed + unsigned integer types, enumerated types
- floating types: real floating types, complex types, imaginary types (optional)
- real types: integer types and real floating types
- arithmetic types: integer types and floating types
- scalar types: arithmetic types and pointer types
- aggregate types: array types and structure types
- derived declarator types: array types, function types, and pointer types

___References___:
https://en.cppreference.com/w/c/language/type \
https://en.cppreference.com/w/c/language/arithmetic_types \
https://en.cppreference.com/w/c/types/integer \
https://en.cppreference.com/w/c/numeric/complex \
https://en.cppreference.com/w/c/language/atomic \
https://en.cppreference.com/w/c/types \
https://en.cppreference.com/w/c/header \
https://en.wikipedia.org/wiki/C_data_types
