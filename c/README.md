# polyglot/c

### C Experiments, Exercises, Refcards, Notes

- [Ex 1: C's Type System: Interview Questions & Coding Exercises](ex_1.q.c_types_questions_exercises.md)
- [Ex 2: C's Type System: Classification](ex_2.q.c_types_classification.md)
- [Arithmetic Type Conversions: Ansi C vs K&R C](C_Arithmetic_Type_Conversions.md)

Quick links: \
[C sources with Q&A](src/)

_TODO_: add gradle or cmake build files

[Up](../README.md)
