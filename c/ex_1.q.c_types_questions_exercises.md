# polyglot/c

### C's Type System: Self-developed Interview Questions & Coding Exercises

_Background_: Surprisingly, to many C and C++ developers (and interviewees),
C's type algebra appears to be somewhat uncharted waters...

This set of questions and coding exercises, grown over time, turned out
useful for self-assessment and for interviewing developers on their
- understanding of C and C++ fundamentals
- ability to incorporate and apply newly learned type patterns.

Also see notes on
[C's Type System: Classification](ex_2.q.c_types_classification.md)

#### 1.1 C Types: Basics

```C
  // Q: Explain the types of the variables:
  {
    int** a, b;
  }

  // Q: Declare a pointer to pointer to pointer to double
  {
  }

  // Q: Which of these variables have the same type?
  {
    int a = 0;
    typedef int INT;
    INT b = 0;
    double c = 0;
  }

  // Q: Which of these statements is legal C, C++ code?
  {
    bool a = true;
    int b = 1;
    long c = 1L;
    float d = 1.1F;
    double e = 1.1;
    long double f = 1.1L;
    double _Complex g = 1.1 + 2.2I;
    a = b;
    b = c;
    c = d;
    d = e;
    e = f;
    f = g;
  }

  // Q: Which of these variables have the same type?
  {
    int a = 0;
    const int b = 0;
    volatile int c = 0;
    const volatile int d = 0;
    volatile const int e = 0;
  }

  // Q: What types have a, b?
  {
    int a = 0;
    static int b = 0;
  }

  // Q: Explain the types of a, ..., e; which ones are different?
  {
    const int * a = 0;
    int const * b = 0;
    int * const c = 0;
    const int * const d = 0;
    int const * const e = 0;
  }

  // Q: Declare a const pointer to a const pointer to const int:
  {
  }
}
```

[C source with Q&A](src/ex_1_1.c_types_basics_QnA.c)

#### 1.2 C Types: Arrays

```C
  // Q: Declare an array of 3 int:
  {
  }

  // Q: Declare an array of 3 pointers to int:
  {
  }

  // Q: Declare a pointer to array of 3 int:
  {
  }

  // Q: Declare a pointer to array of 3 pointers to int:
  {
  }

  // Q: declare a pointer to array of int unknown length:
  {
  }
```

[C source with Q&A](src/ex_1_2.c_types_arrays_QnA.c)

#### 1.3 C Types: Functions

```C
  // Q: Explain these declarations; what is their type in C, C++?
  {
    void f3();
    void f4(void);
  }

  // Q: Functions can be invoked by their name; is there another way?
  {
    void f3();
    f3();
  }

  // Q: Which of these statements are legal C, C++?
  {
    void f3();
    void (*a)();
    void (*b)(void) = f3;
    a = f3;
    a = &f3;
    b = a;
    (*a)();
    b();
  }

  // Q: Declare variables of type:
  //    - pointer to function taking no arguments returning an int
  //    - pointer to function taking an int argument returning no value
  {
  }

  // Q: Declare a variable of type:
  //    pointer to int-returning function
  //      taking pointer to int-returning function
  {
  }

  // Q: Declare a variable of type:
  //    pointer to function taking int
  //      and returning pointer to int function taking int
  {
  }

  // Q: Declare a variable of type:
  //    pointer to function taking int
  //      and returning pointer to int-returning function
  //        taking pointer to int-returning function
  {
  }
```

[C source with Q&A](src/ex_1_3_1.c_types_functions_QnA.c)

#### 1.3.2 C Types: Functions, a Real-World Use Case

```C
  // Q: Explain this type (a real-world use case):
  {
    // #include <signal.h>
    void (*signal(int sig, void (*func)(int)))(int);
  }
```

[C source with Q&A](src/ex_1_3_2.c_types_functions_QnA.c)

#### 1.4 C Types: Functions w/ Arrays

```C
  // Q: Declare a function taking no argument and returning an array of 3 int
  {
  }

  // Q: _Define_ a variable of type:
  //    array of 3 pointers to functions taking an int and returning int
  {
  }

  // Q: Declare a variable of type:
  //    pointer to function taking an array of 3 int and returning int
  {
  }

  // Q: What (other) syntax variants exists for declaring this variable?
  {
  }

  // Q: Declare a variable of type:
  //    pointer to function taking and returning an array of 3 int
  {
  }
```

[C source with Q&A](src/ex_1_4.c_types_functions_arrays_QnA.c)

#### 1.5 C Types: Structs

```C
  // Q: Would this code block compile in C, C++?  Explain each line.
  {
    struct A {} a;
    typedef struct A A;
    struct A b;
    A c;
    a = b = c;
  }

  // Q: Would any of the code lines break C, C++ compile?  Explain.
  {
    struct A {} a0, a1;
    struct B {} b0, b1;
    typedef struct A C;
    C c0, c1;
    struct D { long b; long c; } d0, d1;
    a0 = a1;
    b0 = a1;
    c0 = c1;
    c0 = a1;
    d0 = d1;
    d0 = a1;
  }

  // Q: Would this code block compile in C, C++?  Explain.
  {
    typedef struct A {} A;
    struct B { long b; long c; };
    A f0(A a);
    struct B f1(struct B b);
  }
```

[C source with Q&A](src/ex_1_5.c_types_structs_QnA.c)

[Up](./README.md)
