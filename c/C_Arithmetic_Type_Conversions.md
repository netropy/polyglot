# polyglot/c

### C Style Guide: Use of `[un]signed`

_TODO_: move to ../guides/

- avoid mixing `signed` and `unsigned` types
- use type `size_t` (`<stddef.h>`, `<uchar.h>`) for cardinals, array indexing,
  result of `sizeof`, `offsetof` etc
- use `unsigned` types mainly for binary masks, bitfields
- use `signed` types for ordinals
- use explicit type cast or `static_cast` (C++) if necessary

### Arithmetic Type Conversions: Ansi/Iso C vs K&R C

_Example_:
```C
    int i = -2;
    unsigned char uc = 1;

    (i + uc <= 0);
    /* yields:
     * 1 for Ansi C: i + uc == -1
     * 0 for  K&R C: i + uc == UINT_MAX
     *               i + uc = 111...110 + 000...001 = 111...111
	 */
```

___Since Ansi C89, ISO C90: Value Preserving___

The type conversion depends on the actual sizes of the original and promoted
types.

So, as required: convert operands to the nearest floatiest, longest, or signed
type without loosing bits at conversion.

___K&R C: Unsigned Preserving___

When an unsigned type needs to be widened, it is widened to an unsigned type;
when an unsigned type mixes with a signed type, the result is an _unsigned_
type.

_Background_:

K&R's _The C Programming Language (First Edition)_ specified `unsigned` as
exactly one type.

So, we had:
1. promote `{char,short}` -> `int`, `float` -> `double`
2. if any operand `double` -> all `double`
3. if any operand `long` -> all `long`
4. if any operand `unsigned` -> all `unsigned`
5. else both operands are `int` -> all `int`

C compilers soon introduced types `unsigned {char,short[,long]}` but differed
on the promotions rules for mixing unsigned types with others.

___References:___

https://en.cppreference.com/w/c/language/conversion \
https://en.cppreference.com/w/c/types/size_t \
https://en.cppreference.com/w/c/types \
https://northstar-www.dartmouth.edu/doc/solaris-forte/manuals/c/user_guide/tguide.html#670 \
https://docs.oracle.com/cd/E19205-01/819-5265/bjajv/index.html

[Up](./README.md)
