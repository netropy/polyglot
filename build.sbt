name := "polyglot"
organization := "netropy"
description := "Programming exercises in Scala, Java, C++ et al"
version := "1.0.0-SNAPSHOT"
licenses += "MIT License" -> url("https://choosealicense.com/licenses/mit")

// Multi-Project Build - Aggregation

lazy val adsp = (project in file("adsp"))
lazy val adsp_knapsack = (project in file("adsp/knapsack"))
lazy val fp = (project in file("fp"))
lazy val java = (project in file("java"))
lazy val scala = (project in file("scala"))
lazy val testing_junit4 = (project in file("testing/junit4"))
lazy val testing_junit5 = (project in file("testing/junit5"))
lazy val testing_munit = (project in file("testing/munit"))
lazy val testing_munit_scalacheck = (project in file("testing/munit_scalacheck"))
lazy val testing_scalacheck = (project in file("testing/scalacheck"))
lazy val testing_scalatest = (project in file("testing/scalatest"))
lazy val testing_scalatest_scalacheck = (project in file("testing/scalatest_scalacheck"))
lazy val testing_scalatest_scalamock = (project in file("testing/scalatest_scalamock"))

lazy val root = (project in file("."))
  .aggregate(
    adsp,
    adsp_knapsack,
    fp,
    java,
    scala,
    testing_junit4,
    testing_junit5,
    testing_munit,
    testing_munit_scalacheck,
    testing_scalacheck,
    testing_scalatest,
    testing_scalatest_scalacheck,
    testing_scalatest_scalamock
  )

// Multi-Project Build - Common Behaviour
//
// sbt doc: The definitions in .sbt files are not visible in other .sbt files.
// In order to share code between .sbt files, define one or more Scala files
// in the project/ directory of the build root.
//
// https://www.scala-sbt.org/1.x/docs/Multi-Project.html
