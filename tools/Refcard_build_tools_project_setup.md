# polyglot/tools

### Refcards & Tips: Project Setup for sbt, Maven, Gradle

#### Common (Maven) Project Coordinates:

`groupId:artifactId:version` with:
- `groupId`: dot-notated organization, often corresponding to package
  structure,
- `artifactId`: project name, often _kebab-case_, _dot.case_, or _PascalCase_
  (sometimes camelCase, snake_case),
- `version`: alphanumeric, often _major.minor.patch[-SNAPSHOT]_ (with
  x.y.z-SNAPSHOT < x.y.z).

#### Common Default Project Layout:

| directory | content |
|:---|:---|
| `src/main/{java,scala}` | production Java, Scala sources |
| `src/main/resources` | production resources, for example, .xml or .properties files |
| `src/test/{java,scala}` | test Java, Scala sources |
| `src/test/resources` | test resources, for example, .csv test data |
| `src/<sourceSet>/{java,scala,resources}` | [re-]sources for the _sourceSet_, for example, _scala2_12_ |
| `src/it<TestName>/{java,scala,resources}` | [re-]sources for the _integration test_ |

#### Initial Project Setup:

Options:
1. Copy and modify a seed project at hand.
2. Create a project skeleton using the build tool's init/generate function
   (with optional IDE support).
3. Create a project from an IDE (IntelliJ, Eclipse, Visual Studio, Xcode
   etc).

#### Generate a Project: sbt

Create a seed project for Scala2/3 (the entered project name is lower-cased
to the directory name):
```
  sbt new scala/scala3.g8              # Scala 3 "hello", JUnit 4.11
  sbt new scala/scala-seed.g8          # Scala 2 "hello", ScalaTest/AnyFlatSpec
  sbt new scala/scalatest-example.g8   # Scala 2 calc, ScalaTest/AnyFunSuite
  sbt new scala/dotty-cross.g8         # Scala 3/2 stateful cross-compile
```

These templates seem not well-maintained (for example, use of JUnit 4.11).

See:
[sbt new and Templates](https://www.scala-sbt.org/1.x/docs/sbt-new-and-Templates.html),
[build.sbt examples](https://www.scala-sbt.org/1.x/docs/Examples.html),
[giter8 templates](https://github.com/foundweekends/giter8/wiki/giter8-templates)
with configurations for: ScalaTest, Cats, Zio, Akka, Spark, Flink etc.

[sbt cross-building](https://www.scala-sbt.org/1.x/docs/Cross-Build.html),
[sbt-projectmatrix](https://github.com/sbt/sbt-projectmatrix).

#### Generate a Project: Maven

Currently, Maven offers 2789 "archetypes"; still 48 when filtering for
`scala`, 8 for `scala-archetype`, 2 for `scala-archetype-simple`.  Selecting
the reasonable template from the scala-maven-plugin repository `alchim31`:
```
  $ mvn archetype:generate [-DgroupId=com.mycompany.app] [-DartifactId=my-app]
  ...
  2789: remote -> za.co.absa.hyperdrive:component-archetype (-)
  Choose a number or apply filter (format: [groupId:]artifactId, case sensitive
  contains): 1667: : scala-archetype-simple
  Choose archetype:
  1: remote -> net.alchim31.maven:scala-archetype-simple (The maven-scala-plugin is used for compiling/testing/running/documenting scala code in maven.)
...
```

Practically, a small (yet not minimal) pom.xml consists of:
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>...</groupId>
  <artifactId>...</artifactId>
  <version>...</version>
  <name>...</name>
  <description>...</description>

  <properties>...</properties>
  <dependencies>...</dependencies>
  <build><plugins>...</plugins></build>
</project>
```

Also see:
[Scala with Maven](https://docs.scala-lang.org/tutorials/scala-with-maven.html),
[Maven in 5 Minutes](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html),
[Maven Getting Started Guide](https://maven.apache.org/guides/getting-started/index.html),
[Settings Reference](https://maven.apache.org/settings.html).

#### Generate a Project: Gradle

Gradle features a (semi-) automatic conversion from Maven:
[Migrating Builds From Maven](https://docs.gradle.org/current/userguide/migrating_from_maven.html).

Alternatively, run in an _empty_ directory and interactively answer questions:
```
  gradle init
  	Select type of project to generate:
  	  1: basic
  	  2: application
  	  3: library
  	  4: Gradle plugin
    Enter selection (default: basic) [1..4]

  gradle init --type scala-library
    ...
  gradle init --type {cpp,groovy,java,kotlin,scala,swift}-{application,library}
```

Note that `scala-application` is not available; choose `java-application` and
configure classpaths as needed.

A `-library` project might be sufficient, `-application` utilizes the
[Application Plugin](https://docs.gradle.org/current/userguide/application_plugin.html)
to build and verify an application packaged as `jar`.

Gradle has excellent user documentation on how to start a JVM-based project:
[Creating New Gradle Builds](https://guides.gradle.org/creating-new-gradle-builds/),
[Building Scala Libraries](https://guides.gradle.org/building-scala-libraries/),
[Building Java Applications](https://guides.gradle.org/building-java-applications/),
[Consuming JVM Libraries](https://guides.gradle.org/consuming-jvm-libraries/),
[Building Kotlin JVM Libraries](https://guides.gradle.org/building-kotlin-jvm-libraries/),
[Java Plugin](https://docs.gradle.org/current/userguide/java_plugin.html),
[Scala Plugin](https://docs.gradle.org/current/userguide/scala_plugin.html),
[Java Library Plugin](https://docs.gradle.org/current/userguide/java_library_plugin.html),
[Testing in Java & JVM projects](https://docs.gradle.org/current/userguide/java_testing.html),
[Test](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.Test.html),
[Application Plugin](https://docs.gradle.org/current/userguide/application_plugin.html),
[Distribution Plugin](https://docs.gradle.org/current/userguide/distribution_plugin.html).

For other plugins, see:
[Plugin Reference](https://docs.gradle.org/current/userguide/plugin_reference.html) and
[Build Init Plugin](https://docs.gradle.org/current/userguide/build_init_plugin.html).

Gradle also supports non-JVM projects:
[Building C++ projects](https://docs.gradle.org/5.5.1/userguide/building_cpp_projects.html#sec:cpp_supported_tool_chain),
[Building C++ Applications](https://guides.gradle.org/building-cpp-applications/),
[Building and Testing C++ Libraries](https://guides.gradle.org/building-cpp-libraries/),
[C++ Application](https://docs.gradle.org/current/userguide/cpp_application_plugin.html),
[C++ Library](https://docs.gradle.org/current/userguide/cpp_library_plugin.html),
[C++ Unit Test](https://docs.gradle.org/current/userguide/cpp_unit_test_plugin.html),
[Swift Application](https://docs.gradle.org/current/userguide/swift_application_plugin.html),
[Swift Library](https://docs.gradle.org/current/userguide/swift_library_plugin.html).

Gradle can also generate projects ready for IDE import:
[Eclipse](https://docs.gradle.org/current/userguide/eclipse_plugin.html),
[IntelliJ IDEA](https://docs.gradle.org/current/userguide/idea_plugin.html),
[Visual Studio](https://docs.gradle.org/current/userguide/visual_studio_plugin.html),
[Xcode](https://docs.gradle.org/current/userguide/xcode_plugin.html).

Note: Gradle is typically invoked through a local wrapper script that invokes
a fixed version of Gradle (downloading it as necessary), see
[The Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html).
This script can be generated in a project directory or upgraded by:
```
gradle wrapper [--gradle-version x.y.z] [--distribution-type=bin]`
```

[Up](README.md)
