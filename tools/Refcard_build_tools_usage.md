# polyglot/tools

### Refcards & Tips: Usage sbt, Maven, Gradle

#### Help / Info Tasks, Files:

| `sbt` or sbt:..> shell | `mvn` | `gradle` | action, goal, task |
|:---|:---|:---|:---|
| `-h`, `help` | `-h` | `-h`, `help` | CLI usage (not too helpful), general help |
| `tasks`, `help <task>` | `help` or invalid input, `help:describe -Dcmd=<goal>` | `tasks`, `tasks --all`, `--info <task>`, `help --task <task>` | show available tasks or goals, usage info |
| `projects`, `project` | `dependency:tree` | `proj`, `projects` | show (or change) sub-projects |
| `settings`, `inspect <setting>` | `help:all-profiles`, `help:effective-settings` | `prop`, `properties` | show settings |
| `last`, `last <task>`, `set <every|scope|task|project/> traceLevel := 0`, logging levels (`error`, `warn`, `info`, `debug`, `about`), `settings -V`, `inspect tree <task>`, `show <task>`, `inspect <setting>`, `show <setting>`, `session list` | `<task> --scan`, `components`, `dependencies`, `buildEnvironment`, `dependencyInsight`, `--debug <task>` | `-X` or `--debug`, `help:system`, `dependency:analyze`, `help:help` | show additional information |
| `build.sbt`, `local.sbt`, `project/build.properties`, `project/plugins.sbt`, `project/assembly.sbt`, `project/*.scala`, `~/.sbt/1.0/global.sbt`, `~/.sbtrc` | `pom.xml`, `.mvn/maven.config`, `.mvn/jvm.config`, `.mvn/extensions.xml`, `~/.m2/settings.xml` | `build.gradle[.kts]`, `settings.gradle[.kts]`, `master/settings.gradle[.kts]`, `.../settings.gradle[.kts]`, `gradle.properties`, `~/.gradle/...` | project definition files, local/global settings |
| `target/.history`, `project/*/*`, `~/.sbt/1.0/*` | | `.gradle/`, `gradlew[.bat]`, `gradle/`, `~/.gradle` | additional build tool files, cache, wrappers etc |
| `target/` | `target/` | `build/` | default output directory (configurable) |
| `~/.cache/coursier` | `~/.m2/repository/` | `~/.gradle/caches/modules-2/` | default download directory |

#### Clean / Compile / Testing Tasks:

| `sbt` or sbt:..> shell | `mvn` | `gradle` | action, goal, phase, task |
|:---|:---|:---|:---|
| `clean` | `clean` | `clean`, `clean<Task>` | delete build output files |
| `compile`, `<sub-project>/compile` | `compile` | `classes`, `compileJava`, `compileScala` | compile the sources |
| `Test/compile` \* | `test-compile` | `testClasses`, `compileTestJava`, `compileTestScala` | compile the test sources |
| | | `compile<SourceSet>Java`, `compile<SourceSet>Scala`, `<sourceSet>Classes` | compile the named source set |
| `test` | `test` | `cleanTest test` | run all unit tests (prior to packaging) |
| `testOnly <pattern>` or `testOnly -- <testRunnerOptions> <pattern>` (when running as CLI: must quote as `sbt 'testOnly <pattern>'`) | `test -Dsuites='<pattern>'` | `test --tests <pattern>` (or filtering or include/exclude from build script) | run the specified tests |
| `testQuick` | (no such feature) | `test` | run only the tests that need to run or have failed before |
|  (no such feature) | (no such feature) | `cleanTest` | run only the tests that need to run or failed before |
| (no such feature) | `-ff` or `--fail-fast` | `--fail-fast` | fail test task on the first test failure |
| `console`, `Test/console`, `Test/consoleQuick` \* (does [not] force _Test/compile_, does [not] include [test] dependencies) | `scala:console` (does not force _test-compile_) | write a custom `repl` task or configure repl startup script (see blogs) | run the Scala shell with the currently compiled classes plus dependencies |
| `run <args>`, `show discoveredMainClasses` | `scala:run` | `run`, `run --debug-jvm --args="..."` | run a selected/configured main class |
| `runMain <name> <args>` | `scala:run -DmainClass=<name>` | `runWith[Java]Exec` after creating a task `runWith[Java]Exec(type: [Java]Exec)`, `runWithExecJarOnClassPath`, `runWithExecJarExecutable` | Runs a named main class. |
| `[Test/]scalastyle`, `[Test/]checkstyle`, `[Test/]jcheckStyle` \* (depending upon the used plugin) | `checkstyle::check` (maven-checkstyle-plugin, bind to `test` or other phase) | `check` (also `checkstyleMain`, `checkstyleTest` with the Checkstyle plugin) | run all verification tests (prior to packaging) |

\* sbt test:xyz -- deprecated sbt 0.13 shell syntax; use slash syntax instead: Test/xyz

#### Documentation / Packaging / Verification / Deployment Tasks:

| `sbt` or sbt:..> shell | `mvn` | `gradle` | action, goal, phase, task |
|:---|:---|:---|:---|
| `doc` (`javadoc`, `scaladoc` seem unavailable with dotty?) (html placed into local `_site/` or `target/` directory) | `site` | `javaDoc`, `scalaDoc` | generate the API and site documentation |
| `package` | `package` | `jar`, `assemble`, `build`, `buildNeeded`, `buildDependents` | produce the main artifact, such as a jar |
| write a custom `it:test` configuration (no running tests in forked JVMs, though) | `integration-tests`, `verify` | create a source set, configure dependencies & classpaths, create a task to run with clean-up | upload the package into an environment, set up, run, and tear down all integration tests |
| `publishLocal` | `install` | `publish<Name>PublicationTo...`, `publishToMavenLocal` | publish artifacts to a local Docker/Ivy/Maven repository |
| `publish` | `deploy` | `publish` | publish artifacts to a remote repository |

#### Tips: sbt

`sbt` assumes solid familiarity with its elaborate DSL syntax and terminology:
[sbt: methods, types, and values](https://www.scala-sbt.org/1.x/docs/Name-Index.html).

`sbt` has a hefty startup time (_~7s_, here).  Hence, it is much faster to run
commands from an open `sbt:..>` shell (also faster compile):
- Can use <Tab> completion (1x for unambigous completion, 2x for suggestions,
  3x to increase verbosity).
- Triggered execution: prefix `~ <command>` automatically re-executes the
  command whenever one of the source files is modified.
- Multiple commands: `<command1>; <command2>`.
- Changes to `build.sbt` require a `reload` from the shell to be applied.
- The command to quit is `exit` (unlike like the Scala REPL).
- May want to change location of sbt history file from `target/.history` via
  ```
    historyPath := Some(baseDirectory.value / ".history")
  ```

By default, sbt runs all tasks in parallel and within the same JVM.   To only
use a single thread for building:
```
  parallelExecution := false
```

Environment variables: `SBT_OPTS`, `JAVA_OPTS` (past bugs: not always
honored).  For faster compile, try `SBT_OPTS="-server -Xms512M -Xmx2048m"`.

Documentation:
[sbt](https://www.scala-sbt.org),
[sbt Reference Manual](https://www.scala-sbt.org/1.x/docs/),
[Scalastyle - SBT plugin](http://www.scalastyle.org/sbt.html),
[sbt-jcheckstyle](https://index.scala-lang.org/xerial/sbt-jcheckstyle/sbt-jcheckstyle/),
[sbt-checkstyle-plugin](https://github.com/etsy/sbt-checkstyle-plugin),
[So, what's wrong with SBT?](http://www.lihaoyi.com/post/SowhatswrongwithSBT.html)

#### Tips: Maven

Maven's documentation assumes solid familiarity with its elaborate
terminology: _lifecycle_ commands consisting of _phases_, which translate into
_goals_, which are carried out by registered _plugins_, as described in:
[Maven: Introduction to the Build Lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html).

Environment variables: `MAVEN_OPTS`, `JAVA_HOME`; for faster compile, try
`MAVEN_OPTS="-Xms256m -Xmx1024m"`.

Maven performs parallel builds with `-T` option (expect intermingled console
output):
```
mvn -T 4 ...     # 4 threads
mvn -T 1C ...    # 1 thread per cpu core
mvn -T 1.5C ...  # 1.5 thread per cpu core
```
Basic Java/Scala compile/test plugins seem to work under parallel execution.
However, other plugins/libraries are reported to have incompatibilities.

Maven supports multi-project builds via Aggregator Project POMs (Parent
POMs), which can be used for:
- Project Aggregation: Parent project can build a group of modules.  The
  parent project's pom.xml lists its child projects in a `<modules>` section.
- Project Inheritance: Modules take settings from parent project.  The child
  project's pom.xml specifies its `<parent>` (including `<relativePath>`).

To work with individual modules from parent dir:
```
mvn ... -pl <moduleX>,<moduleY>
# -pl|--projects <modules by relative path or [groupId]:artifactId>
```

For a (local) dependency between submodules, the dependent's pom.xml must
declare a `<dependency>` upon the provider module (including `<version>`).
Then, work from the parent pom dir (cannot work from dependent dir):
```
$ mvn ... -pl <provider-module>,<dependent-module>
$ mvn ... -pl <dependent-module> -am
$ mvn ... -pl <provider-module> -amd
# -am|--also-make               = also build required modules
# -amd|--also-make-dependents   = also build dependent modules
```

Documentation:
[Maven](https://maven.apache.org),
[Maven Users Centre](https://maven.apache.org/users/),
[Introduction to the POM](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html),
[Maven CLI Options Reference](https://maven.apache.org/ref/current/maven-embedder/cli.html),
[Java Compiler Plugin](https://maven.apache.org/plugins/maven-compiler-plugin/),
[Scala Compiler Plugin](http://davidb.github.io/scala-maven-plugin/),
[Surefire Plugin](https://maven.apache.org/surefire/maven-surefire-plugin/),
[Failsafe Plugin](https://maven.apache.org/surefire/maven-failsafe-plugin/),
[Checkstyle Plugin](https://maven.apache.org/plugins/maven-checkstyle-plugin/usage.html),
[Maven is broken by design](https://blog.ltgt.net/maven-is-broken-by-design),
[Parallel builds in Maven 3](https://cwiki.apache.org/confluence/display/MAVEN/Parallel+builds+in+Maven+3)

#### Tips: Gradle

Gradle assumes familiarity with its relatively small core of terminology, plus
the Groovy or Kotlin DSL:
[What is Gradle?](https://docs.gradle.org/current/userguide/what_is_gradle.html),
[Build Phases](https://docs.gradle.org/current/userguide/build_lifecycle.html),
[Groovy Build Script Primer](https://docs.gradle.org/current/userguide/groovy_build_script_primer.html),
[Kotlin DSL Primer](https://docs.gradle.org/current/userguide/kotlin_dsl.html).

_Task Graphs_ are Directed Acyclic Graphs that configure a set of tasks and
wires them together based on their dependencies.  _Standard Lifecycle Tasks_
don't have any actions but simply aggregate multiple tasks together.  Gradle
always builds the complete dependency graph _before_ any task is executed.
Build scripts configure this dependency graph.  A Settings file defines which
projects are taking part in the multi-project build (hence, optional for
single-project build).

The division between these files reflects Gradle's 3 build phases: 1)
Initialization (which projects take part in the build), 2) Configuration
(execure the build scripts of all projects), 3) Execution (determine and run
those tasks that need to be executed).

Gradle provides bash and zsh tab completion support for tasks, options, and
Gradle properties through gradle-completion, installed separately.

Gradle is typically invoked through the local `gradlew[.bat]` wrapper script:
```
./gradlew --version  # project's installed gradle version`
```
This script can be generated or upgraded by any of:
```
gradle wrapper [--gradle-version x.y.z]     # defaults to gradle's version`
./gradlew wrapper [--gradle-version x.y.z]  # defaults to ./gradlew's version`
```

Gradle starts a daemon process to speedup builds.  (Sometimes, this daemon
has caused problems like preventing the CPU to go into deep sleep modes):
```
./gradlew --status
./gradlew --stop
rm -rf ~/.gradle/daemon/...  # in case of sticky status msg etc
./gradlew --no-daemon ...    # don't start a daemon process
-Dorg.gradle.daemon=false    # in GRADLE_OPTS environment variable
org.gradle.daemon=false      # in $GRADLE_USER_HOME/gradle.properties
```

Gradle can execute tasks in parallel as long as those tasks are in different
projects (naturally, they must be decoupled at execution time, i.e. not modify
shared state):
```
./gradlew --parallel ...
org.gradle.parallel=true     # in $GRADLE_USER_HOME/gradle.properties
```
_Nice:_ The console output is not intermingled.

Other Gradle CLI options:
```
-q|w|i|d, -Dorg.gradle.logging.level=(quiet,warn,lifecycle,info,debug)
-S|--full-stacktrace, -s|--stacktrace
--dry-run
--refresh-dependencies
[task] --continuous   # triggered execution (run test task + dependent tasks)
```

Environment variables (after CLI flags, system properties, gradle properties):
`GRADLE_OPTS`, `GRADLE_USER_HOME`, `JAVA_HOME`.

Documentation:
[Gradle](https://gradle.org),
[Gradle User Manual](https://docs.gradle.org/current/userguide/userguide.html),
[Command-Line Interface](https://docs.gradle.org/current/userguide/command_line_interface.html),
[Java Plugin](https://docs.gradle.org/current/userguide/java_plugin.html),
[Scala Plugin](https://docs.gradle.org/current/userguide/scala_plugin.html),
[Java Library Plugin](https://docs.gradle.org/current/userguide/java_library_plugin.html),
[Testing in Java & JVM projects](https://docs.gradle.org/current/userguide/java_testing.html),
[Test](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.Test.html),
[Application Plugin](https://docs.gradle.org/current/userguide/application_plugin.html),
[Distribution Plugin](https://docs.gradle.org/current/userguide/distribution_plugin.html),
[Plugin Reference](https://docs.gradle.org/current/userguide/plugin_reference.html),
[Gradle Scala REPL](https://chris-martin.org/2015/gradle-scala-repl),
[Scala REPL in Gradle](https://stackoverflow.com/questions/35632085/scala-repl-in-gradle),
[The Gradle Daemon](https://docs.gradle.org/current/userguide/gradle_daemon.html),
[The Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html)

[Up](README.md)
