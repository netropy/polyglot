# polyglot/tools

### Notes: Tools to try out

#### bazel: Google's build tool

<https://github.com/bazelbuild/bazel> \
<https://bazel.build>

Scala rules: \
<https://github.com/bazelbuild/rules_scala> \
<https://github.com/higherkindness/rules_scala>

documentation: \
<https://databricks.com/blog/2019/02/27/speedy-scala-builds-with-bazel-at-databricks.html> \
<https://scalac.io/set-up-bazel-build-tool-for-scala-project>


#### mill: Scala/Java build tool by Li Haoyi

Motivation: "build tools as pure functional programs"
- ok: build tools define Directed Acyclic Graphs: "build graphs"
- hmm: "the simplest way to define a DAG is with a pure functional program"
- ok: named, side-effect-free expressions, factored out common-sub-expressions
- hmm: wouldn't a modelling of builds as _stateful_ processes come to mind
  first?

Features:
- nice: project definitions as Scala scripts: _build.sc_
- parallel task execution (still labaled experimental)
- IDE support through Build Server Protocol (2.0.0), IntelliJ IDEA
- hmm: unusual project layout
  ```
  build.sc
  foo/
      src/
          FileA.java
          FileB.scala
      resources/
          ...
  out/
      foo/
          compile/
          run/
          runBackground/
          launcher/
          jar/
          assembly/
      ...
  ```

Usage examples:
- `mill -h|--help` command-line tool
- `-w|--watch` automatically re-evaluate task when the inputs change
- `-j|--jobs <int>` allow to process N targets in parallel (0 = available
  processors)
- task examples:
  ```
    mill foo.compile               # compile sources into classfiles
    mill foo.run                   # run the main method, if any
    mill foo.jar                   # bundle the classfiles into a jar
    mill foo.assembly              # bundle classfiles and all dependencies into a jar
    mill -i foo.console            # start a Scala console with project
    mill -i foo.repl               # start an Ammonite REPL with project
  ```
- wildcards and brace-expansion examples:
  ```
    mill foo._.compile             # compile all direct sub-modules of foo
    mill foo.__.test               # test for all sub-modules of foo
    mill {foo,bar}.__.testCached   # run testCached for all sub-modules of foo and bar
    mill __.compile + foo.__.test  # run all compile targets and all tests under foo
  ```

<https://github.com/com-lihaoyi/mill>

documentation: \
<https://com-lihaoyi.github.io/mill/mill/Intro_to_Mill.html> \
<https://www.lihaoyi.com/post/MillBetterScalaBuilds.html> \
<https://www.lihaoyi.com/post/BuildToolsasPureFunctionalPrograms.html>

#### scoverage: code coverage tool for scala

Features:
- supports statement and branch coverage
- plugins for scalac, sbt, maven, gradle, jenkins, sonar, Travis CI etc
- sbt: `addSbtPlugin("org.scoverage" % "sbt-scoverage" % <version>)`

<https://github.com/scoverage> \
<https://mvnrepository.com/artifact/org.scoverage> \
<http://scoverage.org> (from 2014)

[Up](README.md)
