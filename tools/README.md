# polyglot/tools

### Refcards, Notes

Refcards & Tips:
- [Project Setup for sbt, Maven, Gradle](Refcard_build_tools_project_setup.md)
- [Usage of sbt, Maven, Gradle](Refcard_build_tools_usage.md)

Notes:
- [tools to try out](Notes_tools_try_out.md) - bazel, mill, scoverage etc
- [../testing](../testing/README.md) - unit test frameworks X build tools

[Up](../README.md)
