# polyglot/math

## 1 Ex: 3-Point Topologies

For a set with 3 elements $`X=\{a,b,c\}`$, there are exactly these
[9 inequavalent topologies][wp:fts] $`\tau\subseteq\mathbb{P}(X)`$ (where
$`\mathbb{P}(X)`$ is the [power set][wp:pws] of $`X`$).

These 3-point topologies provide a simple, rich-enough playground for
trying out basic, topological definitions.

__Note:__ A topology $`\tau_i`$ will be simply referred to as ($`i`$)

| $`\tau_i`$ | $`\tau_i=\ldots`$ | includes | also called |
|:--|:--|:--|:--|
| (0) | $`\{\emptyset,\{a,b,c\}\}`$ | | the [trivial topology][wp:trt] |
| (1) | $`\{\emptyset,\{c\},\{a,b,c\}\}`$ | (0) | |
| (2) | $`\{\emptyset,\{a,b\},\{a,b,c\}\}`$ | (0) | |
| (3) | $`\{\emptyset,\{c\},\{a,b\},\{a,b,c\}\}`$ | (1), (2) | |
| (4) | $`\{\emptyset,\{c\},\{b,c\},\{a,b,c\}\}`$ | (1) | |
| (5) | $`\{\emptyset,\{c\},\{a,c\},\{b,c\},\{a,b,c\}\}`$ | (4) | |
| (6) | $`\{\emptyset,\{a\},\{b\},\{a,b\},\{a,b,c\}\}`$ | (2) | |
| (7) | $`\{\emptyset,\{b\},\{c\},\{a,b\},\{b,c\},\{a,b,c\}\}`$ | (3), (4) | |
| (8) | $`\{\emptyset,\{a\},\{b\},\{c\},\{a,b\},\{a,c\},\{b,c\},\{a,b,c\}\}`$ | (5), (6), (7) | the [discrete topology][wp:dct] |

__Note:__ The topologies $`\tau`$ are [partially ordered][wp:pos] by
_inclusion_ (and form the [lattice][wp:lat] of all subsets of a set).  Here,
we also call a topology $`\tau_i`$ _finer than_ $`\tau_j`$ if it includes
$`\tau_j`$.

### 1.1 Ex: Limit Points, Interiors, Closures etc of Sets

_Motivation:_ Internalize the definitions of _limit points, interior points,_
_adherent points (= points of closure)_ etc; apply them to subsets and
topologies on a small, finite set example to check for patterns.

Expand above table and list for every 3-point topology $`\tau`$ and every
subset $`S\subseteq{X}`$ all its: \
[limit points][wp:lip], [isolated points][wp:isp], [interior points][wp:inp],
[boundary points][wp:bnd], [adherent points][wp:cls], [external points][wp:exp].

Also check for patterns along the two axis _is-finer-than_ (topologies) and
_is-superset_ (sets).

[Solution](ex_1_1.r.sets_of_3point_topologies.md)

#### 1.1.1 List all sets' limit points and isolated points

#### 1.1.2 List all sets' interior points and boundary points

#### 1.1.3 List all sets' adherent and exterior points

### 1.2 Ex: Examples of Points/Sets [Not] Fulfilling the Separation Criteria

_Motivation:_ Develop an intuition for the definitions of the various
_separation criteria._

In the [9 topologies][wp:fts] $`\tau\subseteq\mathbb{P}(X)`$ on the 3-point set
$`X=\{a,b,c\}`$, give examples of points and sets that _do_ or _do not_
fulfill the separation criteria listed in [[wp:sps][]]: \
$`\qquad\text{P}_{0}\Leftarrow\text{P}_{1}\Leftarrow\text{P}_{2}\Leftarrow\text{P}_{2½}\Leftarrow\text{P}_\text{CF}\qquad`$

| where | = ... |
|:--:|:--|
| $`\text{P}_0`$ | points distinguishable |
| $`\text{P}_1`$ | points/sets separated |
| $`\text{P}_2`$ | points/sets separated by neighbourhood |
| $`\text{P}_{2½}`$ | points/sets separated by closed neighbourhood |
| $`\text{P}_\text{CF}`$ | points/sets separated by a continuous function |

[Solution](ex_1_1.r.sets_of_3point_topologies.md)

[em:isp]: https://encyclopediaofmath.org/wiki/Isolated_point

[pw:isp]: https://proofwiki.org/wiki/Definition:Isolated_Point_(Topology)

[wp:aft]: https://en.wikipedia.org/wiki/Axiomatic_foundations_of_topological_spaces
[wp:adp]: https://en.wikipedia.org/wiki/Adherent_point
[wp:bnd]: https://en.wikipedia.org/wiki/Boundary_(topology)
[wp:cls]: https://en.wikipedia.org/wiki/Closure_(topology)
[wp:dct]: https://en.wikipedia.org/wiki/Discrete_space
[wp:exp]: https://en.wikipedia.org/wiki/Interior_(topology)#Exterior_of_a_set
[wp:fts]: https://en.wikipedia.org/wiki/Finite_topological_space#3_points
[wp:inp]: https://en.wikipedia.org/wiki/Interior_(topology)
[wp:isp]: https://en.wikipedia.org/wiki/Isolated_point
[wp:lat]: https://en.wikipedia.org/wiki/Lattice_(order)
[wp:lip]: https://en.wikipedia.org/wiki/Accumulation_point
[wp:pos]: https://en.wikipedia.org/wiki/Partially_ordered_set
[wp:pws]: https://en.wikipedia.org/wiki/Power_set
[wp:sps]: https://en.wikipedia.org/wiki/Separated_sets
[wp:trt]: https://en.wikipedia.org/wiki/Trivial_topology

[Up](./README.md)
