# polyglot/math

### Notes: Point-Set Topology

___TOC:___

___Motivation:___ Summarize basic point-set definitions in Toplogical Spaces
-- such as _neighbourhood, interior, closure,_ or _boundary_ -- which lay the
ground for the most general notions of limits, continuity, and connectedness
in abstract spaces, see [[XXX]].

XXX Motivation:

Continuous functions between topological spaces

Another, more abstract, notion of continuity is the continuity of functions
between topological spaces in which there generally is no formal notion of
distance, as there is in the case of metric spaces.

The definitions and lemmas below link to these sources:
| link format | source |
|:--:|:--|
| [em:...] | [Encyclopedia of Mathematics](https://encyclopediaofmath.org), open access, published by the [European Mathematical Society](https://euro-math-soc.eu) |
| [wm:...] | [Wolfram MathWorld](https://mathworld.wolfram.com/about/), educational resource based on [Wolfram Mathematica](https://www.wolfram.com/mathematica/) |
| [wp:...] | [Wikipedia](https://en.wikipedia.org), the community-edited encyclopedia |

XXX
https://ncatlab.org/nlab/show/Introduction+to+Topology+--+1

#### Overview

_General topology_ or _point-set topology_ is the "low-level language" that
provides the basic set-theoretic definitions and constructions used in
topology.  [[wp:pst][]], [[wm:pst][]], [[wmc:pst][]], [[em:pst][]]

The fundamental concepts are _limits, continuity, compactness,_ and
_connectedness._  Intuitively: \
\- Limits are the points of convergence of sequences of points or subsets. \
\- Continuous functions, take nearby points to nearby points. \
\- Compact sets are those that can be covered by finitely many sets of
   arbitrarily small size. \
\- Connected sets are sets that cannot be divided into two pieces that are
   far apart.

The terms 'convergence', 'nearby', 'arbitrarily small', and 'far apart' can
all be made precise based on the notion of _open sets_ (and other,
equivalent approaches). [[wp:ops][]], [[wm:ops][]], [[em:ops][]]

### 1 Basic Definitions: Topological Space, Neighbourhood, Closure, ...

#### 1.1 Def: Topological Space $`(X, \tau)`$, Open Sets, Closed Sets

A _topological space_ is a geometrical space in which closeness is defined but
cannot necessarily be measured by a numeric distance.  It is the most general
type of a mathematical space that allows for the definition of limits,
continuity, and connectedness. [[wp:tps][]]

There are several, equivalent definitions of a _topology_ as an axiomatic
foundation of topological spaces.  The most common definition is based on
_open sets;_ for some use cases, the dual notion of _closed sets_ is said to
have practical benefits.  Other axiomatizations are based on the notion of
_neighbourhoods,_ a _closure operator,_ or an _interior operator_ et al.
[[wp:aft][]], [[wp:tps][]]

##### Preliminaries: Family of Sets, Indexed Family, Sequence

XXX see Notes_Set_Class_Multiset_Family.md

##### Topological Space

| A _topological space_ $`(X,\tau)`$ | is ... [[wp:tps][]], [[wm:tps][]], [[em:tps][]] |
|:--|:--|
| $`X`$ | a set of elements, typically called _points_ |
| $`\tau\subseteq\mathbb{P}(X)`$ | a collection of subsets of $`X`$, called _topology_ on $`X`$ (where $`\mathbb{P}(X)`$ is the power set of $`X`$) [[wp:pws][]] |
| | also called (when countable): a _family of subsets of_ $`X`$ or a _family of sets over_ $`X`$ [[wp:fms][]] |

For notational convenience, the distinction between a set and a topological
space is often blurred with the unadorned symbol $`X`$ referring to both the
set $`X`$ and $`(X,\tau)`$, depending on the context. [[wp:sst][]]

XXX see Notes_Set_Class_Multiset_Family.md
[wp:pws]: https://en.wikipedia.org/wiki/Power_set
[wp:fms]: https://en.wikipedia.org/wiki/Family_of_sets

##### Definition via Open Sets and Closed Sets

The members of $`\tau`$ are called _open sets_ and must satisfy the following
conditions: [[wp:tps][]], [[wm:tps][]], [[wp:ops][]], [[wm:ops][]]

| axioms/defs | | |
|:--:|:--|:--|
| (1) | $`\emptyset\in\tau\;\text{and}\;X\in\tau`$ | the empty set and $`X`$ are open |
| (2) | for any indexed set $`\;\{T_{i:i\in{I}}\}\subseteq\tau\;\text{then}\;\bigcup_{i\in{I}}T_i\in\tau`$ | any arbitrary (finite or infinite) union of open sets is open |
| (3) | $`T_1\cap\cdots\cap{T_n}\in\tau`$ | any finite intersection of open sets is open |
| def.: | a subset of $`X`$ is _closed_ (in $`(X,\tau)`$) iff $`(X\setminus{C})\in\tau`$ | iff its complement in $`X`$ is open in $`tau`$ [[wp:cds][]], [[em:cds][]] |

Open and closed sets generalize the notion of _open_ and _closed intervals_ in
the real numbers.  In the (usual definition of the) _Euclidean topology_ on
$`\mathbb{R}`$) (a.k.a., the "real line"), the interval $`(0,1)`$ is an open
set and $`[0,1]`$ is a closed set. [[wp:eut][]], [[wp:rea][]], [[wp:ops][]],
[[wp:cds][]], [[wp:itv][]]

Note, though, that "openness" or "closedness" of a set are not properties
determinable from the set itself but with respect to a given topology.

A set might be open, closed, both, or neither. [[wp:ops][]], [[wm:cds][]] For
example, the empty set and $`X`$ itself are both open (by defintion)
and closed (as complements of another). [[wp:pst][]] The half-open interval
$`(0,1]`$ is neither open nor closed. [[wm:ops][]], [[wp:itv][]]

Sets that are both open and closed are also called _clopen_.  For example,
the space $`X=(0,1)\cup(2,3)`$ (as a subspace of the _Euclidean topology_ on
$`\mathbb{R}`$) has each open interval clopen (as they themselves and their
complements are open). [[wp:cos][]]

The equivalent definition of a topological space $`(X,\tau)`$ in terms of
_closed sets_ follows from applying
[de Morgan's laws](https://en.wikipedia.org/wiki/De_Morgan%27s_laws):
A set $`X`$ together with a collection $`\tau`$ of subsets of $`X`$ satisfying
the following conditions: [[wp:cds][]], [[wm:cds][]], [[em:cds][]]

| axioms/defs | | |
|:--:|:--|:--|
| (1) | $`\emptyset \in \tau\;`$ and $`\;X \in \tau`$ | the empty set and $`X`$ itself belong to $`\tau`$ |
| (2) | if $`\;\{T_i: i \in I\} \subseteq \tau\;`$ then $`\;\bigcap_{i \in I} T_i \in \tau`$ | any arbitrary (finite or infinite) intersection of members of $`\tau`$ belongs to $`\tau`$ |
| (3) | $`T_1 \cap \cdots \cup T_n \in \tau`$ | the union of any finite number of members of $`\tau`$ belongs to $`\tau`$ |

Here, the sets in the topology $`\tau`$ are the closed sets, and their
complements in $`X`$ are the open sets. [[wp:aft][]]

##### Definition via Closure Operator and Interior Operator

Given a set $`X`$, a topology can also be defined via a map between subsets of
$`X`$: the _closure operation_ - or alternatively - the  _interior operation._
[[wp:aft][]], [[em:cls][]]

Certain properties must be met (which replace above axioms for open/closed
sets), where $`\mathbb{P}(X)`$ is the power set of $`X`$: [[wp:aft][]]

| axioms/defs | any operation $`\mathrm{cl}:\mathbb{P}(X)\to\mathbb{P}(X)`$ satisfying ... | any operation $`\mathrm{int}:\mathbb{P}(X)\to\mathbb{P}(X)`$ satisfying ... |
|:--:|:--|:--|
| (1) | $`A\subseteq\mathrm{cl}(A)`$ | $`\mathrm{int}(A)\subseteq{A}`$ |
| (2) | $`\mathrm{cl}(\mathrm{cl}(A))=\mathrm{cl}(A)`$ | $`\mathrm{int}(\mathrm{int}(A))=\mathrm{int}(A)`$ |
| (3) | $`\mathrm{cl}(A\cup{B})=\mathrm{cl}(A)\cup\mathrm{cl}(B)`$ | $`\mathrm{int}(A\cap{B})=\mathrm{int}(A)\cap\mathrm{int}(B)`$ |
| (4) | $`\mathrm{cl}(\emptyset)=\emptyset`$ | $`\mathrm{int}(X)=X`$ |
| def.: | a subset of $`X`$ is closed _iff_ it equals its closure | a subset of $`X`$ is open _iff_ it equals its interior |
| def.: | a subset of $`X`$ is open _iff_ its complement in $`X`$ is closed | a subset of $`X`$ is closed _iff_ its complement in $`X`$ is open |

A set's _closure, interior,_ and _boundary_ (the set difference of the former)
are fundamental concepts and further discussed below (see XXX).

##### Example: Finite Topological Spaces

For _finite topological spaces,_ where $`X`$ is finite, there can be only
finitely many open sets and closed sets (since the power set of a finite set
is finite).  In this case, the structural difference between open/closed sets
(possibly infinite unions/intersections) disappears. [wp:fts][]

Sets with 0, 1, or 2 points have these topologies:

| $`X=\ldots`$ | possible topologies | comments |
|:--|:--|:--|
| $`\emptyset`$ | $`\{\emptyset\}`$ | is unique |
| | | |
| $`\{a\}`$ | $`\{\emptyset,\{a\}\}`$ | is unique |
| | | |
| $`\{a,b\}`$ | $`\{\emptyset,\{a,b\}\}`$ | (0): called the _trivial topology_ (see next) |
| | $`\{\emptyset,\{a\},\{a,b\}\}`$ | (1): called a _Sierpiński space_ [[wp:sis][]] |
| | $`\{\emptyset,\{b\},\{a,b\}\}`$ | (2): _equivalent_ to (1) after swapping $`\{a\}\rightleftarrows\{b\}`$ $`^1`$|
| | $`\{\emptyset,\{a\},\{b\},\{a,b\}\}`$ | (3): called the _discrete topology_ (see next) |

 $`^1`$ Topologies and spaces (1), (2) are called _homeomorphic:_ just
swapping points is a _topological isomorphism._ [[wp:hmm][]]

__Counterexample:__ 3-point topologies

| $`X=\ldots`$ | valid/invalid topologies | comments |
|:--|:--|:--|
| $`\{a,b,c\}`$ | $`\{\emptyset,\{a,b,c\}\}`$ | the _trivial topology_ (see next) |
| | $`\{\emptyset,\{a\},\{b\},\{c\},\{a,b\},\{a,c\},\{b,c\},\{a,b,c\}\}`$ | the _discrete topology_ (see next) |
| | $`\ldots`$ | in total: 29 distinct topologies, only 9 are _inequivalent_ (i.e., _nonhomeomorphic_) after renaming |
| | $`\{\emptyset,\{a\},\{b\},\{a,b,c\}\}`$ | __not a topology__, missing union $`\{a,b\}`$ |
| | $`\{\emptyset,\{a,b\},\{b,c\},\{a,b,c\}\}`$ | __not a topology,__ missing intersection $`\{b\}`$ |

##### Other examples and counterexamples

See list of topologies: [[wp:lot][]]

#### 1.2 Def: Trivial (Indiscrete) Topology, Discrete Topology

For every set $`X`$, there exists the
| | |
|:--|:--|
| the _trivial topology:_ $`\tau=\{\emptyset,X\}`$ | with the empty set and $`X`$ as only open sets, which therefore are also closed sets [[wp:trt][]] |
| | also called: an _indiscrete, anti-discrete, coarse, concrete_ or _codiscrete space_ (when bundled with $`X`$) |
| | the trivial topology is the _smallest_ or _bottom element_ in the lattice of all topologies on $`X`$ |
| the _discrete topology:_ $`\tau=\mathbb{P}(X)`$ | contains all subsets of $`X`$ (i.e., the _power set_) [[wp:dct][]], [[wm:dct][]], [[em:dct][]] |
| | every subset of $`X`$ is open, and therefore every subset is also closed |
| | the discrete topology is the _largest_ or _top element_ in the lattice of all topologies on $`X`$ |

Any two _trivial spaces_ or any two _discrete spaces_ with the same
cardinality are _homeomorphic_ (topologically isomorph). [[wp:hmm][]]

A discrete structure is often used as the "default structure" on a set that
doesn't carry any other natural topology, uniformity, or metric.  Discrete
structures can often be used as "extreme" examples to test particular
suppositions. [[wp:dct][]], [[wp:mes][]], [[wp:uns][]]

### 2 Limits, Continuity, Homeomorphism

[wp:cfn]: https://en.wikipedia.org/wiki/Continuous_function
[wp:hmm]: https://en.wikipedia.org/wiki/Homeomorphism
[wp:ocm]: https://en.wikipedia.org/wiki/Open_and_closed_maps
[wp:tkn]: https://en.wikipedia.org/wiki/Trefoil_knot
[wp:hmt]: https://en.wikipedia.org/wiki/Homotopy
[wp:imt]: https://en.wikipedia.org/wiki/Isometry
[wp:atr]: https://en.wikipedia.org/wiki/Affine_transformation

[wm:edd]: https://mathworld.wolfram.com/Epsilon-DeltaDefinition.html
[wm:hmm]: https://mathworld.wolfram.com/Homeomorphism.html

[em:cfn]: https://encyclopediaofmath.org/wiki/Continuous_function
[em:cnm]: https://encyclopediaofmath.org/wiki/Continuous_mapping
[em:hmm]: https://encyclopediaofmath.org/wiki/Homeomorphism
[em:ocm]: https://encyclopediaofmath.org/wiki/Open_mapping
[em:clm]: https://encyclopediaofmath.org/wiki/Closed_mapping

[hatcher]: https://pi.math.cornell.edu/~hatcher/Top/Topdownloads.html

[graph1]: topological_homeomorphism_graph1.png
[graph2]: topological_homeomorphism_graph2.png

#### 1.3 Def: Limits, Continuity

XXX

[[wp:lim][]], [[wm:lim][]], [[em:lim][]]

https://en.wikipedia.org/wiki/Limit_(mathematics)

For a topological space $`X`$ and a sequence $`\{a_n\}_{n\geq{0}}=a_0,a_1,\ldots`$ in
$`X`$:

| definition | meaning, comments |
|:--|:--|
| a point $`a\in{X}`$ is the _limit_ of $`\{a_n\}`$ | iff for each neighborhood $`U`$ of $`a`$, there exists an $`N`$ such that for all $`n\ge{N}`$, $`a_n\in{U}`$ is satisfied [[wp:lim][]], [[wm:lim][]], [[em:lim][]] |
| | also written as: $`x_n\to{x}`$ or $`\lim_{n\to\infty}x_n=x_0`$ |
| | also called: the sequence $`\{a_n\}`$ _converges to_ or _tends to_ the limit $`a\in{X}`$[wp:lis]: |
| a sequence is called _convergent_ / _divergent_ | iff it does / not have a limit [[wp:lim][]],[wp:lis]: |

"the limit of f of x as x approaches c equals L".

" f {\displaystyle f} of x {\displaystyle x} tends to L {\displaystyle L} as x {\displaystyle x} tends to c {\displaystyle c}".

[wp:lis]: https://en.wikipedia.org/wiki/Limit_of_a_sequence

If such a limit exists, the sequence is called convergent.[2] A sequence that
does not converge is said to be divergent.

The sequence ( x n ) {\displaystyle (x_{n})} is said to converge to or tend to the limit x {\displaystyle x}.

https://en.wikipedia.org/wiki/Limit_(mathematics)#Topological_space

Topological space

In some sense the most abstract space in which limits can be defined are
topological spaces.

If $`X`$ is a topological space with topology $`\tau`$, and
$`\{a_n\}_{n\geq{0}}`$ is a sequence in $`X`$, then the limit (when it exists)
of the sequence is a point $`a\in{X}`$ such that, given a (open) neighborhood
$`U\in\tau`$ of $`a`$, there exists an $`N`$ such that for every $`n>N`$,
$`a_n\in{U}`$ is satisfied.

In this case, the limit (if it exists) may not be unique. However it must be
unique if $`X`$ is a Hausdorff space (see XXX).

https://en.wikipedia.org/wiki/Limit_%28mathematics%29#Continuous_functions

Continuous functions

An important class of functions when considering limits are continuous
functions. These are precisely those functions which preserve limits, in the
sense that if $`f`$ is a continuous function, then whenever
$`a_n\rightarrow{a}`$ in the domain of $`f`$, then the limit $`f(a_{n})`$
exists and furthermore is $`f(a)}`$.

https://mathworld.wolfram.com/Limit.html

A sequence $`x_1,x_2,\ldots`$ of elements in a topological space $`X`$ is said
to have limit $`x`$ provided that for each neighborhood $`U`$ of $`x`$, there
exists a natural number $`N`$ so that $`x_n`$ in $`U`$ for all $`n\geq{N}`$.

https://encyclopediaofmath.org/wiki/Limit

Let $`X`$ be a topological space. A sequence of points $`x_n`$,
$`n=1,2,\ldots`$ of $`X`$ is said to _converge_ to a point $`x_0\in{X}`$ or,
which is the same, the point $`x_0`$ is said to be a _limit_ of the given
sequence if for each neighbourhood $`U`$ of $`x_0`$ there is a natural number
$`N`$ such that for all $`n>N`$ the membership $`x_n\in{U}`$ is satisfied. In
this case one writes $`\lim_{n\to\infty}x_n=x_0`$.


https://encyclopediaofmath.org/wiki/Limit

For the general theory of topological spaces one needs to study more general
convergence concepts than that of sequences of points. One needs to consider
limits of sets of points indexed by a directed partially ordered set
(cf. Directed set). Such sets are called nets, and the convergence of nets is
sometimes known as Moore–Smith convergence.

The basic (inductive) idea of "limit" is that of some object being
approximated arbitrarily closely by a sequence of other objects.

----------------------------------------------------------------------

#### 1.3 Def: Mappings: Open, Closed, Continuous

##### Open Mappings, Closed Mappings

For topological spaces $`X, Y`$ and a function (map, mapping) $`f:X\to{Y}`$...

| definition | meaning, comments |
|:--|:--|
| $`f`$ is an _open / closed map_ | if the image of every open set is itself open / closed, respectively [[em:ocm][]], [[em:clm][]] |
| | note: competing definitions can be found differentiating between _strongly_ vs. _relatively open / closed_ [[wp:ocm][]] |
| | note: a maping may be open, closed, both, or neither [[wp:ocm][]] |

Both openness and closeness of a mapping is a necessary condition for it to be
_homeomorphic_ (see XXX).

Open mappings are used in the classification of spaces. [[em:ocm][]]

The class of continuous closed mappings plays an important role in general
topology and its applications. [[em:clm][]]

##### Continuous Mappings

For topological spaces $`X, Y`$ and a function (map, mapping) $`f:X\to{Y}`$...

| definition | meaning, comments |
|:--|:--|
| $`f`$ is _continuous at point_ $`x_0\in{X}`$ | iff for any neighborhood $`V`$ of $`f(x_0)`$ in $`Y`$, there is a neighborhood $`U`$ of $`x_0`$ such that its image $`f(U)`$ is contained in $`V`$ [[wp:cfn][]] |
| | note: competing definitions: [[em:cnm][]] requires $`\;{f(U)\subset{V}}\;`$ vs. $`\;{f(U)\subseteq{V}}\;`$ allowed in [[wp:cfn][]] |
| | |
| | equivalently: iff for any neighborhood $`V`$ of $`f(x_0)`$ in $`Y`$, the preimage $`f^{-1}(V)`$ is a neighborhood of $`x_0`$ [[wp:cfn][]] |
| | note: while function $`f`$ maps points of $`X`$ to points of $`Y`$, the continuity of $`f`$ depends on the given topologies $`\tau_X`$ and $`\tau_Y`$ |
| | |
| | note: the definition above generalizes the _epsilon-delta definition_ for real functions: [[wp:cfn][]], [[wm:edd][]], [[em:cfn][]] |
| | $`\qquad`$ iff for every $`\epsilon>0`$ there exists a $`\delta>0`$ such that for all $`x`$: $`\quad`$ whenever $`|x-x_0|<\delta`$, then $`|f(x)-f(x_0)|<\epsilon`$ |
| | equivalently: iff the limit of $`f(x)`$, as $`x`$ approaches $`x_0`$ through the domain of $`f`$, exists and is equal to $`f(x_0)`$: [[wp:cfn][]] |
| | $`\qquad`$ $`\lim_{x\to{x_0}}=f(x_0)`$ |
| | |
| $`f`$ is _continuous_ | iff $`f`$ is continuous at every point of $`X`$ [[wp:cfn][]] |
| | note: intuitively, iff for any subset $`A\subseteq{X}`$, $`f`$ maps points that are _close_ to $`A`$ to points that are _close_ to $`f(A)`$ |
| | $`\qquad`$ whereby _close_ is taken as "within the closure of" [[wp:cfn][]] |
| | |
| | equivalently, in terms of open/closed sets: [[em:cnm][]], [[wp:cfn][]] |
| | iff for every open set $`B`$ in $`Y`$, the preimage $`f^{-1}(B)`$ is open in $`X`$ |
| | iff for every closed set $`B`$ in $`Y`$, the preimage $`f^{-1}(B)`$ is closed in $`X`$ |
| | |
| | equivalently, in terms of interior/closure operators: [[wp:cfn][]], [[em:cnm][]] |
| | iff for every subset $`A\subseteq{X}`$, the image of the closure of $`A`$ is contained in the closure of the image of $`A`$: |
| | $`\qquad{f(\text{cl}_X(A))\subseteq{\text{cl}_Y(f(A))}}`$ |
| | iff for every subset $`B\subseteq{Y}`$, the preimage of the interior of $`B`$ is contained in the interior of the preimage of $`B`$: |
| | $`\qquad{f^{-1}(\text{int}_Y(B))\subseteq{\text{int}_X(f^{-1}(B))}}`$ |
| | |
| | for more, equivalent definitions of _continuous_, see [[wp:cfn][]] |
| | |
| | note: confusingly, _maps_ or _mappings_ are sometimes used synonymously with _continuous_ functions [[hatcher][]] |

Continuity of a mapping is a necessary condition for it to be _homeomorphic_
(see XXX).

##### The composition of continuous functions is continuous

Lemma: If maps $`f:X\to{Y}`$ and $`g:Y\to{Z}`$ are continuous, then so is the
composition $`g\circ{f}:X\to{Z}`$. [[wp:cfn][]]

#### 1.3 Def: Homeomorphism

For topological spaces $`X, Y`$ and a function (map, mapping) $`f:X\to{Y}`$...

| definition | meaning, comments |
|:--|:--|
| $`f`$ is _homeomorphic_ or a _homeomorphism_ | if $`f`$ is a one-to-one correspondence between points that is continuous in both directions: [[wp:hmm][]], [[wm:hmm][]], [[em:hmm][]] |
| | $`\quad`$ (1) $`f`$ is bijective $`\quad`$ (2) $`f`$ is continuous $`\quad`$ (3) the inverse $`f^{-1}`$ is continuous |
| | note: intuitively (with caveats), a continuous, invertible transformation between two geometric objects [[wp:hmm][]] |
| | also called: $`f`$ is _homeomorphic_ or a _topological mapping_ [[em:hmm][]] |
| | also called: $`f`$ is a_topological isomorphism_ or a _bicontinuous function_ [[wp:hmm][]] |
| | note: the similarity in form and meaning between "homomorphism" and "homeomorphism" is a common source of confusion [[wm:hmm][]] |
| | |
| $`X, Y`$ are_homeomorphic_ (to each other) | if there exist a homeomorphism between these spaces [[wp:hmm][]] |
| | also called: $`X, Y`$ are _topologically equivalent_ or _belong to the same topological type_ [[em:hmm][]] |
| | note: intuitively, if $`Y`$ can be obtained from $`X`$ by a continuous deformation, _and vice versa_ [[wp:hmm][]] |

##### Examples and counterexamples

A square and a circle are homeomorphic to each other, but a sphere and a torus
are not. [[wp:hmm][]]

A closed circle is homeomorphic to any closed convex polygon. [[wp:hmm][]]

The open interval $`(0,1)`$ and the real line $`\mathbb{R}`$ are homeomorphic
by various, continuous mappings, for example:

| $`f:(0,1)\to\mathbb{R}`$ | $`f^{-1}:\mathbb{R}\to(0,1)`$ | see graph | comment |
|:---:|:---:|:---:|:---|
| $`\ln(\frac{1}{x}-1)`$ | $`\frac{1}{e^{x}+1}`$ | [graph][graph1] | derived from [[em:hmm][]], for any interval $`(a,b)`$: $`\frac{b-a}{e^{x}+1}+a`$ |
| $`\frac{1}{-x}+\frac{1}{1-x}`$ | $`-\frac{-x+2-\sqrt{x^2+4}}{2x}`$ | [graph][graph2]| derived from [[wp:hmm][]], for any interval $`(a,b)`$: $`\frac{1}{a-x}+\frac{1}{b-x}`$ |

The one-dimensional intervals $`[0,1]`$ and $`(0,1)`$ are not homeomorphic,
as one is _compact_ while the other is not (see XXX). [[wp:hmm][]]

A homeomorphism that also preserves distances is called an [isometry][wp:imt].
[[wm:hmm][]]

A common type of geometric homeomorphism are [affine transformations][wp:atr],
which preserve lines and parallelism, but not necessarily Euclidean distances
and angles. [[wm:hmm][]]

##### Caveats: continuous deformations

The informal notion of homeomorphisms as continuous deformations can be
misleading: [[wp:hmm][]]
- Some continuous deformations do not result into homeomorphisms. \
  Example: the deformation of a line into a point. [[wp:hmm][]]
- Some homeomorphisms do not result from continuous deformations. \
  Example: A thickened [trefoil knot][wp:tkn] is homeomorphic to a solid torus,
  but not isotopic in $`\mathbb{R}^3`$. [[wp:hmm][]]

The precise definitions for the concept of continuous deformation are
[homotopy and isotopy][wp:hmt]. [[wp:hmm][]]

##### Euclidean spaces of different dimensions are not homeomorphic

Lemma: $`\mathbb{R}^m`$ and $`\mathbb{R}^n`$ are not homeomorphic for
$`m\neq{n}`$. [[wp:hmm][]]

Important discovery by L.E.J. Brouwer, which restored the faith in geometric
intuition, after [[em:hmm][]]
- G. Cantor's result stating that $`\mathbb{R}^n`$ and $`\mathbb{R}^m`$ have
  the same cardinality and
- G. Peano's result on the possibility of a continuous mapping from
  $`\mathbb{R}^n`$ onto $`\mathbb{R}^m`$, $`n<m`$.

##### Properties

Lemma: The composition of two homeomorphisms is again a
homeomorphism. [[wp:hmm][]]

Every homeomorphism is open, closed, and continuous. [[wp:ocm][]]

If $`f:X\to{Y}`$ is a continuous map that is also open or closed then if $`f`$
is [[wp:ocm][]]
- a surjection then it is a _quotient map_ (see XXX),
- an injection then it is a _topological embedding_ (see XXX),
- a bijection then it is a _homeomorphism_ (see XXX).

##### Homeomorphic spaces share the same topological properties

Example: if one of the spaces is _compact_ / _connected_ / _Hausdorff_ / ...,
then so is the other. [[wp:hmm][]]

#### 1.3 Def: Topological Basis, Subspaces, Quotient Spaces, Metrizable Spaces

##### Topological Basis

The simplest method to define a topology on a set is to directly list all the
open sets that make up that topology.  Often, though, it has benefits to
generate the topology from a much smaller colelction of open sets, called a
_topological basis_ or _base,_ in which all other open sets can be written as
(possibly infite) unions.

A _topological base_ of the topology $`\tau`$ on $`X`$ (or simply a _base_ for
$`X`$) is a family $`\mathcal{B}\subseteq{\tau}`$ of open sets such that every
open set in $`\tau`$ can be represented as the union of some subfamily of
$`\mathcal{B}`$. [[wp:tpb][]], [[wm:tpb][]], [[em:tpb][]]

Such a base is also called an _open base._  The dual notion defined in
terms of (possibly infite) intersections of closed sets is called a _closed
base._  Open bases are more often considered than closed bases, hence _base_
usually refers to the open case.

Conversely, a family $`\mathcal{B}`$ of subsets of $`X`$ forms a basis for a
topology $`\tau`$ on $`X`$ if it satisfies these two properties: [[wp:tpb][]],
[[wm:tpb][]], [[em:tpb][]]

| | |
|:--:|:--|
| (1) | the elements of $`\mathcal{B}`$ cover $`X`$; that is, every point $`x\in{X}`$ belongs to some element of $`\mathcal{B}`$ |
| (2) | for every $`B_1,B_2\in\mathcal{B}`$ and every point $`x\in{B_1}\cap{B_2}`$, there exists some $`B_3\in\mathcal{B}`$ such that $`x\in{B_3}\subseteq{B_1}\cap{B_2}`$ |

Every topological space has $`\tau`$ itself as base (by definition).

##### Topological Subspaces

For a given topological space $`(X,\tau)`$, each subset of $`X`$ can naturally
be made into a topological space: [[em:tps][]]

| A _subspace_ $`(S,\tau_S)`$ of a topological space $`(X,\tau)`$ | is ... [[wp:sst][]], [[em:tps][]] |
|:--|:--|
| a _subset_ $`S\subseteq{X}`$ | |
| the _subspace topology_ $`\tau_S=\{S\cap{U}\;|\;U\in\tau\}`$ | the intersections of $`S`$ with the open sets in $`(X,\tau)`$ [[wp:sst][]] |
| | also called: the relative topology, induced topology, or trace topology [[wp:sst][]] |

Alternatively, the subspace topology for a subset $`S\subseteq{X}`$ can be
defined as the _coarsest_ topology that makes _continuous_ the injective \
$`\qquad`$ _inclusion map_ $`\iota:S\hookrightarrow{X}`$ \
which is the "type-cast" function that treats each point of $`S`$ as one of
$`X`$. [[wp:sst][]], [[wp:inm][]]

Function $`\iota`$ meets the generalized notion of a _topological embedding._
[[wp:sst][]], [[wp:tpe][]]

If a topological space having some topological property implies its subspaces
have that property, then the property is called _hereditary._ [[wp:sst][]]

For example: Being a _Hausdorff space_ is hereditary. [[wp:sst][]]

[wp:inm]: https://en.wikipedia.org/wiki/Inclusion_map
[wp:tpe]: https://en.wikipedia.org/wiki/Topological_embedding

##### Quotient Spaces

For a given topological space $`(X,\tau)`$, each _equivalence relation_
$`\sim`$ on $`X`$ constructs a new topological space: [[wp:qus][]]

| The _quotient space_ $`(X/\!\sim,\tau_\sim)`$ of $`(X,\tau)`$ under $`\sim`$ | is ... [[wp:qus][]], [[wm:qus][]] |
|:--|:--|
| the _quotient set_ $`Y=X/\!\sim\;=\{[x]\;|\;x\in{X}\;\text{and}\;[x]=\{a\in{X}\;|\;a\sim{x}\}\}`$ | the set of equivalent classes of points in $`X`$ [[wp:qus][]], [[wm:qus][]] |
| | pronounced: "the quotient set of $`X`$ by $`R`$" or "$`X`$ modulo $`R`$" or "$`X`$ mod $`R`$" for an equivalence relation $`R`$ [[wp:eqc][]] |
| the _quotient topology_ $`\tau_\sim=\{U\;|\;U\subseteq{Y}\;\text{and}\;\{x\in{X}\;|\;[x]\in{U}\}\in\tau\}`$ | the subsets $`U`$ for each of which the union of _representatives_ (equivalent points in $`X`$) is open in $`(X,\tau)`$ [[wp:qus][]], [[wm:qus][]] |

Alternatively, the quotient topology under an equivalence relation $`\sim`$
can be defined as the _finest_ topology that makes _continuous_ the surjective \
$`\qquad`$ _canonical projection map_ $`q: X\twoheadrightarrow{Y}`$ \
which is the function that maps each point to its equivalence class.
[[wp:qus][]]

Function $`q`$ meets the generalized notion of a _quotient map._ [[em:qum][]],
[[wp:qus][]]

As an example of a quotient map: Identifying the points of a sphere that
belong to the same diameter produces the projective plane as a quotient space.
[[wp:qus][]]

In general, quotient spaces are not well behaved, and little is known about
them. [[wm:qus][]]

[wp:qus]: https://en.wikipedia.org/wiki/Quotient_space_(topology)
[wp:eqc]: https://en.wikipedia.org/wiki/Equivalence_class
[wm:qus]: https://mathworld.wolfram.com/QuotientSpace.html
[em:qum]: https://encyclopediaofmath.org/wiki/Quotient_mapping

##### Duality of Topological Subspaces and Quotient Spaces

XXX

![Subspaces](topological_inclusion_vs_quotient_map.png "Commutative Diagram of Inclusion vs Quotient Map")

The subspace topology $`\tau_Y`$ is characterized by the following property:
given a subspace $`Y`$ of $`X`$ and its inclusion map $`i:Y\to{X}`$, then for
any topological space $`Z`$, a map $`f:Z\to{Y}`$ is continuous if and only if
the composite map $`i\circ{f}`$ is continuous. [[wp:sst][]]

Quotient maps $`q:X\to{Y}`$ are characterized among surjective maps by the
following property: for any topological space $`Z`$ and for any function
$`f:Y\to{Z}`$, $`f`$ is continuous if and only if the composite map
$`f\circ{q}`$ is continuous. [[wp:qus][]]


##### Metrizable Spaces

A topological space $`(X,\tau)`$ is _metrizable_ if there is a metric \
$`\qquad{}d:X\times{X}\to[0,\infty)`$ \
on its underlying set such that the topology induced by $`d`$ is
$`\tau`$. [[wp:mts][]], [[em:tps][]]

Metrizable spaces form one of the most important classes of topological
spaces.  [[em:tps][]]

_Metrization theorems_ give sufficient conditions for a topological space to
be metrizable. [[wp:mts][]]

##### Examples

| | |
|:--|:--|
| trivial topology $`\tau=\{\emptyset,X\}`$ | [[wp:trt][]] |
| | the only base: $`\mathcal{B}=\{X\}`$ |
| | _note:_ the empty set $`\emptyset`$ is the union of the empty subfamily of $`\mathcal{B}`$ |
| | the topological space $`(X,\tau)`$ is not _metrizable_ for $`|X|>1`$ |
| discrete topology $`\tau=\mathbb{P}(X)`$ | [[wp:dct][]], [[wm:dct][]], [[em:dct][]] |
| | the smallest base: $`\mathcal{B}=\{\{x\}: x\in{X}\}`$, i.e., the collection of all singletons |
| | every discrete space is _metrizable_ (by the discrete metric) |
| | a finite space is metrizable only if it is discrete |
| _Euclidean topology_ on $`\mathbb{R}`$ | [[wp:tpb][]], [[wp:eut][]] |
| | base: the set $`\Gamma`$ of all _bounded_ open intervals in $`\mathbb{R}`$ |
| | base: the set $`\Gamma_\mathbb{Q}`$ of all intervals in $`\Gamma`$ where both endpoints are rational numbers |
| the discrete topology on $`\mathbb{R}`$ | [[wp:tpb][]],  [[wp:dct][]] |
| | base: the set $`\Sigma`$ of all closed intervals in $`\mathbb{R}`$ |
| | base: the set $`\Sigma_\mathbb{Q}`$ of all intervals in $`\Sigma`$ where both endpoints are rational numbers |


##### Compact spaces

XXX
[wp:cts]: https://en.wikipedia.org/wiki/Compact_space

#### References

XXX

----------------------------------------------------------------------

### 4 Neighbourhood, Limit Point, Isolated Point, Closure, Interior, Exterior, Boundary

#### 1.4 Def: Neighbourhood, Limit Point, Isolated Point, Closure, Interior, Exterior, Boundary

For a topological space $`(X,\tau)`$, for all subsets $`S,T\subseteq{X}`$, and
for all points $`p,q\in{X}`$ :

| definition | meaning, comments |
|:--|:--|
| $`S`$ is a _neighbourhood_ of $`p`$ | if $`S`$ includes an open set containing $`p`$ [[wp:nbh][]], [[wm:nbh][]] |
| | note: some texts require $`S`$ to be an open set [[em:nbh][]]; otherwise, $`S`$ is then called an _open neighbourhood_ of $`p`$ |
| $`S`$ is a _neighbourhood_ of $`T`$ | if $`S`$ includes an open set containing $`T`$ [[wp:nbh][]], [[em:nbh][]] |
| | equivalently: if $`S`$ is a neighbourhood of all points in $`T`$ |
| | |
| $`p`$ is a _limit point_ of a set $`S`$ | if every neighbourhood of $`p`$ contains at least one point of $`S`$ different from $`p`$ [[wp:lip][]], [[wm:lip][]] |
| | also called: _accumulation point, cluster point_ |
| | note: intuitively, $`p`$ can be "approximated" by _other_ points of $`S`$ |
| | note: $`p`$ does not have to belong to $`S`$ |
| the _derived set_ of a set $`S`$: $`S'`$ | is the set of all limit points of $`S`$ [[em:lip][]] |
| | |
| $`p`$ is an _isolated point_ of a set $`S`$ | if $`p`$ belongs to $`S`$ and the intersection of some neighborhood of $`p`$ with $`S`$ is the singleton $`\{p\}`$ [[em:isp][]] |
| | equivalently: if the singleton $`\{p\}`$ is an open set in the topological space $`S`$ as a subspace of $`X`$ [[wp:isp][]], [[wp:sst][]] |
| | equivalently: if $`p`$ belongs to $`S`$ and is not a limit point of $`S`$ [[wp:bnd][]] |
| the isolated points of a set $`S`$: $`S^i`$ | as denoted on proofwiki [[pw:isp][]] (no notations seen on [wp:], [wm:], or [em:]) |
| | |
| the _closure_ of a set $`S`$ : $`\;\mathrm{cl}\,S\;`$ or $`\;\overline{S}\;`$ or $`\;[S]`$ | is the union of $`S`$ and all limit points of $`S`$ [[wp:cls][]], [[wp:adp][]], [[em:cls][]] |
| | equivalently: is the smallest closed set (or the intersection of all closed sets) containing $`S`$ [[wp:aft][]] |
| | equivalently: is the set of all points such that any open set containing such a point must intersect $`S`$ [[wp:aft][]] |
| | note: the closure/exterior of $`S`$ is the complement of the exterior/closure of $`S`$ |
| | note: interior and closure are dual notions: the interior of $`S`$ is the complement of the closure of the complement of $`S`$ |
| $`p`$ is an _adherent point_ of a set $`S`$ | if $`p`$ belongs to the closure of $`S`$ [[wp:adp][]], [[wp:cls][]], [[em:prp][]] |
| | equivalently: if every neighbourhood of $`p`$ contains a point of $`S`$ |
| | also called: _point of closure, closure point, proximate point, contact point_ |
| | |
| the _interior_ of a set $`S`$ : $`\;\mathrm{int}\,S`$ | is the union of all open sets contained in $`S`$ [[wp:inp][]] |
| | equivalently: is the largest open set (or the union of all open sets) contained in $`S`$ [[wp:aft][]] |
| $`p`$ is an _interior point_ of a set $`S`$ | if $`p`$ belongs to the interior of $`S`$ [[wp:inp][]] |
| | equivalently: if $`S`$ is a neighbourhood of $`p`$ |
| | |
| the _exterior_ of a set $`S`$ : $`\;\mathrm{ext}\,S`$ | is the union of all open sets disjoint from $`S`$ [[wp:exp][]] |
| | equivalently: is the largest open set disjoint from $`S`$ |
| | equivalently: is the complement of the closure of $`S`$ |
| | note: the exterior/interior of $`S`$ is the interior/exterior of the complement of $`S`$ |
| $`p`$ is an _exterior point_ of a set $`S`$ | if $`p`$ belongs to the exterior of $`S`$ [[wp:exp][]] |
| | |
| the _boundary_ of a set $`S`$ : $`\;\mathrm{bd}\,S\;`$ or $`\;\partial{S}`$ | is the closure of $`S`$ minus the interior of $`S`$ [[wp:cls][]], [[wp:adp][]], [[em:bnd][]] |
| | equivalently: the intersection of the closure of $`S`$ with the closure of its complement |
| | also called: _frontier_ (to avoid confusion with the notion of _boundary_ in algebraic topology and for manifolds) |
| $`p`$ is a _boundary point_ of a set $`S`$ | if $`p`$ belongs to the boundary of $`S`$ [[wp:bnd][]] |
| | equivalently: if every neighborhood of $`p`$ contains at least one point of $`S`$ and at least one point not of $`S`$ |


XXX
\* Note: Wikipedia's definition incorrectly conflates the definitions of
an _isolated point_ of a subset $`S`$ vs of $`X`$: ...

  	a point x is called an isolated point of a subset S (in a topological space
  	X) if x is an element of S and there exists a neighborhood of x that does
  	not contain any other points of S. This is equivalent to saying that the
  	singleton {x} is an open set in the topological space S (considered as a
  	subspace of X).

$`p`$ is an _isolated point_ of a set $`S`$ if $`p`$ belongs to $`S`$ and there exists a neighborhood of $`p`$ that does not contain any other points of $`S`$ [[wp:isp][]]
XXX
see also [[pw:isp][]]
[pw:isp]: https://proofwiki.org/wiki/Definition:Isolated_Point_(Topology)

##### Example: Subsets in the Euclidean topology on $`\mathbb{R}`$

Let $`X=\mathbb{R}`$ and let $`S=[0,1)\cup\{2\}`$ (with a
right-open interval):

| ... points of $`S`$ | Euclidean topology [[wp:eut][]] |
|:--|:--|
| limit points: | $`[0,1]`$ |
| isolated points: | $`\{2\}`$ |
| interior points: | $`(0,1)`$ |
| boundary points: | $`\{0,1,2\}`$ |
| adherent (or closure) points: | $`[0,1]\cup\{2\}`$ |
| exterior points: | $`\mathbb{R}\setminus([0,1]\cup\{2\})`$ |

##### Example: Subsets in the Trivial and the Discrete topology

For the trivial topology and the discrete topology on a nonempty set $`X`$ and
for a subset $`S\subseteq{X}`$:
| ... points of $`S`$ | trivial topology [[wp:trt][]] | proof | discrete topology [[wp:dct][]] | proof |
|:--|:--|:--:|:--|:--:|
| limit points: | $`\begin{cases} X&\text{for\;}|S|>1\\ X\setminus{S}&\text{for\;}|S|=1\\ \emptyset&\text{otherwise} \end{cases}`$ | (a) | $`\emptyset`$ | (b) |
| isolated points: | $`\begin{cases} S&\text{for\;}|S|=1\\ \emptyset&\text{otherwise} \end{cases}`$ | (c) | $`S`$ | (c) |
| interior points: | $`\begin{cases} \emptyset&\text{for\;}S\subset{X}\\ X&\text{otherwise} \end{cases}`$ | (d) | $`S`$ | (d) |
| boundary points: | $`\begin{cases} X&\text{for\;}S\subset{X}\text{\;and\;}S\neq\emptyset\\ \emptyset&\text{otherwise} \end{cases}`$ | (f) | $`\emptyset`$ | (f) |
| adherent (or closure) points: | $`\begin{cases} X&\text{for\;}S\neq\emptyset\\ \emptyset&\text{otherwise} \end{cases}`$ | (e) | $`S`$ | (e) |
| exterior points: | $`\begin{cases} \emptyset&\text{for\;}S\neq\emptyset\\ X&\text{otherwise} \end{cases}`$ | (g) | $`X\setminus{S}`$ | (g) |

(a) $`X`$ is the only neighbourhood of each point in $`S`$; hence, every point
in $`X`$ is a _potential_ limit point of $`S`$.  But only those actually are
for which there is a _different_ point in $`S`$. [[wp:lip][]]

(b) For each point in $`X`$, there's an open singleton set $`\{p\}`$, which
provides a neighbourhood of $`p`$ that does not contain any other point in
$`S`$; hence, no point in $`X`$ is a limit point of $`S`$. [[wp:lip][]]

(c) Points of $`S`$ that are not limit points of $`S`$.

(d) The largest open set contained in $`S`$.

(e) The union of $`S`$ and all its limit points.

(f) The closure of $`S`$ minus the interior of $`S`$.

(g) The complement of the closure of $`S`$.

#### 1.5 Diagram: Limit/Isolated/Interior/Boundary/Adherent/Exterior Points

The inclusion/exclusion relationships among those six subsets of points can be
depicted as a
[Venn diagram](https://en.wikipedia.org/wiki/Venn_diagram),
see [[wp:bnd][]].  There, some circle intersections stand for mutually
exclusive criteria, that is, they're actually unpopulated (impossible).

In contrast, this
[Euler-style diagram](https://en.wikipedia.org/wiki/Euler_diagram)
renders the extent of the subsets not just by shape but by hatched background:

![Limit Points et al](topological_points.png
"Inclusion/Exclusion Relationships: Euler diagram")

The four inner triangles represent intersections between the three sets: _S,_
limit points of _S,_ and boundary points of _S._

(__*unnamed__) Curiously, one subset appears to be unnamed in the
[Wikipedia](https://en.wikipedia.org)
and
[Wolfram MathWorld](https://mathworld.wolfram.com)
sources (and elsewhere): those limit and boundary points that don't belong to
_S._  Since those points lie "just outside" of _S,_ a suitable name might be:
_nearby points_ of _S._

The inclusions/exclusions among the subsets form
[set partitions](https://en.wikipedia.org/wiki/Partition_of_a_set),
which can also be nicely stated verbally:

| | |
|:--|:--|
| every limit point of $`S`$ | is either an interior point or a boundary point of $`S`$ |
| every boundary point of $`S`$ | is either an isolated point or a limit point of $`S`$ |
| every adherent point of $`S`$ | is either a limit point or an isolated point of $`S`$ |
| every adherent point of $`S`$ | is either a boundary point or an interior point of $`S`$ |
| every point in $`X`$ | is either an adherent point or an exterior point of $`S`$ |
| every point in $`X`$ | is either an interior, boundary, or exterior point of $`S`$ |

----------------------------------------------------------------------

### 5 Separation Axioms, $`T_{\{0,1,2,3,4\}}`$-Spaces

A topology may "lump together" points of $`X`$ in that a point $`a`$ may
belong to all open sets that also contain a distinct point $`b`$, which makes
every neighbourhood of $`b`$ also one of $`a`$.

Inversely, points and entire subsets may be separated by varying degree under
a hierarchy of conditions, expressed as propositions on neighbourhoods.

Those _separation conditions_ (see 2.2) are important to the notion of
_connected spaces_ (and their connected components). [[wp:sps][]]

Their main use, though, is to _classify_ topological spaces: in which case
they're referred to as _separation axioms_ (see 2.3).  [[wp:spa][]]

#### 2.1 Example: Topologically Distinguishable Points, Separated Points

The open sets of these 4 (out of 9) topologies on $`X=\{a,b,c\}`$ ...

| $`\tau_0`$ | $`\tau_1`$ | $`\tau_2`$ | $`\tau_3`$ | [[wp:fts][]] |
|:--:|:--:|:--:|:--:|:--:|
| $`\{\emptyset,\{a,b,c\}\}`$ | $`\{\emptyset,\{c\},\{a,b,c\}\}`$ | $`\{\emptyset,\{a,b\},\{a,b,c\}\}`$ | $`\{\emptyset,\{c\},\{a,b\},\{a,b,c\}\}`$ | $`\ldots`$ |

... translate into the following propositions on neighbourhoods:

| | |
|:--:|:--|
| $`\tau_0`$ | points $`a,b,c`$ are in each other's neighbourhood $`^1`$ |
| $`\tau_0\,\ldots\,\tau_3`$ | points $`a`$ and $`b`$ have identical neighbourhoods |
| $`\tau_1\,\ldots\,\tau_3`$ | point $`c`$ and points $`a,b`$ do not have the same neighbourhoods $`^2`$ |
| $`\tau_0\,\ldots\,\tau_2`$ | point $`c`$ is in all neighbourhoods of point $`a`$ or of point $`b`$ |
| $`\tau_3`$ | point $`c`$ and points $`a,b`$ each have a neighbourhood not containing the other point(s) $`^3`$ |

Such propositions provide criteria for defining varying degrees of separation
of points (see 2.2), for example:

$`^1`$ Points $`a,b`$ and $`c`$ are called _topologically indistinguishable_
(while still distinct). [[wp:tpd][]]

$`^2`$ Points $`c`$ and $`a`$ are called _topologically distinguishable_ (as
are $`c`$ and $`b`$). [[wp:tpd][]]

$`^3`$ Points $`c`$ and $`a`$ are called _separated_ (as are $`c`$ and
$`b`$). [[em:t0a][]]

Note how the separation criteria also _classify_ the 3-point topologies
$`\tau_0\,\ldots\,\tau_3`$.

#### 2.2 Def.: Separation Conditions, Separated Points and Sets

_Separated sets_ are pairs of subsets of a topological space that are,
intuitively, neither intersecting nor touching.  (Not to be confused with
_separated spaces_ and _separable spaces._) [[wp:sps][]]

There are various degrees at which two points or two subsets can be considered
to be separated.  In order of increasing strength: [[wp:sps][]], [[wp:spa][]] \
\- any two _topologically distinguishable_ points must be distinct; \
\- any two _separated points_ must be topologically distinguishable; \
\- any two _separated sets_ must be disjoint, \
\- etc.

Each of the subsequent _separation conditions_ is stronger than the preceding
one: [[wp:spa][]], [[wp:sps][]] \
$`\qquad`$
distinct points / disjoint subsets
$`\Leftarrow\text{P}_{0}\Leftarrow\text{P}_{1}\Leftarrow\text{P}_{2}\Leftarrow\text{P}_{2½}\Leftarrow\text{P}_\text{CF}\;(\Leftarrow\ldots)`$
$`\qquad`$

Note: The separation conditions for sets also extend to points, or to a point
and a set, via singleton sets. [[wp:spa][]]

For a topological space $`(X,\tau)`$, for subsets $`S,T\subseteq{X}`$, and for
points $`p,q\in{X}`$ (or for their singleton sets $`S=\{p\},T=\{q\}`$):

| property | definition | meaning, comments |
|:--:|:--|:--|
| $`\text{P}_0`$ | points $`p,q`$ are _topologically distinguishable_ | if the points do _not_ have identical neighborhoods [[wp:tpd][]], [[wp:spa][]] |
| | | equivalently: iff there is an open set containing precisely one of the two points [[wp:tpd][]] |
| | | equivalently: iff there is a closed set containing precisely one of the two points [[wp:tpd][]] |
| | | equivalently: iff at least one point does not belong to the closure of the other point's singleton set [[wp:spa][]] |
| | | note: intuitively, if the topology is unable to discern between the points [[wp:tpd][]] |
| | | trivial example: points $`0,1,2`$ are pairwise distinguishable in $`\mathbb{R}`$ by the neighbourhoods $`[0,1)`$ and $`[1,2)`$ [[wp:rea][]] |
| | | |
| $`\text{P}_1`$ | points $`p,q`$ or subsets $`S,T`$ ... | |
| | ... are _separated_ | if each subset has some neighbourhood that is disjoint from of the other subset [[em:t0a][]] (extended to sets) |
| | | equivalently: iff each subset is included in an open set that is disjoint from the other subset [[wp:spa][]] |
| | | equivalently: iff each subset is disjoint from the other subset's closure [[wp:sps][]], [[wp:spa][]] |
| | | equivalently: iff the subsets are disjoint and each is disjoint from the other subset's derived set [[wp:sps][]] |
| | | def.: two points, or point and subset, are _separated_ iff the points' singletons are |
| | | note: intuitively, the neighbourhood-symmetric version of $`\text{P}_0`$ |
| | | note: the closures or derived sets themselves do not have to be disjoint [[wp:sps][]] |
| | | example: intervals $`[0,1)`$ and $`(1,2]`$ are separated in $`\mathbb{R}`$ by (non-disjoint) closures $`[0,1]`$ and $`[1,2]`$ [[wp:sps][]] |
| | | |
| $`\text{P}_2`$ | points $`p,q`$ or subsets $`S,T`$ ... | |
| | ... are _separated by neighbourhoods_ | if each subset has some neighbourhood that is disjoint from the other neighbourhood [[wp:sps][]], [[wp:spa][]] |
| | | also called: _Hausdorff's separation axiom_ [[wp:t2a][]], [[em:t2a][]] |
| | | equivalently: iff each subset is included in some open set that is disjoint from the other open set [[wm:spa][]] |
| | | def.: two points, or point and subset, are _separated by neighbourhoods_ iff the points' singletons are |
| | | note: intuitively, the two disjoint (open) neighbourhoods may still "touch" another |
| | | example: intervals $`[0,1)`$ and $`(1,2]`$ are separated in $`\mathbb{R}`$ by the disjoint neighbourhoods $`(-1,1)`$ and $`(1,3)`$ [[wp:sps][]] |
| | | __note:__ The separated subsets' closures may intersect, for example: in this 3-point topology... (see XXX) |
| | | $`\qquad\tau_6=\{\emptyset,\{a\},\{b\},\{a,b\},\{a,b,c\}\}`$, points $`a,b`$ are separated by nbhds yet have intersecting closures $`\{a,c\},\{b,c\}`$ |
| | | |
| $`\text{P}_{2½}`$ | points $`p,q`$ or subsets $`S,T`$ ... | |
| | ... are _separated by closed neighbourhoods_ | if each subset has some _closed_ neighbourhood that is disjoint from the other neighbourhood [[wp:sps][]], [[wp:spa][]] |
| | | def.: two points, or point and subset, are _separated by closed neighbourhoods_ iff the points' singletons are |
| | | note: intuitively, the two disjoint (closed) neighbourhoods cannot "touch" another |
| | | example: intervals $`[0,1)`$ and $`(2,3]`$ are separated in $`\mathbb{R}`$ by the closed, disjoint nbhds $`[0,1]`$ and $`[2,3]`$ [[wp:sps][]] |
| | | example: intervals $`[0,1)`$ and $`(1,2]`$ are _not_ separated by closed nbhds, cannot be made both closed & disjoint [[wp:sps][]] |
| | | __note:__ Having disjoint closures does not imply separation by closed nbhds, for example: in this 3-point topology... (see XXX) |
| | | $`\qquad\tau_5=\{\emptyset,\{c\},\{a,c\},\{b,c\},\{a,b,c\}\}`$, points $`a,b`$ have closures $`\{a\},\{b\}`$ yet are _not_ separated by closed nbhds|
| | | __question:__ How come that separated by closed nbhds $`\not\equiv`$ closures disjoint, yet $`\equiv`$ in the definition of $`\text{T}_{2½}`$ below? (see XXX) |
| | | |
| $`\text{P}_\text{CF}`$ | points $`p,q`$ or subsets $`S,T`$ ... | |
| | ... are _separated by a continuous function_ | if some _continous_ function on $`X`$ maps points of one set to $`0`$ and of the other set to $`1`$ [[wp:sps][]], [[wp:spa][]] |
| | | def.: two points, or point and subset, are _separated by continuous function_ iff the points' singletons are |
| | | example: intervals $`[0,1)`$ and $`(2,3]`$ are separated in $`\mathbb{R}`$ by this continuous function... |
| | | $`\qquad{x}\mapsto\big\{\quad{0}\quad\text{for}\;x<1\quad|\quad{x-1}\quad\text{for}\;1\le{x}\le{2}\quad|\quad{1}\quad\text{for}\;x>2\quad\big\}`$ |
| | | example: intervals $`[0,1)`$ and $`(1,2]`$ are _not_ separated by continuous function, cannot be made continuous at $`\{1\}`$ [[wp:sps][]] |
| | | __question:__ How can subsets be separated by closed & disjoint nbhds but _not_ by continuous function? ($`\leftrightarrow`$ compactness?) |
| | | |
| ... | more separation conditions: | see  [[wp:sps][]], [[wp:spa][]] |

#### 2.3 Def.: Separation Axioms, $`T_{\{0,1,2,3,4\}}`$-Spaces

Note: _The history of the separation axioms is convoluted, with many meanings
competing for the same terms and many terms competing for the same concept._
[[wp:hsa][]]

The separation axioms all say, in one way or another, that points or sets that
are distinguishable or separated in some weak sense must also be
distinguishable or separated in some stronger sense. [[wp:spa][]]

For a topological space $`(X,\tau)`$, for subsets $`S,T\subseteq{X}`$, and for
points $`p,q\in{X}`$:

| property | definition | meaning, comments |
|:--:|:--|:--|
| $`\text{T}_0`$ | $`X`$ is $`\text{T}_0`$ or _Kolmogorov_ | if any two distinct points in $`X`$ are topologically distinguishable [[wp:t0a][]], [[wm:t0a][]], [[em:t0a][]], [[wp:spa][]], [[wm:spa][]], [[em:spa][]] |
| | | also called: _Kolmogorov's separation axiom_ [[wp:t0a][]], [[wm:t0a][]], [[em:t0a][]] |
| | | note: a singleton set need not be closed [[em:t0a][]] |
| | | |
| $`\text{T}_1`$ | $`X`$ is $`\text{T}_1`$ or _accessible_ or _has a Fréchet topology_ | if any two distinct points in $`X`$ are separated [[wp:t1a][]], [[wm:t1a][]], [[em:t0a][]], [[wp:spa][]], [[wm:spa][]], [[em:spa][]] |
| | | equivalently: iff $`X`$ is both $`\text{T}_0`$ and any two topologically distinguishable points are separated ($`R_0`$) [[wp:t1a][]] |
| | | equivalently: iff $`X`$ is both $`\text{T}_0`$ and all singleton sets are closed [[em:t0a][]], minor difference to [[wp:t1a][]] |
| | | equivalently: iff all finite sets are closed; implies: a finite $`\text{T}_1`$ space is necessarily discrete [[wp:t1a][]] |
| | | note: not to be confused with _Fréchet space_ in functional analysis [[wp:spa][]] |
| | | |
| $`\text{T}_2`$ | $`X`$ is $`\text{T}_2`$ or _separated_ or _Hausdorff_ | if any two distinct points are separated by neighbourhoods [[wp:t2a][]], [[wm:t2a][]], [[em:t2a][]], [[wp:spa][]], [[wm:spa][]], [[em:spa][]] |
| | | equivalently: iff any singleton set equals the intersection of all closed neighbourhoods of its point [[wp:t2a][]] |
| | | note: implies the uniqueness of limits of sequences, nets, and filters [[wp:t2a][]] |
| | | note: implies each compact set is a closed set [[wp:t2a][]] |
| | | |
| $`\text{T}_{2½}`$ | $`X`$ is $`\text{T}_{2½}`$ or _Urysohn_ | if any two distinct points are separated by closed neighbourhoods [[wp:urs][]]], [[em:urs][]] |
| | | equivalently: iff any two distinct points have neighbourhoods with disjoint closures [[em:urs][]] |
| | | |
| $`\text{CT}_2`$ | $`X`$ is _completely_ $`\text{T}_2`$ or _completely Hausdorff_ | any two distinct points are separated by a continuous function [[wp:urs][]], [[em:urs][]], [[wp:spa][]], [[em:spa][]] |
| | | note: every $`\text{T}_{2½}`$ space is a $`\text{CT}_2`$ space is a $`\text{T}_{3½}`$ space; but $`\text{CT}_2`$ and $`\text{T}_3`$ are incomparable [[wp:spa][]] |
| | | |
| $`\text{R}`$ | $`X`$ is _regular_ | if any point and each closed set not containing it are separated by neighbourhoods [[wp:rgs][]], [[em:rgs][]] |
| | | also called: _axiom_ $`\text{T}_3`$ or _regularity axiom,_ sometimes conflated with $`\text{T}_3`$-space [[wp:rgs][]], [[em:rgs][]] |
| | | equivalently: any point and each closed set not containing it are contained in some disjoint open sets [[em:rgs][]] |
| | | |
| $`\text{T}_3`$ | $`X`$ is $`\text{T}_3`$ or _regular Hausdorff_ (or _Vietoris_) | if $`X`$ if is both regular and $`\text{T}_0`$ [[wp:rgs][]], [[wp:spa][]] or $`\text{T}_1`$ [[wm:t3a][]], [[em:rgs][]], [[wm:spa][]], [[em:spa][]])|
| | | equivalently: iff all singletons are closed and $`X`$ is regular [[em:rgs][]] |
| | | equivalently: iff $`X`$ a space with a _countable base_ and _metrizable_ [[em:rgs][]] |
| | | |
| $`\text{CR}`$ | $`X`$ is _completely regular_ | if any point and closed set not containing it are separated by a continuous function [[wp:tys][]], [[wm:crs][]], [[em:crs][]], [[wp:spa][]], [[em:spa][]] |
| | | note: every $`\text{R}`$ space is a $`\text{CR}`$ space [[wp:spa][]] |
| | | |
| $`\text{T}_{3½}`$ | $`X`$ is $`\text{T}_{3½}`$ or _completely regular Hausdorff_ or _Tychonoff_ | if $`X`$ if is both completely regular and $`\text{T}_0`$ [[wp:tys][]], [[wp:spa][]] or $`\text{T}_1`$ [[wm:tys][]], [[em:tys][]] |
| | | |
| $`\text{N}`$ | $`X`$ is _normal_ | if any two disjoint closed sets are separated by neighbourhoods [[wp:nms][]], [[wm:nms][]], [[em:nms][]], [[wp:spa][]], [[wm:spa][]], [[em:spa][]] |
| | | also called: _axiom_ $`\text{T}_4`$ or _normality axiom,_ sometimes conflated with $`\text{T}_4`$-space [[wm:t4a][]], [[wp:nms][]], [[em:nms][]], [[wm:spa][]], [[em:spa][]] |
| | | equivalently: iff any two disjoint closed sets are contained in some disjoint open sets [[em:nms][]], [[wp:nms][]] |
| | | equivalently: iff any two disjoint closed sets can be separated by a continuous function [[em:nms][]], [[em:spa][]], [[wp:nms][]], [[wp:spa][]] |
| | | note: $`\text{N}`$ and $`\text{CR}`$ (or $`\text{R}`$) are incomparable [[wp:spa][]] |
| | | |
| $`\text{T}_4`$ | $`X`$ is $`\text{T}_4`$ or _normal Hausdorff_ (or _Tietze_) | iff $`X`$ is both normal and $`\text{T}_1`$ [[wp:nms][]], [[wm:nms][]], [[em:nms][]], [[wp:spa][]], [[wm:spa][]], [[em:spa][]] |
| | | note: all _compact_ Hausdorff spaces are normal [[wp:nms][]] |
| | | note: all _metric_ spaces (and hence all _metrizable_ spaces) are _perfectly normal Hausdorff_ (and hence $`\text{T}_4`$) [[wp:nms][]] |
| | | |
| ... | more spaces (intermediate or higher up): | see [[wp:sps][]], [[wp:spa][]], [[em:spa][]] |

##### Example and counterexample of 3-point topologies

XXX  [[wp:fts][]]

| $`\tau_i`$ | $`\tau_i=\ldots`$ | example, comments | example, comments |
|:--|:--|:--|:--|
| (0) | $`\{\emptyset,\{a,b,c\}\}`$ | no points distinguishable | $`\Rightarrow\textbf{is}\;\neg\mathbf{T_{0}}\Rightarrow\neg\mathbf{T_{1}}\Rightarrow\neg\mathbf{T_{2}}\Rightarrow\neg\mathbf{T_{2½}}`$ |
| (1) | $`\{\emptyset,\{c\},\{a,b,c\}\}`$ | points $`a,b`$ not distinguishable | points $`a,c`$ distinguishable but not separated |
| (2) | $`\{\emptyset,\{a,b\},\{a,b,c\}\}`$ | $`\qquad`$ " | $`\qquad`$ " |
| (3) | $`\{\emptyset,\{c\},\{a,b\},\{a,b,c\}\}`$ | points $`a,c`$ separated by closed nbhd | sets $`\{a,b\},\{c\}`$ separated by closed nbhd |
| (4) | $`\{\emptyset,\{c\},\{b,c\},\{a,b,c\}\}`$ | all distinct points distinguishable $`\Rightarrow\textbf{is}\mathbf{\;T_0}`$ | no points separated $`\Rightarrow\textbf{is}\;\neg\mathbf{T_{1}}\Rightarrow\neg\mathbf{T_{2}}\Rightarrow\neg\mathbf{T_{2½}}`$ |
| (5) | $`\{\emptyset,\{c\},\{a,c\},\{b,c\},\{a,b,c\}\}`$ | points $`a,b`$ separated but not by nbhd | sets $`\{a,c\},\{b\}`$ separated but not by nbhd |
| (6) | $`\{\emptyset,\{a\},\{b\},\{a,b\},\{a,b,c\}\}`$ | points $`a,b`$ separated by nbhd but not by closed nbhd | no points separated by closed nbhd |
| (7) | $`\{\emptyset,\{b\},\{c\},\{a,b\},\{b,c\},\{a,b,c\}\}`$ | points $`b,c`$ separated by nbhd but not by closed nbhd | points $`a,c`$ separated by closed nbhd |
| (8) | $`\{\emptyset,\{a\},\{b\},\{c\},\{a,b\},\{a,c\},\{b,c\},\{a,b,c\}\}`$ | all distinct points and disjoint sets separated by closed nbhd | $`\Rightarrow\textbf{is}\;\mathbf{T_{2½}}\Rightarrow\mathbf{T_{2}}\Rightarrow\mathbf{T_{1}}\Rightarrow\mathbf{T_{0}}`$ |

---

[em:crs]: https://encyclopediaofmath.org/wiki/Completely-regular_space
[em:nms]: https://encyclopediaofmath.org/wiki/Normal_space
[em:rgs]: https://encyclopediaofmath.org/wiki/Regular_space
[em:spa]: https://encyclopediaofmath.org/wiki/Separation_axiom
[em:t0a]: https://encyclopediaofmath.org/wiki/Kolmogorov_axiom
[em:t2a]: https://encyclopediaofmath.org/wiki/Hausdorff_space
[em:tys]: https://encyclopediaofmath.org/wiki/Tikhonov_space
[em:urs]: https://encyclopediaofmath.org/wiki/Urysohn_space

[wp:hsa]: https://en.wikipedia.org/wiki/History_of_the_separation_axioms
[wp:nms]: https://en.wikipedia.org/wiki/Normal_space
[wp:rgs]: https://en.wikipedia.org/wiki/Regular_space
[wp:spa]: https://en.wikipedia.org/wiki/Separation_axiom
[wp:sps]: https://en.wikipedia.org/wiki/Separated_sets
[wp:t0a]: https://en.wikipedia.org/wiki/T0_space
[wp:t1a]: https://en.wikipedia.org/wiki/T1_space
[wp:t2a]: https://en.wikipedia.org/wiki/Hausdorff_space
[wp:tys]: https://en.wikipedia.org/wiki/Tychonoff_space
[wp:urs]: https://en.wikipedia.org/wiki/Urysohn_and_completely_Hausdorff_spaces

[wm:crs]: https://mathworld.wolfram.com/CompletelyRegularSpace.html
[wm:nms]: https://mathworld.wolfram.com/NormalSpace.html
[wm:spa]: https://mathworld.wolfram.com/SeparationAxioms.html
[wm:t0a]: https://mathworld.wolfram.com/T0-SeparationAxiom.html
[wm:t1a]: https://mathworld.wolfram.com/T1-SeparationAxiom.html
[wm:t2a]: https://mathworld.wolfram.com/T2-SeparationAxiom.html
[wm:t3a]: https://mathworld.wolfram.com/T3-Space.html
[wm:t4a]: https://mathworld.wolfram.com/T4-SeparationAxiom.html
[wm:tys]: https://mathworld.wolfram.com/TychonoffSpace.html

----------------------------------------------------------------------

XXX

One may speak of whether two points, or more generally two subsets, of a
topological space are "near" without concretely defining a distance.

The most
common case is given by _manifolds,_ which are topological spaces that, near
each point, resemble an open set of a Euclidean space, but on which no
distance is defined in general. [[wp:mnf][]], [[wm:mnf][]], [[em:mnf][]]

[wp:mnf]: https://en.wikipedia.org/wiki/Manifold
[wm:mnf]: https://mathworld.wolfram.com/Manifold.html
[em:mnf]: https://encyclopediaofmath.org/wiki/Manifold

#### References

[em:bnd]: https://encyclopediaofmath.org/wiki/Boundary
[em:cls]: https://encyclopediaofmath.org/wiki/Closure_of_a_set
[em:cds]: https://encyclopediaofmath.org/wiki/Closed_set
[em:dct]: https://encyclopediaofmath.org/wiki/Discrete_topology
[em:pst]: https://encyclopediaofmath.org/wiki/General_topology
[em:inp]: https://encyclopediaofmath.org/wiki/Interior_of_a_set
[em:isp]: https://encyclopediaofmath.org/wiki/Isolated_point
[em:lim]: https://encyclopediaofmath.org/wiki/Limit
[em:lip]: https://encyclopediaofmath.org/wiki/Limit_point_of_a_set
[em:nbh]: https://encyclopediaofmath.org/wiki/Neighbourhood
[em:ops]: https://encyclopediaofmath.org/wiki/Open_set
[em:prp]: https://encyclopediaofmath.org/wiki/Proximate_point
[em:tpb]: https://encyclopediaofmath.org/wiki/Base
[em:tps]: https://encyclopediaofmath.org/wiki/Topological_space

[wp:adp]: https://en.wikipedia.org/wiki/Adherent_point
[wp:aft]: https://en.wikipedia.org/wiki/Axiomatic_foundations_of_topological_spaces
[wp:bnd]: https://en.wikipedia.org/wiki/Boundary_(topology)
[wp:cds]: https://en.wikipedia.org/wiki/Closed_set
[wp:cls]: https://en.wikipedia.org/wiki/Closure_(topology)
[wp:cos]: https://en.wikipedia.org/wiki/Clopen_set
[wp:dct]: https://en.wikipedia.org/wiki/Discrete_space
[wp:eut]: https://en.wikipedia.org/wiki/Euclidean_topology
[wp:exp]: https://en.wikipedia.org/wiki/Interior_(topology)#Exterior_of_a_set
[wp:fts]: https://en.wikipedia.org/wiki/Finite_topological_space
[wp:inp]: https://en.wikipedia.org/wiki/Interior_(topology)
[wp:isp]: https://en.wikipedia.org/wiki/Isolated_point
[wp:itv]: https://en.wikipedia.org/wiki/Interval_(mathematics)
[wp:lim]: https://en.wikipedia.org/wiki/Limit_(mathematics)
[wp:lip]: https://en.wikipedia.org/wiki/Accumulation_point
[wp:lot]: https://en.wikipedia.org/wiki/List_of_topologies
[wp:mes]: https://en.wikipedia.org/wiki/Metric_space
[wp:mts]: https://en.wikipedia.org/wiki/Metrizable_space
[wp:nbh]: https://en.wikipedia.org/wiki/Neighbourhood_(mathematics)
[wp:ops]: https://en.wikipedia.org/wiki/Open_set
[wp:pst]: https://en.wikipedia.org/wiki/Point-set_topology
[wp:rea]: https://en.wikipedia.org/wiki/Real_line
[wp:sis]: https://en.wikipedia.org/wiki/Sierpi%C5%84ski_space
[wp:sst]: https://en.wikipedia.org/wiki/Subspace_topology
[wp:tpb]: https://en.wikipedia.org/wiki/Basis_(topology)
[wp:tpd]: https://en.wikipedia.org/wiki/Topologically_distinguishable
[wp:tps]: https://en.wikipedia.org/wiki/Topological_space
[wp:trt]: https://en.wikipedia.org/wiki/Trivial_topology
[wp:uns]: https://en.wikipedia.org/wiki/Uniform_space

[wm:cds]: https://mathworld.wolfram.com/ClosedSet.html
[wm:dct]: https://mathworld.wolfram.com/DiscreteTopology.html
[wm:pst]: https://mathworld.wolfram.com/Point-SetTopology.html
[wmc:pst]: https://mathworld.wolfram.com/classroom/Point-SetTopology.html
[wm:lim]: https://mathworld.wolfram.com/Limit.html
[wm:lip]: https://mathworld.wolfram.com/LimitPoint.html
[wm:nbh]: https://mathworld.wolfram.com/OpenNeighborhood.html
[wm:ops]: https://mathworld.wolfram.com/OpenSet.html
[wm:tpb]: https://mathworld.wolfram.com/TopologicalBasis.html
[wm:tps]: https://mathworld.wolfram.com/TopologicalSpace.html

----------------------------------------------------------------------

XXX Connectedness

----------------------------------------------------------------------

XXX The Discrete and Trivial Topilogy Revisited

For every set $`X`$, there exists the
| | |
|:--|:--|
| _trivial topology_ $`\tau=\{\emptyset,X\}`$ | with only the empty set and $`X`$ as open sets (and therefore also closed sets) [[wp:trt][]] |
| | all points are "lumped together" and cannot be _distinguished_ by topological means (see XXX below) |
| | every indiscrete space X is (path-connected and therefore) _connected_ (see XXX below) |
| | for $`|X|>1`$: does not satisfy any of _separation axioms_ (see XXX below) |
| _discrete topology_ $`\tau=\mathbb{P}(X)`$ | contains all subsets of $`X`$ (i.e., the _power set_) [[wp:dct][]], [[wm:dct][]], [[em:dct][]] |
| | the points form a _discontinuous sequence:_ they are _isolated_ from each other (see XXX below) |
| | every discrete space is _totally disconnected_ and satisfies each of the _separation axioms_ (see XXX below) |
| | every map from $`X`$ to any other topological space is _continuous_ (see XXX below) |

[[wp:dct][]], [[wp:mes][]], [[wp:uns][]]


[Up](./README.md)
