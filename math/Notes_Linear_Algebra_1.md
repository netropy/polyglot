### [polyglot](https://gitlab.com/netropy/polyglot)/[math](./README.md)

# Notes: Linear Algebra (1) - Vector Spaces, Linear Maps

___Goals:___ Summarize basic definitions, theorems, and methods of Linear
Algebra with links to expert sources. \
Unlike many textbooks, have definitions and theorems cover or at least hint
at: \
\+ algebraic generalizations (to connect with abstract algebra), \
\+ the infinite-dimensional case (as preparation for functional analysis).

XXX: too long, split this markdown into 3..4 files

___TOC:___
[[_TOC_]]

[Resources][r0] \
[1 Linear Algebra, Multilinear Algebra][r1] \
[2 Vector Space][r2] \
[2.1 Vector space, field, module, ring, vector axioms][r2.1] \
[2.2 Linear combination][r2.2] \
[2.3 Affine/conical/convex combination][r2.3] \
[2.4 Linear subspace, vector sum, intersection][r2.4] \
[2.5 Hulls, span, spanning set][r2.5] \
[2.6 Linear independence][r2.6] \
[2.7 Vector basis, coordinate systems][r2.7] \
[2.8 Dimension, hyperplane, infinite-dimensional vector space][r2.8] \
[2.9 Basis and dimension theorems][r2.9] \
[2.10 Quotient space, codimension, relative dimension][r2.10] \
[2.11 Vector sum, direct sum decomposition, standard basis][r2.11] \
[2.12 Normed space, unit vectors][r2.12] \
[2.13 Inner product space, orthogonal/orthonormal vectors][r2.13] \
[3 Linear map][r3] \
[3.1 Linear map/transformation/operator, vector space homomorphism][r3.1] \
[3.2 Linear maps and their matrix representations][r3.2] \
[3.3 Kernel, image, rank, rank–nullity theorem][r3.3] \
[3.4 Vector space endomorphism, eigenvector, eigenvalue][r3.4] \
[3.5 Linear form, dual vector space][r3.5] \
[3.6 Bilinear/multilinear map/form][r3.6] \
[German resources][r4]

[r0]: #resources
[r1]: #1-linear-algebra-multilinear-algebra
[r2]: #2-vector-space
[r2.1]: #21-vector-space-field-module-ring-vector-axioms
[r2.2]: #22-linear-combination
[r2.3]: #23-affineconicalconvex-combination
[r2.4]: #24-linear-subspace-vector-sum-intersection
[r2.5]: #25-hulls-span-spanning-set
[r2.6]: #26-linear-independence
[r2.7]: #27-vector-basis-coordinate-systems
[r2.8]: #28-dimension-hyperplane-infinite-dimensional-vector-space
[r2.9]: #29-basis-and-dimension-theorems
[r2.10]: #210-quotient-space-codimension-relative-dimension
[r2.11]: #211-vector-sum-direct-sum-decomposition-standard-basis
[r2.12]: #212-normed-space-unit-vectors
[r2.13]: #213-inner-product-space-orthogonalorthonormal-vectors
[r3]: #3-linear-map
[r3.1]: #31-linear-maptransformationoperator-vector-space-homomorphism
[r3.2]: #32-linear-maps-and-their-matrix-representations
[r3.3]: #33-kernel-image-rank-rank–nullity-theorem
[r3.4]: #34-vector-space-endomorphism-eigenvector-eigenvalue
[r3.5]: #35-linear-form-dual-vector-space
[r3.6]: #36-bilinearmultilinear-mapform
[r4]: #german-resources

## Resources

My preference for these 5 math encyclopedias - short, precise, and with linkable
definitions:
| cited as | source |
|:--:|:--|
| [em:...] | [Encyclopedia of Mathematics][em], articles refereed by members of the [European Mathematical Society](https://euro-math-soc.eu) |
| [nl:...] | [nLab][nl] a "higher structures" math/physics/philosophy wiki, entries by experts and contributors on [nForum](https://ncatlab.org/nlabmeta/show/Welcome+to+the+nForum), [n-Category Café](https://golem.ph.utexas.edu/category/) |
| [pm:...] | [PlanetMath.org][pm] encyclopedia, entries by experts and contributors, hosted by [University of Waterloo Mathematics Faculty](https://uwaterloo.ca/math/) |
| [wm:...] | [Wolfram MathWorld][wm], entries by experts and contributors, hosted by [Wolfram Research](https://www.wolfram.com/), [Wolfram MathWorld Classroom](https://mathworld.wolfram.com/classroom/) |
| [wp:...] | [Wikipedia][wp], community-edited encyclopedia hosted by the [Wikimedia Foundation](https://en.wikipedia.org/wiki/Wikimedia_Foundation) |

[em]: https://encyclopediaofmath.org
[nl]: https://ncatlab.org
[pm]: https://planetmath.org
[wm]: https://mathworld.wolfram.com/about/
[wp]: https://en.wikipedia.org

Some open-access (under-)graduate textbooks:
| cited as | |
|:--:|:--|
| \[ax\] | [Linear Algebra Done Right][ax] (2024-11-29) by [Sheldon Axler (SFSU)](https://www.axler.net) |
| \[st\] | [Linear Algebra Done Wrong][st] (2024-10-01) by [Sergei Treil (Brown)](https://sites.google.com/a/brown.edu/sergei-treil-homepage/home) |
| \[ko\] | [Linear Algebra][ko] (2024-12-18) by [Emmanuel Kowalski (ETH Zürich)](https://people.math.ethz.ch/~kowalski) |
| \[lns\] | [Linear Algebra][lns] (2016-11-15) by [Isaiah Lankham, Bruno Nachtergaele, Anne Schilling (UC Davis)](https://www.math.ucdavis.edu/~anne/linear_algebra/index.html) |
| \[cdtw\] | [Linear Algebra][cdtw] (2016-08-24) by [David Cherney, Tom Denton, Rohit Thomas, Andrew Waldron (UC Davis)](https://www.math.ucdavis.edu/~linear) |
| \[gq\] | [Vol. I: Linear Algebra for Computer Vision, Robotics, and Machine Learning][gq] (2024-12-06) by [Jean Gallier, Jocelyn Quaintance (UPenn)](https://www.cis.upenn.edu/~jean/home.html) |
| | various free linear algebra textbooks listed on [FreeTechBooks](http://www.freetechbooks.com/linear-algebra-f78.html) and [LibreTexts](https://math.libretexts.org/Bookshelves/Linear_Algebra) |

Free video courses:
| cited as | |
|:--:|:--|
| \[mm\] | [Math 8530, Advanced Linear Algebra][mm] (with course material) by [Matthew Macauley (Clemson)](http://www.math.clemson.edu/~macaule) |
| \[gs\] | [MIT 18.06SC Linear Algebra, Fall 2011][gs] (with course material) by [Gilbert Strang et al (MIT OpenCourseWare)](https://ocw.mit.edu/courses/18-06sc-linear-algebra-fall-2011) |
| \[3b1b\] | [Essence of linear algebra][3b1b] by [Grant Sanderson @3Blue1Brown](https://www.youtube.com/@3blue1brown) |
| \[mp\] | [Abstract Linear Algebra][mp] by [Michael Penn @MathMajor](https://www.youtube.com/@mathmajor) |
| \[jg\] | [Abstract Linear Algebra][jg], [Linear Algebra 1](https://www.youtube.com/playlist?list=PLBh2i93oe2quLc5zaxD0WHzQTGrXMwAI6), [2](https://www.youtube.com/playlist?list=PLBh2i93oe2qtXb7O-kPaEhAtFEb3n9Huu), and [3](https://www.youtube.com/playlist?list=PLBh2i93oe2quJ__clFXblfzPHWd-AiwRF) by [Julian Großmann @brightsideofmaths](https://www.youtube.com/@brightsideofmaths) |

Blogs:
| cited as | |
|:--:|:--|
| \[ku\] | [Math ∩ Programming](https://www.jeremykun.com/posts) by [Jeremy Kun](https://www.jeremykun.com/about) |

Visualization videos:
| |
|:--|
| [Matrices (and Matrix Manipulation)](https://www.youtube.com/watch?v=4csuTO7UTMo) by [Zach Star](https://www.youtube.com/@zachstar) |
| [SEE Matrix](https://www.youtube.com/playlist?list=PLWhu9osGd2dB9uMG5gKBARmk73oHUUQZS) by [Visual Kernel](https://www.youtube.com/@visualkernel) |
| [Linear Algebra Vids](https://www.youtube.com/playlist?list=PLs7Mi3VYqzLOVfC5HaVL0fOCflaiehIuT) by [QualityMathVisuals](https://www.youtube.com/@qualitymathvisuals) |

[3b1b]: https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab
[ax]: https://linear.axler.net
[cdtw]: https://www.math.ucdavis.edu/~linear/
[gq]: https://www.cis.upenn.edu/~jean/gbooks/linalg.html
[gs]: https://www.youtube.com/playlist?list=PL221E2BBF13BECF6C
[jg]: https://www.youtube.com/playlist?list=PLBh2i93oe2qvyrgm_lchujtMbEdXFAHEQ
[ko]: https://people.math.ethz.ch/~kowalski/script-la.pdf
[lns]: https://www.math.ucdavis.edu/~anne/linear_algebra/index.html
[mm]: http://www.math.clemson.edu/~macaule/classes/f23_math8530/
[mp]: https://www.youtube.com/playlist?list=PLVMgvCDIRy1wONAFFjMV9c0QaNuyfT5Gk
[st]: https://sites.google.com/a/brown.edu/sergei-treil-homepage/linear-algebra-done-wrong

## 1 Linear Algebra, Multilinear Algebra

No explicit definition of the terms _linear/multilinear algebra_ by authors:
[[3b1b][]], [[gq][]], [[gs][]], [[jg][]], [[ko][]], [[mp][]]

Different and overlapping definitions of these terms by:
[[ax][]], [[cdtw][]], [[em][]], [[lns][]], [[pm][]], [[wm][]], [[wp][]]

Multilinear algebra seems to be treated as \
\- a subfield of linear algebra by [[em][]], [[pm][]], [[gq][]] \
\- an extension by [[wp][]], [[ax][]] or a generalization by [[nl][]]

Best definitions (in my view): __[[pm:lia][]]__, __[[em:mla][]]__, see below.

| term | is the study of... |
|:--|:--|
| _linear algebra_ | [[ax:p1][ax]]: linear maps on finite-dimensional vector spaces, [[ax:pXIII][ax]]: main goal: understand the structure of linear operators |
| | _remark:_ unanswered which branch of mathematics then studies the infinite-dimensional case; but see quote\* below |
| | _remark:_ also see quote\*\* below from [[pm][]] cautioning about the narrow focus on the _finite-dimensional_ case |
| | |
| | [[lns][]]: solving systems of linear equations with a ﬁnite number of unknowns |
| | [[wm:lia][]]: linear sets of equations and their transformation properties |
| | _remark:_ narrow focus on linear equations, which is just one use case; also see quote\*\* |
| | |
| | [[cdtw][]]: vectors and linear transformations |
| | _remark:_ the main objects of study are not so much vectors but the structure of a vector space |
| | |
| | [[wp:lia][]], [[wm:clr][]]: linear systems of equations, linear maps, and their representations in vector spaces and through matrices |
| | _remark:_ short & simple, also see comment\*\*\* on use of linear algebra for nonlinear use cases |
| | |
| | __[[pm:lia][]]:__ the axiomatic treatment of linear structure based on the notions of a linear space and a linear mapping |
| | key topics (excerpts, see [[pm:lia][]] for detailed list): |
| | - linear structure: systems of linear equations, matrices, vector spaces, linear independence, basis, linear mappings |
| | - affine structure: determinants, Cramer’s rule, classical adjoint, geometric aspects, equiaffine transformations |
| | - diagonalization and decomposition: eigen-vector/value/space, characteristic polynomial, obstructions, structure theory |
| | - multi-linearity: vector space dual, bilinearity, bilinear forms, quadratic forms, tensor algebra, symmetry operations |
| | - Euclidean and Hermitian structure: inner product axioms, the adjoint operation, normal transformations, spectral theorem |
| | - computational methods: LU-factorization, QR decomposition, eigenvalue problems, singular value decomposition |
| | _remark:_ nice: going by _structures_ and _algorithms_ [[pm:str][]], [[pm:sig][]], [[pm:mot][]], [[pm:alg][]] |
| | |
| | [[em:lia][]]: vector/linear spaces; linear operators/mappings; linear, bilinear and quadratic functions/functionals/forms |
| | - 18th..19th century: theory of linear algebraic equations, determinant, Cramer's rule, Gauss method; matrix, rank |
| | - 20th century: vector space, linear transformation; linear, bilinear, multilinear functions on a vector space |
| | _remark:_ nice summary of historical development and expansion of this field |
| | |
| | [[nl:lia][]]: linear algebra over a skewfield $`K`$ is the study of the category $`K`$-Vect, that is the study of vector spaces over $`K`$ |
| | sometimes one uses the term '$`K`$-linear algebra' to mean an associative algebra over $`K`$ (compare '$`K`$-linear map') |
| | _remark:_ jargon-laden; introduces terms "linear algebra _over_ $`K`$", "$`K`$-linear algebra" not seen in other encyclopedias |
| | _remark:_ also see quote \*\*\*\* on edge cases and generalizations |
| | |
| _multilinear algebra_ | [[wp:mla][]]: functions with multiple vector-valued arguments, with functions being linear maps with respect to each argument |
| | __[[em:mla][]]:__ multilinear mappings between modules (in particular, vector spaces) |
| | |
| | [[em:mla][]]: bilinear + quadratic forms; determinants, Grassmann calculus; tensor on a vector space, multilinear forms |
| | [[ax:pXVII][ax]]: bilinear forms, quadratic forms, multilinear forms, tensor products, determinants (via alternating multilinear forms) |
| | __question:__ in what way are _quadratic forms_ multilinear, to be studied as part of multilinear algebra? |
| | |
| | [[nl:mla][]]: multilinear maps and constructions, usually formulated as: |
| | - linear-algebra constructions on tensor products (especially tensor powers) of vector spaces |
| | - their subspaces and quotient spaces that have some symmetry with respect to a natural action of a symmetric group |
| | [[nl:lia][]]: if one is interested in tensor products as well, then one gets a generalization called multilinear algebra: |
| | - tensor algebra, tensors, exterior and symmetric algebras are some of the main characters in that theory |
| | - study of determinants is important in the usual linear algebra but it is also closely related to the study of exterior algebras |
| | |
| _higher linear algebra_ | [[nl:hla][]]: the higher algebra/higher structures refinement of ordinary linear algebra [[nl:hia][]], [[nl:his][]] |
| | _remark:_ this term seems primarily used by the [[nl][]] community, not seen in other encyclopedias |

\* from [[nl:lia][]]:
> In infinite dimensions one rarely studies purely algebraic version, which is
> considered as a linear algebra, but more often one equips them with
> topological structure, what enters the subject of functional analysis.

\*\* from [[pm:leq][]], [[pm:flp][]]:
> Elementary treatments of linear algebra focus almost exclusively on
> _finite-dimensional_ linear problems. They neglect to mention the underlying
> mapping, preferring to focus instead on "variables and equations." \
> \- However, the scope of the general concept is considerably wider,
> e.g. linear differential equations such as $`y''+y=0`$. \
> \- Polynomial interpolation is a linear problem, but one that is specified
> abstractly, rather than in terms of variables and equations.

\*\*\* from [[wp:lia][]]:
> For nonlinear systems, linear algebra is often used for dealing with first-order approximations.

\*\*\*\* from [[nl:lia][]]:
> Classical linear algebra is done over a real-closed field or an
> algebraically closed field of characteristic zero.  Fancier linear algebra
> is done over incomplete fields and fields with positive characteristic (and
> constructively over nondiscrete fields).

## 2 Vector Space

A _vector space_ is a set whose elements, often called _vectors,_ can be added
together and multiplied by numbers called _scalars._ [[wp:ves][]]

Vector spaces generalize _Euclidean vectors,_ which allow modeling of physical
quantities (such as forces and velocity) that have not only a magnitude, but
also a direction. [[wp:ves][]]

The concept of vector spaces is fundamental for _linear algebra,_ together
with the concept of _matrices,_ which allows computing in vector spaces.
[[wp:ves][]]

### 2.1 Vector space, field, module, ring, vector axioms

| | |
|:--|:--|
| a _vector space_ or _linear space_ ... | a set that is closed under finite vector addition and scalar multiplication [[wm:ves][]], [[wm:clr][]] |
| $`\qquad`$... _over a field_ $`F`$ (or $`K`$) | a given, fixed _field_ (see below); (german: K=Körper=body); called: the _ground field_ or _base field_ [[wp:grf][]], [[nl:grf][]] |
| $`\qquad`$ short: an $`F`$_-vector space_ | is a triple $`(V,\oplus,\odot)`$ with ... [[wp:ves][]], [[wm:ves][]], [[em:ves][]], [[pm:ves][]], [[nl:ves][]] |
| $`\qquad{}V`$ | $`\qquad{}`$ a non-empty set of elements, typically called _vectors_ |
| $`\qquad{}\oplus:V\times{V}\to{V}`$ | $`\qquad{}`$ a binary operation called _vector addition_ or _addition_ and denoted by $`+`$ |
| $`\qquad{}\odot:F\times{V}\to{V}`$ | $`\qquad{}`$ a binary function called _scalar multiplication_ and denoted by juxtaposition |
| | ... that satisfy the _vector axioms_ (1)..(8), see below |
| | equivalently: a _module_ over a _ring_ that is a _field_ or a _division ring_ (see below) [[pm:ves][]], [[nl:ves][]] |
| | equivalently: an Abelian group, written additively, in which multiplication of the elements by scalars is defined [[em:ves][]] |
| $`\qquad\text{Vect}_k`$ | the _category_ whose objects are $`k`$_-vector spaces_ and morphisms are $`k`$_-linear maps_ (see [3][r3]) [[nl:kvs][]] |
| | |
| a _field_ | a set on which addition, subtraction, multiplication, division are defined... |
| | ... and behave as the corresponding operations on $`\mathbb{Q}`$, $`\mathbb{R}`$, $`\mathbb{C}`$ (but not $`\mathbb{Z}`$, which is only a _ring_) [[wp:fld][]], [[em:fld][]] |
| | equivalently:
| | - addition and multiplication satisfy associativity, commutativity, distributivity, identity, inverses [[wp:fld][]], [[wm:fld][]] |
| | - 2x binary/unary/nullary operations (addition, multiplication, inverses, constants 0 and 1) with axioms [[wp:fld][]] |
| | - a simple non-zero, commutative, associative _ring_ containing a unit [[em:fld][]] |
| | |
| a _vector_ | can refer to: [[wp:vec][]], [[wm:vec][]], [[em:vec][]], [[pm:vec][]] |
| | - abstractly, an element of a vector space when discussing general properties [[wp:vec][]] |
| | - a _coordinate vector_ as a tuple that describes the vector in terms of a particular, _ordered basis_ [[wp:coo][]], [[pm:coo][]], see [2.7][r2.7] |
| | - a _list vector_ as an indexed sequence; use of superscripts (column vector) or subscripts (row vector) [[pm:liv][]] |
| | - a _geometric_ or _Euclidean vector_ as an object with a magnitude (length, modulus) and a direction [[wp:geo][]], [[pm:geo][]] |
| | - a _physical vector_ as a quantity with magnitude and direction, represented relative to a _coordinate system_ [[pm:phy][]], see [2.7][r2.7] |
| | - colloquially, a quantity that cannot be expressed by a single number or _scalar_ (see below)...
| | $`\;`$ ...even in contexts where vector-space operations do not apply (such as a _logical vector_ of 0s and 1s) [[wp:vec][]] |
| | is typically _not_ used for other mathematical structures that form vectors spaces, such as: [[wp:vec][]] |
| | - an _algebra over a field_ [[wp:alf][]], [[wm:alf][]], [[em:alf][]] |
| | - a _function space_ [[wp:fus][]], [[wm:fus][]], [[pm:fus][]] |
| a _scalar_ | is an element of the field used to define a vector space [[wp:sca][]], [[wm:sca][]], [[em:sca][]], [[nl:sca][]] |
| | scalars are unaffected by changes to a _basis_ (see [2.7][r2.7]), such as coordinate rotation [[wp:sca][]], [[wm:sca][]], [[em:sca][]] |
| | |
| a _module_ | a generalization of a vector space when the field $`F`$ is replaced by a _ring_ (see below) |
| | [[wp:mod][]], [[wm:mod][]], [[em:mod][]], [[pm:mod][]], [[pm][pm:mod1]], [[pm][pm:mod2]], [[pm][pm:mod3]], [[nl:mod][]] |
| | |
| a _ring_ or _number ring_ | a generalization of a field: multiplication need not be commutative, its identity and inverses need not exist [[wp:rng][]] |
| | by mosts texts [[wm:rng][]], [[pm:rng][]], multiplication must be associative; but not by [[em:rng][]] |
| | by some texts [[nl:rng][]], the multiplicative identity must exist; such rings are also called _unital_ or _unitary_ [[pm:rng][]] |
| a _division ring_ or _skew field_ | a nontrivial, unital ring in which division by nonzero elements is defined [[wp:dir][]], [[wm:dir][]], [[em:dir][]], [[pm:dir][]], [[nl:dir][]] |

[pm:mod1]: https://planetmath.org/Module
[pm:mod2]: https://planetmath.org/Module1
[pm:mod3]: https://planetmath.org/ExamplesOfModules

#### Vector axioms:

For a vector space $`V`$ over a field $`F`$:

| axiom(s) | for all $`{u},{v},{w}\in{V}`$ and $`\alpha,\beta\in{F}`$: | |
|:--:|:--|:--|
| | | [[wp:ves][]], [[wm:ves][]], [[em:ves][]], [[pm:ves][]] |
| (1) | $`{u}+({v}+{w})=({u}+{v})+{w}`$ | associativity of vector addition |
| (2) | $`{u}+{v}={v}+{u}`$ | commutativity of vector addition |
| (3) | $`{v}+{0}={v}\;`$ where $`\;{0}\in{V}`$ | existence of identity of vector addition, called the _zero vector,_ by (2): $`\;{0}+{v}={v}+{0}\;`$ |
| (4) | $`{v}+(−{v})={0}\;`$ where $`\;(−{v})\in{V}`$ | existence of additive inverse elements |
| | | = a vector space is an _abelian_ or _commutative group_ under addition |
| | | |
| (5) | $`\alpha(\beta{v})=(\alpha\beta){v}`$ | associativity of scalar multiplication |
| (6) | $`1{v}={v}\;`$ where $`\;1\in{F}:\;1\alpha=\alpha`$ | existence of identity of scalar multiplication |
| (7) | $`(\alpha+\beta){v}=\alpha{v}+\beta{v}`$ | distributivity of scalar sums |
| (8) | $`\alpha({u}+{v})=\alpha{u}+\alpha{v}`$ | distributivity of vector sums |
| | | = scalar multiplication forms a _ring homomorphism_ from $`F`$ into the _endomorphism ring_ of this group |

#### Examples:

- As _fields,_ the rational numbers $`\mathbb{Q}`$, real numbers
  $`\mathbb{R}`$, and complex numbers $`\mathbb{C}`$ are each also a vector
  space over themself. [[wp:fld][]], [[wm:fld][]], [[em:fld][]], also see
  [[ku](https://www.jeremykun.com/2014/02/26/finite-fields-a-primer)]
- As _extension fields,_ $`\mathbb{R}`$ is also a vector space over
  $`\mathbb{Q}`$; and $`\mathbb{C}`$ is also a vector space over
  $`\mathbb{R}`$. [[wp:ves][]], [[wp:exf][]],
  [[wm:exf][], [[em:exf][]]
- _Euclidian $`n`$-space_ (or _Euclidian vector space,_ _Cartesian space,_ or
  simply _$`n`$-space_) commonly denoted $`\mathbb{R}^n`$. \
  The space of all $`n`$-tuples of real numbers,
  $`(x_1,x_2,...,x_n)`$. [[wm:eus][]], [[wp:eus][]], [[em:eus][]],
  [[pm:eus][]], [[wp:tup][]], [[wm:tup][]], [[em:tup][]], [[wm:clr][]]
- _Polynomial rings:_ The set of polynomials in one (or more) indeterminates
  $`X`$ over a field (or a ring) $`F`$. \
  Equipped with addition, multiplication, and scalar multiplication (and even
  multiplication of two polynomials, wich also makes them an _algebra over a
  field_). [[wp:ves][]], [[wp:por][]], [[wm:por][]], [[em:por][]],
  also see [[ku](https://www.jeremykun.com/2013/04/30/rings-a-primer)]
- A _function space_ is a vector space whose "points" are functions. \
  For a field $`F`$ and any set $`X`$, the set of functions $`X\to{F}`$ form a
  vector space over $`F`$ with addition and scalar multiplication are defined
  pointwise: \
  $`(f+g)(x)=f(x)+g(x)\;`$ and $`\;(c\cdot{f})(x)=c\cdot{f}(x)\;`$
  for any $`\;f,g\colon{}X\to{F}`$, any $`x\in{X}`$, and any $`c\in{F}`$.
  [[wp:ves][]], [[wp:fus][]], [[pm:fus][]], [[wm:fus][]]

### 2.2 Linear combination

For a vector space $`V`$ over a field $`F`$:

| | for a finite\* sequence\** (list) of vectors $`{v}_1,...,{v}_n`$ in $`V`$ and of scalars $`a_1,...,a_n`$ in $`F`$ |
|:--|:--|
| a _linear combination_ or _superposition_ | can refer to: |
| | (1) the value/vector $`\;w=a_1{v}_1+\cdots+a_n{v}_n=\sum_{i=1}^{n}{a_i}{v_i}\;`$ [[wp:lic][]], [[wm:lic][]], [[pm:lic][]] |
| | (2) an expression built from a set of terms by summing over the product of each term with a constant [[wp:lic][]] |
| | |
| | there is some usage ambiguity, for example: [[wp:lic][]] |
| | (1) as value: "The set of all linear combinations of $`v_1,...,v_n`$ always forms a subspace." |
| | (2) as expression: "Two different linear combinations can have the same value." |
| | |
| $`\qquad(v_i)_{i\in{I}}\;`$ and $`\;(a_i)_{i\in{I}}`$ | \*\* generalizations: some authors choose _$`I`$-indexed families_ of vectors $`(v_i)`$ and scalars $`(a_i)`$ over sequences: [[gq][]] |
| | $`\qquad`$ the index set $`I`$ is required to be _finite_ but does not have to be _ordered_ (unlike $`I=\{1,...,n\}\subset\mathbb{N}`$) [[gq][]] |
| | $`\qquad`$ then $`\;w=\sum_{i\in{I}}{a_i}{v_i}\;`$ is the linear combination, which is order-independent by axioms (1) and (2) [[gq][]] |
| | $`\qquad`$ with $`w=0`$ stipulated for $`I=\emptyset`$ [[gq][]] |
| | \* further, $`I`$ may be infinite if it has _finite support,_ that is, only finitely many nonzero scalars in $`(a_i)_{i\in{I}}`$ [[gq][]], [[wp:coo][]] |
| | other authors (per [[gq][]]) define a linear combination on a _set_ of vectors, which precludes duplicates among them |
| | |
| | generalization: the definition of _linear combination_ extends to _modules over a ring_ (and even further) [[nl:lic][]] |

#### Examples:

All these basic operations on a vector space are linear combinations: [[nl:lic][]]

| operations | arity | coefficients |
|:--|:--:|:--:|
| zero vector | 0 | none |
| identity operation; additive inverse | 1 | $`1\;;\;-1`$ |
| multiplication with a scalar $`a`$ | 1 | $`a`$ |
| vector addition; vector subtraction | 2 | $`(1,1)\;;\;(1,-1)`$ |
| the mean vector of $`n`$ vectors | $`n`$ | $`(\frac{1}{n},...,\frac{1}{n})`$ |

### 2.3 Affine/conical/convex combination

By restricting the coefficients in linear combinations, related operations are
defined, along with sets closed under the operation (_hull_):

| name of operation | restrictions | closure, see [2.5][r2.5] | examples |
|:--|:--|:--|:--|
| _linear combination_ | none | _linear hull/span_ | $`\mathbb{R}^n`$, linear subspace [[wp:lic][]] |
| _affine (linear) combination_ [[wp:afc][]], [[pm:afc][]], [[nl:afs][]] | $`\sum a_i=1`$ | _affine hull_ | line, affine subspace [[wp:lic][]] |
| _conical (linear) combination_ or _conical sum_ [[wp:cnc][]], [[nl:cns][]] | $`a_i\geq{0}\;\dagger`$ | _conical hull_ | quadrant, convex cone [[wp:lic][]] |
| _convex (linear) combination_ [[wp:cvc][]], [[pm:cvc][]], [[wm:cvc][]], [[nl:cvs][]] | $`a_i\geq{0}\;\ddagger\;`$ and $`\;\sum{}a_i=1`$ | _convex hull_ | line segment, convex set [[wp:lic][]] | |

$`\dagger\;`$ Testing for non-negativity requires an _ordered field,_ such as
$`\mathbb{R}`$ but not $`\mathbb{C}`$.
[[pm](https://planetmath.org/MathbbCIsNotanOrderedField)],
[[nl](https://ncatlab.org/nlab/show/positive+number)] \
[[wp:cnc][]], [[wp:cvc][]], [[pm:cvc][]], [[nl:cns][]] only define conical/convex
combinations for _real_ vector spaces.  Also see $`\ddagger`$.

| | |
|:--|:--|
| _ordered field_ | a field with a _total ordering_ of its elements that is compatible with the field operations [[wp:orf][]], [[pm:orf][]], [[em:orf][]] |

$`\ddagger\;`$ __Question:__ The requirement of an _ordered field_
__conflicts__ with _convex sets_ being well defined in _complex_ vector
spaces. \
[[pm:cvx][]]: For a _real_ or _complex_ vector space $`V`$, a subset $`S`$ of
$`V`$ is _convex:_ \
$`\qquad`$ if for all points $`x,y`$ in $`S`$, the line segment
$`\{\alpha{}⁢x+(1-\alpha)⁢y\;|\;\alpha\in[0,1]\}`$ is also in $`S`$. \
[[wm:cvc][]] defines _convex combination_ without reference to real or complex
vector spaces in terms of this $`\alpha`$-convexity criterion. \
[[pm:cvx][]] also shows how the notion of convexity can be generalized to any
_partially ordered_ set. \
[[wp](https://en.wikipedia.org/wiki/Complex_convexity)] hints at the notion of
_complex convexity._

### 2.4 Linear subspace, vector sum, intersection

Standard constructions that yield vector spaces related to given ones.

| for a vector space $`V`$ over $`F`$ | |
|:--|:--|
| a _linear subspace_ or _vector subspace_ $`W`$ of $`V`$ | a nonempty subset $`W`$ of $`V`$ that is closed under (finite) vector addition and scalar multiplication |
| | [[wp:ves][]], [[wp:lss][]], [[em:lss][]], [[pm:lss][]], [[nl:lss][]] |
| | equivalently: if $`W`$ is a subset of $`V`$ and a vector space over $`F`$ for the operations of $`V`$ [[pm:lss][]] [[wp:lss][]] |
| a _proper linear subspace_ | if  $`W\subset{}V`$ [[pm:lss][]] |

#### Properties:

- Any subspace of $`V`$ contains the zero-vector. [[wp:lss][]]
- A nonempty set $`W`$ is a subspace of $`V`$ iff every linear combination of
  finitely many elements of $`W`$ also belongs to $`W`$. [[wp:lss][]]

#### Examples: vector sum, intersection

- $`V`$ and $`\{0\}`$ are linear subspaces of $`V`$, also called the _trivial
  subspaces._ [[pm:lss][]], [[wp:lss][]]
- For linear subspaces $`S,T`$ of $`V`$: [[pm:lss][]] \
  the _vector sum_ $`S+T=\{s+t\in{V}|s\in{S},t\in{T}\}`$ and \
  the intersection $`S\cap{}T=\{u\in{V}∣u\in{S},u\in{T}\}`$ \
  are subspaces of $`V`$.

#### Theorem:

A vector space over an infinite field cannot be a finite union of proper
subspaces of itself.
[[pm](https://planetmath.org/VectorSpaceOverAnInfiniteFieldIsNotAFiniteUnionOfProperSubspaces)]

### 2.5 Hulls, span, spanning set

For a subset $`S`$ of a vector space $`V`$, the closure of $`S`$ under the
respective combinations (set of all finite combinations) are denoted as:

| combination | closure | written | |
|:--|:--|:--|:--|
| _linear_ | the _(linear) hull/span/envelope_ of $`S`$ | $`span(S)`$ or $`Sp(S)`$ or $`\langle{S}\rangle`$ | [[pm:lih][]], [[wp:lih][]], [[wm:lih][]], [[em:lih][]] |
| _affine_ | the _affine hull/span_ of $`S`$ | $`\text{aff}(S)`$ | [[wp:afh][]], [[wm:afh][]], [[pm:afc][]], [[wp:afs][]], [[nl:afs][]] |
| _conical_ | the _conical hull_ of $`S`$ | $`cone(S)`$ or $`coni(S)`$ | [[wp:cnc][]], [[nl:cns][]] |
| _convex_ | the _convex hull/envelope_ $`\dagger`$ of $`S`$ | $`co(S)`$ or $`conv(S)`$ | [[wp:cvh][]], [[wp:cvc][]], [[pm:cvc][]], [[wm:cvc][]], [[em:cvh][]], [[nl:cvs][]] |

$`\dagger`$ Also called _convex closure_ [[wp:cvh][]], which can be confused
with terminology of _topological vector spaces:_ \
\- the _linear closure_ of $`S`$ is the intersection of all _closed_ subspaces
   containing $`S`$; [[em:lih][]] \
\- the _closed convex hull_ is the _closure of the convex hull._
   [[em:cvh][]]

_Span_ denotes both a noun and a verb:

| for subsets $`S,T`$ of a vector space $`V`$ | |
|:--|:--|
| $`S`$ _spans_ $`T\;`$ or $`\;S`$ is a _spanning set_ of $`T`$ | if $`T`$ is the linear span of $`S`$ [[wp:lih][]], [[pm:lih][]], [[em:lih][]] |
| $`S`$ is a _generator set_ of $`T\;`$ or $`\;T`$ is _generated_ by $`S`$ | if $`S`$ _spans_ $`T`$ [[wp:lih][]], [[pm:lih][]] |
| | generalizes to: modules over a ring (though, no generator set may exist) [[pm:lih][]] |
| $`V`$ is _finitely generated_ | if a vector space (or module) $`V`$ admits a generating set that is finite [[nl:veb][]], [[pm:fgm][]] |

#### Theorems:

For the _span_ or _linear hull_ of a subsets $`S`$ of a vector space $`V`$
holds:
- The _span_ of the empty set is the singleton consisting of the zero vector.
  [[pm:lih][]], [[wp:lih][]]
- The _span_ of $`S`$ is the smallest (for set inclusion) linear subspace
  containing $`S`$. [[wp:lih][]]
- If $`S\subseteq{}T`$ for a subsets $`T`$ of $`V`$, then
  $`span⁡(S)\subseteq{}span⁡(T)`$.  Hence, if $`span⁡(S)=V`$, every superset of
  $`S`$ spans $`V`$. [[pm:pls][]]

See [[pm:lih][]], [[wp:lih][]] for more properties of linear spans.

#### Other Properties:

The _affine hull_ of a subset $`S`$ of a vector space $`V`$ is:
- the smallest affine subspace containing $`S`$; [[wp:afh][]], [[pm:afc][]]
- the intersection of all affine sets containing $`S`$; [[wp:afh][]]
- the _ideal_ generated by $`S`$. [[wm:afh][]]
- See [[wp:afh][]] for a list of properties of the _affine hull_ (presented
  for Euclidean space).

The _conical hull_ of a subset $`S`$ of Euclidean space is:
- a convex set; [[wp:cnc][]]
- the intersection of all convex cones containing $`S`$ plus the origin.
  [[wp:cnc][]]

The _convex hull_ of a subset $`S`$ of Euclidean space may be defined as:
[[wp:cvh][]]
- the (unique) smallest convex set containing $`S`$; [[wp:cvh][]],
  [[pm:cvc][]], [[em:cvh][]], [[wp:cvx][]]
- the intersection of all convex sets containing $`S`$; [[wp:cvh][]],
  [[em:cvh][]], [[em:cvx][]], [[pm:cvx][]], [[wp:cvx][]]
- the union of all simplices with vertices in $`S`$; [[wp:cvh][]]
- the set of all convex combinations of points in $`S`$ (as above);
  [[wp:cvh][]]
- the set of possible locations of the centre of gravity of a mass
  (barycenter) which can be distributed in $`S`$ in different manners.
  [[em:cvh][]], [[wp:afs][]]

### 2.6 Linear independence

| For a subset $`S`$ of a vector space $`V`$ | |
|:--|:--|
| a finite set $`S`$ is _linearly dependent_ | if there exist scalars not all zero such that the linear combination is the zero vector; |
| $`\qquad`$ ... _linearly independent_ | otherwise [[pm:lii][]], [[wp:lii][]] [[wm:lii][]], [[em:lii][]], [[nl:lii][]] |
| an infinite set $`S`$ is _linearly independent_ | if every nonempty finite subset is linearly independent; |
| $`\qquad`$ ... _linearly dependent_ | otherwise [[wp:lii][]], [[pm:lii][]] |
| | |
| | generalizations: these definitions extend to |
| | - indexed families of vectors [[gq][]], [[wp:lii][]] |
| | - functions and equations seen as vectors [[wm:lii][]] |
| | - a _module over a ring_ (or _rig_) [[pm:lii][]], [[nl:lii][]] |

#### Properties:

- A (possily infinite) set of vectors is linearly dependent \
  \- if one of the vectors is a linear combination of others; [[wp:lii][]],
  [[pm:pli][]] \
  \- if one of them is zero or if the set contains the same vector twice.
  [[wp:lii][]], [[pm:pli][]]
- Every spanning set $`S`$ of $`V`$ must contain at least as many elements as
  any linearly independent set of vectors from $`V`$. [[wp:lih][]]
- If a set of vectors is linearly independent, so is any subset of it.
  [[pm:pli][]]
- If $`S_1\subseteq{}S_2\subseteq{}\cdots`$ is a chain of linearly independent
  subsets, so is their union. [[pm:pli][]]

### 2.7 Vector basis, coordinate systems

For a vector space $`V`$ over a field $`F`$:
| | |
|:--|:--|
| a _(vector) basis_ or _base_ of $`V`$ | a subset of vectors in $`V`$ that are linearly independent and span $`V`$ [[wp:veb][]], [[wm:veb][]] |
| $`\qquad`$ plural: _bases_ | of both _basis_ and _base_ [[nl:bas][]] |
| | also called: _linear basis,_ _algebraic basis,_ _Hamel basis_ [[nl:veb][]], [[pm:veb][]] |
| | $`\qquad`$ these terms stress that there is no connection with additional structures defined on $`V`$ [[em:bas][]] |
| a _basis vector_ | an element of a chosen vector basis [[wm:bav][]] |
| | |
| | generalizations: |
| | - a basis of a module over a ring (though, no basis may exist), see [[em:bas][]], [[pm:lih][]], [[wp:lih][]] |
| | - a _topological (linear) basis_ for the linear+topological structure of a _topological vector space_ [[em:bas][]], [[nl:tlb][]] |
| | $`\quad\neq`$ a _topological base:_ a basis for the topology of a _topological space_ [[nl:tob][]], [[pm:tob][]], [[em:tob][]]], [[wp:tob][]] |
| | - a _free basis_ of an algebraic system, see [[em:bas][]] |
| | - a basis of a set $`X`$ as a (in some sense) _minimal_ subset $`B`$ that _generates_ $`X`$, see [[em:bas][]] |
| | |
| an _ordered basis_ $`B`$ of $`V`$ | a basis $`B=\{b_i\}_{i{\in}I}=(b_i)_{i{\in}I}`$ whose vectors are $`I{\subseteq}\mathbb{N}`$_-indexed_ as tuple, rows/columns in a matrix, or sequence [[wp:veb][]] |
| | if $`B`$ (and $`I`$) is infinite, it must have _finite support_ (due to linear combinations defined as finite sums), see [2.2][r2.2] |
| | also called: a _(coordinate) frame,_ for example, a _Cartesian frame_ or an _affine frame_ [[wp:veb][]] |
| the _coordinate vector_ $`[v]_B`$ ... | a tuple or sequence of (finitely many nonzero) scalars $`[v]_B=(a_i)_{i{\in}I}`$ such that $`\;v=\sum_{i{\in}I}{a_i}{b_i}\;`$ for ... |
| $`\qquad`$... of $`v{\in}{V}`$ relative to $`B`$ | $`\qquad`$ the basis $`B=(b_i)_{i{\in}I}\;`$ (which may be omitted if understood from the context) [[wp:coo][]], [[pm:coo][]] |

#### Examples:

- The trivial vector space consisting only of its zero vector has the empty
  set is a basis. [[pm:veb][]], [[wp:dim][]]
- The vector space $`\mathbb{R}^3`$ has
  $`\{[1\,0\,0]^T,[0\,1\,0]^T,[0\,0\,1]^T\}`$ as a _standard basis._
  [[wp:dim][]], [[wp:svb][]]
- The set of _monomials_ $`\{1,x,x^2\}`$ is a basis for the vector space of
  _polynomials_ with degree at most 2, over a division ring. [[pm:veb][]],
  [[wp:mon][]], [[wp:pol][]]
- The set
  $`\big\{\big[\begin{smallmatrix}1&0\\0&0\end{smallmatrix}\big],\big[\begin{smallmatrix}0&1\\0&0\end{smallmatrix}\big],\big[\begin{smallmatrix}0&0\\1&0\end{smallmatrix}\big],\big[\begin{smallmatrix}0&0\\0&1\end{smallmatrix}\big]\big\}`$
  is a basis for the vector space of $`2{\times}2`$-_matrices_ over a division
  ring. [[pm:veb][]], [[wp:mat][]]

#### Properties:

Given a basis $`B`$ of a vector space $`V`$ over $`F`$:
- Each vector $`v\in{V}`$ is _uniquely_ $`\dagger`$ described by its
  coordinate vector $`[v]_B`$ as a linear combination of the basis vectors.
  [[em:bas][]], [[pm:coo][]]
- (For a finite basis $`|B|=n`$:) the map $`\varphi:[v]_B\mapsto{v}`$ is a
  _linear isomorphism_ (see [3.1][r3.1]) between $`F^n`$ and $`V`$, hence
  $`F^n\cong{V}`$. [[wp:veb][]], [[pm:coo][]]
- In other words $`F^n`$ is the _coordinate space_ of $`V`$, and
  $`\varphi^{-1}(v)=[v]_B`$ yields the coordinate vector of $`v`$.
  [[wp:veb][]]

$`\dagger`$ __Question:__ What about counterexamples to the uniqueness of
coordinate vectors? \
_Polar coordinates_ (see below) are _not_ inherently unique:
[[wm:poc][]], [[wp:poc][]] \
\- $`(r,\varphi+2n\pi)=(r,\varphi)`$ for any integer $`n`$, \
\- the pole can be expressed as $`(0,\varphi)`$ for any angle $`\varphi`$.

Excerpt from [[wp:poc][]]:
> Where a unique representation is needed, it is usual to limit $`r`$ to
> $`r\gt{0}`$ and $`\varphi`$ to the interval $`[0,2\pi)`$ or [...]. \
> In all cases a unique azimuth for the pole $`(r=0)`$ must be chosen, e.g.,
> $`\varphi=0`$.

#### Examples: coordinate systems

By above properties, an ordered (finite) basis provides a _coordinate system,_
which locates every vector by a list of numbers relative to the basis vectors.
[[wp:cos][]], [[wm:cos][]], [[pm:cos][]], [[em:cos][]], [[nl:cos][]]

For $`n`$-space, common coordinate systems are:

| | |
|:--|:--|
| $`\mathbb{R}^1`$ | _real number line:_ one (Cartesian) coordinate [[wm:rnl][]], [[wp:rnl][]] |
| $`\mathbb{R}^2`$, $`\mathbb{C}`$, $`\mathbb{R}^{n+1}`$ | _Cartesian coordinates:_ $`n+1`$ rectilinear coordinates [[em:cac][]], [[wm:cac][]], [[pm:cac][]], [[wp:cac][]] |
| $`\mathbb{R}^2`$, $`\mathbb{C}`$ | _polar coordinates:_ distance from origin and angle (spherical coordinates) [[em:poc][]], [[wm:poc][]], [[nl:poc][]], [[pm:poc][]], [[wp:poc][]] |
| $`\mathbb{R}^3`$, $`\mathbb{R}^{n+2}`$ | _(hyper-) spherical coordinates:_ distance from origin and $`n+1`$ angles [[em:spc][]], [[wm:spc][]], [[pm:spc][]], [[wp:spc][]] |
| $`\mathbb{R}^3`$ | _cylindrical coordinates:_ polar coordinates and $`z`$ axis [[em:cyc][]], [[wm:cyc][]], [[pm:cyc][]], [[wp:cyc][]] |

The use of a _coordinate system_ translates problems in geometry into problems
about numbers and vice versa; this is the basis of analytic geometry.
[[wp:veb][]]

Excerpts from [[em:cos][]]:
> The coordinate method has become useful not only as a means toward the
> algorithmization of reasoning [...] but also for the discovery of new facts
> and relationships. \
> For example, the consistency of Euclidean geometry is reduced via the use
> of coordinates to the consistency of arithmetic.

> Despite that many branches of mathematics, such as Riemannian geometry, can
> be presented in a "coordinate-free" form, \
> ... the results are most frequently achieved by the coordinate method, or
> more precisely via the judicious choice of coordinate systems for the
> specific problem at hand.

### 2.8 Dimension, hyperplane, infinite-dimensional vector space

For a vector space $`V`$ over a field $`F`$:
| | |
|:--|:--|
| the _dimension_ $`\;\dim_F(V)`$ | the cardinality of a basis of $`V`$ over $`F`$ [[wp:dim][]], [[wp:dit][]], [[pm:dim][]] |
| $`\qquad`$ or $`\dim(V)`$ | $`\qquad`$ the ground field $`F`$ may be omitted if understood from the context [[wp:dim][]] |
| | $`\qquad`$ the dimension depends on the ground field: $`\dim_{\mathbb{R}}(\mathbb{C})\neq\dim_{\mathbb{C}}(\mathbb{C})`$, see example below [[wp:dim][]] |
| | also called: _algebraic dimension_ or _Hamel dimension_ [[em:bas][]] |
| | every vector space admits a basis as a famous classical consequence of the axiom of choice, see [[2.9][r2.9]] |
| | the dimension is uniquely defined: all bases have the same cardinality (number of elements), see [[2.9][r2.9]] |
| | |
| | specializations: there are many other notions of _dimension_ of spaces, for example: [[nl:dim][]] |
| | - a _topological space_ has a (Lebesgue) _covering dimension_ [[nl:dim][]] |
| | - a _metric space_ has a _Hausdorff dimension_ [[nl:dim][]] |
| | - all have in common that the Cartesian space $`\mathbb{R}^n`$ has dimension $`n`$ [[nl:dim][]] |
| | |
| a vector space is _finite-dimensional_ | if its dimension is finite; [[wp:dim][]], [[pm:dim][]] |
| $`\qquad`$ ... _infinite-dimensional_ | otherwise |
| | |
| a _hyperplane_ | a linear subspace of dimension $`n−1`$ [[wp:ves][]], [[wp:hyp][]], [[wm:hyp][]], [[em:hyp][]] |
| a _plane_ | a linear subspace of dimension 2 [[wp:ves][]] |
| a _(vector) line_ | a linear subspace of dimension 1 [[wp:ves][]] |

#### Examples:

- The trivial vector space consisting only of its zero vector is the only
  vector space with dimension $`0`$. [[wp:dim][]]
- The complex numbers $`\mathbb{C}`$ are both a real and complex vector space
  with $`\;\dim_{\mathbb{R}}(\mathbb{C})=2\;`$ and
  $`\;\dim_{\mathbb{C}}(\mathbb{C})=1`$. [[wp:dim][]]
- For any field $`F`$: $`\;\dim_F(F^n)=n`$. [[wp:dim][]]
- Finite-dimensional vector spaces occur naturally in geometry and related
  areas. [[wp:ves][]]
- Infinite-dimensional vector spaces occur in many areas of mathematics.
  [[wp:ves][]]
- _Polynomial rings_ are _countably infinite-dimensional_ vector spaces;
  [[wp:ves][]], [[wp:por][]], [[wm:por][]], [[em:por][]]
- Many _function spaces_ have the _cardinality of the continuum_ as a
  dimension. [[wp:ves][]], [[wp:veb][]], [[wp:fus][]], [[wm:fus][]]

#### Infinite-dimensional vector space:

Excerpts from [[nl:tlb][]], [[nl:veb][]]:
> Bases in linear algebra are extremely useful tools for analysing problems.
>
> Using a basis, one can often rephrase a complicated abstract problem in
> concrete terms, perhaps even suitable for a computer to work with.

> In infinite dimensions, having a basis becomes more valuable as the spaces
> get more complicated.
>
> However, the notion of a basis also becomes (more) complex: depending on
> whether we allow (only) finite sums, sequences, or infinite sums.

> In practice, infinite-dimensional vector spaces tend to appear and to be
> understood with extra structure (typically that of topological vector
> spaces).
>
> In which cases there are more appropriate notions of linear bases for them
> (e.g., allow infinite-linear combinations subject to a condition of
> convergence of a sequence).

### 2.9 Basis and dimension theorems

#### Basis theorem for vector spaces:

Every vector space has a basis. [[nl:bat][]], [[pm:bat][]], [[wp:dim][]],
[[wp:dit][]], [[wp:veb][]], [[wm:veb][]]

Generalization: [[nl:bat][]], [[pm:bat][]] \
\- In a vector space $`V`$, given any linearly independent set $`I`$ and
   spanning set $`S`$: \
   $`\qquad`$ if $`I\subseteq{S}`$, then there is a basis $`B`$ of $`V`$ with
   $`I\subseteq{B}\subseteq{S}`$. \
\- The basis theorem is the special case where $`I=\emptyset`$ and
   $`S=V`$.

The theorem is a famous classical consequence of the _axiom of choice_ and is
equivalent to it. [[nl:bat][]], [[pm:bat][]], [[wp:aoc][]]

From [[pm:bat][]]:
> This result, trivial in the finite case, is in fact rather surprising for
> infinite-dimensionial vector spaces [...]: \
> just try to imagine a basis of the vector space of all continuous mappings
> $`f:\mathbb{R}\to\mathbb{R}`$.

From [[nl:veb][]]:
> Since a linear combination is defined to be a sum of finitely many vectors,
> a basis of a vector space must be such that every vector in the space is the
> (unique) combination of finitely many basis elements – even if there are
> infinitely many elements in the basis.

> For an infinitely-generated vector space it is not in general possible to
> construct a (Hamel-)basis, but the existence of such a basis is nevertheless
> implied, in classical mathematics, by Zorn's lemma (essentially a form of the
> axiom of choice): This is the content of the basis theorem.

#### Dimension theorem for vector spaces:

Any two bases of a vector space have the same cardinality. [[wp:dit][]],
[[wp:dim][]], [[wm:veb][]], [[em:bas][]], [[nl:dim][]]

The dimension theorem is a consequence of the following theorem:
[[wp:dit][]], [[wp:sel][]] \
\- In a vector space $`V`$, given any linearly independent set $`I`$ and
   spanning set $`S`$: \
   $`\qquad`$ the cardinality of $`I`$ is not larger than the cardinality of
   $`S`$. \
\- A basis is a generating set that is linearly independent.

The uniqueness of the cardinality of the basis requires only the _ultrafilter
lemma,_ which is strictly weaker than the _axiom of choice._ [[wp:dit][]],
[[wp:aoc][]]

##### Corollaries:

- Given a vector space $`V`$, for any family $`B=(v_i)_{i\in{I}}`$ of vectors
  of $`V`$, the following properties are equivalent: [[gq][]] \
  (1) $`B`$ is a basis of $`V`$. \
  (2) $`B`$ is a maximal linearly independent family of $`V`$. \
  (3) $`B`$ is a minimal generating family of $`V`$.
- Here, maximal / minimal means that any proper superset / subset of $`B`$ is
  no longer linearly independent / a spanning set of $`V`$. [[pm:lii][]],
  [[pm:pls][]]
- Every spanning set of a vector space can be reduced to a basis by discarding
  linearly dependent vectors (assuming the axiom of choice). [[wp:lih][]]

#### Theorem: Vector spaces are completely characterized by their dimension

Any two vector spaces are _isomorphic_ if and only if their bases have equal
cardinalities. [[em:ves][]], [[wp:ves][]]  (For vector space isomorphism, see
[3.1][r3.1])

Any bijective map between their bases can be uniquely extended to a bijective
linear map between the vector spaces. [[wp:dim][]] (only stated for
finite-dimensional vector spaces) \
__Question:__ Why should this not hold for the infinite-dimensional case?

#### Dimension theorem for vector subspaces:

For linear subspaces $`S,T`$ of a (finite-dimensional?!) vector space $`V`$
and their \
\- _intersection_ subspace $`S\cap{}T`$ and \
\- _vector sum_ subspace $`S+T`$ (see [2.4][r2.4]):
[[pm:lss][]], [[pm:dfv][]]

$`\qquad\dim⁡(S\cap{}T)+\dim⁡(S+T)=\dim⁡(S)+\dim⁡(T)`$

For linear subspace $`W`$ of a finite-dimensional vector space $`V`$: [[pm:lss][]]

$`\qquad\dim(W)=\dim(V)`$ implies $`W=V`$

#### Theorem: cardinality of a vector space

The cardinality (number of elements) of a vector space $`V`$ is determined by
its dimension and the cardinality of the ground field $`F`$: [[pm:dfv][]]

$`\qquad|V|=\begin{cases}|F|\dim⁡(V)&\dim⁡(V)⁢\;\text{is\;finite}\\\max⁡\{|F|,\dim⁡(V)\}&\dim⁡(V)\;\text{is\;infinite}\end{cases}`$

More dimension formulae and theorems: [[pm:dfv][]]

### 2.10 Quotient space, codimension, relative dimension

XXX: cleanup

[[pm:dim][]]
For a subspace $`U{\subset}V`$, the dimension of the quotient vector space
$`V/U`$ is called the _codimension_ of $`U`$ relative to $`V`$.

More generally, a hyperplane (see [XXX]) is any _codimension-1_ vector subspace of a
vector space, where the _codimension_ of $`W`$ in $`V`$ is the difference
$`\operatorname{codim}(W)=\dim(V)-\dim(W)`$.  This is the dual notion of
_relative dimension_ (see below). [[wp:cod][]], [[wm:cod][]],
[[em:cod][]]

Equivalently, a hyperplane $`W`$ in a vector space $`V`$ is any subspace such
that the _quotient space_ $`V/W`$ is one-dimensional (see below).
[[wp:hyp][]], [[wm:hyp][]], [[em:hyp][]]

The _quotient_ of a vector space $`V`$ by a subspace $`N`$ is a vector space
obtained by mapping ("collapsing") $`N`$ into the equivalence class of the
zero vector.  The space obtained is called a _quotient (vector) space_ and is
denoted $`V/N`$ ("$`V`$ mod $`N`$" or "$`V`$ by $`N`$"). [[wp:qvs][]]

For a vector space $`V`$ over a field $`\mathbb{F}`$ and a subspace $`N`$ of
$`V`$, an equivalence relation $`\sim`$ on $`V`$ is defined as $`x\sim{y}`$ if
$`x-y\in{N}`$.  That is, $`x`$ is related to $`y`$ if and only if one can be
obtained from the other by adding an element of $`N`$. [[wp:qvs][]]

The equivalence relation $`\sim`$ induces a partition of $`V`$ consisting of
equivalence classes, also called the _quotient set:_ \
$`V/{\sim}:=\{[x]:x\in{V}\}`$ with $`[x]:=\{x+n:n\in{N}\}`$ [[wp:qvs][]],
[[wp:eqc][]]

The quotient space $`V/N`$ is then defined as $`V/{\sim}`$ with addition and
scalar multiplication are defined on the equivalence classes as \
\- $`[x]+[y]=[x+y]`$ \
\- $`\alpha[x]=[\alpha{x}]`$ for all $`\alpha\in\mathbb{F}`$ [[wp:qvs][]]

Note that all of $`N`$ maps to $`[0]`$ per $`N=\{0+n:n\in{N}\}=[0]`$.  This
way, the quotient space "forgets" information contained in the
subspace $`N`$. [[wp:qvs][]], [[wp:ves][]]

The difference $`\dim(V)-\dim(V/N)`$ is called the _relative dimension_
of the _quotient map_ $`V\to{V/N}`$ (?).  It is the dual notion to
_codimension_ (see [2.7][r2.7]). [[wp:rdi][]], [[wp:qum][]]

### 2.11 Vector sum, direct sum decomposition, standard basis

XXX: cleanup

| for linear subspaces $`S,T`$ of $`V`$ | |
|:--|:--|
| the _vector sum_ $`S+T`$ | $`=\{s+t\in{V}|s\in{S},t\in{T}\}`$ (so, the set of all pairwise sum vectors) [[pm:css][]] |
| the _intersection_ $`S\cap{T}`$ | $`=\{u\in{V}|u\in{S},u\in{T}\}`$ (so, the intersection of sets) [[pm:css][]] |
| | |
| $`S,T`$ form a _direct sum decomposition_ of $`V`$ | if $`V=S+T`$ and $`S\cap{}T={0}`$ [[pm:css][]] |
| $`\qquad`$ written: $`\;V=S\oplus{}T`$ | so, if every $`v\in{}V`$ can be expressed as a unique, pairwise sum of vectors of $`S,T`$ |
| | also called: $`S,T`$ are _complementary subspaces_ [[pm:css][]] |

XXX: incorporate [[pm:dsv][]], [[pm:dpm][]], [[pm:dsd][]], [[pm:dfv][]]

[pm:dsv]: https://planetmath.org/DirectSum
[pm:dpm]: https://planetmath.org/DirectProductOfModules
[pm:dsd]: https://planetmath.org/TheoremForTheDirectSumOfFiniteDimensionalVectorSpaces

#### Examples: standard basis

XXX: incorporate use of direct sum: [[wp:svb][]], [[wm:svb][]], [[pm:svb][]]

[wm:svb]: https://mathworld.wolfram.com/StandardBasis.html
[pm:svb]: https://planetmath.org/StandardBasis
[wp:svb]: https://en.wikipedia.org/wiki/Standard_basis

#### Properties:

- $`S+T`$ and $`S\cap{T}`$ are vector subspaces of $`V`$. [[pm:lss][]]
- The set of all subspaces of $`V`$ forms a bounded _modular lattice_ where: \
  $`\{0\}`$, the least element, is the identity element of the _vector sum_
  on subspaces, and \
  $`V`$, the greatest element, is the identity element of the _intersection_
  on subspaces.
  [[wp:lss][]], [[wp:mol][]]
- If $`V`$ is finite-dimensional, the subspaces $`S,T`$ are _complementary_ if
  and only if: \
  for every basis $`\{s_1,...,s_m\}`$ of $`S`$ and every basis
  $`\{t_1,...,t_n\}`$ of $`T`$,
  the combined list $`\{s_1,...,s_m,t_1,...,t_n\}`$ is a basis of $`V`$.
  [[pm:css][]]

### 2.12 Normed space, unit vectors

XXX: cleanup, move & link to: abstract spaces

These concepts require a _normed vector space_ ...
For a real or complex vector space $`V`$ equipped with norm ...

| | |
|:--|:--|
| a _unit vector_ | a vector of _norm_ (or length) $`||v||=1`$, where $`||.||`$ is a _norm_ [[wm:unv][]], [[pm:unv][]], [[wp:unv][]], [[em:unv][]] |
| | also called: a _direction vector_ or _normalized vector_ [[wm:unv][]],, [[wm:nov][]], [[wp:unv][]] |
| | for a nonzero vector $`v`$, the unit vector having the same direction is defined by $`\hat{v}=\frac{v}{||v||}`$ |
| | also called: to _normalize_ a vector $`v`$ |

XXX: incorporate [[em:nvs][]], [[nl:nvs][]], [[pm:nvs][]], [[wm:nvs][]],
[[wp:nvs][]], [[wp:vno][]]

XXX: incorporate [[wp:unv][]], [[wm:unv][]], [[em:unv][]], [[pm:unv][]],
[[wm:nov][]], [[em:nov][]]

[em:nvs]: https://encyclopediaofmath.org/wiki/Normed_space
[nl:nvs]: https://ncatlab.org/nlab/show/norm
[pm:nvs]: https://planetmath.org/NormedVectorSpace
[wm:nvs]: https://mathworld.wolfram.com/NormedSpace.html
[wp:nvs]: https://en.wikipedia.org/wiki/Normed_vector_space
[wp:vno]: https://en.wikipedia.org/wiki/Norm_(mathematics)

[wp:unv]: https://en.wikipedia.org/wiki/Unit_vector
[wm:unv]: https://mathworld.wolfram.com/UnitVector.html
[em:unv]: https://encyclopediaofmath.org/wiki/Unit_vector
[pm:unv]: https://planetmath.org/UnitVector
[wm:nov]: https://mathworld.wolfram.com/NormalizedVector.html
[em:nov]: https://encyclopediaofmath.org/wiki/Normalized_system

### 2.13 Inner product space, orthogonal/orthonormal vectors

XXX: cleanup, move & link to: abstract spaces

These concepts require a _inner product space_ ...
For a real or complex vector space $`V`$ equipped an inner product ...

XXX: incorporate [[em:ips][]], [[em:vip][]], [[wm:vdp][]], [[nl:ips][]],
[[pm:ips][]], [[pm:vdp][]], [[pm:vip][]], [[wm:ips][]], [[wp:ips][]],
[[wp:vdp][]]

XXX: incorporate [[em:ogb][]], [[em:ogv][]], [[wm:ogb][]], [[wm:ogv][]],
[[nl:ogb][]], [[nl:ogl][]], [[pm:ogl][]], [[pm:ogv][]], [[wp:ogb][]],
[[wp:ogl][]]

XXX: incorporate [[em:onv][]], [[pm:onb][]], [[pm:onv][]], [[wm:onb][]],
[[wm:onv][]], [[wp:onb][]], [[wp:onv][]]

XXX: incorporate propositions: [[pm:on1][], [[pm:on2][], [[pm:on3][]

#### Examples:

XXX: incorporate orthogonal/orthonormal basis [[wm:veb][]]

[em:ips]: https://encyclopediaofmath.org/wiki/Pre-Hilbert_space
[em:vip]: https://encyclopediaofmath.org/wiki/Inner_product
[wm:vdp]: https://mathworld.wolfram.com/DotProduct.html
[nl:ips]: https://ncatlab.org/nlab/show/inner+product+space
[pm:ips]: https://planetmath.org/InnerProductSpace
[pm:vdp]: https://planetmath.org/dotproduct
[pm:vip]: https://planetmath.org/InnerProduct
[wm:ips]: https://mathworld.wolfram.com/InnerProductSpace.html
[wp:ips]: https://en.wikipedia.org/wiki/Inner_product_space
[wp:vdp]: https://en.wikipedia.org/wiki/Dot_product

[em:ogb]: https://encyclopediaofmath.org/wiki/Orthogonal_basis
[em:ogv]: https://encyclopediaofmath.org/wiki/Orthogonal_system
[wm:ogb]: https://mathworld.wolfram.com/OrthogonalBasis.html
[wm:ogv]: https://mathworld.wolfram.com/OrthogonalVectors.html
[nl:ogb]: https://ncatlab.org/nlab/show/orthogonal+basis
[nl:ogl]: https://ncatlab.org/nlab/show/orthogonality
[pm:ogl]: https://planetmath.org/orthogonal
[pm:ogv]: https://planetmath.org/OrthogonalVectors
[wp:ogb]: https://en.wikipedia.org/wiki/Orthogonal_basis
[wp:ogl]: https://en.wikipedia.org/wiki/Orthogonality_(mathematics)

[em:onv]: https://encyclopediaofmath.org/wiki/Orthonormal_system
[pm:onb]: https://planetmath.org/OrthonormalBasis
[pm:onv]: https://planetmath.org/OrthonormalSet
[wm:onb]: https://mathworld.wolfram.com/OrthonormalBasis.html
[wm:onv]: https://mathworld.wolfram.com/OrthonormalSet.html
[wp:onb]: https://en.wikipedia.org/wiki/Orthonormal_basis
[wp:onv]: https://en.wikipedia.org/wiki/Orthonormality

[pm:on1]: https://planetmath.org/EveryOrthonormalSetIsLinearlyIndependent
[pm:on2]: https://planetmath.org/AllOrthonormalBasesHaveTheSameCardinality
[pm:on3]: https://planetmath.org/EveryHilbertSpaceHasAnOrthonormalBasis

## 3 Linear map

Linear maps are functions between two vector spaces over a given field that
preserve vector addition and scalar multiplication. [[wp:ves][]]  If bases are
chosen for the vector spaces, a linear transformation can be given by a
matrix. [[wm:clr][]]

### 3.1 Linear map/transformation/operator, vector space homomorphism

The relation of two vector spaces can be expressed by _linear map,_ also
called a _linear mapping, linear transformation, linear operator,_ or _vector
space homomorphism._

Alas, there's an inconsistent use of terminology.  Sometimes the term...
- _linear function_ is used to mean a linear map, while having a different
  meaning in analysis [[wp:lit][]], [[wp:lnf][]], [[wm:lnf][]], [[em:lnf][]];
- _linear operator_ is used to mean a linear map $`V\to{V}`$ ("a linear
  operator on $`V`$") [[wp:lit][]], [[wp:opr][]];
- _linear operator_ is used to mean a linear map $`V\to{W}`$ between vector
  spaces (over the same field) [[em:lio][]];
- _linear transformation_ is used to mean a linear map $`V\to{V}`$
  [[em:lit][]];
- _real operator_ or _complex operator_ generally mean a linear operator on
  $`\mathbb{R}^n`$ or $`\mathbb{C}^n`$, respectively [[wp:opr][]];
- _operator_ is used to mean any (structure-preserving) mapping between spaces
  (not just vector spaces) [[wp:opr][]], [[em:opr][]];
- _operator_ is used when the domain (and codomain) are _function spaces,_
  that is, an operator maps functions [[wp:opr][]], [[wm:opr][]],
  [[wp:fus][]], [[wm:fus][]], [[pm:fus][]].

For vector spaces $`V`$ and $`W`$ over the same field $`F`$, a function
$`f\colon{}V\to{W}`$ is a _linear map_ if for any two vectors
$`\mathbf{u},\mathbf{v}\in{V}`$ and any scalar $`c\in{F}`$ vector addition and
scalar multiplication are preserved: \
$`f(\mathbf{u}+\mathbf{v})=f(\mathbf{u})+f(\mathbf{v})\;`$ and
$`\;f(c\mathbf{u})=cf(\mathbf{u})`$ [[wp:lit][]]

By the associativity of addition, a linear map preserves linear combinations.
For any vectors $`\mathbf{u}_1,...,\mathbf{u}_n\in{V}`$ and scalars
$`c_1,...,c_n\in{F}`$, $`f`$ satisfies: \
$`f(c_1\mathbf{u}_1+\cdots+c_n\mathbf{u}_n)=c_{1}f(\mathbf{u}_1)+\cdots+c_{n}f(\mathbf{u}_n)`$
[[wp:lit][]], [[wp:opr][]], [[em:lio][]]

A linear map $`V\to{W}`$ always maps the origin of $`V`$ to the origin of
$`W`$.  Moreover, it maps linear subspaces in $`V`$ onto linear subspaces in
$`W`$ (possibly of a lower dimension).  Therefore, a linear transformation
always maps lines to lines (or to zero). [[wp:lit][]], [[wm:lit][]],
[[em:lit][]]

The set $`\mathcal{L}(V,W)`$ of linear maps $`V\to{W}`$ forms a vector
space over $`F`$ sometimes denoted $`\operatorname{Hom}(V,W)`$.
Addition and scalar multiplication are defined pointwise: [[wp:lit][]],
[[em:lit][]] \
- $`(f_1+f_2)\colon\mathbf{x}\mapsto{f_1(\mathbf{x})+f_2(\mathbf{x})}`$ is
  linear for any linear $`f_1,f_2\colon{}V\to{W}`$.
- $`(\alpha{f})\colon\mathbf{x}\mapsto{\alpha(f(\mathbf{x}))}`$ is linear for
  any linear $`f\colon{}V\to{W}`$ and $`\alpha\in{F}`$.

If a linear map $`f`$ \
\- is _bijective_ it is called a _linear isomorphism;_ \
\- maps $`V\to{V}`$ it is called a _linear endomorphism;_ \
\- is _injective_ (left-cancellable, left-invertible) it is a
   _monomorphism;_ \
\- is _surjective_ (right-cancellable, right-invertible) it is an
   _epimorphism._ [[wp:lit][]]

XXX: incorpoprate: [[nl:lit][]], [[pm:lit][]], [[pm:adf][]], [[pm:anl][]],
[[pm:lbi][]], [[pm:lis][]], [[pm:lin][]],

#### Examples:

[[wp:lit][]]
- The map $`x\mapsto{cx}`$ is linear for $`x,c\in\mathbb{R}`$ (graph is
  a line through the origin).
- The map $`x\mapsto{x+1}`$ is not linear (but is an affine
  transformation).
- The map $`x\mapsto{x}^2`$ is not linear.
- Differentiation defines a linear map from the space of all differentiable
  functions to the space of all functions.
- A _definite integral_ over some interval $`I`$ is a linear map from the
  space of all real-valued integrable functions on $`I`$ to $`\mathbb{R}`$.
- An _indefinite integral_ (or _antiderivative_) defines a linear map from the
  space of all real-valued integrable functions on $`\mathbb{R}`$ to the
  _quotient space_ (see [2.10][r2.10]) of all real-valued, differentiable
  functions on $`\mathbb{R}`$ _mod_ the linear space of constant functions.
- The _expectation value_ of a random variable is linear, given
  $`E[X+Y]=E[X]+E[Y]`$ and $`E[aX]=aE[X]`$, but the variance of a random
  variable is not linear.

#### Properties:

[[pm:lss][]]
Suppose $`S`$ and $`T`$ are vector spaces, and suppose $`L`$ is a linear mapping
$`L:S\to{}T`$. Then ImL is a vector subspace of $`T`$, and KerL is a vector subspace of $`S`$.

[nl:lit]: https://ncatlab.org/nlab/show/linear+map
[pm:lit]: https://planetmath.org/LinearTransformation
[pm:adf]: https://planetmath.org/AdditiveFunction
[pm:anl]: https://planetmath.org/ThereExistAdditiveFunctionsWhichAreNotLinear
[pm:lbi]: https://planetmath.org/LinearIsomorphism
[pm:lis]: https://planetmath.org/SomeFactsAboutInjectiveAndSurjectiveLinearMaps
[pm:lin]: https://planetmath.org/InvertibleLinearTransformation

### 3.2 Linear maps and their matrix representations

For finite-dimensional vector spaces, there is a one-to-one correspondence
between _linear maps_ and _matrices._  [[wp:lit][]], [[wp:mat][]],
[[wm:mat][]], [[em:mat][]], [[pm:mat][]]

If $`A`$ is a real $`m\times{n}`$ matrix, then
$`\mathbf{x}\mapsto{A\mathbf{x}}`$ describes a linear map
$`\mathbb{R}^n\to\mathbb{R}^m`$ by sending a column vector
$`\mathbf{x}\in\mathbb{R}^n`$ to the column vector
$`A\mathbf{x}\in\mathbb{R}^m`$. [[wp:ves][]], [[wp:lit][]], [[wp:mat][]]

Conversely, each linear transformation $`f\colon\mathbb{R}^n\to\mathbb{R}^m`$
defines a unique $`m`$-by-$`n`$ matrix $`A`$: the $`(i,j)`$-entry of $`A`$
is the $`i`$th coordinate of $`f(e_j)`$, where
$`e_j=(0,...,0,1,0,...,0)`$ is the unit vector with $`1`$ in the $`j`$th
position and $`0`$ elsewhere.  The matrix $`A`$ is said to represent the
linear map $`f`$, and $`A`$ is called the _transformation matrix_ of
$`f`$. [[wp:mat][]]

This shows that a linear map $`f`$ is completely determined by what it does to
a basis.  For a given basis $`(e_1,...,e_n)`$, every vector $`v\in{}V`$ is
a linear combination $`v=a_1{}e_1+\cdots+a_n{}e_n`$, hence \
$`f(v)=f(a_1{}e_1+\cdots+a_n{}e_n)=a_1{}f(e_1)+\cdots+a_n{}f(e_n)`$
by linearity of $`f`$.
[[Jeremy Kun](https://www.jeremykun.com/2011/06/19/linear-algebra-a-primer)]

For the finite-dimensional case with chosen bases: [[wp:lit][]] \
\- the composition of linear maps corresponds to the matrix multiplication, \
\- the addition of linear maps corresponds to the matrix addition, and \
\- the multiplication of linear maps with scalars corresponds to the
   multiplication of matrices with scalars.

Some _non-_linear maps on $`\mathbb{R}^n`$ can be represented as linear maps
on $`\mathbb{R}^{n+1}`$: These include both affine transformations (such as
translation) and projective transformations.  For this reason, $`4×4`$
transformation matrices are widely used in 3D computer graphics. [[wp:trm][]]

XXX: incorporate [[pm:mrl][]]

#### Examples: change of basis

XXX: incorporate [[wm:cob][]], [[pm:cob][]], [[wp:cob][]]

[pm:mrl]: https://planetmath.org/MatrixRepresentationOfALinearTransformation
[wm:cob]: https://mathworld.wolfram.com/ChangeofBasis.html
[pm:cob]: https://planetmath.org/ChangeOfBasis
[wp:cob]: https://en.wikipedia.org/wiki/Change_of_basis

### 3.3 Kernel, image, rank, rank–nullity theorem

XXX: incorporate [[pm:rlt][]], [[pm:nul][]], [[pm:ilt][]], [[pm:klt][]]

XXX: incorporate [[pm:dfv][]]:
Rank-nullity theorem + [proof of rank-nullity theorem](https://planetmath.org/ProofOfRanknullityTheorem)

XXX: cleanup [[pm:dit][]]
This application of the dimension theorem is sometimes itself called the
dimension theorem. Let $`T:U\to{V}`$ be a linear transformation. Then
$`\qquad\dim(\range(T))+\dim(\ker(T))=\dim(U)`$, \
that is, the dimension of U is equal to the dimension of the transformation's
range plus the dimension of the kernel. See rank–nullity theorem

[pm:rlt]: https://planetmath.org/RankOfALinearMapping
[pm:nul]: https://planetmath.org/Nullity
[pm:ilt]: https://planetmath.org/ImageOfALinearTransformation
[pm:klt]: https://planetmath.org/KernelOfALinearMapping

### 3.4 Vector space endomorphism, eigenvector, eigenvalue

Linear maps $`f\colon{}V\to{V}`$ (_vector space endomorphisms_) allow to compare
vectors $`\mathbf{v}`$ with their image under $`f`$, $`f(\mathbf{v})`$.  Any
nonzero vector $`\mathbf{v}`$ satisfying $`λ\mathbf{v}=f(\mathbf{v})`$, where
$`\lambda`$ is a scalar, is called an _eigenvector_ (or _characteristic
vector_) of $`f`$ with _eigenvalue_ (or _characteristic value_) $`\lambda`$.
[[wp:ves][]]

An eigenvector is one of a special set of vectors associated with a linear
system of equations.  An eigenvalue is one of a set of special scalars
associated with a linear system of equations that describes that system's
fundamental modes. [[wm:clr][]]

Formally, given an $`n\times{n}`$ matrix $`A`$ and a nonzero vector
$`\mathbf{v}`$ of length $`n`$, $`\mathbf{v}`$ is an eigenvector of $`A`$ and
$`\lambda`$ the corresponding eigenvalue, if: \
$`A\mathbf{v}=\lambda\mathbf{v}`$ [[wp:eiv][]]

In functional analysis: $`v`$ is an eigenvector and $`\lambda`$ the
corresponding eigenvalue of an _operator_ $`A`$ acting on a vector space
$`V`$. [[em:eiv][]], [[em:opr][]], [[wp:opr][]], [[wm:opr][]]

Geometrically, a linear transformation rotates, stretches, or shears the
vectors upon which it acts. Its eigenvectors are those vectors that are only
stretched, with neither rotation nor shear. The corresponding eigenvalue is
the factor by which an eigenvector is stretched or squished. If the eigenvalue
is negative, the eigenvector's direction is reversed. [[wp:eiv][]]

XXX: incorporate [[pm:eis][]], [[pm:eis1][]], [[pm:eiv][]], [[pm:eiv1][]],
[[pm:eiv2][]], [[pm:eiv3][]], [[pm:eiv4][]], [[pm:eiv5][]], [[pm:eiv6][]

[pm:eis]: https://planetmath.org/Eigenspace
[pm:eis1]: https://planetmath.org/GeneralizedEigenspace
[pm:eiv]: https://planetmath.org/Eigenvector
[pm:eiv1]: https://planetmath.org/Eigenvalue
[pm:eiv2]: https://planetmath.org/Eigenvalue1
[pm:eiv3]: https://planetmath.org/EigenvalueProblem
[pm:eiv4]: https://planetmath.org/FindingEigenvalues
[pm:eiv5]: https://planetmath.org/InvarianceOfEigenvalues
[pm:eiv6]: https://planetmath.org/MultiplicityOfEigenvalue

### 3.5 Linear form, dual vector space

XXX: incorporate [[pm:dfv][]]:
Space of linear mappings,
Dual space,
Space of functions into a vector space
$`\operatorname{Hom}(V,F)`$

A _linear form, linear functional, one-form,_ or _covector_ on a vector space
$`V`$ over a field $`F`$ is a (linear) map $`f\colon{}V\to{F}`$ such that \
$`f(\mathbf{u}+\mathbf{v})=f(\mathbf{u})+f(\mathbf{v})\;`$ and
$`\;f(c\mathbf{u})=cf(\mathbf{u})`$ \
for all $`\mathbf{u},\mathbf{v}\in{V}`$ and $`c\in{F}`$.
[[pm:cov][]], [[em:lif][]], [[wp:lif][]], [[wp:lit][]]

A linear form meets the definition of a _linear map_ with $`F`$ viewed as a
one-dimensional vector space.  [[wp:lif][]], [[wp:lit][]]

Examples of linear forms:
- indexing the second element of $`[x,y,z]`$:
  $`[0,1,0]\cdot[x,y,z]=y`$ [[wp:lif][]].
- the mean value of an $`n`$-vector:
  $`\operatorname{mean}(v)=[1/n,...,1/n]\cdot{v}`$ [[wp:lif][]].
- definite integration:
  $`f\mapsto\int_{a}^{b}f(x)\,dx\;\;`$
  for $`f\in{C}[a,b]`$, the vector space of continuous functions on interval
  $`[a,b]`$ [[wp:lif][]]

As linear maps, the set of all linear functionals $`V\to{F}`$ itself forms a
vector space over $`F`$, denoted $`\operatorname{Hom}(V,F)`$ or, when $`F`$ is
understood, $`V^*`$, $`V'`$, $`V^\#`$ or $`V^\vee`$.  (Addition and scalar
multiplication are defined pointwise, see [[3.1][r3.1]].) [[em:lif][]],
[[wp:lif][]]

The vector space $`V^*`$ over $`F`$ is called the _dual space_ to $`V`$ or _linear
dual_ of $`V`$, or the _algebraic dual space_ of $`V`$ (to avoid ambiguity
with the _continuous dual space_ of a _topological vector space._ [[wp:dus][]]
[[pm:cov][]]

$`V^∗`$ has the same dimension as $`V`$. [[wm:dus][]]

$`V`$ is isomorphic to $`V^∗`$ if and only if $`V`$ is finite-dimensional.
[[em:lif][]]  Also see
[Jeremy Kun](https://www.jeremykun.com/2016/03/28/tensorphobia-outer-product)

But there is no natural isomorphism between these two spaces.  There is a
natural homomorphism $`\Psi`$ from $`V`$ into the _double dual_ $`V^{**}`$.
[[wp:lif][]]

Infinite-dimensional _Hilbert spaces_ are not isomorphic to their _continuous
double duals._ [[wp:lif][]]

When vectors are represented by column vectors (as is common when a basis is
fixed), then linear functionals are represented as row vectors, and their
values on specific vectors are given by matrix products (with the row vector
on the left). [[wp:lif][]]

XXX: incorporate [[pm:fus][]], [[pm:cov][]], [[wp:for][]], [[nl:dus][]],
[[pm:fnl][]], [[pm:lfl][]], [[pm:dus][]],

[pm:cov]: https://planetmath.org/Covector
[wp:for]: https://en.wikipedia.org/wiki/Form_(mathematics)
[nl:dus]: https://ncatlab.org/nlab/show/dual+vector+space
[pm:fnl]: https://planetmath.org/Functional
[pm:lfl]: https://planetmath.org/LinearFunctional
[pm:dus]: https://planetmath.org/DualSpace

### 3.6 Bilinear/multilinear map/form

A _bilinear map_ is a function on vector spaces $`V\times{W}\to{X}`$
that is linear in each argument separately: [[wp:blm][]], [[wm:blm][]],
[[em:blm][]] \
$`B_w:v\mapsto{B(v,w)}`$ is linear for all $`w\in{W}`$ \
$`B_v:w\mapsto{B(v,w)}`$ is linear for all $`v\in{V}`$

Such a map $`B`$ satisfies the following properties for all $`\lambda\in{F}`$,
$`v,v_1,v_2\in{V}`$, $`w,w_1,w_2\in{W}`$: [[wp:blm][]], [[wm:blm][]] \
$`\lambda{B}(v,w)=B(\lambda{v},w)=B(v,\lambda{w})`$ \
$`B(v_1+v_2,w)=B(v_1,w)+B(v_2,w)\;`$ \
$`B(v,w_1+w_2)=B(v,w_1)+B(v,w_2)\;`$

Examples of bilinear maps:
- matrix multiplication $`M(m,n)\times{M}(n,p)\to{M}(m,p)`$ [[wp:blm][]]
- the _cross product_ $`\mathbb{R}^3\times\mathbb{R}^3\to\mathbb{R}^3`$
  [[wp:blm][]], [[wp:crp][]]

A _bilinear form_  on a vector space $`V`$ is a bilinear map
$`V\times{V}\to{F}`$, which is linear in each argument separately.
[[wp:blf][]], [[wm:blf][]], [[em:blf][]]

Examples of bilinear forms:
- on $`\mathbb{R}^2`$:
  $`(x_1,x_2),(y_1,y_2)\mapsto{x_1y_2+x_2y_1}`$ [[wm:blf][]]
- on $`\mathbb{R}^n`$: the _dot product_ (or _inner product_)
  $`(a_1,...,a_n)\cdot(b_1,...,b_n)=a_{1}b_{1}+\cdots+a_{n}b_{n}`$
  [[em:blf][]]
- on $`\mathbb{C}^n`$: the _Hermetian inner product:_
  $`h(z,w)=\sum{z_i}\overline{w_i}`$
  [[wm:hip][]]

A _multilinear map, $`k`$-linear mapping,_ or _multilinear operator_ is a
function of several variables that is linear separately in each variable: \
$`f\colon{}V_1\times\cdots\times{}V_n\to{}W`$,
where $`V_1,...,V_n`$ and $`W`$ are vector spaces (or modules over a
commutative ring), with the following property: \
for each $`i`$, if all of the variables but $`v_i`$ are held constant, then
$`f(v_1,...,v_i,...,v_n)`$ is a linear function of $`v_i`$.
[[wp:mlm][]], [[em:mlm][]]

Multilinear maps $`f\colon{}V_1\times\cdots\times{}V_n\to{}W`$ and
maps $`F\colon{}V_1\otimes\cdots\otimes{}V_n\to{}W`$, where
$`V_1\otimes\cdots\otimes{}V_n`$ denotes the _tensor product,_ \
are in a one-to-one correspondence given by the formula
$`F(v_1\otimes\cdots\otimes{}v_n)=f(v_1,...,v_n)`$.
[[wp:mlm][]], [[wp:tep][]]

A _multilinear form_ or _$`k`$-linear form_ on a vector space $`V`$ over a
field $`F`$ (or modules over a commutative ring) is a map: \
$`f\colon{}V^k\to{}F`$ that is linear separately in each of its $`k`$
arguments. [[wp:mlf][]], [[wm:mlf][]], [[em:mlf][]]

Example: The _determinant_ of a matrix is an _alternating multilinear form_ of
the columns (or rows) of a square matrix. [[wp:mlm][]], [[wp:det][]]

XXX: incorporate [[pm:blm][]], [[pm:blf][]], [[pm:mlm][]], [[pm:det][]],
[[pm:mrb][]]

[pm:blm]: https://planetmath.org/BilinearMap
[pm:blf]: https://planetmath.org/BilinearForm
[pm:mlm]: https://planetmath.org/Multilinear
[pm:det]: https://planetmath.org/DeterminantAsAMultilinearMapping
[pm:mrb]: https://planetmath.org/MatrixRepresentationOfABilinearForm

## German resources

| |
|:--|
| video courses (excerpts) by [Edmund Weitz @WeitzHAWHamburg](https://www.youtube.com/@WeitzHAWHamburg): |
| [Vektorräume](https://www.youtube.com/watch?v=DhW-Qef7mhQ&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=67), [Linearkombinationen, lineare Unabhängigkeit, Basis, Dimension](https://www.youtube.com/watch?v=UfaKHR0vcxo&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=68), [Lineare Abbildungen und die Kern-Bild-Formel](https://www.youtube.com/watch?v=blroCEbv1X8&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=69), [Matrizen](https://www.youtube.com/watch?v=c4t9X1FOUGE&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=70), [Geometrische Interpretation linearer Abbildungen ](https://www.youtube.com/watch?v=EQ5Xct2YyLk&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=71), [Homogene Koordinaten](https://www.youtube.com/watch?v=dCQzgvwxpvs&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=72), [Skalarprodukt](https://www.youtube.com/watch?v=xJ0BpXkJIGg&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=73), [Orthogonale Abbildungen, Isometrien](https://www.youtube.com/watch?v=LpQZy84OrHk&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=74), [Hessesche Normalenform, Hyperebenen, Orthonormalbasen, Vektorprodukt](https://www.youtube.com/watch?v=iKJZ4E40j7c&list=PLb0zKSynM2PCLnXVwfjVcefXa6iZA-an4&index=75) |
| |
| video courses by "Leo" [@Algorithmen & Beweise](https://www.youtube.com/@algorithmenbeweise4071): |
| [Vektorräume](https://www.youtube.com/playlist?list=PLHi0WgifODX31yrT61carp6WdoGbKeXz_), [Vektorraum-Homomorphismen](https://www.youtube.com/playlist?list=PLHi0WgifODX0P1OJmcHkKLDL10Lu8ZAPo), [Determinante](https://www.youtube.com/playlist?list=PLHi0WgifODX3vWmcUYZHbuHgGYB_AQe2a) |
| |
| video courses with excercises, proofs by [Julian Großmann @brightsideofmaths](https://www.youtube.com/@brightsideofmaths): |
| [Lineare Algebra 1](https://www.youtube.com/playlist?list=PLBh2i93oe2qvrzR114cOR9i-aJb6_GVuw), [Lineare Algebra 2](https://www.youtube.com/playlist?list=PLBh2i93oe2qsrbh8B26GIlQK_TSgxVFeq) |
| |
| video courses with excercises, proofs by [Cengiz Aydi @PianistMathsCA](https://www.youtube.com/@PianistMathsCA): |
| [Vektorraum Eigenschaften, Axiome](https://www.youtube.com/playlist?list=PLDI03FHYJ05PGuTadQwrRBPW1IewsX7Si), [Untervektorraum](https://www.youtube.com/playlist?list=PLDI03FHYJ05N15dav8N0JnrGo-ef6O-nU), [Lineare Unabhängigkeit](https://www.youtube.com/playlist?list=PLDI03FHYJ05OWwYsPdM2nQNpnV9S29pg6), [Lineare Abbildung, Kern](https://www.youtube.com/playlist?list=PLDI03FHYJ05NjVDIckvdwdthvC4PbrUcI), [Darstellungsmatrix](https://www.youtube.com/playlist?list=PLDI03FHYJ05MR8JuFSt888o2cyYoeSIih), [Matrix, invertierbar](https://www.youtube.com/playlist?list=PLDI03FHYJ05Mvy-eiUfPJ81FSDx2o171Z), [Matrix, Rang, Dimension, Bild](https://www.youtube.com/playlist?list=PLDI03FHYJ05NsUOePzhkgjTG02iNMV0Ye), [Matrix, symmetrisch, schiefsymmetrisch](https://www.youtube.com/playlist?list=PLDI03FHYJ05M3PxsTcerz-TMY7C_cgB8p), [Matrix, diagonalisierbar, Minimalpolynom, Basiswechselmatrix, Übergangsmatrix](https://www.youtube.com/playlist?list=PLDI03FHYJ05M809_IF1R8tYuPwPqqv1ZU), [Matrix, orthogonal, Determinante](https://www.youtube.com/playlist?list=PLDI03FHYJ05NdhC12aGAwhE8IV1kMPxDi), [Matrix mit Parameter](https://www.youtube.com/playlist?list=PLDI03FHYJ05OYkrFOR66MXOcXi_l59SZi) |
| |
| video courses by [Math Intuition](https://www.youtube.com/@mathintuition): |
| [Lineare Algebra 1](https://www.youtube.com/playlist?list=PLOWqYl7tM7vkBxlEPUaqQIdtJL_JpoP-u), [Lineare Algebra 2](https://www.youtube.com/playlist?list=PLOWqYl7tM7vlfkyyPH2s-c87_hE3eaWVg), [Von Matrix bis Basiswechsel](https://www.youtube.com/playlist?list=PLOWqYl7tM7vkvX7qUAaguAfNXp7DdjNkR) |
| |
| video courses by [Daniel Jung](https://www.youtube.com/@MathebyDanielJung): |
| [Vektorraum, Untervektorraum, Basis, Dimension](https://www.youtube.com/playlist?list=PLLTAHuUj-zHg5aCwkY8rEdui-bhZnJloM), [Punkte im Raum, Vektoren, Längen, Skalarprodukt, Winkel, Analytische Geometrie](https://www.youtube.com/playlist?list=PLLTAHuUj-zHg9mkP7aU3BQyhrxP7Ji74v), [Matrizen](https://www.youtube.com/playlist?list=PLA45E89808E3DB64D), [Rechnen mit Matrizen, Matrixalgebra](https://www.youtube.com/playlist?list=PLLTAHuUj-zHg_4njOaG9hxAzbRiBFg9Z2), [Abbildungen, linear, affin, Eigenwerte, Eigenvektoren](https://www.youtube.com/playlist?list=PLLTAHuUj-zHgMbsnuYFI4faJVawyKOM97), [Diagonalmatrix, Jordanform, Hülle, Spann, Eigenraum](https://www.youtube.com/playlist?list=PLLTAHuUj-zHhmXW_vXrZ3GlNw7CF8YFZC), [Determinanten](https://www.youtube.com/playlist?list=PLLTAHuUj-zHhQ9B-kEyUp2-ySYSzufo9W), [Gleichungssysteme lösen, Gaußalgorithmus, Ränge, Determinanten](https://www.youtube.com/playlist?list=PLLTAHuUj-zHiew-S1iYQ0zPFjOzugMB_D) |

[r0]: Refcard_Abstract_Spaces_in_Functional_Analysis.md
[r1]: Refcard_Point_Set_Topology_Basics.md

[em:alf]: https://encyclopediaofmath.org/wiki/Algebra
[em:bas]: https://encyclopediaofmath.org/wiki/Basis
[em:blf]: https://encyclopediaofmath.org/wiki/Bilinear_form
[em:blm]: https://encyclopediaofmath.org/wiki/Bilinear_mapping
[em:cac]: https://encyclopediaofmath.org/wiki/Cartesian_coordinates
[em:cod]: https://encyclopediaofmath.org/wiki/Codimension
[em:cos]: https://encyclopediaofmath.org/wiki/Coordinates
[em:cvh]: https://encyclopediaofmath.org/wiki/Convex_hull
[em:cvx]: https://encyclopediaofmath.org/wiki/Convex_set
[em:cyc]: https://encyclopediaofmath.org/wiki/Cylinder_coordinates
[em:dim]: https://encyclopediaofmath.org/wiki/Dimension
[em:dir]: https://encyclopediaofmath.org/wiki/Division_ring
[em:eiv]: https://encyclopediaofmath.org/wiki/Eigen_vector
[em:eus]: https://encyclopediaofmath.org/wiki/Euclidean_space
[em:exf]: https://encyclopediaofmath.org/wiki/Extension_of_a_field
[em:fld]: https://encyclopediaofmath.org/wiki/Field
[em:hyp]: https://encyclopediaofmath.org/wiki/Hyperplane
[em:lia]: https://encyclopediaofmath.org/wiki/Linear_algebra
[em:lif]: https://encyclopediaofmath.org/wiki/Linear_functional
[em:lih]: https://encyclopediaofmath.org/wiki/Linear_hull
[em:lii]: https://encyclopediaofmath.org/wiki/Linear_independence
[em:lio]: https://encyclopediaofmath.org/wiki/Linear_operator
[em:lit]: https://encyclopediaofmath.org/wiki/Linear_transformation
[em:lnf]: https://encyclopediaofmath.org/wiki/Linear_function
[em:lss]: https://encyclopediaofmath.org/wiki/Linear_subspace
[em:mat]: https://encyclopediaofmath.org/wiki/Matrix
[em:mla]: https://encyclopediaofmath.org/wiki/Multilinear_algebra
[em:mlf]: https://encyclopediaofmath.org/wiki/Multilinear_form
[em:mlm]: https://encyclopediaofmath.org/wiki/Multilinear_mapping
[em:mod]: https://encyclopediaofmath.org/wiki/Module
[em:opr]: https://encyclopediaofmath.org/wiki/Operator
[em:orf]: https://encyclopediaofmath.org/wiki/Ordered_field
[em:poc]: https://encyclopediaofmath.org/wiki/Polar_coordinates
[em:por]: https://encyclopediaofmath.org/wiki/Ring_of_polynomials
[em:rng]: https://encyclopediaofmath.org/wiki/Ring
[em:sca]: https://encyclopediaofmath.org/wiki/Scalar
[em:spc]: https://encyclopediaofmath.org/wiki/Spherical_coordinates
[em:tob]: https://encyclopediaofmath.org/wiki/Base
[em:tup]: https://encyclopediaofmath.org/wiki/Tuple
[em:vec]: https://encyclopediaofmath.org/wiki/Vector
[em:ves]: https://encyclopediaofmath.org/wiki/Vector_space

[nl:afs]: https://ncatlab.org/nlab/show/affine+space
[nl:bas]: https://ncatlab.org/nlab/show/basis
[nl:bat]: https://ncatlab.org/nlab/show/basis+theorem
[nl:cns]: https://ncatlab.org/nlab/show/conical+space
[nl:cos]: https://ncatlab.org/nlab/show/coordinate+system
[nl:cvs]: https://ncatlab.org/nlab/show/convex+space
[nl:dim]: https://ncatlab.org/nlab/show/dimension
[nl:dir]: https://ncatlab.org/nlab/show/skewfield
[nl:fld]: https://ncatlab.org/nlab/show/field
[nl:grf]: https://ncatlab.org/nlab/show/ground+ring
[nl:hia]: https://ncatlab.org/nlab/show/higher+algebra
[nl:his]: https://ncatlab.org/nlab/show/higher+structure
[nl:hla]: https://ncatlab.org/nlab/show/higher+linear+algebra
[nl:kvs]: https://ncatlab.org/nlab/show/Vect
[nl:lia]: https://ncatlab.org/nlab/show/linear+algebra
[nl:lic]: https://ncatlab.org/nlab/show/linear+combination
[nl:lii]: https://ncatlab.org/nlab/show/linearly+independent+subset
[nl:lss]: https://ncatlab.org/nlab/show/linear+subspace
[nl:mla]: https://ncatlab.org/nlab/show/multilinear+algebra
[nl:mod]: https://ncatlab.org/nlab/show/module
[nl:poc]: https://ncatlab.org/nlab/show/polar+coordinates
[nl:rng]: https://ncatlab.org/nlab/show/ring
[nl:sca]: https://ncatlab.org/nlab/show/scalar
[nl:tlb]: https://ncatlab.org/nlab/show/basis+in+functional+analysis
[nl:tob]: https://ncatlab.org/nlab/show/topological+base
[nl:veb]: https://ncatlab.org/nlab/show/basis+of+a+vector+space
[nl:ves]: https://ncatlab.org/nlab/show/vector+space

[pm:afc]: https://planetmath.org/AffineCombination
[pm:alg]: https://planetmath.org/Algorithm
[pm:bat]: https://planetmath.org/EveryVectorSpaceHasABasis
[pm:cac]: https://planetmath.org/CartesianCoordinates
[pm:coo]: https://planetmath.org/CoordinateVector
[pm:cos]: https://planetmath.org/CoordinateSystems
[pm:css]: https://planetmath.org/ComplementarySubspace
[pm:cvc]: https://planetmath.org/ConvexCombination
[pm:cvx]: https://planetmath.org/ConvexSet
[pm:cyc]: https://planetmath.org/CylindricalCoordinates
[pm:dfv]: https://planetmath.org/DimensionFormulaeForVectorSpaces
[pm:dim]: https://planetmath.org/DimensionVectorSpace
[pm:dir]: https://planetmath.org/DivisionRing
[pm:eus]: https://planetmath.org/EuclideanVectorSpace
[pm:fgm]: https://planetmath.org/FinitelyGeneratedModule
[pm:flp]: https://planetmath.org/FinitedimensionalLinearProblem
[pm:fus]: https://planetmath.org/FunctionSpace
[pm:geo]: https://planetmath.org/EuclideanVector
[pm:leq]: https://planetmath.org/LinearEquation
[pm:lia]: https://planetmath.org/LinearAlgebra
[pm:lic]: https://planetmath.org/LinearCombination
[pm:lih]: https://planetmath.org/Span
[pm:lii]: https://planetmath.org/LinearlyIndependent
[pm:liv]: https://planetmath.org/ListVector
[pm:lss]: https://planetmath.org/VectorSubspace
[pm:mat]: https://ncatlab.org/nlab/show/matrix
[pm:mod]: https://planetmath.org/ModulesAreAGeneralizationOfVectorSpaces
[pm:mot]: https://planetmath.org/ModelTheory
[pm:orf]: https://planetmath.org/OrderedRing
[pm:phy]: https://planetmath.org/PhysicalVector
[pm:pli]: https://planetmath.org/PropertiesOfLinearIndependence
[pm:pls]: https://planetmath.org/PropertiesOfSpanningSets
[pm:poc]: https://planetmath.org/PolarCoordinates
[pm:rng]: https://planetmath.org/Ring
[pm:sig]: https://planetmath.org/Signature
[pm:spc]: https://planetmath.org/SphericalCoordinates
[pm:str]: https://planetmath.org/Structure
[pm:tob]: https://planetmath.org/BasisTopology
[pm:veb]: https://planetmath.org/Basis
[pm:vec]: https://planetmath.org/Vector
[pm:ves]: https://planetmath.org/VectorSpace

[wm:afh]: https://mathworld.wolfram.com/AffineHull.html
[wm:alf]: https://mathworld.wolfram.com/Algebra.html
[wm:bav]: https://mathworld.wolfram.com/BasisVector.html
[wm:blf]: https://mathworld.wolfram.com/BilinearForm.html
[wm:blm]: https://mathworld.wolfram.com/BilinearFunction.html
[wm:cac]: https://mathworld.wolfram.com/CartesianCoordinates.html
[wm:clr]: https://mathworld.wolfram.com/classroom/classes/LinearAlgebra.html
[wm:cod]: https://mathworld.wolfram.com/Codimension.html
[wm:cos]: https://mathworld.wolfram.com/Coordinates.html
[wm:cvc]: https://mathworld.wolfram.com/ConvexCombination.html
[wm:cvh]: https://mathworld.wolfram.com/ConvexHull.html
[wm:cvx]: https://mathworld.wolfram.com/ConvexSet.html
[wm:cyc]: https://mathworld.wolfram.com/CylindricalCoordinates.html
[wm:dim]: https://mathworld.wolfram.com/Dimension.html
[wm:dir]: https://mathworld.wolfram.com/DivisionAlgebra.html
[wm:dus]: https://mathworld.wolfram.com/DualVectorSpace.html
[wm:eiv]: https://mathworld.wolfram.com/Eigenvector.html
[wm:eus]: https://mathworld.wolfram.com/EuclideanSpace.html
[wm:exf]: https://mathworld.wolfram.com/ExtensionField.html
[wm:fld]: https://mathworld.wolfram.com/Field.html
[wm:fus]: https://mathworld.wolfram.com/FunctionSpace.html
[wm:hip]: https://mathworld.wolfram.com/HermitianInnerProduct.html
[wm:hyp]: https://mathworld.wolfram.com/Hyperplane.html
[wm:lia]: https://mathworld.wolfram.com/LinearAlgebra.html
[wm:lic]: https://mathworld.wolfram.com/LinearCombination.html
[wm:lih]: https://mathworld.wolfram.com/VectorSpaceSpan.html
[wm:lii]: https://mathworld.wolfram.com/LinearlyIndependent.html
[wm:lit]: https://mathworld.wolfram.com/LinearTransformation.html
[wm:lnf]: https://mathworld.wolfram.com/LinearFunction.html
[wm:mat]: https://mathworld.wolfram.com/Matrix.html
[wm:mlf]: https://mathworld.wolfram.com/MultilinearForm.html
[wm:mod]: https://mathworld.wolfram.com/Module.html
[wm:opr]: https://mathworld.wolfram.com/Operator.html
[wm:poc]: https://mathworld.wolfram.com/PolarCoordinates.html
[wm:por]: https://mathworld.wolfram.com/PolynomialRing.html
[wm:qvs]: https://mathworld.wolfram.com/QuotientVectorSpace.html
[wm:rng]: https://mathworld.wolfram.com/Ring.html
[wm:rnl]: https://mathworld.wolfram.com/RealLine.html
[wm:sca]: https://mathworld.wolfram.com/Scalar.html
[wm:spc]: https://mathworld.wolfram.com/SphericalCoordinates.html
[wm:tup]: https://mathworld.wolfram.com/n-Tuple.html
[wm:veb]: https://mathworld.wolfram.com/VectorBasis.html
[wm:vec]: https://mathworld.wolfram.com/Vector.html
[wm:ves]: https://mathworld.wolfram.com/VectorSpace.html

[wp:afc]: https://en.wikipedia.org/wiki/Affine_combination
[wp:afh]: https://en.wikipedia.org/wiki/Affine_hull
[wp:afs]: https://en.wikipedia.org/wiki/Affine_space
[wp:alf]: https://en.wikipedia.org/wiki/Algebras_over_a_field
[wp:aoc]: https://en.wikipedia.org/wiki/Axiom_of_choice
[wp:blf]: https://en.wikipedia.org/wiki/Bilinear_form
[wp:blm]: https://en.wikipedia.org/wiki/Bilinear_map
[wp:cac]: https://en.wikipedia.org/wiki/Cartesian_coordinate_system
[wp:cnc]: https://en.wikipedia.org/wiki/Conical_combination
[wp:cod]: https://en.wikipedia.org/wiki/Codimension
[wp:coo]: https://en.wikipedia.org/wiki/Coordinate_vector
[wp:cos]: https://en.wikipedia.org/wiki/Coordinate_system
[wp:crp]: https://en.wikipedia.org/wiki/Cross_product
[wp:cvc]: https://en.wikipedia.org/wiki/Convex_combination
[wp:cvh]: https://en.wikipedia.org/wiki/Convex_hull
[wp:cvx]: https://en.wikipedia.org/wiki/Convex_set
[wp:cyc]: https://en.wikipedia.org/wiki/Cylindrical_coordinate_system
[wp:det]: https://en.wikipedia.org/wiki/Determinant
[wp:dim]: https://en.wikipedia.org/wiki/Dimension_(vector_space)
[wp:dir]: https://en.wikipedia.org/wiki/Division_ring
[wp:dit]: https://en.wikipedia.org/wiki/Dimension_theorem_for_vector_spaces
[wp:dus]: https://en.wikipedia.org/wiki/Dual_space
[wp:eiv]: https://en.wikipedia.org/wiki/Eigenvalues_and_eigenvectors
[wp:eqc]: https://en.wikipedia.org/wiki/Equivalence_class
[wp:eus]: https://en.wikipedia.org/wiki/Euclidean_space
[wp:exf]: https://en.wikipedia.org/wiki/Field_extension
[wp:fld]: https://en.wikipedia.org/wiki/Field_(mathematics)
[wp:fus]: https://en.wikipedia.org/wiki/Function_spaces
[wp:geo]: https://en.wikipedia.org/wiki/Geometric_vector
[wp:grf]: https://en.wikipedia.org/wiki/Ground_field
[wp:hyp]: https://en.wikipedia.org/wiki/Hyperplane
[wp:lia]: https://en.wikipedia.org/wiki/Linear_algebra
[wp:lic]: https://en.wikipedia.org/wiki/Linear_combination
[wp:lif]: https://en.wikipedia.org/wiki/Linear_form
[wp:lih]: https://en.wikipedia.org/wiki/Linear_span
[wp:lii]: https://en.wikipedia.org/wiki/Linear_independence
[wp:lit]: https://en.wikipedia.org/wiki/Linear_map
[wp:lnf]: https://en.wikipedia.org/wiki/Linear_function_(calculus)
[wp:lss]: https://en.wikipedia.org/wiki/Linear_subspace
[wp:mat]: https://en.wikipedia.org/wiki/Matrix_(mathematics)
[wp:mla]: https://en.wikipedia.org/wiki/Multilinear_algebra
[wp:mlf]: https://en.wikipedia.org/wiki/Multilinear_form
[wp:mlm]: https://en.wikipedia.org/wiki/Multilinear_map
[wp:mod]: https://en.wikipedia.org/wiki/Module_(mathematics)
[wp:mol]: https://en.wikipedia.org/wiki/Modular_lattice
[wp:mon]: https://en.wikipedia.org/wiki/Monomial
[wp:opr]: https://en.wikipedia.org/wiki/Operator_(mathematics)
[wp:orf]: https://en.wikipedia.org/wiki/Ordered_field
[wp:poc]: https://en.wikipedia.org/wiki/Polar_coordinate_system
[wp:pol]: https://en.wikipedia.org/wiki/Polynomial
[wp:por]: https://en.wikipedia.org/wiki/Polynomial_ring
[wp:qum]: https://en.wikipedia.org/wiki/Quotient_map
[wp:qvs]: https://en.wikipedia.org/wiki/Quotient_space_(linear_algebra)
[wp:rdi]: https://en.wikipedia.org/wiki/Relative_dimension
[wp:rng]: https://en.wikipedia.org/wiki/Ring_(mathematics)
[wp:rnl]: https://en.wikipedia.org/wiki/Number_line
[wp:sca]: https://en.wikipedia.org/wiki/Scalar_(mathematics)
[wp:sel]: https://en.wikipedia.org/wiki/Steinitz_exchange_lemma
[wp:spc]: https://en.wikipedia.org/wiki/Spherical_coordinate_system
[wp:tep]: https://en.wikipedia.org/wiki/Tensor_product
[wp:tob]: https://en.wikipedia.org/wiki/Base_(topology)
[wp:trm]: https://en.wikipedia.org/wiki/Transformation_matrix
[wp:tup]: https://en.wikipedia.org/wiki/Tuple
[wp:veb]: https://en.wikipedia.org/wiki/Basis_(linear_algebra)
[wp:vec]: https://en.wikipedia.org/wiki/Vector_(mathematics_and_physics)
[wp:ves]: https://en.wikipedia.org/wiki/Vector_space

[Up](./)
