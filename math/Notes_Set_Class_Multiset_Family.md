# polyglot/math

XXX cleanup

[em:alt]: https://encyclopediaofmath.org/wiki/Alternative_set_theory
[em:ast]: https://encyclopediaofmath.org/wiki/Axiomatic_set_theory
[em:cls]: https://encyclopediaofmath.org/wiki/Class
[em:ems]: https://encyclopediaofmath.org/wiki/Empty_set
[em:fol]: https://encyclopediaofmath.org/wiki/Predicate_calculus
[em:hhp]: https://encyclopediaofmath.org/wiki/Hilbert_infinite_hotel
[em:mal]: https://encyclopediaofmath.org/wiki/Mathematical_logic
[em:nst]: https://encyclopediaofmath.org/wiki/Set_theory
[em:pws]: https://encyclopediaofmath.org/wiki/Power_set
[em:rup]: https://encyclopediaofmath.org/wiki/Antinomy
[em:set]: https://encyclopediaofmath.org/wiki/Set
[em:trs]: https://encyclopediaofmath.org/wiki/Transitive_set
[em:tyt]: https://encyclopediaofmath.org/wiki/Types,_theory_of
[em:ure]: https://encyclopediaofmath.org/wiki/Atom#Set_theory
[em:uvs]: https://encyclopediaofmath.org/wiki/Universal_set
[em:unv]: https://encyclopediaofmath.org/wiki/Universe
[em:zfc]: https://encyclopediaofmath.org/wiki/ZFC
[em:far]: https://encyclopediaofmath.org/wiki/Arithmetic,_formal
[em:chp]: https://encyclopediaofmath.org/wiki/Continuum_hypothesis
[em:cuv]: https://encyclopediaofmath.org/wiki/G%C3%B6del_constructive_set
[em:fan]: https://encyclopediaofmath.org/wiki/Formal_mathematical_analysis
[em:pea]: https://encyclopediaofmath.org/wiki/Peano_axioms

[ep:alt]: https://plato.stanford.edu/entries/settheory-alternative/
[ep:aoc]: https://plato.stanford.edu/entries/axiom-choice/
[ep:ast]: https://plato.stanford.edu/entries/set-theory/
[ep:con]: https://plato.stanford.edu/entries/set-theory-constructive/
[ep:fol]: https://plato.stanford.edu/entries/logic-classical/
[ep:fom]: https://plato.stanford.edu/entries/philosophy-mathematics/
[ep:inl]: https://plato.stanford.edu/entries/logic-intuitionistic/
[ep:mal]: https://plato.stanford.edu/entries/logic-classical/
[ep:nfs]: https://plato.stanford.edu/entries/quine-nf/
[ep:nst]: https://plato.stanford.edu/entries/settheory-early/
[ep:nwf]: https://plato.stanford.edu/entries/nonwellfounded-set-theory/
[ep:set]: https://plato.stanford.edu/entries/set-theory/basic-set-theory.html
[ep:stp]: https://plato.stanford.edu/entries/paradoxes-contemporary-logic/
[ep:tyc]: https://plato.stanford.edu/entries/type-theory-church/
[ep:tyi]: https://plato.stanford.edu/entries/type-theory-intuitionistic/
[ep:tyt]: https://plato.stanford.edu/entries/type-theory/
[ep:zfc]: https://plato.stanford.edu/entries/set-theory/ZF.html
[ep:cal]: https://plato.stanford.edu/entries/independence-large-cardinals/
[ep:chp]: https://plato.stanford.edu/entries/continuum-hypothesis/
[ep:inc]: https://plato.stanford.edu/entries/goedel-incompleteness/
[ep:inz]: https://plato.stanford.edu/entries/large-cardinals-determinacy/

[wm:can]: https://mathworld.wolfram.com/CardinalNumber.html
[wm:chp]: https://mathworld.wolfram.com/ContinuumHypothesis.html
[wm:pea]: https://mathworld.wolfram.com/PeanoArithmetic.html
[wm:aes]: https://mathworld.wolfram.com/AxiomoftheEmptySet.html
[wm:aoc]: https://mathworld.wolfram.com/AxiomofChoice.html
[wm:aoe]: https://mathworld.wolfram.com/AxiomofExtensionality.html
[wm:aof]: https://mathworld.wolfram.com/AxiomofFoundation.html
[wm:aoi]: https://mathworld.wolfram.com/AxiomofInfinity.html
[wm:aor]: https://mathworld.wolfram.com/AxiomofReplacement.html
[wm:aos]: https://mathworld.wolfram.com/AxiomofSubsets.html
[wm:aou]: https://mathworld.wolfram.com/AxiomoftheSumSet.html
[wm:aps]: https://mathworld.wolfram.com/AxiomofthePowerSet.html
[wm:ast]: https://mathworld.wolfram.com/AxiomaticSetTheory.html
[wm:aup]: https://mathworld.wolfram.com/AxiomoftheUnorderedPair.html
[wm:cls]: https://mathworld.wolfram.com/Class.html
[wm:cst]: https://mathworld.wolfram.com/ConsistencyStrength.html
[wm:ems]: https://mathworld.wolfram.com/EmptySet.html
[wm:fol]: https://mathworld.wolfram.com/First-OrderLogic.html
[wm:hhp]: https://mathworld.wolfram.com/HilbertHotel.html
[wm:nbg]: https://mathworld.wolfram.com/vonNeumann-Bernays-GoedelSetTheory.html
[wm:nst]: https://mathworld.wolfram.com/NaiveSetTheory.html
[wm:ord]: https://mathworld.wolfram.com/OrdinalNumber.html
[wm:pcl]: https://mathworld.wolfram.com/ProperClass.html
[wm:pws]: https://web.archive.org/web/20230315013335/https://mathworld.wolfram.com/PowerSet.html
[wm:rup]: https://mathworld.wolfram.com/RussellsAntinomy.html
[wm:set]: https://mathworld.wolfram.com/Set.html
[wm:stt]: https://mathworld.wolfram.com/SetTheory.html
[wm:tyt]: https://mathworld.wolfram.com/Type.html
[wm:ure]: https://mathworld.wolfram.com/Urelement.html
[wm:uvs]: https://mathworld.wolfram.com/UniversalSet.html
[wm:zfa]: https://mathworld.wolfram.com/Zermelo-FraenkelAxioms.html
[wm:zfc]: https://mathworld.wolfram.com/Zermelo-FraenkelSetTheory.html

[wp:acb]: https://en.wikipedia.org/wiki/Axiom_of_constructibility
[wp:cai]: https://en.wikipedia.org/wiki/Inaccessible_cardinal
[wp:cal]: https://en.wikipedia.org/wiki/Large_cardinal
[wp:can]: https://en.wikipedia.org/wiki/Cardinal_number
[wp:chp]: https://en.wikipedia.org/wiki/Continuum_Hypothesis
[wp:inc]: https://en.wikipedia.org/wiki/G%C3%B6del%27s_incompleteness_theorems
[wp:ind]: https://en.wikipedia.org/wiki/Independence_(mathematical_logic)
[wp:inz]: https://en.wikipedia.org/wiki/List_of_statements_independent_of_ZFC
[wp:muv]: https://en.wikipedia.org/wiki/Multiverse_(set_theory)
[wp:pea]: https://en.wikipedia.org/wiki/Peano_axioms
[wp:roa]: https://en.wikipedia.org/wiki/Robinson_arithmetic
[wp:soa]: https://en.wikipedia.org/wiki/Second-order_arithmetic
[wp:acs]: https://en.wikipedia.org/wiki/Ackermann_set_theory
[wp:aes]: https://en.wikipedia.org/wiki/Axiom_of_the_empty_set
[wp:alt]: https://en.wikipedia.org/wiki/Alternative_set_theory
[wp:aoc]: https://en.wikipedia.org/wiki/Axiom_of_choice
[wp:aoe]: https://en.wikipedia.org/wiki/Axiom_of_extensionality
[wp:aof]: https://en.wikipedia.org/wiki/Axiom_of_regularity
[wp:aoi]: https://en.wikipedia.org/wiki/Axiom_of_infinity
[wp:aor]: https://en.wikipedia.org/wiki/Axiom_schema_of_replacement
[wp:aos]: https://en.wikipedia.org/wiki/Axiom_schema_of_specification
[wp:aou]: https://en.wikipedia.org/wiki/Axiom_of_union
[wp:aps]: https://en.wikipedia.org/wiki/Axiom_of_power_set
[wp:asc]: https://en.wikipedia.org/wiki/Axiom_schema
[wp:ast]: https://en.wikipedia.org/wiki/Set_theory
[wp:aup]: https://en.wikipedia.org/wiki/Axiom_of_pairing
[wp:cls]: https://en.wikipedia.org/wiki/Class_(set_theory)
[wp:con]: https://en.wikipedia.org/wiki/Constructive_set_theory
[wp:cst]: https://en.wikipedia.org/wiki/Equiconsistency
[wp:cuv]: https://en.wikipedia.org/wiki/Constructible_universe
[wp:cxt]: https://en.wikipedia.org/wiki/Conservative_extension
[wp:elm]: https://en.wikipedia.org/wiki/Element_(mathematics)
[wp:ems]: https://en.wikipedia.org/wiki/Empty_set
[wp:fol]: https://en.wikipedia.org/wiki/First-order_logic
[wp:fom]: https://en.wikipedia.org/wiki/Foundations_of_mathematics
[wp:fms]: https://en.wikipedia.org/wiki/Formal_system
[wp:fot]: https://en.wikipedia.org/wiki/List_of_first-order_theories#Set_theories
[wp:guv]: https://en.wikipedia.org/wiki/Grothendieck_universe
[wp:hes]: https://en.wikipedia.org/wiki/Hereditary_set
[wp:hhp]: https://en.wikipedia.org/wiki/Hilbert%27s_paradox_of_the_Grand_Hotel
[wp:imp]: https://en.wikipedia.org/wiki/Impredicativity
[wp:inl]: https://en.wikipedia.org/wiki/Intuitionistic_logic
[wp:mal]: https://en.wikipedia.org/wiki/Mathematical_logic
[wp:nbg]: https://en.wikipedia.org/wiki/Von_Neumann%E2%80%93Bernays%E2%80%93G%C3%B6del_set_theory
[wp:nfs]: https://en.wikipedia.org/wiki/New_Foundations
[wp:nst]: https://en.wikipedia.org/wiki/Naive_set_theory
[wp:nuv]: https://en.wikipedia.org/wiki/Von_Neumann_universe
[wp:nwf]: https://en.wikipedia.org/wiki/Non-well-founded_set_theory
[wp:ord]: https://en.wikipedia.org/wiki/Ordinal_number
[wp:pws]: https://en.wikipedia.org/wiki/Power_set
[wp:rup]: https://en.wikipedia.org/wiki/Russell%27s_paradox
[wp:sbn]: https://en.wikipedia.org/wiki/Set-builder_notation
[wp:set]: https://en.wikipedia.org/wiki/Set_(mathematics)
[wp:stp]: https://en.wikipedia.org/wiki/Paradoxes_of_set_theory
[wp:trs]: https://en.wikipedia.org/wiki/Transitive_set
[wp:tyt]: https://en.wikipedia.org/wiki/Type_theory
[wp:ure]: https://en.wikipedia.org/wiki/Urelement
[wp:uvs]: https://en.wikipedia.org/wiki/Universal_set
[wp:unv]: https://en.wikipedia.org/wiki/Universe_(mathematics)#In_set_theory
[wp:wfr]: https://en.wikipedia.org/wiki/Well-founded_relation
[wp:wor]: https://en.wikipedia.org/wiki/Well-order
[wp:zfc]: https://en.wikipedia.org/wiki/Zermelo%E2%80%93Fraenkel_set_theory
[wp:tfr]: https://en.wikipedia.org/wiki/Transfinite_induction#Transfinite_recursion
[wp:fuz]: https://en.wikipedia.org/wiki/Fuzzy_set_theory
[wp:sub]: https://en.wikipedia.org/wiki/Theory_(mathematical_logic)#Subtheories_and_extensions
[wp:agc]: https://en.wikipedia.org/wiki/Axiom_of_global_choice
[wp:dop]: https://en.wikipedia.org/wiki/Ordered_pair#Defining_the_ordered_pair_using_set_theory
[wp:thm]: https://en.wikipedia.org/wiki/Theorem

XXX link to:
https://en.wikipedia.org/wiki/Glossary_of_logic

### Notes: Basic Definitions: Set, Class, Multiset, Family

___TOC:___

The definitions below link to these sources:
| link format | source |
|:--:|:--|
| [em:...] | [Encyclopedia of Mathematics](https://encyclopediaofmath.org), now open access, experts articles refereed by members of the [European Mathematical Society](https://euro-math-soc.eu) |
| [ep:...] | [Stanford Encyclopedia of Philosophy](https://plato.stanford.edu/), experts articles refereed by members of [Philosophy Department, Stanford University](https://philosophy.stanford.edu/) |
| [wm:...] | [Wolfram MathWorld](https://mathworld.wolfram.com/about/), entries by experts and contributors, hosted by [Wolfram Research](https://www.wolfram.com/) |
| [wp:...] | [Wikipedia](https://en.wikipedia.org), community-edited encyclopedia, hosted by the [Wikimedia Foundation](https://en.wikipedia.org/wiki/Wikimedia_Foundation) |

Other online resources (not used): \
[Encyclop�dia Britannica: Set Theory](https://www.britannica.com/science/set-theory), open access, experts articles

### 1 Basic Definitions:

#### 1.1 Def: Set, Class, Axiomatic Set Theory

| definition | meaning, comments |
|:--|:--|
| a _set_ | a finite or infinite collection of objects, called elements or members, in which order and multiplicity have no significance [[wm:set][]], [[wp:set][]], [[ep:set][]] |
| - _naive set theory_ | early study of sets defined informally in natural language; gives rise to logical contradictions (see _paradoxes_) [[em:nst][]], [[ep:nst][]], [[wm:nst][]], [[wp:nst][]] |
| - _axiomatic set theory_ | study of sets using formal logic; various _axiom systems_ with relative _consistency strengths_ exist (see below) [[em:ast][]], [[ep:ast][]], [[wm:ast][]], [[wp:ast][]] |
| | |
| _notations:_ | |
| $`S=\{4,2,1,3\}`$ | _roster_ or _enumeration notation,_ same as $`S=\{1,2,3,4,4,4\}`$ $`\;`$ (extensional definition: listing all elements) [[wp:set][]] |
| $`S=\{...,-2,-1,0,1,2,...\}\;`$ | infinite sets in roster notation with ellipsis at the end, same as $`S=\{0,1,−1,2,-2,...\}\;`$ (ostensive definition: giving examples) [[wp:set][]] |
| $`S=\{x\;|\;P(x)\}\;`$ or $`\;\{x:P(x)\}`$ | $`x`$ such that $`P,`$ $`x`$ for which $`P,`$ _set-builder notation_ or _set comprehension_ with a predicate $`P\;`$ (intensional definition: giving membership rule) [[wp:sbn][]] |
| $`\{\}\;`$ or $`\;\emptyset\;`$ or $`\;\varnothing\;`$ or the _empty set_ | the unique set having no elements, a subset of any set $`\;`$ (LaTeX: \\{\\}, \emptyset, \varnothing) [[em:ems][]], [[wm:ems][]], [[wp:ems][]] |
| $`a\in{S}\;`$, $`\;a\notin{S}\;`$, $`\;S\ni{a}\;`$, $`\;S\notni{a}`$ | $`a`$ is element of $`S\;`$, $`a`$ is not element of $`S\;`$, $`S`$ contains $`a\;`$, $`S`$ does not contain $`a`$ $`\;`$ (LaTeX: \in , \notin , \ni , \notni) [[wp:elm][]] |
| | |
| _paradoxes:_ | counter-intuitive results and logical contradictions (_antinomies_) in naive set theory (and logic), examples: [[wp:stp][]], [[ep:stp][]] |
| _Russell's antinomy_ | the set of all sets that are not members of themselves, then the set is a member of itself if and only if it is not [[em:rup][]], [[wm:rup][]], [[wp:rup][]] |
| _Hilbert's (infinite) hotel_ | a fully occupied hotel with infinitely many rooms may still host additional guests, infinitely many of them, infinitely often [[em:hhp][]], [[wm:hhp][]], [[wp:hhp][]] |
| _the set of all ..._ | ... sets / cardinal numbers / ordinal numbers, see Cantor's paradox, Burali-Forti paradox, and others in [[wp:stp][]], [[em:rup][]] |
| | |
| a _class_ | - in a general context: often synonymous with _set,_ that is, a collection of objects with some common, definite property [[em:cls][]], [[wm:cls][]], [[wp:cls][]] |
| | - in axiomatic set theory: a generalized set that cannot be member of another entity (set or class$`^\dagger`$) to avoid paradoxes [[em:ast][]], [[wm:ast][]], [[wp:ast][]] |
| | $`\quad`$ - in _ZFC_ (see below): classes are not defined by axioms but a notational construct allows to specify _virtual classes,_ which only contain sets [[wp:zfc][]] |
| | $`\quad`$ - in _NBG_ (see below): a class is a collection of sets defined by a formula whose quantifiers range only over sets (not classes) [[wp:nbg][]] |
| a _set_ or a _small class_ | a class that belongs to at least one class, i.e., a class that is not a _proper class_ [[wp:cls][]], [[wp:nbg][]] |
| a _proper class_ | a class that does not belong to any classes$`^\dagger`$, i.e., a class that is not a _set_ [[wp:cls][]], [[wp:nbg][]] |
| | examples: the _proper class_ of all sets (the _universal class_) / of all cardinal numbers / of all ordinal numbers [[wp:cls][]] |
| | $`^\dagger`$ exception: for example, _Ackermann set theory_ (AST) via other restrictions _allows_ for proper classses to be member of another proper class [[wp:acs][]] |

#### 1.1 Summary: ZFC, NBG, Properties of axiomatic set theories

| definition | meaning, comments |
|:--|:--|
| _axiomatic set theory_ | study of formal set theories, such as _ZFC_ (see below), as a branch of _mathematical logic_ [[em:ast][]], [[ep:ast][]], [[wm:ast][]], [[wp:ast][]], [[em:mal][]], [[wp:mal][]], [[ep:mal][]] |
| _alternative set theories_ | axiom systems that differ significantly from ZFC and its relatives [[em:alt][]], [[ep:alt][]], [[wp:alt][]], [[wp:fot][]], [[ep:con][]], [[wp:con][]], [[ep:nwf][]], [[wp:nwf][]] |
| _foundation of mathematics_ | set theory is the standard foundation: objects can be viewed as sets, theorems be derived from (suitable) axioms of set theory [[ep:ast][]], [[ep:fom][]], [[wp:fom][]] |
| | example: classical _formal arithmetic_ is equivalent to ZFC without the axiom of infinity [[em:far][]]; note: theorems are proven/provable statements [[wp:thm][]] |
| _type theory_ | formal systems that decompose the object domain into _strata_ or _type hierarchies_ [[ep:tyt][]], [[wp:tyt][]], [[em:tyt][]], [[wm:tyt][]], [[wp:fms][]] |
| | note: type theory is an alternative foundation of mathematics and is fundamental in logic and computer science [[ep:tyt][]], [[ep:tyc][]], [[ep:tyi][]] |
| | |
| properties of set theories: | [collated:] |
| - formal (inference) systems | theory formulated in, e.g.: _first-order logic_ ($`\pm`$), _intuitionistic logic_ (e.g., _constructive set theory_) [[wp:fol][]], [[ep:fol][]], [[wp:inl][]], [[ep:inl][]], [[wp:con][]], [[ep:con][]] |
| - atomic predicates | standard $`.\!=\!.`$ (equality) and $`.\!\in\!.`$ (membership), or some nonstandard membership relation (e.g., _Fuzzy Set Theory_) [[em:ast][]], [[wp:ast][]], [[ep:ast][]], [[wp:fuz][]] |
| - ontology | what entities get defined: _hereditary sets_ (see below), _urelements_ (see below), _proper classes_ (see above), or other [[wp:ast][]], [[ep:ast][]], [[wp:alt][]], [[ep:alt][]] |
| - universes/models | which _universal sets/classes_ are admitted by the axioms [[em:uvs][]], [[wp:uvs][]], [[wm:uvs][]], [[em:unv][]], [[wp:unv][]], [[wp:cuv][]], [[wp:nuv][]], [[wp:guv][]], [[wp:zfc][]] |
| - regularity/foundation | whether sets are _well-founded_ or if sets (_hypersets_) can be elements of themselves (e.g, _New Foundations_) [[wp:wfr][]], [[wp:nwf][]], [[ep:nwf][]], [[wp:nfs][]], [[ep:nfs][]] |
| - axioms | choice of which axioms or stronger/weaker/negated versions are [not] included (e.g., the _Axiom of Choice_ (AC)) [[wp:alt][]], [[wp:fot][]], [[wp:aoc][]], [[ep:aoc][]] |
| - finitely axiomatizable | whether the theory can be formulated (in first-order logic) without _axiom schemas_ denoting an infinite family of axioms [[wp:asc][]], [[wp:fot][]], [[wp:zfc][]] |
| - consistency strength | a theory has _greater_ consistency strength than another theory if its own consistency implies the other's, _equiconsistent_ if both ways [[wp:cst][]], [[wm:cst][]] |
| - extension | a theory is a _conservative extension_ of another if it proves the same theorems; it is a _proper extension_ if it also proves new theorems [[wp:cxt][]], [[wp:sub][]] |
| | |
| _ZF, ZFC_ | _Zermelo-Fraenkel set theory_ (ZF) with the _axiom of choice_ (C); modern set theory's standard [[em:zfc][]], [[ep:zfc][]], [[wm:zfc][]], [[wp:zfc][]] |
| | note: different (yet equivalent) formulations of the ZFC axioms exist (within first-order logic) [[em:zfc][]], [[ep:zfc][]], [[wm:zfc][]], [[wm:zfa][]], [[wp:zfc][]] |
| _ZFC_ axioms: | [informal:] |
| - _extensionality_ | _isEqual:_ sets formed by the same elements are equal; sets are uniquely characterized by their members [[wm:aoe][]], [[wp:aoe][]], [[wp:set][]] |
| - _empty set_ | _empty:_ there's a set $`\varnothing`$ that has no members; sometimes omitted, since it can be derived (_non-independence_) [[wm:aes][]], [[wp:aes][]] |
| - _pairing_ | _pair:_ there's a set $`\{x,y\}`$ for any two sets $`x`$ and $`y`$; sometimes omitted; can also form _ordered pairs_ (see below) [[wm:aup][]], [[wp:aup][]] |
| - _union_ | _flatten:_ for any set $`x`$, there's a set $`\bigcup{x}`$ that consists of just the elements of the elements of $`x`$ [[wm:aou][]], [[wp:aou][]] |
| - _power set_ | _haveSubsetsAsSet:_ for any set $`x`$, there's a set of all subsets of $`x`$; _impredicative_ (self-referential) [[wm:aps][]], [[wp:aps][]], [[wp:imp][]], [[wp:fot][]] |
| - _infinity_ | _haveOmegaAsSet:_ there's a set closed under $`x\mapsto{x\cup\{x\}}`$ (_successor_); admits the _von Neumann ordinal_ $`\omega`$ (see below) [[wm:aoi][]], [[wp:aoi][]] |
| - _replacement_ | _map:_ the image of any set under any definable mapping is a set; axiom _schema_ (infinite family of axioms) [[wm:aor][]], [[wp:aor][]], [[wp:asc][]] |
| - _separation_ | _filter:_ any definable subclass of a set is a set (_restricted comprehension_); axiom _schema;_ sometimes omitted [[wm:aos][]], [[wp:aos][]], [[wp:asc][]] |
| - _regularity_ | _onlyWellFoundedSets:_ every nonempty set $`x`$ has a member _disjoint_ from $`x`$; so, no set can contain itself [[wm:aof][]], [[wp:aof][]], [[wp:wfr][]] |
| - _choice_ | _alwaysSomeChoiceFunction:_ for any set $`x`$ of nonempty sets, there's a function selecting an element from each set of $`x`$ [[wm:aoc][]], [[wp:aoc][]], [[ep:aoc][]] |
| | note: ZF is not finitely axiomatizable [[em:zfc][]], [[ep:ast][]], [[wm:zfc][]], [[wp:zfc][]] (in first-order logic [[wm:fol][]], [[wp:fol][]], [[em:fol][]], [[ep:fol][]]) |
| | |
| _NBG_ | _von Neumann-Bernays-G�del set theory,_ a conservative extension of ZFC on sets that defines _classes_ and has _finitely many_ axioms: [[wm:nbg][]], [[wp:nbg][]] |
| | - formally introduces classes as a collection of sets defined by a formula whose quantifiers range only over sets (lowercase variables) [[wp:nbg][]] |
| | - the infinite number of ZF axioms is replaced by a finite number of axioms containing a class variable (uppercase letters) [[em:ast][]] |
| | - NBG states the stronger of AC: the _axiom of global choice,_ which applies to proper classes as well as sets [[wp:agc][]], [[wp:nbg][]] |
| | note: NBG and ZFC imply the same statements about sets, but NBG is more expressive as it can make statements about classes [[wp:nbg][]], [[em:ast][]] |

#### 1.1 Def: Ordered Pair, Urelement, Hereditary Set, von Neumann Ordinals, Universes

| definition | meaning, comments |
|:--|:--|
| _ordered pair_ $`(x,y)`$ | typically defined as $`\;\{\{x\},\{x,y\}\}\;`$, which satisfies the _characteristic property:_ $`\;(a,b)= (c,d)\longleftrightarrow{a=c}\land{b=d}\;`$ [[wp:dop][]] |
| | note: this definition also admits the "accidental" theorems $`\;\{a\}\in(a,b)\;`$ and $`\;(a,a)=\{\{a\}\}\;`$; for other set-theoretic definitions see [[wp:dop][]] |
| | |
| _urelement_ or _atom_ | an object that contains no elements, belongs to some set, and is not identical with the empty set [[em:ure][]], [[wm:ure][]], [[wp:ure][]] |
| | note: there's a loose duality: urelements cannot _have_ members whereas proper classes cannot _be_ members (of sets) [[wp:ure][]] |
| _hereditary_ or _pure set_ | a set whose elements are all pure sets (inductive definition), that is, no urelements or other objects; example: $`\{\emptyset,\{\emptyset\}\}`$ [[wp:hes][]] |
| a _transitive set_ (or _class_) | a set/class in which $`.\!\in\!.`$ (membership) is a transitive relation, that is: whenever $`x\in{A}`$ and $`y\in{x}`$, then $`y\in{A}`$ [[em:trs][]], [[wp:trs][]] |
| | note: equivalent (for hereditary sets): a set for which each element is also a subset; a set that is a subset of its own power set [[wp:trs][]] |
| | |
| _von Neumann ordinals_ | the ordinal numbers axiomatically constructed as hereditary, transitive (and well-ordered) sets [[wm:ord][]], [[wp:ord][]], [[wp:wfr][]], [[wp:wor][]] |
| | - each ordinal is the set of all smaller ordinals [[wp:ord][]]: $`\;0=\emptyset\;,\;1=\{0\}=\{\emptyset\}\;,\;2=\{0,1\}=\{\emptyset,\{\emptyset\}\}\;,\;3=\{0,1,2\}\;,\;\ldots\;`$ |
| | - as _transfinite sequence_ [[wp:tfr][]]: $`\;0=\emptyset\;,\;1=\{0\}\;,\;2=\{0,1\}\;,\;\ldots\;,\;\omega=\{0,1,\ldots\}=\mathbb{N}\;,\;\omega+1=\omega\cup\{\omega\}\;,\;\ldots\;,\;\omega\cdot{2}\;,\;\ldots`$ |
| | |
| _universe_ | the set/class/type/collection consisting of all objects considered in a theory and possessing suitable properties, such as, _closed under ..._ [[em:unv][]], [[wp:unv][]] |
| _universal set/class_ | the set/class consisting of all objects, including itself; complement of the empty set [[wm:uvs][]], [[wp:uvs][]], [[em:uvs][]] |
| | examples: the Constructible universe $`L`$, the Von Neumann universe $`V`$, a Grothendieck universe, a Herbrand universe [[wp:cuv][]], [[wp:nuv][]], [[wp:guv][]], [[wp:huv][]]|

#### 1.1 Def: Interpretation, Model

| definition | meaning, comments |
|:--|:--|
| _interpretation of ..._ | sentence, language  |
| _interpretation of_ a first-order language | assigns a denotation to each non-logical symbol (predicate symbol, function symbol, or constant symbol) in that language [[wp:fol][]] |
| | note: also determines a domain of discourse that specifies the range of the quantifiers [[wp:fol][]] |
| | note: the result is that each term is assigned an object, each predicate is assigned a property of objects, and each sentence is assigned a truth value [[wp:fol][]] |
| _valuation_ | The interpretation must be a homomorphism, while valuation is simply a function [[wp:val][]] |
| | |
| a _formal system_ | a _formal language_ together with a _proof calculus_ consisting of _axioms_ and _rules of inference_ to infer _theorems_ [[wp:fms][]] |
| a _formal language_ | the words whose letters are taken from an alphabet and are well-formed according to a set of rules called a formal grammar [[wp:fln][]] |
| a _formula_ or _well-formed formula_ | a finite sequence of symbols from a given alphabet that is part of a formal language [[wp:wff][]] |
| | note: abbreviated as _wff,_ pronounced "woof" or sometimes "whiff" [[wp:wff][]] |
| a _closed formula_ or _sentence_ | a formula with no free variables in the language [[wp:sen][]], [[wp:val][]] |
| | |
| a _structure_ | in _universal algebra_ and in _model theory:_ a set with finitary operations and relations defined on it [[em:str][]], [[em:als][]], [[wp:str][]] |
| | also called: an _algebraic system_ [[em:als][]] (compare: _algebraic structure_) |
| a _first-order structure_ | a domain of discourse D and an interpretation function I mapping non-logical symbols to predicates, functions, and constants [[wp:fol][]] |
| | |
| _model_ | overloaded term in math, science, technology: 5,383 Wikipedia page titles containing 'model' (at present) [[wp:mdl][]] |
| | - in ...: XXX [[wm:mot][]], [[wp:mot][]], [[ep:mot][]], [[ep:mop][]], [[em:mot][]] |
| a _model of_ a theory | - in logic: an _interpretation_ of a formal language satisfying certain axioms [[em:mod][]], [[em:str][]], [[wp:str][]], [[em:als][]], [[wp:als][]] |
| | a _structure_ that satisfies the defining axioms of that theory [[wp:str][]] |
| | |
| an _algebraic structure_ or an _algebra_ | XXX [[wp:alg][]] |
| | |
| _model theory_ | the study of first-order theories, including foundational structures such as models of set theory [[wp:str][]] XXX [[wm:mot][]], [[wp:mot][]], [[ep:mot][]], [[ep:mop][]], [[em:mot][]] |
| _universal algebra_ | the sudy of structures of first-order theories with no relation symbols [[wp:str][]] |
| | |
| a _first-order theory_ of a signature | a set of axioms, which are sentences consisting of symbols from that signature and considered to hold within the theory [[wp:fol][]] |

[wp:sen]: https://en.wikipedia.org/wiki/Sentence_(mathematical_logic)
[wp:wff]: https://en.wikipedia.org/wiki/Well-formed_formula
[wp:fln]: https://en.wikipedia.org/wiki/Formal_language

[wp:huv]: https://en.wikipedia.org/wiki/Herbrand_structure

[wm:mot]: https://mathworld.wolfram.com/ModelTheory.html
[wp:mot]: https://en.wikipedia.org/wiki/Model_theory
[ep:mot]: https://plato.stanford.edu/entries/modeltheory-fo/
[ep:mop]: https://plato.stanford.edu/entries/model-theory/
[em:mot]: https://encyclopediaofmath.org/wiki/Model_theory

[em:mod]: https://encyclopediaofmath.org/wiki/Model_(in_logic)
[em:str]: https://encyclopediaofmath.org/wiki/Structure
[wp:str]: https://en.wikipedia.org/wiki/Structure_(mathematical_logic)
[em:als]: https://encyclopediaofmath.org/wiki/Algebraic_system

[wp:alg]: https://en.wikipedia.org/wiki/Algebraic_structure
https://en.wikipedia.org/wiki/Theorem
https://en.wikipedia.org/wiki/Axiom
https://en.wikipedia.org/wiki/Formation_rules

https://en.wikipedia.org/wiki/Mathematical_model
https://en.wikipedia.org/wiki/Propositional_logic#Semantics
https://en.wikipedia.org/wiki/First-order_logic#Semantics

[wp:val]: https://en.wikipedia.org/wiki/Valuation_(logic)
[wp:int]: https://en.wikipedia.org/wiki/Interpretation_(logic)
[wp:inm]: https://en.wikipedia.org/wiki/Interpretation_(model_theory)
[em:int]: https://encyclopediaofmath.org/wiki/Interpretation


https://en.wikipedia.org/wiki/Proof_theory
https://en.wikipedia.org/wiki/Semantics_of_logic

https://stanford-cs221.github.io/autumn2020-extra/modules/logic/propositional-logic-semantics.pdf

[wp:mdl]: https://en.wikipedia.org/w/index.php?search=intitle%3A%22Model%22&title=Special:Search&ns0=1

#### 1.1 Resources: Criticisms of ZFC, Independence Results, Multiverse

| criticisms of ZFC: | comments, resources |
|:--|:--|
| - fails to capture objects | examples: _proper classes_ (see above), the _universal set_ (see above) [[wp:zfc][]] |
| - is too strong | many mathematical theorems can be proven in much weaker systems [[wp:zfc][]] |
| | examples (in order of increasing strength): |
| | - _Robinson arithmetic_ (Q): natural numbers, yet already _incomplete_ and _undecidable_ (under G�del's incompleteness theorems) [[wp:roa][]], [[wp:inc][]], [[ep:inc][]] |
| | - _Peano arithmetic_ (PA): theory of natural numbers, equiconsistent with ZFC with _axiom of infinity_ replaced by its negation [[wm:pea][]], [[em:pea][]], [[wp:pea][]] |
| | - _second-order arithmetic_ (Z$`_2`$): supports _analysis_ on the real numbers, can prove essentially all of the results of classical mathematics [[wp:soa][]], [[em:fan][]] |
| - is too weak | numerous _independence results:_ statements that can neither be proven nor disproven from ZFC alone [[wp:zfc][]], [[wp:ind][]], [[wp:inz][]], [[ep:inz][]] |
| | examples: |
| | - _continuum hypothesis:_ $`2^{\aleph_0}=\aleph_1\;`$, no sets with cardinality between the integers and the real numbers [[wm:chp][]], [[em:chp][]], [[wp:chp][]], [[ep:chp][]], [[ep:ast][]] |
| | - _large cardinals:_ there are some uncountable cardinals with properties that make them very large [[wm:can][]], [[wp:can][]], [[wp:cal][]], [[ep:cal][]], [[wp:cai][]], [[ep:ast][]] |
| | - _axiom of constructibility:_ $`V=L\;`$ (Von Neumann universe $`=`$ Constructible universe) [[wp:nuv][]], [[wp:cuv][]], [[em:cuv][]], [[wp:acb][]], [[wp:zfc][]], [[ep:ast][]] |

Conclusion by [[ep:ast][]]:

> [...] there are now literally hundreds of problems and questions, in practically all areas of mathematics, that have been shown independent of ZFC. These include almost all important questions about the structure of uncountable sets. One might say that the undecidability phenomenon is pervasive, to the point that the investigation of the uncountable has been rendered nearly impossible in ZFC alone.

Approach #1: The search for an "ultimate" axiom system [[ep:ast][]]

> [...] common one among mathematicians, is G�del's position: the undecidability only shows that the ZFC system is too weak to answer those questions, and therefore one should search for new axioms that once added to ZFC would answer them. The search for new axioms has been known as G�del's Program.

Approach #2: The "multiverse" view [[wp:muv][]]

> In mathematical set theory, the multiverse view is that there are many models of set theory, but no "absolute", "canonical" or "true" model. The various models are all equally valid or true, though some may be more useful or attractive than others. The opposite view is the "universe" view of set theory in which all sets are contained in some single ultimate model.

#### 1.1 Def: Subset, Power Set, Universes XXX

For arbitrary sets A, B:

| definition | meaning, comments |
|:--|:--|
| $`\mathbb{P}(A)\;`$ or $`\;\mathcal{P}(A)\;`$ or $`\;2^A\;`$ or ... the _power set_ of $`A`$ | the set of all subsets of $`A`$ including the empty set and $`A`$ itself $`\;`$ (LaTeX: \mathbb, \mathcal, 2^A) [[em:pws][]], [[wm:pws][]], [[wp:pws][]] |
| $`A^B`$ | the set of functions or maps from $`B`$ to $`A`$  [[wm:set][]] |

https://en.wikipedia.org/wiki/Algebra_of_sets
https://en.wikipedia.org/wiki/Boolean_algebra_(structure)
https://en.wikipedia.org/wiki/Field_of_sets
https://en.wikipedia.org/wiki/%CE%A3-algebra


#### 1.1 Def: Multiset, Collection

https://encyclopediaofmath.org/wiki/Multiset

https://en.wikipedia.org/wiki/Multiset

https://mathworld.wolfram.com/Multiset.html

XXX: only in wm:
https://mathworld.wolfram.com/Collection.html


#### 1.1 Def: Family, Indexed Family, Sequence

[wk]: https://en.wikipedia.org/wiki
[wp:fms]: https://en.wikipedia.org/wiki/Family_of_sets
[wp:pws]: https://en.wikipedia.org/wiki/Power_set

[wp:seq]: https://en.wikipedia.org/wiki/Sequence
[wp:ixf]: https://en.wikipedia.org/wiki/Indexed_family

[wm]: https://mathworld.wolfram.com
[wm:ixf]: https://mathworld.wolfram.com/Family.html
[wm:seq]: https://mathworld.wolfram.com/Sequence.html

[em]: https://encyclopediaofmath.org
[em:seq]: https://encyclopediaofmath.org/wiki/Sequence
[em:pws]: https://encyclopediaofmath.org/wiki/Power_set

XXX: only in wp:
https://en.wikipedia.org/wiki/Parametric_family

XXX quote:
[wp:tst]: https://encyclopediaofmath.org/wiki/Topological_structure_(topology)

The mathematical term _family of $`\ldots`$_ needs some disambiguation due to
its overloaded usage.

For a set $`X`$ of elements:

| definition | meaning, comments |
|:--|:--|
| a _family of..._ or a _collection of..._ | a nondescript collection, which can mean any of: a _set,_ _indexed set,_ _multiset,_ or _class_ [[wp:fms][]] |
| | note: the context informs about a given index set, or whether repetition is allowed, or definition by a common property |
| | note: [[wm][]] compatibly defines _family_ as _indexed family_ (see below) with no restrictions on the index set |
| | note: [[em][]] lacks a definiton of _family_ (but lists specific families of sets/functions/distributions) |
| | |
| a _family of sets_ | self-explanatory; also called: a _set family_ or a _set system_ [[wp:fms][]] |
| a _family of subsets_ of $`X`$ | self-explanatory; also called: a _family of sets_ over $`X`$ [[wp:fms][]] |
| | example: the _power set_ of $`X`$ is a family of subsets of $`X`$ denoted as $`\mathbb{P}(X)`$, $`\mathcal{P}(X)`$, $`\wp(X)`$ et al. [[wp:pws][]], [[em:pws][]] |
| | note: conversely, any family of subsets of $`X`$ is a subset of the _power set_ when repetition is not allowed [[wp:fms][]] |
| | example: a finite family of subsets of a finite set $`X`$ is also called a _hypergraph_ (may allow for multiple edges) [[wp:fms][]] |
| | |
| an _indexed family_ $`\;(a_i)_{i\in{I}}\;`$ or $`\;(a_i)\;`$ | a collection of objects, each associated with an index from a given or implied, non-empty _index set_ $`I`$ [[wm:ixf][]] |
| | also called: a _family of elements in $`X`$ indexed by $`I`$_ [[wp:ixf][]] |
| | formally: a function $`f:I\to{X}`$ with $`x_i:=f(i)`$ denoting the _term of index $`i`$ of the family_ [[wp:ixf][]], [[wm:ixf][]] |
| | also written as: $`\;\{a_i\}_{i\in{I}}\;`$ or $`\;\{a_i\}\;`$ at the risk of confusing indexed families with sets [[wp:ixf][]], [[wm:ixf][]] |
| | note: the index set or (equivalently) the family are not required to be _countable_ [[wp:ixf][]], [[wm:ixf][]] |
| | note: functions and indexed families are formally equivalent; in practice, though, a family is viewed as a collection [[wp:ixf][]] |
| | note: every set $`X`$ gives rise to a family $`f:X\to{X},f(x)=x`$, from which the set can be recovered as the range [[wm:ixf][]] |
| | note: every family also gives rise to a set $`X=\{a_i\;|\;i\in{I}\}`$, from which the family in general cannot be recovered [[wm:ixf][]] |
| | |
| a _sequence_ $`\{a_n\}`$ | XXX a function whose domain is an interval of integers [[wp:seq][]] a family with index set $`I=\mathbb{N}`$ [[wp:seq][]], [[wm:seq][]] |
| | $`\{a_n\}_{n\in\mathbb{N}}`$ |


#### 1.1 Def: Relation, Correspondence

https://encyclopediaofmath.org/wiki/Relation
https://encyclopediaofmath.org/wiki/Correspondence
https://en.wikipedia.org/wiki/Binary_relation

https://mathworld.wolfram.com/Relation.html
https://mathworld.wolfram.com/BinaryRelation.html

https://en.wikipedia.org/wiki/Relation_(mathematics)
https://en.wikipedia.org/wiki/Binary_relation

#### 1.1 Def: Function, Mapping, Map, Domain, Codomain, Image

https://encyclopediaofmath.org/wiki/Function
https://encyclopediaofmath.org/wiki/Mapping https://encyclopediaofmath.org/index.php?title=Codomain&redirect=no
https://encyclopediaofmath.org/wiki/Domain_of_definition
https://encyclopediaofmath.org/wiki/Range_of_values

https://mathworld.wolfram.com/Function.html
https://mathworld.wolfram.com/Map.html
https://mathworld.wolfram.com/Domain.html
https://mathworld.wolfram.com/Range.html
https://mathworld.wolfram.com/Codomain.html

https://en.wikipedia.org/wiki/Function_(mathematics)
https://en.wikipedia.org/wiki/Map_(mathematics)
https://en.wikipedia.org/wiki/Domain_of_a_function
https://en.wikipedia.org/wiki/Range_of_a_function
https://en.wikipedia.org/wiki/Image_(mathematics)
https://en.wikipedia.org/wiki/Codomain

https://en.wikipedia.org/wiki/Injective
https://en.wikipedia.org/wiki/Surjective
https://en.wikipedia.org/wiki/Bijective


Under the usual set-theoretic definition of a function as an ordered triplet (or equivalent ones), there is exactly one empty function for each set, thus the empty function ∅ → X {\displaystyle \varnothing \to X} is not equal to ∅ → Y {\displaystyle \varnothing \to Y} if and only if X ≠ Y {\displaystyle X\neq Y}, although their graphs are both the empty set.
https://en.wikipedia.org/wiki/Empty_function#Standard_functions

https://functions.wolfram.com/

[Up](./README.md)
