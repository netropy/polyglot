# polyglot/math

### Refcard: 10 Abstract Spaces in Functional Analysis

___TOC:___

___Motivation:___ Memorize these 10 _mathematical spaces_ with their key
properties and subset relationships in form of an _Euler diagram._
[[wp:eud][]]

![Abstract Spaces](abstract_spaces.svg "Abstract Spaces: Euler diagram")

Resources cited as:
| link format | source |
|:--:|:--|
| [em:...] | [Encyclopedia of Mathematics](https://encyclopediaofmath.org), open access, articles refereed by members of the [European Mathematical Society](https://euro-math-soc.eu) |
| [wm:...] | [Wolfram MathWorld](https://mathworld.wolfram.com/about/), entries by experts and contributors, hosted by [Wolfram Research](https://www.wolfram.com/) |
| [wp:...] | [Wikipedia](https://en.wikipedia.org), community-edited encyclopedia, hosted by the [Wikimedia Foundation](https://en.wikipedia.org/wiki/Wikimedia_Foundation) |

[wp:eud]: https://en.wikipedia.org/wiki/Euler_diagram

---

In mathematics, a space is a set (sometimes known as a universe) endowed with
a structure defining the relationships among the elements of the set. While
modern mathematics uses many types of spaces, such as Euclidean spaces, linear
spaces, topological spaces, Hilbert spaces, or probability spaces, it does not
define the notion of "space" itself.  [wp:spc][]

A space consists of selected mathematical objects that are treated as points,
and selected relationships between these points.   [wp:spc][]

XXX Types of spaces... [wp:spc][]
Every space treated in Section "Types of spaces" above, except for
"Non-commutative geometry", "Schemes" and "Topoi" subsections, is a set (the
"principal base set" of the structure, according to Bourbaki) endowed with
some additional structure; elements of the base set are usually called
"points" of this space. In contrast, elements of (the base set of) an
algebraic structure usually are not called "points".

The concept of a space is an extremely general and important mathematical
construct. Members of the space obey certain addition properties. [wm:spc][]

In contemporary mathematics space is defined as a certain set of objects,
which are called its points; they can be geometric objects, functions, states
of a physical system, etc. By considering such a set as a space one abstracts
any property of its elements and considers only those properties of their
totality that can be defined by relations that are taken into account or that
are introduced by definition. These relations between points and some figures,
i.e. sets of points, determine the "geometry" of the space. In an axiomatic
construction, the basic properties of these relations are expressed in
corresponding axioms. [em:spc][]

[wp:spc]: https://en.wikipedia.org/wiki/Space_(mathematics)
[wm:spc]: https://mathworld.wolfram.com/Space.html
[em:spc]: https://encyclopediaofmath.org/wiki/Space

---

In Mathematics, a structure on a set (or on some sets) refers to providing it
(or them) with certain additional features (e.g. an operation, relation,
metric, or topology). Τhe additional features are attached or related to the
set (or to the sets), so as to provide it (or them) with some additional
meaning or significance. [wp:str][]

a mathematical structure may have more than one definition (for example,
topological space has at least seven definitions; ordered field has at least
two definitions). [wp:eds][]

In mathematics, an algebraic structure consists of a nonempty set A (called
the underlying set, carrier set or domain), a collection of operations on A
(typically binary operations such as addition and multiplication), and a
finite set of identities (known as axioms) that these operations must
satisfy. [wp:ast][]


[wp:eds]: https://en.wikipedia.org/wiki/Equivalent_definitions_of_mathematical_structures
[wp:str]: https://en.wikipedia.org/wiki/Mathematical_structure
[wp:ast]: https://en.wikipedia.org/wiki/Algebraic_structure

---

Functional analysis is a branch of mathematical analysis, the core of which is
formed by the study of vector spaces endowed with some kind of limit-related
structure (for example, inner product, norm, or topology) and the linear
functions defined on these spaces and suitably respecting these
structures. [wp:fna][]

The part of modern mathematical analysis in which the basic purpose is to
study functions y=f(x) for which at least one of the variables x or y varies
over an infinite-dimensional space. [ep:fna][]

Functional analysis is a branch of mathematics concerned with
infinite-dimensional vector spaces (mainly function spaces) and mappings
between them. The spaces may be of different, and possibly infinite,
dimensions. These mappings are called operators or, if the range is on the
real line or in the complex plane, functionals. [wm:fna][]

[wp:fna]: https://en.wikipedia.org/wiki/Functional_analysis
[wm:fna]: https://mathworld.wolfram.com/FunctionalAnalysis.html
[ep:fna]: https://encyclopediaofmath.org/wiki/Functional_analysis

---

#### 1.1 Def: Topological Space $`(\small{X},\tau)`$

XXX quote/shorten

A _topological space_ is a geometrical space in which closeness is defined but
cannot necessarily be measured by a numeric distance.  It is the most general
type of a mathematical space that allows for the definition of limits,
continuity, and connectedness. [[wp:tps][]]

There are several equivalent definitions of a _topology,_ the most commonly
used of which is the definition through _open sets_ (or by the dual notion of
_closed sets_ or via _neighbourhoods_ et al). [[wp:tps][]]

A _topology_ $`\tau`$ on a set $`\small{X}`$ of elements, typically called
_points,_ is a collection of subsets of $`\small{X}`$, called _open sets,_
satisfying the following conditions: [[wp:tps][]], [[wm:tps][]], [[wp:ops][]],
[[wm:ops][]]

| | |
|:--|:--|
| the empty set and $`\small{X}`$ itself belong to $`\tau`$ | $`\emptyset \in \tau\;`$ and $`\;X \in \tau`$ |
| any arbitrary (finite or infinite) union of members of $`\tau`$ belongs to $`\tau`$ | if $`\;\{T_i: i \in I\} \subseteq \tau\;`$ then $`\;\bigcup_{i \in I} T_i \in \tau`$ |
| the intersection of any finite number of members of $`\tau`$ belongs to $`\tau`$ | $`T_1 \cap \cdots \cap T_n \in \tau`$ |

_Core concepts defined in terms of _open sets__

_General topology_ or _point-set topology_ is the "low-level language" that
provides the basic set-theoretic definitions and constructions used in
topology.  [[wp:pst][]], [[wm:pst][]]

The fundamental concepts in point-set topology are _continuity,_
_compactness,_ and _connectedness:_ \
\- Continuous functions, intuitively, take nearby points to nearby points. \
\- Compact sets are those that can be covered by finitely many sets of
   arbitrarily small size. \
\- Connected sets are sets that cannot be divided into two pieces that are
   far apart. \
The terms 'nearby', 'arbitrarily small', and 'far apart' can all be made
precise by using the concept of _open sets._ [[wp:pst][]], [[wm:pst][]]

XXX

A _topological basis_ or _base_ is a subset $`B`$ of a set $`\tau`$ in which
all other open sets can be written as unions or finite intersections of $`B`$.
For the real numbers, the set of all open intervals is a basis. [[wm:tpb][]],
[[wp:tpb][]] By definition, $`\tau`$ is a base for itself, so every topological
space has a base.

But it is often simpler to specify not all the sets making up the given
topology but only some of them (i.e. to specify a base of the given topology),
in terms of which all remaining elements of the topology can be obtained as
unions (in the case of an open topology) or intersections (in the closed case)
of sets belonging to the base [em:tps][]

XXX

The concept of a topological space axiomatizes the relation of absolute
nearness of a point to a set, whereas the concept of a metric space formalizes
the notion of relative nearness of points [em:mes][]

XXX

One may speak of whether two points, or more generally two subsets, of a
topological space are "near" without concretely defining a distance.

The most
common case is given by _manifolds,_ which are topological spaces that, near
each point, resemble an open set of a Euclidean space, but on which no
distance is defined in general. [[wp:ops][]], [[wp:mnf][]], [[wm:mnf][]]


XXX

[wp:tps]: https://en.wikipedia.org/wiki/Topological_space
[wp:ops]: https://en.wikipedia.org/wiki/Open_set
[wp:cds]: https://en.wikipedia.org/wiki/Closed_set
[wp:tpd]: https://en.wikipedia.org/wiki/Topologically_distinguishable
[wp:lip]: https://en.wikipedia.org/wiki/Limit_point
[wp:pst]: https://en.wikipedia.org/wiki/Point-set_topology
[wp:mnf]: https://en.wikipedia.org/wiki/Manifold
[wp:tpb]: https://en.wikipedia.org/wiki/Basis_(topology)
[wp:itv]: https://en.wikipedia.org/wiki/Interval_(mathematics)


[wm:tps]: https://mathworld.wolfram.com/TopologicalSpace.html
[wm:ops]: https://mathworld.wolfram.com/OpenSet.html
[wm:cds]: https://mathworld.wolfram.com/ClosedSet.html
[wm:lip]: https://mathworld.wolfram.com/LimitPoint.html
[wm:mnf]: https://mathworld.wolfram.com/Manifold.html
[wm:tpb]: https://mathworld.wolfram.com/TopologicalBasis.html
[wm:pst]: https://mathworld.wolfram.com/Point-SetTopology.html

[em:tps]: https://encyclopediaofmath.org/wiki/Topological_space

#### 1.2 Def: Vector Space $`(\small{X},\oplus,\odot)`$

A _linear space_ or _vector space_ over a _field_ is a set that is closed
under finite vector addition and scalar multiplication.
[[wm:ves][]], [[wp:ves][]], [em:ves][]

A _field_ is a set on which addition, subtraction, multiplication, and
division are defined and behave as the corresponding operations on rational
and real numbers.  That is, addition and multiplication satisfy the _field
axioms_ of associativity, commutativity, distributivity, identity, and
inverses. [[wp:fld][]], [[wm:fld][]], [em:fld][]

##### Def.:

A vector space over a field $`F`$ is a \
\- non-empty set $`V`$ together with \
\- a binary operation $`\oplus`$, called _vector addition_ or _addition_ and
   denoted by $`+`$, \
\- a binary function $`\odot`$, called _scalar multiplication_ and denoted by
   juxtaposition, \
that satisfy these axioms: [[wp:ves][]], [[wm:ves][]], [em:ves][]

| | for all $`\mathbf{u},\mathbf{v},\mathbf{w}\in{V},`$, $`a,b\in{F}`$: |
|:--|:--|
| (1) | associativity of vector addition: $`\mathbf{u} + (\mathbf{v} + \mathbf{w}) = (\mathbf{u} + \mathbf{v}) + \mathbf{w}`$ |
| (2) | commutativity of vector addition: $`\mathbf{u} + \mathbf{v} = \mathbf{v} + \mathbf{u}`$ | | |
| (3) | identity element of vector addition: there exists $`\mathbf{0} \in{V}:\; \mathbf{v} + \mathbf{0} = \mathbf{v}`$ |
| (4) | inverse elements of vector addition: there exists $`(−\mathbf{v}) \in{V}:\; \mathbf{v} + (−\mathbf{v}) = \mathbf{0}`$ |
| | |
| (5) | associativity of scalar multiplication: $`a(b\mathbf{v}) = (ab)\mathbf{v}`$ |
| (6) | distributivity of scalar sums: $`(a + b)\mathbf{v} = a\mathbf{v} + b\mathbf{v}`$ |
| (7) | distributivity of vector sums: $`a(\mathbf{u} + \mathbf{v}) = a\mathbf{u} + a\mathbf{v}`$ |
| (8) | identity element of scalar multiplication: $`1\mathbf{v} = \mathbf{v}\;`$ where $`\; 1\in{F}:\; 1a = a`$ |

An equivalent and more technical definition of a vector space can be given in
terms of other algebraic concepts.  Axioms (1)..(4) say that a vector space is
an _abelian_ or _commutative group_ under addition.  Axioms (5)..(8) say that
scalar multiplication forms a _ring homomorphism_ from the field $`F`$ into
the _endomorphism ring_ of this group. [wp:ves][], [em:ves][]

##### Terminology:

The term _vector_ is used to refer to: [[wp:vec][]], [[wm:vec][]],
[[em:vec][]] \
\- colloquially, a quantity that cannot be expressed by a single number, i.e.,
   a _scalar_; [[wp:sca][]], [[wm:sca][]], [[em:sca][]] \
\- a _geometric_ or _Euclidean vector_ as an object that has a magnitude (or
   length, modulus) and a direction; [[wp:geo][]] \
\- a _coordinate vector_ as a tuple that describes the vector in terms of a
   particular, ordered basis; [[wp:coo][]] \
\- abstractly, as elements of vector spaces when discussing general
   properties. [[wp:vec][]]

The term _vector_ is typically not used for other mathematical structures that
form vectors spaces, such as an _algebra over a field_ or a _function space._
[[wp:vec][]], [[wp:alf][]], [[wm:alf][]], [[em:alf][]], [[wp:fus][]],
[[wm:fus][]]

##### Concepts:

As a consequence of the _axiom of choice,_ every vector space $`V`$ has a
_vector basis,_ that is, a minimal subset of vectors in $`V`$ that are
_linearly independent_ and _span_ (generate) $`V`$ as _linear combinations._
[[wm:ves][]], [[wp:veb][]], [[wm:veb][]], [[em:veb][]]

The relation of two vector spaces can be expressed by _linear map_ (also
called a _linear mapping, linear transformation, vector space homomorphism_).
They are functions $`V\to{W}`$ between two vector spaces that preserve vector
addition and scalar multiplication. [[wp:ves][]]

If $`V,W`$ are finite-dimensional vector spaces with a defined basis, then
every linear map can be represented by a matrix.  For example, if $`A`$ is a
real $`m\times{n}`$ matrix, then $`\mathbf{x}\mapsto{A\mathbf{x}}`$ describes
a linear map $`\mathbb{R}^n\to\mathbb{R}^m`$ (see [Euclidean space][XXX]).
[[wp:ves][]]

Linear maps $`f:V\to{V}`$ (_endomorphisms_) allow to compare vectors
$`\mathbf{v}`$ with their image under $`f`$, $`f(\mathbf{v})`$.  Any nonzero
vector $`\mathbf{v}`$ satisfying $`λ\mathbf{v}=f(\mathbf{v})`$, where $`λ`$ is
a scalar, is called an _eigenvector_ of $`f`$ with _eigenvalue_ $`λ`$.
[[wp:ves][]]

There are some standard constructions that yield vector spaces related to
given ones.

A nonempty subset $`W`$ of a vector space $`V`$ that is closed under addition
and scalar multiplication (and therefore contains the $`\mathbf{0}`$-vector of
$`V`$ ) is called a _(linear) subspace_ of $`V`$. [[wp:ves][]]

The intersection of all subspaces containing a given set $`S`$ of vectors is
called its _span,_ and it is the smallest subspace of $`V`$ containing $`S`$.
The span is the subspace consisting of all the _linear combinations_ of
elements of $`S`$. [[wp:ves][]]

Linear subspace of dimension 1 and 2 are referred to as a _line_ (or _vector
line_), and a _plane_ respectively.  If $`W`$ is an n-dimensional vector
space, any subspace of dimension $`n − 1`$ is called a _hyperplane._
[[wp:ves][]]

Given any subspace $`W\subseteq{V}`$, the quotient space $`V/W`$ ("$`V`$
modulo $`W`$) is defined as: \
\- the set of all vectors
   $`\mathbf{v}+W=\{\mathbf{v}+\mathbf{w}:\mathbf{w}\in{W}\}`$,
   where $`\mathbf{v}`$ is an arbitrary vector in $`V`$; \
\- the sum of two elements $`\mathbf{v}_1+W`$ and $`\mathbf{v}_2+W`$ is
   $`(\mathbf{v}_1+\mathbf{v}_2)+W`$; \
\- scalar multiplication is given by
   $`a\cdot(\mathbf{v}+W)=(a\cdot\mathbf{v})+W`$. \

The key point in this definition is that $`\mathbf{v}_1+W=\mathbf{v}_2+W`$ if
and only if the difference of $`\mathbf{v}_1`$ and $`\mathbf{v}_2`$ lies in
$`W`$. This way, the quotient space "forgets" information that is contained in
the subspace $`W`$.




[wp:ves]: https://en.wikipedia.org/wiki/Vector_space
[wp:fld]: https://en.wikipedia.org/wiki/Field_(mathematics)
[wp:vec]: https://en.wikipedia.org/wiki/Vector_(mathematics_and_physics)
[wp:geo]: https://en.wikipedia.org/wiki/Geometric_vector
[wp:coo]: https://en.wikipedia.org/wiki/Coordinate_vector
[wp:sca]: https://en.wikipedia.org/wiki/Scalar_(mathematics)
[wp:alf]: https://en.wikipedia.org/wiki/Algebras_over_a_field
[wp:fus]: https://en.wikipedia.org/wiki/Function_spaces
[wp:veb]: https://en.wikipedia.org/wiki/Basis_(linear_algebra)
[wp:lit]: https://en.wikipedia.org/wiki/Linear_map

[wm:ves]: https://mathworld.wolfram.com/VectorSpace.html
[wm:fld]: https://mathworld.wolfram.com/Field.html
[wm:alf]: https://mathworld.wolfram.com/Algebra.html
[wm:fus]: https://mathworld.wolfram.com/FunctionSpace.html
[wm:vec]: https://mathworld.wolfram.com/Vector.html
[wm:sca]: https://mathworld.wolfram.com/Scalar.html
[wm:veb]: https://mathworld.wolfram.com/VectorBasis.html
[wp:lit]: https://mathworld.wolfram.com/LinearTransformation.html

[em:ves]: https://encyclopediaofmath.org/wiki/Vector_space
[em:fld]: https://encyclopediaofmath.org/wiki/Field
[em:vec]: https://encyclopediaofmath.org/wiki/Vector
[em:sca]: https://encyclopediaofmath.org/wiki/Scalar
[em:alf]: https://encyclopediaofmath.org/wiki/Algebra
[em:veb]: https://encyclopediaofmath.org/wiki/Basis
[wp:lit]: https://encyclopediaofmath.org/wiki/Linear_transformation

#### 1.3 Def: Topological Vector Space

A _topological vector space_ (also called a _linear topological space_ and
commonly abbreviated _TVS_)

Topological Vector Space
A topological vector space is a vector space that is also a topological space
with the property that the vector space operations (vector addition and scalar
multiplication) are also continuous functions. [wp:tvs]

Such a topology is called a vector topology and every topological vector space
has a uniform topological structure, allowing a notion of uniform convergence
and completeness.

Ein topologischer Vektorraum ist ein Vektorraum, auf dem neben seiner
algebraischen auch noch eine damit verträgliche topologische Struktur
definiert ist.
https://de.wikipedia.org/wiki/Topologischer_Vektorraum

A vector space such that the operations of vector addition and scalar multiplication are continuous. [[wm:tvs][]]

[wp:tvs]: https://en.wikipedia.org/wiki/Topological_vector_space

[wm:tvs]: https://mathworld.wolfram.com/TopologicalVectorSpace.html

#### 1.4 Def: Metric Space $`(\small{X}, d)`$

Eine Metrik (auch Abstandsfunktion) ist in der Mathematik eine Funktion, die
je zwei Elementen (auch Punkte genannt) einer Menge (auch Raum genannt) einen
nichtnegativen reellen Wert zuordnet. Dieser Wert wird (unter dieser Metrik)
als Abstand der beiden Punkte voneinander bezeichnet. Unter einem metrischen
Raum versteht man eine Menge, auf der eine Metrik definiert ist.
https://de.wikipedia.org/wiki/Metrischer_Raum

A metric space is a set S with a global distance function (the metric g) that,
for every two points x,y in S, gives the distance between them as a
nonnegative real number g(x,y). [wp:mes][]

In mathematical analysis, a metric space M is called complete (or a Cauchy
space) if every Cauchy sequence of points in M has a limit that is also in
M. [wp:cms][]

[wp:mes]: https://mathworld.wolfram.com/MetricSpace.html
[wp:cms]: https://en.wikipedia.org/wiki/Complete_metric_space

[em:mes]: https://encyclopediaofmath.org/wiki/Metric_space

#### 1.5 Def: Locally Convex Space

In functional analysis and related areas of mathematics, locally convex
topological vector spaces (LCTVS) or locally convex spaces are examples of
topological vector spaces (TVS) that generalize normed spaces. [wp:lcs]

Es handelt sich dabei um topologische Vektorräume, in denen jeder Punkt über
„beliebig kleine“ konvexe Umgebungen verfügt.
https://de.wikipedia.org/wiki/Lokalkonvexer_Raum

[wp:lcs]: https://en.wikipedia.org/wiki/Locally_convex_topological_vector_space

#### 1.6 Def: Normed Vector Space $`(\small{X}, \|\cdot\|)`$

In mathematics, a normed vector space or normed space is a vector space over
the real or complex numbers on which a norm is defined. A norm is a
generalization of the intuitive notion of "length" in the physical world. [wp:nvs]

Ein normierter Raum oder normierter Vektorraum ist in der Mathematik ein
Vektorraum, auf dem eine Norm definiert ist. Jeder normierte Raum ist mit der
durch die Norm induzierten Metrik ein metrischer Raum und mit der durch diese
Metrik induzierten Topologie ein topologischer Raum.
https://de.wikipedia.org/wiki/Normierter_Raum

[wp:nvs]: https://en.wikipedia.org/wiki/Normed_vector_space

#### 1.7 Def: Banach Space

a Banach space (pronounced [ˈbanax]) is a complete normed vector space. Thus,
a Banach space is a vector space with a metric that allows the computation of
vector length and distance between vectors and is complete in the sense that a
Cauchy sequence of vectors always converges to a well-defined limit that is
within the space. [wp:bas][]

Ein Banachraum (auch Banach-Raum, Banachscher Raum) ist in der Mathematik ein
vollständiger normierter Vektorraum. Insbesondere sind viele
unendlichdimensionale Funktionenräume Banachräume.
https://de.wikipedia.org/wiki/Banachraum

A Banach space is a complete vector space B with a norm ||·||.  In the
finite-dimensional case, all norms are equivalent. An infinite-dimensional
space can have many different norms. [wm:bas][]

Usually, the notion of Banach space is only used in the infinite dimensional
setting, typically as a vector space of functions. [wm:bas][]

Hilbert spaces with their norm given by the inner product are examples of
Banach spaces. While a Hilbert space is always a Banach space, the converse
need not hold. Therefore, it is possible for a Banach space not to have a
norm given by an inner product. For instance, the supremum norm cannot be
given by an inner product. [wm:bas][]

A complete normed vector space. [em:bas][]

[wp:bas]: https://en.wikipedia.org/wiki/Banach_space
[wm:bas]: https://mathworld.wolfram.com/BanachSpace.html
[em:bas]: https://encyclopediaofmath.org/wiki/Banach_space

#### 1.8 Def: Inner Product Space $`(\small{X}, \langle\cdot,\cdot\rangle)`$

ein reeller oder komplexer Vektorraum, auf dem ein inneres Produkt
(Skalarprodukt) definiert ist, als Prähilbertraum (auch prähilbertscher Raum)
oder Skalarproduktraum (auch Vektorraum mit innerem Produkt, vereinzelt auch
Innenproduktraum) bezeichnet.
https://de.wikipedia.org/wiki/Pr%C3%A4hilbertraum

an inner product space is a
real vector space or a complex vector space with an operation called an inner
product. The inner product of two vectors in the space is a scalar, often
denoted with angle brackets such as in ⟨ a , b ⟩ {\displaystyle \langle
a,b\rangle }. Inner products allow formal definitions of intuitive geometric
notions, such as lengths, angles, and orthogonality (zero inner product) of
vectors. [wp:ips][]

[wp:ips][]
An inner product space is a vector space V over the field F together with an inner product, that is, a map
    ⟨ ⋅ , ⋅ ⟩ : V × V → F {\displaystyle \langle \cdot ,\cdot \rangle :V\times V\to F}
that satisfies the following three properties for all vectors x , y , z ∈ V
{\displaystyle x,y,z\in V} and all scalars a , b ∈ F {\displaystyle a,b\in F}.

Conjugate symmetry: ⟨ x , y ⟩ = ⟨ y , x ⟩ ¯ . {\displaystyle \langle
x,y\rangle ={\overline {\langle y,x\rangle }}.} As a = a ¯ {\textstyle
a={\overline {a}}} if and only if a {\displaystyle a} is real, conjugate
symmetry implies that ⟨ x , x ⟩ {\displaystyle \langle x,x\rangle } is always
a real number. If F is R {\displaystyle \mathbb {R} }, conjugate symmetry is
just symmetry.

Linearity in the first argument:[Note 1] ⟨ a x + b y , z ⟩ = a ⟨ x , z ⟩ + b ⟨
y , z ⟩ . {\displaystyle \langle ax+by,z\rangle =a\langle x,z\rangle +b\langle
y,z\rangle .}

Positive-definiteness: if x {\displaystyle x} is not zero, then ⟨ x , x ⟩ > 0
{\displaystyle \langle x,x\rangle >0} (conjugate symmetry implies that ⟨ x , x
⟩ {\displaystyle \langle x,x\rangle } is real).


An inner product space is a vector space together with an inner product on
it.  Historically, inner product spaces are sometimes referred to as
pre-Hilbert spaces. [wm:ips][]

If the inner product defines a complete metric, then the inner product space
is called a Hilbert space. [wm:ips][]

[wp:ips]: https://en.wikipedia.org/wiki/Inner_product_space
[wm:ips]: https://mathworld.wolfram.com/InnerProductSpace.html
[em:ips]: https://encyclopediaofmath.org/wiki/Pre-Hilbert_space

[Jeremy Kun](https://www.jeremykun.com/2011/07/25/inner-product-spaces-a-primer/)

#### 1.9 Def: Hilbert Space

A Hilbert space is a real or complex inner product space that is also a
complete metric space with respect to the distance function induced by the
inner product. [wp:his][]

The completeness of H is expressed using a form of the Cauchy criterion for
sequences in H: a pre-Hilbert space H is complete if every Cauchy sequence
converges with respect to this norm to an element in the space.  [wp:his][]

A Hilbert space is a vector space H with an inner product <f,g> such that the
norm defined by |f|=sqrt(<f,f>) turns H into a complete metric
space. [wm:his][]

If the metric defined by the norm is not complete, then H is instead known as
an inner product space. [wm:his][]

Hilbert spaces (named after David Hilbert) allow the methods of linear algebra
and calculus to be generalized from (finite-dimensional) Euclidean vector
spaces to spaces that may be infinite-dimensional. [wm:his][]

Ein Vektorraum über dem Körper der reellen oder komplexen Zahlen, versehen mit
einem Skalarprodukt – und damit Winkel- und Längenbegriffen –, der vollständig
bezüglich der vom Skalarprodukt induzierten Norm (des Längenbegriffs) ist. Ein
Hilbertraum ist ein Banachraum, dessen Norm durch ein Skalarprodukt induziert
ist.
https://de.wikipedia.org/wiki/Hilbertraum


A vector space H over the field of complex (or real) numbers, together with a
complex-valued (or real-valued) function (x,y) defined on H×H, with the
following properties:
...
7) H is an infinite-dimensional vector space. [em:his][]

In the definition of a Hilbert space the condition of infinite dimensionality
is often omitted, i.e. a pre-Hilbert space is understood to mean a vector
space over the field of complex (or real) numbers with a scalar product, while
a Hilbert space is the name given to a complete pre-Hilbert space. [em:his][]

[wp:his]: https://en.wikipedia.org/wiki/Hilbert_space
[wm:his]: https://mathworld.wolfram.com/HilbertSpace.html
[ep:his]: https://encyclopediaofmath.org/wiki/Hilbert_space

#### 1.10 Def: Euclidian $`n`$-Space $`\small{\mathbb{R}^n}`$

A _Euclidean vector space_ is a finite-dimensional inner product space over
the real numbers. [wp:eus][]

A _Euclidean space_ is an affine space over the reals such that the associated
vector space is a Euclidean vector space. Euclidean spaces are sometimes
called _Euclidean affine spaces_ to distinguish them from _Euclidean vector
spaces._  If E is a Euclidean space, its associated vector space (Euclidean
vector space) is often denoted E → . {\displaystyle {\overrightarrow {E}}.}
The dimension of a Euclidean space is the dimension of its associated vector
space. [wp:eus][]

_Euclidian $`n`$-space,_ or _Cartesian space_ or simply _$`n`$-space,_
is the space of all $`n`$-tuples of real numbers, $`(x_1,x_2,\ldots,x_n)`$.
The totality of $`n`$-space is commonly denoted $`\mathbb{R}^n`$. [wm:eus][]

There is essentially only one Euclidean space of each dimension; that is, all
Euclidean spaces of the same dimension are isomorphic. Therefore, it is usually
possible to work with a specific Euclidean space, denoted $`\mathbb{E}^n`$,
which can be represented using Cartesian coordinates as the real $`n`$-space
$`\mathbb{R}^n`$ equipped with the standard dot product.  [wp:eus][]

Euclidean space is a finite-dimensional real vector space $`R^n`$ with an
inner product $`\langle{x,y}\rangle`$, $`x,y\in{R}^n`$, which in a suitably
chosen (Cartesian) coordinate system $`x=(x_1,\ldots,x_n)`$ and
$`y=(y_1,\ldots,y_n)`$ is given by the formula
$`(x,y)=\sum_{i=1}^{n}x_{i}y_{i}`$. [em:eus][]

[wp:eus]: https://en.wikipedia.org/wiki/Euclidean_space
[wm:eus]: https://mathworld.wolfram.com/EuclideanSpace.html
[em:eus]: https://encyclopediaofmath.org/wiki/Euclidean_space

#### 1.11 Orthogonality, orthonormality, unit vectors:

XXX

[wp:unv]: https://en.wikipedia.org/wiki/Unit_vector
[wp:org]: https://en.wikipedia.org/wiki/Orthogonality
[wp:orn]: https://en.wikipedia.org/wiki/Orthonormality

[wp:ogb]: https://en.wikipedia.org/wiki/Orthogonal_basis
[wm:ogb]: https://mathworld.wolfram.com/OrthogonalBasis.html
[em:ogb]: https://encyclopediaofmath.org/wiki/Orthogonal_basis

[wp:onb]: https://en.wikipedia.org/wiki/Orthonormal_basis
[wm:onb]: https://mathworld.wolfram.com/OrthonormalBasis.html

[Up](./README.md)
