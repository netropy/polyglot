# [polyglot](https://gitlab.com/netropy/polyglot)/[math](./README.md)

### Math Notes & Exercises

- [Refcard: Permutations, Variations, Combinations](Refcard_Permutations_Variations_Combinations.md)
- [Refcard: Binary Graph and Order Relations](Refcard_Binary_Graph_and_Order_Relations.md)
- [Refcard: 10 Abstract Spaces in Functional Analysis](Refcard_Abstract_Spaces_in_Functional_Analysis.md)
- [Notes: Linear Algebra](Notes_Linear_Algebra_1.md)
- [Notes: Point-Set Topology](Notes_Point_Set_Topology_Basics.md)
- [Notes: Set, Class, Multiset, Family](Notes_Set_Class_Multiset_Family.md)

Exercises, Examples:
- [1 Ex: 3-Point Topologies](ex_1.q.3point_topologies.md)

[Up](../README.md)
