# polyglot/math

### Refcard: Binary Relations, Graph Types, Order Relations

___TOC:___ \
[1.1 Overview](#11-overview) \
[1.2 Graph Types](#12-graph-types) \
[1.3 Order Relations](#13-order-relations) \
[1.4 Properties of Binary Endorelations](#14-properties-of-binary-endorelations) \
[Appendix: Proofs: asymmetric <=> irreflexive, antisymmetric, transitive](#appendix-proofs-asymmetric-irreflexive-antisymmetric-transitive) \
[References](#references)

___Motivation:___ Summarize which
[types of graphs](https://en.wikipedia.org/wiki/Graph_theory#Definitions)
and which
[order relations](https://en.wikipedia.org/wiki/Preorder)
rely on which [properties of pairwise relations (endorelations)][].

#### 1.1 Overview

In math and in programming, _pairwise relations_ are used everywhere: \
(1) to compare values for equivalence or for order, \
(2) to group or classify objects, or \
(3) to express information about connectedness. \
Defining such relations for new data types and implementing them correctly can
be tricky _(*)_.

_(*)_ For illustration: \
\- this refcard already lists _~35 references_ just on the math basics; \
\- the textbook
  [Effective Java (2018)](https://www.pearson.com/us/higher-education/program/Bloch-Effective-Java-3rd-Edition/PGM1763855.html?tab=contents)
  spends _~20 pages_ (items 10, 11, 14) on implementing _equality_ and
  _ordering_ in Java 9; \
\- the book
  [Programming in Scala (2019)](https://www.artima.com/shop/programming_in_scala_4ed)
  describes on _~23 pages_ (chapter 30) four common pitfalls in defining
  _object equality_ in Scala 2 (and Java).

In mathematics, pairwise relations are studied in
[order theory](https://en.wikipedia.org/wiki/Order_theory),
[graph theory](https://en.wikipedia.org/wiki/Graph_theory),
and other fields.

These fields capture and formalize intuitive notions of _equivalence,
ordering, grouping,_ or _connectedness_ as properties of [endorelations][],
which are _binary, homogeneous relations_ over a set $`\small{X}`$ ("on
$`\small{X}`$").  Hence, most graph types (except for
[multigraphs](https://en.wikipedia.org/wiki/Multigraph))
and all order relations are defined as subsets of the
[Cartesian product](https://en.wikipedia.org/wiki/Cartesian_product)
$`\small{X}\!\times\!\small{X}`$.

Programming languages then add their own notions and concepts of
- two- and three-way comparisons for identity, equality, truthy/falsy, and
  order (_==, ===, eq, equals, !=, ?:, <=>, compare_ etc);
- how operators or functions are given, overridden, or synthesized; and
- what consistency requirements must be met.

Examples:
- [C++20's](https://en.cppreference.com/w/cpp/20)
  _...\_comparable\_..._ concepts and the
  ["spaceship" operator<=>](https://en.cppreference.com/w/cpp/language/default_comparisons)
  with result types
  [std::strong\_ordering](https://en.cppreference.com/w/cpp/utility/compare/strong_ordering),
  [std::weak\_ordering](https://en.cppreference.com/w/cpp/utility/compare/weak_ordering), and
  [std::partial\_ordering](https://en.cppreference.com/w/cpp/utility/compare/partial_ordering);
- [Scala's](https://www.scala-lang.org/api/current/index.html)
  traits and
  [scala.math](https://www.scala-lang.org/api/current/scala/math/index.html)
  type-classes:
  [Ordered](https://www.scala-lang.org/api/current/scala/math/Ordered.html),
  [Ordering](https://www.scala-lang.org/api/current/scala/math/Ordering.html),
  [PartiallyOrdered](https://www.scala-lang.org/api/current/scala/math/PartiallyOrdered.html),
  [PartialOrdering](https://www.scala-lang.org/api/current/scala/math/PartialOrdering.html); and
  [Scala 3's](https://docs.scala-lang.org/scala3) support for
  [multiversal equality](https://dotty.epfl.ch/docs/reference/contextual/multiversal-equality.html);
- [Java's](https://openjdk.java.net/)
  interfaces for _total orderings_,
  [java.lang.Comparable](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Comparable.html) and
  [java.util.Comparator](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Comparator.html);
  and the design of _equals_ having _universal equality_ in
  [java.lang.Object](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Object.html).

#### 1.2 Graph Types

| | [transitive][] | [reflexive][] | [irreflexive][] | [connex][] | [semiconnex][] | [symmetric][] | [antisymmetric][] | [asymmetric][] | [transitive incomparability][] |
|:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| [directed (simple) graph (or digraph)][] | | | $`\small{\dagger}`$ |
| [undirected (simple) graph][] | | | $`\small{\dagger}`$ | | | T |
| [dependency][], [tolerance relation][] | | T | | | | T |
| [oriented graph][] | | | T | | | | T |
| [tournament][] | | | T | | T | | T | T |

___Notes:___
- $`\small{\dagger}`$ Definitions of graphs vary, but _simple_ graphs are
  _irreflexive_, as they exclude _loops_ (arrows or edges that directly
  connect nodes with themselves).
- _Simple_ graphs also prohibit _multiple_ arrows or edges between two nodes:
  [multigraphs](https://en.wikipedia.org/wiki/Multigraph)
  are excluded.
- The table only shows graph types that rely on local, _pairwise_ properties
  (why, for instance,
  [directed acyclic graphs (or dags)](https://en.wikipedia.org/wiki/Directed_acyclic_graph)
  are left out).

#### 1.3 Order Relations

| | [transitive][] | [reflexive][] | [irreflexive][] | [connex][] | [semiconnex][] | [symmetric][] | [antisymmetric][] | [asymmetric][] | [transitive incomparability][] |
|:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| [(non-strict) preorder (or quasiorder)][] | T | T |
| [strict preorder][] | T | | T |
| [total preorder][] | T | T | | T | T |
| [equivalence (or partition)][] | T | T | | | | T |
| [equality][] | T | T | | | | T | T |
| [(non-strict) partial order (or poset)][] | T | T | | | | | T |
| [strict partial order (or dag)][] | T | | T | | | | T | T |
| [(non-strict) weak order][] | T | T | | | | | T | | T |
| [strict weak order][] | T | | T | | | | T | T | T |
| [total (or linear) order (or loset)][] | T | T | | T | T | | T | | T |
| [strict total order][] | T | | T | | T | | T | T | T |

___Notes:___
- All order relations require _transitivity_, which conforms with an intuitive
  demand for consistency of an ordering.
- _Total_ orders require _connexity_, which formalizes the notion that any two
  objects are comparable under the relation.  Total orders are also _weak_,
  but
  [vacuously](https://en.wikipedia.org/wiki/Vacuous_truth) so.
- _Partial_ orders allow for _incomparable_ objects, or pairs for which none
  precedes the other under the relation.
- _Weak_ orderings capture the intuitive notion of a _ranking_ or _ordered
  partition_ of a set: the objects are divided into disjoint subsets, together
  with a total order on the subsets.  This is equivalent to demanding
  transitivity of incomparability.
- _Strict_ orderings express _irreflexivity_ of the relation, that is, no
  object precedes or compares to itself.
- Inversely, _non-strict_ orderings require _reflexivity_ of the relation.
  While _strict_ and _non-strict_ are used as mutually exclusives, transitive
  relations exists where neither property holds for _all_ objects.
  perceived as an ordering based on transitivity.
- _Equivalence_ relations characteristically require _symmetry_, which
  captures that objects are equivalent with respect to another.  Naturally,
  every _equivalence_ relation on a set defines a
  [partition](https://en.wikipedia.org/wiki/Partition_of_a_set)
  of this set, and
  [vice versa](https://en.wikipedia.org/wiki/Equivalence_relation#Fundamental_theorem_of_equivalence_relations).
- In contrast, _partial orders_ formalize the notion of _precedence_ and so
  demand _antisymmetry_: two objects cannot precede each other, unless equal.
  Naturally, every _partial ordering_ forms a [dag][]; inversely, the
  [transitive closure](https://en.wikipedia.org/wiki/Transitive_closure)
  of a _dag_ is a partial ordering.
- _Equality_ is a stronger version of _equivalence_ that also entails
  substitutability: $`\small{x = y \:\to\: f(x) = f(y)}`$ for expressions
  $`\small{f}`$ that test for _behaviour_ and not _identity_.
- Note that an _equality_ relation meets both properties, _symmetry_ and
  _antisymmetry_; so, equality is
  [both](https://en.wikipedia.org/wiki/Equivalence_relation#Connections_to_other_relations)
  an _equivalence relation_ and a _partial order_.

#### 1.4 Properties of Binary Endorelations

| $`\small{\forall\: x,\: y,\: z}`$ : | primary form | as implication | as transposition | as normal form |
|:---|:---|:---|:---|:---|
| [transitive][] | $`\small{xRy \:\land\: yRz \:\to\: xRz}`$ | $`\small{xRy \:\to\: (yRz \:\to\: xRz)}`$ | $`\small{\neg\: xRz \:\to\: \neg\: xRy \:\lor\: \neg\: yRz}`$ | $`\small{\neg\: xRy \:\lor\: \neg\: yRz \:\lor\: xRz}`$ |
| [reflexive][] | $`\small{xRx}`$ | $`\small{x = y \:\to\: xRy}`$ | $`\small{\neg\: xRy \:\to\: x \ne y}`$ | $`\small{x \ne y \:\lor\: xRy}`$ |
| [irreflexive][] | $`\small{\neg\: xRx}`$ | $`\small{x = y \:\to\: \neg\: xRy}`$ | $`\small{xRy \:\to\: x \ne y}`$ | $`\small{x \ne y \:\lor\: \neg\: xRy}`$ |
| [connex][] | $`\small{xRy \:\lor\: yRx}`$ | $`\small{\neg\: xRy \:\to\: yRx}`$ |
| [semiconnex][] | $`\small{x \ne y \:\to\: xRy \:\lor\: yRx}`$ | | $`\small{\neg\: xRy \:\land\: \neg\: yRx \:\to\: x = y}`$ | $`\small{xRy \:\lor\: yRx \:\lor\: x = y}`$ |
| [symmetric][] | $`\small{xRy \:\to\: yRx}`$ | | | $`\small{\neg\: xRy \:\lor\: yRx}`$ |
| [antisymmetric][] | $`\small{xRy \:\land\: yRx \:\to\: x = y}`$ | $`\small{x \ne y \:\to\: (\:xRy \:\to\: \neg\: yRx\:)}`$ | $`\small{x \ne y \:\to\: \neg\: xRy \:\lor\: \neg\: yRx}`$ | $`\small{\neg\: xRy \:\lor\: \neg\: yRx \:\lor\: x = y}`$ |
| [asymmetric][] | $`\small{xRy \:\to\: \neg\: yRx}`$ | | | $`\small{\neg\: xRy \:\lor\: \neg\: yRx}`$ |
| [trichotomous][] (exactly one of three) $`^{(a),\:(b)}`$ | $`\small{(xRy \:\oplus\: yRx \:\oplus\: x = y) \land \neg\: (xRy \:\land\: yRx \:\land\: x = y)}`$ |
| [transitive incomparability][] $`^{(c))}`$ | $`\small{x\overline{\overline{R}}y \:\land\: y\overline{\overline{R}}z \:\to\: x\overline{\overline{R}}z }`$ |

___Abbrev:___ \
$`\small{(a)}`$ $`\small{\oplus}`$ = xor \
$`\small{(b)}`$ code: $`\;\small{(xRy \:?\: 1:0)+(yRx \:?\: 1:0)+(x = y \:?\: 1:0) = 1}`$ \
$`\small{(c)}`$ def: $`\;\small{x\overline{\overline{R}}y := \neg\: xRy \:\land\: \neg\: yRx}`$, read: _x and y are [incomparable][] with respect to R_

___Notes:___

- [transitive][]:
  - A _transitive_ relation is _irreflexive_ if and only if it is
    _asymmetric_.
- [reflexive][], [irreflexive][]:
  - _irreflexive_ often called "strict"; _reflexive_ then called
    "non-strict".
  - The _reflexive_ property and the _irreflexive_ property are _mutually
    exclusive_; however, it is possible for a relation to be _neither_.
- [connex][], [semiconnex][]:
  - Ambigous naming: _connex_ also called "complete" or "total" (clashing
    with other meanings); _semiconnex_ sometimes called "total" or "connex".
  - A relation is _connex_ if, and only if, it is _semiconnex_ and _reflexive_.
  - A _connex_ relation cannot be _symmetric_, except for the
    [universal relation][].
  - A relation is _connex_ if, and only if, its [complementary relation][] is
    _asymmetric_.
  - A relation is _semiconnex_ if, and only if, its [complementary relation][]
    is _antisymmetric_.
- [symmetric][], [antisymmetric][], [asymmetric][]
  - Example: $`\small{\geq}`$ is an _antisymmetric_ relation; so is
    $`\small{>}`$, but vacuously (the condition is always false).
  - A relation is _asymmetric_ if and only if it is both _antisymmetric_ and
    _irreflexive_.
  - A relation can be both, _symmetric_ and _antisymmetric_ (for example,
    _equality_); so, these properties are independent of each other.
- [trichotomous][]:
  - A relation is _trichotomous_ if, and only if, it is _asymmetric_ and
    _semiconnex_.

#### Appendix: Proofs: asymmetric <=> irreflexive, antisymmetric, transitive

```
     xRy -> not yRx                         (1) asymmetry
     not xRx                                (2) irreflexivity
     xRy and yRx -> x = y                   (3) antisymmetry
     xRy and yRz -> xRz                     (4) transitivity
```

_Notation:_
- using ascii _not, or, and_ instead of
  [logical operator symbols](https://en.wikipedia.org/wiki/Logical_connective)
- using ascii -> for the
  [(material) conditional](https://en.wikipedia.org/wiki/Material_conditional)
  operator
- using ascii => for logical consequence instead of
  [sequent notation](https://en.wikipedia.org/wiki/Sequent)
- inference rules per
  [wiki1](https://en.wikipedia.org/wiki/Propositional_calculus#Basic_and_derived_argument_forms),
  [wiki2](https://en.wikipedia.org/wiki/List_of_rules_of_inference).

##### Proof: asymmetric => irreflexive

```
                                           show (1) => (2):
     not xRy or not yRx                    (10) (1), normal form
     not xRx or not xRx                    (10) case y = x
  => not xRx                               (11) idempotence or
```

##### Proof: asymmetric => antisymmetric

```
                                           show (1) => (3) by contradiction:
     not (xRy and yRx -> x = y)            (20) (3), negated
  => not (not yRx and yRx -> x = y)        (21) modus ponens (20), (1)
  => not (false -> x = y)                  (22) simplification
  => false                                 (23) simplification
```

##### Proof: asymmetric <= irreflexive and antisymmetric

```
                                           show (2), (3) => (1):
     xRy -> x != y                         (30) (2), transposition
     x != y -> not xRy or not yRx          (31) (3), transposition
  => xRy -> not xRy or not yRx             (32) transitivity of ->, (30), (31)
  => not xRy or not xRy or not yRx         (33) material implication
  => not xRy or not yRx                    (34) idempotence or
  => xRy -> not yRx                        (35) material implication
```

##### Proof: asymmetric <= irreflexive and transitive

```
                                           show (2), (4) => (1):
     not xRz -> not xRy or not yRz         (40) (4), transposition
  => not xRx -> not xRy or not yRx         (41) case z = x
  => not xRy or not yRx                    (42) modus ponens (41), (2)
  => xRy -> not yRx                        (43) material implication
```

#### References

All links in this document to articles on
[Wikipedia](https://en.wikipedia.org),
except for references to language documentation of
[C++](https://en.cppreference.com),
[Scala](https://www.scala-lang.org), and
[Java](https://docs.oracle.com/en/java/javase).

[endorelations]: https://en.wikipedia.org/wiki/Binary_relation#Homogeneous_relation
[properties of pairwise relations (endorelations)]: https://en.wikipedia.org/wiki/Binary_relation#Properties
[complementary relation]: https://en.wikipedia.org/wiki/Complement_(set_theory)
[universal relation]: https://en.wikipedia.org/wiki/Binary_relation#Particular_homogeneous_relations
[directed (simple) graph (or digraph)]: https://en.wikipedia.org/wiki/Directed_graph
[undirected (simple) graph]: https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)#Undirected_graph
[dag]: https://en.wikipedia.org/wiki/Directed_acyclic_graph
[dependency]: https://en.wikipedia.org/wiki/Dependency_relation
[tolerance relation]: https://en.wikipedia.org/wiki/Tolerance_relation
[oriented graph]: https://en.wikipedia.org/wiki/Orientation_(graph_theory)
[tournament]: https://en.wikipedia.org/wiki/Tournament_(graph_theory)
[(non-strict) preorder (or quasiorder)]: https://en.wikipedia.org/wiki/Preorder
[strict preorder]: https://en.wikipedia.org/wiki/Preorder#Formal_definition
[total preorder]: https://en.wikipedia.org/wiki/Preorder#Related_definitions
[equivalence (or partition)]: https://en.wikipedia.org/wiki/Equivalence_relation
[equality]: https://en.wikipedia.org/wiki/Equality_(mathematics)
[(non-strict) partial order (or poset)]: https://en.wikipedia.org/wiki/Partial_order
[strict partial order (or dag)]: https://en.wikipedia.org/wiki/Partially_ordered_set#Strict_and_non-strict_partial_orders
[(non-strict) weak order]: https://en.wikipedia.org/wiki/Weak_ordering
[strict weak order]: https://en.wikipedia.org/wiki/Weak_ordering#Strict_weak_orderings
[total (or linear) order (or loset)]: https://en.wikipedia.org/wiki/Total_order
[strict total order]: https://en.wikipedia.org/wiki/Total_order#Strict_total_order
[transitive]: https://en.wikipedia.org/wiki/Transitive_relation
[reflexive]: https://en.wikipedia.org/wiki/Reflexive_relation
[irreflexive]: https://en.wikipedia.org/wiki/Irreflexive_relation
[connex]: https://en.wikipedia.org/wiki/Connex_relation
[semiconnex]: https://en.wikipedia.org/wiki/Connex_relation
[trichotomous]: https://en.wikipedia.org/wiki/Trichotomy_(mathematics)
[symmetric]: https://en.wikipedia.org/wiki/Symmetric_relation
[antisymmetric]: https://en.wikipedia.org/wiki/Antisymmetric_relation
[asymmetric]: https://en.wikipedia.org/wiki/Asymmetric_relation
[transitive incomparability]: https://en.wikipedia.org/wiki/Transitivity_of_incomparability
[incomparable]: https://en.wikipedia.org/wiki/Comparability

[Up](./README.md)
