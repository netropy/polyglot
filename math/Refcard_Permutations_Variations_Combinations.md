# polyglot/math

### Refcard: Permutations, Variations, Combinations

___TOC:___ \
[Overview: Classifying Ways of Grouping and Arranging of Objects](#overview-classifying-ways-of-grouping-and-arranging-of-objects) \
[Counting Permutations, Variations, Combinations](#counting-permutations-variations-combinations) \
[References](#references)

___Motivation:___ Memorize as _3x2_ table the formulas for counting these
basic ways of arranging and picking elements: { Permutations, Variations,
Combinations } X { with, without } repetition.

#### Overview: Classifying Ways of Grouping and Arranging of Objects

Patterns of arranging and grouping of objects are studied as part of
[Enumerative Combinatorics][enumerative combinatorics].  Knowing "how to
count" permutations, combinations or partitions has many applications and is
especially important for computing frequency-based probabilities and
statistics.

##### 2 x 2 Classification: {order?} X {repetition?}

Many tutorials, for example [introduction to probability - counting (2014)][],
begin with the criterion of "whether order matters", which serves as
distinction between [permutations][permutation] and
[combinations][combination].  One then further distinguishes between "w/ and
w/o repetition" (also called "replacement").

One shortcoming of this _2x2_ approach is that yet another criterion is then
introduced ad-hoc for permutations: whether also a selection of elements takes
place (sampling).  This introduces _k-permutations_, which are quickly found
to resemble _combinations._  Huh?

The answer is: _k_-permutations_ are _not_ combinations, they just happen to
share with them an _independent_ property -- the sampling of elements.

##### 3 x 2 Classification: {order?, selection?} X {repetition?}

This approach introduces _Variations_ as a group between Permutations and
Combinations by "what they do": _arrange_ vs _(arrange & pick)_ vs _pick._

The _3x2_ classification seems much more common in Non-English language texts.
For example, compare: \
\- the
[German Wikipedia article: Variations](https://de.wikipedia.org/wiki/Variation_(Kombinatorik))
or this
[German-language Tutorial](https://www.studienkreis.de/mathematik/permutation-variation-kombination) \
\- against the _lack_ of an English Wikipedia entry on _Variations:_

![English Wikipedia: Variations](en_wikipedia_variations.small.png)

The _3x2_ classification scheme yields a good compromise: simple enough to
memorize while pointing out two independent criteria and covering more
important use cases.

##### 4 x 3 Classification: {balls/boxes marked?} X {rules on placement?}

The so-called [Twelvefold Way][twelvefold way] is an interesting and
comprehensive classification based on equivalence classes of functions between
two finite sets, which allows for counting permutations, combinations and
partitions.

The criteria include whether functions are injective or surjective or neither,
and what granularities of equality are demanded, whether up to permutations in
either set.

This results in a _4x3_ table covering many use cases (and test questions).
Yet, this aproach requires deep reading and extensive memorization.

##### 2 x 10 and 6 x 5 Classifications

Another systematic grouping of combinatorial structures is called
[The twentyfold way][twentyfold way]. This approach lists "models for
distribution of _n_ structures among _x_ recipients" in form of a _2x10_
table.

Yet another proposal expands the _4x3 Twelvefold Way_ into a
[_6x5_ "Thirtyfold Way"][thirtyfold way (2007)].

These approaches might be instructive but appear as too detailed for
memorization.

#### Counting Permutations, Variations, Combinations

The _3x2_ classification scheme seems practical and important enough to "know
by heart".

| | ___Permutations___ | ___Variations___ | ___Combinations___ |
|:--|:--:|:--:|:--:|
| | = _arrange_ | = _arrange & pick_ | = _pick_ |
| _ordered:_ | yes | yes | |
| _sampled:_ | | yes | yes |
| | | | |
| **without repetition:** | **Permutation** | **_k_-Permutation (Variation w/o repetition)** | **_k_-Combination** |
| def: | a bijection from an _n_-element set _S_ to itself | a permutation of a _k_-element subset of an _n_-set _S_ | a _k_-element subset of an _n_-set _S_ |
| count: | $`P_n:={P}^n_n=P(n,n)=n!`$ | $`{V}^n_k:={P}^n_k:=P(n,k):=\frac{n!}{(n-k)!}`$ | $`{C}^n_k:=C(n,k):=\frac{P(n,k)}{P(k,k)}={n \choose k}=\frac{n!}{k!(n-k)!}`$ |
| comments: | see (a), (b), (c), (d), (e), (f), (g) | see (a), (h), (i), (j) | see (a), (k), (l), (m), (n) |
| examples: | arrangements of all elements of a set; strict total orders on a set; anagrams of a word whose letters are all different | _k_-permutations of _S_; ordered sampling without replacement; words of length _k_ of different letters over an alphabet _S_; injective functions from a _k_-set to _S_ | _k_-subsets of _S_; unordered sampling without replacement; lotteries; injective functions from a _k_-set _K_ to _S_ up to a permutation of _K_ |
| | | | |
| **with repetition:** | **Multiset Permutation** | **_k_-Sequence, Tuple (Variation w/ repetition)** | **_k_-Multicombination** |
| def: | a permutation of a given, _k_-element multiset _M_ from an _n_-set _S_ | a sequence of length $`k`$ of an _n_-set _S_ | a _k_-element multisubset from an _n_-set _S_ |
| count: | $`P_{m_1,\ldots,m_n}:={k \choose {m_1,\ldots,m_n}}=\frac{k!}{{m_1}!{m_2}!\cdots{m_n}!}`$ | $`\overline{V}^n_k:=n^k`$ | $`\overline{C}^n_k:=\big({n \choose k}\big)={n+k-1 \choose k}=C^{n+k-1}_{k}=\frac{(n+k-1)!}{k!(n-1)!}`$ |
| comments: | where $`(\small{\sum_{i=1}^n m_i=k})`$  are _M_'s multiplicities; see (a), (o), (p), (q) | see (a), (r), (s), (t) | see (a), (o), (u), (v), (w), (x), (y) |
| examples: | arrangements of all elements of a multiset; anagrams of a word with repeated letters; ways to select under a number distribution of labels; partitioning _k_ objects into _n_ bins with capacities $`m_{i=1\ldots n}`$ | _k_-sequences in _S_; ordered sampling with replacement; words of length _k_ over an alphabet _S_; functions from a _k_-set to _S_ | _k_-multisubsets of _S_; unordered sampling with replacement; functions from a _k_-set _K_ to _S_ up to a permutation of _K_ |


##### Comments:

(a) An "_n_-set _S_" or a "_k_-element subset of _S_" means, respectively: \
$`\qquad`$ a [set][] _S_ with cardinality $`n=|S|`$ \
$`\qquad`$ a [subset][] $`S'\subseteq S`$ with cardinality $`k=|S'|`$

(b) A [bijection][] or _one-to-one correspondence_ between two sets pairs each
element of either set with exactly one element of the other set.

(c) As an edge case of the [factorial function][factorial], the _empty set_
$`\emptyset`$ has exactly one permutation: \
$`\qquad P_0=0!=1`$

(d) [Permutations][permutation] are customarily denoted with lowercase
[Greek letters][greek alphabet], such as α, β, γ or σ, τ, ρ, π.

(e) The _product_ of two permutations σ and τ is defined as
[function composition][]: \
$`\qquad στ = (στ)(i) = σ(τ(i))`$

(f) The collection of all [permutations][permutation] of a set forms a
[group][], called the [symmetric group][] $`S_n`$ of the set, with
[function composition][] as the group operation.

(g) Several [notations][permutation notations] exist to write-down or define
permutations, such as _two-line notation, one-line notation,_ and _cycle
notation._

(h) A [_k_-permutation][k-permutation] is sometimes also called a
_k-permutation of n, variation, sequence without repetition,_ or an
_arrangement._  Multiple denotations exist, among them: \
$`\qquad P(n,k),{P}^n_k,{V}^n_k,{A}^k_n`$

(i) The number of _k_-permutations $`{P}^n_k`$ can also be stated as the
[_k_-th falling factorial power][falling and rising factorials]
$`n^{\underline{k}}`$ (note the underbar) with _k_ factors: \
$`\qquad n^{\underline{k}}:=(n)(n-1)(n-2)\cdots(n-k+1)=\prod_{i=1}^{k}(n-i+1)`$ \
which can be restated as \
$`\qquad n^{\underline{k}}=\frac{n^{\underline{k}}(n-k)!}{(n-k)!}=\frac{n!}{(n-k)!}=P(n,k)`$

(j) These edge cases for _k_-permutations are sensible: \
$`\qquad P(n,0)=\frac{n!}{(n-0)!}=1\quad`$ and
$`\quad P(n,n)=\frac{n!}{(n-n)!}=n!=P^n\quad`$ for $`n >= 0`$ \
and for $`k > n`$: \
$`\qquad P(n,k):=0`$

(k) Multiple denotations exist for [_k_-combinations][combination], among
them: \
$`\qquad C(n,k),{n \choose k},{C}^n_k,{C}^k_n`$

(l) The number of _k_-combinations is given by the [binomial coefficient][],
read as "_n choose k_": \
 $`\qquad {n \choose k}:=\frac{n!}{k!(n-k)!}`$

(m) It is easy to see how the number of _k_-combinations is given by the
binomial coefficient, when counting the involved permutations: the number of
ways to arrange _k_ of _n_ objects $`P(n, k)`$ reduced by the arrangements
having the same subset $`P(k,k)`$: \
$`\qquad C(n,k):=\frac{P(n,k)}{P(k,k)}=\frac{n!}{k!(n-k)!}={n \choose k}`$ \
Or expressed as [falling factorial power][falling and rising factorials]
$`n^{\underline{k}}`$ (note the underbar): \
$`\qquad C(n,k):=\frac{P(n,k)}{P(k,k)}=\frac{n^{\underline{k}}}{k!}=\frac{n(n-1)\cdots(n-k+1)}{k!}`$

(n) These edge cases for _k_-combinations and the binomial coefficient are
sensible: \
$`\qquad C(n,0)={n \choose 0}=\frac{n!}{0!(n-0)!}=1\quad`$ and
$`\quad C(n,n)={n \choose n}=\frac{n!}{n!(n-n)!}=1\quad`$ for $`n >= 0`$ \
and for $`k > n`$: \
$`\qquad C(n,k)={n \choose k}:=0`$

(o) [Multisets][multiset] allow for multiple instances of elements from a
set _S,_ as encoded by an associated function $`m: S \rightarrow \mathbb{N}`$
(or an equivalent [indexed family][]). \
A "_k_-multiset _M_" or "_k_-element multiset _M_" means: \
$`\qquad`$ a [multiset, mset, or bag][multiset] _M=(S,m)_ from a finite
_n_-set _S_ \
$`\qquad\qquad`$ with multiplicities (repetition numbers)
$`m_{i=1 \ldots n}=m_M(s_i)`$ with $`\{s_i\}=S`$  \
$`\qquad\qquad`$ and cardinality $`k=\sum_{i=1}^n m_i=|M|`$ \
An "_l_-element multisubset of _M_" means, \
$`\qquad`$ a
[multisubset, msubset, submultiset, or submset][multiset basics (2014)]
$`M'\subseteq M`$ \
$`\qquad\qquad`$ with multiplicities $`m_{M'}(s_i) \le m_M(s_i)`$ with
$`\{s_i\}=S`$  \
$`\qquad\qquad`$ and cardinality $`l=\sum_{i=1}^n m_{M'}(s_i)=|M'|`$

(p) The number of multiset permutations is given by the
[multinomial coefficient][],
which can be [read as][multinomial theorem (2016)]
"_n_ over _k\_1_, _k\_2_ through _k\_m_" or as
"_n_ choose _k\_1_, choose _k\_2_, choose _k\_m_": \
$`\qquad {n \choose {k_1,\ldots,k_m}}:=\frac{n!}{{k_1}!{k_2}!\cdots{k_m}!}\quad`$
where $`\quad\sum_{i=1}^m k_i=n`$ \
The multinomial coefficient can also be expressed as a product of
[binomial coefficients][binomial coefficient]: \
$`\qquad {n \choose {k_1,\ldots,k_m}}={k_1 \choose k_1}{k_1+k_2 \choose k_2}\cdots{k_1+k_2+\cdots+k_m \choose k_m}=\frac{(\sum_{i=1}^m k_i)!}{\prod_{i=1}^m (k_{i}!)}`$

(q) It is easy to see how the multiset permutation is given by the multinomial
coefficient: each multiplicity $`m_i`$ results in $`P_{m_i}`$ permutations
that "do not make a difference"; so, we reduce the total number $`P_n`$ of
permutations by their product: \
$`\qquad P_{m_1,\ldots,m_n}:=\frac{P_n}{P_{m_1}\cdots{P_{m_n}}}=\frac{k!}{{m_1}!{m_2}!\cdots{m_n}!}={k\choose{m_1,\ldots,m_n}}`$

(r) A [sequence][sequence] in a [set][] _S_ is an enumerated collection of
elements of _S_ in which repetitions are allowed and order matters: \
\- The elements are indexed by natural numbers, encoding their position. \
\- An [indexed family][] generalizes this notion to arbitrary index sets. \
\- A _k_-sequence has _k_ elements or a length of _k._ \
\- Sequences can be infinite and may be defined recursively.

(s) A [tuple][tuple] is a finite [sequence][] of objects, so, repetitions are
allowed and order matters.  Tuples are used when grouping is important while
indexing is not: \
\- An _n_-tuple has _n_ elements (or an [arity][] of _n_). \
\- The (unique) _0_-tuple is denoted as _()_ and called the _empty tuple._ \
\- Tuples are the elements of [Cartesian product][cartesian product] sets. \
\- Tuples can be defined as functions, nested ordered pairs et al.

(t) It is easy to see how the number of _k_-sequences in _S_ grows
exponentially in _k,_ given that they form the
[_k_-ary Cartesian power][cartesian power] set $`S^k`$.

(u) A [_k_-multicombination][k-multicombination] $`\overline{C}^n_k`$ (note
the overbar) is sometimes also called a _k-multiset, k-combination with
repetition,_ or _k-selection._

(v) The number of _k_-multicombinations $`\overline{C}^n_k`$ is given by the
[multiset coefficient][], read as "_n multichoose k_": \
$`\qquad \big({n \choose k}\big):={n+k-1 \choose k}=\frac{(n+k-1)!}{k!(n-1)!}`$

(w) This is easy to see since drawing a _k_-element _multisubset_ instead of a
subset allows for _k-1_ extra repetitions of elements: \
$`\qquad \overline{C}^n_k=C^{n+k-1}_{k}={n+k-1 \choose k}=\big({n \choose k}\big)`$

(x) This formula can also be stated in terms of the
[_k_-th rising factorial power][falling and rising factorials]
$`n^{\overline{k}}`$ (note the overbar) with _k_ factors: \
$`\qquad n^{\overline{k}}:=(n)(n+1)(n+2)\cdots(n+k-1)=\prod_{i=1}^{k}(n+i-1)`$ \
which results in \
$`\qquad \big({n \choose k}\big)=\frac{(n+k-1)!}{k!(n-1)!}=\frac{n^{\overline{k}}}{k!}=\overline{C}^n_k`$

(y) This edge case for _k_-multicombinations and the multiset coefficient is
sensible: \
$`\qquad \overline{C}^n_0=\big({n \choose 0}\big)=\frac{(n-1)!}{0!(n-1)!}=1`$

#### References

All references above to [Wikipedia](https://en.wikipedia.org) except for \
[introduction to probability - counting (2014)][] \
[multinomial theorem (2016)][] \
[multiset basics (2014)][] \
[thirtyfold way (2007)][]

[introduction to probability - counting (2014)]: https://www.probabilitycourse.com/chapter2/2_1_0_counting.php
[multinomial theorem (2016)]: https://www.youtube.com/watch?v=juGgfHsO-xM
[multiset basics (2014)]: https://msme.us/2014-1-2.pdf
[thirtyfold way (2007)]: https://arxiv.org/abs/math/0606404

[arity]: https://en.wikipedia.org/wiki/Arity
[bijection]: https://en.wikipedia.org/wiki/Bijection
[binomial coefficient]: https://en.wikipedia.org/wiki/Binomial_coefficient
[cartesian power]: https://en.wikipedia.org/wiki/Cartesian_product#n-ary_Cartesian_power
[cartesian product]: https://en.wikipedia.org/wiki/Cartesian_product
[combination]: https://en.wikipedia.org/wiki/Combination
[enumerative combinatorics]: https://en.wikipedia.org/wiki/Enumerative_combinatorics
[factorial]: https://en.wikipedia.org/wiki/Factorial
[falling and rising factorials]: https://en.wikipedia.org/wiki/Falling_and_rising_factorials
[function composition]: https://en.wikipedia.org/wiki/Composition_of_functions
[greek alphabet]: https://en.wikipedia.org/wiki/Greek_alphabet
[group]: https://en.wikipedia.org/wiki/Group_(mathematics)
[identity function]: https://en.wikipedia.org/wiki/Identity_function
[indexed family]: https://en.wikipedia.org/wiki/Indexed_family
[k-multicombination]: https://en.wikipedia.org/wiki/Combination#Number_of_k-combinations_for_all_k
[k-permutation]: https://en.wikipedia.org/wiki/Permutation#k-permutations_of_n
[multinomial coefficient]: https://en.wikipedia.org/wiki/Multinomial_coefficient
[multiset coefficient]: https://en.wikipedia.org/wiki/Multiset#Counting_multisets
[multiset]: https://en.wikipedia.org/wiki/Multiset
[permutation notations]: https://en.wikipedia.org/wiki/Permutation#Notations
[permutation]: https://en.wikipedia.org/wiki/Permutation
[sequence]: https://en.wikipedia.org/wiki/Sequence
[set]: https://en.wikipedia.org/wiki/Set_(mathematics)
[subset]: https://en.wikipedia.org/wiki/Subset
[symmetric group]: https://en.wikipedia.org/wiki/Symmetric_group
[tuple]: https://en.wikipedia.org/wiki/Tuple
[twentyfold way]: https://en.wikipedia.org/wiki/Twelvefold_way#The_twentyfold_way

#### Excercises

1) Find out whether $`\overline{C}^n_n=\big({n \choose n}\big)=n^n=\overline{V}^n_n`$

[Up](./README.md)
