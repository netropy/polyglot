# polyglot/rust

### Rust Experiments, Exercises, Refcards, Notes

- [Cargo+Rust basics](basics/README.md) - sample project with configs, notes, experiments
- [Structs and Tuple Structs](experiments/src/structs_tuple_structs.rs) - code experiments, notes
- [Hash, Eq, PartialEq, Ord, PartialOrd](experiments/src/hash\_eq\_ord.rs) - coding exercise, notes
- [Summary: Traits in Rust](Summary_traits.md) - notes
- [Rust terminology not calling language features what they are](Notes_unclear_Rust_terminology.md) - notes

[Up](../README.md)
