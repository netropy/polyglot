#![allow(unused)]

// https://google.github.io/comprehensive-rust/basic-syntax/functions-interlude.html
// no function overloading, no default arguments, only generics
// https://doc.rust-lang.org/book/ch19-05-advanced-functions-and-closures.html

// can have dynamic dispatch via function pointers
// function pointers
// https://doc.rust-lang.org/beta/reference/types/function-pointer.html
// https://doc.rust-lang.org/beta/reference/types/function-item.html

// https://web.mit.edu/rust-lang_v1.25/arch/amd64_ubuntu1404/share/doc/rust/html/book/second-edition/ch05-03-method-syntax.html
//
// Rust has a feature called automatic referencing and dereferencing.
// Calling methods is one of the few places in Rust that has this behavior.
// When you call a method with object.something(), Rust automatically adds
// in &, &mut, or * so object matches the signature of the method.
// In other words, the following are the same:
//   p1.distance(&p2);
//   (&p1).distance(&p2);

// Check: support for member/method pointers in Rust?
