#![allow(unused)]

use rand::random;
use std::cell::Cell;

// ======================================================================
// Summary, Code Examples: Static Items & Lifetime
//
// XXX TODO summarize
//
// + Lifetime Elision:
//   + compiler can infer lifetime annotation for common patterns
//   + no need for <'a>(&'a) annotation for input + pass-through parameters
//
// + Reference Lifetime Coercion:
//   + can return reference with shorter reference lifetime of other argument
//
// + Borrow Checker may not flag "does not live long enough" without a deref
//
// + can require arguments to have:
//   + a Static Reference Lifetime (&'static T): pointee is static
//   + a Static Trait Bound <T: 'static>: argument's borrowed data is static
//
// When are values/references STATIC, stored in read-only memory?
// (references to) local VARIABLES (~lvalues) are NON-STATIC
//
// https://doc.rust-lang.org/reference/const_eval.html
//
// https://doc.rust-lang.org/reference/items/constant-items.html
// https://doc.rust-lang.org/reference/items/static-items.html
// https://doc.rust-lang.org/std/keyword.const.html
// https://doc.rust-lang.org/std/keyword.static.html
//
// https://doc.rust-lang.org/rust-by-example/scope/lifetime.html
// https://doc.rust-lang.org/rust-by-example/scope/lifetime/static_lifetime.html
// https://doc.rust-lang.org/rust-by-example/custom_types/constants.html
// https://doc.rust-lang.org/rust-by-example/scope/lifetime/lifetime_coercion.html
// https://doc.rust-lang.org/rust-by-example/scope/lifetime/explicit.html
// https://doc.rust-lang.org/rust-by-example/scope/lifetime/elision.html
//
// https://doc.rust-lang.org/book/ch03-01-variables-and-mutability.html
//
// ======================================================================

// ----------------------------------------------------------------------
// Static Reference Lifetime vs Static Trait Bound
//
// + can require arguments to have:
//   + a static reference lifetime: pointee is static (including constant)
//   + a static trait bound: argument owns its data or any borrowed is static
// ----------------------------------------------------------------------

// ensures the passed reference has static lifetime
//
// means:
// - the reference is valid for the remaining duration of the program
// - the pointed data exists for the remainder of the program execution
//
fn ensure_static_reference_lifetime<T: ?Sized>(t: &'static T) -> &T { t }

// ensures the argument type meets the static trait bound
//
// means:
// - all the argument's borrowed data are static
// - the argument type does not contain references with non-static lifetime
// - any receiver may hold on to the argument for as long as it wants
//
fn ensure_static_trait_bound<T: 'static>(t: T) -> T { t }

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_references_to_local_variables_are_never_static() {

        // references to local VARIABLES (~lvalues) are NON-STATIC
        let i = 1;
        let mut mi = 1;
        let ri = &i;
        let rmi = &mut mi;
        // error: `i`, `mi` does not live long enough
        //        borrowed value does not live long enough
        //    ensure_static_reference_lifetime(&i);
        //    ensure_static_reference_lifetime(&mi);
        //        argument requires that borrow lasts for `'static`
        //    ensure_static_reference_lifetime(ri);
        //    ensure_static_reference_lifetime(rmi);
    }

    #[test]
    fn test_references_to_constant_values_are_static() {

        // Note: constant expressions <=> _can_ be evaluated at compile time.
        // In let statements, they _may_ be evaluated at compile time.

        // references to CONSTANT VALUES (~rvalues) are STATIC
        let rci: &i32 = &3;
        let rca: &[i32; 2] = &[1; 2];
        let rcs: &str = &"string literal";
        ensure_static_reference_lifetime(rci);
        ensure_static_reference_lifetime(rca);
        ensure_static_reference_lifetime(rcs);

        // make explicit: reference states STATIC lifetime of CONSTANT VALUE
        let rci: &'static i32 = &3;
        let rca: &'static [i32; 2] = &[1; 2];
        let rcs: &'static str = &"string literal";
        ensure_static_reference_lifetime(rci);
        ensure_static_reference_lifetime(rca);
        ensure_static_reference_lifetime(rcs);
    }

    #[test]
    fn test_references_to_non_constant_values_are_never_static() {

        // references to NON-CONSTANT VALUES (~rvalues) are NON-STATIC
        // error: temporary value dropped while borrowed
        //        creates a temporary value which is freed while still in use
        //     let rs: &String = &"string literal".to_string();
        //     let rb: &Box<i32> = &Box::new(1);
        //     let rv: &Vec<i32> = &vec![1, 2];
        //        argument requires that borrow lasts for `'static`
        //     ensure_static_reference_lifetime(rs);
        //     ensure_static_reference_lifetime(rb);
        //     ensure_static_reference_lifetime(rv);

        // NON-CONSTANT VALUES (~rvalues) are NON-STATIC
        //
        // error: temporary value dropped while borrowed
        //        type annotation requires that borrow lasts for `'static`
        //    let rb: &'static Box<i32> = &Box::new(1);
        //    let rv: &'static Vec<i32> = &vec![1, 2];
        // error: temporary value dropped while borrowed
        //        argument requires that borrow lasts for `'static`
        //    ensure_static_reference_lifetime(&Box::new(1));
        //    ensure_static_reference_lifetime(&vec![1, 2]);
    }

    #[test]
    fn test_references_to_constant_items_are_static() {

        // Note: constant expressions <=> _can_ be evaluated at compile time.
        // In const contexts, they _are_ always evaluated at compile time.

        // references to CONSTANTS ITEMS (declared constants) are STATIC
        const CI: i32 = 3;
        const CA: [i32; 2] = [1; 2];
        const RCS: &str = &"string literal";  // str itself is unsized
        ensure_static_reference_lifetime(&CI);
        ensure_static_reference_lifetime(&CA);
        ensure_static_reference_lifetime(RCS);

        // make explicit: reference states STATIC lifetime of CONSTANT
        let rci: &'static i32 = &CI;
        let rca: &'static [i32; 2] = &CA;
        let rcs: &'static str = RCS;
        ensure_static_reference_lifetime(rci);
        ensure_static_reference_lifetime(rca);
        ensure_static_reference_lifetime(rcs);

        // compile-time CONSTANTS CANNOT allocate from HEAP
        //
        // error: cannot call non-const fn `<...>::to_string` in constants
        //    const CS: String = "string literal".to_string();
        // error: cannot call non-const fn `Box::<i32>::new` in constants
        //    const CB: Box<i32> = Box::new(1);
        // error: allocations are not allowed in constants
        //    const CV: Vec<i32> = vec![1, 2];
    }

    #[test]
    fn test_declared_statics_are_safe_if_constant() {

        // CONSTANT STATICS are static and their use is safe
        static S: i32 = 1;
        S;
        ensure_static_reference_lifetime(&S);
    }

    #[test]
    fn test_declared_statics_are_unsafe_if_mutable() {

        // MUTABLE STATICS are static and their use is UNSAFE
        static mut MS: i32 = 1;
        // error: use of mutable static is unsafe
        //   MS;
        //   ensure_static_reference_lifetime(&MS);
        unsafe {
            MS;
            ensure_static_reference_lifetime(&MS);
        }
    }

    #[test]
    fn test_static_trait_bound() {

        // XXX
        const I: i32 = 1;
        //let i = 1;
        let ti = (&I,);
        // error: `i` does not live long enough
        ensure_static_trait_bound(ti);
        //        argument requires that borrow lasts for `'static`

        // ------------------------------------------------------------

        // pass [non-]static, [non-]mutable i32
        let i = 1;
        let mut mi = 1;
        static SI: i32 = 1;
        static mut MSI: i32 = 1;
        ensure_static_trait_bound(1);  // copies
        ensure_static_trait_bound(i);
        ensure_static_trait_bound(mi);
        ensure_static_trait_bound(SI);
        unsafe {
            ensure_static_trait_bound(MSI);
        }

        // pass [non-]static, [non-]mutable &i32
        ensure_static_trait_bound(&1);  // constant value -> static
        // error: `i` does not live long enough
        //    ensure_static_trait_bound(&i);
        // error: temporary value dropped while borrowed
        //        argument requires that borrow lasts for `'static`
        //    ensure_static_trait_bound(&mut 1);
        // error: `i` does not live long enough
        //    ensure_static_trait_bound(&mut mi);
        ensure_static_trait_bound(&SI);
        unsafe {
            ensure_static_trait_bound(&MSI);
        }
        // => no difference: static trait bound vs static reference lifetime

        // pass [non-]static, [non-]mutable Box
        let b = Box::new(1);
        let mut mb = Box::new(1);
        ensure_static_trait_bound(Box::new(1));  // moves
        ensure_static_trait_bound(b);
        ensure_static_trait_bound(mb);

        // pass [non-]static, [non-]mutable &Box
        let b = Box::new(1);
        let mut mb = Box::new(1);
        // error: temporary value dropped while borrowed
        //        argument requires that borrow lasts for `'static`
        //    ensure_static_trait_bound(&Box::new(1));  // non-constant
        //    ensure_static_trait_bound(&mut Box::new(1));  // mutable
        // error: `i` does not live long enough
        //    ensure_static_trait_bound(&b);
        //    ensure_static_trait_bound(&mut mb);
        // error: cannot call non-const fn `Box::<i32>::new` in statics
        //    static SB: Box<i32> = Box::new(1);
        //    static mut SMB: Box<i32> = Box::new(1);
        //    ensure_static_trait_bound(&SB);
        //    unsafe {
        //        ensure_static_trait_bound(&SMB);
        //    }
        // => no difference: static trait bound vs static reference lifetime

        // error: cannot call non-const fn `...::to_string` in statics
        // error: cannot call non-const fn `...::into` in statics
        //    static SB: String = "a".to_string();
        //    static mut SMB: String = "a".into();

        // stack-allocated-content empty arrays, slices
        let a0: [(); 0] = [];          // array expression comma-separated list
        let a1: [i32; 0] = [];
        let a2: [i32; 0] = [1; 0];     // repeat expression with length operand
        // XXX where is  &[1; 0] allocated? static?

    }
}
