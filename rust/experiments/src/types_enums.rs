#![allow(unused)]

use core::fmt::Debug;
use core::hash::Hash;
use static_assertions::*;

// ======================================================================
// Summary, Code Examples: Features/Limitations of Enum Types
//
// + can have "associated items" (members)
//     0..*  variants with immutable, sized values
//           or with [im-]mutable reference type
//     0..*  functions/methods defined in ...
//     0..*  impl blocks which may implement ...
//     0..*  traits
// + variants can be of form:
//     <Variant>,                        unit-type-like values
//     <Variant>(<type>, ..)             tuple-type-like values, "tuple variant"
//     <Variant> { <field>: <type, .. }  struct-type-like values
//
// - CANNOT access variant's values as fields
// + can [partially] destructure/pattern-match: let, if-let, while-let, match
//
// + enums receive "drop glue" code as needed (but are NOT Drop, by default)
// - by default, enums are NOT Copy, Clone, Debug, Hash, Eq, Ord, Default
// + auto traits: enums are Send, Sync, Unpin, ... if all field types are
//
// - CANNOT have
//     mutable values -> could use std::cell::Cell, but pointless
//     associated constants or types -> could use traits
//     abstract functions + methods -> could use traits
//     overriden or shadowed functions + methods -> could use trait objects
//     interface or implementation inheritance from enums -> use traits
//
// + can have subtyping / inheritance of enums from traits
//     -> see ./traits_basics.rs
//
// https://doc.rust-lang.org/std/keyword.enum.html
// https://doc.rust-lang.org/reference/dynamically-sized-types.html
// https://doc.rust-lang.org/reference/expressions/field-expr.html
// https://doc.rust-lang.org/reference/expressions/if-expr.html
// https://doc.rust-lang.org/reference/expressions/match-expr.html
// https://doc.rust-lang.org/reference/items/enumerations.html
// https://doc.rust-lang.org/reference/special-types-and-traits.html
// https://doc.rust-lang.org/book/ch06-00-enums.html
// https://doc.rust-lang.org/rust-by-example/custom_types/enum.html
// ======================================================================

// can have 0..n variants with values
enum E {
    NoData,                           // can have unit-type-like values
    Tuple(i32, bool),                 // can have tuple-type-like values
    NamedFields { i: i32, j: bool },  // can have struct-type-like values

    // CANNOT have mutable fields -> could use std::cell::Cell, but pointless
    // error: expected identifier, found keyword `mut`
    //   Mut { mut mfield: i32 },

    // CANNOT have dynamically sized enums with unsized tuples or structs
    // error: size for values `[i32]` cannot be known at compilation time
    //    DST([i32],),
    //    DSS { fdst: [i32] },
}

// CANNOT extract the types of the enum values
// error: expected type, found variant `E::NoData`
//   type t0 = E::NoData;
//   type t1 = E::Tuple;
//   type e2 = E::NamedFields;

// can have 0..n impl blocks
impl E {
    fn f(e: &E) {}         // associated function
    fn m(&self, e: &E) {}  // associated method
}

// RARELY USED: can have members of reference type (with lifetime specifier)
enum EMut<'a> { Tuple(&'a i32, &'a mut bool) }

// CANNOT extend/derive from other enum (or struct/union) -> use traits
// error: expected `where`, `{`, `(`, or `;` after struct name, found `:`
//   enum E4: E {}  // no inheritance (except from traits)

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_enum_std_traits_copy_clone_drop_debug_hash_eq_ord_default() {

        // enums receive "drop glue" code as needed (if field has destructor)
        // by default, enums are NOT Drop, even if any field is
        enum EBi32 { SBi32 { bi: Box<i32> } };
        assert_not_impl_any!(EBi32: Drop);
        assert_impl_all!(Box<i32>: Drop);

        // by default, enums are NOT Copy, Clone
        // can #[derive(Copy, Clone)]
        assert_not_impl_any!(E: Copy, Clone);

        // by default, enums are NOT Debug
        // can #[derive(Debug)]
        assert_not_impl_any!(E: Debug);

        // by default, enums are NOT Hash, Eq, Ord even if all fields are
        // can #[derive(Eq, PartialEq)]
        assert_not_impl_any!(E: Hash, Eq, PartialEq, Ord, PartialOrd);

        // by default, enums are NOT Default even if all fields are
        // CANNOT #[derive(Default)]
        assert_not_impl_any!(E: Default);
    }

    #[test]
    fn test_enum_field_access_destructure_pattern_match() {

        // all types can be inferred
        let e0: E = E::NoData;
        let e1: E = E::Tuple(1, true);
        let e2: E = E::NamedFields { i: 1, j: false };

        // CANNOT access enum value's fields, variants have no individual type
        // error: no field `...` on type `E`
        //   assert_eq!(e1.0, 1);
        //   assert_eq!(e2.i, 1);

        // can destructure enum values (if-let, while-let, match)
        //
        // CANNOT use let to destructure enum values
        // error: refutable pattern in local binding
        //   let E::Tuple(i, b) = e1;
        //
        // use if-let instead:
        if let E::NoData = e0 {
            assert!(true)
        } else {
            assert!(false, /*"e0 = {:?}", e0*/)  // needs Debug
        }
        if let E::Tuple(i, b) = e1 {
            assert_eq!((i, b), (1, true));
        } else {
            assert!(false);
        };
        if let E::NamedFields { i, j } = e2 {
            assert_eq!((i, j), (1, false));
        } else {
            assert!(false);
        };

        // can partially pattern-match enum values
        assert!(match e0 {
            E::NoData => true,
            _ => false,
        });
        assert!(match e1 {
            // can have simple constant value expressions in binding pattern
            E::Tuple(0, true) => false,

            // CANNOT have complex value expressions in binding pattern
            // error: expected one of `)`, `,`, `...`, `..=`, `..`, or `|`,
            //   E::Tuple(0 + 2, true) => false,
            // use guard instead:
            E::Tuple(x, true) if x == 0 + 2 => false,

            // can ignore fields in match
            E::Tuple(1, ..) | E::Tuple(.., true) => true,
            _ => false,
        });
        assert!(match e2 {
            // can ignore fields in match, `..` must come last
            E::NamedFields { j: true, .. } => false,
            E::NamedFields { i: 0, .. } => false,
            E::NamedFields { i: 1, j: false } => true,
            _ => false,
        });
    }

    #[test]
    fn test_enum_functions_methods() {

        let e0: E = E::NoData;
        let e1: E = E::Tuple(1, true);

        assert_eq!(E::f(&e0), ());
        assert_eq!(E::m(&e0, &e1), ());
        assert_eq!(e0.m(&e1), ());
    }
}
