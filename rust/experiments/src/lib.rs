// pub mod const_items_and_evaluation;
// pub mod generics;
// pub mod generics_functions_methods;
// pub mod generics_traits_static_v_dynamic_dispatch;
// pub mod hash_eq_ord;
pub mod reference_lifetime_elision_coercion;
//pub mod static_items_and_lifetime;
// pub mod structs_with_features;
// pub mod traits;
// pub mod traits_static_v_dynamic_dispatch;
// pub mod traits_subtyping;
// pub mod types;
// pub mod types_arrays_slices_vectors;
// pub mod types_empty_tuples_structs_enums;
// pub mod types_enums;
// pub mod types_structs;
// pub mod types_tuples;
// pub mod types_unions;

// code experiment, compiler bug:
// pub mod surprise_sends_rustc_into_endless_loop;
//
// code experiments with compile errors:
// pub mod surprise_let_binding_v_pattern_matching;
