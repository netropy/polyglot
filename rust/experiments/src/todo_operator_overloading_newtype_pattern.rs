#![allow(unused)]

// https://doc.rust-lang.org/book/ch19-03-advanced-traits.html#default-generic-type-parameters-and-operator-overloading
// https://doc.rust-lang.org/book/ch19-04-advanced-types.html#using-the-newtype-pattern-for-type-safety-and-abstraction
// default generic type parameters and operator overloading
// impl Add<Meters> for Millimeters {...}
// using the newtype pattern for type safety and abstraction
