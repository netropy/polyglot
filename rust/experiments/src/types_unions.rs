#![allow(unused)]

use core::fmt::Debug;
use core::hash::Hash;
use static_assertions::*;
use std::mem::ManuallyDrop;

// ======================================================================
// Summary, Code Examples: Features/Limitations of Union Types
//
// In contrast to enums:
// => unions are UNTAGGED (or non-discriminated)
// => hence, MUST access or destructure within UNSAFE blocks
//
// + can have "associated items" (members)
//     1..*  fields that MUST be Copy or ManuallyDrop<...>
//           or of [im-]mutable reference type
//     0..*  functions/methods defined in ...
//     0..*  impl blocks which may implement ...
//     0..*  traits
//
// + can access fields by name
// + can destructure with: let
// - POINTLESS to destructure/pattern-match with: .., if-let, while-let, match
//
// - unions do NOT receive "drop glue" code since UNTAGGED
// - by default, unions are NOT Copy, Clone, Debug, Hash, Eq, Ord, Default
// + auto traits: unions are Send, Sync, Unpin, ... if all field types are
//
// - CANNOT have
//     dummy fields of "never" type: !
//     fields that are Drop -> wrap as ManuallyDrop<...>
//     associated constants or types -> use traits
//     abstract functions + methods -> use traits
//     overriden or shadowed functions + methods -> use trait objects
//     interface or implementation inheritance from unions -> use traits
//
// + can have subtyping / inheritance of unions from traits
//     -> see ./traits_basics.rs
//
// https://doc.rust-lang.org/std/keyword.union.html
// https://doc.rust-lang.org/reference/expressions/field-expr.html
// https://doc.rust-lang.org/reference/items/unions.html
// https://doc.rust-lang.org/reference/special-types-and-traits.html
// https://doc.rust-lang.org/book/ch19-01-unsafe-rust.html
// ======================================================================

// can have 0..n fields
union U {
    // can have "tag" fields, but POINTLESS, since union is untagged
    ft: (),
    // can have fields that are Copy or std::mem::ManuallyDrop<T>
    fi: i32,
    fb: bool,

    // CANNOT have fields of "never" type: !
    // error: the `!` type is experimental
    //   fx: !,

    // CANNOT have fields that have a destructor, i.e., not Copy or Drop
    // error: field must implement `Copy` or be wrapped in `ManuallyDrop<...>`
    //   fs: String,       // is not Copy
    //   fbox: Box<i32>,   // is Drop
    // MUST wrap to disable (automatic) Drop:
    fs: ManuallyDrop<String>,
    fbox: ManuallyDrop<Box<i32>>,

    // RARELY USED: can have members of reference type (with lifetime specifier)
    //   union U<'a> { cref: &'a String, mref: &'a mut String }
}

// can have 0..n impl blocks
impl U {
    fn f(u: &U) {}         // associated function
    fn m(&self, u: &U) {}  // associated method
}

// CANNOT extend/derive from other union (or enum/struct) -> use traits
// error: expected `where`, `{`, `(`, or `;` after struct name, found `:`
//   union U4: U {}  // no inheritance (except from traits)

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_union_std_traits_copy_clone_drop_debug_hash_eq_ord_default() {

        // unions do NOT receive "drop glue" code as needed, since UNTAGGED
        // by default, unions are NOT Drop
        assert_not_impl_any!(U: Drop);

        // by default, unions are NOT Copy, Clone
        // can #[derive(Copy, Clone)]
        assert_not_impl_any!(U: Copy, Clone);

        // by default, unions are NOT Debug even if all fields are
        // CANNOT #[derive(Debug)]
        assert_not_impl_any!(U: Debug);

        // by default, unions are NOT Hash, Eq, Ord even if all fields are
        // CANNOT #[derive(Hash, Eq, PartialEq)]
        assert_not_impl_any!(U: Hash, Eq, PartialEq, Ord, PartialOrd);

        // by default, enums are NOT Default even if all fields are
        // CANNOT #[derive(Default)]
        assert_not_impl_any!(U: Default);
    }

    #[test]
    fn test_union_field_access_destructure_pattern_match_unsafe() {

        // all types can be inferred
        let u0: U = U { fi: 1 };
        let u1: U = U { fb: true };

        // expected: must specify exactly 1 variant/field
        // error: union expressions should have exactly one field
        //   let u2: U = U { fi: 1, fb: true };

        // can access fields = UNSAFE reading of values
        // error: access to union field is unsafe
        //   assert_eq!(u0.fi, 1);
        assert_eq!(unsafe { u0.fi }, 1);
        assert_eq!(unsafe { u1.fb }, true);

        // why UNSAFE: can read UNTAGGED union as any value
        assert_eq!(unsafe { u0.ft }, ());
        assert_eq!(unsafe { u1.ft }, ());
        assert_eq!(unsafe { u0.fb }, true);  // coincidentally 1 == true
        assert_eq!(unsafe { u1.fi }, 1);

        // why UNSAFE: can destructure UNTAGGED union as any value (let)
        unsafe {
            let U { fi: i } = u0;
            assert_eq!(i, 1);
            let U { fb: b } = u0;  // coincidentally 1 == true
            assert_eq!(b, true);
        }
        unsafe { let U { fb: b } = u0; }
        //let u0: U = U { fi: 1 };
        //let u1: U = U { fb: true };

        // POINTLESS to use if-let, while-let since union is UNTAGGED
        unsafe {
            // warning: irrefutable `if let` pattern
            //   if let U { fi: i } = u0 {  // will always match
            //     assert_eq!(i, 1);
            //   }
        }

        // POINTLESS to use match since union is UNTAGGED
        unsafe {
            // can use probing pattern-match on union
            assert!(
                match u0 {
                    U { fi: 1 } => true,
                    _ => false,  // catch-all
                }
            );

            // can use binding pattern-match on union
            assert!(
                match u0 {
                    // MUST use field name for binding
                    // error: union `S` does not have a fields named `f`, `b`
                    //   S { f, b } => assert_eq!(f, 1),
                    U { fi: 0 } => false,
                    U { fi: i } if i == 0 => false,
                    U { fi } if fi == 0 => false,
                    U { fb } => fb,  // coincidentally 1 == true
                    _ => false,  //  catch-all
                }
            );
        }
    }

    #[test]
    fn test_union_functions_methods() {

        let u0: U = U { fi: 1 };
        let u1: U = U { fb: true };

        assert_eq!(U::f(&u0), ());
        assert_eq!(U::m(&u0, &u1), ());
        assert_eq!(u0.m(&u1), ());
    }
}
