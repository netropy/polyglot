#![allow(unused)]

use core::fmt::Debug;
use core::hash::Hash;
use static_assertions::*;

// ======================================================================
// Summary, Code Examples: Features/Limitations of Tuple Types
//
// + can have as "associated items" (members)
//     0..*      immutable elements (fields) of any (sized) type
//               or [im-]mutable fields of reference type
//     1 field:  MUST have trailing comma in type and value expressions
//
// + can access fields by index
// + can [partially] destructure/pattern-match: let, if-let, while-let, match
//
// + tuples receive "drop glue" code as needed (but are NOT Drop, by default)
// + tuples are Copy, Clone if all element types are
// + tuples are Hash, Eq, Ord if of same type, all fields are (<= 12 elements)
// + tuples are Debug if all element types are and <= 12 elements
// - tuples are NOT IntoIterator
// + tuples are From<Array> (<=12 elements)
// + tuples are Default IF element types are (<=12 elements)
// + auto traits: tuples are Send, Sync, Unpin, ... if all field types are
//
// - CANNOT have
//     mutable fields -> for RARELY USED internal mutability, see below
//     impl blocks, functions or methods -> use structs or tuple structs
//     associated constants or types -> use traits
//     subtyping or inheritance for/from tuples -> use traits
//
// https://doc.rust-lang.org/std/primitive.tuple.html
// https://doc.rust-lang.org/reference/expressions/field-expr.html
// https://doc.rust-lang.org/reference/expressions/if-expr.html
// https://doc.rust-lang.org/reference/expressions/match-expr.html
// https://doc.rust-lang.org/reference/expressions/tuple-expr.html
// https://doc.rust-lang.org/reference/special-types-and-traits.html
// https://doc.rust-lang.org/reference/types/tuple.html
// https://doc.rust-lang.org/book/ch03-02-data-types.html#the-tuple-type
// https://doc.rust-lang.org/rust-by-example/primitives/tuples.html
// ======================================================================

// tuples can have 0..n fields
type T = (i32, String,);

// 1-element tuple types MUST have trailing comma
type T1 = (i32,);   // a single
type Ti32 = (i32);  // an integer
assert_type_eq_all!(Ti32, i32);
assert_type_ne_all!(T1, Ti32);

// long tuple >=13 elements (currently) lack automatic Debug, Hash, Eq, Ord etc
type LT = (
    i16, i16, i16, i16, i16,
    i32, i32, i32, i32, i32,
    i64, i64, i64,
);

// RARELY USED: can have members of reference type (with lifetime specifier)
// expectation: tuples group immutable values, not references -> use structs
type T2<'a> = (&'a i32, &'a mut i32);

// UNEXPECTED: can define tuple type with multiple, unsized fields
// -> rustc 1.74.0 compiler bug?
type T3A = (i32, [i32]);
type T3B = ([i32], [i32], i32);
// error: size for values of type `[i32]` cannot be known at compilation time
//   assert_impl_all!(T3A: Sized);
//   assert_not_impl_any!(T3A: Sized);
// expected: cannot declare variables or parameters of unsized type
//   let t3: T3A = (1, [1,2,3][..],);

// CANNOT have mutable fields
// error: expected type, found keyword `mut`
//   type T4 = (mut i32,);
//
// RARELY USED: can emulate (internal) mutability by std::cell::Cell<T>
// expectation: tuples group immutable values -> use structs + encapsulation
//   type T4 = (std::cell::Cell<i32>,);

// expected: CANNOT have impl blocks, associated functions or methods
// error: cannot define inherent `impl` for primitive types
//   impl T {}

// expected: CANNOT extend/derive/subtype tuples -> use traits
// error: expected one of `;` or `where`, found `:`
//   type T5 = (i32) : (String);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1element_tuple() {

        // MUST have trailing comma for 1-element tuple values
        // error: mismatched types, expected `(i32,)`, found integer
        //    let t0: T1 = (1);
        let t0: T1 = (1,);

        // otherwise, parenthesis are redundant
        // error: mismatched types, expected `i32`, found `({integer},)`
        //    let ti: Ti32 = (1,);
        let ti: Ti32 = (1);
    }

    #[test]
    fn test_tuple_std_traits_copy_clone_drop_debug_hash_eq_ord_default() {

        // tuples receive "drop glue" code as needed (if field has destructor)
        // tuples are NOT Drop, even if any field is
        assert_not_impl_any!((Box<i32>,): Drop);
        assert_impl_all!(Box<i32>: Drop);

        // tuples are Copy/Clone IF all element types are
        assert_impl_all!((i32, i64): Copy, Clone);
        assert_impl_all!((i32, String): Clone);
        assert_not_impl_any!((i32, String): Copy);

        // tuples are Debug IF all fields types are
        // (currently limited to <=12 fields)
        assert_impl_all!(T: Debug);
        assert_not_impl_any!(LT: Debug);

        // tuples are component-wise Hash, Eq, Ord IF all fields are
        // (currently limited to <=12 fields)
        assert_impl_all!(T: Hash, Eq, PartialEq, Ord, PartialOrd);
        assert_not_impl_any!(LT: Hash, Eq, PartialEq, Ord, PartialOrd);

        // tuples are Default IF element types are
        // (currently limited to <=12 fields)
        assert_impl_all!((): Default);
        assert_impl_all!(T: Default);
        assert_impl_all!(T1: Default);
        assert_impl_all!(
            (i16, i16, i16, i16, i16,
             i32, i32, i32, i32, i32,
             i64, i64,): Default);
        assert_not_impl_any!(LT: Default);
    }

    #[test]
    fn test_tuple_std_traits_intoiterator_from() {

        // tuples are NOT IntoIterator
        // error: `({integer}, {integer})` is not an iterator
        //    assert_eq!((1, 1).into_iter().sum::<i32>(), 2);
        assert_not_impl_any!((): IntoIterator);
        assert_not_impl_any!((i32,): IntoIterator);

        // tuples are From<Array>
        // (currently limited to <=12 fields)
        assert_not_impl_any!(((),): From<[(); 0]>);
        assert_impl_all!((i32,): From<[i32; 1]>);
        assert_impl_all!(
            (i32, i32, i32, i32, i32,
             i32, i32, i32, i32, i32,
             i32, i32,): From<[i32; 12]>);
        assert_not_impl_any!(
            (i32, i32, i32, i32, i32,
             i32, i32, i32, i32, i32,
             i32, i32, i32,): From<[i32; 13]>);
    }

    #[test]
    fn test_tuple_index_access_destructure_pattern_match() {

        // all types can be inferred
        let a: String = "a".to_string();
        let t0: T = (1, a.clone());
        let t1: (i32, (i32, ())) = (2, (1, ()));
        let lt: LT = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);

        // can access fields by tuple indexing
        assert_eq!(t0, (t0.0.clone(), t0.1.clone()));
        assert_eq!(t1.1.1, ());
        assert_eq!(lt.12, 13);

        // can destructure tuple values (let, if-let, while-let, match)
        let (i, s) = t0.clone();
        assert_eq!((i, s), t0);
        let (i, (s, v)) = t1.clone();
        assert_eq!((i, (s, v)), t1);
        let (l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13) = lt;
        assert_eq!(l13, lt.12);

        // can partially destructure tuple values
        let (i, ..) = t0.clone();
        assert_eq!(i, 1);
        let (.., i) = t0.clone();
        assert_eq!(i, a);
        let (l1, .., l13) = lt;
        assert_eq!((l1, l13), (lt.0, lt.12));

        // can pattern-match tuple values (as place and value expressions)
        assert!(match t0 {
            // can have simple constant value expressions in binding pattern
            (0, _) => false,

            // CANNOT have complex value expressions in binding pattern
            // error: expected one of `)`, `,`, `...`, `..=`, `..`, or `|`,
            //    (0 + 2, _) => false,
            // use guard instead:
            (i, _) if i == 0 + 2 => false,

            // PUZZLER: does not match identifier 'a' but binds a local 'a'
            //    (1, a) => true,
            // use guard instead:
            (1, x) if x == a => true,

            _ => false,  // catch-all
        });
        assert!(match t1 {
            (2, (1, ())) => true,
            _ => false,
        });

        // can partially pattern-match tuple values
        assert!(match lt {
            (0, ..) => false,
            (.., 10) => false,
            (1, .., 10) => false,
            (0, x, ..) | (1, x, ..) if x == -1 => false,
            (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13) => true,
            _ => false,
        });
    }
}
