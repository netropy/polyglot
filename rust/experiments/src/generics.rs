#![allow(unused)]

// ======================================================================
// Summary, Code Examples: Features/Limitations of Generic Types
//
// TOC:
// * Generic Type Aliases
// * Generic Struct/Enum/Union/Impl with Fields/Values/Functions/Methods
// * Can Specialize, Conditionalize, or Augment Generic Types for NEW Features
// ======================================================================

use std::string::ToString;

// ----------------------------------------------------------------------
// Generic Type Aliases
//
// - can have default type arguments
// - can have generic function pointer type alias
// - can have nested/recursive rank-1/kind-1 types
// - can specialize generic type aliases
// - NOT checked/type-safe: trait bounds on type parameters
// - CANNOT have higher-kinded generic type (aliases)
// ----------------------------------------------------------------------

// can have identity type alias
type Identity<A> = A;

// MUST use a (named) lifetime parameter
// error: missing lifetime specifier
//   type RefAlias<A> = &A;
type RefAlias<'a, A> = &'a A;

// can have default type arguments
type DefaultedPair<A = i32, B = f64> = (A, B);
const IF: DefaultedPair = (-1, 2.2);
const FF: DefaultedPair<f32> = (-1.1, 2.2);
// CANNOT skip 1st defaulted type argument
// error: expected one of `>`, a const expression, lifetime, or type, found `,`
//   const II: DefaultedPair<, i32> = (-1, 2);
const II: DefaultedPair<i32, i32> = (-1, 2);

// can have generic function pointer type alias
// error: function pointer types may not have generic parameters
//   type BinGenericOp<A> = fn<A>(A, A) -> A;
type BinOp<A> = fn(A, A) -> A;

// can have nested/recursive rank-1/kind-1 types
type BoxAlias<A> = Box<A>;
type BoxBoxAlias<A> = Box<Box<A>>;

// can specialize generic type aliases
type RefAliasF64<'a> = &'a f64;
type BoxAliasF64 = BoxAlias<f64>;
type BoxBoxAliasF64 = Box<BoxAlias<f64>>;

// NOT checked/type-safe: trait bounds on type parameters
// warning: bounds on generic parameters are not enforced in type aliases
//   type AliasWithTypeBound<A: std::error::Error> = Box<A>;
// NO error or warning:
//   type AliasWithTypeBoundI32 = AliasWithTypeBound<i32>;

// CANNOT have higher-kinded generic types
// error: expected one of `,`, `:`, `=`, or `>`, found `<`
//   type ContainerI32<C<_>> = C<i32>;
//   type ContainerI32<A, C<A>> = C<i32>;
// error: type arguments are not allowed on type parameter `C`
//   type ContainerI32<C> = C<i32>;
//   type Container<A, C> = C<A>;

// ----------------------------------------------------------------------
// Generic Struct/Enum/Union/Impl with Fields/Values/Functions/Methods
//
// - static dispatch (monomorphization)
// - can have trait bounds on type parameters
// - can add derived traits to generic struct/enum/union
// - can have nested/recursive rank-1/kind-1 types
// ----------------------------------------------------------------------

// can have trait bounds for type parameter
// can add derived traits to generic struct
#[derive(Default)]
#[derive(Clone)]
struct S0<A: Default + Clone> {  // with 2 trait bounds
    field: A,
}

impl<A: Default + Clone> S0<A> {
    fn f_impl(a: &A) {}
    fn m_impl(&self, a: &A) {}
}

// can have trait bounds for type parameter
// can add derived traits to generic enum
// error: no default declared
//   #[derive(Default)]
#[derive(Clone)]
enum E0<A> where A: Clone { Value(A) }  // trait bound as where-clause

impl<A: Clone> E0<A> {
    fn f_impl(a: &A) {}
    fn m_impl(&self, a: &A) {}
}

// can have trait bounds for type parameter
// unclear: why do unions need Copy?
// error: field must implement `Copy` or be wrapped in `ManuallyDrop<...>`
//   union U0<A> { value: A }
// error: this trait cannot be derived for unions
//   #[derive(Default)]
// can add derived traits to generic union
#[derive(Clone)]  // required by Copy
#[derive(Copy)]   // to support passing U0<A> as recursive type argument
union U0<A: Copy> { value: A }  // with trait bound

impl<A> U0<A> where A: Copy {  // trait bound as where-clause
    fn f_impl(a: &A) {}
    fn m_impl(&self, a: &A) {}
}

// can have nested/recursive rank-1/kind-1 struct/enum/union types
type S0S0<A> = S0<S0<A>>;
type E0E0<A> = E0<E0<A>>;
type U0U0<A> = U0<U0<A>>;

#[cfg(test)]
mod tests0 {
    use super::*;

    #[test]
    fn test_generic_structs() {

        let i: &i32 = &1;
        let s: S0<i32> = S0::<i32> { field: 0 };  // turbofish ::<...>

        assert_eq!(S0::<i32>::f_impl(i), {});
        assert_eq!(S0::f_impl(i), {});
        assert_eq!(s.m_impl(i), {});
    }

    #[test]
    fn test_generic_enums() {

        let i: &i32 = &1;
        let e: E0<i32> = E0::<i32>::Value(0);  // turbofish ::<...>

        assert_eq!(E0::<i32>::f_impl(i), {});
        assert_eq!(E0::f_impl(i), {});
        assert_eq!(e.m_impl(i), {});
    }

    #[test]
    fn test_generic_unions() {

        let i: &i32 = &1;
        let u: U0<i32> = U0::<i32> { value: 0 };  // turbofish ::<...>

        assert_eq!(U0::<i32>::f_impl(i), {});
        assert_eq!(U0::f_impl(i), {});
        assert_eq!(u.m_impl(i), {});
    }

    #[test]
    fn test_nested_recursive_generic_types() {

        let s: S0S0<i32> =
            S0::<S0::<i32>> { field: S0::<i32> { field: 0 } };

        let e: E0E0<i32> =
            E0::<E0::<i32>>::Value(E0::<i32>::Value(0));

        let u: U0U0<i32> =
            U0::<U0::<i32>> { value: U0::<i32> { value: 0 } };
    }
}

// ----------------------------------------------------------------------
// Can Specialize, Conditionalize, or Augment Generic Types for NEW Features
//
// - can add extra functions/methods/constants to generic struct/enum/union:
//   - as specialized implementations for a concrete argument type
//   - as conditional implementations with additional trait bounds
//   - as unconditional, generic implementations
// - CANNOT shadow/hide functions/methods of more general implementations
// => monomorphization makes specialized struct/enum/union a distinct type
// ----------------------------------------------------------------------

// unconditional, generic implementations

impl<A: Default + Clone> S0<A> {
    // CANNOT shadow functions/methods of more general implementations
    // error: duplicate definitions with name `f_impl`, `m_impl`
    //   fn f_impl(a: &A) {}
    //   fn m_impl(&self, a: &A) {}
}

impl<A: Default + Clone> S0<A> {
    fn f_clone_arg(a: &A) -> A { a.clone() }
    fn m_clone_arg(&self, a: &A) -> A { a.clone() }
    const C: i32 = 1;
}

impl<A: Clone> E0<A> {
    fn f_clone_arg(a: &A) -> A { a.clone() }
    fn m_clone_arg(&self, a: &A) -> A { a.clone() }
    const C: i32 = 1;
}

impl<A: Copy> U0<A> {
    fn f_clone_arg(a: &A) -> A { a.clone() }
    fn m_clone_arg(&self, a: &A) -> A { a.clone() }
    const C: i32 = 1;
}

// conditional implementations with additional trait bounds

// extra trait bound: ToString
impl<A: Default + Clone + ToString> S0<A> {
    // CANNOT shadow functions/methods of more general implementations
    // error: duplicate definitions with name `f_impl`, `m_impl`
    //   fn f_impl(a: &A) {}
    //   fn m_impl(&self, a: &A) {}

    // CANNOT shadow/hide/redefine constants of more general implementations
    // error: duplicate definitions with name `C`
    //   const C: i32 = 1;
}

impl<A: Default + Clone + ToString> S0<A> {
    fn f_to_string(a: &A) -> String { a.to_string() }
    fn m_to_string(&self, a: &A) -> String { a.to_string() }
}

impl<A: Clone + ToString> E0<A> {
    fn f_to_string(a: &A) -> String { a.to_string() }
    fn m_to_string(&self, a: &A) -> String { a.to_string() }
}

impl<A: Copy + ToString> U0<A> {
    fn f_to_string(a: &A) -> String { a.to_string() }
    fn m_to_string(&self, a: &A) -> String { a.to_string() }
}

// specialized implementations for a concrete type

impl S0::<i32> {
    // CANNOT shadow functions/methods of more general implementations
    // error: duplicate definitions with name `f_impl`, `m_impl`
    //   fn f_impl(a: &i32) {}
    //   fn m_impl(&self, a: &i32) {}

    // CANNOT shadow/hide/redefine constants of more general implementations
    // error: duplicate definitions with name `C`
    //   const C: i32 = 1;
}

impl S0::<i32> {
    fn f_impl_i32(a: &i32) {}
    fn m_impl_i32(&self, a: &i32) {}
}

impl E0::<i32> {
    fn f_impl_i32(a: &i32) {}
    fn m_impl_i32(&self, a: &i32) {}
}

impl U0::<i32> {
    fn f_impl_i32(a: &i32) {}
    fn m_impl_i32(&self, a: &i32) {}
}

#[cfg(test)]
mod tests1 {
    use super::*;

    #[test]
    fn test_augmented_impl_for_struct_enum_union() {

        let i: i32 = 1;
        let s: S0<i32> = S0::<i32> { field: 0 };  // turbofish ::<...>
        let e: E0<i32> = E0::<i32>::Value(0);
        let u: U0<i32> = U0::<i32> { value: 0 };

        assert_eq!(S0::<i32>::f_clone_arg(&i), i);
        assert_eq!(S0::f_clone_arg(&i), i);
        assert_eq!(s.m_clone_arg(&i), i);

        assert_eq!(E0::<i32>::f_clone_arg(&i), i);
        assert_eq!(E0::f_clone_arg(&i), i);
        assert_eq!(e.m_clone_arg(&i), i);

        assert_eq!(U0::<i32>::f_clone_arg(&i), i);
        assert_eq!(U0::f_clone_arg(&i), i);
        assert_eq!(u.m_clone_arg(&i), i);

        assert_eq!(S0::<i32>::C, S0::<i32>::C);
        assert_eq!(E0::<i32>::C, E0::<i32>::C);
        assert_eq!(U0::<i32>::C, U0::<i32>::C);
    }

    #[test]
    fn test_conditionalized_impl_for_struct_enum_union() {

        let i: i32 = 1;
        let r: String = i.to_string();
        let s: S0<i32> = S0::<i32> { field: 0 };  // turbofish ::<...>
        let e: E0<i32> = E0::<i32>::Value(0);
        let u: U0<i32> = U0::<i32> { value: 0 };

        assert_eq!(S0::<i32>::f_to_string(&i), r);
        assert_eq!(S0::f_to_string(&i), r);
        assert_eq!(s.m_to_string(&i), r);

        assert_eq!(E0::<i32>::f_to_string(&i), r);
        assert_eq!(E0::f_to_string(&i), r);
        assert_eq!(e.m_to_string(&i), r);

        assert_eq!(U0::<i32>::f_to_string(&i), r);
        assert_eq!(U0::f_to_string(&i), r);
        assert_eq!(u.m_to_string(&i), r);

        // expected: extra functions/methods unavailable on other types
        // error: the function exists but its trait bounds were not satisfied
        //                                                not satisfied:
        //    assert_eq!(S0::<str>::f_to_string("1"), r); // Clone,Default,Sized
        //    assert_eq!(E0::<str>::f_to_string("1"), r); // Clone,Sized
        //    assert_eq!(U0::<str>::f_to_string("1"), r); // Sized,Copy`
    }

    #[test]
    fn test_specialized_impl_for_struct_enum_union() {

        let i: i32 = 1;
        let s: S0<i32> = S0::<i32> { field: 0 };  // turbofish ::<...>
        let e: E0<i32> = E0::<i32>::Value(0);
        let u: U0<i32> = U0::<i32> { value: 0 };

        assert_eq!(S0::<i32>::f_impl_i32(&i), {});
        assert_eq!(S0::f_impl_i32(&i), {});
        assert_eq!(s.m_impl_i32(&i), {});

        assert_eq!(E0::<i32>::f_impl_i32(&i), {});
        assert_eq!(E0::f_impl_i32(&i), {});
        assert_eq!(e.m_impl_i32(&i), {});

        assert_eq!(U0::<i32>::f_impl_i32(&i), {});
        assert_eq!(U0::f_impl_i32(&i), {});
        assert_eq!(u.m_impl_i32(&i), {});

        // expected: extra functions/methods unavailable on other types
        // error: no function or associated item found
        //   assert_eq!(S0::<i64>::f_impl_i32(&i), {});
        //   assert_eq!(E0::<i64>::f_impl_i32(&i), {});
        //   assert_eq!(U0::<i64>::f_impl_i32(&i), {});
    }
}
