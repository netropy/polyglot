#![allow(unused)]

use core::fmt::Debug;
use core::hash::Hash;
use static_assertions::*;
use std::cell::Cell;

// ======================================================================
// Summary, Code Examples: Features/Limitations of Struct Types
//
// + can have as "associated items" (members)
//     0..*  immutable, sized fields
//           or [im-]mutable fields of reference type
//     0..1  last field of unsized type
//     0..*  functions/methods defined in ...
//     0..*  impl blocks which may implement ...
//     0..*  traits
//
// + can access fields by name
// + can [partially] destructure/pattern-match: let, if-let, while-let, match
//
// + structs receive "drop glue" code as needed (but are NOT Drop, by default)
// - by default, structs are NOT Copy, Clone, Debug, Hash, Eq, Ord, Default
// + auto traits: structs are Send, Sync, Unpin, ... if all field types are
//
// - CANNOT have
//     mutable fields -> for RARELY USED internal mutability, see below
//     associated constants or types -> use traits
//     abstract functions + methods -> use traits
//     overriden or shadowed functions + methods -> use trait objects
//     interface or implementation inheritance from structs -> use traits
//
// + can have subtyping / inheritance of structs from traits only
//     -> see ./traits_basics.rs
// + structs with derived traits and converters/formatters
//     -> see ./structs_with_features.rs
//
// https://doc.rust-lang.org/std/keyword.struct.html
// https://doc.rust-lang.org/reference/dynamically-sized-types.html
// https://doc.rust-lang.org/reference/expressions/field-expr.html
// https://doc.rust-lang.org/reference/expressions/if-expr.html
// https://doc.rust-lang.org/reference/expressions/match-expr.html
// https://doc.rust-lang.org/reference/expressions/struct-expr.html
// https://doc.rust-lang.org/reference/expressions/tuple-expr.html
// https://doc.rust-lang.org/reference/items/structs.html
// https://doc.rust-lang.org/reference/special-types-and-traits.html
// https://doc.rust-lang.org/book/ch05-00-structs.html
// https://doc.rust-lang.org/rust-by-example/custom_types/structs.html
// ======================================================================

// 1-element tuple struct
struct TSi16(i16);
struct TS(i16,);  // can have trailing comma (preferred)
assert_type_ne_all!(TS, TSi16);

// long tuple struct >=13 fields
struct LTS(
    i16, i16, i16, i16, i16,
    i32, i32, i32, i32, i32,
    i64, i64, i64,);

// [tuple] structs can have 0..n fields
struct S {
    // can have sized fields
    fi: i32,
    fs: String,

    // CANNOT have mutable fields -> use std::cell::Cell, see below
    // error: expected identifier, found keyword `mut`
    //   mut mfield: i32,

    // CANNOT define constants or types -> use traits
    // error: expected identifier, found keyword `const`
    //   const I: i32 = 1;
    // error: expected identifier, found keyword `type`
    //   type I = i32;

    // CANNOT implement associated functions + methods here -> use impl blocks
    // error: functions are not allowed in struct definitions
    //   fn function() -> S { S { fi: 0, fs: "a".to_string(), } }
    //   fn method(&self) -> i32 { self.fi }

    // CANNOT declare abstract functions + methods -> use traits
    // error: functions are not allowed in struct definitions
    //   fn abstract_f() -> S;
    //   fn abstract_m(&self) -> i32;
}

// can have 0..n impl blocks
impl S {
    fn f(s: &S) {}  // associated function

    // CANNOT have abstract functions/methods in impl blocks
    // error: associated function in `impl` without body
    //   fn abstract_f() -> S;
    //   fn abstract_m(&self) -> i32;
}

// can have additive impl blocks
impl S {
    fn m(&self, s: &S) {}  // associated method

    // CANNOT override definitions -> use trait objects
    // error: duplicate definitions with name
    //   fn f(s: &S) {}
}

// can have dynamically sized structs with a single DST as the last field
struct DST {
    // only the last field in a struct may have a dynamically sized type
    // XXX Enum variants must not have dynamically sized types as data.
    // error: size for values of type `[i32]` cannot be known at compilation
    //   fdst: [i32],
    //   fi: i32,
    // ok:
    fdst: [i32],
}
assert_not_impl_any!(DST: Sized);  // this struct is dynamically sized
assert_impl_all!(LTS: Sized);  // the other structs are statically sized
assert_impl_all!(S: Sized);

// RARELY USED: can have members of reference type (with lifetime specifier)
struct S3<'a> { fci: &'a i32, fmi: &'a mut i32 }

// RARELY USED: can emulate (internal) mutability by std::cell::Cell<T>
// expectation: hide mutability behind encapsulation
struct MS {
    cell: Cell<i32>,
}

// CANNOT extend/derive from other struct (or enum/union) -> use traits
// error: expected `where`, `{`, `(`, or `;` after struct name, found `:`
//   struct S4: S {}  // no inheritance (except from traits)

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_struct_std_traits_copy_clone_drop_debug_hash_eq_ord_default() {

        // structs receive "drop glue" code as needed (if field has destructor)
        // by default, structs are NOT Drop, even if any field is
        struct SBi32 { bi: Box<i32> };
        assert_not_impl_any!(SBi32: Drop);
        assert_impl_all!(Box<i32>: Drop);

        // by default, [tuple] structs are NOT Copy, Clone
        // can #[derive(Copy, Clone)]
        assert_not_impl_any!(TS: Copy, Clone);
        assert_not_impl_any!(LTS: Copy, Clone);
        assert_not_impl_any!(S: Copy, Clone);

        // by default, [tuple] structs are NOT Debug
        // can #[derive(Debug)]
        assert_not_impl_any!(TS: Debug);
        assert_not_impl_any!(LTS: Debug);
        assert_not_impl_any!(S: Debug);

        // by default, [tuple] structs are NOT Hash, Eq, Ord IF all fields are
        // can #[derive(Hash, Eq, PartialEq)]
        assert_not_impl_any!(TS: Hash, Eq, PartialEq, Ord, PartialOrd);
        assert_not_impl_any!(LTS: Hash, Eq, PartialEq, Ord, PartialOrd);
        assert_not_impl_any!(S: Hash, Eq, PartialEq, Ord, PartialOrd);

        // by default, enums are NOT Default even if all fields are
        // can #[derive(Default)]
        assert_not_impl_any!(TS: Default);
        assert_not_impl_any!(LTS: Default);
        assert_not_impl_any!(S: Default);
    }

    #[test]
    fn test_struct_field_access_destructure_pattern_match() {

        // all types can be inferred
        let a: String = "a".to_string();
        let s0: S = S { fi: 1, fs: a.clone() };
        let ts: TS = TS(1,);  // can have trailing comma (preferred)
        let lts : LTS = LTS(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);

        // can access fields
        assert_eq!(s0.fi, 1);
        assert_eq!(s0.fs, a);
        assert_eq!(ts.0, 1);
        assert_eq!(lts.12, 13);

        // can destructure tuple values (let, if-let, while-let, match)
        let S { fi, fs } = s0;
        assert_eq!((fi, fs), (1, a.clone()));
        let TS(i,) = ts;  // can have trailing comma (preferred)
        assert_eq!(i, 1);
        let LTS(l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13) = lts;
        assert_eq!(l13, lts.12);

        // can destructure tuple values (let, if-let, while-let, match)
        let s0: S = S { fi: 1, fs: a.clone() };
        let S { fi, .. } = s0;
        assert_eq!(fi, 1);
        let s0: S = S { fi: 1, fs: a.clone() };
        let S { fs, .. } = s0;  // can ignore fields, `..` must come last
        assert_eq!(fs, a);
        let LTS(l1, .., l13) = lts;  // tuple structs can have `..` anywhere
        assert_eq!((l1, l13), (lts.0, lts.12));

        // can pattern-match struct values (as place and value expressions)
        let s0: S = S { fi: 1, fs: a.clone() };
        assert!(match s0 {
            // can have simple constant value expressions in binding pattern
            S { fi: 0, fs } => false,

            // CANNOT have complex value expressions in binding pattern
            // error: expected one of `)`, `,`, `...`, `..=`, `..`, or `|`,
            //    S { fi: (0 + 2), fs } => false,
            // use guard instead:
            S { fi: i, fs } if i == 0 + 2 => false,

            // PUZZLER: does not match identifier 'a' but binds a local 'a'
            //    S { fi: 1, fs: a } => true,
            // use guard instead:
            S { fi: 1, fs: x } if x == a => true,

            _ => false,  // catch-all
        });

        // can partially pattern-match struct values
        let s0: S = S { fi: 1, fs: a.clone() };
        assert!(match s0 {
            // can ignore fields, `..` must come last
            S { fi: 2, .. } => false,
            S { fs: x, .. } if x == a => true,
            _ => false,
        });

        // can partially pattern-match tuple struct values
        assert!(match ts {
            TS(0,) => false,  // can have trailing comma (preferred)
            TS(1,) => true,
            _ => false,
        });
        assert!(match lts {
            // tuple structs can have `..` anywhere
            LTS(0, ..) => false,
            LTS(.., 10) => false,
            LTS(1, .., 10) => false,
            LTS(0, x, ..) | LTS(1, x, ..) if x == -1 => false,
            LTS(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13) => true,
            _ => false,
        });
    }

    #[test]
    fn test_structs_mutable_cells() {

        let ms: MS = MS { cell: Cell::new(1), };
        assert_eq!(ms.cell.get(), 1);

        ms.cell.set(2);
        assert_eq!(ms.cell.get(), 2);
    }

    #[test]
    fn test_structs_fields_functions_methods() {

        let s: S = S { fi: 0, fs: "a".to_string() };
        assert_eq!(s.fi, 0);

        assert_eq!(S::f(&s), ());
        assert_eq!(S::m(&s, &s), ());
        assert_eq!(s.m(&s), ());
    }
}
