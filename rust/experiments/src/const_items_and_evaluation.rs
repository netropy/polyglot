#![allow(unused)]

use std::cell::Cell;

// ======================================================================
// Summary, Code Examples: Compile-Time Evaluable Expressions and Functions
//
// + Constant Expression = can be evaluated at compile time
//   - in const contexts: always evaluated at compile time
//   - in let statements: possibly evaluated at compile time
//
// + Constant Evaluation = evaluating an expression at compile time
//   + done in the environment of the compilation target, regardless of host
//     e.g., usize is 32 bits on any host if compiling against 32 bit system
//
// + Const Context = requiring a constant expression, one of:
//   - array type length and array repeat length expressions
//   - initializers of constants, statics, enum discriminants
//   - const generic arguments
//   - const functions (with minor restrictions + relaxtions)
//
// + Constant Item = const-declared value definition
//   - must be explicitly typed
//   - type must have a 'static lifetime
//     - any references in the initializer must have 'static lifetimes
//   + name is in SCREAMING_SNAKE_CASE or optional `_` ("unnamed")
//     + can define multiple unnamed constant items within same block
//       -> What's the use case for unnamed const definitions?
//   + call-by-name semantics:
//     + inlined where used, not associated with a specific memory location
//     + yet, can take a shared reference of a constant value
//     + constants may contain destructors, run when value goes out of scope
//   ==> difference to Static Items = call-by-value, single memory location
//   ==> const items behave like nullary const functions (with minor diffs)
//
// + Const Function = const-declared function definition
//   + the only functions that may be called from a const context
//   + turning a function `const` has no effect on its run-time uses
//   - restricts argument/return types: no mutable references (vague Rust Ref)
//   - body is a const context with minor restrictions + relaxtions
//     - limits the use of fp numbers to literals + passing, no arithmetics
//   ==> const functions always yield same result, whether compile- or runtime
//
// + CAN / CANNOT be used in constant expressions:
//   + arithmetic and logical expressions
//   + block expressions with stack allocation
//   - heap allocation
//   - non-const function calls like new iter random
//   + const function calls
//   + path and noncapturing closure expressions
//   + array slice index expressions
//   - range expressions
//     -> PUZZLER: https://doc.rust-lang.org/reference/const_eval.html
//        + the Rust Reference allows for constant range expressions
//        - but the range operators [..], [s..e] etc are all non-const
//   + most cast expressions
//   + tuple struct field expressions
//   + deref_and most shared borrow expressions
//     -> PUZZLER: https://doc.rust-lang.org/reference/const_eval.html
//        - cannot use shared references to interior mutables
//        + but can path expressions to interior mutables
//   - mutable borrow expressions
//   + if let match expressions
//   + terminating loop expressions
//   - iterator for-loop expressions
//     -> PUZZLER: https://doc.rust-lang.org/reference/const_eval.html
//        + terminating loops, while-loops etc can be used in constants
//        - but iterator-for-loops cannot be constant
//        ==> Answer: taking an iterator is a non-const operation
//   + simulated for-loops (implemented as terminating wehile-loop)
//
// + constant items
//   + can be unnamed (allowing for multiple definitions within same block)
//   + can panic (failing compilation)
//   + can have non-const destructors (called when value goes out of scope)
//
// + constant functions
//   - cannot use certain types anywhere: mutable references (+ others?)
//   - must meet most restrictions of a const context
//   - restrict the use of floating-point numbers
//   + allow for const generic parameters
//
// https://doc.rust-lang.org/reference/const_eval.html
// https://doc.rust-lang.org/reference/expressions.html
// https://doc.rust-lang.org/reference/items/constant-items.html
// https://doc.rust-lang.org/reference/items/generics.html#const-generics
// https://doc.rust-lang.org/reference/items/static-items.html
// https://doc.rust-lang.org/rust-by-example/custom_types/constants.html
// https://doc.rust-lang.org/std/keyword.const.html
//
// ======================================================================


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_arithmetic_and_logical_expressions_can_be_constant() {

        // no surprises, all arithmetic and logical operations
        const LITERAL_EXPR: i32 = 3;
        const GROUPED_EXPR: i32 = (3);
        const NEGATION_EXPR0: i32 = -3;
        const NEGATION_EXPR1: bool = !true;
        const ARTIHMETIC_EXPR: i32 = 1 + 1 << 2;
        const COMPARISON_EXPR: bool = 1 == 2;
        const LOGICAL_EXPR: bool = false & true;
        const LAZY_BOOLEAN_EXPR: bool = false || true;

        // all operands must be (compile-time) constants
        // error: attempt to use a non-constant value in a constant
        //    let a: i32 = 1;
        //    const ARTIHMETIC_EXPR1: i32 = 1 + a;
    }

    #[test]
    fn test_block_expressions_with_stack_allocation_can_be_constant() {

        // constant expressions can allocate from stack
        const BLOCK_EXPR: i32 = { let x = 1; x + 1 };
    }

    #[test]
    fn test_heap_allocation_cannot_be_constant() {

        // error: allocations are not allowed in constants
        //    const CV: Vec<i32> = vec![1, 2];
    }

    #[test]
    fn test_nonconst_function_calls_like_new_iter_random_cannot_be_constant() {

        // error: cannot call non-const fn `...` in constants
        //    const CS: String = "string literal".to_string();
        //    const CS: String = String::from("string literal");
        //    const CB: Box<i32> = Box::new(1);

        // error: cannot call non-const fn `iter()`, `sum()` in constants
        //    const S: i32 = [1; 2].iter().sum();

        // error: cannot call non-const fn in constant functions
        //    rand::random::<i32>();

        // error: cannot call non-const fn `as_mut_slice` in constants
        //    const S: &mut [i32] = [1; 2].as_mut_slice();

        // error: cannot call non-const fn `incr` in constants
        //    fn incr(i: i32) -> i32 { i + 1 }
        //    const _: i32 = incr(0);
    }

    fn test_const_function_calls_can_be_constant() {

        // std::array, pub const fn as_slice(&self) -> &[T]
        const S: &[i32] = [1; 2].as_slice();

        const fn incr(i: i32) -> i32 { i + 1 }
        const I: i32 = incr(0);
        assert_eq!(I, 1);
    }

    #[test]
    fn test_path_and_noncapturing_closure_expressions_can_be_constant() {

        const PATH_EXPR0: Option<()> = None;                     // value
        const PATH_EXPR1: Option<i32> = Option::Some(1);         // value
        const PATH_EXPR2: fn(i32) -> Option<i32> = Some::<i32>;  // c'tor
        const CLOSURE_EXPR: fn(i32) -> i32 = |x| x + 1;

        // all operands must be (compile-time) constants
        // error: attempt to use a non-constant value in a constant
        //    let a: i32 = 1;
        //    const CAPTURING_CLOSURE_EXPR: fn(i32) -> i32 = |x| x + a;
    }

    #[test]
    fn test_array_slice_index_expressions_can_be_constant() {

        const ARRAY_EXPR: [i32; 2] = [1; 2];
        const ARRAY_INDEX_EXPR: i32 = ARRAY_EXPR[1];
        const SLICE_EXPR0: &[i32] = &ARRAY_EXPR;
        const SLICE_EXPR1: &str = &"str literal";
        const SLICE_INDEX_EXPR: i32 = SLICE_EXPR0[0];
    }

    #[test]
    fn test_range_expressions_cannot_be_constant() {

        // PUZZLER: the Rust Reference allows for constant range expressions:
        //
        // https://doc.rust-lang.org/reference/const_eval.html
        //    The following expressions are constant expressions, so long as
        //    any operands are also constant expressions:
        //    [...]
        //    * Range expressions.
        //      https://doc.rust-lang.org/reference/expressions/range-expr.html
        //    [...]
        //
        // BUT the range operators are non-const:
        // error: cannot call non-const operator in constants
        //    const RANGE_EXPR: &[i32] = &ARRAY_EXPR[..];
        //    const RANGE_EXPR: &[i32] = &ARRAY_EXPR[0..];
        //    const RANGE_EXPR: &[i32] = &ARRAY_EXPR[0..1];
        //    const RANGE_EXPR: &[i32] = &ARRAY_EXPR[0..=1];
    }

    #[test]
    fn test_most_cast_expressions_can_be_constant() {

        const CAST_EXPR: i64 = 3i32 as i64;

        // constants can be raw pointers
        const RAW_PTR_CAST_EXPR: *const i32 = &1;
        fn f() -> () {}

        // CANNOT coerce a function item, must cast as raw function pointer
        // error: mismatched types
        //    const RAW_FN_PTR_CAST_EXPR: *const fn() = f;
        const RAW_FN_PTR_CAST_EXPR: *const fn() = f as *const fn();

        // CANNOT have [function] pointer to address casts as constants
        // error: pointers cannot be cast to integers during const eval
        //    const ADDRESS_CAST_EXPR: usize = RAW_PTR_CAST_EXPR as usize;
        //    const ADDRESS_CAST_EXPR: usize = RAW_FN_PTR_CAST_EXPR as usize;

    }

    #[test]
    fn test_tuple_struct_field_expressions_can_be_constant() {

        const TUPLE_EXPR: (i32, bool) = (1, true);
        const FIELD_EXPR0: i32 = TUPLE_EXPR.0;

        struct S { x: i32 }
        const STRUCT_EXPR: S = S { x: 1 };
        const FIELD_EXPR1: i32 = STRUCT_EXPR.x;
    }

    #[test]
    fn test_deref_and_most_shared_borrow_expressions_can_be_constant() {

        const SHARED_BORROW_EXPR: &i32 = &1;
        const DEREFERENCE_EXPR: i32 = *SHARED_BORROW_EXPR;

        // PUZZLER: shared references to interior mutables CANNOT be constants
        // error: constants cannot refer to interior mutable data
        //    const BORROW_OF_INTERIOR_MUTABLE: &Cell<i32> = &Cell::new(1);
        //
        // BUT path expressions to interior mutables can be constants
        const INTERIOR_MUTABLE: Cell<i32> = Cell::new(1);
        //
        // constant expressions seem to be call-by-name, not call-by-value:
        // each use seems to result in a new instances due to inlining
        INTERIOR_MUTABLE.set(2);
        assert_eq!(INTERIOR_MUTABLE.get(), 1);
    }

    #[test]
    fn test_mutable_borrow_expressions_cannot_be_constant() {

        // mutable references CANNOT be constant
        // error: mutable references not allowed in the final value of constants
        //    const MUTABLE_BORROW_EXPR: &mut i32 = &mut 1;
    }

    #[test]
    fn test_if_let_match_expressions_can_be_constant() {

        const IF_EXPR: i32 = if true { 1 } else { 0 };
        const IF_LET_EXPR: i32 = if let (i, true) = (3, false) { i } else { 0 };
        const MATCH_EXPR: i32 = match !true { true => 1, _ => 0 };
    }

    #[test]
    fn test_terminating_loop_expressions_can_be_constant() {

        // terminating loops can be constant
        const LOOP_EXPR: () = loop { break; };
        const LABEL_BLOCK_EXPR: () = 'label: { break 'label; };
        const LOOP_IN_BLOCK_EXPR: () = {
            let mut i = 0;
            loop { if i < 2 { break; } else { i += 1; } }
        };

        // terminating while- and while-let-loops can be constant
        const PREDICATE_LOOP_EXPR: () = while false {};
        const PREDICATE_PATTERN_LOOP_EXPR: () =
            while let (i, true) = (1, false) {};
        const WHILE_LOOP_IN_BLOCK_EXPR: () = {
            let mut i = 0;
            while i < 2 { i += 1; }
        };

        // infinite loops are timed-out by the compiler
        // error: constant evaluation is taking a long time
        // note: this lint makes sure the compiler doesn't get stuck
        //       due to infinite loops in const eval.
        //     const LOOP_EXPR: () = loop {};
        //     const PREDICATE_LOOP_EXPR: () = while true {};
        //     const PREDICATE_PATTERN_LOOP_EXPR: () = while let _ = false {};
    }

    #[test]
    fn test_iterator_for_loop_expressions_cannot_be_constant() {

        // PUZZLER: terminating loops, while-loops can be used in constants
        //
        // https://doc.rust-lang.org/reference/const_eval.html
        //    The following expressions are constant expressions, so long as
        //    any operands are also constant expressions:
        //    [...]
        //    * loop, while and while let expressions.
        //    [...]
        //
        // BUT iterator-for-loops CANNOT be constant
        // error: `for` is not allowed in a `const`
        //    const ITERATOR_LOOP_EXPR: () = for i in 1..2 {};
        //    const ITERATOR_LOOP_EXPR: () = for i in [1; 2] {};
        //    const ITERATOR_LOOP_EXPR: () = { for i in [1; 2] {} };
        //    const ITERATOR_LOOP_EXPR: () = for i in [1; 2].iter() {};
        //
        // Answer: taking an iterator is a non-const operation

        // CANNOT use external iteration: taking an iterator is non-const
        // error: cannot call non-const fn `...::iter` in constants
        //    const ITERATOR_FOREACH_LOOP_EXPR: () =
        //        [1; 2].iter().for_each(|i| ());
    }

    #[test]
    fn test_simulated_for_loops_can_be_constant() {

        // HOWEVER, can simulate for-loops with an iterating variable
        const ITERATING_LOOP_WITHIN_BLOCK_EXPRESSION: () = {
            let a = [1; 2];
            let mut i = 0;
            while i < 2 { a[i]; i += 1; };
        };
    }

    #[test]
    fn test_constant_items_can_be_unnamed_allowing_multiple_definitions() {

        // "unnamed" constants may be defined multiple times within same block
        const _: () = ();              // inlined expression
        const _: ((), ()) = ((), ());  // inlined expression
        // error: the name `v` is defined multiple times
        //    const v: () = ();
        //    const v: () = ();

        // "unnamed" constants cannot be used
        // error: `_` can only be used on the left-hand side of an assignment
        //    let x = _;

        // const functions muste be named
        // error: expected identifier, found reserved identifier `_`
        //    const fn _() {}
    }

    #[test]
    fn test_constant_items_can_panic_failing_compilation() {

        // error: evaluation of constant value failed
        //    const PANIC: () = panic!();
        //    const DIV0: i32 = 1 / (1 - 1);
    }

    #[test]
    fn test_constant_items_can_have_nonconst_destructors() {

        // type with destructor
        struct SD();
        impl Drop for SD {
            fn drop(&mut self) {
                println!("* DROPPED: {:?}", self as *const SD);
                //panic!();
            }
        }

        // inline constructor call
        const ZERO_WITH_DESTRUCTOR: SD = SD {};

        // each use creates an instance, gets dropped at end of function
        let _ = ZERO_WITH_DESTRUCTOR;
        let _ = ZERO_WITH_DESTRUCTOR;

        // $ cargo test -- --nocapture
        // * DROPPED: 0x70000137665e
        // * DROPPED: 0x70000137665f
    }

    #[test]
    fn test_constant_functions_cannot_use_certain_types_anywhere() {

        // can have Copy, Drop, mutable, or heap-allocated arguments/returns
        const fn f0(a: i32) -> i32 { a }
        const fn f1(a: String) -> String { a }
        const fn f2(a: [i32; 2]) -> [i32; 2] { a }
        const fn f3(a: Box<i32>) -> Box<i32> { a }

        // can only use shared references in argument/return/body
        // error: mutable references are not allowed in constant functions
        //    const fn f4(a: &mut i32) -> &mut i32 { a }
        //    const fn f5() { let x: &mut i32 = &mut 1; () }
        const fn f6(a: &i32) -> &i32 { a }
        const fn f7(a: &i32) -> &i32 { let x = &1; a }
    }

    #[test]
    fn test_constant_functions_must_meet_most_restrictions_of_const_context() {

        // error: cannot call non-const fn `...` in constant functions
        //    const fn f0() -> i32 { rand::random::<i32>() }

        // error: cannot call non-const operator in constant functions
        //    const fn f1() -> i32 { [1; 2][..][0] }

        // error: allocations are not allowed in constant functions
        //    const fn f2() -> Vec<i32> { vec![1, 2] }
    }

    #[test]
    fn test_constant_functions_restrict_the_use_of_fp_numbers() {

        // can create/copy/return fp numbers but not much more
        const fn f0() -> f64 { 1.1 }
        const fn f1(a: f64) -> f64 { a }
        // error: floating point arithmetic is not allowed in const functions
        //    const fn f2(a: f64) -> bool { a < 0.0 }
        //    const fn f3(a: f64) -> f64 { a - 1.1 }
    }

    #[test]
    fn test_constant_functions_allow_for_const_generic_parameters() {

        const fn f0<const A: bool>() -> bool { !A }
        const fn f1<const A: i32>() -> i32 { A + 1 }
        const fn f2<const A: char>() -> Option<u32> { A.to_digit(10) }

        // standard restrictions of const generic parameters apply:
        // https://doc.rust-lang.org/reference/items/generics.html#const-generics
        //
        // error: can't use generic parameters from outer item
        //    const fn f3<const A: usize>() { const N: usize = A; }
        //
        const fn f4<const A: usize>() -> [usize; A] { [A; A] }
        const fn f5<const A: usize>() -> [usize; A] { [A + 1; A] }
        // error: generic parameters may not be used in const operations
        //    const fn f6<const A: usize>() -> [usize; A] { [A; A + 0] }
        // ...
    }
}
