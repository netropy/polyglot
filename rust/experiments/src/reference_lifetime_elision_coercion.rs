#![allow(unused)]

// ======================================================================
// Summary, Code Examples: Static Items & Lifetime
//
// XXX TODO summarize
//
// + Lifetime Elision:
//   + compiler can infer lifetime annotation for common patterns
//   + no need for <'a>(&'a) annotation for input + pass-through parameters
//
// + Reference Lifetime Coercion:
//   + can return reference with shorter reference lifetime of other argument
//
// + Borrow Checker may not flag "does not live long enough" without a deref
//
// https://doc.rust-lang.org/reference/const_eval.html
//
// https://doc.rust-lang.org/reference/items/constant-items.html
// https://doc.rust-lang.org/reference/items/static-items.html
// https://doc.rust-lang.org/std/keyword.const.html
// https://doc.rust-lang.org/std/keyword.static.html
//
// https://doc.rust-lang.org/rust-by-example/scope/lifetime.html
// https://doc.rust-lang.org/rust-by-example/scope/lifetime/static_lifetime.html
// https://doc.rust-lang.org/rust-by-example/custom_types/constants.html
// https://doc.rust-lang.org/rust-by-example/scope/lifetime/lifetime_coercion.html
// https://doc.rust-lang.org/rust-by-example/scope/lifetime/explicit.html
// https://doc.rust-lang.org/rust-by-example/scope/lifetime/elision.html
//
// https://doc.rust-lang.org/book/ch03-01-variables-and-mutability.html
//
// ======================================================================

// ----------------------------------------------------------------------
// Lifetime Elision: Lifetime Annotation Inferred for Common Patterns
// ----------------------------------------------------------------------

// takes a reference: identical signatures, lifetime inferred by compiler
fn elided_input(x: &i32) { x; }
fn annotated_input<'a>(x: &'a i32) { x; }

// passes a reference: identical signatures, lifetime inferred by compiler
fn elided_pass(x: &i32) -> &i32 { x }
fn annotated_pass<'a>(x: &'a i32) -> &'a i32 { x }

// ----------------------------------------------------------------------
// Explicit Coercion of Reference Lifetime
// ----------------------------------------------------------------------

// coerces reference `s` to the shorter reference lifetime of `t`
//
// lifetime bound <'a: 'b, 'b> means: lifetime 'a is at least as long as 'b
//
fn coerce_lifetime<'a: 'b, 'b, T>(s: &'a T, t: &'b T) -> &'b T {
    s
}

// coerces static reference `s` to the shorter reference lifetime of `t`
//
// equivalent signature: <'a: 'static, 'b, T>(s: &'a T, t: &'b T) -> &'b T
//
fn coerce_static_lifetime<'a, T>(s: &'static T, t: &'a T) -> &'a T {
    coerce_lifetime(s, t)
}

// ensures the passed reference has static lifetime
fn ensure_static_reference_lifetime<T: ?Sized>(t: &'static T) {}


#[cfg(test)]
mod tests {
    use super::*;

    // XXX TODO test:
    //fn elided_input(x: &i32) { x; }
    //fn annotated_input<'a>(x: &'a i32) { x; }
    //fn elided_pass(x: &i32) -> &i32 { x }
    //fn annotated_pass<'a>(x: &'a i32) -> &'a i32 { x }

    #[test]
    fn test_coerce_longer_to_shorter_reference_lifetime() {

        // block-local lifetime
        let i = 2;
        let mut ri = &i;
        {
            // block-local lifetime
            let j = 3;
            let mut rj = &j;

            // XXX fix comments: longer to shorter liefetime
            // can assign &i given its outer-block lifetime
            ri = &i;
            rj = &i;
            rj = &j;

            // XXX positive test for coerce_lifetime()
            //let rsi3: &i32 = coerce_lifetime(rsi, &i);  // same

            // CANNOT assign &j given its inner-block lifetime
            // error: `j` does not live long enough
            //    ri = &j;

            // CANNOT assign &i with the shorter, inner-block lifetime of &j
            // error: `j` does not live long enough
            //    ri = coerce_lifetime(&i, &j);
        }
        assert_eq!(*ri, 2);  // borrow checker needs this deref
    }

    #[test]
    fn test_coerce_static_to_shorter_reference_lifetime() {

        // block-local lifetime
        let i = 2;

        // static lifetime
        let rsi: &'static i32 = &3;
        ensure_static_reference_lifetime(rsi);

        // new binding preserves existing ('static) lifetime
        let rsi1: &i32 = rsi;
        ensure_static_reference_lifetime(rsi1);

        // can coerce 'static to block-local lifetime of `i`
        let rsi2: &i32 = coerce_static_lifetime(rsi, &i);
        // error: `i` does not live long enough
        //    ensure_static_reference_lifetime(rsi2);

        // CANNOT coerce block-local lifetime of rsi2 to 'static
        // error: `i` does not live long enough
        //        argument requires that `i` is borrowed for `'static`
        //    let rsi3: &i32 = coerce_static_lifetime(rsi2, rsi);
    }
}
