#![allow(unused)]

// ======================================================================
// Code Surprise: `let` binding gets parsed as pattern depending on context
//
// thought that this was the motivation for having `if let` statements:
// https://doc.rust-lang.org/rust-by-example/flow_control/if_let.html
// ======================================================================

// existence of this module scope somewhow makes a difference...
#[cfg(test)]
mod tests0 {

    struct S0 { i: i32 }

    // declaring these constants here (lower/upper case doesn't matter) ...
    const s00: S0 = S0 { i: 1 };
    const S00: S0 = S0 { i: 1 };

    #[test]
    fn test_fails_to_compile() {

        // ... makes the `let` statements here be parsed as a pattern:
        //
        // error: to use a constant of type `S0` in a pattern, `S0` must be
        // annotated with `#[derive(PartialEq, Eq)]`
        // = note: the traits must be derived, manual `impl`s are not sufficient
        // = note: see https://doc.rust-lang.org/stable/std/marker/trait.StructuralEq.html for details
        let s00: S0 = S0 { i: 1 };
        let S00: S0 = S0 { i: 1 };
    }
}
