#![allow(unused)]

use static_assertions::*;

// ======================================================================
// Summary, Code Examples: Features/Limitations of Empty Types
//
// Unit Type, Unit/Empty Structs, Empty Enums, No Empty Unions, Never Type
//
// => nominal typing: struct/enum/union definitions denote different types
//
// "never" type: `!` (experimental, edition 2021)
// - unique empty/null type, cannot be instantiated
// - except for uniqueness: corresponds to empty union type, empty enum
//
// unit type: `()` = empty tuple type
// - unique singleton type with value `()`
// - is a proper 1st-class-citizen type,
// - is Eq, Ord, Clone, Debug
//
// unit/empty/tuple structs:
// - 3 notations: S; | S {} | S()
//     == unit struct | empty struct | empty tuple struct
// - can use the "struct literal syntax" to instantiate any of them:
//     let s: S = S {};  // for any S = S; | S {} | S()
// - nominal typing: defines distinct types (structurally equal)
// - by default, unit/empty/tuple structs are NOT Eq, Ord, Clone, Debug
// - unit/empty/tuple structs ~= unit type (structurally equal)
//
// empty enums: "zero-variant enums"
// - notation: enum E {}
// - nominal typing: defines distinct types (structurally equal)
// - CANNOT instantiate empty enum, as there's no value
// - hence, empty enums CANNOT be Eq, Ord, Clone, Debug
// - empty enums ~= "never" type (structurally equal)
//
// CANNOT define empty unions: union U {}
// - even with a field of "never" type
// - empty unions ~= empty enums (structurally equal)
//
// https://doc.rust-lang.org/std/primitive.unit.html
//
// https://doc.rust-lang.org/reference/items/enumerations.html
// https://doc.rust-lang.org/reference/items/structs.html
// https://doc.rust-lang.org/reference/items/unions.html
// https://doc.rust-lang.org/reference/types/tuple.html
// https://doc.rust-lang.org/book/ch03-02-data-types.html#the-tuple-type
// https://doc.rust-lang.org/book/ch05-00-structs.html
// https://doc.rust-lang.org/book/ch06-00-enums.html
// https://doc.rust-lang.org/rust-by-example/custom_types/enum.html
// https://doc.rust-lang.org/rust-by-example/custom_types/structs.html
// https://doc.rust-lang.org/rust-by-example/primitives/tuples.html
// ======================================================================

// can have unit type = empty/nullary tuple type
type V = ();

// can define empty structs = distinct types (nominal typing)
struct S0;     // unit struct <- preferred style
struct S1 {}   // empty struct, "struct literal syntax"
struct S2();   // empty tuple struct = empty struct

// can define empty enums = distinct types (nominal typing)
// there's no "unit enum" (unlike unit structs)
// error: expected `{}`, found `;`
//   enum E0;
enum E0 {}
enum E1 {}

// CANNOT define empty unions, also not with a field of "never" type
// error: unions cannot have zero fields
//   union U0 {}
// error: the `!` type is experimental
//   union U0 { value: ! }  // never

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_unit_type() {

        // unit type `()` is a singleton and is Eq, Ord
        let v: () = ();
        let w: () = {};
        assert!(v == w);
        assert!(() <= ());

        // unit type is a proper 1st-class-citizen type
        fn f(v: V) -> V { v }
        assert_eq!(f(v), ());

        // unit type is Clone, Debug
        assert_eq!((), ().clone());
        let s: String = format!("(v: {:?}", v.clone());
    }

    #[test]
    fn test_unit_empty_tuple_structs() {

        // can instantiate unit struct by unit struct or struct literal syntax
        let s0: S0 = S0;
        let s0: S0 = S0 {};
        // error: expected function, found struct
        //   let s0: S0 = S0();

        // MUST instantiate empty struct by struct literal syntax
        let s1: S1 = S1 {};
        // error: expected value, found struct
        //   let s1: S1 = S1;
        // error: expected function, tuple struct or tuple variant, found struct
        //   let s1: S1 = S1();

        // can instantiate tuple struct by tuple struct or struct literal syntax
        let s2: S2 = S2();
        let s2: S2 = S2 {};
        // error: mismatched types
        //   let s2: S2 = S2;

        // nominal typing: empty structs denote different types
        // error: mismatched types
        //   let s1: S1 = S0;
        //   let s2: S2 = S1 {};
        //   let s0: S0 = S2();
        assert_type_ne_all!(S0, S1, S2);

        // XXX TODO: use static asserts instead of compile errors
        // XXX TODO: apply to above, below

        // by default, unit/empty/tuple structs are NOT Eq, Ord
        // error: binary operation `==` cannot be applied to type `S0`
        // note: an implementation of `PartialEq` might be missing
        //   assert!(s0 == S0 {});
        //   assert!(s1 == S1 {});
        //   assert!(s2 == S2 {});
        // note: an implementation of `PartialOrd` might be missing
        //   assert!(s0 <= s0);
        //   assert!(s1 <= s1);
        //   assert!(s2 <= s2);

        // by default, unit/empty/tuple structs are NOT Clone, Debug
        // error: no method named `clone` found
        //   assert_eq!(s0, s0.clone());
        //   assert_eq!(s1, s1.clone());
        //   assert_eq!(s2, s2.clone());
        // error: `S0` doesn't implement `Debug`
        //   let s: String = format!("(s0: {:?}", s0);
        //   let s: String = format!("(s1: {:?}", s1);
        //   let s: String = format!("(s2: {:?}", s2);
    }

    #[test]
    fn test_empty_enum() {

        // empty enum is a proper type
        fn f(e: &E0) {}

        // nominal typing: empty enums denote different types
        assert_type_ne_all!(E0, E1);

        // CANNOT instantiate empty enum as there are no values
        // "zero-variant enums" behave like "never" type
        // error: expected identifier, found `;`
        //   let e0: E0 = E0::;
        // error: expected value, found enum `E0`
        //   let e0: E0 = E0;

        // hence, empty enums cannot be Eq, Ord, Clone, Debug

    }
}
