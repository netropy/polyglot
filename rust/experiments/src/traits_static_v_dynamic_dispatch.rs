#![allow(unused)]

// ======================================================================
// Summary, Code Examples: Detailed Features/Limitations of Traits
//
// TOC:
// * Supported Trait Features (Members, Associated Items)
// * Traits Supporting Dynamic Dispatch If Object-Safe
// * Traits Limited to Static Dispatch
// ======================================================================

// ----------------------------------------------------------------------
// Supported Trait Features (Members, Associated Items)
// ----------------------------------------------------------------------

trait NoDataMembers {
    // traits CANNOT have data members
    // error: expected one of `!` or `::`
    //   field: isize;
}

trait WithConstants {
    // can have abstract ("non-default") constants:
    const CONSTANT0: i32;

    // can have defaulted constants:
    const CONSTANT1: i32 = 0;

    // can have Self type constant -- BUT WHAT'S THE SEMANTICS?  What value?
    // const CONSTANT2: Self;

    // CANNOT have Self value constant:
    // error: expected value, found module `self`,
    //   `self` keyword only available in methods with a `self` parameter
    //   const CONSTANT3: Self =  self;
}

trait WithFunctions {
    // can have functions (non-methods):
    fn foo() {}
    fn bar(i: i32) -> i32 { i }
}

trait WithTypeMembers {
    // can have abstract type members:
    //
    // HOWEVER: requires to specify the type argument twice:
    //     impl WithTypeMembers for ... {
    //         type A = i32;
    //     }
    //
    // and usage:
    //     let obj: Box<dyn TraitWithTypeMembers<A = i32>> = Box::new(...);
    //
    type A;

    // CANNOT have defaulted type members:
    // error: associated type defaults are unstable
    //   type I = i32;
}

trait WithAbstractMethods {
    // can have abstract methods with receiver of reference/pointer Self type:
    fn by_ref(&self, i: i32) -> i32;
    fn by_ref_mut(&mut self, i: i32) -> i32;
    fn by_box(self: Box<Self>, i: i32) -> i32;  // also: Rc, Arc, Pin
}

trait WithDefaultMethods {
    // can have defaulted methods with receiver of reference/pointer Self type:
    fn by_ref_default(&self, i: i32) -> i32 { i }
    fn by_ref_mut_default(&mut self, i: i32) -> i32 { i }
    fn by_box_default(self: Box<Self>, i: i32) -> i32 { i }
}

trait WithMethodsWithSelfTypeParameters {
    // can have methods with parameters of reference/pointer Self type:
    fn by_ref_param(&self, o: &Self) {}
    fn by_ref_mut_param(&mut self, o: &mut Self) {}
    fn by_box_param(self: Box<Self>, o: Box<Self>) {}
}

trait WithMethodsWithSelfTypeReturns {
    // can have methods returning reference/pointer Self type:
    fn by_ref_return(&self) -> &Self { self }
    fn by_ref_mut_return(&mut self) -> &mut Self { self }
    fn by_box_return(self: Box<Self>) -> Box<Self> { self }
}

trait WithMethodsWithValueSelfTypeReceiverParamsReturn
    where Self: Sized  // type marker
{
    // can have receiver, parameters, return of sized, value/move Self type:
    fn take_by_value(self) {}
    fn return_by_value(self) -> Self { self }
    fn take_other_by_value(self, other: Self) -> Self { other }
}

trait WithGenericMethods {
    fn with_ref_generic_param<T>(&self, t: &T) -> i32 { 5 }
    fn with_ref_mut_generic_param<T>(&self, t: &mut T) -> i32 { 5 }
    fn with_box_generic_param<T>(&self, t: Box<T>) -> i32 { 5 }
    fn with_value_generic_param<T>(&self, t: T) -> i32 { 5 }
}

trait WithGenericTraitBoundedMethods {
    fn with_ref_generic_bounded_param<T: Copy>(&self, t: &T) -> T {
        t.clone()
    }
    fn with_ref_mut_generic_bounded_param<T: Copy>(&self, t: &mut T) -> T {
        t.clone()
    }
    fn with_box_generic_bounded_param<T: Copy>(&self, t: Box<T>) -> T {
        *t.clone()
    }
    fn with_value_generic_bounded_param<T: Copy>(&self, t: T) -> T {
        t.clone()
    }
}

// ----------------------------------------------------------------------
// Traits Supporting Dynamic Dispatch If Object-Safe
//
// feature restrictions:
// - traits must be OBJECT-SAFE (no sized Self type, ...)
// - methods must be DISPATCHABLE (no generic methods, ...)
// + the trait itself may be generic:
//   -> see ./generics_traits_static_v_dynamic_dispatch.rs
//
// Otherwise, error: needs to allow building a vtable...
// see https://doc.rust-lang.org/reference/items/traits.html
// ----------------------------------------------------------------------

// can form trait objects: can call members via references/pointers to trait
trait DynDispatchable:

    // can have methods with receiver of reference/pointer Self type:
    WithAbstractMethods + WithDefaultMethods
    //
    // These features CANNOT be used with Dynamic Dispatch:
    //
    // CANNOT have constants:
    // + WithConstants
    //
    // CANNOT have functions (non-methods):
    // + WithFunctions
    //
    // IMPRACTICAL to have type members (see comments in trait definition)
    // + WithTypeMembers
    //
    // CANNOT have methods with parameters of reference/pointer Self type:
    // + WithMethodsWithSelfTypeParameters
    //
    // CANNOT have methods returning reference/pointer Self type:
    // + WithMethodsWithSelfTypeReturns
    //
    // CANNOT have receiver, parameters, return of sized, value/move Self type:
    // + WithMethodsWithValueSelfTypeReceiverParamsReturn
    //
    // CANNOT have generic methods:
    // + WithGenericMethods
    //
    // CANNOT have generic methods with a bounded type parameter:
    // + WithGenericTraitBoundedMethods
    //
    // CANNOT require fixed sized type:
    // where Self: Sized
{}

// can have these features/supertraits with dynamic dispatch:
struct S2;

impl DynDispatchable for S2 {}

impl WithAbstractMethods for S2 {
    // MUST define all abstract ("non-default") methods:
    fn by_ref(&self, i: i32) -> i32 { -1 }
    fn by_ref_mut(&mut self, i: i32) -> i32 { -1 }
    fn by_box(self: Box<Self>, i: i32) -> i32 { -1 }  // also: Rc, Arc, Pin
}

impl WithDefaultMethods for S2 {}

// CANNOT have any of these features/supertraits with dynamic dispatch:
//
// impl WithConstants for S2 { const CONSTANT0: i32 = 1; }
// impl WithFunctions for S2 {}
// impl WithTypeMembers for S2 { type A = i32; }
// impl WithMethodsWithSelfTypeParameters for S2 {}
// impl WithMethodsWithSelfTypeReturns for S2 {}
// impl WithMethodsWithValueSelfTypeReceiverParamsReturn for S2 {}
// impl WithGenericMethods for S2 {}
// impl WithGenericTraitBoundedMethods for S2 {}

// a different implementation:
struct S3;

impl DynDispatchable for S3 {}

impl WithAbstractMethods for S3 {
    // MUST define all abstract ("non-default") methods:
    fn by_ref(&self, i: i32) -> i32 { 1 }
    fn by_ref_mut(&mut self, i: i32) -> i32 { 1 }
    fn by_box(self: Box<Self>, i: i32) -> i32 { 1 }  // also: Rc, Arc, Pin
}

impl WithDefaultMethods for S3 {}

#[cfg(test)]
mod tests_object_safe {
    use super::*;

    // rand not in std lib
    // add to [dependencies] in Cargo.toml: $ cargo add rand
    use rand::prelude::*;

    #[test]
    fn test_static_dispatch_methods() {

        // can declare _values_ of structs:
        let s2: S2 = S2 {};
        let s3: S3 = S3 {};
        assert_eq!(s2.by_ref(0), -1);
        assert_eq!(s3.by_ref(0), 1);

        // can define _references/pointers_ to struct values:
        let r0: &S2 = &s2;
        let r1: &mut S2 = &mut S2 {};
        let r2: Box<S2> = Box::new(S2);

        let r3: &S3 = &s3;
        let r4: &mut S3 = &mut S3 {};
        let r5: Box<S3> = Box::new(S3);

        // can call methods on _reference/pointer_ to struct value:
        assert_eq!(r0.by_ref(0), -1);
        assert_eq!(r1.by_ref_mut(0), -1);
        assert_eq!(r2.by_box(0), -1);  // moves

        assert_eq!(r3.by_ref(0), 1);
        assert_eq!(r4.by_ref_mut(0), 1);
        assert_eq!(r5.by_box(0), 1);  // moves

        // can call methods with _reference/pointer_ to struct value:
        assert_eq!(S2::by_ref(&S2 {}, 0), -1);
        assert_eq!(S2::by_ref_mut(&mut S2 {}, 0), -1);
        assert_eq!(S2::by_box(Box::new(S2), 0), -1);

        assert_eq!(S3::by_ref(&S3 {}, 0), 1);
        assert_eq!(S3::by_ref_mut(&mut S3 {}, 0), 1);
        assert_eq!(S3::by_box(Box::new(S3), 0), 1);
    }

    #[test]
    fn test_static_dispatch_default_methods() {

        // can define _references/pointers_ to struct values:
        let r0: &S2 = &S2 {};
        let r1: &mut S2 = &mut S2 {};
        let r2: Box<S2> = Box::new(S2);

        let r3: &S3 = &S3 {};
        let r4: &mut S3 = &mut S3 {};
        let r5: Box<S3> = Box::new(S3);

        // can call methods on _reference/pointer_ to struct value:
        assert_eq!(r0.by_ref_default(0), 0);
        assert_eq!(r1.by_ref_mut_default(0), 0);
        assert_eq!(r2.by_box_default(0), 0);  // moves

        assert_eq!(r3.by_ref_default(0), 0);
        assert_eq!(r4.by_ref_mut_default(0), 0);
        assert_eq!(r5.by_box_default(0), 0);  // moves
    }

    #[test]
    fn test_dynamic_dispatch_methods_on_trait_objects() {

        // CANNOT declare _values_ of trait objects (dyn ...):
        // error: s2/s3 doesn't have a size known at compile-time
        // error: mismatched types, expected trait object, found struct
        //   let s2: dyn DynDispatchable = S2 {};
        //   let s3: dyn DynDispatchable = S3 {};

        // can define _references/pointers_ to trait objects:
        let r0: &dyn DynDispatchable = &S2 {};
        let r1: &mut dyn DynDispatchable = &mut S2 {};
        let r2: Box<dyn DynDispatchable> = Box::new(S2);

        let r3: &dyn DynDispatchable = &S3 {};
        let r4: &mut dyn DynDispatchable = &mut S3 {};
        let r5: Box<dyn DynDispatchable> = Box::new(S3);

        // can call methods on _reference/pointer_ to trait objects:
        assert_eq!(r0.by_ref(0), -1);
        assert_eq!(r1.by_ref_mut(0), -1);
        assert_eq!(r2.by_box(0), -1);  // moves

        assert_eq!(r3.by_ref(0), 1);
        assert_eq!(r4.by_ref_mut(0), 1);
        assert_eq!(r5.by_box(0), 1);  // moves
    }

    #[test]
    fn test_dynamic_dispatch_methods_on_runtime_trait_objects() {

        let r20: &dyn DynDispatchable = &S2 {};
        let r21: &mut dyn DynDispatchable = &mut S2 {};

        let r30: &dyn DynDispatchable = &S3 {};
        let r31: &mut dyn DynDispatchable = &mut S3 {};

        // prove: select method at runtime
        for n in 0..10 {
            let b: bool = random();

            let r1: &dyn DynDispatchable =
                // lifetime ok:
                // if b { &S2 {} } else { &S3 {} };
                if b { r20 } else { r30 };
            let r2: &mut dyn DynDispatchable =
                // error: temporary value freed at the end of this statement
                //   if b { &mut S2 {} } else { &mut S3 {} };
                //   match b { true => &mut S2 {}, _ => &mut S3 {} };
                match b { true => r21, _ => r31 };
            let r3: Box<dyn DynDispatchable> =
                if b { Box::new(S2) } else { Box::new(S3) };

            assert_eq!(r1.by_ref(0), if b { -1 } else { 1 });
            assert_eq!(r2.by_ref_mut(0), if b { -1 } else { 1 });
            assert_eq!(r3.by_box(0), if b { -1 } else { 1 });  // moves
        }
    }

}

// ----------------------------------------------------------------------
// Traits Limited to Static Dispatch
//
// no feature restrictions:
// + trait may not be OBJECT-SAFE, methods may not be DISPATCHABLE
// + the trait itself may be generic:
//   -> see ./generics_traits_static_v_dynamic_dispatch.rs
//
// see https://doc.rust-lang.org/reference/items/traits.html
// ----------------------------------------------------------------------

// CANNOT form trait objects: must call members on struct values
trait StaticDispatchOnly:

    // can have methods with receiver of reference/pointer Self type:
    WithAbstractMethods + WithDefaultMethods
    //
    // can have constants:
    + WithConstants
    //
    // can have functions (non-methods):
    + WithFunctions
    //
    // IMPRACTICAL to have type members (see comments in trait definition):
    + WithTypeMembers
    //
    // can have methods with parameters of reference/pointer Self type:
    + WithMethodsWithSelfTypeParameters
    //
    // can have methods returning reference/pointer Self type:
    + WithMethodsWithSelfTypeReturns
    //
    // can have receiver, parameters, return of sized, value/move Self type:
    + WithMethodsWithValueSelfTypeReceiverParamsReturn
    //
    // can have generic methods:
    + WithGenericMethods
    //
    // can have generic methods with a bounded type parameter:
    + WithGenericTraitBoundedMethods
    //
    // can require fixed sized type (and other traits):
    where Self: Sized
{}

// can have any of these features/supertraits with static dispatch:
struct S4;

impl StaticDispatchOnly for S4 {}

impl WithAbstractMethods for S4 {
    // MUST define all abstract ("non-default") methods:
    fn by_ref(&self, i: i32) -> i32 { -1 }
    fn by_ref_mut(&mut self, i: i32) -> i32 { -1 }
    fn by_box(self: Box<Self>, i: i32) -> i32 { -1 }  // also: Rc, Arc, Pin
}

impl WithDefaultMethods for S4 {}
impl WithConstants for S4 { const CONSTANT0: i32 = -1; }
impl WithFunctions for S4 {}
impl WithTypeMembers for S4 { type A = bool; }
impl WithMethodsWithSelfTypeParameters for S4 {}
impl WithMethodsWithSelfTypeReturns for S4 {}
impl WithMethodsWithValueSelfTypeReceiverParamsReturn for S4 {}
impl WithGenericMethods for S4 {}
impl WithGenericTraitBoundedMethods for S4 {}

// can have any of these features/supertraits with static dispatch:
struct S5;

impl StaticDispatchOnly for S5 {}

impl WithAbstractMethods for S5 {
    // MUST define all abstract ("non-default") methods:
    fn by_ref(&self, i: i32) -> i32 { 1 }
    fn by_ref_mut(&mut self, i: i32) -> i32 { 1 }
    fn by_box(self: Box<Self>, i: i32) -> i32 { 1 }  // also: Rc, Arc, Pin
}

impl WithDefaultMethods for S5 {}
impl WithConstants for S5 { const CONSTANT0: i32 = 1; }
impl WithFunctions for S5 {}
impl WithTypeMembers for S5 { type A = i32; }
impl WithMethodsWithSelfTypeParameters for S5 {}
impl WithMethodsWithSelfTypeReturns for S5 {}
impl WithMethodsWithValueSelfTypeReceiverParamsReturn for S5 {}
impl WithGenericMethods for S5 {}
impl WithGenericTraitBoundedMethods for S5 {}

#[cfg(test)]
mod tests_non_object_safe {
    use super::*;

    // rand not in std lib
    // add to [dependencies] in Cargo.toml: $ cargo add rand
    use rand::prelude::*;

    #[test]
    fn test_no_trait_objects() {

        // CANNOT declare _values_ of trait objects (dyn ...):
        // error: s4/s5 doesn't have a size known at compile-time
        // error: mismatched types, expected trait object, found struct
        //   let s4: dyn StaticDispatchOnly = S4 {};
        //   let s5: dyn StaticDispatchOnly = S5 {};

        // CANNOT define _references/pointers_ to trait objects:
        // error: cannot be made into an object, trait not "object safe"
        //   let r0: &dyn StaticDispatchOnly = &S4 {};
        //   let r1: &mut dyn StaticDispatchOnly = &mut S4 {};
        //   let r2: Box<dyn StaticDispatchOnly> = Box::new(S4);
        //   let r3: &dyn StaticDispatchOnly = &S5 {};
        //   let r4: &mut dyn StaticDispatchOnly = &mut S5 {};
        //   let r5: Box<dyn StaticDispatchOnly> = Box::new(S5);
    }

    #[test]
    fn test_static_dispatch_methods() {

        // can declare _values_ of structs:
        let s4: S4 = S4 {};
        let s5: S5 = S5 {};
        assert_eq!(s4.by_ref(0), -1);
        assert_eq!(s5.by_ref(0), 1);

        // can define _references/pointers_ to struct values:
        let r0: &S4 = &s4;
        let r1: &mut S4 = &mut S4 {};
        let r2: Box<S4> = Box::new(S4);

        let r3: &S5 = &s5;
        let r4: &mut S5 = &mut S5 {};
        let r5: Box<S5> = Box::new(S5);

        // can call methods on _reference/pointer_ to struct value:
        assert_eq!(r0.by_ref(0), -1);
        assert_eq!(r1.by_ref_mut(0), -1);
        assert_eq!(r2.by_box(0), -1);

        assert_eq!(r3.by_ref(0), 1);
        assert_eq!(r4.by_ref_mut(0), 1);
        assert_eq!(r5.by_box(0), 1);

        // can call methods with _reference/pointer_ to struct value:
        assert_eq!(S4::by_ref(&S4 {}, 0), -1);
        assert_eq!(S4::by_ref_mut(&mut S4 {}, 0), -1);
        assert_eq!(S4::by_box(Box::new(S4), 0), -1);

        assert_eq!(S5::by_ref(&S5 {}, 0), 1);
        assert_eq!(S5::by_ref_mut(&mut S5 {}, 0), 1);
        assert_eq!(S5::by_box(Box::new(S5), 0), 1);
    }

    #[test]
    fn test_static_dispatch_default_methods() {

        // can define _references/pointers_ to struct values:
        let r0: &S4 = &S4 {};
        let r1: &mut S4 = &mut S4 {};
        let r2: Box<S4> = Box::new(S4);

        let r3: &S5 = &S5 {};
        let r4: &mut S5 = &mut S5 {};
        let r5: Box<S5> = Box::new(S5);

        // can call methods on _reference/pointer_ to struct value:
        assert_eq!(r0.by_ref_default(0), 0);
        assert_eq!(r1.by_ref_mut_default(0), 0);
        assert_eq!(r2.by_box_default(0), 0);  // moves

        assert_eq!(r3.by_ref_default(0), 0);
        assert_eq!(r4.by_ref_mut_default(0), 0);
        assert_eq!(r5.by_box_default(0), 0);  // moves
    }

    #[test]
    fn test_static_access_of_struct_constants() {

        // CANNOT access constants from struct values:
        // let r0: &S4 = &S4 {};
        // let r1: &S5 = &S5 {};
        // assert_eq!(r0.CONSTANT0, -1);
        // assert_eq!(r1.CONSTANT0, 1);

        // can access constants from struct type:
        assert_eq!(S4::CONSTANT0, -1);
        assert_eq!(S5::CONSTANT0, 1);

        assert_eq!(S4::CONSTANT1, 0);
        assert_eq!(S5::CONSTANT1, 0);
    }

    #[test]
    fn test_static_access_of_struct_functions() {

        // CANNOT call functions from struct values:
        // let r0: &S4 = &S4 {};
        // let r1: &S5 = &S5 {};
        // assert_eq!(r0.foo(), ());
        // assert_eq!(r1.foo(), ());
        // assert_eq!(r0.bar(2), 2);
        // assert_eq!(r1.bar(2), 2);

        // can call functions from struct type:
        assert_eq!(S4::foo(), ());
        assert_eq!(S5::foo(), ());
        assert_eq!(S4::bar(2), 2);
        assert_eq!(S5::bar(2), 2);
    }

    #[test]
    fn test_static_access_of_struct_type_members() {

        // CANNOT access type member of struct value:
        // let r4: r0.A = true;
        // let r5: r1.A = 3;
        // ==> No Scala-equivalent of PATH-DEPENDENT TYPES!

        // can access type member on struct type if _fully qualified_:
        let r0: <S4 as WithTypeMembers>::A = true;
        let r1: <S5 as WithTypeMembers>::A = 3;

        // CANNOT access _abstract_ type member on struct:
        // error: ambiguous associated type
        //   let r3: WithTypeMembers::A = 3;

        // NO SUPPORT for _defaulted_ type members on struct:
        // error: associated type defaults are unstable
        //   let r4: WithTypeMembers::I = 3;
    }

    #[test]
    fn test_static_dispatch_methods_with_self_type_params() {

        let r0: &S4 = &S4 {};
        let r1: &mut S4 = &mut S4 {};
        let r2: Box<S4> = Box::new(S4);

        let r3: &S5 = &S5 {};
        let r4: &mut S5 = &mut S5 {};
        let r5: Box<S5> = Box::new(S5);

        assert_eq!(r0.by_ref_param(r0), ());
        assert_eq!(r1.by_ref_mut_param(&mut S4 {}), ());  // borrows mutable
        assert_eq!(r2.by_box_param(Box::new(S4)), ());    // moves

        assert_eq!(r3.by_ref_param(r3), ());
        assert_eq!(r4.by_ref_mut_param(&mut S5 {}), ());  // borrows mutable
        assert_eq!(r5.by_box_param(Box::new(S5)), ());    // moves
    }

    #[test]
    fn test_static_dispatch_methods_with_self_type_returns() {

        let r0: &S4 = &S4 {};
        let r1: &mut S4 = &mut S4 {};
        let r2: Box<S4> = Box::new(S4);

        let r3: &S5 = &S5 {};
        let r4: &mut S5 = &mut S5 {};
        let r5: Box<S5> = Box::new(S5);

        // want to compare references as pointers:
        assert_eq!(r0.by_ref_return() as *const S4, r0 as *const S4);
        assert_eq!(r1.by_ref_mut_return() as *mut S4, r1 as *mut S4);
        // assert_eq!(r2.by_box_return() as *mut S4, r2 as *mut S4);  // moves

        assert_eq!(r3.by_ref_return() as *const S5, r3 as *const S5);
        assert_eq!(r4.by_ref_mut_return() as *mut S5, r4 as *mut S5);
        // assert_eq!(r5.by_box_return() as *mut S5, r5 as *mut S5);  // moves
    }

    #[test]
    fn test_static_dispatch_methods_with_value_self_type() {

        // no Eq defined on structs S4, S5:
        S4 {}.take_by_value();  // moves
        S4 {}.return_by_value();  // moves
        S4 {}.take_other_by_value(S4 {});  // moves
    }

    #[test]
    fn test_static_dispatch_generic_methods() {

        let s4: &S4 = &S4 {};

        // with explicit type specialization using turbofish ::<...>
        assert_eq!(s4.with_ref_generic_param::<i32>(&4), 5);
        assert_eq!(s4.with_ref_mut_generic_param::<i32>(&mut 4), 5);
        assert_eq!(s4.with_box_generic_param::<i32>(Box::new(4)), 5);
        assert_eq!(s4.with_value_generic_param::<i32>(4), 5);

        // with type inference: <i32>
        assert_eq!(s4.with_ref_generic_param(&4), 5);
        assert_eq!(s4.with_ref_mut_generic_param(&mut 4), 5);
        assert_eq!(s4.with_box_generic_param(Box::new(4)), 5);
        assert_eq!(s4.with_value_generic_param(4), 5);

        // with type inference: <bool>
        assert_eq!(s4.with_ref_generic_param(&true), 5);
        assert_eq!(s4.with_ref_mut_generic_param(&mut true), 5);
        assert_eq!(s4.with_box_generic_param(Box::new(true)), 5);
        assert_eq!(s4.with_value_generic_param(true), 5);
    }

    #[test]
    fn test_static_dispatch_generic_trait_bounded_methods() {

        let s4: &S4 = &S4 {};

        // with explicit type specialization using turbofish ::<...>
        assert_eq!(s4.with_ref_generic_bounded_param::<i32>(&4), 4);
        assert_eq!(s4.with_ref_mut_generic_bounded_param::<i32>(&mut 4), 4);
        assert_eq!(s4.with_box_generic_bounded_param::<i32>(Box::new(4)), 4);
        assert_eq!(s4.with_value_generic_bounded_param::<i32>(4), 4);

        // with type inference: <i32>
        assert_eq!(s4.with_ref_generic_bounded_param(&4), 4);
        assert_eq!(s4.with_ref_mut_generic_bounded_param(&mut 4), 4);
        assert_eq!(s4.with_box_generic_bounded_param(Box::new(4)), 4);
        assert_eq!(s4.with_value_generic_bounded_param(4), 4);

        // with type inference: <bool>
        assert_eq!(s4.with_ref_generic_bounded_param(&true), true);
        assert_eq!(s4.with_ref_mut_generic_bounded_param(&mut true), true);
        assert_eq!(s4.with_box_generic_bounded_param(Box::new(true)), true);
        assert_eq!(s4.with_value_generic_bounded_param(true), true);
    }
}
