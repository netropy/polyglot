#![allow(unused)]

use core::fmt::Debug;
use core::hash::Hash;
use static_assertions::*;
use std::iter::IntoIterator;
use std::rc::Rc;

// ----------------------------------------------------------------------
// XXX TODO: add str + String + CString
//
// https://stackoverflow.com/questions/30154541/how-do-i-concatenate-strings
//
// https://doc.rust-lang.org/reference/types/textual.html
// https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html#the-string-type
// https://doc.rust-lang.org/book/ch04-03-slices.html#string-slices
// https://doc.rust-lang.org/book/ch08-02-strings.html#storing-utf-8-encoded-text-with-strings
// https://doc.rust-lang.org/rust-by-example/conversion/string.html
// https://doc.rust-lang.org/rust-by-example/std/str.html
// ----------------------------------------------------------------------

// ======================================================================
// Summary, Code Examples: Features/Limitations of Arrays, Slices, Vectors
//
// XXX TODO:
//
// + can have as "associated items" (members)
//     0..*      immutable elements (fields) of any (sized) type
//               or [im-]mutable fields of reference type
//     1 field:  MUST have trailing comma in type and value expressions
//
// + can access fields by index
// + can [partially] destructure/pattern-match: let, if-let, while-let, match
//
// + arrays receive "drop glue" code as needed (but are NOT Drop, by default)
// + arrays are Copy, Clone if all element types are (any length)
// + arrays are Hash, Eq, Ord if of same type, all fields are (<= 12 elements)
// + arrays are Debug if all element types are and <= 12 elements
// + arrays are reference and value IntoIterator (any length)
// + arrays are From<Tuple> IF element types allow (<=12 elements)
// + arrays are Default IF element type is (<=32 elements)
// + auto traits: arrays are Send, Sync, Unpin, ... if all element types are
//
// can have unchecked array and slice index access within UNSAFE
//
//
// arrays coerce to slices, providing most of the API for working with arrays
//
//
// - CANNOT have
//     mutable fields -> for RARELY USED internal mutability, see below
//     impl blocks, functions or methods -> use structs or array structs
//     associated constants or types -> use traits
//     subtyping or inheritance for/from arrays -> use traits
//
// https://doc.rust-lang.org/std/primitive.array.html
// https://doc.rust-lang.org/std/primitive.slice.html
// https://doc.rust-lang.org/std/slice/index.html
// https://doc.rust-lang.org/reference/dynamically-sized-types.html
// https://doc.rust-lang.org/reference/expressions/array-expr.html
// https://doc.rust-lang.org/reference/types/array.html
// https://doc.rust-lang.org/reference/types/slice.html
// https://doc.rust-lang.org/book/ch03-02-data-types.html#the-array-type
// https://doc.rust-lang.org/rust-by-example/primitives/array.html
//
// https://doc.rust-lang.org/std/vec/index.html
// https://doc.rust-lang.org/book/ch08-01-vectors.html
// https://doc.rust-lang.org/rust-by-example/std/vec.html
//
// https://doc.rust-lang.org/reference/patterns.html#slice-patterns
// https://doc.rust-lang.org/rust-by-example/flow_control/match/destructuring/destructure_slice.html
//
// https://doc.rust-lang.org/std/clone/trait.Clone.html
// https://doc.rust-lang.org/std/cmp/trait.Eq.html
// https://doc.rust-lang.org/std/cmp/trait.Ord.html
// https://doc.rust-lang.org/std/convert/trait.From.html
// https://doc.rust-lang.org/std/default/trait.Default.html
// https://doc.rust-lang.org/std/fmt/trait.Debug.html
// https://doc.rust-lang.org/std/hash/trait.Hash.html
// https://doc.rust-lang.org/std/iter/trait.IntoIterator.html
// https://doc.rust-lang.org/std/marker/trait.Copy.html
// https://doc.rust-lang.org/std/ops/trait.Drop.html
// https://doc.rust-lang.org/reference/special-types-and-traits.html
// ======================================================================

// arrays '[type; expr]' are fixed-sized sequence types of sized elements
// array types are defined structurally by element type (and length)
type A0 = [i32; 0];
type A1 = [i32; 1];
type A13 = [i32; 13];
type A33 = [i32; 33];
type AA0 = [[i32; 0]; 0];
assert_type_eq_all!(A0, [i32; 0]);
assert_type_ne_all!(A0, A1, A13, A33, AA0, [i64; 0]);

// n MUST be constant expression of type usize
// error: cannot call non-const fn `random::<usize>` in constants
//    type AR = [i32; rand::random::<usize>()];
// error: mismatched types, expected `usize`, found floating-point number
//    type AX = [i32; 1.0];

// slices '[type]' are unsized "view" types into a sequence of sized elements
// slice types are defined structurally by element type
type S = [i32];
type SA0 = [[i32; 0]];
assert_not_impl_any!(S: Sized);
assert_not_impl_any!(SA0: Sized);
assert_type_eq_all!(S, [i32]);
assert_type_ne_all!(S, A0, SA0, [i64]);

// slices always know their actual length at runtime: <slice>.len()
// a slice is a two-word object ("fat pointer"):
// - the 1st word is a pointer to the data
// - the 2nd word is the length of the slice as usize

// element types of arrays and slices MUST be sized
assert_impl_all!(A0: Sized);
assert_impl_all!(A33: Sized);
// error: size for values of type `[i32]` cannot be known at compilation time
//    assert_impl_all!([[i32]; 0]: Sized);
//    assert_not_impl_any!([[i32]; 0]: Sized);

// slices MUST be generally used through pointer types (to make them sized):
type SS<'a> = &'a [i32];      // shared slice, borrows the data
type SM<'a> = &'a mut [i32];  // mutable slice, borrows the data
type SB = Box<[i32]>;         // boxed slice, owns the data
type SR = Rc<[i32]>;          // ref-counted slice, owns the data
assert_impl_all!(SS: Sized);
assert_impl_all!(SM: Sized);
assert_impl_all!(SB: Sized);
assert_impl_all!(SR: Sized);
// a few places accept unsized slice types directly, like: <slice>.len()

// vectors std::vec::Vec<T> are sized, growable, heap-allocated array types
type V = Vec<i32>;

// element types of vectors MUST be sized
assert_impl_all!(V: Sized);
// error: size for values of type `[i32]` cannot be known at compilation time
//    assert_impl_all!(Vec<[i32]>: Sized);
//    assert_not_impl_any!(Vec<[i32]>: Sized);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_empty_arrays_slices_vectors() {

        // stack-allocated-content empty arrays, slices
        let a0: [(); 0] = [];          // array expression comma-separated list
        let a1: [i32; 0] = [];
        let a2: [i32; 0] = [1; 0];     // repeat expression with length operand
        // XXX where is  &[1; 0] allocated? static?
        let a3: &mut [i32; 0] = &mut [1; 0];
        let ss: &[i32] = &a2;          // borrows
        let sm: &mut [i32] = a3;  // borrows mutably

        // valid element type needed even for empty arrays
        // error: type annotations needed for `[_; 0]`
        //    let a4 = [];
        // error: cannot have Never as element type; `!` type is experimental
        //    let a5: [!; 0] = [];

        // NOTE: repeat expression is always evaluated, even with length = 0
        // panic: assertion failed
        //    let fail: [(); 0] = [assert!(false); 0];

        // heap-allocated-content empty arrays, slices
        let ab: Box<[i32; 0]> = Box::new([1; 0]);
        let ar: Rc<[i32; 0]> = Rc::new([1; 0]);
        let mut sb: Box<[i32]> = ab;  // moved to boxed slice, owns data
        let sr: Rc<[i32]> = ar;       // moved to ref-counted slice, owns data
        let ss: &[i32] = &sr;
        let sm: &mut [i32] = &mut sb;

        // heap-allocated-content empty vectors
        //
        let v0: Vec<i32> = Vec::new();  // constructor
        let v0: Vec<i32> = Vec::from([0; 0]);  // constructor
        let v0: Vec<i32> = Vec::with_capacity(0);  // constructor
        let v0: Vec<i32> = vec![];      // macro for use of array expressions
        let v0: Vec<i32> = vec![0; 0];

        // length of arrays, slices, vectors
        assert_eq!(a0.len(), 0);
        assert_eq!(ss.len(), 0);
        assert_eq!(v0.len(), 0);
    }

    #[test]
    fn test_unsized_slice_values() {

        // error: size for values of type `[i32]` not known at compilation time
        // help: unsized locals are gated as an unstable feature
        //    let s0: [i32] = [0; 0];       // error: mismatched types
        //    let s0: [i32] = [0; 0][..];
        // error: ...; the size of `[i32]` cannot be statically determined
        //    [0; 0][..];

        // exception: can pass unsized slice as &Self
        let l: usize = [0; 0][..].len();

        // can hold as sized pointers:
        let sr: &[i32] = &[0; 0];      // coerced to &slice
        let sr: &[i32] = &[0; 0][..];
        let smr: & mut[i32] = & mut[0; 0];
        let sbox: Box<[i32]> = Box::from([1; 0]);
        let src: Rc<[i32]> = Rc::from([1; 0]);
    }

    #[test]
    fn test_array_and_slice_expressions() {

        // by construction, elements of arrays/slices are always initialized
        // ArrayExpression  [ ArrayElements? ]
        // ArrayElements:
        //     Expression ( , Expression )* ,?
        //   | Expression ; Expression

        // can have trailing comma for 1-element tuple values
        let a1: A1 = [1];        // comma-separated list
        let a1: A1 = [1,];
        let a1: A1 = [1; 1];     // repeat expression with length operand

        // for length operands >1: element type MUST be Copy ...
        let a33: A33 = [1; 33];
        // error: the trait bound `String: Copy` is not satisfied
        //    ["".to_string(); 2];
        // ... or a constant value:
        const EMPTY: Vec<i32> = Vec::new();
        [EMPTY; 2];
        // error: the trait bound `Vec<_>: Copy` is not satisfied
        //    [Vec::new(); 2];

        // stack-allocated-content arrays, slices
        let a3: [i32; 3] = [1; 3];
        let ss: &[i32] = &[1; 3];                    // borrows
        let ss: &[i32] = [1; 3].as_slice();          // same
        let sm: &mut [i32] = &mut [1; 3];            // borrows mutably
        let sm: &mut [i32] = [1; 3].as_mut_slice();  // same

        // length of array, slice (arrays coerce to slices for most of the API)
        assert_eq!(a3.len(), 3);
        assert_eq!(ss.len(), 3);

        // heap-allocated-content arrays, slices
        let ab: Box<[i32; 3]> = Box::new([1; 3]);
        let mut sb: SB = ab;                // moved to a boxed slice
        let mut sb: SB = Box::new([1; 3]);  // coerced to boxed slice
        let sr: SR = Rc::new([1; 0]);       // coerced to ref-counted slice
        let ss: SS = &sr;                   // borrows
        let sm: SM = &mut sb;               // borrows mutably

        // ------------------------------------------------------------
        // XXX
        // https://doc.rust-lang.org/std/vec/struct.Vec.html#guarantees
        // Vec will never perform a “small optimization” where elements are actually stored on the stack for two reasons:

        // cannot IMPLICITLY move stack- into heap-allocated arrays, slices
        // error: mismatched types, expected `Box<[i32; 3]>`, found `[i32; 3]`
        //    let ab: Box<[i32; 3]> = a3;
        //    let ss: Box<[i32]> = a3;
        //
        // can EXPLICITLY move stack- into heap-allocated arrays, slices
        // using: impl<T, const N: usize> From<[T; N]> for Box<[T], Global>
        //        impl<T, const N: usize> From<[T; N]> for Rc<[T], Global>
        //        ...
        //
        // moves into boxed array (copies if type is Copy):
        let ac: [String; 2] = ["a".into(), "c".into()];
        let ab: Box<[String; 2]> = Box::from(ac);
        assert_eq!(ab.len(), 2);
        // error: borrow of moved value: `ac`
        //    assert_eq!(ac.len(), 2);
        //
        // moves into boxed slice (copies if type is Copy):
        let ac: [String; 2] = ["a".into(), "c".into()];
        let ss: Box<[String]> = Box::from(ac);
        assert_eq!(ss.len(), 2);
        // error: borrow of moved value: `ac`
        //    assert_eq!(ac.len(), 2);
    }

    #[test]
    fn test_array_slice_vector_index_expressions() {

        // index access is always bounds-checked within safe code:
        //   container[index]                        -> T, &T
        //   container[index]                        <- T
        //
        // desugars into:
        //   c[expr] ~> *std::ops::Index::index(&c, expr)
        //   c[expr] ~> *std::ops::IndexMut::index_mut(&c, expr)
        //   ...

        // arrays of Copy and Move (Clone) types:
        let acopy: [i32; 3] = [1; 3];
        let mut amcopy: [i32; 3] = acopy.clone();
        let aclone: [String; 3] = ["a".into(), "b".into(), "c".into()];
        let mut amclone: [String; 3] = aclone.clone();

        // slices of Copy and Move (Clone) types:
        let scopy: &[i32] = &acopy;
        let smcopy: &mut [i32] = &mut amcopy.clone();
        let sclone: &[String] = &aclone;
        let smclone: &mut [String] = &mut amclone.clone();

        // vectors of Copy and Move (Clone) types:
        let vcopy: Vec<i32> = vec![1; 3];
        let mut vmcopy: Vec<i32> = vcopy.clone();
        let vclone: Vec<String> = vec!["a".into(), "b".into(), "c".into()];
        let mut vmclone: Vec<String> = vclone.clone();

        // array[index]:
        acopy[0];        // copy
        &acopy[0];       // ref
        amcopy[0] = 0;   // copy
        // error: cannot move out of type `[String; 3]`, a non-copy array
        //    aclone[0];                 // move
        &aclone[0];                      // ref
        amclone[0] = aclone[0].clone();  // move, hence MUST clone

        // compile-time check of constant array index access
        // error: this operation will panic at runtime, index out of bounds
        //    acopy[3];
        //    amcopy[3] = 0;
        // runtime check of dynamic array index access
        // panic: index out of bounds
        //    acopy[rand::random::<usize>()];

        // slice[index]:
        scopy[0];        // copy
        &scopy[0];       // ref
        smcopy[0] = 0;   // copy
        // error: cannot move out of type `[String; 3]`, a non-copy array
        //    sclone[0];                 // move
        &sclone[0];                      // ref
        smclone[0] = sclone[0].clone();  // move, hence MUST clone

        // runtime check of any index access
        // panic: index out of bounds
        //    scopy[3];
        //    smcopy[3] = 0;

        // vector[index]:
        vcopy[0];        // copy
        &vcopy[0];       // ref
        vmcopy[0] = 0;   // copy
        // error: cannot move out of type `[String; 3]`, a non-copy array
        //    vclone[0];                 // move
        &vclone[0];                      // ref
        vmclone[0] = vclone[0].clone();  // move, hence MUST clone

        // compile-time check of constant array index access
        // error: this operation will panic at runtime, index out of bounds
        //    vcopy[3];
        //    vmcopy[3] = 0;
        // runtime check of dynamic array index access
        // panic: index out of bounds
        //    vcopy[rand::random::<usize>()];
    }

    #[test]
    fn test_array_slice_vector_index_range_expressions() {

        // index access is always bounds-checked within safe code:
        //   container[from .. until/exclusive]      -> [T] slices
        //
        // desugars into:
        //   c[expr] ~> *std::ops::Index::index(&c, expr)
        //   c[expr] ~> *std::ops::IndexMut::index_mut(&c, expr)

        // XXX
        // arrays, slices, vectors of Copy and Move (Clone) types:
        let acopy: [i32; 3] = [1; 3];
        let scopy: &[i32] = &acopy;
        let vcopy: Vec<i32> = vec![1; 3];

        // array[range]: subslices
        assert_eq!(acopy[0..3].len(), 3);
        assert_eq!(acopy[..3].len(), 3);
        assert_eq!(acopy[0..].len(), 3);
        assert_eq!(acopy[..].len(), 3);
        assert_eq!(acopy[0..0].len(), 0);
        assert_eq!(acopy[3..3].len(), 0);  // may use the range end index

        // slice[range], vector[range]: same
        let s: &[i32] = &scopy[..];
        assert_eq!(scopy[0..3].len(), 3);
        assert_eq!(vcopy[0..3].len(), 3);

        // XXX slices are read-only
        // https://doc.rust-lang.org/std/vec/struct.Vec.html#slicing
        // array[range], slice[range], vector[range]: CANNOT write to a slice
        let amcopy: &mut [i32; 3] = &mut acopy.clone();
        let smcopy: &mut [i32] = &mut amcopy.clone();
        let mut vmcopy: Vec<i32> = vcopy.clone();
        // error: size for values of type `[i32]` not known at compilation time
        //    amcopy[0..0] = acopy[0..0];
        //    smcopy[0..0] = scopy[0..0];
        //    vmcopy[0..0] = vcopy[0..0];
        // XXX
        //amcopy[0] = acopy[0];
        //amcopy = &mut acopy.clone();

        // runtime bounds-check
        // panic: range end index out of range
        //    &acopy[0..4];
        //    &scopy[4..4];
        //    &vcopy[4..4];
        // panic: slice index starts at 4 but ends at 3
        //    &acopy[4..3];
        //    &scopy[4..3];
        //    &vcopy[4..3];
    }

    #[test]
    fn test_array_slice_vector_get_methods() {

        // index access is always bounds-checked within safe code:
        //   container.get(index)                    -> std::option::Option<T>
        //   container.get(from .. until/exclusive)  -> std::option::Option<T>
        //
        // desugars into:
        //   c.get(expr) ~> std::slice::SliceIndex::get(&c, expr)
        //   c.get_mut(expr) ~> std::slice::SliceIndex::get_mut(&c, expr)
        //   ...

        // XXX
        // arrays, slices, vectors of Copy and Move (Clone) types:
        let acopy: [i32; 3] = [1; 3];
        let scopy: &[i32] = &acopy;
        let vcopy: Vec<i32> = vec![1; 3];

        // array.get() -> Option<&T>
        assert_eq!(Some(&1), acopy.get(0));               // element
        assert_eq!(Some(&[1; 2][..]), acopy.get(0..2));   // subslice
        if let Some(i) = acopy.get(4) { assert!(false) }  // bounds-checked

        // slice.get() -> Option<&T>
        assert_eq!(Some(&1), scopy.get(0));               // element
        assert_eq!(Some(&[1; 2][..]), scopy.get(0..2));   // subslice
        if let Some(i) = scopy.get(4) { assert!(false) }  // bounds-checked

        // vector.get() -> Option<&T>
        assert_eq!(Some(&1), vcopy.get(0));               // element
        assert_eq!(Some(&[1; 2][..]), vcopy.get(0..2));   // subslice
        if let Some(i) = vcopy.get(4) { assert!(false) }  // bounds-checked

        // can have UNCHECKED array and slice index access within UNSAFE
        unsafe {
            assert_eq!(acopy.get_unchecked(1), &1);
            assert_eq!(scopy.get_unchecked(1), &1);
            assert_eq!(vcopy.get_unchecked(1), &1);
            acopy.get_unchecked(4);
            scopy.get_unchecked(4);
            vcopy.get_unchecked(4);
        }
    }

    #[test]
    fn test_array_slice_destructure_pattern_match() {

        let a0: [i32; 3] = [1, 2, 3];

        // can COPY-destructure/match arrays using _slice patterns:_ [....]
        if let [1, 2, 3] = a0 {}                  // literal pattern
        let [a, b, c] = a0;                       // identifier pattern
        let [_, _, _] = a0;                       // wildcard pattern
        if let [1, ..] = a0 {}                    // rest pattern
        if let [.., 3] = a0 {}
        if let [1, .., 3] = a0 {}
        if let [(0..=2), (1..), (..=4)] = a0 {}   // range pattern (l..=u)
        if let [1, ..] | [.., 3] = a0 {}          // or pattern

        // can COPY-destructure/match slices using _slice patterns:_ [....]
        match a0[..] {
            [1, 2, 3] => true,                    // literal pattern
            [a, b, c] => (a, b, c) == (1, 2, 3),  // identifier pattern
            [_, _, _] => true,                    // wildcard pattern
            [1, ..] => true,                      // rest pattern
            [.., 3] => true,
            [1, .., 3] => true,
            [(0..=2), (1..), (..=4)] => true,     // range pattern (l..=u)
            [1, ..] | [.., 3] => true,            // or pattern
            _ => false
        };

        // for MOVE- and BORROW-destructure/match
        // -> test_array_copy_move_borrow_from_slice()

        assert_eq!(a0.len(), 3);
    }

    #[test]
    fn test_array_std_traits_copy_clone_drop_debug_hash_eq_ord_default() {

        // arrays receive "drop glue" code as needed (if elems have destructor)
        // arrays are NOT Drop, even if elements are
        assert_not_impl_any!([Box<i32>; 1]: Drop);
        assert_impl_all!(Box<i32>: Drop);

        // arrays are Copy/Clone IF element type is
        assert_impl_all!([i32; 0]: Copy, Clone);
        assert_impl_all!([i32; 1]: Copy, Clone);
        assert_impl_all!([i32; 13]: Copy, Clone);
        assert_impl_all!([i32; 33]: Copy, Clone);
        assert_impl_all!([String; 1]: Clone);
        assert_not_impl_any!([String; 1]: Copy);

        // arrays are Debug IF element type is
        assert_impl_all!([i32; 0]: Debug);
        assert_impl_all!([i32; 1]: Debug);
        assert_impl_all!([i32; 13]: Debug);
        assert_impl_all!([i32; 33]: Debug);

        // arrays are element-wise Hash, Eq, Ord IF element type is
        assert_impl_all!([i32; 0]: Hash, Eq, PartialEq, Ord, PartialOrd);
        assert_impl_all!([i32; 1]: Hash, Eq, PartialEq, Ord, PartialOrd);
        assert_impl_all!([i32; 13]: Hash, Eq, PartialEq, Ord, PartialOrd);
        assert_impl_all!([i32; 33]: Hash, Eq, PartialEq, Ord, PartialOrd);

        // arrays are Default IF element type is
        // (currently limited to <=32 fields)
        assert_impl_all!([i32; 0]: Default);
        assert_impl_all!([i32; 1]: Default);
        assert_impl_all!([i32; 32]: Default);
        // assert_impl_all!([i32; 33]: Default);
    }

    #[test]
    fn test_array_copy_move_borrow_from_slice() {

        // stack-allocated slices
        let scopy: &[i32] = &[1, 2];
        let sclone: &[String] = &["a".into(), "b".into()];

        // CANNOT copy/move/borrow from slice into array DIRECTLY
        //
        // error: mismatched types, expected `[...; 2]`, found `[...]`
        //    let acopy: [i32; 2] = [1, 2][..];
        //    let aclone: [String; 2] = ["a".into(), "b".into()][..];
        //
        // error: mismatched types, expected `[...; 2]`, found `&[...]`
        //    let acopy: [i32; 2] = scopy;
        //    let aclone: [String; 2] = sclone;
        //
        // error: mismatched types, expected `&[...; 2]`, found `&[...]`
        //    let acopy: &[i32; 2] = scopy;
        //    let aclone: &[String; 2] = sclone;

        // can COPY-destructure into array via slice pattern -> a, b: i32
        if let [a, b] = scopy[..] {
            assert_eq!([a, b], scopy);
        }
        match scopy[..] {  // same
            [a, b] => { assert_eq!([a, b], scopy) }
            _ => {}
        }

        // CANNOT MOVE-destructure into array via slice pattern -> a, b: i32
        // error: cannot move out of type `[String]`, a non-copy slice
        //    if let [a, b] = sclone[..] {
        //        assert_eq!([a, b], sclone);
        //    }
        //    match sclone[..] {  // same
        //        [a, b] => { assert_eq!([a, b], sclone) }
        //        _ => {}
        //    }

        // can BORROW-destructure into array via slice pattern -> a, b: &i32
        if let [a, b] = &scopy[..] {
            assert_eq!([*a, *b], scopy);
        }
        if let [a, b] = scopy {  // same, since already &[i32]
            assert_eq!([*a, *b], scopy);
        }
        match scopy {  // same
            [a, b] => { assert_eq!([*a, *b], scopy) }
            _ => {}
        }

        // can COPY into array via try_from/into() with bounds-checking
        //    &[T] -> [T; N] succeeds if slice.len() == N
        let a0: [i32; 2] = <[i32; 2]>::try_from(scopy).unwrap();
        let a1: [i32; 2] = scopy.try_into().unwrap();  // same
        assert_eq!(a0, scopy);
        assert_eq!(a1, scopy);
        // panic: called `Result::unwrap()` on `Err` value: TryFromSliceError
        //    let a1: [i32; 1] = scopy.try_into().unwrap();
        //    let a1: [i32; 3] = scopy.try_into().unwrap();

        // CANNOT MOVE into array via try_from/into() with bounds-checking
        //    &[T] -> [T; N] succeeds if slice.len() == N
        // error: trait bound `[String; 2]: TryFrom<&[String]>` not satisfied
        //    let a0: [String; 2] = <[String; 2]>::try_from(sclone).unwrap();
        //    let a1: [String; 2] = sclone.try_into().unwrap();


        // can BORROW into array via try_from/into() with bounds-checking
        //    &[T] -> &[T; N] succeeds if slice.len() == N
        let a0: &[i32; 2] = <&[i32; 2]>::try_from(scopy).unwrap();
        let a1: &[i32; 2] = scopy.try_into().unwrap();  // same
        assert_eq!(a0, scopy);
        assert_eq!(a1, scopy);
        // panic: called `Result::unwrap()` on `Err` value: TryFromSliceError
        //    let a1: &[i32; 1] = scopy.try_into().unwrap();
        //    let a1: &[i32; 3] = scopy.try_into().unwrap();
    }

    #[test]
    fn test_array_std_traits_intoiterator_from() {

        // arrays (of any length) are reference and value IntoIterator
        assert_not_impl_any!([i32; 1]: Iterator);
        assert_impl_all!([i32; 0]: IntoIterator);
        assert_impl_all!([i32; 33333]: IntoIterator);
        assert_impl_all!(&'static [i32; 33333]: IntoIterator);
        assert_impl_all!(&'static mut [i32; 33333]: IntoIterator);
        assert_eq!([1; 33].into_iter().sum::<i32>(), 33);

        // arrays are From<Tuple> IF element types allow
        // (currently limited to <=12 fields)
        assert_not_impl_any!([(); 0]: From<()>);
        assert_not_impl_any!([i32; 1]: From<(bool,)>);
        assert_impl_all!([i32; 1]: From<(i32,)>);
        assert_impl_all!([i32; 12]:
                         From<(i32, i32, i32, i32, i32,
                               i32, i32, i32, i32, i32,
                               i32, i32,)>);
        assert_not_impl_any!([i32; 13]:
                             From<(i32, i32, i32, i32, i32,
                                   i32, i32, i32, i32, i32,
                                   i32, i32, i32,)>);
    }

    #[test]
    fn test_slices_std_traits_copy_clone_drop_debug_hash_eq_ord_default() {

        // slices receive "drop glue" code as needed (if elems have destructor)
        // slices are NOT Drop, even if elements are
        assert_not_impl_any!([Box<i32>]: Drop);
        assert_impl_all!(Box<i32>: Drop);

        // slices are NOT Copy/Clone even if element type is
        assert_not_impl_any!([i32]: Copy, Clone);
        assert_not_impl_any!([String]: Copy, Clone);
        assert_impl_all!(i32: Copy, Clone);
        assert_impl_all!(String: Clone);

        // slices are Debug IF element type is
        assert_impl_all!([i32]: Debug);

        // slices are element-wise Hash, Eq, Ord IF element type is
        assert_impl_all!([i32]: Hash, Eq, PartialEq, Ord, PartialOrd);

        // slices are NOT Default even if element type is
        assert_not_impl_any!([i32]: Default);
    }

    #[test]
    fn test_slice_std_traits_intoiterator_from() {

        // slice types are reference but not value IntoIterator
        // (there are also structs std::slice::Iter, std::slice::IterMut)
        assert_not_impl_any!([i32]: Iterator);
        assert_not_impl_any!([i32]: IntoIterator);
        assert_not_impl_any!(&'static [i32]: Iterator);
        assert_not_impl_any!(&'static mut [i32]: Iterator);
        assert_impl_all!(&'static [i32]: IntoIterator);
        assert_impl_all!(&'static mut [i32]: IntoIterator);
        assert_eq!([1; 33][..].iter().sum::<i32>(), 33);
        assert_eq!([1; 33][..].into_iter().sum::<i32>(), 33);

        // slices are NOT From<Tuple> even if element types allow
        assert_not_impl_any!([i32]: From<(i32,)>);
    }

    // XXX TODO
    #[test]
    fn test_vector() {

        // https://doc.rust-lang.org/std/vec/struct.Vec.html
        // pub fn into_boxed_slice(self) -> Box<[T], A>
        // pub fn as_slice(&self) -> &[T]
        // pub fn as_mut_slice(&mut self) -> &mut [T]
        // pub fn extend_from_slice(&mut self, other: &[T])
        //
        // vec![x; n], vec![a, b, c, d], and Vec::with_capacity(n), will all produce a Vec with exactly the requested capacity. If len == capacity, (as is the case for the vec! macro), then a Vec<T> can be converted to and from a Box<[T]> without reallocating or moving the elements.
    }
}
