#![allow(unused)]

// ======================================================================
// Summary, Code Examples: Features/Limitations of Generic Functions & Methods
//
// TOC:
// * Generic Functions
// * Generic Methods/Functions in Traits/Structs/Enums/Unions/Impl
// ======================================================================

use std::ops::Add;

// ----------------------------------------------------------------------
// Generic Functions
//
// - static dispatch (monomorphization)
// - can implement dynamic dispatch via function pointers
//     see ./todo_functions_methods_lambdas_closures.rs
//
// - can take/return generic type by value/reference
// - can have trait bounds on type parameter
// - CANNOT have default type arguments
// - CANNOT specialize generic functions
// - CANNOT have generic functions with higher-kinded types
// ----------------------------------------------------------------------

// can take/return generic type by value/reference
fn f_map<A>(a: A) -> A { a }
fn f_pass<A>(a: &A) -> &A { a }

// can have trait bounds on type parameter
fn f_make<A: Default>() -> A { A::default() }

// can have complex trait bounds on type parameter
// error: the size for values of type `T` cannot be known at compilation time
//   fn f_add<T: ?Sized + Add<Output = T>>(s: T, t: T) -> T { s + t }
fn f_add<T: Add<Output = T>>(s: T, t: T) -> T { s + t }

// CANNOT have default type arguments
// error: defaults for type parameters only allowed in `struct`, ...
//   fn f_map<A = i32>(a: A) -> A { a }

// CANNOT specialize generic functions
// error: the name `f_map` is defined multiple times
//   fn f_map<f64>(a: f64) -> f64 { a }
// error: expected one of `(` or `<`, found `::`
//   fn f_map::<f64>(a: f64) -> f64 { a }

// CANNOT have higher-kinded generic types
// error: type arguments are not allowed on type parameter `A`
//   fn f<A, B>(a: A<B>) {}

#[cfg(test)]
mod tests0 {
    use super::*;

    #[test]
    fn test_generic_functions_i32() {

        let a: i32 = 1;

        // can infer type from argument
        assert_eq!(f_map(a), a);
        assert_eq!(f_pass(&a), &a);

        // can supply type argument
        assert_eq!(f_map::<i32>(a), a);
        assert_eq!(f_pass::<i32>(&a), &a);

        // MUST supply type argument, cannot infer from parameterless function
        // error: type annotations needed
        //   assert_eq!(f_make(), 0);
        assert_eq!(f_make::<i32>(), i32::default());  // turbofish ::<...>
    }

    #[test]
    fn test_generic_functions_string() {

        let a: String = "a".to_string();

        // can infer type parameter from argument
        assert_eq!(f_map(a.clone()), a);
        assert_eq!(f_pass(&a), &a);

        // can supply type argument
        assert_eq!(f_map::<String>(a.clone()), a);
        assert_eq!(f_pass::<String>(&a), &a);

        // MUST supply type argument, cannot infer from parameterless function
        // error: type annotations needed
        //   assert_eq!(f_make(), 0);
        assert_eq!(f_make::<String>(), String::default());  // turbofish ::<...>
    }
}

// ----------------------------------------------------------------------
// Generic Methods/Functions in Traits/Structs/Enums/Unions/Impl
//
// - static dispatch (monomorphization)
// - CANNOT have dynamic dispatch for generic methods, NOT OBJECT-SAVE:
//   -> see ./traits_static_v_dynamic_dispatch.rs
// - can have dynamic dispatch with generic traits
//   -> see ./generics_traits_static_v_dynamic_dispatch.rs
//
// - can provide generic methods/functions in traits/impl blocks
// - can take/return generic type by value/reference
// - can have trait bounds on type parameter
// - CANNOT have default type arguments
// - CANNOT specialize generic methods/functions
//
// => +/- Generic methods: - specialization, - dynamic binding
// => +/- Generic traits: + specialization, + dynamic binding for methods
// ----------------------------------------------------------------------

// can add generic functions/methods in traits:

trait T {
    // can have generic function, see above
    fn f_map<A>(a: A) -> A { a }

    // CANNOT have default type arguments
    // error: defaults for type parameters only allowed in `struct`, ...
    //   fn f_map<A = i32>(a: A) -> A { a }

    // can have generic methods
    fn m_map<A>(&self, a: A) -> A { a }

    // use a (named) lifetime parameter to align result with arg
    // error: lifetime may not live long enough
    //   fn m_pass<A>(&self, a: &A) -> &A { a }
    fn m_pass<'a, A>(&self, a: &'a A) -> &'a A { a }

    fn m_make<A: Default>(&self) -> A { A::default() }  // with trait bound
}

// can add generic functions/methods in impl blocks:

#[derive(Default)]
struct S;
impl T for S {}

impl S {
    fn m_take<A>(&self, a: A) {}
}

enum E { Value }
impl T for E {}

impl E {
    fn m_take<A>(&self, a: A) {}
}

union U { value: () }
impl T for U {}

impl U {
    fn m_take<A>(&self, a: A) {}
}

// CANNOT specialize generic methods
#[derive(Default)]  // have default()
struct S1;
impl T for S1 {

    // error: expected one of `(` or `<`, found `::`
    //   fn m_map::<f64>(&self, a: f64) -> f64 { a }
    // without turbofish ::<...>, treats 'f64' as a type variable:
    // warning: type parameter `f64` should have an upper camel case name
    //   fn m_map<f64>(&self, a: f64) -> f64 { a }
}

#[cfg(test)]
mod tests1 {
    use super::*;

    #[test]
    fn test_generic_methods_on_structs() {

        let i: i32 = 1;
        let s: String = "a".to_string();
        let r: S = S{};

        // can infer type from argument
        assert_eq!(S::f_map(i), i);
        assert_eq!(r.m_map(i), i);
        assert_eq!(r.m_pass(&i), &i);
        assert_eq!(r.m_take(i), {});

        // can supply type argument
        assert_eq!(S::f_map::<String>(s.clone()), s);
        assert_eq!(r.m_map::<String>(s.clone()), s);
        assert_eq!(r.m_pass::<String>(&s), &s);
        assert_eq!(r.m_take::<String>(s.clone()), {});

        // must supply type, cannot infer from parameterless method
        // error: type annotations needed
        //   assert_eq!(r.m_make(), 0);
        assert_eq!(r.m_make::<i32>(), 0);
        assert_eq!(r.m_make::<String>(), String::default());
    }

    #[test]
    fn test_generic_methods_on_enums() {

        let i: i32 = 1;
        let s: String = "a".to_string();
        let r: E = E::Value;

        // can infer type from argument
        assert_eq!(E::f_map(i), i);
        assert_eq!(r.m_map(i), i);
        assert_eq!(r.m_pass(&i), &i);
        assert_eq!(r.m_take(i), {});

        // can supply type argument
        assert_eq!(E::f_map::<String>(s.clone()), s);
        assert_eq!(r.m_map::<String>(s.clone()), s);
        assert_eq!(r.m_pass::<String>(&s), &s);
        assert_eq!(r.m_take::<String>(s.clone()), {});

        // must supply type, cannot infer from parameterless method
        // error: type annotations needed
        //   assert_eq!(r.m_make(), 0);
        assert_eq!(r.m_make::<i32>(), 0);
        assert_eq!(r.m_make::<String>(), String::default());
    }

    #[test]
    fn test_generic_methods_on_unions() {

        let i: i32 = 1;
        let s: String = "a".to_string();
        let r: U = U { value: () };

        // can infer type from argument
        assert_eq!(U::f_map(i), i);
        assert_eq!(r.m_map(i), i);
        assert_eq!(r.m_pass(&i), &i);
        assert_eq!(r.m_take(i), {});

        // can supply type argument
        assert_eq!(U::f_map::<String>(s.clone()), s);
        assert_eq!(r.m_map::<String>(s.clone()), s);
        assert_eq!(r.m_pass::<String>(&s), &s);
        assert_eq!(r.m_take::<String>(s.clone()), {});

        // must supply type, cannot infer from parameterless method
        // error: type annotations needed
        //   assert_eq!(r.m_make(), 0);
        assert_eq!(r.m_make::<i32>(), 0);
        assert_eq!(r.m_make::<String>(), String::default());
    }
}
