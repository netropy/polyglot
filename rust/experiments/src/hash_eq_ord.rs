#![allow(unused)]

// ======================================================================
// Excercise: Implement by Hand: Hash, Eq, PartialEq, Ord, PartialOrd
//
// TOC:
// * Simple Tuple Struct + Factories + Convenience Converters
// * Hash, PartialEq, Eq, Ord, PartialOrd
//
// excellent code examples in:
// https://doc.rust-lang.org/std/cmp/index.html#traits
// https://doc.rust-lang.org/stable/std/hash/index.html#traits
// ======================================================================

use std::convert::{From, Into};
use std::fmt::{Display, Formatter, Result};
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;
use std::cmp::Ordering;

// ----------------------------------------------------------------------
// Simple Tuple Struct + Factories + Convenience Converters
// ----------------------------------------------------------------------

#[derive(Debug, Default, Clone)]
//#[derive(Hash)]               // implemented below: hash()
//#[derive(Eq, PartialEq)]      // implemented below: eq()
//#[derive(Ord, PartialOrd)]    // implemented below: cmp(), partial_cmp()
pub struct Name(String);

impl Name {

    pub fn new() -> Self { Self::default() }

    pub fn new_with_name(name: String) -> Self { Self(name) }
}

impl From<(&str)> for Name {
    fn from(name: &str) -> Self { Self(name.to_string()) }
}

impl Into<String> for Name {
    fn into(self) -> String { self.0 }  // into() callsite may need type
}

impl Display for Name {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "({})", self.0)
    }
}

#[cfg(test)]
mod tests0 {
    use super::*;

    // some test data
    const JS: &str = "Jeremias Smythe";

    #[test]
    fn test_factories_converters() {

        assert_eq!(JS, Name(JS.to_string()).0);  // tuple struct c'tor

        assert_eq!(String::default(), Name::new().0);

        let n = Name::new_with_name(JS.to_string());
        assert_eq!(JS, n.0);

        let o = Name::from(JS);
        assert_eq!(JS, o.0);

        let s: String = o.into();  // ambigous, needs type
        assert_eq!(JS, s);
    }
}

// ----------------------------------------------------------------------
// Hash, PartialEq, Eq, Ord, PartialOrd
// ----------------------------------------------------------------------

impl Hash for Name {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl PartialEq for Name {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl Eq for Name {}

impl Ord for Name {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl PartialOrd for Name {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[cfg(test)]
mod tests1 {
    use super::*;

    // some test data
    const JS: &str = "Jeremias Smythe";

    fn hashcode<T: Hash>(t: &T) -> u64 {
        let mut s = DefaultHasher::new();
        t.hash(&mut s);
        s.finish()
    }

    #[test]
    fn test_eq_hash() {

        let n = Name::new();
        assert!(n == n);
        assert!(n.eq(&n));
        assert_eq!(n, n);
        assert_eq!(hashcode(&n), hashcode(&n));

        let o = Name::new();
        assert!(n == o);
        assert_eq!(hashcode(&n), hashcode(&o));

        let n0 = Name::from(JS);
        assert!(n0 == n0);
        assert!(n0.eq(&n0));
        assert_eq!(n0, n0);
        assert_eq!(hashcode(&n0), hashcode(&n0));

        assert!(n0 != n);
        assert!(n0.ne(&n));
        assert_ne!(n0, n);

        let n1 = n0.clone();
        let n2 = n1.clone();

        assert!(n0 == n1 && n1 == n0);
        assert_eq!(hashcode(&n0), hashcode(&n1));
        assert!(n1 == n2 && n0 == n2);
        assert_eq!(hashcode(&n1), hashcode(&n2));
        assert_eq!(hashcode(&n2), hashcode(&n0));
    }

    #[test]
    fn test_cmp_lt_gt() {

        use std::cmp::Ordering;

        let n0 = Name::from("Jeremias Smith");
        let n1 = Name::from(JS);
        let n2 = Name::from("John Smith");

        assert_eq!(n0.cmp(&n1), Ordering::Less);
        assert_eq!(n0.cmp(&n0), Ordering::Equal);
        assert_eq!(n1.cmp(&n0), Ordering::Greater);

        assert!(n0 == n0);
        assert!(n0 < n1 && !(n1 < n0));
        assert!(n1 > n0 && !(n0 > n1));
        assert!(n0 < n1 && n1 < n2 && n0 < n2);
        assert!(n2 > n1 && n1 > n0 && n2 > n0);
    }
}
