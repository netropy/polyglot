#![allow(unused)]

// ======================================================================
// Summary, Code Examples: Features/Limitations/Nomenclature of Traits
//
// TOC:
// * Nominal Typing, Structs/Enums/Unions Implementing Traits
// * Trait Objects, Trait Bounds, Trait Parameters
// ======================================================================

// ----------------------------------------------------------------------
// Nominal Typing, Structs/Enums/Unions Implementing Traits
//
// + can have 0..n structs/enums/unions implementing 0..m traits
// + nominal typing, empty traits denote different types
// ----------------------------------------------------------------------

// traits may be empty
// trait T0;  // there's no "unit trait" (unlike unit structs)
trait T0 {}   // empty trait
trait T1 {}   // T0 != T1, nominal typing

// structs can implement traits
struct S0;
impl T0 for S0 {}

struct S1;
impl T1 for S1 {}

struct S2;
impl T0 for S2 {}
impl T1 for S2 {}

// enums can implement traits
enum E0 { Value }
impl T0 for E0 {}
impl T1 for E0 {}

// unions can implement traits
union U0 { value: () }
impl T0 for U0 {}
impl T1 for U0 {}

#[cfg(test)]
mod tests0 {
    use super::*;
    use static_assertions::*;

    #[test]
    fn test_nominal_typing_static_asserts() {

        // all empty struct types are mutually unequal:
        assert_type_ne_all!(S0, S1, S2);

        // all empty trait types are mutually unequal:
        // must check as trait object _dyn_:
        assert_type_ne_all!(dyn T0, dyn T1);

        // all trait/struct/enum/union types are mutually unequal:
        assert_type_ne_all!(dyn T0, dyn T1, S0, S1, S2, E0, U0);
    }

    #[test]
    fn test_struct_enum_union_implementing_empty_traits() {

        // check: instantiating struct/enum/union types
        let s0: S0 = S0 {};
        let s1: S1 = S1 {};
        let s2: S2 = S2 {};
        let e0: E0 = E0::Value;
        let u0: U0 = U0 { value: () };

        // check: struct/enum/union types implementing the listed traits
        assert_impl_all!(S0: T0);
        assert_impl_all!(S1: T1);
        assert_impl_all!(S2: T0, T1);
        assert_impl_all!(E0: T0, T1);
        assert_impl_all!(U0: T0, T1);

        // CANNOT statically pass s0 as T0, ... see trait objects below:
        // error: trait objects must include the `dyn` keyword
        //   let r0: &T0 = &s0;
        //   let r1: &T1 = &s1;
        //   let r2: &T0 = &s2;
        //   let r3: &T0 = &e0;
        //   let r4: &T0 = &u0;

        // expected: typesafe: cannot pass s0 as T1, ...
        // error: mismatched types
        //   let r1: S1 = s0;
    }
}

// ----------------------------------------------------------------------
// Trait Objects, Trait Bounds, Trait Parameters
//
// passing/returning Traits as type _...dyn T_:
// - a _trait object_ is a "fat" trait pointer with dynamic dispatch
// - supported trait pointer types: &, Box, Rc, Arc, Pin
//
// passing/returning Traits as generic type _...impl T_:
// - a _trait bound_ limits a type parameter to those implementing a trait
// - the parameter (t: &impl T) desugars into <S : T>(t: &S)
// - generic methods are statically dispatched (after monomorphization)
// ----------------------------------------------------------------------

// passes values implementing T0, argument is dynamically dispatched
fn pass_trait_object(t: &dyn T0) -> &dyn T0 { t }

// passes values implementing T0, argument is statically dispatched
fn pass_generic_impl(t: &impl T0) -> &impl T0 { t }     // exact type bounds?
fn pass_generic_trait_bound<S : T0>(t: &S) -> &S { t }  // not exactly same?

#[cfg(test)]
mod tests1 {
    use super::*;

    #[test]
    fn test_code_example_trait_object_v_trait_bound() {
        trait T {}
        struct S;
        impl T for S {}
        // fn e(t: &T) {}        // CANNOT pass as static trait reference
        fn f(t: &dyn T) {}       // pass as trait object reference for T
        fn g(t: &impl T) {}      // pass as <X: T>(t: &X) with trait bound T

        let s: S = S {};
        f(&s);
        g(&s);
        // let t: &T = &s;       // CANNOT subsume as static trait reference
        let u: &dyn T = &s;      // subsume as trait object reference for T
        // let v: &impl T = &s;  // CANNOT subsume as trait bound
    }

    #[test]
    fn test_trait_object_ref() {

        let s0: S0 = S0 {};
        let s1: S1 = S1 {};
        let s2: S2 = S2 {};

        // can pass struct values as trait objects = reference/pointer<dyn T>
        let r0: &dyn T0 = &s0;
        let r1: &dyn T1 = &s1;
        let r2: &dyn T0 = &s2;
        let r3: &dyn T1 = &s2;

        // expected: typesafe: cannot pass s0 as T1, s1 as T0 trait object:
        // => nominal typing: empty traits denote different types
        // error: the trait bound is not satisfied (not implemented)
        //   let r0: &dyn T1 = &s0;
        //   let r1: &dyn T0 = &s1;

        // expected: typesafe: cannot coerce or cast &dyn T0 <-> &dyn T1:
        // error: mismatched types, expected trait `TX`, found trait `TY`
        //   let r2: &dyn T0 = r1;  // coercion
        //   let r3: &dyn T1 = r0;
        // error: non-primitive cast, invalid cast
        //   let r2 = r1 as &dyn T0;  // casting
        //   let r3 = r0 as &dyn T1;

        // expected: variables cannot be generic (have trait bounds)
        // error: impl T not allowed in variable bindings
        //   let r0: &impl T0 = &s0;
        //   let r1: &impl T1 = &s1;
        //   let r2: &impl T0 = &s2;
        //   let r3: &impl T1 = &s2;
    }

    #[test]
    fn test_pass_as_trait_object() {

        let s0: S0 = S0 {};
        let s2: S2 = S2 {};

        // can pass struct values as trait objects ...
        let r0 = pass_trait_object(&s0);
        let r2 = pass_trait_object(&s2);
        assert_eq!(r0 as *const _, &s0 as *const _);
        assert_eq!(r2 as *const _, &s2 as *const _);

        // expected: typesafe: cannot pass s1 as T0 trait object:
        //   let s1: S1 = S1 {};
        // error: the trait bound is not satisfied
        //   let r1 = pass_trait_object(&s1);
    }

    #[test]
    fn test_pass_as_generic_impl() {

        let s0: S0 = S0 {};
        let s2: S2 = S2 {};

        // can pass struct values as trait bounded type ...
        let r0 = pass_generic_impl(&s0);
        let r2 = pass_generic_impl(&s2);
        assert_eq!(r0 as *const _, &s0 as *const dyn T0);  // need to cast as
        assert_eq!(r2 as *const _, &s2 as *const dyn T0);  // trait object

        // ... which desugars into this ...
        let r0 = pass_generic_trait_bound(&s0);
        let r2 = pass_generic_trait_bound(&s2);
        assert_eq!(r0 as *const _, &s0 as *const _);  // no need to cast as
        assert_eq!(r2 as *const _, &s2 as *const _);  // trait object

        // ... with explicit type specializations:
        let r0 = pass_generic_trait_bound::<S0>(&s0);
        let r2 = pass_generic_trait_bound::<S2>(&s2);
        assert_eq!(r0 as *const _, &s0 as *const _);  // no need to cast as
        assert_eq!(r2 as *const _, &s2 as *const _);  // trait object

        // expected: typesafe: cannot pass s1 as T0:
        //   let s1: S1 = S1 {};
        // error: the trait bound is not satisfied
        //   pass_generic_impl(&s1);
        //   pass_generic_trait_bound(&s1);
    }
}
