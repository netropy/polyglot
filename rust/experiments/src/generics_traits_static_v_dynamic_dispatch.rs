#![allow(unused)]

// ======================================================================
// Summary, Code Examples: Features/Limitations of Generic Traits
//
// TOC:
// * Generic Traits Allow for Static Type-Specialization AND Dynamic Dispatch
// ======================================================================

// ----------------------------------------------------------------------
// Generic Traits Allow for Static Type-Specialization AND Dynamic Dispatch
//
// CANNOT have dynamic dispatch for generic methods, since not object-save:
//   -> see ./traits_static_v_dynamic_dispatch.rs
// CAN have dynamic dispatch for (non-generic) methods of generic traits:
//   -> see below
//
// The combination of type-specialization + dynamic binding works as expected!
//
// Summary:
// 1. receiver type `R<A>` resolved from given trait + struct + specialization
// 2. if comes out as `dyn R<A>` then dynamically dispatched via given object
//
// Code Experiments:
//
// <- have a generic trait
//    trait T<A> { fn map(&self, a: A) -> A { a } }
//
// <- have multiple, implementing structs
//    struct S0 { i: i32 }; ...
//
// <- have multiple type-specializations per struct
//    impl T<i32> for S0 { fn map(&self, a: i32) -> i32 {...} } ...
//
// <- have multiple values per struct
//    let s00: S0 = S0 { i: 1 }; ...
//
// -> can STATICALLY call generic trait's method as function on struct value:
//    T::<i32>::map(&s00, 1);         // T::<i32> type-selected explicitely
//
// -> can STATICALLY call generic trait's method on struct value:
//    let r00: &S0 = &s00;
//    r00.map(1);                     // T::<i32> inferred from argument
//    //r00.T::<i32>::map(1)          // error: cannot type-select method
//
// -> can DYNAMICALLY call generic trait's method on trait object:
//    let r000: &dyn T<i32> = &s00;
//    r000.map(1);                    // dispatched via trait object
//
// -> can DYNAMICALLY call generic trait's method as function on trait object:
//    let s00: S0 = S0 { i: 1 };
//    <dyn T<i32>>::map(&s00, 1);     // coerces argument as trait object
//
// => +/- Generic traits:  + specialization, + dynamic binding for methods
// => +/- Generic methods: - specialization, - dynamic binding
// ----------------------------------------------------------------------

// an object-safe trait, see ./traits_static_v_dynamic_dispatch.rs
trait T<A: Default> {

    // compile check: presence of function prevents dynamic dispatch of methods
    // fn f(a: A) -> A { a }

    // can take/return generic type by value/reference
    fn map(&self, a: A) -> A { a }
    fn pass<'a>(&self, a: &'a A) -> &'a A { a }
    fn make(&self) -> A { A::default() }
}

// specialized implementations for type S0:

struct S0 { i: i32 }

impl T<i32> for S0 {                                  // or turbofish: T::<...>
    fn map(&self, a: i32) -> i32 { a + self.i + 1 }
    fn make(&self) -> i32 { self.i + 1 }
}

impl T<f64> for S0 {
    fn map(&self, a: f64) -> f64 { a + self.i as f64 + 2.2 }
    fn make(&self) -> f64 { self.i as f64 + 2.2 }
}

// specialized implementations for type S1:

struct S1 { b: bool }

impl T<i32> for S1 {                                  // or turbofish: T::<...>
    fn map(&self, a: i32) -> i32 { if self.b { a } else { -a } }
    fn make(&self) -> i32 { if self.b { 1 } else { -1 } }
}

impl T<f64> for S1 {
    fn map(&self, a: f64) -> f64 { if self.b { -a } else { a } }
    fn make(&self) -> f64 { if self.b { -1.0 } else { 1.0 } }
}

#[cfg(test)]
mod tests0 {
    use super::*;

    /// STATICALLY call a generic trait's methods as functions on struct values.
    #[test]
    fn test_static_function_dispatch_per_struct_type_per_specialized_trait() {

        let s00: S0 = S0 { i: 1 };                     // per struct type
        let s01: S0 = S0 { i: 2 };

        assert_eq!(T::<i32>::map(&s00, 1), 3);         // per struct value
        assert_eq!(T::<i32>::pass(&s00, &1), &1);
        assert_eq!(T::<i32>::make(&s00), 2);

        assert_eq!(T::<f64>::map(&s00, 1.0), 4.2);     // per specialized type
        assert_eq!(T::<f64>::pass(&s00, &1.0), &1.0);
        assert_eq!(T::<f64>::make(&s00), 3.2);

        assert_eq!(T::<i32>::map(&s01, 1), 4);
        assert_eq!(T::<i32>::pass(&s01, &1), &1);
        assert_eq!(T::<i32>::make(&s01), 3);

        assert_eq!(T::<f64>::map(&s01, 1.0), 5.2);
        assert_eq!(T::<f64>::pass(&s01, &1.0), &1.0);
        assert_eq!(T::<f64>::make(&s01), 4.2);

        let s10: S1 = S1 { b: false };                 // per struct type
        let s11: S1 = S1 { b: true };

        assert_eq!(T::<i32>::map(&s10, 1), -1);        // per struct value
        assert_eq!(T::<i32>::pass(&s10, &1), &1);
        assert_eq!(T::<i32>::make(&s10), -1);

        assert_eq!(T::<f64>::map(&s10, 1.0), 1.0);     // per specialized type
        assert_eq!(T::<f64>::pass(&s10, &1.0), &1.0);
        assert_eq!(T::<f64>::make(&s10), 1.0);

        assert_eq!(T::<i32>::map(&s11, 1), 1);
        assert_eq!(T::<i32>::pass(&s11, &1), &1);
        assert_eq!(T::<i32>::make(&s11), 1);

        assert_eq!(T::<f64>::map(&s11, 1.0), -1.0);
        assert_eq!(T::<f64>::pass(&s11, &1.0), &1.0);
        assert_eq!(T::<f64>::make(&s11), -1.0);
    }

    /// STATICALLY call a generic trait's methods on struct values.
    ///
    /// Feature: the type specialization is inferred from method's argument
    //        let foo: &Bar = &Bar {...};
    ///       foo.map(1)  ~~>  T<i32>::map(&self, A) -> A
    ///
    /// Problem: parameterless methods
    ///       error: type annotations needed, cannot satisfy `_: Default`
    ///       foo.make()
    ///
    /// Limitation: methods cannot be qualified for type specialization
    ///       error: field expressions cannot have generic arguments
    ///       foo.T::<i32>::make()
    ///
    /// => CANNOT call generic trait's method
    ///       T<A>::make(&self) -> A
    ///    as method:
    ///       foo.T::<...>::make()
    ///    only as function:
    ///       T::<...>::make(foo)
    #[test]
    fn test_static_method_dispatch_per_struct_type_per_specialized_trait() {

        let s00: S0 = S0 { i: 1 };                     // per struct type
        let s01: S0 = S0 { i: 2 };

        let r00: &S0 = &s00;
        let r01: &S0 = &s01;

        assert_eq!(r00.map(1), 3);                     // per struct value
        assert_eq!(r00.pass(&1), &1);
        // assert_eq!(r00.make(), 2);

        assert_eq!(r00.map(1.0), 4.2);                 // per specialized type
        assert_eq!(r00.pass(&1.0), &1.0);
        // assert_eq!(r00.make(), 3.2);

        assert_eq!(r01.map(1), 4);
        assert_eq!(r01.pass(&1), &1);
        // assert_eq!(r01.make(), 3);

        assert_eq!(r01.map(1.0), 5.2);
        assert_eq!(r01.pass(&1.0), &1.0);
        // assert_eq!(r01.make(), 4.2);

        let s10: S1 = S1 { b: false };                 // per struct type
        let s11: S1 = S1 { b: true };

        let r10: &S1 = &s10;
        let r11: &S1 = &s11;

        assert_eq!(r10.map(1), -1);                    // per struct value
        assert_eq!(r10.pass(&1), &1);
        // assert_eq!(r10.make(), -1);

        assert_eq!(r10.map(1.0), 1.0);                 // per specialized type
        assert_eq!(r10.pass(&1.0), &1.0);
        // assert_eq!(r10.make(), 1.0);

        assert_eq!(r11.map(1), 1);
        assert_eq!(r11.pass(&1), &1);
        // assert_eq!(r11.make(), 1);

        assert_eq!(r11.map(1.0), -1.0);
        assert_eq!(r11.pass(&1.0), &1.0);
        // assert_eq!(r11.make(), -1.0);
    }

    /// DYNAMICALLY call a generic trait's methods on trait objects.
    #[test]
    fn test_dynamic_method_dispatch_on_trait_objects_per_specialized_trait() {

        let s00: S0 = S0 { i: 1 };                     // per struct type
        let s01: S0 = S0 { i: 2 };

        let r000: &dyn T<i32> = &s00;
        let r001: &dyn T<f64> = &s00;
        let r010: &dyn T<i32> = &s01;
        let r011: &dyn T<f64> = &s01;

        assert_eq!(r000.map(1), 3);                    // per trait object
        assert_eq!(r000.pass(&1), &1);
        assert_eq!(r000.make(), 2);

        assert_eq!(r001.map(1.0), 4.2);                // per specialized type
        assert_eq!(r001.pass(&1.0), &1.0);
        assert_eq!(r001.make(), 3.2);

        assert_eq!(r010.map(1), 4);
        assert_eq!(r010.pass(&1), &1);
        assert_eq!(r010.make(), 3);

        assert_eq!(r011.map(1.0), 5.2);
        assert_eq!(r011.pass(&1.0), &1.0);
        assert_eq!(r011.make(), 4.2);

        let s10: S1 = S1 { b: false };                 // per struct type
        let s11: S1 = S1 { b: true };

        let r100: &dyn T<i32> = &s10;
        let r101: &dyn T<f64> = &s10;
        let r110: &dyn T<i32> = &s11;
        let r111: &dyn T<f64> = &s11;

        assert_eq!(r100.map(1), -1);                   // per trait object
        assert_eq!(r100.pass(&1), &1);
        assert_eq!(r100.make(), -1);

        assert_eq!(r101.map(1.0), 1.0);                // per specialized type
        assert_eq!(r101.pass(&1.0), &1.0);
        assert_eq!(r101.make(), 1.0);

        assert_eq!(r110.map(1), 1);
        assert_eq!(r110.pass(&1), &1);
        assert_eq!(r110.make(), 1);

        assert_eq!(r111.map(1.0), -1.0);
        assert_eq!(r111.pass(&1.0), &1.0);
        assert_eq!(r111.make(), -1.0);
    }

    /// DYNAMICALLY call a generic trait's methods as functions on trait objects.
    #[test]
    fn test_dynamic_function_dispatch_on_trait_objects_per_specialized_trait() {

        let s00: S0 = S0 { i: 1 };                     // per struct type
        let s01: S0 = S0 { i: 2 };

        assert_eq!(<dyn T<i32>>::map(&s00, 1), 3);     // per trait object
        assert_eq!(<dyn T<i32>>::pass(&s00, &1), &1);
        assert_eq!(<dyn T<i32>>::make(&s00), 2);

        assert_eq!(<dyn T<f64>>::map(&s00, 1.0), 4.2); // per specialized type
        assert_eq!(<dyn T<f64>>::pass(&s00, &1.0), &1.0);
        assert_eq!(<dyn T<f64>>::make(&s00), 3.2);

        assert_eq!(<dyn T<i32>>::map(&s01, 1), 4);
        assert_eq!(<dyn T<i32>>::pass(&s01, &1), &1);
        assert_eq!(<dyn T<i32>>::make(&s01), 3);

        assert_eq!(<dyn T<f64>>::map(&s01, 1.0), 5.2);
        assert_eq!(<dyn T<f64>>::pass(&s01, &1.0), &1.0);
        assert_eq!(<dyn T<f64>>::make(&s01), 4.2);

        let s10: S1 = S1 { b: false };                 // per struct type
        let s11: S1 = S1 { b: true };

        assert_eq!(<dyn T<i32>>::map(&s10, 1), -1);    // per trait object
        assert_eq!(<dyn T<i32>>::pass(&s10, &1), &1);
        assert_eq!(<dyn T<i32>>::make(&s10), -1);

        assert_eq!(<dyn T<f64>>::map(&s10, 1.0), 1.0); // per specialized type
        assert_eq!(<dyn T<f64>>::pass(&s10, &1.0), &1.0);
        assert_eq!(<dyn T<f64>>::make(&s10), 1.0);

        assert_eq!(<dyn T<i32>>::map(&s11, 1), 1);
        assert_eq!(<dyn T<i32>>::pass(&s11, &1), &1);
        assert_eq!(<dyn T<i32>>::make(&s11), 1);

        assert_eq!(<dyn T<f64>>::map(&s11, 1.0), -1.0);
        assert_eq!(<dyn T<f64>>::pass(&s11, &1.0), &1.0);
        assert_eq!(<dyn T<f64>>::make(&s11), -1.0);
    }
}
