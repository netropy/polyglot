#![allow(unused)]

use static_assertions::*;
use std::rc::Rc;

// ----------------------------------------------------------------------
// XXX TODO: overview type system, special traits

// ----------------------------------------------------------------------
// https://doc.rust-lang.org/std/boxed/struct.Box.html

// seems that Box is an immutable type, like String
// let mut b = Box::new(1);

// compare against internal mutability, Vec, Cell
// https://doc.rust-lang.org/std/vec/struct.Vec.html
// https://doc.rust-lang.org/std/cell/struct.Cell.html

// ----------------------------------------------------------------------

// https://doc.rust-lang.org/std/marker/trait.Copy.html
// https://doc.rust-lang.org/std/clone/trait.Clone.html
// https://doc.rust-lang.org/std/ops/trait.Drop.html
//
// copy is implicit, shallow/bitwise, non-overloadable
// clone is explicit, shallow or deep, overloadable
//
// Copy gives a type 'copy semantics' instead of 'move semantics'
// std::marker::Copy {}
// Clone = duplicates values safely (no double free etc)
// std::clone::Clone { fn clone(&self) -> Self }
//
// Clone is a supertrait of Copy, everything Copy must also implement Clone
// Types that are Copy should have a trivial implementation of Clone:
//    fn clone(&self) -> Self { *self }
//    let x = y.clone() === let x = *y
//
// minor difference between:
// #[derive(Copy)]
// impl Copy for Xyz { }
//
// Copy requires all the type's components to be Copy
//
// Copy and Drop are exclusive
// ...

// ----------------------------------------------------------------------
// https://doc.rust-lang.org/std/index.html#primitives

// https://doc.rust-lang.org/reference/dynamically-sized-types.html
// Note: variables, function parameters, const items, and static items must be Sized.

// Slices and trait objects are two examples of DSTs. Such types can only be used in certain cases:
// Pointer types to DSTs are sized but have twice the size of pointers to sized types:
// - Pointers to slices also store the number of elements of the slice.
// - Pointers to trait objects also store a pointer to a vtable.

// https://doc.rust-lang.org/reference/types/pointer.html

// https://doc.rust-lang.org/reference/types.html
// The list of types is:
// Primitive types:
// Boolean — bool
// Numeric — integer and float
// Textual — char and str
// Never — ! — a type with no values
// Sequence types:
// Tuple
// Array
// Slice
// User-defined types:
// Struct
// Enum
// Union
// Function types:
// Functions
// Closures
// Pointer types:
// References
// Raw pointers
// Function pointers
// Trait types:
// Trait objects
// Impl trait

// https://doc.rust-lang.org/reference/interior-mutability.html
// https://doc.rust-lang.org/reference/subtyping.html

// https://doc.rust-lang.org/reference/special-types-and-traits.html
// https://doc.rust-lang.org/reference/special-types-and-traits.html#auto-traits

// i32
assert_impl_all!(i32: Copy, Clone);
assert_not_impl_all!(i32: Drop);

// String
assert_impl_all!(String: Clone);
assert_not_impl_any!(String: Copy, Drop);

// slice types
assert_not_impl_any!([i32]: Copy, Clone, Drop);
assert_not_impl_any!(str: Copy, Clone, Drop);

// box
assert_impl_all!(Box<i32>: Drop, Clone);
assert_not_impl_all!(Box<i32>: Copy);

// non-atomic Rc
assert_not_impl_any!(Rc<i32>: Send, Sync);

// error: cannot borrow data in an `Rc` as mutable
//    let mut sr: SR = Rc::new([1; 0]);


// XXX describe: & vs &mut vs
// https://doc.rust-lang.org/std/borrow/trait.Borrow.html
// https://doc.rust-lang.org/std/borrow/trait.BorrowMut.html
// https://doc.rust-lang.org/std/convert/trait.AsRef.html
// https://doc.rust-lang.org/std/convert/trait.AsMut.html

/*
https://doc.rust-lang.org/std/primitive.tuple.html
https://doc.rust-lang.org/src/core/any.rs.html
Blanket Implementations

impl<T> Any for T
where
    T: 'static + ?Sized,

impl<T> Borrow<T> for T
where
    T: ?Sized,

impl<T> BorrowMut<T> for T
where
    T: ?Sized,

impl<T> From<T> for T

impl<T, U> Into<U> for T
where
    U: From<T>,

impl<T> ToOwned for T
where
    T: Clone,

impl<T, U> TryFrom<U> for T
where
    U: Into<T>,

impl<T, U> TryInto<U> for T
where
    U: TryFrom<T>,
*/

/*

https://doc.rust-lang.org/std/iter/trait.Iterator.html
https://doc.rust-lang.org/std/iter/trait.IntoIterator.html

https://doc.rust-lang.org/std/slice/struct.Iter.html
https://doc.rust-lang.org/std/slice/struct.IterMut.html
https://doc.rust-lang.org/std/primitive.slice.html#iteration

What is the difference between iter and into_iter?

https://stackoverflow.com/questions/34733811/what-is-the-difference-between-iter-and-into-iter
into_iter is a generic method to obtain an iterator, whether this iterator yields values, immutable references or mutable references is context dependent and can sometimes be surprising.

iter and iter_mut are ad-hoc methods. Their return type is therefore independent of the context, and will conventionally be iterators yielding immutable references and mutable references, respectively.
*/


// TODO: find out when <T: ?Sized> is needed/useful in generic functions
//
// https://doc.rust-lang.org/book/ch19-04-advanced-types.html#dynamically-sized-types-and-the-sized-trait
//
// dynamically sized types and the sized trait
// t may or may not be Sized:
// fn generic<T: ?Sized>(t: &T) {...}
// -> try implement a generic fn for str type (vs &str)
//
// generic functions have an implicit trait bound Sized:
//   fn f<T>(t: &T)
// desugars into:
//   fn f<T: Sized>(t: &T)
// can be dropped with ?Sized
//   fn f<T: ?Sized>(t: &T)
//
// Normally, Rust has just 2 kinds of unsized types: slices, dyn objects
// Cannot find an example with slices for when ?Sized is needed or useful.

// const vs static
// double references
// https://dhghomon.github.io/easy_rust/Chapter_0.html
