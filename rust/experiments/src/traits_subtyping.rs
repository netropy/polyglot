#![allow(unused)]

// ======================================================================
// Summary, Code Examples: Features/Limitations of Subtyping with Traits
//
// TOC:
// * Subtyping, Supertraits, Interface Inheritance, Coercion, Casting
// * Static Dispatch Disambiguation of Inherited Default Methods
// ======================================================================

// ----------------------------------------------------------------------
// Subtyping, Supertraits, Interface Inheritance, Coercion, Casting
//
// listing a supertrait:
// - makes all its functionality available in the subtrait
// - enables type coercion from the subtrait (automatic upcasting)
//
// => Rust supports subtyping (interface inheritance) even if not called so
// ----------------------------------------------------------------------

trait Super0 {
    fn f(&self) -> i32 { 1 }  // default method
}

trait Super1 {
    fn g(&self) -> i32 { 2 }  // default method
}

// MUST implement also the listed supertraits:
trait Sub: Super0 + Super1 {
    // can use supertraits:
    fn h(&self) -> i32 { self.f() + self.g() }
}

struct SuperS;
impl Super0 for SuperS {}
impl Super1 for SuperS {}

struct SubS;
impl Sub for SubS {}

// MUST also implement Super (since Sub: Super):
impl Super0 for SubS {}
impl Super1 for SubS {}

#[cfg(test)]
mod tests0 {
    use super::*;
    use static_assertions::*;

    #[test]
    fn test_subtyping_static_asserts() {

        // struct types implement all listed traits:
        assert_impl_all!(SuperS: Super0, Super1);
        assert_impl_all!(SubS: Sub, Super0, Super1);
        assert_impl_all!(dyn Sub: Super0, Super1);

        // trait is a child of listed traits:
        assert_trait_sub_all!(Sub: Super0, Super1);

        // trait is a parent of listed traits:
        assert_trait_super_all!(Super0: Sub);
        assert_trait_super_all!(Super1: Sub);
    }

    #[test]
    fn test_subtyping_ref_trait_objects() {
        let s0: SuperS = SuperS {};
        let s1: SubS = SubS {};

        // can access s0 as supertrait:
        let r0: &dyn Super0 = &s0;
        let r1: &dyn Super1 = &s0;
        assert_eq!(r0.f(), 1);
        assert_eq!(r1.g(), 2);

        // typesafe: cannot access s1 as Sub:
        // error: the trait bound `Sub` is not satisfied (not implemented)
        //   let r2: &dyn Sub = &s0;

        // can access s1 as Sub:
        let r2: &dyn Sub = &s1;
        assert_eq!(r2.f(), 1);
        assert_eq!(r2.g(), 2);
        assert_eq!(r2.h(), 3);

        // can access s1 as supertrait:
        let r3: &dyn Super0 = &s1;  // coercion
        assert_eq!(r3.f(), 1);
        let r4 = &s1 as &dyn Super0;  // casting
        assert_eq!(r4.f(), 1);
        let r5: &dyn Super1 = &s1;  // coercion
        assert_eq!(r5.g(), 2);
        let r6 = &s1 as &dyn Super1;  // casting
        assert_eq!(r6.g(), 2);
    }

    #[test]
    fn test_subtyping_box_trait_objects() {

        // can box SuperS as Super, SubS as Sub:
        let b0: Box<dyn Super0> = Box::new(SuperS);
        let b1: Box<dyn Super1> = Box::new(SuperS);
        let b2: Box<dyn Sub> = Box::new(SubS);

        // can box SubS as Super:
        let r0: Box<dyn Super0> = Box::new(SubS);
        let r1: Box<dyn Super1> = Box::new(SubS);

        // typesafe: cannot box SuperS as Sub:
        // error: the trait bound `Sub` is not satisfied (not implemented)
        //   let r2: Box<dyn Sub> = Box::new(SuperS);

        // CANNOT upcast boxed SubS as Super:
        // error: trait upcasting coercion is experimental
        //   let r3: Box<dyn Super0> = b2;
        //   let r4: Box<dyn Super1> = b2;
        //   let r3 = b2 as Box<dyn Super0>;
        //   let r4 = b2 as Box<dyn Super1>;

        // CANNOT downcast boxed SubS from Super:
        // error: mismatched types, expected trait `Sub`, found trait `Super`
        //   let r5: Box<dyn Sub> = r0;
        //   let r6: Box<dyn Sub> = r1;
        // error: non-primitive cast, can only coerce to a specific trait object
        //   let r5 = r0 as Box<dyn Sub>;
        //   let r6 = r1 as Box<dyn Sub>;
    }
}

// ----------------------------------------------------------------------
// Static Dispatch Disambiguation of Inherited Default Methods
//
// - CANNOT shadow methods/functions in subtraits, must disambiguate
// - MUST disambiguated name clashes at usage site
// - MAY HAVE to use a trait object where disambiguation was rejected
// ----------------------------------------------------------------------

//
trait Super2 {
    fn f(&self) -> i32 { 3 }  // conflicting default method
}

// NO name-clash error at supertraits definition
trait Sub1: Sub + Super2 {

    // NO name-clash error at method defintion
    fn g(&self) -> i32 { 4 }  // conflicting default method

    // MUST disambiguate name clash at usage site
    // error: multiple applicable items in scope
    //   fn h(&self) -> i32 { self.f() }
    fn h(&self) -> i32 { Super0::f(self) + Super2::f(self) }

    // NO shadowing by subtrait method/function, MUST disambiguate
    // error: multiple applicable items in scope
    //   fn i(&self) -> i32 { self.g() }
    //   fn i(&self) -> i32 { Self::g(self) }  // <- WHY IS THIS AN ERROR?
    fn i(&self) -> i32 { Sub1::g(self) }

    // CANNOT statically refer to supertrait to disambiguate
    // error: trait objects must include the `dyn` keyword
    //   fn j(&self) -> i32 { Sub::f(self) + Sub::g(self) }
    //
    // error: size for values of type `Self` not known at compilation time
    //   fn j(&self) -> i32 { <dyn Sub>::f(self) + <dyn Sub>::g(self) }
    // same:
    //   fn j(&self) -> i32 { (self as &dyn Sub).f() + (self as &dyn Sub).g() }
    //
    fn j(&self) -> i32 where Self: Sized {
        <dyn Sub>::f(self) + <dyn Sub>::g(self)
    }
}

struct Sub1S;
impl Sub for Sub1S {}
impl Sub1 for Sub1S {}
impl Super0 for Sub1S {}
impl Super1 for Sub1S {}
impl Super2 for Sub1S {}

#[cfg(test)]
mod tests1 {
    use super::*;
    use static_assertions::*;

    #[test]
    fn test_subtyping_static_asserts() {

        // struct types implement all listed traits:
        assert_impl_all!(Sub1S: Super0, Super1, Super2, Sub, Sub1);
        assert_impl_all!(dyn Sub1: Super0, Super1, Super2, Sub);

        // trait is a child of listed traits:
        assert_trait_sub_all!(Sub1: Super0, Super1, Super1, Sub);

        // trait is a parent of listed traits:
        assert_trait_super_all!(Sub: Sub1);
        assert_trait_super_all!(Super2: Sub1);
    }

    #[test]
    fn test_subtyping_ref_trait_objects() {
        let s: Sub1S = Sub1S {};
        let r: &dyn Sub1 = &s;

        // NO shadowing, MUST disambiguate name
        // error: multiple applicable items in scope
        //   assert_eq!(s.g(), 4);
        //   assert_eq!(r.g(), 4);
        assert_eq!(Sub1::g(&s), 4);
        assert_eq!(Sub1::g(r), 4);

        // NO shadowing, MUST disambiguate name
        // error: multiple applicable items in scope
        //   assert_eq!(s.h(), 4);
        //   assert_eq!(r.h(), 4);
        assert_eq!(Sub1::h(&s), 4);
        assert_eq!(Sub1::h(r), 4);

        // unambigous
        assert_eq!(s.i(), 4);
        assert_eq!(r.i(), 4);

        // can call on sized struct value
        assert_eq!(s.j(), 3);

        // CANNOT call on trait object ref
        // error: cannot be invoked on a trait object, has a `Sized` requirement
        //   assert_eq!(r.j(), 3);
    }
}
