#![allow(unused)]

#[cfg(test)]
mod tests {
    use super::*;

    //fn test_stack_heap() -> &'static [i32; 2] {
    fn test_stack_heap<'a>() -> &'a [i32; 2<<30] {
        let a: [i32; 2<<30] = [1; 2<<30];
        let r: &[i32; 2<<30] = &[1; 2<<30];
        // error: cannot return reference to local variable
        //        returns a reference to data owned by the current function
        //    return &a;
        // OK for:
        //    fn test_stack_heap() -> &'static [i32; 2]
        //    fn test_stack_heap<'a>() -> &'a [i32; 2]
        // WHY?
        return r;

        // OK, for static:
        static R: &[i32; 2<<30] = &[1; 2<<30];
        R
    }
}
