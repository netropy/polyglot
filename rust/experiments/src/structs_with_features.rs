#![allow(unused)]

// ======================================================================
// Summary, Code Examples: Structs + Tuple Structs with All The Trimmings
//
// TOC:
// * Struct with Derived Traits
// * str/String Converters/Formatters
// * Tuple Structs + Converters
//
// XXX TODO
//
// https://doc.rust-lang.org/rust-by-example/custom_types/structs.html
// ======================================================================

use std::convert::{From, Into};
use std::fmt::{Display, Formatter, Result};

// ----------------------------------------------------------------------
// Struct with Derived Traits
// str/String Converters/Formatters

// ----------------------------------------------------------------------

// generate trait implementations -- as far as supported by all fields:
#[derive(Debug)]              // have the {:?} formatter
#[derive(Hash)]               // have .hash(hasher)
#[derive(Default)]            // have default() for empty instance
#[derive(Clone)]              // have .clone(), .clone_from()
//#[derive(Copy)]             // cannot be Copy since String is not Copy
#[derive(Eq, PartialEq)]      // have ==, !=, eq(), ne()
#[derive(Ord, PartialOrd)]    // have cmp(), <, ... lexicographic order
//#[derive(Display)]          // no derive macro, implement {} formatter below
pub struct Name {
    pub first: String,  // by default, fields are module-private
    pub last: String,   // String is a mutable type

    // Variants of struct value construction: see unit-tests below
    //
    // - structs have 1 constructor, which initializes all fields at once:
    //     let s = Struct { field1: value1, field2: value2 ... }
    //
    // - field init shorthand:
    //     let field1 = value1;
    //     let field2 = value2; ...
    //     let s = Struct { field1, field2 ... }
    //
    // - struct update syntax:
    //     let struct1 = Struct { ... };
    //     let fieldx = valuex;
    //     let s = Struct { fieldx, ..struct1 }
    //
    // - derived or implemented trait Default function:
    //     let s = Struct::default();
    //     let s: Struct = Default::default();
    //
    // - implemented trait From function:
    //     let s = Struct::from(...);
    //
    // - implemented new...() factory functions:
    //     let s = Struct::new();
    //     let s = Struct::new_with_...(...);
}

impl Name {

    // convention: add function new() even if having default():
    pub fn new() -> Self {
        Self::default()
    }

    // convention: add function new_with_xyz(...) -- no overloading
    // function takes ownership of arguments since String is no Copy
    pub fn new_with_names(first: String, last: String) -> Self {
        Self { first, last }             // field init shorthand
    }

    // may add constructor converting str->String
    // Note: better naming: of(...), from_str(...)
    pub fn new_with_str(first: &str, last: &str) -> Self {
        Self {                           // cannot use field init shorthand
            first: String::from(first),  // str -> String
            last: last.to_string(),      // str -> String
        }
    }
}

#[cfg(test)]
mod tests0 {
    use super::Name;
    use std::hash::{Hash, Hasher};
    use std::collections::hash_map::DefaultHasher;

    // some test data
    const JS: (&str, &str) = ("Jeremias", "Smythe");

    #[test]
    fn test_constructor() {

        let n = Name {
            first: JS.0.to_string(),    // str -> String
            last: String::from(JS.1),   // str -> String
        };
        assert_eq!(n.first, JS.0);
        assert_eq!(n.last, JS.1);
    }

    #[test]
    fn test_constructor_field_init_shorthand() {

        let (first, last) = (JS.0.to_string(), JS.1.to_string());
        let n = Name { first, last };        // same as field names
        // first, last: consumed, no longer accessible

        assert_eq!(n.first, JS.0);
        assert_eq!(n.last, JS.1);
    }

    #[test]
    fn test_constructor_struct_update_syntax() {

        let n0 = Name { first: JS.0.to_string(), last: JS.1.to_string() };
        let n1 = Name {
            last: String::from("Smith"),
            ..n0 };
        assert_eq!(n1.first, JS.0);
        assert_eq!(n1.last, "Smith");
    }

    #[test]
    fn test_default() {

        assert_eq!(String::default(), "");

        let n = Name::default();
        assert_eq!(n.first, "");
        assert_eq!(n.last, "");

        let n: Name = Default::default();
        assert_eq!(n.first, "");
        assert_eq!(n.last, "");
    }

    #[test]
    fn test_new() {

        let n = Name::new();
        assert_eq!(n.first, "");
        assert_eq!(n.last, "");
    }

    #[test]
    fn test_new_with_names() {

        let n = Name::new_with_names(JS.0.to_string(), JS.1.to_string());
        assert_eq!(n.first, JS.0);
        assert_eq!(n.last, JS.1);
    }

    #[test]
    fn test_new_with_str() {

        let n = Name::new_with_str(JS.0, JS.1);
        assert_eq!(n.first, JS.0);
        assert_eq!(n.last, JS.1);
    }

    #[test]
    fn test_clone() {

        let n = Name::new_with_str(JS.0, JS.1);
        let o = n.clone();
        assert_eq!(n.first, o.first);
        assert_eq!(n.last, o.last);

        assert!((&n as *const _) != (&o as *const _));  // compare references
    }

    fn hashcode<T: Hash>(t: &T) -> u64 {
        let mut s = DefaultHasher::new();
        t.hash(&mut s);
        s.finish()
    }

    #[test]
    fn test_eq_hash() {

        let n = Name::new();
        assert!(n == n);
        assert!(n.eq(&n));
        assert_eq!(n, n);

        let o = Name::new();
        assert!(n == o);
        assert_eq!(hashcode(&n), hashcode(&o));

        let n0 = Name::new_with_str(JS.0, JS.1);
        assert!(n0 == n0);
        assert!(n0.eq(&n0));
        assert_eq!(n0, n0);
        assert_eq!(hashcode(&n0), hashcode(&n0));

        assert!(n0 != n);
        assert!(n0.ne(&n));
        assert_ne!(n0, n);

        let n1 = n0.clone();
        let n2 = n1.clone();

        assert!(n0 == n1 && n1 == n0);
        assert_eq!(hashcode(&n0), hashcode(&n1));
        assert!(n1 == n2 && n0 == n2);
        assert_eq!(hashcode(&n1), hashcode(&n2));
        assert_eq!(hashcode(&n2), hashcode(&n0));
    }

    #[test]
    fn test_cmp_lt_gt() {

        use std::cmp::Ordering;

        let n0 = Name::new_with_str("Jeremias", "Smith");
        let n1 = Name::new_with_str(JS.0, JS.1);
        let n2 = Name::new_with_str("John", "Smith");

        assert_eq!(n0.cmp(&n1), Ordering::Less);
        assert_eq!(n0.cmp(&n0), Ordering::Equal);
        assert_eq!(n1.cmp(&n0), Ordering::Greater);

        assert!(n0 < n1 && !(n1 < n0));
        assert!(n1 > n0 && !(n0 > n1));
        assert!(n0 < n1 && n1 < n2 && n0 < n2);
        assert!(n2 > n1 && n1 > n0 && n2 > n0);
    }
}

// ----------------------------------------------------------------------
// str/String Converters/Formatters
// ----------------------------------------------------------------------

// can add the {} formatter with .to_string() method
impl Display for Name {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "({}, {})", self.first, self.last)
    }
}

// can add "from"-converter: (&str, &str)) -> Name
//
// Note: cannot also have "into"-converter: (&str, &str)) -> Name
// impl Into<Name> for (&str, &str) { fn into(self) -> Name {...} }
//
impl From<(&str, &str)> for Name {
    fn from(first_last: (&str, &str)) -> Self {
        Self {
            first: String::from(first_last.0),
            last: first_last.1.to_string(),
        }
    }
}

// can add destructive "into"-converter: Name -> (String, String)
//
// Usage: ambigous, type needed:
//   let s: (String, String) = n.into();  // consumes n
//
// Note: no luck defining a non-destructive "into"-converter on &Name
// error: method `into` has an incompatible type for trait
//   impl Into<(String, String)> for &Name { fn into(&self) -> ... }
//
impl Into<(String, String)> for Name {
    fn into(self) -> (String, String) {
        (self.first, self.last)
    }
}

#[cfg(test)]
mod tests1 {
    use super::Name;

    // some test data
    const JS: (&str, &str) = ("Jeremias", "Smythe");

    #[test]
    fn test_to_string() {

        let n = Name::new_with_str(JS.0, JS.1);
        let s = String::from("(") + JS.0 + ", " + JS.1 + ")";
        assert_eq!(s, n.to_string());
    }

    #[test]
    fn test_from_into_tuple() {

        let n = Name::from((JS.0, JS.1));
        assert_eq!(n.first, JS.0);
        assert_eq!(n.last, JS.1);

        // ambigous, type needed:
        let s: (String, String) = n.into();  // consumes n
        assert_eq!(s.0, JS.0);
        assert_eq!(s.1, JS.1);
    }
}

// ----------------------------------------------------------------------
// Tuple Structs + Converters
// ----------------------------------------------------------------------

// can define tuple struct with from/into -converters...
// must generate this much, still:
#[derive(Debug, Hash, Default, Clone, Eq, PartialEq)]
pub struct NameT(pub String, pub String);

// can add "from"-converter: NameT -> Name
impl From<(NameT)> for Name {
    fn from(name: NameT) -> Self {
        Self { first: name.0, last: name.1 }  // struct c'tor
    }
}

// can add "into"-converter: Name -> NameT
impl Into<(NameT)> for Name {
    fn into(self) -> NameT {
        NameT(self.first, self.last)  // tuple struct c'tor
    }
}

#[cfg(test)]
mod tests2 {
    use super::Name;
    use super::NameT;

    // some test data
    const JS: (&str, &str) = ("Jeremias", "Smythe");

    #[test]
    fn test_from_into_tuple_struct() {

        let m = NameT(JS.0.to_string(), JS.1.to_string());
        assert_eq!(m.0, JS.0);
        assert_eq!(m.1, JS.1);

        assert_eq!(m, Name::from(m.clone()).into());
    }
}
