// crates have their own scope
// may have duplicate public definitions in:
//   crate::modules::...
//   crate::more_modules::...

pub fn mult(x: usize, y: usize) -> usize {
    x * y
}

pub mod outer_module {
    use super::mult;

    pub fn square(x: usize) -> usize {
    	mult(x, x)
    }

    pub mod nested_module {
        // hmm, 'super' seems to mean: search all outer scopes
        use super::mult;
        use super::square;

        pub fn cube(x: usize) -> usize {
    	    mult(x, square(x))
        }
    }
}
