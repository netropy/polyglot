// declare crates to be included (from root crate)
//extern crate utils;

// declare modules to be included
pub mod modules;
//pub mod modules_tests;  // declared as logical submodule in modules.rs
pub mod more_modules;  // can declare in modules.rs as well

pub fn add(x: usize, y: usize) -> usize {
    x + y
}

#[cfg(test)]
mod tests {
    //use super::*;
    use super::add;

    #[test]
    fn test_add() {
        assert_eq!(add(2, 2), 4);
    }

    //use crate::modules::mult; // private
    use crate::modules::outer_module::square;
    // crates have their own scope, only the import must be unambigous:
    //use crate::modules::outer_module::nested_module::cube;
    use crate::more_modules::outer_module::nested_module::cube;

    // private: mult
    // #[test]
    // fn test_mult() {
    //     assert_eq!(mult(2, 3), 6);
    // }

    #[test]
    fn test_square() {
        assert_eq!(square(3), 9);
    }

    #[test]
    fn test_cube() {
        assert_eq!(cube(3), 27);
    }

    use utils::div;
    use utils::rem;

    #[test]
    fn test_imported_div() {
        assert_eq!(div(2, 2), 1);
        assert_eq!(div(2, 1), 2);
    }

    #[test]
    fn test_imported_rem() {
        assert_eq!(rem(3, 3), 0);
        assert_eq!(rem(3, 2), 1);
        assert_eq!(rem(3, 1), 0);
    }
}
