#![allow(unused)]

// declare modules_tests a submodule and point to its file
#[cfg(test)]
#[path = "modules_tests.rs"]
pub mod modules_tests;  // tests can now access private members!

// can chain-declare other modules, but must give path
// otherwise, looks for ./modules/...
//#[path = "more_modules.rs"]
//pub mod more_modules;

fn mult(x: usize, y: usize) -> usize {  // PRIVATE
    x * y
}

pub mod outer_module {
    use super::mult;

    pub fn square(x: usize) -> usize {
    	mult(x, x)
    }

    pub mod nested_module {
        // hmm, 'super' seems to mean: search all outer scopes
        use super::mult;
        use super::square;

        pub fn cube(x: usize) -> usize {
    	    mult(x, square(x))
        }
    }
}
