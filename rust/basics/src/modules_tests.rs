// modules.rs declares this module a cfg(test) submodule
//#[cfg(test)]
mod modules_tests {

    // since a submodule, now 'modules' is an outer scope
    use super::super::mult;  // can now access private 'mult'!
    use super::super::outer_module::square;
    use super::super::outer_module::nested_module::cube;

    #[test]
    fn test_mult() {
        assert_eq!(mult(2, 3), 6);
    }

    #[test]
    fn test_square() {
        assert_eq!(square(3), 9);
    }

    #[test]
    fn test_cube() {
        assert_eq!(cube(3), 27);
    }
}
