pub fn div(x: usize, y: usize) -> usize {
    x / y
}

pub fn rem(x: usize, y: usize) -> usize {
    x % y
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_div() {
        assert_eq!(div(2, 2), 1);
        assert_eq!(div(2, 1), 2);
    }

    #[test]
    fn test_rem() {
        assert_eq!(rem(3, 3), 0);
        assert_eq!(rem(3, 2), 1);
        assert_eq!(rem(3, 1), 0);
    }
}
