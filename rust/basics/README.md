# Basic Cargo+Rust Project with Configs, Notes, Experiments

Cargo.toml      -- notes on concepts + configs
src/            -- sample package with lib crate, bin crates, modules
utils/          -- extra package in this workspace

Trying out: multiple packages, multiple executables, separate unit-tests files
```
+- src/
| +- main.rs                  // multiple executables
| +- bin/
| |	+- other_single_src_executable.rs
| |	+- other_multi_src_executable/main.rs
| |	+- other_multi_src_executable/greeter.rs
| |
| +- lib.rs
| +- modules.rs               // module in separate file
| +- modules_tests.rs         // unit tests as submodule but in separate file
| +- more_modules.rs
| |
| +- this_file_ignored.rs     // nowhere declared
|
+-utils/src/                  // separate package
  	   +-lib.rs
  	   +-utils/src/main.rs
```
No problemo.
