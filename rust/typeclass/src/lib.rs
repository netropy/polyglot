#![allow(unused)]

// TODO: cleanup, summarize

use std::convert::{From, Into};

// comparator objects that define a total ordering on a given type T
// this trait has no corresponding type in std::cmp
// (not to be confused with enum std::cmp::Ordering)
pub trait Ordering<T> {
    //fn compare(&self, l: &T, r: &T) -> isize;
    fn compare(l: &T, r: &T) -> isize;
}

// types that offer a canonical, total ordering
// this trait corresponds to trait std::cmp::Ord
pub trait Ordered {
    // compares two instances; returns an Integer to match Java/Scala example
    fn compare_to(&self, other: &Self) -> isize;

    fn lt(&self, other: &Self) -> bool { self.compare_to(other) < 0 }
    fn lteq(&self, other: &Self) -> bool { self.compare_to(other) <= 0 }

    // cannot overload operators: <, <=, ... no suitable trait in std::ops
    //   https://doc.rust-lang.org/std/ops/index.html
    // can only get comparison operators from std::cmp::PartialOrd
    //   https://doc.rust-lang.org/std/cmp/index.html
}

// Rust: user-defined comparisons by taking a comparison or key lambda
// https://doc.rust-lang.org/std/vec/struct.Vec.html#method.sort_by
// https://doc.rust-lang.org/std/vec/struct.Vec.html#method.sort_by_key
//
// read: https://docs.rs/sorting/latest/sorting/  :-)

// ----------------------------------------------------------------------
// Test Type
// ----------------------------------------------------------------------

#[derive(Debug, Default, Clone, Eq, PartialEq)]
//#[derive(Ord, PartialOrd)]  // implementing trait Ordered below
pub struct Name {
    pub first: String,
    pub last: String,
}

impl Name {

    pub fn new() -> Self {
        Self::default()
    }

    pub fn new_with(first: &str, last: &str) -> Self {
        Self { first: first.to_string(), last: last.to_string() }
    }
}

impl From<(&str, &str)> for Name {
    fn from(first_last: (&str, &str)) -> Self {
        Self {
            first: String::from(first_last.0),
            last: String::from(first_last.1),
        }
    }
}

// canonical, total ordering
impl Ordered for Name {
    fn compare_to(&self, other: &Self) -> isize {

        use std::cmp::Ordering;  // using String::cmp(), std::cmp::Ord

        match &self.first.cmp(&other.first) {
            Ordering::Less => -1,
            Ordering::Greater => 1,
            Ordering::Equal =>
                match self.last.cmp(&other.last) {
                    Ordering::Less => -1,
                    Ordering::Greater => 1,
                    Ordering::Equal => 0,
                },
        }
    }
}

//impl Ordering<Name> for dyn Ordering<Name> {
//impl Ordering<Name> for Name {
//fn compare(&self, l: &Name, r: &Name) -> isize {


// need a separate type per trait object
struct NameByLast;

impl Ordering<Name> for NameByLast {
    fn compare(l: &Name, r: &Name) -> isize {

        use std::cmp::Ordering;  // returned by Ord::cmp()

        // try out match expressions:
        match l.last.cmp(&r.last) {
            Ordering::Less => -1,
            Ordering::Greater => 1,
            Ordering::Equal =>
                match &l.first.cmp(&r.first) {
                    Ordering::Less => -1,
                    Ordering::Greater => 1,
                    Ordering::Equal => 0,
                },
        }
    }
}

struct NameByFirst;

impl Ordering<Name> for NameByFirst {
    fn compare(l: &Name, r: &Name) -> isize {

        // try out String's comparison operators:
        if l.first < r.first { -1 }
        else if l.first > r.first { 1 }
        else if l.last < r.last { -1 }
        else if l.last > r.last { 1 }
        else { 0 }
    }
}

struct NameByLength;

impl Ordering<Name> for NameByLength {
    fn compare(l: &Name, r: &Name) -> isize {

        // ...:
        let ll = (l.first.len() + l.last.len()) as isize;  // XXX improve
        let lr = (r.first.len() + r.last.len()) as isize;  // overflow
        ll - lr // (ll as isize) - (lr as isize)
    }
}

// ----------------------------------------------------------------------
// Tests
// ----------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    // test data
    const JS: [(&str, &str); 3] = [
         ("Jeremias", "Smith"),
         ("Jeremias", "Smythe"),
         ("John", "Smith"),
     ];

    #[test]
    fn test_name_ordered() {
        let names = [                  // XXX iterate over str& array
            Name::from(JS[0]),
            Name::from(JS[1]),
            Name::from(JS[2]),
        ];

        let (n0, n1) = (&names[0], &names[1]);
        assert!(n0.compare_to(n1) < 0);
        assert!(n0.lt(n1));
        assert!(n0.lteq(n0));
        assert!(n0.lteq(n1));

        //let r = (&names[0]).compare_to(&names[1]) < 0;
        //assert!((&names[0]).compare_to(&names[1]) < 0);
    }

    #[test]
    fn test_name_ordering_by_first() {
        let names = [
            Name::from(JS[0]),
            Name::from(JS[1]),
            Name::from(JS[2]),
        ];

        let (n0, n1) = (&names[0], &names[1]);
        assert!(NameByFirst::compare(n0, n1) < 0);
    }

    #[test]
    fn test_name_ordering_by_last() {
        let names = [
            Name::from(JS[0]),
            Name::from(JS[1]),
            Name::from(JS[2]),
        ];

        let (n0, n1, n2) = (&names[0], &names[1], &names[2]);
        assert!(NameByLast::compare(n0, n1) < 0);
        assert!(NameByLast::compare(n1, n2) > 0);
        assert!(NameByLast::compare(n2, n0) > 0);
    }

    #[test]
    fn test_name_ordering_by_length() {
        let names = [
            Name::from(JS[0]),
            Name::from(JS[1]),
            Name::from(JS[2]),
        ];

        let (n0, n1, n2) = (&names[0], &names[1], &names[2]);
        assert!(NameByLength::compare(n0, n1) < 0);
        assert!(NameByLength::compare(n1, n2) > 0);
        assert!(NameByLength::compare(n2, n0) < 0);
    }
}

/*
Q: Why not statically dispatched struct + impl?
A: error: associated function in `impl` without body

pub struct OrderingS<T>;

impl<T> OrderingS<T> {
    fn compare(&self, l: &T, r: &T) -> isize;
}

pub struct OrderedS;

impl OrderedS {
    fn lt(&self, other: &Self) -> bool;
    fn lteq(&self, other: &Self) -> bool;
}
 */
