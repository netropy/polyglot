# polyglot/rust

### Summary: Traits in Rust

Traits:
- _interface types with subtyping_
- to define shared behaviour (static method dispatch)
- to abstract over shared behaviour (dynamic method dispatch)
- Rust documentation avoids speaking of _subtyping, interface inheritance_ \*

\* see
[./Rust terminology not calling language features what they are](Notes_unclear_Rust_terminology.md) - notes

Trait features:
| | |
|:---|:---|
| 1    | predefined type: Self |
| 0..* | type parameters with optional bounds (generic traits) |
| 0..* | supertraits |
| 0..* | methods or functions, abstract or defaulted |
| 0..* | generic methods or functions, abstract or defaulted |
| 0..* | type members, abstract or defaulted |
| 0..* | constant members (compile time) |
| 0    | data members |
| 0..* | impl blocks for trait or structs |

Official doc: \
<https://doc.rust-lang.org/book/ch10-01-syntax.html>, \
<https://doc.rust-lang.org/book/ch10-02-traits.html>, \
<https://doc.rust-lang.org/book/ch17-02-trait-objects.html>, \
<https://doc.rust-lang.org/book/ch19-03-advanced-traits.html>, \
<https://doc.rust-lang.org/reference/items/traits.html>, \
<https://doc.rust-lang.org/rust-by-example/generics/gen_trait.html>, \
<https://doc.rust-lang.org/rust-by-example/trait.html>, \
<https://doc.rust-lang.org/rust-by-example/trait/disambiguating.html>, \
<https://doc.rust-lang.org/rust-by-example/trait/impl_trait.html>, \

[basics.rs]: ./experiments/src/traits_basics.rs
[dispatch.rs]: ./experiments/src/traits_static_v_dynamic_dispatch.rs
[generic.rs]: ./experiments/src/traits_generic.rs

#### Trait members, also called _associated items_

\- predefined type _Self_ (+ method parameters _&self = self: &Self_ etc) \
\- function/constant/type members: must be accessed from type _T::m_ not
   from values _t.m_ \
\-> no Scala-equivalent of _path-dependent_ types, constants, functions \
\-> [traits\_static\_v\_dynamic\_dispatch.rs][dispatch.rs]

#### _0..n_ structs/enums/unions may implement _0..m_ traits

\- _orphan rule:_ a type may only implement a trait if at least one is local \
\- _newtype pattern:_ allows to implement external traits on external types \
\-> no performance penalty, newtype wrapper type is elided at compile time \
\-> static _assert\_impl\_{all|any|one}!(\<struct\>: \<list of traits\>)_ \
\-> static _assert\_no\_impl\_{all|any}!(\<struct\>: \<list of traits\>)_ \
\-> [traits\_basics.rs][basics.rs]

#### Support for _nominal typing_

\- trait definitions denote different types even if structurally equal \
\- support for type aliases \
\-> empty traits can serve as type bounds/markers \
\-> static _assert\_type\_{eq|ne}\_all!(\<list of types\>)_ \
\-> [traits\_basics.rs][basics.rs]

#### Support for _subtype polymorphism, interface inheritance_

\- 0..* supertraits \
\- structs must implement all supertraits \
\- trait can access the members of all supertraits \
\- implicit type coercion or explicit up-casting to supertype \
\- support for runtime-checked down-casting to subtype [not shown here] \
\-> static _assert\_{sub|super}\_all!(\<trait\>: \<list of traits\>)_ \
\-> [traits\_basics.rs][basics.rs]

#### Support for _static dispatch_ of trait methods/functions

\- implementation of polymorphic method _m_ selected at compile time \
\-> _S::m(...)_ with implementing _struct S_ \
\-> _s.m(...)_ on values/objects _s_ of _struct_ [pointer] types
    _S, &S, Box\<&S\>, Rc\<&S\>, Arc\<&S\>_ \
\- __no restrictions__ on trait members: \
\-> [traits\_static\_v\_dynamic\_dispatch.rs][dispatch.rs]

#### Support for _dynamic dispatch_ of trait methods (late binding)

\- implementation of polymorphic method _m_ selected at runtime \
\-> _t.m(...)_ on objects _t_ of _dyn_ trait pointer types
    _&dyn T, Box\<&dyn T\>, Rc\<&dyn T\>, Arc\<&dyn T\>_ \
\- _trait object_ = a "fat" trait pointer with dynamic dispatch
   (sometimes also referring to a pointer _type_) \
\- _fat pointer_ = object reference with a vtable pointer \
\- __restrictions__ on trait members: \
\-> traits must be _object-safe_ (no sized _Self_ type, ...) \
\-> methods must be _dispatchable_ (no generic methods, ...) \
\-> [traits\_static\_v\_dynamic\_dispatch.rs][dispatch.rs]

#### NO support for static _shadowing_ of methods, must disambiguate

\- subtraits may "inherit" conflicting default methods from supertraits \
\- subtraits may redefine methods but _don't shadow_ "inherited" definition \
\-> call site must statically disambiguate _\<which trait/type\>::f(self)_ \
\-> cases where static disambiguation was rejected, needed _trait objects_

#### Support for _generic_ methods/functions

\- generic methods are statically dispatched (after monomorphization) \
\- implementation of a polymorphic function _f\<S\>_ selected at compile time

#### Support for traits as _type bounds_ in generic methods/functions

\- _trait bound \<S: T\>_ = type argument for _S_ must implement trait _T_ \
\-> special param/return syntax: _&impl T_ desugars into _\<S : T\> ... &S_ \
\-> multiple _&impl T_ seem to resolve to different types _\<S0, S1...: T\>_

#### Support for _generic_ traits

\- 0..* type parameters with optional type bounds \
\- with static + dynamic dispatch, same restrictions as non-generic traits \
\-> [traits\_generic.rs] [generic.rs]

[Up](./README.md)
