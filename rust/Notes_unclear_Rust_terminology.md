# polyglot/rust

### Notes: Rust terminology not calling language features what they are

Here, a few (subjective) examples of Rust's language documentation and
reference needlessly obfuscating concepts for developers coming from C, Go,
C++, or other languages.


TODO: Place Expressions and Value Expressions, lvalue vs rvalue
https://doc.rust-lang.org/reference/expressions.html#place-expressions-and-value-expressions

#### Inheritance, Subtyping

The Rust Book's section on
[supertraits](https://doc.rust-lang.org/rust-by-example/trait/supertraits.html)
tries hard to avoid speaking of _inheritance_ or _subtyping_ (instead,
speaking of "superset"):

> Rust doesn't have "inheritance", but you can define a trait as being a
> superset of another trait.

But the language _does_ offer subtyping by its very
[definition](https://en.wikipedia.org/wiki/Subtyping):
_types related by substitutability_ ("is-a" relationship).

For example, this code passes an _S_ object as a _T_ (by reference):
```rust
  trait T {}
  struct S;
  impl T for S {}
  fn f(t: &dyn T) {}   // pass as trait object
  fn g(t: &impl T) {}  // trait bound <T0: T>(t: &T0)

  let s: S = S {};
  f(&s);
  g(&s);
  let t: &dyn T = &s;
```
Another example: Rust's supertraits allow to form type unlimited hierarchies
(as in object-oriented languages).

Since subtyping is also known as _inclusion polymorphism_ or _interface
inheritance,_ many developers would recognize Rust as offering this most often
used form of inheritance.

#### Objects vs Values

In non-object-oriented C, the values of
[structs](https://en.cppreference.com/w/c/language/struct).
(and other types) are called _objects_ (as they have an _identity_ in form of
their _address_).

Yet, the official "Rust Language Book" on
[traits](https://doc.rust-lang.org/book/ch17-02-trait-objects.html)
wrongly avoids calling struct values _objects:_

> We’ve mentioned that, in Rust, we refrain from calling structs and enums
> "objects" to distinguish them from other languages' objects. In a struct or
> enum, the data in the struct fields and the behavior in impl blocks are
> separated, whereas in other languages, the data and behavior combined into
> one concept is often labeled an object.

#### Trait Objects

The Rust Book is also fuzzy on the exact meaning of the term _trait object:_

> However, trait objects are more like objects in other languages in the sense
> that they combine data and behavior. But trait objects differ from
> traditional objects in that we can’t add data to a trait object. Trait
> objects aren’t as generally useful as objects in other languages: their
> specific purpose is to allow abstraction across common behavior.

There's a slight contradiction: trait objects "combine data and behavior" yet
"we can’t add data" to them.

Subsequent reading makes clear: trait objects _point_ to an underlying struct
object, which can hold data, _and_ to a (base) trait's function pointer
vtable, for dynamic method dispatch.

So, are trait objects then the _values_ of some ("fat" pointer) type?  It
seems so:

> A trait object points to both an instance of a type implementing our
> specified trait and a table used to look up trait methods on that type at
> runtime.

But then, the subsequent construction _&dyn T_ or _&Box\<T\>_ refer to a
_type,_ which can be used in _let_ statements or method parameters:

> We create a trait object by specifying some sort of pointer, such as a &
> reference or a Box\<T\> smart pointer, then the dyn keyword, and then
> specifying the relevant trait.

This use of trait objects to mean a _type_ is also reflected in other Rust
documentation and some (informal) comments by programmers, where "trait
object" to refers to any usage of the _dyn_ keyword (mandatatory since 2001
edition).

But then, the Rust Standard Library's
[dyn](https://doc.rust-lang.org/std/keyword.dyn.html)
keyword clarifies that:

> dyn is a prefix of a trait object’s type.

Also the Rust Reference defines
[trait object](https://doc.rust-lang.org/reference/types/trait-object.html)
as a _value:_

> A trait object is an opaque value of another type that implements a set of
> traits.

#### Associated Items vs Features/Members

The Rust Reference defines
[associated items](https://doc.rust-lang.org/reference/items/associated-items.html),
which are the functions/methods, types, or constants available on a type, as:

> Associated Items are the items declared in traits or defined in
> implementations. They are called this because they are defined on an
> associate type — the type in the implementation.

It's technically correct that these items are _associated_ in that they _are_
defined in separate _impl_ blocks (if a _struct, enum, union_) or they _can_
be (if a _trait_).

Yet, the set of those items in OO parlance forms a type's _features_ (OO
design) or _members_ (C++, Java).

Why introduce a 7-syllable composite word when there are 2-syllable words that
are widely understood and used with (almost) the same meaning?

[Up](./README.md)
