package netropy.polyglot.scala3

import munit.ScalaCheckSuite
import org.scalacheck.Prop.forAll

class HelloTests extends ScalaCheckSuite:

  property("Hello's greeting starts with 'Hello'") {
    forAll { (s: String) =>
      assert(Hello.greet(s).startsWith("Hello"), "not greeted")
    }
  }

  property("Hello greeter composes") {
    forAll { (s: String) =>
      assertEquals(Hello.greet(s), s"Hello $s", "not composed")
    }
  }
