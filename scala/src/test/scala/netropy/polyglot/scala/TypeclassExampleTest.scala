package netropy.polyglot.scala

import munit.FunSuite

import Sorter.isort

class TypeclassExampleTest extends FunSuite {

  // test data
  val numbers = List(0, 2, 1)

  // test data: to be sorted by first name / last name / combined length
  val names = List(
    Name("Jeremias", "Smythe"),
    Name("Jeremias", "Smith"),
    Name("John", "Smith"))

  test("find typeclass instance in the type+companion") {
    //import Ord.intOrd                          // no need, found in companion

    val l = isort(numbers)                       // 'intOrd' passed implicitly

    assertEquals(l, List(0, 1, 2))
  }

  test("summon typeclass instance, pass explicitly") {
    val ord = implicitly[Ord[Int]]               // captures instance by type

    val l = isort(numbers)(ord)                  // can now pass explicitly

    assertEquals(l, List(0, 1, 2))
  }

  test("import typeclass instance from a type+companion") {
    //import NameOps._                           // Scala2: imports everything
    import NameOps.byLast                        // just this instance

    val l = isort(names)                         // 'byLast' passed implicitly

    assertEquals(l, List(names(1), names(2), names(0)))
  }

  test("pass typeclass instance explicitly") {
    import NameOps._                             // Scala2: gives 'byLast'

    val l = isort(names)(NameOps.byFirst)        // override to pass 'byFirst'

    assertEquals(l, List(names(1), names(0), names(2)))
  }

  test("define a local typeclass instance") {
    implicit val x: Ord[Name] = NameOps.byLength // Scala2: name is mandatory

    val l = isort(names)                         // inferred, passed implicitly

    assertEquals(l, List(names(2), names(1), names(0)))
  }

  test("call the default typeclass methods") {
    import NameOps.byLast

    assert(byLast.lt(Name("Jeremias", "Smith"), Name("Jeremias", "Smythe")))
    assert(byLast.lteq(Name("Jeremias", "Smith"), Name("John", "Smith")))
  }

  test("call the typeclass extension methods") {
    import Ord.OrdOps                            // Scala2: the "rich type"
    import NameOps.byLast

    assert(Name("Jeremias", "Smith") < Name("Jeremias", "Smythe"))
    assert(Name("Jeremias", "Smith") <= Name("John", "Smith"))
  }
}
