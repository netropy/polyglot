package netropy.polyglot.scala

object UnboundedTypeParameters {
  // all the same?
  //   yes! same type after erasure: (exp: Object, act: Object): Unit
  def assertResult(exp: Any)(act: Any): Unit = assert(exp == act)
  // forced type conversions? B ~> A ? probably not
  //def assertResult[A](exp: A)(act: A): Unit = assert(exp == act)
  //def assertResult[A, B](exp: A)(act: B): Unit = assert(exp == act)
  //def assertResult[A, B <: A](exp: A)(act: B): Unit = assert(exp == act)
  //def assertResult[A, B >: A](exp: A)(act: B): Unit = assert(exp == act)
}
