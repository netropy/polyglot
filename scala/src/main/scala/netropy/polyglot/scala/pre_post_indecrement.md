Q: Can two compound assignments `acc *= i; i += 1` be shorted to `acc *= i++`?
A: No, Scala does not offer the unary, C-style, in-/decrement operators.  Their side-effect makes them referentially non-transparent and error-prone.

Note that in C as well, multiple use of a variable in expressions with in- or decrements, like `i - ++i`, has undefined behaviour.

Also, note that '++' and '--' are defined in collection base traits `TraversableLike` and `Subtractable` as binary append/remove operators.

Q: How would one write expressions simulating pre/post, in-/decrements?
A: Unlike C-style languages, assignment operators in Scala do not return the assigned value but `Unit`.  Hence it's necessary to use blocks:
```scala
     var i = ...
     { i += 1; i } // pre-increment
     { val j = i; i -= 1; j } // post-decrement
```
