scala> def f: Int = {
     |   val z = Seq(1,2,3).foldLeft(0){ (z,i) => if (true) return (z + i) ; z } ; println(s"z=$z") ; z }
def f: Int

scala> def f: Int = {
     |   val z = Seq(1,2,3).foldLeft(0){ (z,i) => if (true) return (z + i) ; z }
     |   println(s"z=$z")
     |   z
     | }
def f: Int

scala> f
val res13: Int = 1

scala> def g: Int = {
     |   val z = Seq(1,2,3).foldLeft(0){ (z,i) => if (false) return (z + i) ; z }
     |   println(s"z=$z")
     |   z
     | }
def g: Int

scala> g
z=0
val res14: Int = 0
