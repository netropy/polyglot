// Note: expression '1 to 3' resolves to
//   intWrapper(1).to(3)
// with types
//   intWrapper(1):        scala.runtime.RichInt
//   intWrapper(1).to(3):  scala.collection.immutable.Range.Inclusive

// Note: expression '1L to 3' resolves to
//   longWrapper(1L).to(3)
// with types
//   longWrapper(1L):        scala.runtime.RichLong
//   longWrapper(1L).to(3):  scala.collection.immutable.NumericRange.Inclusive[Long]

// Note: expression '1 to 3L' is invalid
// error: type mismatch;
//   found   : Long(3L)
//   required: Int

// Note:
//   predefined type scala.collection.immutable.Range
//   more efficient than scala.collection.immutable.NumericRange.Inclusive[Int]

scala> (BigInt(1) to 3)
res13: scala.collection.immutable.NumericRange.Inclusive[BigInt] = NumericRange 1 to 3
