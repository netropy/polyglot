package netropy.polyglot.scala

// a comparison function that defines a total ordering on values of a type
trait Ord[T] {

  def compare(x: T, y: T): Int

  // related default methods on Ord[T]: o.lt(x, y), o.lteq(x, y)
  def lt(x: T, y: T): Boolean = compare(x, y) < 0
  def lteq(x: T, y: T): Boolean = compare(x, y) <= 0
}

// some default orderings on basic types
object Ord {

  implicit object intOrd extends Ord[Int] {
    def compare(x: Int, y: Int): Int =
      if (x == y) 0 else if (x > y) 1 else -1
  }

  implicit object stringOrd extends Ord[String] {
    def compare(s: String, t: String) =
      s.compareTo(t)
  }

  // "rich type" to offer methods on T: x < y, x <= y where given an Ord[T]
  implicit class OrdOps[T](x: T)(implicit ord: Ord[T]) {

    def < (y: T): Boolean = ord.lt(x, y)
    def <= (y: T): Boolean = ord.lteq(x, y)
  }
}

// a user-defined type, which happens to come without an ordering
case class Name(first: String, last: String)

// Some orderings (or other features) on Name
//
// ACCEPTABLE design:
// * define extra features in an external "*Ops" object (naming convention)
// - must be imported explicitly or offered conversion (to "enriched type")
// + can be added externally, no modification of companion/class needed
//
// BEST design:
// * define extra features in the companion
// + automatically picked up, no need for import or conversion
// - requires modification of companion/class
//
object NameOps {

  // a default ordering, declared as a given value (name is optional)
  implicit object byLast extends Ord[Name] {
    def compare(x: Name, y: Name): Int = {
      val stringOrd = implicitly[Ord[String]]  // capture a suitable Ord

      val cl = stringOrd.compare(x.last, y.last)
      if (cl != 0) cl
      else stringOrd.compare(x.first, y.first)
    }
  }

  // another ordering, here declared as a regular (singleton) object
  object byFirst extends Ord[Name] {
    def compare(x: Name, y: Name): Int = {
      import Ord.stringOrd  // or import a suitable Ord explicitly

      val cf = stringOrd.compare(x.first, y.first)
      if (cf != 0) cf
      else stringOrd.compare(x.last, y.last)
    }
  }

  // yet another ordering, here as an instance of an anonymous class
  val byLength =
    new Ord[Name] {
      def compare(x: Name, y: Name): Int = {
        val lx = x.first.length + x.last.length
        val ly = y.first.length + y.last.length
        lx - ly
      }
    }
}

// usage example: declare + pass ordering parameters
object Sorter {

  // ACCEPTABLE design:
  // * limit type T with a context parameter
  // * pass a context parameter explicitly
  // - cluttered, noisy code
  // + helps debugging

  def insert[T](x: T, xs: List[T])(implicit ord: Ord[T]): List[T] =  // param
    if (xs.isEmpty || ord.lteq(x, xs.head)) x :: xs
    else xs.head :: insert(x, xs.tail)(ord)       // pass explicitly

  // BEST design:
  // * limit type T with a context bound (desugars into context param)
  // * have the context value inferred and passed implicitly
  // + clean, silent code
  // - hides information

  def isort[T : Ord](xs: List[T]): List[T] =            // context bound
    if (xs.isEmpty) Nil
    else insert(xs.head, isort(xs.tail))                // passed implicitly
}
