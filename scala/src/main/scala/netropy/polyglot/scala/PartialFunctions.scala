package netropy.polyglot.scala


/** Questions, experiments, comments on Partial Function API.
  *
  * - Refresher:
  *       Syntax for PF literals: `{ case ... => ... ; case ... }`
  *
  *       Type `scala.PartialFunction[-A, +B] extends A => B`
  *       - `isDefinedAt(a)` tests if argument `a` is in domain (=matched)
  *       - `apply(a)` may not throw! undefined behaviour where undefined!
  *       => normally, caller must check `isDefinedAt(a)` before `apply(a)`
  *       => pitfall: PFs pass as unary functions, can `apply` w/o knowing
  *
  *       Usage (simplified):
  *       - completion, scala2:
  *           `pf0[A, B] orElse pf1[C, D] => pf2[A with C, lub(B, D)]`
  *       - completion, dotty:
  *           `pf0[A, B] orElse pf1[C, D] => pf2[A & C, B | D]`
  *       - concatenation:
  *           `pf0[A, B] andThen pf1[B, C] => pf2[A, C]`
  *       - `lift`, `unlift`:
  *           `pf[A, B]  <=>  f: A => Option[B]`
  *
  *       => composition: need compatible PF signatures!
  *       => lifting: do computations on [[Option]] Monad level
  *
  * - Lessons:
  *   - composition of PFs may require wide input == output types
  *   - for "endo", can emulate `orElse` by `andThen` with `case`s
  *   - may result in complicated `{ case <pattern> if <cond> => ... }`
  *
  * - How robust is programming with Partial Functions?
  *   - caller's responsibility to only `apply` PF on values where defined:
  *     [[https://www.scala-lang.org/api/current/scala/PartialFunction.html]]
  *   => the caller may want to:
  *     - prior check of arguments: `isDefinedAt`
  *     - use secure calls: `applyOrElse`, `runWith`, `cond`, `condOpt`
  *     - complete the PF with an error fallback using `orElse`
  *     - make the PF total: use `lift` to return `Option` Monad
  *
  *
  * - Question: Discrepency, Scala2doc vs. actual Scala2 `PF.apply` ?
  *
  *     Always got a `MatchError` with PFs trying out various definitions,
  *     compositions on invalid input.
  *
  *     It appears that the actual `apply` implementation _is_ safe!
  *
  *     @see [[https://www.scala-lang.org/api/2.13.3/scala/PartialFunction.html]]
  *     ```
  *         It is the responsibility of the caller to call isDefinedAt before calling apply, because if isDefinedAt is false, it is not guaranteed apply will throw an exception to indicate an error condition. If an exception is not thrown, evaluation may result in an arbitrary value.
  *     ```
  *
  *
  * - Question: 2 type discrepencies, Scala2doc vs. actual Scala2 `orElse`
  *
  *     1) scaladoc: shows to have variance `A1 <: A`, `B1 >: B`
  *        but actually takes any `C`, `D`
  *
  *     2) scaladoc: "returns the union of the domains", i.e., `A | A1 = A`,
  *        but actually returns `A with B`, or `A & B` with dotty
  *
  *     @see [[https://www.scala-lang.org/api/2.13.3/scala/PartialFunction.html]]
  *     ```
  *         def orElse[A1 <: A, B1 >: B](that: PartialFunction[A1, B1]): PartialFunction[A1, B1]
  *
  *         Composes this partial function with a fallback partial function which gets applied where this partial function is not defined.
  *
  *         A1
  *             the argument type of the fallback function
  *         B1
  *             the result type of the fallback function
  *         that
  *             the fallback function
  *         returns
  *             a partial function which has as domain the union of the domains of this partial function and that. The resulting partial function takes x to this(x) where this is defined, and to that(x) where it is not.
  *     ```
  *
  *
  * - Questions: If `apply` is unsafe, why isn't there a safe variant?
  *              If `orElse` is unsafe, why isn't there a "catch-all" value?
  *
  *   See code below for simple method `requireAndApply`, `throwMatchError`.
  *
  *
  * - Question: Why no conversion PF<=>Function1 based on throwing/catching?
  *
  *   Not a useful proposal, but to explore the idea.
  *
  *   Existing conversions based on:
  *   - subtyping `PartialFunction extends Function1`
  *   - `Option`: `partialFunction.lift` <-> `Function.unlift(f)`
  *
  *   Could offer conversion based on throwing/catching.
  *   See code below for `pf.raise`, `F.unraise(f)
  */
trait PartialFunctionExt[-A, +B] extends PartialFunction[A, B] {

  /** Applies this partial function safely by requiring the argument. */
  def requireAndApply(a: A) = {
    require(isDefinedAt(a))
    apply(a)
  }

  /** An alternative to [[Option]]-yielding `lift` based throwing. */
  def raise: A => B =
    x => requireAndApply(x)  // possibly useful
}

object PartialFunctionExt {

  /** A throwing, type-neutral catch-all PF that can be `orElse`d. */
  val throwMatchError: PartialFunction[Any, Nothing] = {
    case x => throw new MatchError(s"No match for input: $x")
  }
}

object FunctionExt {

  /** An alternative to [[Option]]-taking `unlift` based catching. */
  def unraise[A, B](f: A => B) =
    Function.unlift[A, B](a => util.Try(f(a)).toOption)  // not really useful
}
