package netropy.polyglot.scala3

// Scala3: avoid name clash of (generated) classes+files for @main methods
@main def helloWorld(): Unit =
  println("Hello world!")

// Scala3: a @main-annotated method can
// - can be at top-level or inside a statically accessible object
// - can have parameter types for given scala.util.CommandLineParser.FromString
// - can end with a repeated parameter taking all remaining arguments
// - has a class generated with a static main(Array[String]) parsing arguments
@main def helloYou(you: String, others: String*): Unit =
  println(Hello.greet(you))
  for s <- others do println(Hello.greet(s))

object Hello:
  def greet(s: String) = s"Hello $s"

// Scala2 compatibility: scala.App will be deprecated
object HelloScala2App extends App:
  // Scala2 runs this code as scala.Delayedinit, deprecated since 2.11
  // TODO: check that Scala3 runs this code in the object initializer
  // TODO: check difference class/object vs trait extending Delayedinit
  println("Hello!")
