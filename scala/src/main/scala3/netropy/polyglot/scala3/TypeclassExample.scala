package netropy.polyglot.scala3

// a comparison function that defines a total ordering on values of a type
trait Ord[T]:

  def compare(x: T, y: T): Int

  // related default methods on Ord[T]: o.lt(x, y), o.lteq(x, y)
  def lt(x: T, y: T): Boolean = compare(x, y) < 0
  def lteq(x: T, y: T): Boolean = compare(x, y) <= 0

  // make also available as methods on T: x < y, x <= y where given an Ord[T]
  extension (x: T)
    def < (y: T): Boolean = lt(x, y)
    def <= (y: T): Boolean = lteq(x, y)

// some default orderings on basic types
object Ord:

  given intOrd: Ord[Int] with
    def compare(x: Int, y: Int): Int =
      if x == y then 0 else if x > y then 1 else -1

  given stringOrd: Ord[String] with
    def compare(s: String, t: String) =
      s.compareTo(t)

// a user-defined type, which happens to come without an ordering
case class Name(first: String, last: String)

// Some orderings (or other features) on Name
//
// ACCEPTABLE design:
// * define extra features in an external "*Ops" object (naming convention)
// - must be imported explicitly or offered conversion (to "enriched type")
// + can be added externally, no modification of companion/class needed
//
// BEST design:
// * define extra features in the companion
// + automatically picked up, no need for import or conversion
// - requires modification of companion/class
//
object NameOps:

  // a default ordering, declared as a given value (name is optional)
  given byLast: Ord[Name] with
    def compare(x: Name, y: Name): Int =
      val stringOrd = summon[Ord[String]]  // capture a suitable Ord

      val cl = stringOrd.compare(x.last, y.last)
      if cl != 0 then cl
      else stringOrd.compare(x.first, y.first)

  // another ordering, here declared as a regular (singleton) object
  object byFirst extends Ord[Name]:
    def compare(x: Name, y: Name): Int =
      import Ord.stringOrd  // or import a suitable Ord explicitly

      val cf = stringOrd.compare(x.first, y.first)
      if cf != 0 then cf
      else stringOrd.compare(x.last, y.last)

  // yet another ordering, here as an instance of an anonymous class
  val byLength =
    new Ord[Name]:
      def compare(x: Name, y: Name): Int =
        val lx = x.first.length + x.last.length
        val ly = y.first.length + y.last.length
        lx - ly

// usage example: declare + pass ordering parameters
object Sorter:

  // ACCEPTABLE design:
  // * limit type T with a context parameter
  // * pass a context parameter explicitly
  // - cluttered, noisy code
  // + helps debugging

  def insert[T](x: T, xs: List[T])(using ord: Ord[T]): List[T] =  // param
    if xs.isEmpty || ord.lteq(x, xs.head) then x :: xs
    else xs.head :: insert(x, xs.tail)(using ord)       // pass explicitly

  // BEST design:
  // * limit type T with a context bound (desugars into context param)
  // * have the context value inferred and passed implicitly
  // + clean, silent code
  // - hides information

  def isort[T : Ord](xs: List[T]): List[T] =            // context bound
    if xs.isEmpty then Nil
    else insert(xs.head, isort(xs.tail))                // passed implicitly
