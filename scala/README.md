# polyglot/scala

### Scala Experiments, Exercises, Refcards, Notes

_TODO:_ add TOC, Q&A for scala code experiments
_TODO:_ replace ScalaTest with munit for Scala3 support

Tools: \
[sbt](https://www.scala-sbt.org),
[Maven](https://maven.apache.org),
[Gradle](https://gradle.org),
[scala3-migrate](https://github.com/scalacenter/scala3-migrate),
[MUnit](https://scalameta.org/munit),
[ScalaCheck](https://www.scalacheck.org),
[MUnit-ScalaCheck](https://index.scala-lang.org/scalameta/munit/artifacts/munit-scalacheck)

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/scala/),
[Scala3 sources](src/main/scala3/netropy/polyglot/scala/),
[Scala tests](src/test/scala/netropy/polyglot/scala/),
[Scala3 tests](src/test/scala3/netropy/polyglot/scala/)

#### Scala 3: Practical `scalac` options (like `-rewrite`)

```
  -explain          explain errors in more detail
  -deprecation      emit warning for usages of deprecated APIs
  -feature          emit warning for features that should be imported explicitly
  -unchecked        emit warnings where generated code depends on assumptions

  -new-syntax       require `then` and `do` in control expressions
  -old-syntax       require `(...)` around conditions
  -no-indent        require classical {...} syntax, ignore indentation
  -language         enable one or more language features

  -rewrite          with -source *-migration, rewrite old to new syntax
  -source           choices: 3.0, future, 3.0-migration, future-migration
  -indent           with -rewrite, remove {...} syntax when possible
  -pagewidth        set page width; default: 80

  -uniqid  			uniquely tag all identifiers in debugging output
```

#### Scala 2->3 migration tool from Scala Center

Docs: \
[video tutorial](https://www.youtube.com/watch?v=RDY3NMZYWwY),
[Scala 3 Migration Guide](https://docs.scala-lang.org/scala3/guides/migration/compatibility-intro.html)

Requirements: Scala >= 2.13[.5], sbt >= 1.5 \
Usage:
```
  // project/plugins.sbt
  addSbtPlugin("ch.epfl.scala" % "sbt-scala3-migrate" % "0.4.6")

  $ sbt
  > migrate-libs main             	helps updating libraryDependencies
  > migrate-scalacOptions main    	helps updating scalacOptions
  > migrate-syntax main           	fixes syntax of incompatibilities
  > migrate main                  	tries adding inferred types and implicits
```

[Up](../README.md)
