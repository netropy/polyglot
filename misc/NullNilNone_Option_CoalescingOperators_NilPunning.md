# polyglot/misc

TODO: check/discuss
<https://github.com/google/guava/wiki/UsingAndAvoidingNullExplained>

### _null/nil/None_, _Option_ type, Coalescing Operators, and Nil Punning

TLDR:
- languages: most offer _null/nil/None_, a few only _Option_, but some _both_
- _null/nil/None_, _Option_ overlap but also exhibit complementary features
- _nil punning_ and _coalescing operators_ provide features akin to _Option_
- _nil punning_ has pros & cons: (+) practical (-) puzzlers + fineprint
- where _null_ and _Option_ supported, useful to design APIs that offer both

For Scala and Java, see notes on:
[Method Signature Design](Scala_Java_MethodSignatureDesign.md)

TOC:
[[_TOC_]]

#### Concepts: _null/nil/None_, _Option_

- The
  [null pointer / reference](https://en.wikipedia.org/wiki/Null_pointer)
  (depending on the language:)
  `null`, `nil`, `None`, `NULL`, `std::nullptr`, `std::ptr::null`
  denotes the absence of an object to an otherwise
  non-empty pointer or reference.
  ([What is Null?](https://wiki.c2.com/?WhatIsNull))
- The
  [Option type](https://en.wikipedia.org/wiki/Option_type), `Option` (Scala,
  Rust), `Optional` (Java8, Swift), `Maybe` (Haskell), or `std::optional`
  (C++17) offers to express the same information with the unique "empty"
  value: `None`, `Optional.empty()`, `Optional.none`, `Nothing`, or
  `std::nullopt`.
- Algebraic meaning:
  - The Null type `Null`, `std::nullptr_t`, `NoneType` etc is a
    [singleton subtype](https://en.wikipedia.org/wiki/Unit_type)
    of all reference or pointer types (contrast with Dotty _-Yexplicit-nulls_,
	see below).
  - The Option type `Option[+T]`, `Optional<T>`, `std::optional` etc is a
    covariant
    [GADT](https://en.wikipedia.org/wiki/Generalized_algebraic_data_type)
    (algebraic data type with type parameter and specialization).

#### Usage: _null/nil/None_ vs _Option_, or both?

##### _null/nil/None_:

- is _idiomatic_ in most languages
- is (together with zero) often used as the _default initialization_ (by
  the JVM; in C/C++ as zero initialization for statics, thread-locals)
- encodes "absence" most efficiently
  - for example: memory consumption, locality, garbage collection overhead
    of large and long-living (Scala) _Vector[T]_ vs _Vector[Option[T]]_
- may come with _non-strict_ or _coalescing operators_ (see below)
- may be given extra, context-dependent meaning (_nil punning_, see below)

##### _Option_:
- is _idiomatic_ in some functional languages
- _None_ carries useful behaviour, unlike _null/nil_, for example (Scala)
  - as 0/1-element Iterable: _seq :++ opt_, _opt.map(...)_, _opt.toList_
  - as foldable: _opt.fold(...)(...)_, _opt.filter(...)_
  - as monadic: _for (v0 <- opt0; v1 <- opt1) {...}_
  - as zippable: _opt0.zip(opt1).unzip_
  - with recovery: _opt0.orElse(opt1)_, _opt.getOrElse(...)_, _opt.orNull_
- is sometimes the only, good option to encode "absence", for example
  - sorted collections may not hold _null_ values (e.g. SortedSet, TreeSet)
  - only _Option_ can hold non-reference _AnyVal_ values (Scala)
  - Rust does not have a null reference but _optional pointers_ only (outside
    unsafe), based on
    [Rust std::option](https://doc.rust-lang.org/std/option/index.html)
- may come with a linear _for/do_ notation saving nested flatMaps (see below)

##### Languages with both, a _Null_ and _Option_ type (in the std library)

- [Scala Option](https://www.scala-lang.org/api/current/scala/Option.html),
  [Java (>=8) Optional](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Optional.html),
  [C++ (>=17) std::optional](https://en.cppreference.com/w/cpp/utility/optional)
- it seems useful to design APIs that offer both variants, see
  [Scala, Java Method Design](Scala_Java_method_design.md).

#### Are _null_ references/pointers really such a problem?

A [billion-dollar mistake](https://en.wikipedia.org/wiki/Tony_Hoare#Apologies_and_retractions),
  how Tony Hoare characterized introducing _null pointers_ into Algol W.

Scala FP tutorials, some developers, style guides:
  - avoid passing _null_, replace its usage replaced with _Option_
  - for example,
    [scala-lang](https://docs.scala-lang.org/overviews/scala-book/no-null-values.html),
    [AA](https://alvinalexander.com/scala/best-practice-eliminate-null-values-from-code-scala-idioms/),
    [PD](https://www.lucidchart.com/techblog/2015/08/31/the-worst-mistake-of-computer-science/).

In my own experience, though:
- Incorrect _null_ tends to fail fast (unlike, say, race conditions); often,
  _NullPointerExceptions_ (or crashdumps) point to near the root cause.
- NPEs are often revealed during unit- or integration-testing (especially
  with test data generators, e.g., with properties-based testing).
- In contrast, _None_ (_Option_) may propagate more easily, as it nicely
  combines with other information (through _map_, _flatMap_, or _zip_ etc),
  which can make it harder to trace back the origin of an incorrect _None_
  ("coding for the happy path", "where did None come from?").
- Non-nullability is not that expensive to document and check; usually, just a
  1-liner in pre-/post-conditions (_assert_, _require(r != null)_ etc).
  Generally, arguments/results are to be checked at API boundaries anyway.

A similar view on _nil punning_ in Clojure by
[EN](https://lispcast.com/nil-punning/):
- "After a bit of experience with Clojure, I rarely have difficult problems
  with out-of-place nils in pure Clojure code.  However, there is often some
  Java interop – namely, calling Java methods directly – that will cause a
  NullPointerException if the object of the method call is nil.  In these
  cases, wrapping a Java method call in a _(when )_ is often appropriate.  But
  sometimes not, and the NullPointerException is welcome."

Is the case for banning _null_ _overstated_?
- _null/nil/None_ is idiomatic in quite successful, ubiquitous, object-oriented
and functional languages.

#### Means to express non-nullability

Some languages also offer type options to ensure non-nullability:
- [C++ l/r-value references `&`, `&&`](https://en.cppreference.com/w/cpp/language/reference)
  always refer to an existing object (a null reference create by casting is
  undefined by the language).  References are unassignable, though.
- [Kotlin's nullable types `T?`](https://kotlinlang.org/docs/reference/null-safety.html)
  whereas `T` is non-nullable (some fineprint applies regarding default
  initialization by the JVM, same with next).
- [Dotty's "explicit nulls" union type `T | Null`](http://dotty.epfl.ch/docs/reference/other-new-features/explicit-nulls.html).
  The _-Yexplicit-nulls_ compiler flag changes the type hierarchy and removes
  _Null_ from under _AnyRef_, which forces the use of _T | Null_ for nullable
  values.
- Note: In Scala and Java, _lower type bounds_ cannot be used to exclude the
  _Null_ type, since _null_ is a valid value of any reference type.

By convention, idioms:
- In Java, _nullability_ is a general possibility for reference types, so APIs
  should document where _null_ is not accepted or returned.
- In Scala, vice versa, the general expectation is _non-nullability_.
- Wherever a _collection_ or _container_ type is passed, the reference should
  be _non-null_; "absence" is better, solely expressed as empty collection._
- Also, _collection_ and _container_ types are rarely combined with _Option_
  or _null_.  Examples of questionable modelling: option of collection, a map
  having optional keys or values, unordered collection containing nulls._
- (As stated above already:) It's inexpensive and good practice for APIs to
  document and check [non-]nullability as part of a method's pre- or
  post-conditions (usually just a 1-liner).
- P.S.: At the price of a level of indirection, C++, Scala, or Java generics
  naturally allow to define a non-nullable pointer/reference wrapper type
  (not helpful, though, rather pervasive and distracting).

#### Linear _for/do_, _truthy_ values, _null/nil/None-aware_ operators

Navigating a cascade of references `x0.x1.x2 ... .xn` tends to result in a
code pattern with many levels of nested indentation with repeated checks for
_null_ and the return of a default value, also called the
[pyramid of doom](https://en.wikipedia.org/wiki/Pyramid_of_doom_(programming)).

Languages with a _monadic Option_ type (Scala, Haskell) offer a linear
_for/do_ comprehensions, which desugars into nested _flatMap_ calls.

Scala: returns _None_ if any of the options is _None_, otherwise _Some(xn)_
```
  for (x0 <- opt0; x1 <- opt1; ...; xn <- optn) yield xn
  // or, per Scala style guide:
  for {
    x0 <- opt0
    x1 <- opt1
    ...
    xn <- optn
  } yield xn
```

Despite having an _Option type_ Java, Rust (and C++17) do not offer a monadic
_for/do_ notation.

Languages with _truthy/falsey values_ or _nil punning_ (see next) allow for
their use with the short-circuiting,
[non-strict `&&`, `and`, `||`, `or` Boolean operators](https://en.wikipedia.org/wiki/Short-circuit_evaluation)
and the
[ternary `?:` if-then-else operator](https://en.wikipedia.org/wiki/%3F:).

Some languages support a compact notation by providing _null-aware operators_:
- [`?.`, `?[]` (safe call, null-conditional, null-aware member/index access, maybe dot, maybe subscript)](https://en.wikipedia.org/wiki/Safe_navigation_operator)
- [binary `?:` ("elvis", truthy-or)](https://en.wikipedia.org/wiki/Elvis_operator) or
  [`??` (null-coalescing)](https://en.wikipedia.org/wiki/Null_coalescing_operator)
- [Kotlin's `!!` (not-null assertion), `as?` (safe cast)](https://kotlinlang.org/docs/reference/null-safety.html)
- [Kotlin’s java.util.Optional API Equivalents](https://4comprehension.com/kotlins-java-util-optional-equivalents/)
- Python has truthy _and_ and _or_ operators, but as of
  [3.8 still lacks None-aware operators](https://www.python.org/dev/peps/pep-0505/).
- Java, Rust so far do not seem to have any of above operators. Yet they also
  lack a linear _for/do_ notation for their Option type's _flatMap_.

The convenience of _truthy/falsey values_ and _null-aware operators_ may also
have a potential downside: when it leads to complex compositions ("where did
null/nil/None come from?").  This mirrors the above-mentioned downside with
compositions of _Options_ ("where did Option.None come from?").

#### Pros & cons of nil punning

Some languages have a notion of conflating (to varying degree) the literals
or their meaning (depending upon the context): _null_, _nil_, _0_, _false_,
the _empty list_, _empty map_, _empty tuple_ etc.

For example: C/C++ (literals/macro with overlapping meaning _0_, _false_,
_NULL_, _std::nullptr_).  Python (extensive notion of falsey values such as
_None_, _0_, _0.0_, _()_, _[]_, _{}_, _""_ etc).  Lisp (idiomatic _nil_
punning as empty list, false), Clojure (some punning), Scheme (no punning).

For _Clojure_, nicely summarized as _"Every abstraction has a meaning for what
you do with a nil"_ in this
[discussion by EN](https://lispcast.com/what-is-nil-punning/).

For _Scheme_, the lack of nil punning nicely rendered by this _poem_:
[A Short Ballad Dedicated to the Growth of Programs](https://ashwinram.org/1986/01/28/a-short-ballad-dedicated-to-the-growth-of-programs/).

But then, consider this Clojure _puzzler_:
```
  user=> (if (new java.lang.Boolean false) "true" "false")
  "true"
```

[Up](README.md)
