# polyglot/misc

### Q&A: Which categories of runtime checks throw which exception types?

A runtime check, or _conditional failure,_ is code that throws an exception if
a boolean condition holds.

[Conditional Failures Explained](https://github.com/google/guava/wiki/ConditionalFailuresExplained#summary)
(of Google
[Guava](https://github.com/google/guava)),
provides a nice summary table relating: \
kinds of runtime checks <-> meaning of failures ("who messed up") <-> types of
Java exceptions.

My expanded version for Java/Scala/C++ (less readable, though):

| Kind of runtime check | Plain meaning of failure | Java/Scala/C++ constructs to check | Typical Java/Scala exceptions | Typical C++ exceptions/behaviour |
|:---|:---|:---|:---|:---|
| _precondition_ | Method: "Caller, you messed up." | [(scala.Predef) require][15]; _assert,_ _if-then_ ... | [IllegalArgumentException][13a], [IllegalStateException][13b], [UnsupportedOperationException][13c], [AssertionError][11a], ([NullPointerException][13d]), ([IndexOutOfBoundsException][13e]), ([RuntimeException][13]) | [assert/abort][24a] (vs [throw][25a]), [std::invalid_argument][21a], [std::domain\_error][21b], [std::length\_error][21c], [std::out\_of\_range][21d], ([std::logic\_error][21]), ([std::runtime\_error][22]) |
| _assertion_ | Method: "I or someone else messed up." | _assert_ | [AssertionError][11a] | [assert/abort][24a], ([std::logic\_error][21]), ([std::runtime\_error][22]) |
| _postcondition_ | Method: "I messed up." | [(scala.Predef) ensuring][16]; _assert_ | [AssertionError][11a] | [assert/abort][24a] |
| --- | Method: "I am messed up, or someone I depend on is." | --- | [RuntimeException][13] (= all unchecked exceptions), ([scala.UninitializedError][30c]), ([scala.NotImplementedError][30a]) | [std::logic\_error][21], [std::runtime\_error][22], ([std::range\_error][22e]), ([std::overflow\_error][22c]), ([std::underflow\_error][22d]) |
| _verification_ of dynamic calls or casts | JVM: "The caller or the callee messed up." | _(...)...,_ _try-catch,_ use of _java.lang.reflect,_ [scala.util.Try][30d], [(C++) dynamic\_cast][23f] | [ReflectiveOperationException][12a], [ClassCastException][13f],  [scala.ScalaReflectionException][30b] | ([bad_function_call][20c]), ([bad_cast][20b]) |
| _verification_ of resource acquisition+use+release | Method: "Something is messed up right now." | [(Java) try(with resources)][14]; [scala.util.Using][30e]; C++ [RAII][23] principle: [std::unique\_ptr][23a], [std::shared\_ptr][23b], [std::lock\_guard][23c], [std::unique\_lock][23d], [std::shared\_lock][23e] ... | [Exception][12], ([IOException][12b]), ([SQLException][12c]) ... | [std::system\_error][22a], [std::future_error][21e], ([std::runtime\_error][22]), ([std::ios_base::failure][22b]), ([bad_weak_ptr][20d]) |
| _verification_ of other calls | Method: "Someone I depend on messed up." |  _try-catch,_ [scala.util.Try][30d] | [Exception][12] (= all checked exceptions) | [std::runtime\_error][22], [std::exception][20] |
| exceptional result | Method: "No one messed up, exactly -- except the code's authors by not returning a special value." | _if-then_ ... | (user-defined, checked or unchecked exceptions) | (user-defined exceptions) |
| _test assertion_ | Method: "The code I am testing messed up." | _assertXYZ,_ _assert_ | [AssertionError][11a], [RuntimeException][13] | |
| _metaprogramming check_ | Compiler/Runtime: "You, code authors, messed up." | [(scala.Predef) assume][17], [(C++) static\_assert][26] | [AssertionError][11a] | compile error |
| --- | JVM: "I am messed up or simply out of resources; I cannot continue." | --- | [VirtualMachineError][11b], ([OutOfMemoryError][11c]), ([StackOverflowError][11d]) | ([std::bad_alloc][20a]) |
| --- | JVM: "Something is messed up badly; I cannot or should not continue." | --- | [Error][11] | --- |
| --- | Code: "The code's authors messed up by throwing a non-standard object." | --- | any other instance of [Throwable][10] | any value other than [std::exception][20] |

[10]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Throwable.html
[11]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Error.html
[11a]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/AssertionError.html
[11b]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/VirtualMachineError.html
[11c]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/OutOfMemoryError.html
[11d]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/StackOverflowError.html
[12]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Exception.html
[12a]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/ReflectiveOperationException.html
[12b]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/io/IOException.html
[12c]: https://docs.oracle.com/en/java/javase/17/docs/api/java.sql/java/sql/SQLException.html
[13]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/RuntimeException.html
[13a]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/IllegalArgumentException.html
[13b]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/IllegalStateException.html
[13c]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/UnsupportedOperationException.html
[13d]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/NullPointerException.html
[13e]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/IndexOutOfBoundsException.html
[13f]: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/ClassCastException.html
[14]: https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
[15]: https://www.baeldung.com/scala/assert-vs-require
[16]: https://www.artima.com/pins1ed/assertions-and-unit-testing.html
[17]: https://www.geeksforgeeks.org/scala-preconditionsassert-assume-require-ensuring

[20]: https://en.cppreference.com/w/cpp/error/exception
[20a]: https://en.cppreference.com/w/cpp/memory/new/bad_alloc
[20b]: https://en.cppreference.com/w/cpp/types/bad_cast
[20c]: https://en.cppreference.com/w/cpp/utility/functional/bad_function_call
[20d]: https://en.cppreference.com/w/cpp/memory/bad_weak_ptr
[21]: https://en.cppreference.com/w/cpp/error/logic_error
[21a]: https://en.cppreference.com/w/cpp/error/invalid_argument
[21b]: https://en.cppreference.com/w/cpp/error/domain_error
[21c]: https://en.cppreference.com/w/cpp/error/length_error
[21d]: https://en.cppreference.com/w/cpp/error/out_of_range
[21e]: https://en.cppreference.com/w/cpp/thread/future_error
[22]: https://en.cppreference.com/w/cpp/error/runtime_error
[22a]: https://en.cppreference.com/w/cpp/error/system_error
[22b]: https://en.cppreference.com/w/cpp/io/ios_base/failure
[22c]: https://en.cppreference.com/w/cpp/error/overflow_error
[22d]: https://en.cppreference.com/w/cpp/error/underflow_error
[22e]: https://en.cppreference.com/w/cpp/error/range_error
[23]: https://en.cppreference.com/w/cpp/language/raii
[23a]: https://en.cppreference.com/w/cpp/memory/unique_ptr
[23b]: https://en.cppreference.com/w/cpp/memory/shared_ptr
[23c]: https://en.cppreference.com/w/cpp/thread/lock_guard
[23d]: https://en.cppreference.com/w/cpp/thread/unique_lock
[23e]: https://en.cppreference.com/w/cpp/thread/shared_lock
[23f]: https://en.cppreference.com/w/cpp/language/dynamic_cast
[24a]: https://isocpp.org/wiki/faq/exceptions
[25a]: https://learn.microsoft.com/en-us/cpp/cpp/errors-and-exception-handling-modern-cpp
[26]: https://en.cppreference.com/w/cpp/language/static_assert

[30a]: https://www.scala-lang.org/api/3.x/scala/NotImplementedError.html
[30b]: https://www.scala-lang.org/api/3.x/scala/ScalaReflectionException.html
[30c]: https://www.scala-lang.org/api/3.x/scala/UninitializedError.html
[30d]: https://www.scala-lang.org/api/3.x/scala/util/Try.html
[30e]: https://www.scala-lang.org/api/3.x/scala/util/Using$.html

[Up](./README.md)
