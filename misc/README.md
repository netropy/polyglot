# polyglot/misc

### Comments: General Programming Topics, Style Guides

- [When to use Functional/Persistent/Immutable Data Structures?](ImmutableDataStructures.md)
- [null/nil/None, Option type, Coalescing Operators, and Nil Punning](NullNilNone_Option_CoalescingOperators_NilPunning.md)
- [Scala, Java Method Signature Design](Scala_Java_MethodSignatureDesign.md)
- [YAML Style Guide](YAML_StyleGuide.md)

### Q&A, Exercises

#### Q&A: What are Assertions? What purposes do they serve in code?

#### Q&A: Which categories of runtime checks throw which exception types?

A runtime check, or _conditional failure,_ is code that throws an exception
if (and only if) a boolean condition holds.

Exception types are organized as a _class hierachy_ with broad failure
categories at the top.

Relate with another in a table:
- categories of conditional failures
- the failure type's plain english meaning
- top Java exception types
- top C++ exception types

[Remarks](RuntimeChecks_vs_ExceptionTypes.md)

### Resources: Style Guides

#### Scala

[scala-lang Style Guide](https://docs.scala-lang.org/style/) \
[Twitter Effective Scala Guide](https://twitter.github.io/effectivescala/) \
[Databrick Scala Guide](https://github.com/databricks/scala-style-guide)

[scala-lang Scaladoc Style Guide](https://docs.scala-lang.org/style/scaladoc.html),
[scala-lang Scaladoc for Library Authors](https://docs.scala-lang.org/overviews/scaladoc/for-library-authors.html) \
[John's Cheatsheets: Scaladoc Style Guide](https://www.john-cd.com/cheatsheets/Scala/Scaladoc)

#### Java

[Google Java Style Guide](https://google.github.io/styleguide/javaguide.html) \
[Effective Java, Joshua Bloch](https://www.pearson.com/us/higher-education/program/Bloch-Effective-Java-3rd-Edition/PGM1763855.html)

#### C++

[C++ Core Guidelines, Bjarne Stroustrup, Herb Sutter](https://isocpp.github.io/CppCoreGuidelines/) \
[Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html) \

#### C

[C Linux Kernel Coding Style](https://www.kernel.org/doc/html/latest/process/coding-style.html)

#### Go

[Google Go Style Guide](https://google.github.io/styleguide/go)
[Effective Go](https://go.dev/doc/effective_go)

#### Python

[PEP 8 Python Style Guide](https://www.python.org/dev/peps/pep-0008/) \
[Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)

[Up](../README.md)
