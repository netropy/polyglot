# polyglot/misc

#### Q&A: What are Assertions? What purposes do they serve in code?

An _Assertion_ is a statement that checks whether an assumption holds.

More formally, an
[assertion](https://en.wikipedia.org/wiki/Assertion_(software_development))
is
- a [logical proposition](https://en.wikipedia.org/wiki/Proposition)
  at a position in code
- over the variables, parameters, or constants of that program,
- which states an assumption to hold true at that point in code execution.

Assertions
- help to read, write, and document code as they formulate a contract;
- allow to reason about, or even prove, the correctness a block of code;
- make software detect its own defects during testing (or in production);
- formulate the failure criteria in unit/integration/acceptance tests;
- assist metaprogramming by having the compiler check conditions (fail
  compile).

In a block of code, assertions serve different purposes depending on their
location:
- near the beginning: to formulate a _requirement on input data_, i.e., a
  [precondition](https://en.wikipedia.org/wiki/Precondition);
- in the middle: to formulate an _assumption on local or global data_, often
  an [invariant](https://en.wikipedia.org/wiki/Invariant_(computer_science));
- near the exit: to formulate a _guarantee on output data_, i.e., a
  [postcondition](https://en.wikipedia.org/wiki/Postcondition);
- within top-level definitions (e.g., templates): to formulate a _check on
  compile-time data_ (types and constants), i.e., a
  [static assert](https://en.cppreference.com/w/cpp/language/static_assert).

Those purposes cannot be sharply separated: Any assertion states a requirement
on the code before and a guarantee for the code thereafter.

[Up](./README.md)
