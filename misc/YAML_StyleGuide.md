# polyglot/misc

Also see general notes: [YAML](../bigdata/local/YAML_Notes.md)

### YAML Style Guide

- files should have extension: _.yml_ (or _.yaml_)
- use 2 spaces for indentation, _tabs are not allowed_
- always indent child elements (even where not required)
- do not align values (blast radius)
- emphasize structure by newlines, can continue collection by indentation
- label nodes with type or metadata tags
  - primary tag `!foo` (means a local "!foo")
  - secondary tag `!!foo` (means "tag:yaml.org,2002:foo" URL), e.g. `!!str`,
 	`!!seq`, `!!map`, \
	see [language-independent types for YAML](https://yaml.org/type/)
- scalars:
  - for keys or primitive values: use plain/unquoted scalars \
    -> avoid punctuation or white space content
  - for single line strings: prefer double-quoted scalars \
    -> can use generic escapes, including unicode `\x..`, `\u....`,
    `\U........` \
    -> must escape `\\`, `\"`
  - for multi-line strings: \
    -> use literal block `|` or folded block `\>` scalars \
 	-> try avoid optional strip/keep chomp modifiers `-` or `+`
- use a linter, e.g.,
  [yamllint](https://github.com/adrienverge/yamllint)
  in its
  [default configuration](https://yamllint.readthedocs.io/en/stable/configuration.html#default-configuration)

___References:___

[YAML 1.2 Spec](https://yaml.org/spec/1.2/spec.html),
[RedHat YAML](https://www.redhat.com/sysadmin/yaml-tips),
[Flathub YAML](https://github.com/flathub/flathub/wiki/YAML-Style-Guide),
[Strings in YAML](http://blogs.perl.org/users/tinita/2018/03/strings-in-yaml---to-quote-or-not-to-quote.html)

[Up](README.md)
