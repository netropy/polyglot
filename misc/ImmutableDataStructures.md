# polyglot/misc

### When to use Functional/Persistent/Immutable Data Structures?

TLDR:
- for self-contained value types: immutability is great, use as default
- for complex data structures: design for immutability _judiciously_
  - no-brainer benefits: simplicity, robustness, hashing, thread-safety etc
  - consider efficiency: structural sharing, copying, standard operations etc
  - real expertise required: designing a quality immutable collection type
- Scala's support for immutability is _invaluable_ and _unmatched_:
  - developer's initial design decision: model as a _trait/class/case class_
  - when _case class_: immutable by default, not intended for subclassing
  - has all the trimmings of _values_: _equals, hashCode, apply, unapply_ etc
  - many immutable standard types: _Tuple_, _Option_, _Either_, _Try_ etc
  - high-quality immutable collection types: _Vector_, _HashMap_ etc
  - complementary mutable collections, all unified under _scala.collection_

TOC:
[[_TOC_]]

#### Definitions:

A ___functional data structure___ is operated on using only _pure functions_
(hence, it can be implemented in a purely functional language).  Since a pure
function does not change data in-place or perform other side effects, such
data structures are strongly _immutable_ (including changes from a subclass).
Modifying operations always yield a new updated structure.

A ___persistent data structure___ preserves the previous version of itself
when it is modified such that existing references are never changed by
operations.  This makes them _effectively immutable_: modifications never
spread to the aliases of a data structure, nobody else can observe them other
than the mutator itself.

_Suggestion_: drop distinction, subsume as ___immutable data structure___.
This abstracts from the _how_ (pure/impure) in favour of observable behaviour
(immutable) and avoids confusion with _persistent data_ (as understood to
survive program termination).

#### Pros & cons of immutability:

* `+/-` Simplicity:
  * `+` _transparency_: a constant value, referential equality
  * `+` _modularity_: avoids implicit long-distance couplings
  * `+` _composition_: easier to fathom whole when (some) parts don't change
  * `-` _subtyping_: immutability imposes an invariant on a type _hierarchy_
  * `-` _subclassing_: no, immutable classes are often made effectively final
  * `+` _easier_: to evolve data structure for non-strictness, corecursion
  * `-` _harder_: to create or modify circular/arbitrary data structures
  * `-` _hard_: to design data structures for efficiency, structural sharing
* `++` Usage, Safety:
  * `+` _hashing, sorted collections_: ready for use as keys, values, elements
  * `+` _robustness_: no possibility of inadvertent changes, data corruption
  * `+` _thread-safety_: reentrant, ready for concurrent or parallel access
  * `+` _exception safety, failure atomicity_: unchanged, consistent state
* `++` Implementation, Debugging, Testing:
  * `+` no need to check or assert class invariants after initialization
  * `+` no copy constructor, copy-assign, or clone (prefer pass by reference)
  * `+` no move constructor or move-assign (prefer pass by reference)
  * `+` methods such as _updated_ still simple: re-use values, overlay changes
  * `+` no need for synchronization code (mutual exclusion, coordination)
  * `+` debugging: no need for _watchpoints_ or _monitoring_ of object
  * `+` testing: supports properties-based testing, generating of test data
  * `-` must preclude modifications and limit access in class definition
  * `-` must defensively copy any mutable, shared dependencies
  * `-` best to prevent subclassing (or document immutability requirement)
* `+/-` Efficiency, Performance:
  * `+` _call-by-need_: _hashCode_ may use lazy initialization, cache result
  * `+` _memoization_: can cache frequently requested instances
  * `+` _use of reference (vs value) equality_: ready for interning
  * `+` _scalability_: no overhead, bottleneck from synchronizing this object
  * `+` _data sharing, copy-on-write_: no need for pessimistic copying of data
  * _potentially efficient_ or quite _inefficient_, e.g.:
  * `+` change _head_ of singly-linked list: maximum use of data sharing
  * `-` change _last_ of singly-linked list: no possibility of data sharing
  * `-` new object required for each change in value, however small

Summary by Joshua Bloch, [Effective Java][2]:
- "Classes should be immutable unless there’s a very good reason to make them
  mutable. Immutable classes provide many advantages, and their only
  disadvantage is the potential for performance problems under certain
  circumstances. You should always make small value objects, such as
  PhoneNumber and Complex, immutable. (There are several classes in the Java
  platform libraries, such as java.util.Date and java.awt.Point, that should
  have been immutable but aren’t.) You should seriously consider making larger
  value objects, such as String and BigInteger, immutable as well. You should
  provide a public mutable companion class for your immutable class only once
  you’ve confirmed that it’s necessary to achieve satisfactory performance."

#### Implementation:

Scala:
- just define your
  [ADT](https://en.wikipedia.org/wiki/Algebraic_data_type)
  in terms of _sealed/final_, _trait/class_, _case class/object_, or _enum_
  (Scala3) over other, immutable types;
- optionally, declare the primary constructor _private_ and define the
  companion with _apply_ or other factory methods.

More generally:
- protect fields against modification from:
  - this instance or this class (may allow for lazy initialization)
  - inner classes, subclasses, package-private or external classes
- if fields refer to mutable objects:
  - don't allow those objects to be changed, must retain exclusive access
  - create copies upon construction and before sharing, as necessary
- no use of external variables or objects unless immutable; otherwise, copy
- prohibit subclassing (or document that subtypes must guarantee immutability)
  - declare type/class as _sealed_ or as _[effectively] final_ (private c'tor)
  - _example_: final classes
    [Scala BigInt, BigDecimal](https://www.scala-lang.org/api/current/scala/math/index.html)
  - _counterexample_: overridable classes
    [Java BigInteger, BigDecimal](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/math/package-summary.html)
    - may have to check instances that not of an untrusted subclass
    - may want to defensively copy: _new BigInteger(val.toByteArray())_
- if serializable
  - must prevent an attacker from creating a mutable instance
  - consider _serialization proxies_, see below (Effective Java)
- if _type-safe immutability_ is not possible or not practical:
  - consider _views_ whose setters throw an _UnsupportedOperationException_
  - analog to
    [Java unmodifiable view collections](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Collection.html#unmodview)

#### Resources:

- [Functional Programming in Scala, Paul Chiusano and Rúnar Bjarnason, 2014](https://www.manning.com/books/functional-programming-in-scala)
- [Functional Data Structures in C++: Lists, Bartosz Milewski, 2013](https://bartoszmilewski.com/2013/11/13/functional-data-structures-in-c-lists/)
- [Purely Functional Data Structures, Chris Okasaki, 1996](http://www.cs.cmu.edu/~rwh/theses/okasaki.pdf)
- Wikipedia:
  [Purely functional data structure](https://en.wikipedia.org/wiki/Purely_functional_data_structure),
  [Persistent data structure](https://en.wikipedia.org/wiki/Persistent_data_structure),
  [Pure function](https://en.wikipedia.org/wiki/Pure_function),
  [Immutable object](https://en.wikipedia.org/wiki/Immutable_object),
  [Copy-on-write](https://en.wikipedia.org/wiki/Copy-on-write),
  [Interning](https://en.wikipedia.org/wiki/String_interning)
- [Scala _collection  API](https://www.scala-lang.org/api/current/scala/collection/index.html)
- [How to create immutable class in Java](https://howtodoinjava.com/java/basics/how-to-make-a-java-class-immutable/)
- [Java Tutorial: A Strategy for Defining Immutable Objects](https://docs.oracle.com/javase/tutorial/essential/concurrency/imstrat.html)
- [Effective Java, Third Edition, Joshua Bloch, 2018][2]: related items
  - Item 1: Consider static factory methods instead of constructors
  - Item 6: Avoid creating unnecessary objects
  - Item 10: Obey the general contract when overriding equals
  - Item 11: Always override hashCode when you override equals
  - Item 13: Override clone judiciously
  - Item 15: Minimize the accessibility of classes and members
  - Item 16: In public classes, use accessor methods, not public fields
  - _Item 17: Minimize mutability_
  - Item 19: Design and document for inheritance or else prohibit it
  - Item 30: Favor generic methods
  - Item 34: Use enums instead of int constants
  - Item 50: Make defensive copies when needed
  - Item 54: Return empty collections or arrays, not nulls
  - Item 55: Return optionals judiciously
  - Item 63: Beware the performance of string concatenation
  - Item 76: Strive for failure atomicity
  - Item 82: Document thread safety
  - Item 88: Write readObject methods defensively
  - Item 90: Consider serialization proxies instead of serialized instances

[2]: https://www.pearson.com/us/higher-education/program/Bloch-Effective-Java-3rd-Edition/PGM1763855.html

[Up](README.md)
