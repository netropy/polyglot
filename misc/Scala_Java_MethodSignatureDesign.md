# polyglot/misc

### Scala, Java Method Signature Design

#### When to support _null_, _Option_, both versions, throw?

TLDR:
- _null_ should not be passed as _collection_ or _container_ argument/result
- _collection_ or _container types_ rarely combine with _Option_ or _null
- for optional _T_: provide getter + updater for both, _Option[T]_ and _T_
- covering both variants supports complementary usage, choice by clients
- getter + updater for _T_ may have to throw or return/accept _null_
- "getter", "updater" = according to language idiom, uniform access principle

TOC:
[[_TOC_]]

Scala, Java offer _nullable references_ and
[Scala Option](https://www.scala-lang.org/api/current/scala/Option.html),
[Java (>=8) Optional](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Optional.html).

This translates into choices for _signatures_, albeit with [anti-] patterns.

##### 1. _Collection_ types: rarely combined with _null_ or _Option_

- never pass _null_, pass the empty collection (immutable, singleton)
- no good case for an _Option_ of a collection, empty collection should do
- some collections cannot store _null_ (e.g., Scala _SortedSet_, _TreeSet_)
- for _unordered collections_: no good use case for _null/Option elements_
- for _ordered collections_: limited use case for _null/Option elements_
- for _maps_: no good use case for _null/Option_ as keys/values
- no good case for _Option_ of _null/Option_ elements
- if deviating from the above, document case and usage

##### 2. _Non-container types T_: offer getter + updater for _T_ + _Option[T]_

- if _T_ is _mandatory_:
  - offer getter + updater _T_
  - no point offering _Option[T]_ (unless anticipating making optional)
  - if _nullable_, updater _T_ must throw (IllegalArgumentException)
- if _T_ is optional:
  - offer getter + updater _Option[T]_
    - may also want to offer getter _T_ with _hasT_ (or other test)
    - this mirrors Scala Standard Lib design, e.g., _Iterable_:
      [head](https://www.scala-lang.org/api/current/scala/collection/Iterable.html#head:A),
      [headOption](https://www.scala-lang.org/api/current/scala/collection/Iterable.html#headOption:Option[A])
    - mutual conversions: `Option.orNull`, `Option.get`, `Option(nullable)`
  - may not want to offer _setT_ with _clearT_ as it further increases the API
  - if _non-nullable_, getter _T_ must throw (UnsupportedOperationException) \
    (or NoSuchElementException, as preferred by container types)
  - if _nullable_, getter + updater _T_ should return/accept _null_

##### 3. Scala: for _type parameters [T]_ or _abstract type members T_

- _T_ might not be nullable (unless covariant and allowed by type bounds)
- this leads to subtleties with getter + updater for _T_ vs _Option[T]_:
    - clients of the generic API may _not pass null_ to method taking _T_
    - clients of a concrete class _may pass null_ to method taking _T_
    - the implementation in an abstract class may _not return null_ for _T_
    - the implementation of a concrete class _may return null_ for _T_
- so, clients may see that getter returns _null_ but updater does not accept
- may want to add API documention that _Option[T]_ does not have this anomaly

##### 4. For _value types T_:

- Java: use _OptionalLong_, _OptionalInt_, or _OptionalDouble_, \
  avoid _Optional<T>_ (applies option-wrapping + boxing)
- Scala: _Option[T]_

#### Type bounds, co- and contravariance, wildcard type parameters

- TODO (over time): add for Scala, Java...
- Java supports upper but not _lower bound_ type parameters for methods \
  [Why is there no lower bound for type parameters?](http://www.angelikalanger.com/GenericsFAQ/FAQSections/TypeParameters.html#FAQ107) \
  Imperfect workarounds: lower bound _wildcards_, static methods

#### Resources:

- [Effective Java, Third Edition, Joshua Bloch, 2018][1]: related items
  - Item 49: Check parameters for validity
  - Item 50: Make defensive copies when needed
  - Item 51: Design method signatures carefully
  - Item 52: Use overloading judiciously
  - Item 53: Use varargs judiciously
  - Item 54: Return empty collections or arrays, not nulls
  - Item 55: Return optionals judiciously
  - Item 56: Write doc comments for all exposed API elements
- [Java Generics FAQs, Angelika Langer, 2015](http://www.angelikalanger.com/GenericsFAQ/JavaGenericsFAQ.html)

[1]: https://www.pearson.com/us/higher-education/program/Bloch-Effective-Java-3rd-Edition/PGM1763855.html

[Up](README.md)
