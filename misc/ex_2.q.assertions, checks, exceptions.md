# polyglot/testing

### 2 Q&A, Exercise: Programming with Assertions, Checks, and Exceptions

#### 2.1 Q&A: What are Assertions? What purposes do they serve in code?

[Remarks](ex_2_1.r.md)

#### 2.2 Q&A: Which categories of runtime checks throw which exception types?

A runtime check, or _conditional failure,_ is code that throws an exception if
(and only if) a boolean condition holds.  Runtime checks can be grouped into
broad categories.

Similarly, object-oriented languages organize exception types as a _class
hierachy_ with broad failure categories at the top.

Relate with another in a table:
- categories of conditional failures
- top Java exception types
- top C++ exception types
- the failure type's plain english meaning

[Remarks](ex_2_2.r.md)

#### 2.3 Exercise: Write a minimal assertion API for unit testing.

_Context:_ Some professional unit test frameworks and assertion libraries, see
[Notes](Notes_unit_test_frameworks.md),
come with a large API: tens of packages, hundreds of type definitions, tons of
overloaded _assertXYZ_ methods (or functions, or macros), plus fluent
_assertThat_ methods with _matchers_ for various data types etc.

_Question:_ If we were to make a proposal for a _minimalist_ assertion API,
what functions would we identify as most useful to, or most often needed in
unit testing?

Perhaps, just 3 functions: 1 general + 2 specialized
- the language's pre-defined `assert` function for testing any proposition,
- an `assertResult/Equal` for comparing an actual against expected value,
- an `assertThrows` function for verifying an expected exception.

_Tasks:_
1. Write `assertResult`, `assertThrows` in Scala (or other languages).
1. Discuss (and document) method design and implementation decisions.
1. Write a unit test verifying these functions in terms of themselves.

[Remarks](ex_2_3.r.md),
[./asserts_pojo_tests](./asserts_pojo_tests/README.md)

[Up](./README.md)
