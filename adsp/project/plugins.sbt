resolvers += Resolver.jcenterRepo

// implementation of SBT's test interface for JUnit Jupiter
// https://github.com/maichler/sbt-jupiter-interface
// also requires ../build.sbt:
// libraryDependencies += "net.aichler" % "jupiter-interface" % "0.8.4" % Test
addSbtPlugin("net.aichler" % "sbt-jupiter-interface" % "0.8.4")
