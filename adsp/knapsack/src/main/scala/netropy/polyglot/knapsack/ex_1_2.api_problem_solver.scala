package netropy.polyglot.knapsack

/** Type definitions for the simplified 0-1 Knapsack Packing problem.
  *
  * Features:
  * - focus on recursion, models after `KnapsackProblem_v1` using [[Set]]s
  *   for the problem and result data
  * - simplified: no item values as input, implicitely assigns values=weights
  * - uses [[Int]] for item-weights (and values) instead of [[Double]]
  *   (no comparisons with `NaN` or infinity, supports solution strategies)
  * - for convenience, returns items with (redundant) weights and sumWeight
  * - adds convenience constructors and functions for problem/result types
  * - adds pre- and post-conditions for consistency checking & documentation
  */
object SimpleKnapsackProblem:
  type Weight = Int
  type Items[A] = Set[(A, Weight)]

  /** An instance of the simplified 0-1 Knapsack problem (values:= weights). */
  case class P[A](items: Items[A], maxWeight: Weight):
    require(items != null)
    //require(!items.contains(null)) // disabled check -> Scala TreeSet NPE
    require(items.size == items.map(_._1).size)  // distinctness of items
    require(maxWeight >= 0)  // allow for `R.empty.isFeasible(p)` for all `p`

  /** A (partial) result to a simplified 0-1 Knapsack problem instance. */
  case class R[A](items: Items[A], sumWeight: Weight):
    require(items != null)
    //require(!items.contains(null))  // disabled check -> Scala TreeSet NPE
    require(items.toMap.values.sum == sumWeight)  // consistency

    /** Whether this is a (not necessarily optimal) solution to the problem. */
    def isFeasible(problem: P[A]): Boolean =
      items.subsetOf(problem.items) && sumWeight <= problem.maxWeight

  /** Convenience functions (constructors) for result values. */
  object R:
    /** Can be used as neutral/start value under max #items, max weight. */
    def empty[A]: R[A] = R[A](Set(), 0)

    /** Creates an `Out` from the passed `Set` of items. */
    def from[A](items: Items[A]): R[A] =
      R[A](items, items.toMap.values.sum) // (== items.toSeq.map(_._2).sum)


/** A solver (or heuristic) API for the simplified 0-1 Knapsack problem.
  *
  * Features:
  * - adds pre- and post-conditions for consistency checking & documentation
  */
trait SimpleKnapsackSolver:
  import SimpleKnapsackProblem.{P, R}

  /** Returns a [sub-]optimal result to the 0-1 Knapsack problem instance. */
  def solve1[A](in: P[A]): R[A]

  /** Same as `solve1` but additionally checks pre- and post-conditions. */
  final def solve[A](in: P[A]): R[A] = {
    require(in != null)
    solve1(in)
  } ensuring((out: R[A]) => out.isFeasible(in))
