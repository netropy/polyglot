package netropy.polyglot.knapsack

/** A basic solver; abstract types `P`, `R` sketched out in Problem traits. */
trait KnapsackSolver_v0:
  type P                // an instance of the 0-1 Knapsack problem
  type R                // a result to a 0-1 Knapsack problem instance
  def solve(in: P): R   // a heuristic or solver


/** A minimal, math-like modelling of the 0-1 Knapsack Problem.
  *
  * Features:
  * - space+time-efficient representation of weights, values as [[Array]]s
  * - no instantiation of items as objects: just indexes in input+output
  * - space-efficient result vector as [[collection.immutable.BitSet]]
  * - does not ensure the correct cardinality of weights, values by type
  * - no redundant information in output (e.g., final sum value/weight)
  * - recursion cumbersome/inefficient with index-accessed/fixed-size types
  * - closest to traditional, mathematical description:
  *   @see [[https://en.wikipedia.org/wiki/Knapsack_problem]]
  */
trait KnapsackProblem_v0:
  type Value = Double
  type Weight = Double
  type P = (Array[Value], Array[Weight], Weight)  // i.e., (v, w, maxWeight)
  type R = collection.immutable.BitSet

  // assumed invariants (informal):
  //   in.values.length == in.weights.length == out.items.size
  //   sum over weights(out.items) <= in.maxWeight


/** A quick, intuitive modelling of the 0-1 Knapsack Problem.
  *
  * Features:
  * - supports recursion better than the index-based, math-like modelling
  * - explicit representation of items as instances of a type parameter `A`
  * - bundling of item-related information as tuples (vs separate collections)
  * - ensures the correct cardinality of items, weights, values by type
  * - use of [[collection.Set]] in the in+output (vs index-access `Seq`)
  * - problem: distinctness of items in the input not ensured due to tupling
  * - provides the computed sumValue, sumWeight in the output for convenience
  */
trait KnapsackProblem_v1:
  type Item[A] = A
  type Value = Double
  type Weight = Double

  case class P[A](items: Set[(A, Value, Weight)], maxWeight: Weight)
  case class R[A](items: Set[A], sumValue: Value, sumWeight: Weight)

  // assumed invariants (informal):
  //   in.items.map(_._1) distinct
  //   out.sumWeight <= in.maxWeight
  //   out.items.subsetOf(in.items)
  //   out.sumValue/sumWeight == sum over values/weights(out.items)


/** A versatile, elaborate modelling of the 0-1 Knapsack Problem.
  *
  * Features:
  * - similar to the `Set`-based modelling but using [[collection.Map]]
  * - ensures distinctness of the items in the input by type
  * - ensures the correct cardinality of items, weights, values by type
  * - performance of `{Hash,Tree}Map` likely on par with `{Hash,Tree}Set`
  * - returns the computed sumValue, sumWeight in the output for convenience
  * - also returns the (redundant) item values and weights for convenience
  */
trait KnapsackProblem_v2:
  type Item[A] = A
  type Value = Double
  type Weight = Double
  type Items[A] = Map[A, (Value, Weight)]

  case class P[A](items: Items[A], maxWeight: Weight)
  case class R[A](items: Items[A], sumValue: Value, sumWeight: Weight)

  // assumed invariants (informal):
  //   out.sumWeight <= in.maxWeight
  //   out.items.subsetOf(in.items)
  //   out.sumValue/sumWeight == sum over out.items->value/weight


/** A flexible, functional modelling of the 0-1 Knapsack Problem.
  *
  * Features:
  * - similar to the `Map`-based modelling but using [[Function]], [[Iterable]]
  * - `Iterable` allows for use of arbitrary collection types underneath
  * - `Function` allows for use of computation/memoization of weights, values
  * - does not ensure correct cardinality of items, weights, values by type
  * - does not ensures distinctness of the items in the input by type
  * - probably constant-factor overhead of indirection going through function
  * - returns the computed sumValue, sumWeight in the output for convenience
  */
trait KnapsackProblem_v3:
  type Item[A] = A
  type Value = Double
  type Weight = Double
  type Items[A] = Iterable[A]
  type Values[A] = A => Value
  type Weights[A] = A => Weight

  case class P[A](items: Items[A],
    valueOf: Values[A], weightOf: Weights[A], maxWeight: Weight)
  case class R[A](items: Items[A], sumValue: Value, sumWeight: Weight)

  // assumed invariants (informal):
  //   in.items distinct
  //   in.weightOf, in.valueOf total functions (defined on all input)
  //   out.sumWeight <= in.maxWeight
  //   out.items.subsetOf(in.items)
  //   out.sumValue/sumWeight = sum over values/weights(out.items)


/** A push-based, reactive modelling of the 0-1 Knapsack Problem.
  *
  * Features:
  * - the caller receives from the solver all ongoing, feasible solutions
  * - caller provides a callback function through which to receive results
  * - push-based: solver invokes callback on each feasible solution
  * - callback may filter results, discarding any not better than so far best
  * - method `solve` is blocking caller and computing results until having
  *   - enumerated all feasible solutions or
  *   - received `false` as return value from the callback
  * - thus, caller can notify solver when to stop producing more results
  * - alternative: `solve` returns [[Boolean]] indicating exhaustion/abortion
  * - alternative: instead of a callback lambda, use an explicit/SAM-type trait
  */
trait KnapsackProblem_v4:
  // reusing types: Value, ..., P, R, from KnapsackProblem_v2 (or any other)
  case class P[A](
    p: KnapsackProblem_v2#P[A],                 // problem data input
    r: KnapsackProblem_v2#R[A] => Boolean)      // callback receiving output
  type R[A] = Unit                              // no other output

  // assumed invariants: as per KnapsackProblem_v2 (or other)


/** A pull-based, lazy-evaluation modelling of the 0-1 Knapsack Problem.
  *
  * Features:
  * - the caller immediately receives from the solver a `Stream` or `LazyList`
  * - pull-based: a new result is computed when the caller pulls a new element
  * - the caller may filter results, discarding any not better than so far best
  * - however, since streams memoize traversed elements, all computed results
  *   are cached in the stream, regardless of which results caller holds on to
  * - hence, the memory impact could be significant for large problems while
  *   any performance benefit from caching all results might be non-existent
  */
trait KnapsackProblem_v5:
  // reusing types: Value, ..., P, R, from KnapsackProblem_v2 (or any other)
  type P[A] = KnapsackProblem_v2#P[A]            // problem data input
  type R[A] = LazyList[KnapsackProblem_v2#R[A]]  // lazily evaluated output

  // assumed invariants: as per KnapsackProblem_v2 (or other)
