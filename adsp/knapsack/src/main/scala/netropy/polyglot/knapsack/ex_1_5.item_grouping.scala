package netropy.polyglot.knapsack

// also see local import below
import scala.math.Ordering

import SimpleKnapsackProblem._

/** Extension methods on sets of items for grouping or flattening subsets.
  *
  * Under methods `groupByToString` this set of gadgets with weights
  * {{{
  * P(Set(
  *   "Camera"->340, "LaptopPowerAdapter"->350,
  *   "CameraLens1"->120, "CameraLens2"->400, "Laptop"->1300),
  *  1000)
  * }}}
  * is transformed into a set of sets and no change to `maxWeight`:
  * {{{
  * P(Set(
  *   (Set("Camera"->340, "CameraLens1"->120, "CameraLens2"->400), 860),
  *   (Set("Laptop"->1300, "LaptopPowerAdapter"->350), 1650)),
  *  1000)
  * }}}
  *
  * Method `collapse` will fold these subsets into single [[String]]s:
  * {{{
  * P(Set(
  *   ("Set((Camera,340), (CameraLens1,120), (CameraLens2,400))", 860),
  *   ("Set((Laptop,1300), (LaptopPowerAdapter,350))", 1650)),
  *  1000)
  * }}}
  *
  * Method `ungroup` does the inverse of `groupByToString` and flattens a
  * set of set of items into a single set.
  */
object SimpleKnapsackProblemGroupExt:

  /** Set of set of items with their individual and respective sum weights. */
  type Itemss[A] = Items[Items[A]]

  implicit class ItemGroups0[A](in: Items[A]) {

    /** Groups this set of items into subsets whose `toString` prefix-match. */
    def groupByToString: Itemss[A] = {
      import collection.immutable.{TreeSet,HashSet} // uses immutables only

      // a group of items with a common name prefix and their sum weight
      case class Group(name: String, items: Items[A], weight: Weight)

      // traverse items by tail-recursion (stack-safe)
      @annotation.tailrec
      def go(in: Items[A], group: Group, out: Itemss[A]): Itemss[A] = {
        if in.isEmpty then {
          // Scala 2.13:
          //out.incl((group.items, group.weight))       // finalize group
          // Scala 2.12:
          out + ((group.items, group.weight))   // finalize group
        } else {
          val (headSet, tailSet) = in.splitAt(1)
          val item = headSet.head
          val (itemName, itemWeight) = (item._1.toString, item._2)
          if group.name == null then {
            val newGroup = Group(itemName, headSet, itemWeight)
            go(tailSet, newGroup, out)          // start 1st group
          } else if itemName.startsWith(group.name) then {
            val extGroup =
              Group(group.name, group.items + item, group.weight + itemWeight)
            go(tailSet, extGroup, out)          // continue with group
          } else {
            val newGroup = Group(itemName, headSet, itemWeight)
            // Scala 2.13:
            //val extOut = out.incl((group.items, group.weight))
            // Scala 2.12:
            val extOut = out + ((group.items, group.weight))
            go(tailSet, newGroup, extOut)       // start new group
          }
        }
      }

      // sort items by their toString, allows to compare by startsWith
      val ordering = Ordering.String.on[(A, Weight)](_._1.toString)
      // Scala 2.13:
      //val sortedItems = TreeSet.from(in)(ordering)
      // Scala 2.12:
      val sortedItems = TreeSet.apply(in.toSeq: _*)(ordering)
      val emptyGroup = Group(null, HashSet[(A, Weight)](), 0)
      val emptyOut = HashSet[(Items[A], Weight)]()
      go(sortedItems, emptyGroup, emptyOut)
    }
  }

  implicit class ItemGroups1[A](in: Itemss[A]) {

    /** Flattens this set of set of items (inverts `groupByToString`). */
    def ungroup: Items[A] = in.flatMap(
      (cw: (Items[A], Weight)) => cw._1)

    /** Replaces subsets in this set of set of items with their `toString`. */
    def collapse: Items[String] = in.map(
      (cw: (Items[A], Weight)) => (cw._1.toString, cw._2))
  }
