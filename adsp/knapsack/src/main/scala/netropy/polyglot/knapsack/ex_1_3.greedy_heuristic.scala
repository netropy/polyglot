package netropy.polyglot.knapsack

// also see local imports below
import scala.math.Ordering

import SimpleKnapsackProblem.{P, R, Weight}

/*
 * Shows a tail-recursive, and iterative implementation of a greedy algorithm
 * for the simplified Knapsack Problem.
 *
 * The tail-recursive version `GreedySolver_v0` uses the API's input/output
 * types `P[A]` and `R[A]` also for the internal, recursive function (called
 * `go()` or `loop()`, conventionally).  By passing the items in a `TreeSet`
 * with an ordering by weight (as stated in the exercise), the inner function
 * mimics "greediness" without having to express or to know the criterion for
 * selecting the next item.  The `tailrec` annotation ensures that the
 * recursion is compiled into an iteration.
 *
 * The iterative version `GreedySolver_v1` demonstrates mutable collection
 * types (though it could use immutables as well) by holding items in a
 * `mutable.PriorityQueue` and assembling the result in a `mutable.HashSet`
 * together with a local vaiable `sumWeight`.
 */

/** Implements a Greedy Heuristic by tail-recursion on immutable `Set`s. */
object GreedySolver_v0 extends SimpleKnapsackSolver:

  def solve1[A](p: P[A]): R[A] = {
    import collection.immutable.{TreeSet, HashSet} // uses immutables only

    // traverse items by tail-recursion (stack-safe)
    @annotation.tailrec
    def go(p: P[A], r: R[A]): R[A] = { // name by FP convention
      if p.items.isEmpty then
        r
      else {
        val (head, tail) = p.items.splitAt(1)
        val newP = P(tail, p.maxWeight)
        val sum = r.sumWeight + head.head._2
        if sum > p.maxWeight then
          go(newP, r)
        else
          go(newP, R(r.items ++ head, sum))
      }
    }

    // greedy for max-weight: hold items in a tree sorted by -weight
    val ordering = Ordering.Int.on[(A, Weight)](iw => -iw._2)
    // Scala 2.13:
    //val sortedItems = TreeSet.from(p.items)(ordering)
    // Scala 2.12:
    val sortedItems = TreeSet.apply(p.items.toSeq: _*)(ordering)

    go(P(sortedItems, p.maxWeight), R.empty)
  }


/** Implements a Greedy Heuristic by iteration on a mutable `PriorityQueue`. */
object GreedySolver_v1 extends SimpleKnapsackSolver:

  def solve1[A](p: P[A]): R[A] = {
    import collection.mutable.{PriorityQueue, HashSet} // uses mutables only

    // greedy for max-weight: hold items in a heap prioritized by +weight
    // no 2nd-order criterion for items of equal weight, random by heap
    val ordering = Ordering.Int.on[(A, Weight)](iw => iw._2)
    // Scala 2.13:
    //val heap = PriorityQueue.from(p.items)(ordering)
    // Scala 2.12:
    val heap = PriorityQueue.apply(p.items.toSeq: _*)(ordering)

    // traverse items by iteration
    val items = HashSet[(A, Weight)]()
    var sumWeight = 0
    while heap.size > 0 do {
      val item = heap.dequeue()
      val sum = sumWeight + item._2
      if sum <= p.maxWeight then {
        sumWeight = sum
        items += item
      }
    }

    R(items.toSet, sumWeight)
  }
