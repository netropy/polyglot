package netropy.polyglot.knapsack

import SimpleKnapsackProblem.{P, R}


/*
 * Shows a recursive, tail-recursive, and iterative implementation of the
 * backtracking algorithm for the simplified Knapsack Problem.
 *
 * The non-tail-recursive version `BacktrackingSolver_v0` branches into the
 * "incl/excl item" calls storing intermediate `(problem, result)` pairs on
 * the callstack.  With depth-first-search traversal, the stack grows at
 * `O(log n)` in the size of the input, which makes this implementation not
 * stack-safe.
 *
 * The tail-recursive version `BacktrackingSolver_v1` stores the intermediate
 * results in a `List`, which is passed through the recursive calls.  Since
 * the list's access pattern is LIFO (depth-first-search traversal), the use
 * of immutable (linked) `List` is very efficient.  The `tailrec` annotation
 * ensures that the recursion is compiled into an iteration.
 *
 * The iterative version `BacktrackingSolver_v2` stores the intermediate,
 * immutable `(problem, result)` pairs most efficiently on a `mutable.Stack`
 * (which utilizes `mutable.ArrayDeque`).  The currently best solution is
 * held in a local variable of immutable type `R[A]`.
 *
 * On the internal representation of problem/solution data:
 *
 * What all 3 implementations show: the choice of the self-contained tuple
 * type `(P[A], R[A])` for the internal representation of problem instances
 * and (partial) solutions allows for quick coding -- but is wastefull, as
 * redundant `Weight` is massively duplicated.
 *
 * Now, some data sharing by the use of immutable `Set`s ameliorates the
 * data duplication, but overall, the choice of data types is "heavy" on
 * creating objects, including boxing of primitive [[Int]].
 *
 * See subsequent exercises for a changed representation of problem/solution
 * data.
 */

/** Implements a Solver by recursive backtracking. */
object BacktrackingSolver_v0 extends SimpleKnapsackSolver:

  def solve1[A](p: P[A]): R[A] = {

    // traverse items depth-first-search by recursion (not stack-safe)
    //@annotation.tailrec
    def go(p: P[A], r: R[A]): R[A] = {  // name by FP convention
      if r.sumWeight > p.maxWeight then  // infeasible, cut branch
        R.empty  // neutral element
      else if p.items.isEmpty then
        r
      else {
        val (head, tail) = p.items.splitAt(1)
        val tailP = P(tail, p.maxWeight)
        val r0 = go(tailP,
          R(r.items ++ head, r.sumWeight + head.head._2))  // incl head
        val r1 = go(tailP, r)  // excl head

        // reduce the subresults: maximize sumWeight
        if r0.sumWeight > r1.sumWeight then r0 else r1
      }
    }

    go(p, R.empty)
  }


/** Implements a Solver by tail-recursive backtracking on a `List`. */
object BacktrackingSolver_v1 extends SimpleKnapsackSolver:

  def solve1[A](p: P[A]): R[A] = {
    //import collection.immutable.List // default List is optimal for LIFO

    // traverse items depth-first-search by tail-recursion (stack-safe)
    @annotation.tailrec
    def go(prs: List[(P[A], R[A])], opt: R[A]): R[A] = prs match {
      case Nil => opt
      case (p, r) :: rest =>
        if r.sumWeight > p.maxWeight then  // infeasible, cut branch
          go(rest, opt)
        else if p.items.isEmpty then {
          // reduce the subresults: maximize sumWeight
          val newOpt = if opt.sumWeight > r.sumWeight then opt else r
          go(rest, newOpt)
        } else {
          val (head, tail) = p.items.splitAt(1)
          val tailP = P(tail, p.maxWeight)
          val newRest =
            ( (tailP, R(r.items ++ head, r.sumWeight + head.head._2)) ::
              (tailP, r) ::
              rest )
          go(newRest, opt)
        }
    }

    go(List((p, R.empty)), R.empty)
  }


/** Implements a Solver by iterative backtracking on a `Stack`. */
object BacktrackingSolver_v2 extends SimpleKnapsackSolver:

  def solve1[A](p: P[A]): R[A] = {
    import collection.mutable.Stack // efficient mutable LIFO, uses ArrayDeque

    // traverse items depth-first-search by iteration
    val prs = Stack[(P[A], R[A])]((p, R.empty))
    var opt = R.empty[A]
    while prs.size > 0 do {
      val (p, r) = prs.pop()
      if r.sumWeight > p.maxWeight then {
        // infeasible, cut branch
      } else if p.items.isEmpty then {
        // reduce the subresults: maximize sumWeight
        if r.sumWeight > opt.sumWeight then
          opt = r
      } else {
        val (head, tail) = p.items.splitAt(1)
        val tailP = P(tail, p.maxWeight)
        prs.push((tailP, r))
          .push((tailP, R(r.items ++ head, r.sumWeight + head.head._2)))
      }
    }
    opt
  }
