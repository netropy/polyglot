package netropy.polyglot.knapsack

import munit.FunSuite

import SimpleKnapsackProblem.{P, R}
import SimpleKnapsackSolverData.*

trait MaxWeightSolverSuite
  extends BasicKnapsackSolverSuite:  // easiest to collect tests by inheritance

  test("MaxWeightSolver on |items| == 3") {
    assert(solver.solve(P(items3, 1)) == R.empty[String])
    assert(solver.solve(P(items3, 2)) == R(Set("i2"->2), 2))
    assert(solver.solve(P(items3, 3)) == R(Set("i3"->3), 3))
    assert(solver.solve(P(items3, 4)) == R(Set("i4"->4), 4))
    assert(solver.solve(P(items3, 5)) == R(Set("i3"->3, "i2"->2), 5)) // =opt
    assert(solver.solve(P(items3, 6)) == R(Set("i2"->2, "i4"->4), 6))
    assert(solver.solve(P(items3, 7)) == R(Set("i4"->4, "i3"->3), 7))
    assert(solver.solve(P(items3, 8)) == R(Set("i4"->4, "i3"->3), 7))
    assert(solver.solve(P(items3, 9)) == R(Set("i2"->2, "i4"->4, "i3"->3), 9))
    assert(solver.solve(P(items3, 10)) == R(Set("i2"->2, "i4"->4, "i3"->3), 9))
  }

  test("MaxWeightSolver on gadgets example input") {
    val p = P(gadgets, 3000)
    val s = solver.solve(p)
    assert(s.isFeasible(p))
    assert(s.sumWeight == 3000)  // optimum
    //assert(optima.contains(s))  // for debugging
  }

  // for debugging, listing of (some) optimal solutions to the gadget example
  val optima = Set(
    // 6 items
    R(Set(
        ("Camera", 340),
        ("CameraLens1", 120),
        ("CameraLens2", 400),
        ("Laptop", 1300),
        ("Smartphone", 140),
        ("Tablet", 700),
    ), 3000),
    R(Set(
        ("Camera", 340),
        ("CameraLens2", 400),
        ("CameraPowerAdapter", 200),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("Tablet", 700),
    ), 3000),
    R(Set(
        ("Camera", 340),
        ("CameraLens2", 400),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("Tablet", 700),
        ("TabletPowerAdapter", 200),
    ), 3000),
    // 7 items
    R(Set(
        ("Camera", 340),
        ("CameraLens2", 400),
        ("CameraPowerAdapter", 200),
        ("Laptop", 1300),
        ("LaptopPowerAdapter", 350),
        ("SmartphonePowerAdapter", 210),
        ("TabletPowerAdapter", 200),
    ), 3000),
    R(Set(
        ("Camera", 340),
        ("CameraLens1", 120),
        ("CameraPowerAdapter", 200),
        ("Laptop", 1300),
        ("Smartphone", 140),
        ("Tablet", 700),
        ("TabletPowerAdapter", 200),
    ), 3000),
    R(Set(
        ("CameraLens2", 400),
        ("CameraPowerAdapter", 200),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("Smartphone", 140),
        ("Tablet", 700),
        ("TabletPowerAdapter", 200),
    ), 3000),
    // 8 items
    R(Set(
        ("Camera", 340),
        ("CameraLens2", 400),
        ("CameraPowerAdapter", 200),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("LaptopPowerAdapter", 350),
        ("Smartphone", 140),
        ("SmartphonePowerAdapter", 210),
    ), 3000),
    R(Set(
        ("Camera", 340),
        ("CameraLens2", 400),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("LaptopPowerAdapter", 350),
        ("Smartphone", 140),
        ("SmartphonePowerAdapter", 210),
        ("TabletPowerAdapter", 200),
    ), 3000),
    R(Set(
        ("CameraLens1", 120),
        ("CameraLens2", 400),
        ("CameraPowerAdapter", 200),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("Tablet", 700),
        ("TabletPencil", 20),
        ("TabletPowerAdapter", 200),
    ), 3000),
    // 9 items
    R(Set(
        ("Camera", 340),
        ("CameraLens1", 120),
        ("CameraLens2", 400),
        ("CameraPowerAdapter", 200),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("LaptopPowerAdapter", 350),
        ("SmartphonePowerAdapter", 210),
        ("TabletPencil", 20),
    ), 3000),
    R(Set(
        ("Camera", 340),
        ("CameraLens1", 120),
        ("CameraLens2", 400),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("LaptopPowerAdapter", 350),
        ("SmartphonePowerAdapter", 210),
        ("TabletPencil", 20),
        ("TabletPowerAdapter", 200),
    ), 3000),
    // 10 items
    R(Set(
        ("CameraLens1", 120),
        ("CameraLens2", 400),
        ("CameraPowerAdapter", 200),
        ("CameraSpareBattery", 60),
        ("Laptop", 1300),
        ("LaptopPowerAdapter", 350),
        ("Smartphone", 140),
        ("SmartphonePowerAdapter", 210),
        ("TabletPencil", 20),
        ("TabletPowerAdapter", 200),
    ), 3000),
  )


// test backtracking max-weight solver
class BacktrackingKnapsackSolver_v0_Suite
  extends MaxWeightSolverSuite:
  val solver = BacktrackingSolver_v0

class BacktrackingKnapsackSolver_v1_Suite
  extends MaxWeightSolverSuite:
  val solver = BacktrackingSolver_v1

class BacktrackingKnapsackSolver_v2_Suite
  extends MaxWeightSolverSuite:
  val solver = BacktrackingSolver_v2
