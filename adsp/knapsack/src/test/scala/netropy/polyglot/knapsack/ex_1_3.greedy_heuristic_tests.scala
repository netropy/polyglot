package netropy.polyglot.knapsack

import munit.FunSuite

import SimpleKnapsackProblem.{P, R}
import SimpleKnapsackSolverData.*

trait GreedyMaxWeightSolverSuite
  extends BasicKnapsackSolverSuite:  // easiest to collect tests by inheritance

  test("GreedyMaxWeightSolver on |items| == 3") {
    assert(solver.solve(P(items3, 1)) == R.empty[String])
    assert(solver.solve(P(items3, 2)) == R(Set("i2"->2), 2))
    assert(solver.solve(P(items3, 3)) == R(Set("i3"->3), 3))
    assert(solver.solve(P(items3, 4)) == R(Set("i4"->4), 4))
    assert(solver.solve(P(items3, 5)) == R(Set("i4"->4), 4)) // !=opt
    assert(solver.solve(P(items3, 6)) == R(Set("i2"->2, "i4"->4), 6))
    assert(solver.solve(P(items3, 7)) == R(Set("i4"->4, "i3"->3), 7))
    assert(solver.solve(P(items3, 8)) == R(Set("i4"->4, "i3"->3), 7))
    assert(solver.solve(P(items3, 9)) == R(Set("i2"->2, "i4"->4, "i3"->3), 9))
    assert(solver.solve(P(items3, 10)) == R(Set("i2"->2, "i4"->4, "i3"->3), 9))
  }

  test("GreedyMaxWeightSolver on gadgets example input") {
    // greedy result: 6 items
    assertEquals(
      solver.solve(P(gadgets, 3000)),
      R(Set(
        ("CameraLens2", 400),
        ("Laptop", 1300),
        ("LaptopPowerAdapter", 350),
        ("SmartphonePowerAdapter", 210),
        ("Tablet", 700),
        ("TabletPencil", 20)),
        2980))
  }


// test greedy max-weight solver variants (recursive, iterative)
class GreedyKnapsackSolver_v0_Suite
  extends GreedyMaxWeightSolverSuite:
  val solver = GreedySolver_v0

class GreedyKnapsackSolver_v1_Suite
  extends GreedyMaxWeightSolverSuite:
  val solver = GreedySolver_v1
