package netropy.polyglot.knapsack

import munit.FunSuite

import SimpleKnapsackProblem.{P, R}
import SimpleKnapsackProblemGroupExt.{ItemGroups0, ItemGroups1}
import SimpleKnapsackSolverData.*

trait GroupMaxWeightSolverSuite
  extends MaxWeightSolverSuite:  // easiest to collect tests by inheritance

  val groups0 = Set((Set[(String, Int)](), 0))
  val groups1 = Set((Set("a" -> 1, "aa" -> 2), 3))
  val groups2 = Set((Set("aa" -> 2, "a" -> 1), 3), (Set("b" -> 4), 4))
  val groups3 = Set((Set("0" -> 0), 0)) ++ groups2

  test("GroupExt: collapse() on grouped |items| == 0..3") {
    val g0c = groups0.collapse
    assert(
      g0c == Set(("Set()",0)))
    val g1c = groups1.collapse
    assert(
      g1c == Set(("Set((a,1), (aa,2))",3)) ||
      g1c == Set(("Set((aa,2), (a,1))",3)))
    val g2c = groups2.collapse
    assert(
      g2c == Set(("Set((a,1), (aa,2))",3), ("Set((b,4))",4)) ||
      g2c == Set(("Set((aa,2), (a,1))",3), ("Set((b,4))",4)))
    val g3c = groups3.collapse
    assert(
      g3c == Set(("Set((0,0))",0), ("Set((a,1), (aa,2))",3), ("Set((b,4))",4)) ||
      g3c == Set(("Set((0,0))",0), ("Set((aa,2), (a,1))",3), ("Set((b,4))",4)))
  }

  test("GroupExt: ungroup() on grouped |items| == 0..3") {
    assert(groups0.ungroup ==
      Set[(String, Int)]())
    assert(groups1.ungroup ==
      Set("a" -> 1, "aa" -> 2))
    assert(groups2.ungroup ==
      Set("a" -> 1, "aa" -> 2, "b" -> 4))
    assert(groups3.ungroup ==
      Set("0" -> 0, "a" -> 1, "aa" -> 2, "b" -> 4))
  }

  test("GroupExt: ungroup().groupByToString() on |grouped items| == 0..3") {
    assert(groups0.ungroup.groupByToString == groups0)
    assert(groups1.ungroup.groupByToString == groups1)
    assert(groups2.ungroup.groupByToString == groups2)
    assert(groups3.ungroup.groupByToString == groups3)
  }

  test("GroupExt: groupByToString().ungroup() on |items| == 0..3") {
    assert(items0.groupByToString.ungroup == items0)
    assert(items1.groupByToString.ungroup == items1)
    assert(items2.groupByToString.ungroup == items2)
    assert(items3.groupByToString.ungroup == items3)
  }

  test("GroupExt: groupByToString().ungroup() on gadgets example input") {
    assert(gadgets.groupByToString.ungroup == gadgets)
  }

  test("GroupExt: groupByToString().collapse() on gadgets example input") {
    assert(gadgets.groupByToString.collapse == Set(
      ("TreeSet((Camera,340), (CameraLens1,120), (CameraLens2,400), (CameraPowerAdapter,200), (CameraSpareBattery,60))", 1120),
      ("TreeSet((Tablet,700), (TabletPencil,20), (TabletPowerAdapter,200))", 920),
      ("TreeSet((Smartphone,140), (SmartphonePowerAdapter,210))", 350),
      ("TreeSet((Laptop,1300), (LaptopPowerAdapter,350))", 1650))
    )
  }

  test("MaxWeightSolver on gadgets.groupByToString() example input") {
    // optimal solution: 3 groups, max weight
    assertEquals(
      solver.solve(P(gadgets.groupByToString, 3000)),
      R(Set(
        (Set(("Smartphone", 140), ("SmartphonePowerAdapter", 210)), 350),
        (Set(("Tablet", 700), ("TabletPencil", 20), ("TabletPowerAdapter", 200)), 920),
        (Set(("Laptop", 1300), ("LaptopPowerAdapter", 350)), 1650)),
        2920))
  }


// test grouping items and solving with backtracking solver
class BacktrackingKnapsackSolver_v1_GroupSuite
  extends GroupMaxWeightSolverSuite:
  val solver = BacktrackingSolver_v1
