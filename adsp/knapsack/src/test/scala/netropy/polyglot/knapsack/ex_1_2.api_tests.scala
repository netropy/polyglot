package netropy.polyglot.knapsack

import munit.FunSuite

object SimpleKnapsackSolverData:

  val items0 = Set[(String, Int)]()
  val items1 = Set("i1" -> 1)
  val items2 = Set("i1" -> 1, "i0" -> 0)
  val items3 = Set("i2" -> 2, "i4" -> 4, "i3" -> 3)

  val gadgets = Set(
    "Camera" -> 340,
    "CameraLens1" -> 120,
    "CameraLens2" -> 400,
    "CameraPowerAdapter" -> 200,
    "CameraSpareBattery" -> 60,
    "Laptop" -> 1300,
    "LaptopPowerAdapter" -> 350,
    "Smartphone" -> 140,
    "SmartphonePowerAdapter" -> 210,
    "Tablet" -> 700,
    "TabletPencil" -> 20,
    "TabletPowerAdapter" -> 200,
  )


class BasicKnapsackProblemSuite
  extends FunSuite:

  import SimpleKnapsackProblem.{P, R}
  import SimpleKnapsackSolverData._

  test("KnapsackProblem.{P,R}() throw on illegal arguments") {
    intercept[IllegalArgumentException](P(null, 0))
    intercept[IllegalArgumentException](P(Set("i1"->1), -1))
    intercept[IllegalArgumentException](P(Set("i1"->0,"i1"->1), 1))

    intercept[IllegalArgumentException](R(null, 0))
    intercept[IllegalArgumentException](R(Set("i1"->1), 0))
  }

  test("KnapsackProblem.R.{empty,from}() on |items| == 0..2") {
    assert(R.empty[String] == R[String](Set(), 0))
    assert(R.from(items0) == R.empty[String])
    assert(R.from(items1) == R(Set("i1"->1), 1))
    assert(R.from(items2) == R(Set("i1"->1, "i0"->0), 1))
  }

  test("KnapsackProblem.R.isFeasible() on |items| == 0..2") {
    assert(R.from(items0).isFeasible(P[String](Set(), 0)))

    assert(R.empty[String].isFeasible(P(items1, 0)))
    assert(R.from(items1).isFeasible(P(items1, 1)))
    assert(!R.from(items1).isFeasible(P(items1, 0)))
    assert(!R.from(items1).isFeasible(P[String](Set(), 1)))

    assert(R.from(items2).isFeasible(P(items2, 1)))
    assert(R(Set("i1"->1), 1).isFeasible(P(items2, 1)))
    assert(!R.from(items2).isFeasible(P(items2, 0)))
    assert(!R.from(items2).isFeasible(P[String](Set(), 1)))
  }
