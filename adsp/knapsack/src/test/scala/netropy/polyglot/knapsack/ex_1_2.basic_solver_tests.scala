package netropy.polyglot.knapsack

import munit.FunSuite

import SimpleKnapsackProblem.{P, R}
import SimpleKnapsackSolverData.*

trait HasSimpleKnapsackSolver:
  val solver: SimpleKnapsackSolver  // injected by bottom test class

trait BasicKnapsackSolverSuite
  extends FunSuite
  with HasSimpleKnapsackSolver:  // easiest to collect tests by inheritance

  test("KnapsackSolver.solve() on |items| == 0") {
    assert(R.empty[String] == solver.solve(P(items0, 0)))
    assert(R.empty[String] == solver.solve(P(items0, 1)))
  }

  test("KnapsackSolver.solve() on |items| == 1") {
    assert(R.empty[String] ==
      solver.solve(P(items1, 0)))
    assert(items1.subsets().map(R.from(_)).contains(
      solver.solve(P(items1, 1))))
    assert(items1.subsets().map(R.from(_)).contains(
      solver.solve(P(items1, 2))))
  }

  test("KnapsackSolver.solve() on |items| == 2") {
    assert(Set("i0"->0).subsets().map(R.from(_)).contains(
      solver.solve(P(items2, 0))))
    assert(items2.subsets().map(R.from(_)).contains(
      solver.solve(P(items1, 1))))
    assert(items2.subsets().map(R.from(_)).contains(
      solver.solve(P(items1, 2))))
  }
