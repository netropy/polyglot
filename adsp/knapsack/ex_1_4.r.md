##### R: 1.4 Implement a _Backtracking Algorithm_ with unit tests.

[Backtracking  algorithms](https://en.wikipedia.org/wiki/Backtracking) are a
class of exhaustive search algorithms for finding all (or some) solutions to
discrete optimization or constraint satisfaction problems by incrementally
builds candidates to the solutions while abandoning a candidate as soon as it
is found that the candidate cannot possibly be completed to a valid solution.

Since backtracking enumerates all feasible solutions, finding a global optimum
is guaranteed, but the computational complexity also makes it impractical for
[NP-hard problems](https://en.wikipedia.org/wiki/NP-hardness).

__The Implemented Backtracking Algorithm__

The algorithm enumerates all feasible solutions by depth-first-search
traversal of a binary tree.  At each inner tree node, an item from the input
set is in-/excluded in the candidate set.  However, branches that cannot
possibly be completed to a feasible solution (already exceeding max weight),
are abandoned.  Thus, all reached leaf nodes represent feasible solutions.
As soon as two solutions become available, that is, at up-traversal of the
tree, they are compared under the objective function, discarding the lesser.

Here, the implemented goal is to maximize the number of items, and as a
2nd-order ciriterion, a preference for larger weight (to mimic greedy).

In general terms, the selection at up-traversal represents a _reduce_
operation on all feasible solutions under associative selection
`max(a, b) = f(a) > f(b) ? a : b`, given objective function `f`.

Note that _Backtracking_ is typically stated as a sequential algorithm.  It
can be generalized to
[Branch & Bound](https://en.wikipedia.org/wiki/Branch_and_bound)
with parallelism at both, downward and upward tree-traversal:
- The enumeration of solutions in the rootet tree (_state space search_) is
  data-independent in the different branches.
- The selection is an associative operation.  It also is commutative without
  a preference among multiple, optimal solutions (can be done in any order).

__Scala__: See the scaladoc for the use of [im-]mutable collection types.

Ensures per `tailrec` annotation that tail-recursion is compiled into an
iteration.

[scala code](src/main/scala/netropy/polyglot/knapsack/ex_1_4.backtracking_solver.scala) \
[scala tests](src/test/scala/netropy/polyglot/knapsack/ex_1_4.backtracking_solver_test.scala)

__Python__: See the module's docstring for design & code discussion.

Why Python will likely never support:
[tail-recursion elimination, tail-call optimization](../../q&a/tail-recursion elimination, tail-call optimization)

[python code](py/knapsack/ex_1_4_backtracking_solver.py) \
[python tests](py/knapsack/test_ex_1_4_backtracking_solver.py)
