# -*- coding: utf-8 -*-
"""
Implementations of a Backtracking Solver for the simplified Knapsack Problem.

This module features three implementations:
- knapsack_backtracking_solve_v0(): recursive, callstack, mutable sets
- knapsack_backtracking_solve_v1(): tail-recursive, deque, immutable sets
- knapsack_backtracking_solve_v2(): iterative, deque, mutable sets

These versions reflect different choices (see details further below) on
a) how the algorithm's descent and ascent is formulated,
b) where and how intermediate problems and results are stored, and
c) what container type is used for holding the input (and output) items.

To access these three functions through the SimpleKnapsackSolver Protocol,
they're also provided as global objects: BACKTRACKING_KNAPSACK_SOLVER_V{0..2}

Code Discussion:

The backtracking algorithm is a depth-first-search traversal of a binary tree
that includes/excludes an item in the solution at each inner tree node.  It
therefore may enumerate all possible combinations, running in O(2**n) time in
the size of the item input set.  However, branches that cannot possibly be
completed to a feasible solution (here: already exceeding max_weight) are
abandoned.  Any two feasible results are immediatly compared and reduced under
the goal function, which is given as maximizing the sum weight.

On the choices a) and b): The tail-recursive and iterative versions need to
store the intermediate problems+results.  With depth-first-search traversal,
the access pattern is stack-like.  This suggests a container type that is
growable on (at least) one end, such as unsynchronized 'collections.deque'
or a synchronized/concurrent version of {queue|asyncio}.LifoQueue.

Note, however, the callstack growth rates of these code variants -- as the
CPython standard core distribution does not (and likely never will, see Q&A)
employ Tail-Recursion Elimination (TRE).  While versions v1() and v2() both
store the bulk of intermediate data on heap-allocated memory, the lack of
TRE translates every visited node in the branch tree into a nested call,
each resulting in a frame on the Python interpreter's callstack.

Code version's max depth of the Python callstack in input size n:
- v0(), recursive:      O(log n) => stack-unsafe
- v1(), tail-recursive: O(2**n) => stack-explosive
- v2(), iterative:      O(1) => stack-safe

See the unittests module comments in test_ex_1_4_backtracking_solver.py for
the need to increase sys.setrecursionlimit() to make the tests pass.

=> CPython's lack of TRE turns tail-recursion from a stack-safe programming
practice in functional and procedural languages (translating it as iteration)
into a forbidden pattern.

On the choice of c): Similar to ex_1_3_greedy_heuristic.py, the backtracking
algorithm's performance benefits from a collection type that is either
mutable or, if immutable, offers efficient add/removal operations (e.g., by
internal data sharing).  Otherwise, the removal of an item from the local
input set would require copying the set twice (one clone for each branch).
This is the case when using the (non-data-sharing) immutable API type
'kn.P[Item]' also for the internal representation of items.

The code versions show different ways to hold intermediate problems/results:
- v0(): as (item, weight) tuples in mutable sets
- v1(): as immutable kn.P[Item] and (mutable) kn.R[Item]
- v2(): most efficiently, as Items in mutable sets

Outside the scope of this exercise are a discussion of the pros&cons of
immutable data (e.g., for use of functional parallelism).

Uses PEP 484, 526 type annotations for functions+variables.
"""

from typing import TypeVar, Set, Tuple, Deque, List, Dict
from collections import deque

from knapsack_py import ex_1_2_knapsack_api as kn

# type aliases for PEP 484/526-style annotations
Item = TypeVar('Item')  # type variable for generics (cannot import kn.Item)
Weight = kn.Weight  # type alias


def knapsack_backtracking_solve_v0(problem: kn.P[Item]) -> kn.R[Item]:
    """Implements a backtracking algorithm by recursion on mutable sets.

    Picks an item, recursively branches into in-/excluding the item in the
    solution, drops a branch if infeasible, and reduces the solutions under
    the (implicit) goal function, which prefers larger sum weight.

    See code discussion in this module's doc: This version holds and passes
    intermediate problem+result data as mutable types Set and kn.R.
    """

    # local constants
    max_weight: Weight = problem.max_weight

    # name of local function per FP convention, pylint: disable=invalid-name
    def go(items: Set[Tuple[Item, Weight]], r: kn.R[Item]) -> kn.R[Item]:
        """Traverses items by recursion (stack-unsafe).
        See code discussion in this module's doc on callstack growth rate.
        This inner, recursive function is typically named go() or loop().
        """

        if r.sum_weight > max_weight:  # infeasible, cut branch
            return kn.R()  # neutral element

        if not items:  # finished branch
            return r

        head: Tuple[Item, Weight] = items.pop()  # mutable Set offers pop()
        sum_w: Weight = r.sum_weight + head[1]

        # Since using mutable sets for input, each branch needs its own copy.
        # Same with result data, must be able to diverge between branches.
        new_p: Set[Tuple[Item, Weight]] = set(items)  # input copy
        new_r: kn.R[Item] = kn.R(r.items | {head}, sum_w)  # result copy
        r0: kn.R[Item] = go(new_p, new_r)  # incl head, uses the copies
        r1: kn.R[Item] = go(items, r)  # excl head, re-uses input+result

        # reduce the subresults: prefer larger sum_weight
        return r0 if r0.sum_weight > r1.sum_weight else r1

    # copy the immutable input to Set, start with neutral element as result
    return go(set(problem.items), kn.R())


def knapsack_backtracking_solve_v1(problem: kn.P[Item]) -> kn.R[Item]:
    """Implements a backtracking algorithm by tail-recursion on mutable deque.

    Picks a (problem, result) tuple from the deque, tests if infeasible,
    tests if leaf node reducing its result with the current optimum, or
    picks an item, creates branches in-/excluding the item in the solution,
    and adds them onto the deque.

    See code discussion in this module's doc: This version holds intermediate
    problem+result data as immutable types kn.P[Item], kn.R[Item].
    """

    # local constants
    max_weight: Weight = problem.max_weight
    Item1 = TypeVar('Item1')  # need unbound type variable for generic alias
    PRs = Deque[Tuple[kn.P[Item1], kn.R[Item1]]]  # generic type alias

    # name of local function per FP convention, pylint: disable=invalid-name
    def go(prs: PRs[Item], opt: kn.R[Item]) -> kn.R[Item]:
        """Traverses nodes by tail-recursion (stack-explosive w/o TRE).
        See code discussion in this module's doc on callstack growth rate.
        This inner, recursive function is typically named go() or loop().
        """

        if not prs:  # finished branches
            return opt

        (p, r) = prs.pop()  # get next branch
        rest: PRs[Item] = prs

        if r.sum_weight > max_weight:  # infeasible, cut branch
            return go(rest, opt)

        if not p.items:  # finished branch
            # reduce the subresults: prefer larger sum_weight
            new_opt: kn.R[Item] = opt if opt.sum_weight > r.sum_weight else r
            return go(rest, new_opt)

        # The choice of immutable, AbstractSet-based API type kn.P[Item] for
        # intermediate data precludes calling pop().  This results 2x extra
        # copying set -> list =>> set, as each branch needs its own copy.
        # The result data must be cloned as well to allow to diverge.
        # See knapsack_backtracking_solve_v2() for avoiding extra copying.
        items: List[Tuple[Item, Weight]] = list(p.items)  # copies items
        head: Tuple[Item, Weight] = items.pop()
        sum_w: Weight = r.sum_weight + head[1]
        new_p0: kn.P[Item] = kn.P(set(items), max_weight)  # copies items
        new_p1: kn.P[Item] = kn.P(set(items), max_weight)  # copies items
        new_r: kn.R[Item] = kn.R(r.items | {head}, sum_w)  # copies r.items

        rest.append((new_p1, new_r))  # incl head, uses items+result copies
        rest.append((new_p0, r))  # excl head, uses items copy, re-uses result
        return go(rest, opt)

    # init the deque with the immutable input and neutral element as result
    return go(deque([(problem, kn.R())]), kn.R())


def knapsack_backtracking_solve_v2(problem: kn.P[Item]) -> kn.R[Item]:
    """Implements a backtracking algorithm by iteration on mutable deque, set.

    Same algorithm as knapsack_backtracking_solve_v1() but pushes and pops
    intermediate problems+results on the deque from a loop.

    See code discussion in this module's doc: This version holds intermediate
    problem+result data as triple of 2 mutable Set[Item], and a sum_weight.
    This is the most space- and time-efficient representation among the code
    variants v0..v2().
    """

    # local constants, pylint: disable=too-many-locals
    max_weight: Weight = problem.max_weight
    weight_of: Dict[Item, Weight] = dict(problem.items)  # hold once only
    Item1 = TypeVar('Item1')  # need unbound type variable for generic alias
    PR = Tuple[Set[Item1], Set[Item1], Weight]  # (problem, result, sum_weight)

    # initialize (problem, result, sum_weight), currently best solution
    items: Set[Item] = set(dict(problem.items).keys())  # easiest->annotation
    prs: Deque[PR[Item]] = deque([(items, set(), 0)])
    best: kn.R[Item] = kn.R()

    while prs:  # unfinished branches
        (p_items, r_items, r_sum_w) = prs.pop()  # take next branch

        if r_sum_w > max_weight:  # infeasible, cut branch
            continue

        if not p_items:  # finished branch
            # reduce the subresults: prefer larger sum_weight
            if best.sum_weight < r_sum_w:
                # not strictly necessary to create the full output type here
                i_w: Set[Tuple[Item, Weight]] = \
                    set(map(lambda i: (i, weight_of[i]), r_items))
                best = kn.R[Item](i_w, r_sum_w)
            continue

        head: Item = p_items.pop()  # take some item

        # each branch needs its own set of problem/result items
        pr0: PR = (set(p_items), r_items | {head}, r_sum_w + weight_of[head])
        pr1: PR = (p_items, r_items, r_sum_w)
        prs.append(pr1)  # excl head, re-uses items, re-uses result
        prs.append(pr0)  # incl head, uses items+result copies

    return best

#
# Backtracking solvers as stateless SimpleKnapsackSolver objects
#

# For convenience, and to support unit-testing, this module also exports the
# solver functions (heuristics) as SimpleKnapsackSolver-typed objects.
#
# The resulting class definitions are boiler-plate.  (The pylint complaints
# "Too few public methods" and "Method could be a function" rather reflect
# the inability in Python to define subtyped singleton objects directly.)
#
# In contrast to ex_1_3_greedy_heuristic.py, nominal subtyping is shown here.

# pylint: disable=too-few-public-methods,missing-function-docstring
class BacktrackingKnapsackSolverV0(kn.SimpleKnapsackSolver):
    """knapsack_backtracking_solve_v0() as SimpleKnapsackSolver object."""
    def solve(self, problem: kn.P[Item]) -> kn.R[Item]:
        return knapsack_backtracking_solve_v0(problem)

# pylint: disable=too-few-public-methods,missing-function-docstring
class BacktrackingKnapsackSolverV1(kn.SimpleKnapsackSolver):
    """knapsack_backtracking_solve_v1() as SimpleKnapsackSolver object."""
    def solve(self, problem: kn.P[Item]) -> kn.R[Item]:
        return knapsack_backtracking_solve_v1(problem)

# pylint: disable=too-few-public-methods,missing-function-docstring
class BacktrackingKnapsackSolverV2(kn.SimpleKnapsackSolver):
    """knapsack_backtracking_solve_v1() as SimpleKnapsackSolver object."""
    def solve(self, problem: kn.P[Item]) -> kn.R[Item]:
        return knapsack_backtracking_solve_v2(problem)

BACKTRACKING_KNAPSACK_SOLVER_V0 = BacktrackingKnapsackSolverV0()
"""Function knapsack_backtracking_solve_v0() as SimpleKnapsackSolver object."""

BACKTRACKING_KNAPSACK_SOLVER_V1 = BacktrackingKnapsackSolverV1()
"""Function knapsack_backtracking_solve_v1() as SimpleKnapsackSolver object."""

BACKTRACKING_KNAPSACK_SOLVER_V2 = BacktrackingKnapsackSolverV2()
"""Function knapsack_backtracking_solve_v2() as SimpleKnapsackSolver object."""
