# -*- coding: utf-8 -*-
"""
Type definitions for modelling the simplified Knapsack Problem.

Provides:
- Generic type aliases for representing items, weights and collections.
- Dataclass definitions for representing problem data, result data.
- Protocol class for defining a solver or heuristic per structural or
  nominal subtyping.

Design Goal: Educational, experimental API

Displays a mix of data types to check with recursive/iterative implementations:
- mutable and immutable, abstract and concrete collection (container) types
- tuples, (namedtuples), data classes, property decorators
- generics, type variables, type aliases
- convenience factory methods, checking of preconditions
- fully type-annotated, documented

Uses PEP 484, 526 type annotations for functions+variables.
Uses PEP 544 for structural subtyping via Protocols.
Uses PEP 557 for data classes.
"""

from typing import Tuple, AbstractSet, Set, TypeVar, Generic
from typing import Type  # false positive: pylint: disable=unused-import
from typing import Protocol  # >= python 3.8, structural subtyping via PEP 544
from dataclasses import dataclass, field

#
# Types for problem + solution data of the simplified Knapsack Problem
#

# type aliases for PEP 484/526-style annotations
Item = TypeVar('Item')  # type variable for generic types
Weight = int  # type alias


# short class name for test data literals & prototyping (not production code)
@dataclass(frozen=True)
# pylint: disable=invalid-name
class P(Generic[Item]):
    """An instance of the simplified 0-1 Knapsack problem (values:=weights).

    This class bundles items, their weights, and a max weight as input to
    method solve() (declared below).

    As a @dataclass, instances can be readily created, printed, and compared.
    Additional methods provide for a type+value consistency check and for a
    convenience factory.  See the [q&a/python/data classes vs namedtuples.md]
    for a feature summary of @dataclass vs namedtuple()/typing.NamedTuple.

    The (contravariant) input position in solve() invites to try out 2 ideas:
    - Use the broader type AbstractSet instead of Set for items to allow for
      passing FrozenSets as well as Sets as constructor argument.
    - Since AbstractSet denotes immutability, this also hints to the caller
      of solve() that this function does not alter the passed set of items.
    - Declare this dataclass as immutable (frozen=True).  This hints to the
      caller that solve() does not alter the input instance itself.

    Note, however, that caution must be applied when hashing instances of this
    class.  While the combination of frozen=True with (default) eq=True causes
    the @dataclass decorator issue a __hash__() method, the items themselves
    may be mutable, or the passed set instance, affecting this object's hash.
    """
    # mutable default values require 'field', no-arg callable default factory
    items: AbstractSet[Tuple[Item, Weight]] = field(default_factory=set)
    max_weight: Weight = 0

    def __post_init__(self) -> None:
        """This dataclass method allows for extra initialization actions."""
        self.assert_consistency()

    def assert_consistency(self) -> None:
        """Checks the fields' types+values against the stated invariants."""
        # instance type checks not allowed for parameterized generics
        # assert isinstance(self.items, AbstractSet[Tuple[Item, Weight]])
        assert isinstance(self.items, AbstractSet)  # also catches: None
        assert all(isinstance(t, tuple) for t in self.items)
        assert all(i is not None for i in list(dict(self.items).keys()))
        assert len(self.items) == len(dict(self.items).keys())  # distinctness
        assert isinstance(self.max_weight, int)
        assert self.max_weight >= 0
        assert all(isinstance(i, int) for i in list(dict(self.items).values()))
        assert all(i >= 0 for i in list(dict(self.items).values()))


# short class name for test data literals & prototyping (not production code)
@dataclass
# pylint: disable=invalid-name
class R(Generic[Item]):
    """A (partial) result to a simplified 0-1 Knapsack problem instance.

    This class bundles items, their weights, and a sum weight as result of
    method solve() (declared below).

    As a @dataclass, instances can be readily created, printed, and compared.
    Additional methods provide for: a type+value consistency check, a test
    whether this result is a feasibile solution to a given problem instance,
    and for two convenience factory methods.

    The (covariant) result position in solve() invites to apply these ideas:
    - Use the narrower type Set instead of AbstractSet for items so that the
      caller can modify the item set directly and wouldn't have to copy it.
    - This also allows the implementations of solve() to directly append to
      the item set in a result object.
    - Declare this dataclass as mutable (default: frozen=False) so that both,
      implementation and caller, may update any fields.

    Note that under defaults frozen=False and eq=True, decorator @dataclass
    sets __hash__() to None, marking this mutable class unhashable.  See
    Python's library documentation on @dataclass for combinations involving
    eq=True and unsafe_hash=True.
    """
    # mutable default values require 'field', no-arg callable default factory
    items: Set[Tuple[Item, Weight]] = field(default_factory=set)
    sum_weight: Weight = 0

    def __post_init__(self) -> None:
        """This dataclass method allows for extra initialization actions."""
        self.assert_consistency()

    def assert_consistency(self) -> None:
        """Checks the fields' types+values against the stated invariants."""
        # instance type checks not allowed for parameterized generics
        # assert isinstance(self.items, AbstractSet[Tuple[Item, Weight]])
        assert isinstance(self.items, AbstractSet)  # also catches: None
        assert all(isinstance(t, tuple) for t in self.items)
        assert all(i is not None for i in list(dict(self.items).keys()))
        assert len(self.items) == len(dict(self.items).keys())  # distinctness
        assert isinstance(self.sum_weight, int)
        assert self.sum_weight >= 0
        assert all(isinstance(i, int) for i in list(dict(self.items).values()))
        assert all(i >= 0 for i in list(dict(self.items).values()))
        # extra consistency condition on sum_weight
        assert self.sum_weight == sum(dict(self.items).values())

    def is_feasible(self, problem: P[Item]) -> bool:
        """Tests whether this result is a solution to the passed problem."""
        self.assert_consistency()
        return (
            self.items <= problem.items
            and self.sum_weight <= problem.max_weight)

    @classmethod
    def of(cls: 'Type[R[Item]]',
           items: AbstractSet[Tuple[Item, Weight]]) -> 'R[Item]':
        """Convenience factory creating an R from any passed set of items."""
        return cls(set(items), sum(dict(items).values()))

#
# Structural Type for Heuristics/Solvers for the simplified Knapsack Problem
#

class SimpleKnapsackSolver(Protocol):
    """A protocol for heuristics/solvers for the simplified Knapsack Problem.

    Can be used, for example, in type-annotated unit tests that want to
    invoke different solver implementations through a common interface.

    Implementing classes can either just provide a compliant solve() method
    ("duck tupying") or nominally subtype this class.
    """

    # false positive, pylint: disable=too-few-public-methods
    def solve(self, problem: P[Item]) -> R[Item]:
        "Returns a solution ensuring solution.is_feasible(problem)."
        # ... empty method body
