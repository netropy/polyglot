# -*- coding: utf-8 -*-
"""
Exercise 1.2: Unit-tests the SimpleKnapsackSolver provided by subclasses.

Uses PEP 484, 526 type annotations for functions+variables.
"""

from typing import Sequence

from knapsack_py import ex_1_2_knapsack_api as kn
from knapsack_py.test import test_ex_1_2_api as knp

# To pass a solver into the class, pytest requires to create a "fixture".
# However: easier to just parametrize the test functions to take a solver.
#
# Therefore, since pytest's test discovery scans for class+function names
# starting or ending with "Test" (modulo capitalization, plus other rules):
# - the name of this class must _escape_ test discovery,
# - the names of the functions must _not_ escape test discovery.
class BasicKnapsackSolverTestSuite:
    "Unit tests taking and running a SimpleKnapsackSolver on |items| == 0..2"
    solver: kn.SimpleKnapsackSolver

    # unittest names allowed to be irregular, pylint: disable=invalid-name
    def test_KnapsackSolver_items0(
            self, solver: kn.SimpleKnapsackSolver) -> None:
        """tests a SimpleKnapsackSolver on |items| == 0 data."""

        assert kn.R() == solver.solve(kn.P(knp.ITEMS0, 0))
        assert kn.R() == solver.solve(kn.P(knp.ITEMS0, 1))

    # unittest names allowed to be irregular, pylint: disable=invalid-name
    def test_KnapsackSolver_items1(
            self, solver: kn.SimpleKnapsackSolver) -> None:
        """tests a SimpleKnapsackSolver on |items| == 2 data."""

        # Would be better to generate than enumerate the possible solutions.
        # Sadly, Python's sets/containers do not offer a predefined function
        # 'powerset()' or 'subsets()'.  However, it can be written using
        # itertools.{chain, combinations}.  Not worth doing it here, though.
        items1_solutions: Sequence[kn.R] = [
            kn.R(),
            kn.R.of({('i1', 1)}),
        ]
        assert solver.solve(kn.P(knp.ITEMS1, 0)) == kn.R()
        assert solver.solve(kn.P(knp.ITEMS1, 1)) in items1_solutions
        assert solver.solve(kn.P(knp.ITEMS1, 2)) in items1_solutions

    # unittest names allowed to be irregular, pylint: disable=invalid-name
    def test_KnapsackSolver_items2(
            self, solver: kn.SimpleKnapsackSolver) -> None:
        """tests a SimpleKnapsackSolver on |items| == 2 data."""

        # Would be better to generate than enumerate the possible solutions.
        # Sadly, Python's sets/containers do not offer a predefined function
        # 'powerset()' or 'subsets()'.  However, it can be written using
        # itertools.{chain, combinations}.  Not worth doing it here, though.
        items2_solutions: Sequence[kn.R] = [
            kn.R(),
            kn.R.of({('i0', 0)}),
            kn.R.of({('i1', 1)}),
            kn.R.of({('i0', 0), ('i1', 1)}),
        ]
        assert solver.solve(kn.P(knp.ITEMS2, 0)) in items2_solutions
        assert solver.solve(kn.P(knp.ITEMS2, 1)) in items2_solutions
        assert solver.solve(kn.P(knp.ITEMS2, 2)) in items2_solutions
