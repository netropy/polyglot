# -*- coding: utf-8 -*-
"""
Exercise 1.2: Unit-tests the basic types of the simplified Knapsack Problem.

Uses PEP 484, 526 type annotations for functions+variables.
"""

from typing import AbstractSet, Set, Tuple, cast

import pytest

from knapsack_py import ex_1_2_knapsack_api as kn

#
# Test Data for the simplified Knapsack Problem:
#

Items = AbstractSet[Tuple[kn.Item, kn.Weight]]
ITEMS0: Items = frozenset()
ITEMS1: Items = frozenset({('i1', 1)})
ITEMS2: Items = frozenset({('i1', 1), ('i0', 0)})
ITEMS3: Items = frozenset({('i2', 2), ('i4', 4), ('i3', 3)})

GADGETS: Items = frozenset({
    ('Camera', 340),
    ('CameraLens1', 120),
    ('CameraLens2', 400),
    ('CameraPowerAdapter', 200),
    ('CameraSpareBattery', 60),
    ('Laptop', 1300),
    ('LaptopPowerAdapter', 350),
    ('Smartphone', 140),
    ('SmartphonePowerAdapter', 210),
    ('Tablet', 700),
    ('TabletPencil', 20),
    ('TabletPowerAdapter', 200),
})


#
# Unit Tests for the SimpleKnapsackProblem P/R Types:
#

# unittest names allowed to be irregular, pylint: disable=invalid-name
def test_KnapsackProblem_P_check_illegal_arguments() -> None:
    """test SimpleKnapsackProblem.kn.P() raising expected exceptions"""
    ItemsP = AbstractSet[Tuple[kn.Item, kn.Weight]]
    # check presence of expected exceptions via context manager:
    # pylint: disable=multiple-statements
    with pytest.raises(AssertionError): kn.P(cast(ItemsP, None), 0)
    with pytest.raises(AssertionError): kn.P(cast(ItemsP, {None}), 0)
    with pytest.raises(AssertionError): kn.P(cast(ItemsP, {(None, 1)}), 0)
    with pytest.raises(AssertionError): kn.P(cast(ItemsP, {('i1', None)}), 0)
    with pytest.raises(AssertionError): kn.P({('i', 0)}, cast(kn.Weight, None))
    with pytest.raises(AssertionError): kn.P({('i', -1)}, 0)
    with pytest.raises(AssertionError): kn.P({('i', 1)}, -1)

# unittest names allowed to be irregular, pylint: disable=invalid-name
def test_KnapsackProblem_R_check_illegal_arguments() -> None:
    """test SimpleKnapsackProblem.kn.R() raising expected exceptions"""
    ItemsR = Set[Tuple[kn.Item, kn.Weight]]
    # check presence of expected exceptions via context manager:
    # pylint: disable=multiple-statements
    with pytest.raises(AssertionError): kn.P(cast(ItemsR, None), 0)
    with pytest.raises(AssertionError): kn.P(cast(ItemsR, {None}), 0)
    with pytest.raises(AssertionError): kn.P(cast(ItemsR, {(None, 1)}), 0)
    with pytest.raises(AssertionError): kn.P(cast(ItemsR, {('i1', None)}), 0)
    with pytest.raises(AssertionError): kn.P({('i', 0)}, cast(kn.Weight, None))
    with pytest.raises(AssertionError): kn.P({('i', -1)}, 0)
    with pytest.raises(AssertionError): kn.P({('i', 1)}, -1)
    # extra consistency condition on sum_weight
    with pytest.raises(AssertionError): kn.R({('i', 1)}, 0)

# unittest names allowed to be irregular, pylint: disable=invalid-name
def test_KnapsackProblem_P_empty_of() -> None:
    """test SimpleKnapsackProblem.kn.P()"""
    assert kn.P[str]() == kn.P[int]()  # type annotation ignored
    assert kn.P() == kn.P[str](set(), 0)

# unittest names allowed to be irregular, pylint: disable=invalid-name
def test_KnapsackProblem_R_empty_of() -> None:
    """test SimpleKnapsackProblem.kn.R.{empty,of}() on |items| == 0..2"""
    assert kn.R[str]() == kn.R[int]()  # type annotation ignored
    assert kn.R() == kn.R[str](set(), 0)
    assert kn.R.of(ITEMS0) == kn.R()
    assert kn.R.of(ITEMS1) == kn.R({('i1', 1)}, 1)
    assert kn.R.of(ITEMS2) == kn.R({('i1', 1), ('i0', 0)}, 1)

# unittest names allowed to be irregular, pylint: disable=invalid-name
def test_KnapsackProblem_R_is_feasible() -> None:
    """KnapsackProblem.kn.R.is_feasible() on |items| == 0..2"""
    assert kn.R.of(ITEMS0).is_feasible(kn.P(set(), 0))

    assert kn.R().is_feasible(kn.P(ITEMS1, 0))
    assert kn.R.of(ITEMS1).is_feasible(kn.P(ITEMS1, 1))
    assert not kn.R.of(ITEMS1).is_feasible(kn.P(ITEMS1, 0))
    assert not kn.R.of(ITEMS1).is_feasible(kn.P(set(), 1))

    assert kn.R.of(ITEMS2).is_feasible(kn.P(ITEMS2, 1))
    assert kn.R({('i1', 1)}, 1).is_feasible(kn.P(ITEMS2, 1))
    assert not kn.R.of(ITEMS2).is_feasible(kn.P(ITEMS2, 0))
    assert not kn.R.of(ITEMS2).is_feasible(kn.P(set(), 1))
