# -*- coding: utf-8 -*-
"""
Exercise 1.4: Unit-tests the Backtracking Solvers for simplified Knapsack.

Uses PEP 484, 526 type annotations for functions+variables.
"""

from typing import List

import sys

import pytest

from knapsack_py import ex_1_2_knapsack_api as kn
from knapsack_py import ex_1_4_backtracking_solver as kns
from knapsack_py.test import test_ex_1_2_api as knp
from knapsack_py.test import test_ex_1_2_basic_solver as knbs

# necessary to increase the interpreter's recursion limit
_RECURSION_LIMIT = 10000
print(
    """
    ######################################################################

    The 'gadget' example with |items|=12 blows up CPython's default
    recursion limit as the CPython core distribution does not (and likly
    never will) apply Trail Recursion Elimination.

    To avoid RecursionErrors, this test suite requires to increase the
    recursion limit (maximum depth of the Python interpreter stack):
    """)
print(f"    OLD: sys.getrecursionlimit()={sys.getrecursionlimit()}")
sys.setrecursionlimit(_RECURSION_LIMIT)
print(f"    NEW: sys.getrecursionlimit()={sys.getrecursionlimit()}")
print(
    """
    ######################################################################
    """)


@pytest.mark.parametrize(
    "solver",
    [kns.BACKTRACKING_KNAPSACK_SOLVER_V0,
     kns.BACKTRACKING_KNAPSACK_SOLVER_V1,
     kns.BACKTRACKING_KNAPSACK_SOLVER_V2,
    ])
class TestOptMaxWeightSolverSuite(knbs.BasicKnapsackSolverTestSuite):
    "Unit tests running OptMaxWeight solver on items3, gadgets."

    # unittest names allowed to be irregular, pylint: disable=invalid-name
    def test_OptMaxWeightSolver_items3(
            self, solver: kn.SimpleKnapsackSolver) -> None:
        "Tests an OptMaxWeight solver on |items| == 3 data."
        assert (
            solver.solve(kn.P(knp.ITEMS3, 1))
            == kn.R())
        assert (
            solver.solve(kn.P(knp.ITEMS3, 2))
            == kn.R({('i2', 2)}, 2))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 3))
            == kn.R({('i3', 3)}, 3))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 4))
            == kn.R({('i4', 4)}, 4))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 5))
            == kn.R({('i3', 3), ('i2', 2)}, 5))  # =opt
        assert (
            solver.solve(kn.P(knp.ITEMS3, 6))
            == kn.R({('i2', 2), ('i4', 4)}, 6))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 7))
            == kn.R({('i4', 4), ('i3', 3)}, 7))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 8))
            == kn.R({('i4', 4), ('i3', 3)}, 7))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 9))
            == kn.R({('i2', 2), ('i4', 4), ('i3', 3)}, 9))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 10))
            == kn.R({('i2', 2), ('i4', 4), ('i3', 3)}, 9))

    # unittest names allowed to be irregular, pylint: disable=invalid-name
    def test_BacktrackingSolver_gadgets(
            self, solver: kn.SimpleKnapsackSolver) -> None:
        "Tests an OptMaxWeight solver on gadgets example data."

        p: kn.P[str] = kn.P(knp.GADGETS, 3000)
        r: kn.R[str] = solver.solve(p)

        assert r.is_feasible(p)
        assert r.sum_weight == 3000  # optimum
        #assert r in optima  # for debugging


# For debugging, listing of (some) optimal solutions to the gadget example
optima: List[kn.R[str]] = [
    # 6 items
    kn.R({
        ('Camera', 340),
        ('CameraLens1', 120),
        ('CameraLens2', 400),
        ('Laptop', 1300),
        ('Smartphone', 140),
        ('Tablet', 700),
    }, 3000),
    kn.R({
        ('Camera', 340),
        ('CameraLens2', 400),
        ('CameraPowerAdapter', 200),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('Tablet', 700),
    }, 3000),
    kn.R({
        ('Camera', 340),
        ('CameraLens2', 400),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('Tablet', 700),
        ('TabletPowerAdapter', 200),
    }, 3000),
    # 7 items
    kn.R({
        ('Camera', 340),
        ('CameraLens2', 400),
        ('CameraPowerAdapter', 200),
        ('Laptop', 1300),
        ('LaptopPowerAdapter', 350),
        ('SmartphonePowerAdapter', 210),
        ('TabletPowerAdapter', 200),
    }, 3000),
    kn.R({
        ('Camera', 340),
        ('CameraLens1', 120),
        ('CameraPowerAdapter', 200),
        ('Laptop', 1300),
        ('Smartphone', 140),
        ('Tablet', 700),
        ('TabletPowerAdapter', 200),
    }, 3000),
    kn.R({
        ('CameraLens2', 400),
        ('CameraPowerAdapter', 200),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('Smartphone', 140),
        ('Tablet', 700),
        ('TabletPowerAdapter', 200),
    }, 3000),
    # 8 items
    kn.R({
        ('Camera', 340),
        ('CameraLens2', 400),
        ('CameraPowerAdapter', 200),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('LaptopPowerAdapter', 350),
        ('Smartphone', 140),
        ('SmartphonePowerAdapter', 210),
    }, 3000),
    kn.R({
        ('Camera', 340),
        ('CameraLens2', 400),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('LaptopPowerAdapter', 350),
        ('Smartphone', 140),
        ('SmartphonePowerAdapter', 210),
        ('TabletPowerAdapter', 200),
    }, 3000),
    kn.R({
        ('CameraLens1', 120),
        ('CameraLens2', 400),
        ('CameraPowerAdapter', 200),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('Tablet', 700),
        ('TabletPencil', 20),
        ('TabletPowerAdapter', 200),
    }, 3000),
    # 9 items
    kn.R({
        ('Camera', 340),
        ('CameraLens1', 120),
        ('CameraLens2', 400),
        ('CameraPowerAdapter', 200),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('LaptopPowerAdapter', 350),
        ('SmartphonePowerAdapter', 210),
        ('TabletPencil', 20),
    }, 3000),
    kn.R({
        ('Camera', 340),
        ('CameraLens1', 120),
        ('CameraLens2', 400),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('LaptopPowerAdapter', 350),
        ('SmartphonePowerAdapter', 210),
        ('TabletPencil', 20),
        ('TabletPowerAdapter', 200),
    }, 3000),
    # 10 items
    kn.R({
        ('CameraLens1', 120),
        ('CameraLens2', 400),
        ('CameraPowerAdapter', 200),
        ('CameraSpareBattery', 60),
        ('Laptop', 1300),
        ('LaptopPowerAdapter', 350),
        ('Smartphone', 140),
        ('SmartphonePowerAdapter', 210),
        ('TabletPencil', 20),
        ('TabletPowerAdapter', 200),
    }, 3000),
]
