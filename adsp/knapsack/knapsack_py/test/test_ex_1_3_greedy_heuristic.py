# -*- coding: utf-8 -*-
"""
Exercise 1.3: Unit-tests the Greedy Heuristic for simplified Knapsack.

Uses PEP 484, 526 type annotations for functions+variables.
"""

import pytest

from knapsack_py import ex_1_2_knapsack_api as kn
from knapsack_py import ex_1_3_greedy_heuristic as kng
from knapsack_py.test import test_ex_1_2_api as knp
from knapsack_py.test import test_ex_1_2_basic_solver as knbs

@pytest.mark.parametrize(
    "solver", [kng.GREEDY_KNAPSACK_SOLVER_V0, kng.GREEDY_KNAPSACK_SOLVER_V1])
class TestGreedyMaxWeightSolverSuite(knbs.BasicKnapsackSolverTestSuite):
    "Unit tests running GreedyMaxWeight solver on items3, gadgets."

    # unittest names allowed to be irregular, pylint: disable=invalid-name
    def test_GreedySolver_items3(
            self, solver: kn.SimpleKnapsackSolver) -> None:
        """Tests a GreedyMaxWeight Heuristic on |items| == 3 data."""

        assert (
            solver.solve(kn.P(knp.ITEMS3, 1))
            == kn.R())
        assert (
            solver.solve(kn.P(knp.ITEMS3, 2))
            == kn.R({('i2', 2)}, 2))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 3))
            == kn.R({('i3', 3)}, 3))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 4))
            == kn.R({('i4', 4)}, 4))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 5))
            == kn.R({('i4', 4)}, 4))  # !=opt
        assert (
            solver.solve(kn.P(knp.ITEMS3, 6))
            == kn.R({('i2', 2), ('i4', 4)}, 6))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 7))
            == kn.R({('i4', 4), ('i3', 3)}, 7))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 8))
            == kn.R({('i4', 4), ('i3', 3)}, 7))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 9))
            == kn.R({('i2', 2), ('i4', 4), ('i3', 3)}, 9))
        assert (
            solver.solve(kn.P(knp.ITEMS3, 10))
            == kn.R({('i2', 2), ('i4', 4), ('i3', 3)}, 9))

    # unittest names allowed to be irregular, pylint: disable=invalid-name
    def test_GreedySolver_gadgets(
            self, solver: kn.SimpleKnapsackSolver) -> None:
        """Tests a GreedyMaxWeight Heuristic on gadgets example data."""

        gadget_solution: kn.R = kn.R(
            {
                ('CameraLens2', 400),
                ('Laptop', 1300),
                ('LaptopPowerAdapter', 350),
                ('SmartphonePowerAdapter', 210),
                ('Tablet', 700),
                ('TabletPencil', 20),
            },
            2980)
        assert solver.solve(kn.P(knp.GADGETS, 3000)) == gadget_solution
