# -*- coding: utf-8 -*-
"""
Implementations of a Greedy Heuristic for the simplified Knapsack Problem.

This module features two implementations:
- knapsack_greedy_solve_v0(): a tail-recursive, list-based algorithm
- knapsack_greedy_solve_v1(): an iterative, heapq-based algorithm

To access these two functions through the SimpleKnapsackSolver Protocol,
they are also provided as global objects: GREEDY_KNAPSACK_SOLVER_V{0..1}

Code Discussion:

The Greedy Heuristic for the simplified Knapsack Problem works as follows:
- Picks an item from the set of remaining items by largest weight (greedy,
  although a better, generalized version would aim for max item value),
- tests if adding it to the solution would exceed the total weight capacity
  (more generally, could not possibly be completed to a feasible solution),
- discards the item if it would, and continues until no items are left.

Note that this heuristics visits each item in the input set once.  Hence, it
runs in linear time in the size of the input set.

For efficiency, this algorithm depends on a sorted data structure that is
either mutable or, if immutable, offers efficient add/removal operations
(e.g., by internal data sharing).

A natural candidate for holding the input set of items are SortedSets, which
would allow to use the solve() method's input type -- the immutable and
AbstractSet-based type 'kn.P[Item]' (see ex_1_2_knapsack_api.py) -- also for
the internal representation in the iteration or recursion, for example, by
simply passing initially: kn.P[Item](SortedSet(p.items), p.max_weight)

In Python, this approach runs into 3 problems:
- The standard library lacks sorted sets among the containers, sadly.
- The AbstractSet type lacks element addition/removal operations.
- The implementing, immutable type 'frozenset' is not documented to offer
  efficient union() and difference() operations that can avoid full copying
  of all elements at each addition/removal.

This lack of SortedSets implementing {Abstract,Mutable}Set leaves 3 options:

1) Use a mutable, standard sequence container, like 'list', for holding and
   passing items in the internal, recursive function; sort this sequence by
   sort() or sorted() before starting the recursion or iteration.

   => This approach is shown in method knapsack_greedy_solve_v0().

2) Use a different, (partially) sorted standard container, like 'heapq' or
   the synchronized or concurrent version of {queue|asyncio}.PriorityQueue.

   => This approach is shown in method knapsack_greedy_solve_v1().

3) Use a 3rd party library for sorted sets, for example:
     http://www.grantjenks.com/docs/sortedcontainers/

   Here the problem was that it seemed unclear whether library stubs with
   type hints are available for this (or other) libraries, such that the
   sorted item set would be recognized as instances of typing.AbstractSet.
   Attempts to import library stubs with type hints for this library from
     https://github.com/python/typeshed
   have not been successful to make SortedSet meet typing.AbstractSet.

Uses PEP 484, 526 type annotations for functions+variables.
"""

from typing import Tuple, List, TypeVar
import heapq

from knapsack_py import ex_1_2_knapsack_api as kn

# type aliases for PEP 484/526-style annotations
Item = TypeVar('Item')  # type variable for generic types (no import kn.Item)
Weight = kn.Weight  # type alias

def knapsack_greedy_solve_v0(problem: kn.P[Item]) -> kn.R[Item]:
    """Implements a greedy heuristic by tail-recursion on a sorted list.

    Instead of immutable and AbstractSet-based type 'kn.P[Item]' (see above),
    this implementation internally holds items in an ordered and mutable
    List[Tuple[Item, Weight]].  This list is sorted by +weight for efficient
    access to the item with the largest weight (or value, generally).  The
    result is assembled in a mutable 'kn.R[Item]'.

    This implementation is not stack-safe, see below.
    """

    # shortcuts
    max_weight: Weight = problem.max_weight

    # name of local function per FP convention, pylint: disable=invalid-name
    def go(items: List[Tuple[Item, Weight]], r: kn.R[Item]) -> kn.R[Item]:
        """Traverses items by tail-recursion left-to-right, adding them to
        the result unless they cannot possibly form a feasible solution.

        This inner, recursive function is typically named go() or loop().

        Python (like other languages) lacks a mechanism (standard decorator,
        for example) to detect tail-recursion and compile it into iteration.

        Hence, this implementation is not stack-safe, as the interpreter's
        call stack grows linearly in the size of the input set.
        """
        if not items:
            return r

        head: Tuple[Item, Weight] = items.pop()
        tail: List[Tuple[Item, Weight]] = items
        sum_w: Weight = r.sum_weight + head[1]
        if sum_w > max_weight:
            return go(tail, r)

        # may update & re-use result, mutable and only used sequentially
        r.items.add(head)
        r.sum_weight = sum_w
        return go(tail, r)

    items: List[Tuple[Item, Weight]] = sorted(  # transform input to list
        problem.items, key=(lambda iw: iw[1]))  # sort by weight, ascending
    return go(items, kn.R())


def knapsack_greedy_solve_v1(problem: kn.P[Item]) -> kn.R[Item]:
    """Implements a greedy heuristic by iteration on mutable heapq.

    The greedy algorithm benefits from efficient access and removal of the
    item with the largest weight (or value, generally).  In absence of a
    SortedSet, a PriorityQueue (typically heap-based) would be suitable
    (each of them running in O(n*log n for n items/removal operations).

    Since Python's queue.PriorityQueue is synchronized (there are also
    non-thread-safe asyncio/queues for async/await-style concurrency), basic
    'heapq' appears good enough for this inherently sequential algorithm.

    However, Python's heapq does not support an application-defined ordering
    by taking a 'compare' function, or just a 'key' mapper as for sort().
    Hence, applications that need to apply their own ordering must pass the
    data as a type that provides a suitable __lt__() method -- for example,
    by wrapping the input data in a '@dataclass' with redefined comparison,
    or by data transformation w.r.t. the ordering of types list, tuple etc.

    Here, 3 adjustments need to be applied to the input data:
    1) heapq expects a list, so the (unordered) set needs to be converted.
    2) heapq works as a min-heap, that is, 'heappop' returns the smallest
       element.  Therefore, we invert the sign of all weights in the heap.
    3) The predefined ordering on tuples compares elements left-to-right.
       So, we swap the tuple components to '(weight, item)' in the heap.
    """
    heap: List[Tuple[Weight, Item]] = list(  # convert
        map(lambda iw: (-iw[1], iw[0]), problem.items))  # swap, invert sign
    heapq.heapify(heap)

    # may update & re-use result, mutable and only used sequentially
    result: kn.R[Item] = kn.R()
    while len(heap) > 0:
        w_i: Tuple[Weight, Item] = heapq.heappop(heap)
        weight: Weight = -w_i[0]  # invert sign back
        item: Item = w_i[1]
        sum_w: Weight = result.sum_weight + weight
        if sum_w <= problem.max_weight:
            result.sum_weight = sum_w
            result.items.add((item, weight))  # swap order back

    return result


#
# Greedy heuristics as objects that meet the SimpleKnapsackSolver Protocol
#

# For convenience, and to support unit-testing, this module also exports the
# solver functions (heuristics) as SimpleKnapsackSolver-typed objects.
#
# The resulting class definitions are boiler-plate.  (The pylint complaints
# "Too few public methods" and "Method could be a function" rather reflect
# the inability in Python to define subtyped singleton objects directly.)
#
# Instead of nominal subtyping, e.g., 'GreedyKnapsackSolverV0(KnapsackSolver)'
# the approach here demonstrates structural subtyping or "duck typing" per
# 'KnapsackSolver(Protocol)' and the solvers just implementing the protocol.

# pylint: disable=too-few-public-methods,missing-function-docstring
class GreedyKnapsackSolverV0:
    """knapsack_greedy_solve_v0() as SimpleKnapsackSolver object."""
    def solve(self, problem: kn.P[Item]) -> kn.R[Item]:
        return knapsack_greedy_solve_v0(problem)

# pylint: disable=too-few-public-methods,missing-function-docstring
class GreedyKnapsackSolverV1:
    """knapsack_greedy_solve_v1() as SimpleKnapsackSolver object."""
    def solve(self, problem: kn.P[Item]) -> kn.R[Item]:
        return knapsack_greedy_solve_v1(problem)

GREEDY_KNAPSACK_SOLVER_V0 = GreedyKnapsackSolverV0()
"""Function knapsack_greedy_solve_v0() as SimpleKnapsackSolver object."""

GREEDY_KNAPSACK_SOLVER_V1 = GreedyKnapsackSolverV1()
"""Function knapsack_greedy_solve_v1() as SimpleKnapsackSolver object."""
