name := "polyglot.programming.knapsack"
organization := "netropy"
description := "Excercise, Scala, 0/1 Knapsack Problem, Recursion, ScalaTest"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

// code + tests migrated to Scala 3
scalaVersion := "3.2.1"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  //"-rewrite",
  //"-new-syntax",
  //"-Ytasty-reader",  // scala2
  //"-explaintypes",  // scala2
  //"-explain",  // scala3
)

// Resolvers (pre- or user-defined)
// https://www.scala-sbt.org/1.x/docs/Resolvers.html
//resolvers += DefaultMavenRepository  // inluded by default
//resolvers += Resolver.mavenLocal  // ~/.m2

// https://mvnrepository.com/artifact/org.scalameta/munit
// https://scalameta.org/munit/
// https://index.scala-lang.org/scalameta/munit/munit
//
// scalac 3.2 + munit_3, scalac 2.13 + munit_2.13
//libraryDependencies += "org.scalameta" %% "munit" % "0.7.29" % Test
libraryDependencies += "org.scalameta" %% "munit" % "1.0.0-M7" % Test

// only use a single thread for building
//parallelExecution := false
// execute tests in the _current_ project serially (default parallel)
//Test / parallelExecution := false

// fork a new JVM for 'run' and 'Test/run'
//fork := true
// fork a new JVM for 'Test/run', but not 'run'
//Test / fork := true
// add a JVM option to use when forking a JVM for 'run'
//javaOptions += "-Xmx8G"

//Test / testOptions := Seq(Tests.Filter(s => s.endsWith("Test")))

// print the output of tests immediately instead of buffering
//Test / logBuffered := false
