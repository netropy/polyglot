TAGS: Problem: \
recursion, tail-recursion, iteration, API design, heuristic, backtracking,
Knapsack Problem, generics, mutable collections, immutable collections,
container types, tuple, set, stack

TAGS: Scala: \
scala3, munit, generics, @annotation.tailrec, require, ensuring,
math.Ordering, collection.Seq, collection.Set, collection.Map,
collection.immutable.HashSet, collection.immutable.TreeSet,
collection.immutable.LazyList, collection.mutable.HashSet,
collection.mutable.PriorityQueue

TAGS: Python: \
python3, pytest, mypy, pylint, heapq, collections.deque, data classes,
PEP 557, dataclasses.dataclass, @dataclass, type annotations, PEP 484,
PEP 526, typing.Tuple, typing.Sequence, typing.List, typing.AbstractSet,
typing.Set, typing.Dict, typing.TypeVar, typing.Type, typing.Generic,
namedtuple, typing.Protocol, structural subtyping, duck typing, subtyping,
sys.setrecursionlimit, sys.getrecursionlimit, @pytest.mark.parametrize
