# polyglot/knapsack

### Scala, Python Coding Exercise: Knapsack Problem, Recursion, API Design

___Origin of this Exercise: Friend, programming course assignment.___

Task: Implement a
[greedy heuristic](https://en.wikipedia.org/wiki/Greedy_algorithm)
and a
[backtracking algorithm](https://en.wikipedia.org/wiki/Backtracking).
for the 0-1 Knapsack Problem (see
[README](README.md)).

Original problem instance: Given a maximum weight capacity, try packing as
many of these gadgets:
```
  Camera:                    340g
  CameraLens1:               120g
  CameraLens2:               400g
  CameraPowerAdapter:        200g
  CameraSpareBattery:         60g
  Laptop:                   1300g
  LaptopPowerAdapter:        350g
  Smartphone:                140g
  SmartphonePowerAdapter:    210g
  Tablet:                    700g
  TabletPencil:               20g
  TabletPowerAdapter:        200g
  -------------------------------
                            4040g
  maximum weight capacity:  3000g
```

_Notes:_
- This example implies the same value of _1_ for all items ("pack as many").
- It therefore is a _simplified_ version of the general 0-1 Knapsack Problem.
- Only when the item weights and values provide incongruent incentives, will a
  greedy heuristic render a suboptimal solution.
- A backtracking algorithm does an exhaustive search and thus always yields an
  optimal solution.
- See [README](README.md) for comments on 0-1 Knapsack's complexity.

___Broadened excercise: Implement solutions in Scala and Python.___

For the 0-1 Knapsack combinatorial optimization problem,
- sketch a simple problem-result-solver API,
- implement greedy heuristics and an exact backtracking algorithm,
- write unit tests, and
- _discuss ease-of-coding and tool/library usage._

See subsequent series of exercises.

Quick links: \
[scala3 sources](../src/main/scala/netropy/polyglot/knapsack/),
[scala3 tests](../src/test/scala/netropy/polyglot/knapsack/),
[python3 sources](../py/knapsack/),
[python3 tests](../py/knapsack/test/)

#### 1.1 Sketch APIs for how to model the Knapsack Problem data and solver.

Discuss types or ideas for how to model
- the input data (i.e., the parameters or instance of a 0-1 Knapsack problem),
- the output data (i.e., solutions to a 0-1 Knapsack problem instance),
- the interaction with solvers or heuristics.

[remarks](ex_1_1.r.md),
[scala code](../src/main/scala/netropy/polyglot/knapsack/ex_1_1.api_sketches.scala)

#### 1.2 Define an API for the simplified Knapsack problem with unit tests.

Define an API that is simple (for educational value) and readily supports
implementing and comparing _recursive_ and _iterative_ code variants.

Provide a basic unit test suite with elementary test data; also include the
above "gadget packing" example.

[remarks](ex_1_2.r.md),
[scala code](../src/main/scala/netropy/polyglot/knapsack/ex_1_2.api_problem_solver.scala),
[scala tests](../src/test/scala/netropy/polyglot/knapsack/ex_1_2.api_problem_solver_test.scala),
[python code](../py/knapsack/ex_1_2_knapsack_api.py),
[python tests](../py/knapsack/test_ex_1_2_knapsack_api.py)

#### 1.3 Implement a _Greedy Heuristic_ with unit tests.

The [Greedy Algorithm](https://en.wikipedia.org/wiki/Greedy_algorithm)
is a [Heuristic](https://en.wikipedia.org/wiki/Heuristic),
which as such typically produces only suboptimal solutions.

Implement a _greedy heuristic_ for the simplified Knapsack problem that
favours maximum weight.  Compare _recursive_ and _iterative_ code variants.

Since a greedy's selection of the next item may not always be unique, document
in the code which (if any) other criteria are used.

Provide unit tests, including for the "gadget packing" example.

[remarks](ex_1_3.r.md),
[scala code](../src/main/scala/netropy/polyglot/knapsack/ex_1_3.greedy_heuristic.scala),
[scala tests](../src/test/scala/netropy/polyglot/knapsack/ex_1_3.greedy_heuristic_test.scala),
[python code](../py/knapsack/ex_1_3_greedy_heuristic.py),
[python tests](../py/knapsack/test_ex_1_3_greedy_heuristic.py)

#### 1.4 Implement a _Backtracking Algorithm_ with unit tests.

The [Backtracking Algorithm](https://en.wikipedia.org/wiki/Backtracking)
produces optimal solutions by depth-first-search enumeration of candidates
while abandoning those branches that cannot possibly be completed to a valid
solution.

Implement a _backtracking algorithm_ for the simplified Knapsack problem.
Discuss _recursive_ or _iterative_ code variants.

Since a problem may have multiple, optimal solutions, document in the code
which (if any) secondary objectives are used in favouring a solution.

Provide unit tests, including for the "gadget packing" example.

[remarks](ex_1_4.r.md),
[scala code](../src/main/scala/netropy/polyglot/knapsack/ex_1_4.backtracking_solver.scala),
[scala tests](../src/test/scala/netropy/polyglot/knapsack/ex_1_4.backtracking_solver_test.scala),
[python code](../py/knapsack/ex_1_4_backtracking_solver.py),
[python tests](../py/knapsack/test_ex_1_4_backtracking_solver.py)

#### 1.5 Provide for a feature to group items for an all-or-none selection.

Considering the above "gadget packing" example, the implementations so far
suffer the practical problem that any packing solution which, for example,
picks a `CameraLens` but not the `Camera` is hardly useful.

Design and implement a mechanism that allows users to form groups of items
with the intent that either all or none of the items in a group should occur
in a solution.  A simple mechanism would be to provide a function that
clusters items based on their names: If two or more items share a common
prefix, like `CameraLens` and `Camera`, they'd be put into the same group of
Camera-related items, while `Laptop`, for example, would belong to a different
group.  The user can then express grouping-intent when assigning item names.

Provide unit tests, including for the "gadget packing" example.

[remarks](ex_1_5.r.md),
[scala code](../src/main/scala/netropy/polyglot/knapsack/ex_1_5.item_grouping.scala),
[scala tests](../src/test/scala/netropy/polyglot/knapsack/ex_1_5.item_grouping_test.scala)

#### 1.6 Summarize and compare the multi-language implementations.

Criteria like ease-of-coding or readability/clarity of code are inherently
subjective, and also depend on effort, skill etc.  Neverthless interesting.

[remarks](ex_1_6.r.md)

[Up](./README.md)
