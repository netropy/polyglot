# polyglot/misc/knapsack

### Scala, Python Coding Exercise: Knapsack Problem, Recursion, API Design

Compare ease-of-coding and tool/library usage by coding this excercise in
complementary languages: Scala and Python.

Project: For the (popular) Knapsack combinatorial optimization problem,
- sketch a simple problem-result-solver API,
- implement greedy heuristics and an exact backtracking algorithm, and
- write unit tests.

Discuss
- API design, use of data classes, objects, and interfaces;
- recursive decompositions, tail-recursion, iteration;
- usage of standard collection types, immutable vs mutable collections;
- static typing with _type inference_ vs dynamic typing with _optional
  type annotation;_
- usage of unit test libraries, tools in each language;
- see [Q&A excercise: knapsack](ex_1.q.knapsack.md).

Quick links: \
[scala3 sources](src/main/scala/netropy/polyglot/knapsack/),
[scala3 tests](src/test/scala/netropy/polyglot/knapsack/),
[python3 sources](py/knapsack/),
[python3 tests](py/knapsack/test/)

Tools: \
[sbt](https://www.scala-sbt.org),
[Maven](https://maven.apache.org),
[MUnit](https://scalameta.org/munit)
[pytest](https://docs.pytest.org/en/latest/),
[pylint](https://www.pylint.org),
[mypy](https://mypy.readthedocs.io/en/latest)

Problem/language/library features: \
[tags](tags.md)

#### Background: Knapsack Problem

Origin of this excercise: friend, modified programming course assignment, see
[Q&A excercise: knapsack](ex_1.q.knapsack.md).

[Knapsack](https://en.wikipedia.org/wiki/Knapsack_problem)
is an optimization problem well-suited for algorithmic coding excercises, as
it offers nice recursive decompositions for heuristics and for exact
algorithms (e.g., greedy heuristic, backtracking algorithm, branch & bound,
dynamic programming).

The problem's name derives from the task of packing a knapsack (or container)
with items of assigned value (or profit) under a total capacity constraint
(weight, volume, cost, or loss).  Knapsack covers a set of problem variations
(0/1, Bounded, Unbounded Knapsack etc) and is closely related to other
problems in
[Combinatorial Optimization](https://en.wikipedia.org/wiki/Combinatorial_optimization)
(Subset-Sum, Bin-Packing etc).

In it's general form, Knapsack is
[weakly NP-complete](https://en.wikipedia.org/wiki/Weak_NP-completeness)
due to the existence of
[fully-polynomial-time approximation schemes (FPTAS)](https://en.wikipedia.org/wiki/Fully_polynomial-time_approximation_scheme).
Some problem variations (e.g., restricting to fractional or integral values
and weights) make the problem solvable in
[polynomial](https://en.wikipedia.org/wiki/P_(complexity))
time.

#### Scala Code

The
[scala3 sources](src/main/scala/netropy/polyglot/knapsack/)
have no external dependencies.  They are re-formatted to the new
[Scala 3 "quiet" syntax](https://docs.scala-lang.org/scala3/new-in-scala3.html#new--shiny-the-syntax)
for an indentation-sensitive, python-like style of programming (nice!).

The
[scala3 unit tests](src/test/scala/netropy/polyglot/knapsack/)
use
[MUnit](https://scalameta.org/munit).
(no need to package or locally deploy).  This project has definitions for
[sbt](https://www.scala-sbt.org),
and
[Maven](https://maven.apache.org):
```
$ sbt test
...
[info] Passed: Total 40, Failed 0, Errors 0, Passed 40

$ mvn test
...
[INFO] Tests run: 40, Failures: 0, Errors: 0, Skipped: 0
```

Upgraded to versions:
```
scala 3.2.1 (java 11.0.17)
munit 1.0.0-M7, 0.7.29
sbt 1.8.0
maven 3.8.6
```

#### Python Code

The
[python3 sources](knapsack_py/)
have no external dependencies.  The code is fully type-annotated (PEP 484,
526).  Used static type checker:
[mypy](https://mypy.readthedocs.io/en/latest/)
(see config file
[mypy.ini](mypy.ini)):
```
$ mypy .
Success: no issues found in 8 source files
```

The embedded
[python3 unit tests](knapsack_py/test/)
use
[pytest](https://docs.pytest.org/en/latest/).
(No need to package or install as distribution, no
[tox](https://tox.wiki/en/latest/index.html)).
Option _-s_ prints captured output:
```
$ pytest -s
...
collected 30 items
knapsack_py/test/test_ex_1_2_knapsack_api.py .....                       [ 16%]
knapsack_py/test/test_ex_1_3_greedy_heuristic.py ..........              [ 50%]
knapsack_py/test/test_ex_1_4_backtracking_solver.py ...............      [100%]
============================== 30 passed in 0.29s ==============================
```

The code aims to comply with
[Google Python Style Guide](http://google.github.io/styleguide/pyguide.html).
Deviations from the style guide and disabled
[pylint](https://www.pylint.org).
warnings (mostly false positives) are documented in the code:
```
$ pylint knapsack_py
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
```

Tested with versions:
```
python: 3.10.6, 3.7.5
pytest: 7.2.0, 5.3.2
mypy: 0.991, 0.761
pylint: 2.12.2, 2.4.4
```

[Up](../README.md)
