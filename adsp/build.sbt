name := "polyglot.adsp"
organization := "netropy"
description := "Algorithms, Data Structures, Patterns, Code Experiments, Exercises"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

//// OK: Scala 2.13 and 3
// TODO: migrate to Scala 3.2, compile warnings: deprecated nonlocal returns
scalaVersion := "2.13.10"
//scalaVersion := "3.2.1"

// add Scala3 sources
//Compile / unmanagedSourceDirectories += baseDirectory.value / "src/main/scala3"
//Test / unmanagedSourceDirectories += baseDirectory.value / "src/test/scala3"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  //"-Ytasty-reader",  // scala2
  //"-explaintypes",  // scala2
  //"-explain",  // scala3
)

// Resolvers (pre- or user-defined)
// https://www.scala-sbt.org/1.x/docs/Resolvers.html
//resolvers += DefaultMavenRepository  // inluded by default
//resolvers += Resolver.mavenLocal  // ~/.m2

// https://index.scala-lang.org/scalameta/munit/artifacts/munit-scalacheck
// https://mvnrepository.com/artifact/org.scalameta/munit-scalacheck
// https://scalameta.org/munit/docs/integrations/scalacheck.html
//
// scalac 3.2 + munit-scalacheck_3, scalac 2.13 + munit-scalacheck_2.13
//libraryDependencies += "org.scalameta" %% "munit-scalacheck" % "0.7.29" % Test
libraryDependencies += "org.scalameta" %% "munit-scalacheck" % "1.0.0-M7" % Test

// only use a single thread for building
//parallelExecution := false
// execute tests in the _current_ project serially (default parallel)
//Test / parallelExecution := false

// fork a new JVM for 'run' and 'Test/run'
//fork := true
// fork a new JVM for 'Test/run', but not 'run'
//Test / fork := true
// add a JVM option to use when forking a JVM for 'run'
//javaOptions += "-Xmx8G"

//Test / testOptions := Seq(Tests.Filter(s => s.endsWith("Test")))

// print the output of tests immediately instead of buffering
//Test / logBuffered := false
