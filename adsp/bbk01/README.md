# polyglot/misc/bbk01

### Scala Talk: Lessons in Idiomatic Scala, Branch & Bound, 0/1 Knapsack Problem

__Talk Title__: \
From Imperative to Functional - Lessons in Idiomatic Scala

__Talk Summary__:

Programming in Scala has immensely rewarding moments: When code...
- almost "writes itself" once types and signatures are thought out,
- can be safely refactored due to use of strong typing,
- can be easily parallelized due to lack of side effects,
- ...

What makes coding in Scala "taxing", at times?  Suggestion: Decision Fatique.

Scala just seems to offer way more options than other languages on anything.
Coding can sometimes feel like a stream of prompts for an informed choice:
language constructs, collections types, how to define your types, method
signatures, error values vs exceptions, FP idioms, OO patterns -- plus "big"
choices: Scala3, libraries, test frameworks, tools.

Over time, judgment grows from experimentation and practice.  In this spirit,
this talk explores a coding exercise:  We hold a problem constant and see how
well all available Scala/OO/FP style options apply to it.

In a series of steps, this coding exercise
- grabs a simple enough, popular algorithm from combinatorial optimization,
- models the basic data types and discusses API options,
- implements a recursive version with imperative control-flow,
- and then transforms this code over a series of versions:
  - as `match` expressions
  - as partial functions composed with `orElse`, `andThen`
  - using `Option` plus mutual state
  - as monadic, right-biased `Either`
  - use of the `State Monad`
  - as state transformations
  - as a "fluent API" design

At each transformation, we summarize the pros & cons and try to improve in the
next version.  Thus, this coding exercise travels a distance: From Imperative
to Functional.

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/bbk01/),
[Scala tests](src/test/scala/netropy/polyglot/bbk01/),
[Test datasets](src/test/resources/netropy/polyglot/knapsack01/)

#### 1.2 Write a _0/1 Knapsack Problem Solver_ that uses Backtracking.

Model a small API that can be re-used or extended for Branch & Bound:
- data structures (problem, search tree) can be specific to 0/1 Knapsack
  (no need to model general MILP here),
- solver API and implementations should reflect generic Branch & Bound,
  Backtracking.

Discuss code/design (see source code comments and remarks).

API: \
[ex_1_1.api_problem.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_1.api_problem.scala),
[remarks](ex_1_1.r.api_problem.md), \
[ex_1_1.api_solver.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_1.api_solver.scala),
[remarks](ex_1_1.r.api_solver.md), \
[ex_1_1.api_node.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_1.api_node.scala),
[remarks](ex_1_1.r.api_node.md), \
[ex_1_1.api_statistics.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_1.api_statistics.scala)

Solver: \
[ex_1_2.solver_dfs_recursive.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_2.solver_dfs_recursive.scala),
[remarks](ex_1_2.r.solver_dfs_recursive.md)

Base classes: \
[ex_1_2.problem_extension.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_2.problem_extension.scala),
[ex_1_2.solver_base.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_2.solver_base.scala),
[ex_1_2.statistics_extension.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_2.statistics_extension.scala)

Unit test: \
[ex_1_2.knapsack_gadgets_test.scala](src/test/scala/netropy/polyglot/bbk01/ex_1_2.knapsack_gadgets_test.scala),
[ex_1_2.solver_backtracking_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_1_2.solver_backtracking_tests.scala)

#### 1.3 Derive an imperative _K01 Branch & Bound Solver_ with unit tests.

Unit tests and a benchmark runner to support subsequent code refactoring.

Code specific to 0/1 Knapsack Problem/Node:
- Implement the LP bound ("Dantzig Bound") as a problem relaxation
  (sufficiently simple to code and comes with Greedy as a primal heuristic).
- Combine with another heuristic that fixes infeasible variables to zero.

The control flow, node fathoming should reflect generic Branch & Bound.

Discuss code/design (see source code comments and remarks):

Solver: \
[ex_1_3.solver_dfs_recursive.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_3.solver_dfs_recursive.scala),
[remarks](ex_1_3.r.solver_dfs_recursive.md)

Base classes: \
[ex_1_3.heuristics_base.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_3.heuristics_base.scala),
[ex_1_3.heuristics_combiner.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_3.heuristics_combiner.scala),
[ex_1_3.solver_base.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_3.solver_base.scala)

0/1 Knapsack primal/dual heuristics: \
[ex_1_3.knapsack_heuristics.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_3.knapsack_heuristics.scala),
[ex_1_3.knapsack_heuristics_lp.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_3.knapsack_heuristics_lp.scala),
[ex_1_3.knapsack_heuristics_residual.scala](src/main/scala/netropy/polyglot/bbk01/ex_1_3.knapsack_heuristics_residual.scala)

Test datasets, unit test suites, and benchmark runner: \
[Test datasets](src/test/resources/netropy/polyglot/knapsack01/README.md), \
[ex_1_3.knapsack_benchmarksuite.scala](src/test/scala/netropy/polyglot/bbk01/ex_1_3.knapsack_benchmarksuite.scala),
[ex_1_3.knapsack_runner.scala](src/test/scala/netropy/polyglot/bbk01/ex_1_3.knapsack_runner.scala),
[ex_1_3.knapsack_testsuite.scala](src/test/scala/netropy/polyglot/bbk01/ex_1_3.knapsack_testsuite.scala), \
[ex_1_3.solver_dfs_recursive_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_1_3.solver_dfs_recursive_tests.scala)

#### 2.1 Refactor to B&B solver based on _match_.

Discuss refactored code (see source code comments and remarks).

Solver: \
[ex_2_1.solver_dfs_match.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_1.solver_dfs_match.scala),
[remarks](ex_2_1.r.solver_dfs_match.md)

Unit Test: \
[ex_2_1.solver_dfs_match_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_1.solver_dfs_match_tests.scala)

#### 2.2 Refactor to B&B solver based on _Partial Functions_.

Discuss refactored code (see source code comments and remarks).

Solver: \
[ex_2_2.solver_dfs_partial_functions.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_2.solver_dfs_partial_functions.scala),
[remarks](ex_2_2.r.solver_dfs_partial_functions.md)

Unit Test: \
[ex_2_2.solver_dfs_partial_functions_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_2.solver_dfs_partial_functions_tests.scala)

#### 2.3 Refactor to B&B solver based on monadic _Option_ and mutable state.

Discuss refactored code (see source code comments and remarks).

Solver: \
[ex_2_3.solver_dfs_monadic_option_mutable_state.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_3.solver_dfs_monadic_option_mutable_state.scala),
[remarks](ex_2_3.r.solver_dfs_monadic_option_mutable_state.md)

Unit Test: \
[ex_2_3.solver_dfs_monadic_option_mutable_state_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_3.solver_dfs_monadic_option_mutable_state_tests.scala)

#### 2.4 Refactor to B&B solver based on monadic _Either_.

Discuss refactored code (see source code comments and remarks).

Solver: \
[ex_2_4.solver_dfs_monadic_either.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_4.solver_dfs_monadic_either.scala),
[remarks](ex_2_4.r.solver_dfs_monadic_either.md)

Unit Test: \
[ex_2_4.solver_dfs_monadic_either_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_4.solver_dfs_monadic_either_tests.scala)

#### 2.5 Refactor to B&B solver based on the _State Monad_.

Discuss refactored code (see source code comments and remarks).

Solver: \
[ex_2_5.solver_dfs_state_monad.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_5.solver_dfs_state_monad.scala),
[remarks](ex_2_5.r.solver_dfs_state_monad.md)

TODO: move StateMonad ../fp/ \
[ex_2_5.state_monad.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_5.state_monad.scala),
[ex_2_5.state_monad_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_5.state_monad_tests.scala)

Unit Test: \
[ex_2_5.solver_dfs_state_monad_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_5.solver_dfs_state_monad_tests.scala)

#### 2.6 Refactor to B&B solver based on _State Transformations_.

Discuss refactored code (see source code comments and remarks).

Solver: \
[ex_2_6.solver_dfs_bbstate_monads_functors.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_6.solver_dfs_bbstate_monads_functors.scala),
[remarks](ex_2_6.r.solver_dfs_bbstate_monads_functors.md)

TODO: move ReaderMonad ../fp/ \
[ex_2_6.reader_monad.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_6.reader_monad.scala),
[ex_2_6.reader_monad_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_6.reader_monad_tests.scala)

Unit Test: \
[ex_2_6.solver_dfs_bbstate_monads_functors_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_6.solver_dfs_bbstate_monads_functors_tests.scala)

#### 2.7 Refactor to B&B solver using a _Fluent API_ for fathoming nodes.

Discuss refactored code (see source code comments).

Solver: \
[ex_2_7.solver_dfs_fluent_fathomer.scala](src/main/scala/netropy/polyglot/bbk01/ex_2_7.solver_dfs_fluent_fathomer.scala)

Unit Test: \
[ex_2_7.solver_dfs_fluent_fathomer_tests.scala](src/test/scala/netropy/polyglot/bbk01/ex_2_7.solver_dfs_fluent_fathomer_tests.scala)

[Up](../README.md)
