package netropy.polyglot.bbk01


/** Runs the 0-1 Knapsack gadgets example from SimpleKnapsackSolverData. */
class BBKnapsack_GadgetsTest(factory: SolverFactory) {

  // no need for external dependency
  //import SimpleKnapsackSolverData.gadgets
  val gadgets = Set(
    "Camera" -> 340,
    "CameraLens1" -> 120,
    "CameraLens2" -> 400,
    "CameraPowerAdapter" -> 200,
    "CameraSpareBattery" -> 60,
    "Laptop" -> 1300,
    "LaptopPowerAdapter" -> 350,
    "Smartphone" -> 140,
    "SmartphonePowerAdapter" -> 210,
    "Tablet" -> 700,
    "TabletPencil" -> 20,
    "TabletPowerAdapter" -> 200,
  )

  def test_BBSolver_solve_knapsack01_gadgets_example_12(): Unit = {

    val (names, weights) = gadgets.toIndexedSeq.unzip
    val weightsL = weights.map(_.toLong)
    val capacity = 3000
    val optimum = 3000
    val p = Problem(weightsL, weightsL, capacity)
    val (r, st) = factory.create(p).solve()
    print()

    def print() = {
      println("------------------------------------------------------------")
      println(s"result = ${r.mkString}")
      println(s"problem = ${p.mkString}")
      val sol = r.packed.toIndexedSeq.map(i => i -> (names(i), weights(i)))
      val sols = sol.map(inw => "" + inw._1 + "->" + inw._2)
      println(s"""i->(n,w) = ${sols.mkString(", ")}""")
      println(s"stats = ${st.mkString}")
      println(s"expected optimum = $optimum")
      println("------------------------------------------------------------")
    }

    // check result (no need for util.Try)
    try {
      r.assertIsSolution(p)
      assertResult(optimum)(r.weight)
    } catch {
      case ex: AssertionError => print(); throw ex
    }
  }
}
