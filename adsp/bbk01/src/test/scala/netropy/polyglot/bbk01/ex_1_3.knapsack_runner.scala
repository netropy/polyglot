package netropy.polyglot.bbk01

import java.util.concurrent.atomic.AtomicReference  // workaround for @volatile
import java.io.FileNotFoundException

import io.Source
import util.Try
import collection.immutable.IndexedSeq // for Scala 2.12


/** Loads and solves 0-1 knapsack datasets from src/resources/...(->README). */
class BBKnapsackDatasetRunner(factory: SolverFactory) {

  /** Resource path of the test data files. */
  val testDataDir = "/netropy/polyglot/knapsack01"

  def loadLongsFromResource(fileName: String): IndexedSeq[Long] = {

    val url = this.getClass.getResource(testDataDir + "/" + fileName)
    if (url == null)
      //throw new FileNotFoundException(s"resource not found: '$fileName'")
      return Vector()

    val resource = Source.fromURL(url)
    val longs = resource.getLines().map(_.trim.toLong).toVector
    resource.close  // not exception-safe, OK for unit tests
    longs
  }

  def loadAndSolve(
    fileNamePrefix: String, print: Boolean = true): (Node, Statistics) = {

    // parse test dataset files, check consistency
    val values = loadLongsFromResource(s"${fileNamePrefix}_p.txt")
    val weights = loadLongsFromResource(s"${fileNamePrefix}_w.txt")
    val capacity = loadLongsFromResource(s"${fileNamePrefix}_c.txt")
    val optimum = loadLongsFromResource(s"${fileNamePrefix}_o.txt")
    val solution = IndexedSeq[Long]()  // default: no solution vector
    // for debugging: load solution vector if available, not unique however
    //val solution = loadLongsFromResource(s"${fileNamePrefix}_s.txt")
    assert(values.size == weights.size,
      s"Inconsistent test data:"
        + s"values.size = ${values.size} != ${weights.size} = weights.size, "
        + s"values = $values, weights = $weights, file... = $fileNamePrefix")
    assert(1 == capacity.size,
      s"Inconsistent test data:"
        + s"capacity.size != 1, capacity=$capacity, file... = $fileNamePrefix")
    assert(1 == optimum.size,
      s"Inconsistent test data:"
        + s"optimum.size != 1, optimum=$optimum, file... = $fileNamePrefix")
    assert(solution.size == 0 || solution.size == values.size, // optional
      s"Inconsistent test data:"
        + s"solution.size = ${solution.size} != ${values.size} = values.size, "
        + s"solution = $solution, values = $values, file... = $fileNamePrefix")

    // solve
    val p = Problem(values, weights, capacity(0))
    val s = factory.create(p)
    //val (r, st) = factory.create(p).solve()
    // TODO: double-check this @volatile observation, cannot be correct...
    // PROBLEM: @volatile on local variable seems ineffective as mem barrier
    //@volatile
    //var r = Node.empty(0)
    // WORKAROUND: use Java's AtomicReference
    val rRef = new AtomicReference[Node](Node.empty)
    val st = factory.create(p).solve(o => {
      //r = o
      rRef.set(o)
      if (print) println(s"${fileNamePrefix}: r=${o.value}")
    })
    val r = rRef.get  // continue with non-atomic reference
    if (print) printDiagnostics()

    def printDiagnostics(): Unit = {
      println("------------------------------------------------------------")
      println(s"solver = ${factory.getClass.getName}")
      println(s"files... = ${fileNamePrefix}...")
      //println(s"problem = ${p.mkString}")
      println(s"result = ${r.mkString}")
      //val sol = r.packed.toIndexedSeq.map(i => i->(values(i), weights(i)))
      //val sols = sol.map(ivw => "" + ivw._1 + "->" + ivw._2)
      //println(s"""i->(v,w) = ${sols.mkString(", ")}""")
      println(s"expected optimum = ${optimum(0)}")
      //println(s"expected solution = $solution")
      println(s"stats = ${st.mkString}")
      println("------------------------------------------------------------")
    }

    // chain the (overridable) solution checks
    val e =
      for {
        e0 <- checkFeasibility(p, r)
        e1 <- checkSolutionValue(optimum(0), r)
        e2 <- checkOptionalSolutionVector(solution, r)
      } yield e2

    if (e.isFailure) {
      printDiagnostics()
      e.get  // throw
    }

    (r, st)
  }

  // Overridable result check methods if subclasses want to substitute their
  // own error text or exception (e.g., jUnit's or scalatest's `assertResult`).

  /** Returns [[util.Failure]] if the feasibility check fails. */
  def checkFeasibility(problem: Problem, result: Node): Try[Unit] =
    Try {
      result.assertIsSolution(problem)
    }

  /** Returns [[util.Failure]] if the solution value check fails. */
  def checkSolutionValue(exp: Long, result: Node): Try[Unit] =
    Try {
      val actSumValue = result.value
      assert(
        exp == actSumValue,
        s"expSumValue: $exp, actSumValue: $actSumValue")
    }

  /** Returns [[util.Failure]] if the optional solution vector check fails.
    *
    * A `null` or empty solution vector accepts any result.
    */
  def checkOptionalSolutionVector(
    exp: IndexedSeq[Long], result: Node): Try[Unit] =
    Try {
      if (exp != null && !exp.isEmpty) {
        val expPacked = (0 until exp.size).filter(exp(_) == 1)
        val actPacked = result.packed.toIndexedSeq
        assert(
          expPacked == actPacked,
          s"expSolutionPacked: $expPacked,\nactSolutionPacked: $actPacked")
      }
    }
}
