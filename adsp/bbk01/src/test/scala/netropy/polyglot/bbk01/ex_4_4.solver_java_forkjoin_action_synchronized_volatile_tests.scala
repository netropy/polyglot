package netropy.polyglot.bbk01

// TODO: outdated code

// test B&B solvers

// unfixable ForkJoinPool.awaitQuiescence() problem for small datasets (->code)
class BBKnapsack_v20_MinTests
    extends BBKnapsack_MinSuite(BBSolver_v20)

// benchmark B&B solvers

// 1s: easily solvable:
object BBKnapsack_Dataset_03_Suite_100_v20
  extends BBKnapsack_BenchmarkSuite(BBSolver_v20)
  with BBKnapsack_Dataset_03_BenchmarkSuite_100

// ~3min: solvable:
object BBKnapsack_Dataset_03_Suite_200_v20
  extends BBKnapsack_BenchmarkSuite(BBSolver_v20)
  with BBKnapsack_Dataset_03_BenchmarkSuite_200
