package netropy.polyglot.bbk01

import java.io.{BufferedWriter, FileWriter}


/* Dataset `03` with large scale 0/1 knapsack problems:
 * knapPI_1: uncorrelated data instances
 * knapPI_2: weakly correlated instances
 * knapPI_3: strongly correlated instances
 * @see [[http://artemisa.unicauca.edu.co/~johnyortega/instances_01_KP/]]
 */

/** Loads and runs 0-1 knapsack datasets from src/resources/...(see README). */
class BBKnapsack_BenchmarkSuite(
  val factory: SolverFactory,
  val repeatDefault: Int = 3,
  val printDefault: Boolean = true) extends App {

  def run(
    datasetName: String,
    repeat: Int = repeatDefault,
    print: Boolean = printDefault): Unit = {

    println("============================================================")
    val tsvFileName = "BBKnapsack_BenchmarkSuite.tsv"
    val append = true
    println(s"appending tab-separated records to file: ./$tsvFileName")
    val writer = new BufferedWriter(new FileWriter(tsvFileName, append))
    var header = ""

    val qn = factory.getClass.getName
    val factoryName = qn.substring(qn.lastIndexOf('.') + 1, qn.lastIndexOf('$'))
    val runner = new BBKnapsackDatasetRunner(factory)
    for (i <- 0 until repeat) {
      println(s"$factoryName:  $datasetName,  run #$i of $repeat")
      val (r, st) = runner.loadAndSolve(datasetName, print)

      val keys = st.toMap.keys.mkString("\t")
      header = s"factoryName\tdatasetName\trun\tvalue\t${keys}"
      val values = st.toMap.values.mkString("\t")
      val line = s"${factoryName}\t${datasetName}\t${i}\t${r.value}\t$values"
      writer.newLine()
      writer.write(line)
      writer.flush()
    }

    writer.close()  // TODO: handle exceptions, ->util.using

    // write tsv column header
    val tsvKeysFileName = tsvFileName + ".header.tsv"
    println(s"writing tab-separated header to file: ./$tsvFileName")
    val keysWriter = new BufferedWriter(new FileWriter(tsvKeysFileName, false))
    keysWriter.newLine()
    keysWriter.write(header)
    keysWriter.close()  // TODO: handle exceptions, ->util.using
  }
}

/** Runs 0-1 Knapsack dataset 03: |variables| == 100. */
trait BBKnapsack_Dataset_03_BenchmarkSuite_100 {
  this: BBKnapsack_BenchmarkSuite =>

  val repeat = 1
  run("dataset_03/knapPI_1_100_1000_1", repeat)
  run("dataset_03/knapPI_2_100_1000_1", repeat)
  run("dataset_03/knapPI_3_100_1000_1", repeat)
}

/** Runs 0-1 Knapsack dataset 03: |variables| == 200. */
trait BBKnapsack_Dataset_03_BenchmarkSuite_200 {
  this: BBKnapsack_BenchmarkSuite =>

  val repeat = 1
  run("dataset_03/knapPI_1_200_1000_1", repeat)
  run("dataset_03/knapPI_2_200_1000_1", repeat)
  run("dataset_03/knapPI_3_200_1000_1", repeat)
}

/** Runs 0-1 Knapsack dataset 03: |variables| == 500. */
trait BBKnapsack_Dataset_03_BenchmarkSuite_500 {
  this: BBKnapsack_BenchmarkSuite =>

  val repeat = 1
  run("dataset_03/knapPI_1_500_1000_1", repeat)
  run("dataset_03/knapPI_2_500_1000_1", repeat)
  run("dataset_03/knapPI_3_500_1000_1", repeat)
}

/** Runs 0-1 Knapsack dataset 03: |variables| == 1000. */
trait BBKnapsack_Dataset_03_BenchmarkSuite_1000 {
  this: BBKnapsack_BenchmarkSuite =>

  val repeat = 1
  run("dataset_03/knapPI_1_1000_1000_1", repeat)
  run("dataset_03/knapPI_2_1000_1000_1", repeat)
  run("dataset_03/knapPI_3_1000_1000_1", repeat)
}

/** Runs 0-1 Knapsack dataset 03: |variables| == 2000. */
trait BBKnapsack_Dataset_03_BenchmarkSuite_2000 {
  this: BBKnapsack_BenchmarkSuite =>

  val repeat = 1
  run("dataset_03/knapPI_1_2000_1000_1", repeat)
  run("dataset_03/knapPI_2_2000_1000_1", repeat)
  run("dataset_03/knapPI_3_2000_1000_1", repeat)
}

/** Runs 0-1 Knapsack dataset 03: |variables| == 5000. */
trait BBKnapsack_Dataset_03_BenchmarkSuite_5000 {
  this: BBKnapsack_BenchmarkSuite =>

  val repeat = 1
  run("dataset_03/knapPI_1_5000_1000_1", repeat)
  run("dataset_03/knapPI_2_5000_1000_1", repeat)
  run("dataset_03/knapPI_3_5000_1000_1", repeat)
}
