package netropy.polyglot.bbk01

// B&B solver configurations

object BBSolver_v05_monadic_either_hNone_dfs
    extends BBSolver_v05_monadic_either_dfs_factory(BBHeuristics.none)

object BBSolver_v05_monadic_either_hAll_dfs
    extends BBSolver_v05_monadic_either_dfs_factory(BBKnapsackHeuristics.all)

// min-test B&B solver

class BBKnapsack_v05_monadic_either_hNone_dfs_MinTests
    extends BBKnapsack_MinSuite(BBSolver_v05_monadic_either_hNone_dfs)

class BBKnapsack_v05_monadic_either_hAll_dfs_MinTests
    extends BBKnapsack_MinSuite(BBSolver_v05_monadic_either_hAll_dfs)

// med-test B&B solver

class BBKnapsack_v05_monadic_either_hAll_dfs_MedTests
    extends BBKnapsack_MedSuite(BBSolver_v05_monadic_either_hAll_dfs)

// max-test B&B solver

class BBKnapsack_v05_monadic_either_hAll_dfs_MaxTests
    extends BBKnapsack_MaxSuite(BBSolver_v05_monadic_either_hAll_dfs)

// benchmark B&B solver

object BBKnapsack_Dataset_03_Suite_100_v05_monadic_either_hAll_dfs
  extends BBKnapsack_BenchmarkSuite(BBSolver_v05_monadic_either_hAll_dfs)
  with BBKnapsack_Dataset_03_BenchmarkSuite_100

object BBKnapsack_Dataset_03_Suite_200_v05_monadic_either_hAll_dfs
  extends BBKnapsack_BenchmarkSuite(BBSolver_v05_monadic_either_hAll_dfs)
  with BBKnapsack_Dataset_03_BenchmarkSuite_200

object BBKnapsack_Dataset_03_Suite_1000_v05_monadic_either_hAll_dfs
  extends BBKnapsack_BenchmarkSuite(BBSolver_v05_monadic_either_hAll_dfs)
  with BBKnapsack_Dataset_03_BenchmarkSuite_1000

object BBKnapsack_Dataset_03_Suite_v05_dfs
  extends App {
  BBKnapsack_Dataset_03_Suite_100_v05_monadic_either_hAll_dfs.main(args)
  BBKnapsack_Dataset_03_Suite_200_v05_monadic_either_hAll_dfs.main(args)
  BBKnapsack_Dataset_03_Suite_1000_v05_monadic_either_hAll_dfs.main(args)
}

object BBKnapsack_Dataset_03_Suite_v05
  extends App {
  BBKnapsack_Dataset_03_Suite_v05_dfs.main(args)
}
