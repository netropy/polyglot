package netropy.polyglot

package object bbk01 {

  // All that is needed here for unit testing

  /** Fails if the actual expression does not match the expected value. */
  def assertResult[A, B](exp: => A)(act: => B): Unit =
    assert(exp == act, s"expected value: $exp, actual value: $act")
}
