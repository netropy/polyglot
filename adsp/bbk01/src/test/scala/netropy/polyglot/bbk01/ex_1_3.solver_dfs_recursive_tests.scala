package netropy.polyglot.bbk01

// B&B solver configurations

object BBSolver_v01_recursive_hNone_dfs
    extends BBSolver_v01_recursive_dfs_factory(BBHeuristics.none)

object BBSolver_v01_recursive_hGreedyLpBound_dfs
    extends BBSolver_v01_recursive_dfs_factory(BBKnapsackHeuristics.greedyLpBound)

object BBSolver_v01_recursive_hResidualValue_dfs
    extends BBSolver_v01_recursive_dfs_factory(BBKnapsackHeuristics.residualValue)

object BBSolver_v01_recursive_hAll_dfs
    extends BBSolver_v01_recursive_dfs_factory(BBKnapsackHeuristics.all)

// gadget-test B&B solver

class BBKnapsack_v01_recursive_hNone_dfs_GadgetsMinTests
    extends BBKnapsack_GadgetsTest(BBSolver_v01_recursive_hNone_dfs)

class BBKnapsack_v01_recursive_hAll_dfs_GadgetsMinTests
    extends BBKnapsack_GadgetsTest(BBSolver_v01_recursive_hAll_dfs)

// min-test B&B solver

class BBKnapsack_v01_recursive_hNone_dfs_MinTests
    extends BBKnapsack_MinSuite(BBSolver_v01_recursive_hNone_dfs)

class BBKnapsack_v01_recursive_hGreedyLpBound_dfs_MinTests
    extends BBKnapsack_MinSuite(BBSolver_v01_recursive_hGreedyLpBound_dfs)

class BBKnapsack_v01_recursive_hResidualValue_dfs_MinTests
    extends BBKnapsack_MinSuite(BBSolver_v01_recursive_hResidualValue_dfs)

class BBKnapsack_v01_recursive_hAll_dfs_MinTests
    extends BBKnapsack_MinSuite(BBSolver_v01_recursive_hAll_dfs)

// med-test B&B solver

class BBKnapsack_v01_recursive_hGreedyLpBound_dfs_MedTests
    extends BBKnapsack_MedSuite(BBSolver_v01_recursive_hGreedyLpBound_dfs)

class BBKnapsack_v01_recursive_hResidualValue_dfs_MedTests
    extends BBKnapsack_MedSuite(BBSolver_v01_recursive_hResidualValue_dfs)

class BBKnapsack_v01_recursive_hAll_dfs_MedTests
    extends BBKnapsack_MedSuite(BBSolver_v01_recursive_hAll_dfs)

// max-test B&B solver

class BBKnapsack_v01_recursive_hAll_dfs_MaxTests
    extends BBKnapsack_MaxSuite(BBSolver_v01_recursive_hAll_dfs)

// benchmark B&B solver

object BBKnapsack_Dataset_03_Suite_100_v01_recursive_hAll_dfs
  extends BBKnapsack_BenchmarkSuite(BBSolver_v01_recursive_hAll_dfs)
  with BBKnapsack_Dataset_03_BenchmarkSuite_100

object BBKnapsack_Dataset_03_Suite_200_v01_recursive_hAll_dfs
  extends BBKnapsack_BenchmarkSuite(BBSolver_v01_recursive_hAll_dfs)
  with BBKnapsack_Dataset_03_BenchmarkSuite_200

object BBKnapsack_Dataset_03_Suite_1000_v01_recursive_hAll_dfs
  extends BBKnapsack_BenchmarkSuite(BBSolver_v01_recursive_hAll_dfs)
  with BBKnapsack_Dataset_03_BenchmarkSuite_1000

object BBKnapsack_Dataset_03_Suite_v01_dfs
  extends App {
  BBKnapsack_Dataset_03_Suite_100_v01_recursive_hAll_dfs.main(args)
  BBKnapsack_Dataset_03_Suite_200_v01_recursive_hAll_dfs.main(args)
  BBKnapsack_Dataset_03_Suite_1000_v01_recursive_hAll_dfs.main(args)
}

object BBKnapsack_Dataset_03_Suite_v01
  extends App {
  BBKnapsack_Dataset_03_Suite_v01_dfs.main(args)
}
