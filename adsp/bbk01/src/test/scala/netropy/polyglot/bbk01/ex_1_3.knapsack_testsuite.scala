package netropy.polyglot.bbk01


/*
 * Datasets for the 0/1 Knapsack Problem
 *
 * Dataset `01`, low-dimensional, |variables| <= 25
 * @see [[https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01/knapsack_01.html]]
 *
 * Dataset `02`, low-dimensional, |variables| <= 25
 * @see [[http://artemisa.unicauca.edu.co/~johnyortega/instances_01_KP/]]
 *
 * Dataset `03`, |variables| >= 100
 * knapPI_1: uncorrelated data instances
 * knapPI_2: weakly correlated instances
 * knapPI_3: strongly correlated instances
 * @see [[http://artemisa.unicauca.edu.co/~johnyortega/instances_01_KP/]]
 */

/** Loads and runs 0-1 knapsack datasets from src/resources/...(see README).
  *
  * Design Notes:
  *
  * Unit Tests and Integration Tests come with dependencies such as: the
  * object-under-test, a file, a system resource, or a running server.
  *
  * Scala offers a staircase of options for prividing resources (see link):
  * 1) hard-code the dependency
  * 2) pass it as a method parameter (inversion of control)
  * 3) pass it as a class parameter (constructor-based dependency injection)
  * 4) declare it as `abstract` member and define it in a subclass/object
  * 5) pass it as an `implicit` / `given` method parameter
  * 6) hold it in a global variable (global, mutable state)
  * 7) hold it in a member variable (setter-based dependency injection)
  *
  * @see [[https://www.lihaoyi.com/post/StrategicScalaStylePrincipleofLeastPower.html#dependency-injection]]
  *
  * The above list (blog) is most helpful -- yet, we might add to it:
  * - the "Loan" pattern (an extension of 2)
  * - the "Cake" pattern (an extension of 4 with self-type annotation)
  * - arguably, 6) and 7) ought to be swapped in terms of "harmfulness"
  * - 7) with Container-Based Dependency Injection (e.g., JEE, Spring)
  * - combination of 3) and 5): class constructor with `implicit` parameter
  * - use of Scala3 features: Context Functions, Conditional Givens etc.
  *
  * Here, the "Cake" pattern is used, as it supports the mixin composition of
  * tests into suites:
  * - this base class declares the resource (`solver`) as a member
  * - subsequent traits self-type over this class and define unit tests
  * - a test suite class extends this class and adds the tests as mixins
  */
class BBKnapsack_TestSuite(factory: SolverFactory) {

  val solver = new BBKnapsackDatasetRunner(factory)
}

/** Runs 0-1 Knapsack datasets with |variables| <= 15. */
trait BBKnapsack_Small_Tests { this: BBKnapsack_TestSuite =>

  // Easiest to just make each test a method (not using parametrized tests).

  def test_BBSolver_solve_knapsack01_dataset_01_p01_10(): Unit = {
    solver.loadAndSolve("dataset_01/p01")
  }

  def test_BBSolver_solve_knapsack01_dataset_01_p02_5(): Unit = {
    solver.loadAndSolve("dataset_01/p02")
  }

  def test_BBSolver_solve_knapsack01_dataset_01_p03_6(): Unit = {
    solver.loadAndSolve("dataset_01/p03")
  }

  def test_BBSolver_solve_knapsack01_dataset_01_p04_7(): Unit = {
    solver.loadAndSolve("dataset_01/p04")
  }

  def test_BBSolver_solve_knapsack01_dataset_01_p05_8(): Unit = {
    solver.loadAndSolve("dataset_01/p05")
  }

  def test_BBSolver_solve_knapsack01_dataset_01_p06_7(): Unit = {
    solver.loadAndSolve("dataset_01/p06")
  }

  def test_BBSolver_solve_knapsack01_dataset_01_p07_15(): Unit = {
    solver.loadAndSolve("dataset_01/p07")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f1_10(): Unit = {
    solver.loadAndSolve("dataset_02/f1_l-d_kp_10_269")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f3_4(): Unit = {
    solver.loadAndSolve("dataset_02/f3_l-d_kp_4_20")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f4_4(): Unit = {
    solver.loadAndSolve("dataset_02/f4_l-d_kp_4_11")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f5_15(): Unit = {
    solver.loadAndSolve("dataset_02/f5_l-d_kp_15_375")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f6_10(): Unit = {
    solver.loadAndSolve("dataset_02/f6_l-d_kp_10_60")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f7_7(): Unit = {
    solver.loadAndSolve("dataset_02/f7_l-d_kp_7_50")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f9_5(): Unit = {
    solver.loadAndSolve("dataset_02/f9_l-d_kp_5_80")
  }
}

/** Runs 0-1 Knapsack datasets with |variables| <= 100. */
trait BBKnapsack_Medium_Tests { this: BBKnapsack_TestSuite =>

  // Easiest to just make each test a method (not using parametrized tests).

  def test_BBSolver_solve_knapsack01_dataset_01_p08_24(): Unit = {
    solver.loadAndSolve("dataset_01/p08")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f2_20(): Unit = {
    solver.loadAndSolve("dataset_02/f2_l-d_kp_20_878")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f8_23(): Unit = {
    solver.loadAndSolve("dataset_02/f8_l-d_kp_23_10000")
  }

  def test_BBSolver_solve_knapsack01_dataset_02_f10_20(): Unit = {
    solver.loadAndSolve("dataset_02/f10_l-d_kp_20_879")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_1_100(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_1_100_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_2_100(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_2_100_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_3_100(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_3_100_1000_1")
  }
}

/** Runs 0-1 Knapsack datasets with |variables| > 100. */
trait BBKnapsack_Large_Tests { this: BBKnapsack_TestSuite =>

  // Easiest to just make each test a method (not using parametrized tests).

  def test_BBSolver_solve_knapsack01_dataset_03_1_200(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_1_200_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_2_200(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_2_200_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_3_200(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_3_200_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_1_500(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_1_500_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_2_500(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_2_500_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_3_500(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_3_500_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_1_1000(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_1_1000_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_2_1000(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_2_1000_1000_1")
  }

  def test_BBSolver_solve_knapsack01_dataset_03_3_1000(): Unit = {
    solver.loadAndSolve("dataset_03/knapPI_3_1000_1000_1")
  }
}

class BBKnapsack_MinSuite(
  factory: SolverFactory,
  print: Boolean = true)
    extends BBKnapsack_TestSuite(factory)
    with BBKnapsack_Small_Tests

class BBKnapsack_MedSuite(
  factory: SolverFactory,
  print: Boolean = true)
    extends BBKnapsack_TestSuite(factory)
    with BBKnapsack_Medium_Tests

class BBKnapsack_MaxSuite(
  factory: SolverFactory,
  print: Boolean = true)
    extends BBKnapsack_TestSuite(factory)
    with BBKnapsack_Large_Tests
