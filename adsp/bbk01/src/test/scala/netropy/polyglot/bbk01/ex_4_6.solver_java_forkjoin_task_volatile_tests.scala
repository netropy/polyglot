package netropy.polyglot.bbk01

// TODO: outdated code

// test B&B solvers

class BBKnapsack_v22_MinTests
    extends BBKnapsack_MinSuite(BBSolver_v22)

// benchmark B&B solvers

// 1s: easily solvable:
object BBKnapsack_Dataset_03_Suite_100_v22_java_forkjoin_task
  extends BBKnapsack_BenchmarkSuite(BBSolver_v22)
  with BBKnapsack_Dataset_03_BenchmarkSuite_100

// ~3min: solvable:
object BBKnapsack_Dataset_03_Suite_200_v22_java_forkjoin_task
  extends BBKnapsack_BenchmarkSuite(BBSolver_v22)
  with BBKnapsack_Dataset_03_BenchmarkSuite_200

object BBKnapsack_Dataset_03_Suite_v22
  extends App {
  BBKnapsack_Dataset_03_Suite_100_v22_java_forkjoin_task.main(args)
  BBKnapsack_Dataset_03_Suite_200_v22_java_forkjoin_task.main(args)
}
