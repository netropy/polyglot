### Test Datasets for the 0/1 Knapsack Problem

These freely available datasets from published research:
- [dataset `01`, low-dimensional <=25 variables](dataset_01/README.md)
- [dataset `02`, low-dimensional <=25 variables](dataset_02/README.md)
- [dataset `03`, large scale >=100 variables](dataset_03/README.md)

Datasets `02` and `03` converted to the file format used by dataset `01`:
- xxx_c.txt: the knapsack capacity (max weight)
- xxx_p.txt: the profits or values, 1 line per variable
- xxx_w.txt: the weights, 1 line per variable
- xxx_o.txt: the optimal value
- xxx_s.txt: the optimal variable assignment, 1 line per variable, optional

Also, dataset `02` problem `f5_l-d_kp_15_375` has been adapted from `Float`
to `Long` values and weights.
