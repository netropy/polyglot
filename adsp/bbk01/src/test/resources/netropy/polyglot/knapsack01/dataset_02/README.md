#### 0/1 Knapsack Problem: Test Dataset 02

10 low-dimensional problems, <= 25 variables.

The file format has been converted the one used in dataset `01`: see
[Up](../README.md).

Problem `f5_l-d_kp_15_375` has been adapted from `Float` values and weights to
`Long`.  The previously rounded optimum has been changed from `481.0694` to
exact `481069368L`.

Origin:
http://artemisa.unicauca.edu.co/~johnyortega/instances_01_KP/

Saved
[origin page as pdf](Instances_of_01_Knapsack_Problem.pdf)

[Up](../README.md)
