#### 0/1 Knapsack Problem: Test Dataset 01

8 low-dimensional problems, <= 25 variables.  File format, see
[Up](../README.md).

Origin:
https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01/knapsack_01.html

Saved
[origin page as pdf](Data_for_the_01_Knapsack_Problem.pdf)

[Up](../README.md)
