#### 0/1 Knapsack Problem: Test Dataset 03

21 large scale problems, >= 100 variables.

Naming:
- knapPI_1_nvars: uncorrelated data instances
- knapPI_2_nvars: weakly correlated instances
- knapPI_3_nvars: strongly correlated instances

The file format has been converted the one used in dataset `01`: see
[Up](../README.md).

Origin:
http://artemisa.unicauca.edu.co/~johnyortega/instances_01_KP/

Saved
[origin page as pdf](Instances_of_01_Knapsack_Problem.pdf)

Test data generator seems to come from Pisinger:
http://hjemmesider.diku.dk/~pisinger/codes.html

Saved
[Pisinger page as pdf](David_Pisinger_optimization_codes.pdf)

[Up](../README.md)
