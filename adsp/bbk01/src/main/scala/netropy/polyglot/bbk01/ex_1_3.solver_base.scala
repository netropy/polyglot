package netropy.polyglot.bbk01


/** Base class for B&B solvers, provides heuristics for computing bounds.
  *
  * Encapsulates the "Bounding" part in Branch & Bound:
  * - `heuristics`: solves a relaxation of the node's problem instance and
  *   optionally
  *   - proposes a solution, i.e., a (lower) bound on the global optimum
  *   - provides a tighter (upper) bound on the node's best possible solution
  *
  * Subclasses will define other algorithmic parameters of B&B, namely:
  * - Node selection: the branch tree traversal strategy
  *
  * @param heuristicsFactory creates a heuristics instance, not null
  * @param problem the (enriched) input problem instance, not null
  */
abstract class BBSolver
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BTSolver {

  val heuristics: BBHeuristics = heuristicsFactory(problem)
}
