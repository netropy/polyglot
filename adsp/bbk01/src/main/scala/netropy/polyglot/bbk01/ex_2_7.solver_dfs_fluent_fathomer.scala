package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** A recursive, DFS B&B solver using a Fluent API for fathoming nodes.
  *
  * Idea: Compose B&B as transformations with support for method chaining
  *       -> model the node processing state as a `case class Fathomer`
  *       -> add operations as methods returning `Fathomer` (Fluent API)
  *
  * Sketch:
  * ```
  *     case class Fathomer(node: Option[Node], best: Node) {
  *
  *       // ways to formulate operations... (prepare for: Option -> Seq?)
  *
  *       def checkIsInfeasible: Fathomer = node match {
  *         case None => this
  *         case Some(n) if (n.isInfeasible) => Fathomer(None, best)
  *         case _ => this
  *       }
  *
  *       def checkIsFathomed: Fathomer =
  *         node.filter(_.isFathomed).map { n => ... } getOrElse this
  *
  *       def checkIsSuboptimal: Fathomer =
  *         node.fold(this) { n => ... }
  *
  *       def applyHeuristics: Fathomer =
  *         node.map { n => ... } getOrElse this
  *
  *       def fathom: Fathomer = this
  *         .checkIsInfeasible
  *         .checkIsFathomed
  *         .checkIsSuboptimal
  *         .applyHeuristics
  *         ...
  *     }
  * ```
  *
  * Summary:
  *
  * - Only depth-first-search so far, function go() not tail-recursive
  *
  * + Excellent readability, extendability:
  *   + operations formulated as _methods_, uniform access to input/output
  *   + encapsulation, data+methods as `case class` (still extendable)
  *   + control flow uniformly expressed as data flow, still no side-effects
  *   + branching & recursion on child nodes separate from fathoming
  *   + prepared to generalize `Option` -> `Seq` for breadth-first-search
  *   ~ 7 lines of code for fathom flow (same as before ~8)
  *   ~ 8 lines of code for recursive function `go` (down from ~14)
  *
  * - Limits
  *   - no short-circuit behaviour, each function must deal with `None` node
  *   - `Option` -> `Seq` change may require individual operation patterns
  *   - Fluent APIs -- automatically make for great FP or OOP design?
  *     - can refactor any procedural, side-effecting code -> immutable class
  *       => may give false appearance of information locality
  *
  * ==> Good basis for adding features: BFS/search strategies, concurrency...
  */
class BBSolver_v08_fluent_fathomer_dfs
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated per side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    /** Fathoms a node and possibly updates the best solution. */
    case class Fathomer(node: Option[Node], best: Node) {

      // helper methods

      def toTuple: (Option[Node], Node) = (node, best)
      // Scala 2:
      // def toTuple: (Option[Node], Node) = Fathomer.unapply(this).get
      // Scala 3:
      // import scala.deriving.Mirror
      // def toTuple: (Option[Node], Node) = Tuple.fromProductTyped(this)

      /** Returns given node if its value exceeds `best`, otherwise `best`.
        * Invokes the callback `onSolution(s)` when `s` exceeds `best`.
        */
      def reduceSolution(s: Node): Node = {

        //require(s.assertIsSolution)  // already checked by go(), bind()
        //require(best.assertIsSolution)  // already checked by go(), bind()

        if (!s.isBetterThan(best)) best
        else {
          stats.nNodesBest += 1
          onSolution(s) // notify caller, side-effect
          s
        }
      }

      // operations (uniform pattern using `fold`)

      def checkIsInfeasible: Fathomer =
        node.fold(this) { n =>
          if (!n.isInfeasible) this
          else {
            stats.nNodesInfeasible += 1
            Fathomer(None, best)
          }
        }

      def checkIsFathomed: Fathomer =
        node.fold(this) { n =>
          if (!n.isFathomed) this
          else {
            stats.nNodesFathomed += 1
            Fathomer(None, reduceSolution(n.closed))
          }
        }

      def checkIsSuboptimal: Fathomer =
        node.fold(this) { n =>
          if (!n.isSuboptimalBy(best)) this
          else {
            stats.nNodesSuboptimal += 1
            Fathomer(None, best)
          }
        }

      def applyHeuristics: Fathomer =
        node.fold(this) { n =>
          val (refined, proposed) = heuristics.bind(n)
          if (refined ne n)
            stats.nNodesRefined += 1
          val best1 = proposed.fold(best) { s =>
            stats.nNodesProposed += 1
            reduceSolution(s)
          }
          Fathomer(Some(refined), best1)
        }

      // composition (as method chain/cascade)

      def fathom: Fathomer = this
        .checkIsInfeasible
        .checkIsFathomed
        .checkIsSuboptimal
        .applyHeuristics
        .checkIsFathomed
        .checkIsSuboptimal
    }

    // Alternative versions (prepare for generalizing: Option -> Seq)

    class Fathomer_v1(node: Option[Node], best: Node)
        extends Fathomer(node, best) {

      override def checkIsInfeasible: Fathomer =
        node.filterNot(_.isInfeasible).map(_ => this).getOrElse {
          stats.nNodesInfeasible += 1
          new Fathomer_v1(None, best)
        }

      override def checkIsFathomed: Fathomer =
        node.filter(_.isFathomed).map { n =>
          stats.nNodesFathomed += 1
          new Fathomer_v1(None, reduceSolution(n.closed))
        } getOrElse this


      override def checkIsSuboptimal: Fathomer =
        (for (n <- node if !n.isSuboptimalBy(best)) yield this)
          .getOrElse {
            stats.nNodesSuboptimal += 1
            new Fathomer_v1(None, best)
          }

      override def applyHeuristics: Fathomer =
        node.map { n =>
          val (refined, proposed) = heuristics.bind(n)
          if (refined ne n)
            stats.nNodesRefined += 1
          val best1 = proposed.fold(best) { s =>
            stats.nNodesProposed += 1
            reduceSolution(s)
          }
          new Fathomer_v1(Some(refined), best1)
        } getOrElse this
    }

    /*
     * Branching & Recursion separate from Fathomer
     */

    /** Returns the branched child nodes of a node. */
    def branchedNodes(node: Node): Seq[Node] = {
      val nodes = branchNode(node)
      stats.nNodesBranched += nodes.size
      nodes
    }

    /** Solves a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be solved
      * @param best a start solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // not: @annotation.tailrec
    def go(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      val (node1, best1) = Fathomer(Some(node), best).fathom.toTuple
      node1.fold(best1)(branchedNodes(_).foldLeft(best1)((b, n) => go(n, b)))

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    val start = Node.start
    val best = go(start, start.closed)
    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v08_fluent_fathomer_dfs_factory
  (heuristicsFactory: BBProblem => BBHeuristics)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v08_fluent_fathomer_dfs(
      heuristicsFactory)(new BBProblem(problem))
}
