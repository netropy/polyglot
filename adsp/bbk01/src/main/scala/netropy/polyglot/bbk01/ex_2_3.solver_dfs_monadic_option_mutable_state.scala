package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** A recursive, DFS B&B solver based on monadic `Option` and mutable state.
  *
  * Idea: Utilize [[Option]] to short-circuit the fathoming of a node
  *       -> model the local processing state as `Option[Node]`
  *       -> introduce mutable state holding result `var best: Node`
  *       -> compose fathoming as series of (`flatMap`) transformations
  *
  * Refresher:
  *       Type `Option[+A]` is a context holding a value `A` in a `Some[A]`
  *       or indicating absence of a value through object `None[Nothing]`.
  *
  *       As monad (`map`, `flatMap`), `Option` allows to lift initial values
  *       and computations on `A` into its context, which are
  *       - a no-op in case of `None`
  *       - effective on a `Some[A]`.
  *
  * Sketch:
  * ```
  *     // mutable state to pass result
  *     var best: Node = start value
  *
  *     def checkIsFathomed(node: Node): Option[Node] =
  *       if (...) Some(node)                    // node needs more work
  *       else { ...best = ... ; None }          // pruned, node is done
  *     ...
  *
  *     for {
  *       r0   <- Some(node)                     // initial value in a Some
  *       r1   <- checkIsInfeasible(r0)          // may inject None
  *       ...
  *       rm   <- applyHeuristics(rm-1)          // skipped on None
  *       rm+1 <- checkIsFathomed(rm)            // ...
  *       ...
  *       _ = branchedNodes(rn-1).foreach(recurse ...)
  *     }
  *     // result available in `best`
  * ```
  *
  * Notes: .../ex_2_3.r.solver_dfs_monadic_option_mutable_state.md
  *
  * Summary:
  *
  * - Only depth-first-search so far, function go() not tail-recursive
  *
  * + Improved readability:
  *   + each operation expressed as ordinary method (not as function `val`)
  *   + processing of a node formulated outside the recursive function
  *   + implicit control flow, expressed as data (return `Some`, `None`)
  *   + integrates the recursion on child nodes as final action
  *   ~ 10 lines of code for fathom+branch flow (same as before ~8)
  *   ~ 17 lines of code for recursive function `go` (down from ~25)
  *
  * - Code's readability, robustness not ideal
  *   - side-effect, mutable state: `var best: Node = ...`
  *   - go_v0(): fragile naming of parameters `r0, r1...`
  *   - go_v1(): deeply nested: `.foreach` chain
  *   - no "flat" version of `.foreach` (not associative like `flatMap`)
  *
  * ==> Try use monadic `Either` for passing results w/o side-effect
  */
class BBSolver_v04_monadic_option_dfs
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated by side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)
    val start: Node = Node.start

    // mutable state to pass result between B&B operations
    var best: Node = start.closed

    // Refactored monadic tests and transformations on nodes

    def checkIsInfeasible(node: Node): Option[Node] =
      if (!node.isInfeasible) Some(node)
      else {
        stats.nNodesInfeasible += 1
        None
      }

    def checkIsFathomed(node: Node): Option[Node] =
      if (!node.isFathomed) Some(node)
      else {
        stats.nNodesFathomed += 1
        reduceSolution(node.closed)
        None
      }

    def checkIsSuboptimal(node: Node): Option[Node] =
      if (!node.isSuboptimalBy(best)) Some(node)
      else {
        stats.nNodesSuboptimal += 1
        None
      }

    def applyHeuristics(node: Node): Some[Node] = {
      val (refined, proposed) = heuristics.bind(node)
      if (refined ne node)
        stats.nNodesRefined += 1
      proposed.foreach { s =>
        stats.nNodesProposed += 1
        reduceSolution(s)
      }
      Some(refined)
    }

    /** Returns the branched child nodes of a node. */
    def branchedNodes(node: Node): Seq[Node] = {
      val nodes = branchNode(node)
      stats.nNodesBranched += nodes.size
      nodes
    }

    /** Returns given node if its value exceeds `best`, otherwise `best`.
      *
      * Note: reduceSolution(s, o) is not commutative.
      * Invokes the callback `onSolution(s)` when `s` exceeds `best`.
      */
    def reduceSolution(s: Node): Node = {

      //require(s.assertIsSolution)  // already checked by go(), bind()
      //require(best.assertIsSolution)  // already checked by go(), bind()

      if (!s.isBetterThan(best)) best
      else {
        stats.nNodesBest += 1
        onSolution(s)  // notify caller, side-effect
        best = s   // update incumbent, side-effect
        s
      }
    }

    /** Solves a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be solved
      * @param best a start solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // select code version
    def go(node: Node): Unit = go_v0(node)  // _v0..1

    /** B&B-chain as `for`-comprehension with side-effect. */
    // not: @annotation.tailrec
    def go_v0(node: Node): Unit = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      for {
        r0 <- Some(node)
        r1 <- checkIsInfeasible(r0)
        r2 <- checkIsFathomed(r1)
        r3 <- checkIsSuboptimal(r2)
        r4 <- applyHeuristics(r3)
        r5 <- checkIsFathomed(r4)
        r6 <- checkIsSuboptimal(r5)
        _ = branchedNodes(r6).foreach(go_v0)  // recurse
      }
      ()  // hmm, why doesn't `for` w/o `yield` return `()`?

    } ensuring { _ =>
      best.assertIsSolution
      true
    }

    /** B&B-chain as desugared, nested `foreach` operations. */
    // not: @annotation.tailrec
    def go_v1(node: Node): Unit = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      // the desugared version of go_v0()
      // - simplified the innermost .map(r6 => (r6, branchedNodes(r6)...))
      // - no need to yield (), implicit conversion of any value -> Unit
      Some(node)
        .foreach(checkIsInfeasible(_)
          .foreach(checkIsFathomed(_)
            .foreach(checkIsSuboptimal(_)
              .foreach(applyHeuristics(_)
                .foreach(checkIsFathomed(_)
                  .foreach(checkIsSuboptimal(_)
                    .map(branchedNodes(_).foreach(go_v1))))))))  // recurse

    } ensuring { _ =>
      best.assertIsSolution
      true
    }

    go(start)
    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v04_monadic_option_dfs_factory
  (heuristicsFactory: BBProblem => BBHeuristics)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v04_monadic_option_dfs(
      heuristicsFactory)(new BBProblem(problem))
}
