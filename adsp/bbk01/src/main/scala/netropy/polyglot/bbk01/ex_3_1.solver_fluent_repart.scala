package netropy.polyglot.bbk01

import collection.immutable.Seq


// TODO: add scaladoc + design discussion + summary

/** A recursive B&B solver with fluent node-fathoming API.
  *
  * Summary:
  * + user-configurable traversal strategies
  * + function go() tailrecursive
  */
class BBSolver_v10_fluent_fathomer_repart_seq
  (heuristicsFactory: BBProblem => BBHeuristics,
    nodes: Seq[Node],
    repartitioned: Traversal.Repartition)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  // TODO: fix scaladoc
  /** Provides for fathoming of nodes with a given optimum. */
  case class Fathomer(
    nodes: Seq[Node],
    best: Node,
    stats: BBStatistics,
    onSolution: Node => Unit) {

    def assertIsConsistent(implicit problem: Problem): Boolean = {
      assert(nodes.forall(_.assertIsConsistent))
      assert(best.assertIsSolution)
      true
    }

    def checkIsInfeasible: Fathomer = {
      val feasible = nodes.filterNot(_.isInfeasible)
      val nInfeasible = nodes.size - feasible.size
      if (nInfeasible == 0) this
      else {
        stats.nNodesInfeasible += nInfeasible
        Fathomer(feasible, best, stats, onSolution)
      }
    }

    def checkIsFathomed: Fathomer = {
      // as single pass, preserve relative order using (foldRight, +:)
      val e = nodes.take(-1)
      val (fathomed, nonFathomed) = nodes.foldRight(e, e) {
        case (n, (f, nf)) =>
          if (n.isFathomed) (n +: f, nf) else (f, n +: nf)
      }

      if (fathomed.isEmpty) this
      else {
        stats.nNodesFathomed += fathomed.size
        // no need to close a node per: included, refined, assertIsConsistent
        // val best1 = reduceSolutions(fathomed.map(_.closed))
        // assert(fathomed.forall(n => n eq n.closed))
        val best1 = reduceSolutions(fathomed)
        Fathomer(nonFathomed, best1, stats, onSolution)
      }
    }

    def checkIsSuboptimal: Fathomer = {
      val capable = nodes.filterNot(_.isSuboptimalBy(best))
      val nIncapable = nodes.size - capable.size
      if (nIncapable == 0) this
      else {
        stats.nNodesSuboptimal += nIncapable
        Fathomer(capable, best, stats, onSolution)
      }
    }

    def applyHeuristics: Fathomer = {

      // preserves collection type
      val (refined, proposed) = nodes.map(heuristics.bind).unzip

      val proposed1 = proposed.flatten
      val best1 = reduceSolutions(proposed1)
      stats.nNodesProposed += proposed1.size

      assert(refined.size == nodes.size)
      val nRefined = refined.zip(nodes).count(rn => rn._1 ne rn._2)
      stats.nNodesRefined += nRefined

      if (nRefined == 0 && proposed1.isEmpty) this  // minor optimization
      else Fathomer(refined, best1, stats, onSolution)
    }

    def fathom: Fathomer = {

      require(assertIsConsistent)

      this
        .checkIsInfeasible
        .checkIsFathomed
        .checkIsSuboptimal
        .applyHeuristics
        .checkIsFathomed
        .checkIsSuboptimal

    } ensuring { f =>
      f.assertIsConsistent
      assert(f.best.value >= best.value)
      true
    }

    /** Returns the best solution among given nodes and the current `best`.
      *
      * Invokes `onSolution(s)` foreach solution `s` exceeding current optimum.
      */
    def reduceSolutions(solutions: Seq[Node]): Node = {

      // already checked by go(), bind()
      //require(solutions.forall(_.assertIsSolution))
      //require(best.assertIsSolution)

      solutions.foldLeft(best) { (b, s) =>
        if (!s.isBetterThan(b)) b
        else {
          stats.nNodesBest += 1
          onSolution(s) // notify caller, side-effect
          s
        }
      }
    }
  }

  // TODO: scaladoc
  def branchedNodes(nodes: Seq[Node], stats: BBStatistics): Seq[Node] = {
    val nodes1 = nodes.flatMap(branchNode)  // preserves collection type
    stats.nNodesBranched += nodes1.size
    nodes1
  }

  def solve(onSolution: Node => Unit): BBStatistics = {

    /** Solves the nodes in `next` and `later` by tail-recursion.
      *
      * @param next the nodes to be fathomed with currently best solution
      * @param later a pool of unfathomed nodes including newly branched nodes
      * @return a fathomer with no nodes and the optimum nodes for in `next`
      */
    @annotation.tailrec
    def go(next: Fathomer, later: Seq[Node]): Fathomer = {

      // debug: incorrectly pruned node
      // import collection.immutable.{BitSet}
      // val s = Node(BitSet.empty, BitSet(...),.,.,.)
      // val has_s = next.nodes.exists(_.isAllowedBy(s))
      //
      // fathomed...branched...repartitioned
      //
      // val remaining = next1 ++ later1 ++ Seq(fathomed.best)
      // if (has_s && !remaining.exists(_.isAllowedBy(s))) {
      //   println(s"----------------\n")
      //   println(s"s=${s}\n")
      //   println(s"next.best=${next.best}\n")
      //   println(s"next.nodes=${next.nodes}\n")
      //   println(s"!!!!!!!!!!!!!!!!\n")
      //   println(s"fathomed.best=${fathomed.best}\n")
      //   println(s"fathomed.nodes=${fathomed.nodes}\n")
      //   println(s"branched=${branched}\n")
      //   println(s"next1=${next1}\n")
      //   println(s"later1=${later1}\n")
      //   assert(false)
      // }

      val fathomed = next.fathom  // map operation
      val branched = branchedNodes(fathomed.nodes, fathomed.stats)  // map
      val (next1, later1) = repartitioned(branched, later)  // reshuffle
      assert(!next1.isEmpty || later1.isEmpty)

      if (next1.isEmpty) fathomed
      else go(
        Fathomer(next1, fathomed.best, fathomed.stats, fathomed.onSolution),
        later1)
    }

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated per side-effect
    BBStatistics.start(stats)
    stats.nNodesBranched = 1
    val empty = nodes.take(-1)  // TODO: -> emptyNodes
    val start = Node.start
    val next = start +: empty
    val later = empty
    val done = go(Fathomer(next, start.closed, stats, onSolution), later)
    assert(done.nodes.isEmpty)
    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v10_fluent_fathomer_repart_seq_factory
  (heuristicsFactory: BBProblem => BBHeuristics,
    nodes: Seq[Node],
    repartitioned: Traversal.Repartition)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v10_fluent_fathomer_repart_seq(
      heuristicsFactory, nodes, repartitioned)(new BBProblem(problem))
}
