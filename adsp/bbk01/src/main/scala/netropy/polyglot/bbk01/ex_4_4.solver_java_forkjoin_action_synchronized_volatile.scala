package netropy.polyglot.bbk01

import java.util.concurrent.{ForkJoinPool, RecursiveAction, TimeUnit}


// TODO: outdated code

/** A concurrent B&B Solver using `ForkJoinPool` + `volatile/synchronized`.
  *
  * This `ForkJoinPool` code version wraps the processing of a `Node` in a
  * `RecursiveAction`; the currently best solution is held in a function-wide
  * `@volatile` variable, updated from a single location by side-effect.
  *
  * The updating of the statistics counters, in this initial code version,
  * happens fully `synchronized{}`.  These critial sections are short-lived
  * and no other resources are acquired (deadlock-free) -- why this design
  * is still seen as compliant with the `ForkJoinPool` programming model,
  * which demands non-blocking, non-throwing, and short-lived tasks.
  *
  * However, as nodes are most frequently created and destroyed, the fully
  * synchronized reads and writes of the statistics counters is expected to
  * result in lock contention at a large number of threads.  See subsequent
  * code versions for designs with better scalability.
  *
  * @see [[https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html]]
  * @see [[https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveAction.html]]
  */
class BBSolver_v20(implicit problem: BBProblem) extends BBSolver4 {

  val FORK_THRESHOLD_SIZE_OF_NODE_UNSET: Int = 8

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    // currently best solution ("incumbent")
    //
    // Sufficient to use @volatile (or java.util.concurrent.AtomicReference):
    // - concurrent reads from RecursiveActionSolvers.go()
    // - atomic updates from critical section in compareAndUpdateBest()
    val start = Node.start
    @volatile
    var best = start.closed

    /** Atomically updates the incumbent and notifies caller of solve(). */
    def compareAndUpdateBest(node: Node): Unit = {
      this.synchronized {
        if (node.value > best.value) {  // new incumbent
          best = node
          onSolution(node) // side-effect

          // Design note: whether to invoke onSolution() from a CountedCompleter.
          //
          // The javadoc on util.concurrent.CountedCompleter states that this
          // action type uses its own continuations and is "more robust in the
          // presence of subtask stalls or blockage than other ForkJoinTasks".
          //
          // Therefore, the onSolution() callback handler could be invoked as:
          //   new CountedCompleter[Void] {
          //     def compute(): Unit = {
          //       onSolution(node) // side-effect
          //       tryComplete()
          //     }
          //   }.invoke()  // cannot fork(), must wait for onSolution to complete
          //
          // Such a design, however, has no upside:
          // - onSolution() forms a critical section per solve() API guarantee;
          //   otherwise, race condition could updates have overtake another.
          // - Therefore, this thread must wait for the CountedCompleter to
          //   complete from within synchronized{}.
          // - This would block (or stall) this thread, i.e., a ForkJoinPool
          //   worker thread, until the scheduling of the CountedCompleter,
          //   and introduce an additional uncertainty to calling onSolution().
          // - The API requirement on the user-supplied onSolution() callback to
          //   be non-blocking and of short duration are well documented.
        }
      }
    }

    class RecursiveActionSolver(node: Node) extends RecursiveAction {

      def compute(): Unit = go(node)  // re-use recursive decomposition :-)

      def go(node: Node): Unit = {
        //node.assertIsConsistent(problem) // debug

        if (node.weight > problem.maxWeight) {  // infeasible
          stats.synchronized { stats.nNodesInfeasible += 1 }
          return
        }

        if (valueUpperBound(node) <= best.value) {  // suboptimal
          stats.synchronized { stats.nNodesSuboptimal += 1 }
          return
        }

        if (node.unset.isEmpty) {  // possibly new best
          compareAndUpdateBest(node)  // side-effect
          stats.synchronized { stats.nNodesBest += 1 }
          return
        }

        // incomplete, branch
        val refined = node
        val nodes = branchNode(refined)
        stats.synchronized { stats.nNodesBranched += nodes.size }
        val h = nodes.headOption
        if (h.exists(_.unset.size < FORK_THRESHOLD_SIZE_OF_NODE_UNSET)) {
          nodes.foldLeft(())((u, n) => go(n))
          return
        }

        // TODO: check task selection order, perhaps foldLeft
        // traverses from left
        nodes.foldRight(())((n, u) => new RecursiveActionSolver(n).fork())
      }
    }

    ForkJoinPool.commonPool.execute(new RecursiveActionSolver(start))

    // Design note: detecting the end of processing by awaitQuiescence().
    //
    // This code version wraps the processing of node in a RecursiveAction.
    // As it only forks actions but never joins them or waits for them to
    // complete, it requires a mechanism to indicate the end of processing.
    //
    // Per javadoc, method ForkJoinPool.awaitQuiescence() looks promising,
    // although with a caveat: "This method is conservative; it might not
    // return true immediately upon idleness of all threads, but will
    // eventually become true if threads remain inactive."
    //
    // The timeout argument to awaitQuiescence() seems to apply to the extra
    // duration needed to "conservatively" detect quiescence, not the total
    // amount of wait time -- fortunately (javadoc unclear).
    //
    // PROBLEM: As this B&B solver uses the global ForkJoinPool.commonPool,
    // an open question remains whether awaitQuiescence() can detect the end
    // a B&B computation if unrelated, parallel actions have been submitted.
    //
    // WORKAROUND: Give each B&B solver its own instance of a ForkJoinPool.
    // Such a design, however, would negate the benefits of a common pool,
    // for example, composibility of code without overcommitting on threads.
    //
    ForkJoinPool.commonPool.awaitQuiescence(60L, TimeUnit.SECONDS)
    BBStatistics.stop(stats)
    stats
  }
}

object BBSolver_v20 extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v20()(new BBProblem(problem))
}
