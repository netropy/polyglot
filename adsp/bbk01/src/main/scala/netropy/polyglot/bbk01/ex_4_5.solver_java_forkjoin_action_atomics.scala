package netropy.polyglot.bbk01

import java.util.concurrent.{ForkJoinPool, RecursiveAction, TimeUnit}
import java.util.concurrent.atomic.AtomicReference
import java.util.function.BinaryOperator


// TODO: outdated code

/** A concurrent B&B Solver using `ForkJoinPool` + atomic counters/reference.
  *
  * Like the preceding code version, this `ForkJoinPool`-based implementation
  * wraps the processing of a `Node` in a `RecursiveAction`.  However, the
  * local state of function solve() (incumbent and statistics counters), is
  * held in stateful `java.util.concurrent.atomic` objects.
  *
  * Using atomics for the statistics counters seems obvious as they're only
  * incremented by node processing (a subsequent code version will go further
  * and model them without side-effect as local objects).  For simplicity, a
  * new class [[BBStatisticsAtomic]] duplicates each counter as `LongAdder`.
  *
  * Although not necessary from a performance perspective, this code version
  * also substitutes an `AtomicReference` for the `@volatile` variable to
  * hold the incumbent.  The reference's `accumulateAndGet` operation
  * is used to atomically select a new incumbent (which requires to wrap
  * the lambda in a Java `BinaryOperator` instance).
  *
  * @see [[https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveAction.html]]
  * @see [[https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicReference.html]]
  * @see [[BBStatisticsAtomic]]
  */
class BBSolver_v21(implicit problem: BBProblem) extends BBSolver4 {

  val FORK_THRESHOLD_SIZE_OF_NODE_UNSET: Int = 8

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatisticsAtomic(size)
    stats.nNodesBranchedA.increment()
    BBStatistics.start(stats)

    // currently best solution ("incumbent")
    //
    // This code version shows the use of java.util.concurrent.AtomicReference
    // with java.util.function.BinaryOperator for the selection of nodes.
    //
    // Sufficient to use AtomicReference (or @volatile):
    // - concurrent reads from RecursiveActionSolvers.go()
    // - atomic updates from critical section in compareAndUpdateBest()
    val start = Node.start
    val best = new AtomicReference[Node]
    best.set(start.closed)
    val maxSumValueNode: BinaryOperator[Node] =
      (n0: Node, n1: Node) => if (n1.value > n0.value) n1 else n0

    /** Atomically updates the incumbent and notifies caller of solve(). */
    def compareAndUpdateBest(node: Node): Unit = {
      this.synchronized {
        if (best.accumulateAndGet(node, maxSumValueNode) == node)
          onSolution(node) // side-effect
      }
    }

    class RecursiveActionSolver(node: Node) extends RecursiveAction {

      def compute(): Unit = go(node)  // re-use recursive decomposition :-)

      def go(node: Node): Unit = {
        //node.assertIsConsistent(problem) // debug

        if (node.weight > problem.maxWeight) {  // infeasible
          stats.nNodesInfeasibleA.increment()
          return
        }

        if (valueUpperBound(node) <= best.get.value) {  // suboptimal
          stats.nNodesSuboptimalA.increment()
          return
        }

        if (node.unset.isEmpty) {  // possibly new best
          compareAndUpdateBest(node)  // side-effect
          stats.nNodesBestA.increment()
          return
        }

        // incomplete, branch
        val refined = node
        val nodes = branchNode(refined)
        stats.nNodesBranchedA.add(nodes.size)
        val h = nodes.headOption
        if (h.exists(_.unset.size < FORK_THRESHOLD_SIZE_OF_NODE_UNSET)) {
          nodes.foldLeft(())((u, n) => go(n))
          return
        }

        // TODO: check task selection order, perhaps foldLeft
        // traverses from left
        nodes.foldRight(())((n, u) => new RecursiveActionSolver(n).fork())
      }
    }

    ForkJoinPool.commonPool.execute(new RecursiveActionSolver(start))

    // see design note in preceding code version on awaitQuiescence()
    ForkJoinPool.commonPool.awaitQuiescence(60L, TimeUnit.SECONDS)
    BBStatistics.stop(stats)
    stats.copyAtomicLongs()
    stats
  }
}

object BBSolver_v21 extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v21()(new BBProblem(problem))
}
