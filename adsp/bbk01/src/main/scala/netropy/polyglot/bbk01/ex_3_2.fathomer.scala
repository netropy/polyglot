package netropy.polyglot.bbk01

import collection.{Seq => GSeq}  // support mutable and immutable collections


// TODO: fix scaladoc
/** Provides for fathoming of nodes with a given optimum. */
case class Fathomer
  (nodes: GSeq[Node],
    val best: Node,
    stats: BBStatistics,
    heuristics: BBHeuristics,
    onSolution: Node => Unit)
  (implicit problem: Problem) {

  /** Ensures this fathomer is consistent under given problem instance. */
  def assertIsConsistent(implicit problem: Problem): Boolean = {
    assert(nodes.forall(_.assertIsConsistent))
    assert(best.assertIsSolution)
    true
  }

  /** Returns a copy of this fathomer with the nodes replaced. */
  def updated(nodes1: GSeq[Node]): Fathomer =
    Fathomer(nodes1, best, stats, heuristics, onSolution)

  /** Returns a copy of this fathomer with the nodes and best replaced. */
  def updated(nodes1: GSeq[Node], best1: Node): Fathomer =
    Fathomer(nodes1, best1, stats, heuristics, onSolution)

  def checkIsInfeasible: Fathomer = {
    val feasible = nodes.filterNot(_.isInfeasible)
    val nInfeasible = nodes.size - feasible.size
    if (nInfeasible == 0) this
    else {
      stats.nNodesInfeasible += nInfeasible
      updated(feasible)
    }
  }

  def checkIsFathomed: Fathomer = {
    // TODO: document variants
    // as single pass, preserve relative order using (foldRight, +:)
    //val e = nodes.take(-1)
    //val (fathomed, nonFathomed) = nodes.foldRight(e, e) {
    //  case (n, (f, nf)) =>
    //    if (n.isFathomed) (n +: f, nf) else (f, n +: nf)  XXX not good
    //}
    //val fathomed = nodes.filter(_.isFathomed)
    //val nonFathomed = nodes.filterNot(_.isFathomed)
    val grouped = nodes.groupBy(_.isFathomed)
    //val fathomed = grouped.get(true).getOrElse(nodes.take(-1))
    //val nonFathomed = grouped.get(false).getOrElse(nodes.take(-1))
    val GSeq(fathomed, nonFathomed) =
      GSeq(true, false).map(grouped orElse { case _ => nodes.take(-1) })
    if (fathomed.isEmpty) this
    else {
      stats.nNodesFathomed += fathomed.size
      // no need to close a node per: included, refined, assertIsConsistent
      // val best1 = reduceSolutions(fathomed.map(_.closed))
      // assert(fathomed.forall(n => n eq n.closed))
      val best1 = reduceSolutions(fathomed)
      updated(nonFathomed, best1)
    }
  }

  def checkIsSuboptimal: Fathomer = {
    val capable = nodes.filterNot(_.isSuboptimalBy(best))
    val nIncapable = nodes.size - capable.size
    if (nIncapable == 0) this
    else {
      stats.nNodesSuboptimal += nIncapable
      updated(capable)
    }
  }

  def applyHeuristics: Fathomer = {

    // preserves collection type
    val (refined, proposed) = nodes.map(heuristics.bind).unzip

    val proposed1 = proposed.flatten
    val best1 = reduceSolutions(proposed1)
    stats.nNodesProposed += proposed1.size

    assert(refined.size == nodes.size)
    val nRefined = refined.zip(nodes).count(rn => rn._1 ne rn._2)
    stats.nNodesRefined += nRefined

    if (nRefined == 0 && proposed1.isEmpty) this  // minor optimization
    else updated(refined, best1)
  }

  def fathom: Fathomer = {

    require(assertIsConsistent)

    this
      .checkIsInfeasible
      .checkIsFathomed
      .checkIsSuboptimal
      .applyHeuristics
      .checkIsFathomed
      .checkIsSuboptimal

  } ensuring { f =>
    f.assertIsConsistent
    assert(f.best.value >= best.value)
    true
  }

  /** Returns the best solution between given nodes and the current `best`.
    *
    * Invokes `onSolution(s)` foreach solution `s` exceeding current optimum.
    */
  def reduceSolutions(solutions: GSeq[Node]): Node = {

    // already checked by go(), bind()
    //require(solutions.forall(_.assertIsSolution))
    //require(best.assertIsSolution)

    solutions.foldLeft(best) { (b, s) =>
      if (!s.isBetterThan(b)) b
      else {
        stats.nNodesBest += 1
        onSolution(s) // notify caller, side-effect
        s
      }
    }
  }
}
