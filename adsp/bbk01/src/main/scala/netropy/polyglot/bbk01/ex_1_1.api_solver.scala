package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** A solver for the 0/1 Knapsack Problem.
  *
  * This API is kept to a minimum for simplicity.  So far, `solve` is the
  * single abstract method (making this trait a Functional or SAM Interface).
  *
  * Clients may assume that solver implementations are thread-safe (though,
  * the current API makes repeated or concurrent invocations pointless).
  *
  * Notes: .../ex_1_1.r.api_solver.md
  *
  * Summary:
  *
  * - a solver for the 0/1 Knapsack Problem (no generalization as MILP)
  * - stateless: solver instance created to problem instance
  * - simple: single abstract method trait (SAM interface)
  * - dynamic: user-supplied _callback_ receives newly found solutions
  * - informative: a computational summary statistics returned as result
  *
  * @see [[SolverFactory]]
  */
trait Solver {

  /** Solves the configured problem instance.
    *
    * Usage Contract:
    *
    * 1) The solver invokes the `onSolution` callback only sequentially and
    *    only with monotonically improving solutions.
    *
    * 2) The passed callback must behave like an event handler: It must be
    *    _non-blocking_, _non-throwing_, and _lightweight in computation_.
    *
    * 3) The callback may be invoked from another thread, so, the callback
    *    must ensure for writes to _happen-before_ reads.
    *
    * @param user-supplied callback taking a newly found solution
    * @return computational statistics
    */
  def solve(onSolution: Node => Unit): Statistics

  //
  // Convenience method and Usage example
  //

  /** Solves the configured problem for just the final, optimal solution. */
  final def solve(): (Node, Statistics) = {

    @volatile  // the solver's writes must happen before caller's reads
    var best: Node = Node.empty
    val stats = solve(r => { best = r })
    (best, stats)
  }
}

/** Creates a solver initialized to a 0/1 Knapsack Problem instance. */
trait SolverFactory {

  /** Returns a solver for the given problem instance. */
  def create(problem: Problem): Solver
}
