package netropy.polyglot.bbk01

import collection.immutable.Seq  // default since Scala 2.13
import collection.mutable.Map


/** Data class for summary statistics on B&B-Solver runs.
  *
  * Most counters are variables, which a solver updates by side-effect.
  */
class BBStatistics(nVariables: Long = 0)
    extends Statistics(nVariables) {

  // search tree/nodes counters (locally updated by B&B computation)
  var nNodesBranched: Long = 0
  var nNodesRefined: Long = 0
  var nNodesProposed: Long = 0
  var nNodesBest: Long = 0
  var nNodesSuboptimal: Long = 0
  var nNodesFathomed: Long = 0
  var nNodesInfeasible: Long = 0

  override def toMap: Map[String, Any] = {
    val m = super.toMap
    m ++= Seq(
      "nNodesBranched" -> nNodesBranched,
      "nNodesRefined" -> nNodesRefined,
      "nNodesProposed" -> nNodesProposed,
      "nNodesBest" -> nNodesBest,
      "nNodesSuboptimal" -> nNodesSuboptimal,
      "nNodesFathomed" -> nNodesFathomed,
      "nNodesInfeasible" -> nNodesInfeasible,
    )
    m ++= jvmExecutionMap
    m ++= jvmEnvironmentMap
  }

  /** Adds the counters from the argument to this instance. */
  def += (s: BBStatistics): this.type = {
    nNodesBranched += s.nNodesBranched
    nNodesRefined += s.nNodesRefined
    nNodesProposed += s.nNodesProposed
    nNodesBest += s.nNodesBest
    nNodesSuboptimal += s.nNodesSuboptimal
    nNodesFathomed += s.nNodesFathomed
    nNodesInfeasible += s.nNodesInfeasible
    this
  }
}

object BBStatistics extends StatisticsHelper
