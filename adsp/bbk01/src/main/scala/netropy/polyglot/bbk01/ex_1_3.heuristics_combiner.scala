package netropy.polyglot.bbk01

// Scala 2.12<>13 difference: 2.12 uses collection.Seq for A* vargs
//import collection.immutable.{Seq}

import scala.concurrent.{Future, ExecutionContext, Await}
import scala.concurrent.duration.Duration
import scala.util.Success


/** Combines multiple heuristics for the tightest bound and best solution.
  *
  * This combiner sequentially runs the configured heuristics on a node (map)
  * and produces a node with the tightest bound and a best solution (reduce).
  *
  * Notes:
  *
  * For simplicity, this code assumes a maximization problem (like Knapsack).
  *
  * This class is a great candidate for parallelism:
  * - Most computing time is spent here.
  * - Heuristics may run concurrently (in principle).
  * - Results may be combined in any order (can introduce non-determinism).
  *
  * Theoretically, a HeuristicsCombiner forms a (commutative) Monoid with the
  * BBNullHeuristic as neutral element.  Practically, the order of reduction
  * may matter due to different nodes comparing equal under a chosen metric,
  * or due to rounding effects, for example.
  *
  * Possible specializations for #heuristics argument:
  *   = 0  allowed, behaves like BBNullHeuristics
  *   > 1  may require this limit to flag unnecessary overhead
  *   = 2  may add specialized handling of this most frequent case
  *
  * @param heuristics the algorithms to run, may be empty, not null
  * @param problem the input problem instance, not null
  */
class BBHeuristicsCombiner
  (heuristics: BBHeuristics*)
  (implicit problem: Problem)
    extends BBHeuristics {

  type Nodes = (Node, Option[Node])  // (refined, solution)

  /** Runs the heuristics sequentially and reduces their results. */
  def bind1(node: Node): Nodes = {

    val nodes = heuristics.map(_.bind1(node))
    reduce(node, nodes)
  }

  /** Left-Reduces the refined nodes and optional solutions (deterministic). */
  def reduce(node: Node, nodes: Seq[Nodes]): Nodes = {

    // assumes maximization problem (like Knapsack), could merge the folds
    val (refines, solutions) = nodes.unzip
    val bestMaxValue = refines.foldLeft(node)(Node.cmpByMaxValue.min)
    val bestUnsetSize = refines.foldLeft(node)(Node.cmpByUnsetSize.min)
    val bestRefined = bestUnsetSize.refined(bestMaxValue.maxValue)

    val bestSolution = solutions.flatten.reduceLeftOption(Node.cmpByValue.max)
    (bestRefined, bestSolution)
  }
}


/** Concurrently applies multiple heuristics and reduces their results. */
class BBHeuristicsCombinerConcurrent
  (heuristics: BBHeuristics*)
  (implicit problem: Problem,
    executor: ExecutionContext)
    extends BBHeuristicsCombiner(heuristics: _*) {

  override def bind1(node: Node): Nodes = {

    val nodesF = Future
      .traverse(heuristics)(h => Future(h.bind1(node)))
      .map(nodes => reduce(node, nodes))

    /* Notes:
     *
     * Minimal code addition to make sequential version concurrent.
     *
     * Above code runs in the `executor`:
     * - the heuristics as a Parallel Map operation
     * - a sequential Reduce pass on the results
     *
     * A parallel map operation on an Iterable/Traversable via Futures
     * implements   I[A] => I[B]
     * as           I[A] => I[Future[B]] => Future[I[B]] => I[B]
     *
     * In FP terms: Futures are a Traversable Functor with map() + functions
     *   sequence: (I[Future[A]])(...) => Future[I[A]]
     *   traverse: (I[A])(A => Future[B])(...) => Future[I[B]]
     *
     * The sequential Reduce is then chained to the Succes path via map().
     * No need for a Parallel Reduce, here, since not performance-critical.
     * Alternatively, run the short reduce pass in the caller's thread.
     */

    Await.result(nodesF, Duration.Inf)  // not ideal: use of blocking
  }
}

// For illustration: How to run in Scala's global execution context
// (may delegate to java.util.concurrent.ForkJoinPool.commonPool).
import ExecutionContext.Implicits.global

/** Runs heuristics in the global execution context and reduces results. */
class BBHeuristicsCombinerParGlobal
  (heuristics: BBHeuristics*)
  (implicit problem: Problem)
    extends BBHeuristicsCombinerConcurrent(heuristics: _*)
