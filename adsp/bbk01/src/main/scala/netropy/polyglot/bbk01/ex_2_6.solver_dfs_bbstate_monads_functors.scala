package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13

import cats.data.{State, Reader}
//import cats.syntax.all._  // CATS implicits/givens, extension methods etc

// TODO: shorten Sketch

/** A recursive, DFS B&B solver based on state transformations.
  *
  * Idea: Compose B&B as state transformations in a monad/functor context
  *       -> model state `S` as input/output data type for all operations
  *       -> define operations as state-transforming functions `S => S`
  *       -> compose state transformations in monad/functor contexts:
  *          -> try State Monad as pure state accumulation: `S => (S, Unit)`
  *          -> try Reader Monad w/ shared, constant state: `S0 => S`
  *          -> try functor composition on Identity: `S`
  *
  * Sketch:
  * ```
  *     // model all input/output of B&B operations as single data class
  *     case class BBState(node: Option[Node], best: Node) { ... }
  *
  *     // operations are then state transformations
  *     type BBStateF = BBState => BBState
  *
  *     // define B&B operations as state-transforming functions
  *     ...
  *     val checkIsFathomed: BBStateF = bb =>
  *       bb.node.fold(bb) { n =>
  *         if (!n.isFathomed) bb
  *         else { ... BBState(None, newBest) }
  *       }
  *     ...
  *
  *     // lift state transformations into various monad/functor contexts;
  *     // then check how (well) operations compose using `flatMap`, `map`
  *
  *     // ==> try State Monad as pure state accumulation: `S => (S, Unit)`
  *
  *     type BBStateM = StateM[BBState, Unit]
  *     val fathomSM: BBStateM =
  *       for {
  *         _ <- liftToSM(...)
  *         _ <- liftToSM(checkIsFathomed)
  *         ...
  *
  *     // ==> try Reader Monad w/ shared, constant state: `R => A`
  *
  *     class BBStateWithR(...) extends BBState(...) with Resource
  *
  *     type BBReaderM = ReaderM[BBStateWithR, BBState]
  *     val fathomRM: BBReaderM =
  *       for {
  *         r0 <- BBReaderM.get[BBStateWithR]
  *         r1 <- liftToRMF(...)(r0)
  *         r2 <- liftToRMF(checkIsFathomed)(r1)
  *         ...
  *
  *     // ==> try functor composition on Identity: `Some[A]`
  *
  *     val fathomIF: BBStateF =
  *       Some(_)
  *         .map(...)
  *         .map(checkIsFathomed)
  *         ...
  * ```
  *
  * Notes: .../ex_2_6.r.solver_dfs_bbstate_monads_functors.md
  *
  * Summary:
  *
  * - Only depth-first-search so far, function go() not tail-recursive
  *
  * + Excellent readability, extendability:
  *   + direct, uniform access to input/output: `bb.node`, `bb.best`
  *   + single `class BBState` can be extended
  *   + `reduceSolution` now a method of `BBState`, safe usage
  *   + branching & recursion on child nodes separate from fathoming
  *   + compact `for` comprehension or `flatMap`, `map` compositions
  *   + use of `StateM`, `ReaderM` a hook for extensions? (temp results, env)
  *   + composition as functor a hook for extensions? (pipeline parallelism?)
  *   + control flow uniformly expressed as data flow, still no side-effects
  *   ~ 14 lines of code for recursive function `go` (same as before ~14)
  *   ~ 10 +/-2 lines of code for fathom flow (same as before ~8)
  *
  * - Minor readability complaints:
  *   - no short-circuit behaviour, each function must deal with `None` node
  *   - obscure `lift...` wrapper methods for use of `StateM`, `ReaderM`
  *   - expert types: `StateM[..., Unit]`, `ReaderM[...WithR, ...]`
  *     => access to updated/constant state requires composition by `flatMap`
  *     => otherwise, just using a monad type as simple functor
  *   - by law, functor composition is `andThen`; nicer, but worth it?
  *   - better? "fluent" APIs enable composition by method chaining
  *
  * ==> Utilize "endo" signature of ops for a "fluent", class-based design
  */
class BBSolver_v07_bbstate_monads_functors_dfs
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated by side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    // B&B processsing state

    case class BBState(node: Option[Node], best: Node) {

      /** Returns given node if its value exceeds `best`, otherwise `best`.
        * Invokes the callback `onSolution(s)` when `s` exceeds `best`.
        */
      def reduceSolution(s: Node): Node = {

        //require(s.assertIsSolution)  // already checked by go(), bind()
        //require(best.assertIsSolution)  // already checked by go(), bind()

        if (!s.isBetterThan(best)) best
        else {
          stats.nNodesBest += 1
          onSolution(s) // notify caller, side-effect
          s
        }
      }
    }

    /*
     * B&B fathoming operations (as function values ready for composition)
     */

    type BBStateF = BBState => BBState

    val checkIsInfeasible: BBStateF = bb =>
      bb.node.fold(bb) { n =>
        if (!n.isInfeasible) bb
        else {
          stats.nNodesInfeasible += 1
          BBState(None, bb.best)
        }
      }

    val checkIsFathomed: BBStateF = bb =>
      bb.node.fold(bb) { n =>
        if (!n.isFathomed) bb
        else {
          stats.nNodesFathomed += 1
          BBState(None, bb.reduceSolution(n.closed))
        }
      }

    val checkIsSuboptimal: BBStateF = bb =>
      bb.node.fold(bb) { n =>
        if (!n.isSuboptimalBy(bb.best)) bb
        else {
          stats.nNodesSuboptimal += 1
          BBState(None, bb.best)
        }
      }

    val applyHeuristics: BBStateF = bb =>
      bb.node.fold(bb) { n =>
        val (refined, proposed) = heuristics.bind(n)
        if (refined ne n)
          stats.nNodesRefined += 1
        val best = proposed.fold(bb.best) { s =>
          stats.nNodesProposed += 1
          bb.reduceSolution(s)
        }
        BBState(Some(refined), best)
      }

    /*
     * Composed transformations using: State Monad with `Unit` result.
     *
     * Idea: Simplify, try `StateM` accumulating in state: `S => (S, Unit)`
     *
     * See the prior exercise for use of the state monad.
     */

    // monad type wrapping a state transformation, no use of result
    type BBStateM = State[BBState, Unit]
    val BBStateM = State

    // TODO: How to add extension method to companion object?
    //def unit[S, A](a: A): State[S, A] = State.pure(a)

    // helper function lifting a state transformation into state monad
    def liftToSM(f: BBStateF): BBStateM =
      BBStateM(s => (f(s), ()))

    val fathomSM_v0: BBStateM =  // composition of SM values, no parameters
      for {
        _ <- liftToSM(checkIsInfeasible)          // `_ <-` discarded result
        _ <- liftToSM(checkIsFathomed)
        _ <- liftToSM(checkIsSuboptimal)
        _ <- liftToSM(applyHeuristics)
        _ <- liftToSM(checkIsFathomed)
        _ <- liftToSM(checkIsSuboptimal)
      } yield ()

    val fathomSM_v1: BBStateM =  // desugared
      liftToSM(checkIsInfeasible)
        .flatMap(_ => liftToSM(checkIsFathomed)
          .flatMap(_ => liftToSM(checkIsSuboptimal)
            .flatMap(_ => liftToSM(applyHeuristics)
              .flatMap(_ => liftToSM(checkIsFathomed)
                .flatMap(_ => liftToSM(checkIsSuboptimal)
                  .map(_ => ()))))))

    val fathomSM_v2: BBStateM =  // per monad associativity, identity
      BBStateM.pure[BBState, Unit](())
        .flatMap(_ => liftToSM(checkIsInfeasible))
        .flatMap(_ => liftToSM(checkIsFathomed))
        .flatMap(_ => liftToSM(checkIsSuboptimal))
        .flatMap(_ => liftToSM(applyHeuristics))
        .flatMap(_ => liftToSM(checkIsFathomed))
        .flatMap(_ => liftToSM(checkIsSuboptimal))

    // no functor/`map`-based composition of state monad with `Unit` result

    /** Applies the selected Fathomer State Monad to the B&B state. */
    def fathom_sm(bb: BBState): BBState = {

      // select code version
      val fathomSM: BBStateM = fathomSM_v0  // _v0..2

      fathomSM.run(bb).value._1
    }

    /*
     * Composed transformations using: Reader Monad.
     *
     * Idea: Simplify, try `ReaderM` w/ shared, constant state: `S0 => S`
     *
     * Refresher:
     *       `ReaderM[-R, +A]` instances wrap an unary function `R => A`.
     *       This provides a read-only environment `R` for computations.
     *
     *       Functor/monad functions `unit`, `map`, and `flatMap` lift values
     *       and computations using `A`, and `R`, into the reader context.
     *
     *       Examples:
     *       - properties, settings, configuration data
     *       - dependency injection of resources, connections
     *
     * Notes:
     *
     * Generic Reader Monad available from CATS and other FP libaries:
     * [[https://typelevel.org/cats/datatypes/kleisli.html#monad-transformers]]
     *
     * Here, as exercise: `ReaderM` with unit- and property tests.
     */

    // monad type wrapping a state transformation BBState => BBState
    type BBReaderM = Reader[BBState, BBState]
    val BBReaderM = Reader

    // TODO: How to add extension method to companion object?
    //def unit[A](a: => A): Reader[Any, A] = Reader(_ => a)
    def get[R]: Reader[R, R] = Reader(identity)

    // helper function lifting a state transformation into reader monad
    def liftToRMF(f: BBStateF): BBState => BBReaderM =
      b => BBReaderM(_ => f(b))  // TODO: unit/pure

    // Note: Above wrapper `liftToRMF` just utilizes the BBReaderM as functor.
    //
    // Why lifting as `b => BBReaderM(f)` would be wrong:
    //   def liftToRMF(f: BBStateF) =
    //     b => BBReaderM.unit(f(b))  // correct, preserves `b` ~>  _ => f(b)
    //     b => BBReaderM(f)          // wrong, discards `b`    ~>  x => f(x)
    // When used as argument to flatMap, `b` has the accumulated mappings;
    // if disregarded, all `f` will be always applied to the initial state.

    val fathomRM_v0: BBReaderM =
      for {
        r0 <- get[BBState]
        r1 <- liftToRMF(checkIsInfeasible)(r0)
        r2 <- liftToRMF(checkIsFathomed)(r1)
        r3 <- liftToRMF(checkIsSuboptimal)(r2)
        r4 <- liftToRMF(applyHeuristics)(r3)
        r5 <- liftToRMF(checkIsFathomed)(r4)
        r6 <- liftToRMF(checkIsSuboptimal)(r5)
      } yield r6

    val fathomRM_v1: BBReaderM =  // desugared
      get[BBState]
        .flatMap(liftToRMF(checkIsInfeasible)(_)
          .flatMap(liftToRMF(checkIsFathomed)(_)
            .flatMap(liftToRMF(checkIsSuboptimal)(_)
              .flatMap(liftToRMF(applyHeuristics)(_)
                .flatMap(liftToRMF(checkIsFathomed)(_)
                  .flatMap(liftToRMF(checkIsSuboptimal)(_)
                    .map(r6 => r6)))))))

    val fathomRM_v2: BBReaderM =  // per monad associativity, identity
      get[BBState]
        .flatMap(liftToRMF(checkIsInfeasible))
        .flatMap(liftToRMF(checkIsFathomed))
        .flatMap(liftToRMF(checkIsSuboptimal))
        .flatMap(liftToRMF(applyHeuristics))
        .flatMap(liftToRMF(checkIsFathomed))
        .flatMap(liftToRMF(checkIsSuboptimal))

    // This `map`-based composition shows RM entirely used as functor, here:

    val fathomRM_v3: BBReaderM =  // per rm.flatMap(liftToRMF(f)) == rm.map(f)
      get[BBState]
        .map(checkIsInfeasible)
        .map(checkIsFathomed)
        .map(checkIsSuboptimal)
        .map(applyHeuristics)
        .map(checkIsFathomed)
        .map(checkIsSuboptimal)

    /** Applies the selected Fathomer Reader Monad to the B&B state. */
    def fathom_rm(bb: BBState): BBState = {

      // select code version
      val fathomRM: BBReaderM = fathomRM_v2  // _v0..3

      fathomRM(new BBState(bb.node, bb.best))
    }

    /*
     * Composed transformations using: `Some` as Identity Functor.
     *
     * Idea: Simplify further, try functor composition on Identity: `S`
     *
     * Notes:
     *
     * CATS provides identity just as type constructor `F[A] = A` (nice):
     * [[https://www.scala-exercises.org/cats/identity]]
     *
     * Here, we simply wrap the initial value in a `Some`.
     */

    val fathomIF_v0: BBStateF = bb =>
      (
        for {
          r0 <- Some(bb)
          r1 = checkIsInfeasible(r0)
          r2 = checkIsFathomed(r1)
          r3 = checkIsSuboptimal(r2)
          r4 = applyHeuristics(r3)
          r5 = checkIsFathomed(r4)
          r6 = checkIsSuboptimal(r5)
        } yield r6
      ).get

    val fathomIF_v1: BBStateF =  // desugared + detupled + de-lambda-ed
      Some(_)
        .map(checkIsInfeasible)
        .map(checkIsFathomed)
        .map(checkIsSuboptimal)
        .map(applyHeuristics)
        .map(checkIsFathomed)
        .map(checkIsSuboptimal)
        .get

    val fathomIF_v3: BBStateF =  // per functor composition
      Some(_).map(
        checkIsInfeasible andThen
          checkIsFathomed andThen
          checkIsSuboptimal andThen
          applyHeuristics andThen
          checkIsFathomed andThen
          checkIsSuboptimal
      ).get  // -> highlights that lift...unlift redundant

    /** Applies the selected Fathomer Identity Functor to the B&B state. */
    def fathom_if(bb: BBState): BBState = {

      // select code version
      val fathomF: BBStateF = fathomIF_v1  // _v0..2

      fathomF(bb)
    }

    /*
     * Branching & Recursion separate from BBState Transformation
     */

    /** Returns the branched child nodes of a node. */
    def branchedNodes(node: Option[Node]): Seq[Node] = {
      node.fold(Seq.empty[Node]) { n =>
        val nodes = branchNode(n)
        stats.nNodesBranched += nodes.size
        nodes
      }
    }

    /** Solves a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be solved
      * @param best a start solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // not: @annotation.tailrec
    def go(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      // select code version
      val fathomF: BBStateF = fathom_if  // _{sm,rm,if}

      // apply the transformation to the initial state
      val bb: BBState = fathomF(BBState(Some(node), best))

      // branch + recurse
      branchedNodes(bb.node).foldLeft(bb.best)((b, n) => go(n, b))

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    val start = Node.start
    val best = go(start, start.closed)

    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v07_bbstate_monads_functors_dfs_factory
  (heuristicsFactory: BBProblem => BBHeuristics)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v07_bbstate_monads_functors_dfs(
      heuristicsFactory)(new BBProblem(problem))
}
