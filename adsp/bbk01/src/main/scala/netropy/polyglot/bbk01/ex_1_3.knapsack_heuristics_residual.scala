package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13
import collection.immutable.{BitSet}


/** Cheap & simple 0/1 Knapsack heuristic analyzing residual weight & value.
  *
  * This heuristic: +updates upper bound, +fixes variables, -proposes solution
  *
  * Analyzing a node's residual weight & value yields a simple heuristic:
  * - fix variables to `0` whose inclusion would render the node infeasible
  * - update the node's upper bound by the sum value of its `unset` variables
  *
  * Notes:
  *
  * - This is basically a `filter` operation over a node's `unset` indices.
  *
  * - Two code versions are given below:
  *
  *   - strict `bind1_v0` filters `node.unset` looking up `problem.weight`
  *     testing each index for infeasibility
  *
  *   - lazy `bind1_v1` filters sorted `problem.indicesByWeight` looking up
  *     `node.unset`; the infeasibility test only applies while infeasible
  *
  * - This code turns out surprisingly performance-delicate, for example:
  *
  *   - strict `bind1_v0` seems faster than lazy `bind1_v1`
  *
  *   - better to avoid creating a closure, let compiler optimize,
  *     slightly worse:
  *     ```
  *       val residualWeight = problem.maxWeight - node.weight
  *       val feasible = ...
  *         ...(problem.weights(_) > residualWeight)
  *     ```
  *
  *   - better to always compute and update upper bound (may be outdated),
  *     even if no indices have been fixed to `0`; worse:
  *     ```
  *       val feasible = ...
  *         ...
  *       if (feasible.size == node.unset.size) (node, None) else {...}
  *     ```
  *
  * - Possible, further performance experiments:
  *
  *   - fuse filter+map operation (`bind1_v0`) using flatMap, foldLeft
  *   - fuse filter+dropWhile operation (`bind1_v1`) using flatMap, foldLeft
  *   - benchmark use of different collection types
  *     - .iterator, .toIterable (lazy)
  *     - .toSeq, .toIndexedSeq, .toArray, .toVector (immutable)
  *     - .toBuffer (mutable)
  *
  * @param problem the (enriched) input problem instance, not null
  */
class BBKnapsackHeuristicResidualValue
  (implicit problem: BBProblem)
    extends BBHeuristics {

  // select code version
  def bind1(node: Node): (Node, Option[Node]) = bind1_v0(node)  // v0..1

  def bind1_v0(node: Node): (Node, Option[Node]) = {

    // strict filter from node into problem
    val feasible = node.unset
      .toSeq
      .filter(problem.weights(_) <= problem.maxWeight - node.weight)

    val newMaxValue = node.value + feasible.map(problem.values).sum
    val newUnset = BitSet(feasible: _*)
    (node.refined(newUnset).refined(newMaxValue), None)
  }

  def bind1_v1(node: Node): (Node, Option[Node]) = {

    // lazy filter from problem into node, uses non-strict `dropWhile`
    val feasible = problem.indicesByWeight
      .iterator  // lazy, slightly faster
      .filter(node.unset.contains)
      .dropWhile(problem.weights(_) > problem.maxWeight - node.weight)
      .toSeq  // materialize

    val newMaxValue = node.value + feasible.map(problem.values).sum
    val newUnset = BitSet(feasible: _*)
    (node.refined(newUnset).refined(newMaxValue), None)
  }
}
