package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


// TODO: shorten

/** A recursive, DFS B&B solver based on `match`.
  *
  * Idea: Utilize Scala's versatile `match` expression
  *       -> consolidate control-flow using a single language construct
  *       -> try to refactor duplicate code for reuse
  *
  * Refresher:
  *
  * Scala's `match` operator is more expressive than `switch` in other
  * languages: `case`s allow for
  *   - pattern matching
  *   - testing of runtime type
  *   - testing of additional conditions via `if`-guards
  *
  * Sketch:
  * ```
  *     val (nodes, newBest) = node match {
  *       case n if n.isInfeasible => ...
  *         (Seq(), best)                             // prune
  *       case n if ... => ...
  *         (Seq(), best)                             // prune
  *       ...
  *       case n =>
  *         val (refined, proposed) = heuristics.bind(n)
  *         ...
  *         refined match {                           // need a new match
  *           case n if ... => ...
  *             (Seq(), best)                         // prune
  *           ...
  *           case n =>
  *             (branchNode(refined), best)
  *         }
  *     }
  *     recurse
  * ```
  *
  * Notes: .../ex_2_1.r.solver_dfs_match.md
  *
  * Summary:
  *
  * - Only depth-first-search so far, function go() not tail-recursive
  *
  * + Improved readability:
  *   + consolidated control flow using `match`
  *   + could factor out some duplications (but not all)
  *
  * - Code's still cluttered
  *   - still duplicate code
  *   - two levels of nesting
  *   ~ 46 lines of code for fathom+branch flow (down from ~66)
  *   ~ 57 lines of code for recursive function `go` (down from ~84)
  *
  * - Use or Abuse of `match` for control-flow purposes?
  *   - only used as a "switch" with guarded `case`s and fallback
  *   - not a single pattern-match in the `match` expressions
  *   - obscures further decomposition, parallelism
  *
  * ==> Try remove code duplication, refactor `case`s as Partial Functions
  */
class BBSolver_v02_match_dfs
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated per side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    /** Returns given node `s` if its value exceeds `best`, otherwise `best`.
      *
      * Note: reduceSolution(s, o) is not commutative.
      * Invokes the callback `onSolution(s)` when `s` exceeds `best`.
      */
    def reduceSolution(s: Node, best: Node): Node = {

      //require(s.assertIsSolution)  // already checked by go(), bind()
      //require(best.assertIsSolution)  // already checked by go(), bind()

      if (!s.isBetterThan(best)) best
      else {
        stats.nNodesBest += 1
        onSolution(s) // notify caller, side-effect
        s
      }
    }

    /** Solves a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be solved
      * @param best a start solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // not: @annotation.tailrec
    def go(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      val (nodes, best2) = node match {

        case n if n.isInfeasible =>
          stats.nNodesInfeasible += 1
          (Seq(), best)

        case n if n.isFathomed =>
          stats.nNodesFathomed += 1
          (Seq(), reduceSolution(n.closed, best))  // not commutative

        case n if n.isSuboptimalBy(best) =>
          stats.nNodesSuboptimal += 1
          (Seq(), best)

        case n =>

          // try advance node, optimum
          val (refined, proposed) = heuristics.bind(n)
          if (refined ne n)
            stats.nNodesRefined += 1
          val best1 = proposed.fold(best) { s =>
            stats.nNodesProposed += 1
            reduceSolution(s, best)  // not commutative
          }

          refined match {

            case n if (n ne node) && n.isFathomed =>
              stats.nNodesFathomed += 1
              (Seq(), reduceSolution(n.closed, best1))  // not commutative

            // dropped: ... if (((n ne node) || (best1 ne best)) && ...)
            case n if n.isSuboptimalBy(best1) =>
              stats.nNodesSuboptimal += 1
              (Seq(), best1)

            case n =>
              // incomplete, branch
              val nodes = branchNode(refined)
              stats.nNodesBranched += nodes.size
              (nodes, best1)
          }
      }

      // recurse
      nodes.foldLeft(best2)((b, n) => go(n, b))

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    val start = Node.start
    val best = go(start, start.closed)
    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v02_match_dfs_factory
  (heuristicsFactory: BBProblem => BBHeuristics)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v02_match_dfs(heuristicsFactory)(new BBProblem(problem))
}
