package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** Base class for B&B solvers adding support for branching nodes.
  *
  * Encapsulates the "Branching" part in Backtracking and Branch & Bound:
  * - `selectBranchIndex`: selects an index (variable) on which to branch
  * - `branchNode`: does the actual branching using `selectBranchIndex`
  *
  * Subclasses will add the other algorithmic parameters of B&B:
  * - "Bound": providing a dual (upper) bound on a node
  * - Node selection: the branch tree traversal strategy
  *
  * Notes:
  *
  * For the purpose of this exercise, this class just hardcodes the choice
  * of the strategy for selecting the branch index.  Different selection
  * criteria are offered here, including by `Value|Weight|ValueDensity`.
  *
  * Other, more flexible approaches for variable selection:
  * - this class taking a function `(Node)(BBProblem) => Option[Int]`
  * - this companion offering common selection criteria as `enum` values
  *
  * @param problem the input problem instance, not null
  */
abstract class BTSolver(implicit val problem: BBProblem)
    extends Solver {

  require(problem.assertIsConsistent)

  /** Decides on which unset variable to branch on next. */
  def selectBranchIndex(node: Node): Option[Int] = {

    // hardcoded choice of selection strategy (see above note):
    val branchCriterion = 1
    branchCriterion match {
      case 0 => node.unset.headOption
      case 1 => problem.indicesByValue.find(node.unset.contains)
      case 2 => problem.indicesByWeight.find(node.unset.contains)
      case 3 => problem.indicesByValueDensity.find(node.unset.contains)
    }

  } ensuring {
    _.forall(i => i < problem.size && node.unset.contains(i))
  }

  /** Branches a node into nodes with packed indices first, if feasible. */
  def branchNode(node: Node): Seq[Node] =
    selectBranchIndex(node).fold(Seq.empty[Node])(node.branch(_))
}
