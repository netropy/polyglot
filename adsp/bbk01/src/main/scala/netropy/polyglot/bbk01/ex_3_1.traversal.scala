package netropy.polyglot.bbk01

import java.lang.Runtime  // need: freeMemory, maxMemory

import collection.immutable.Seq


/** Predefined search or tree traversal strategies.
  *
  * The order in which nodes are processed is determined by a bipartition
  * `(next, later)`, with nodes in `next` to be fathomed and branched before
  * those in `later`.
  *
  * This bipartition supports different tree traversal strategies by a
  * function that gets the chance to reshuffle (repartition) the nodes in
  * `(next, later)` before the next processing step.
  * For example, a function that
  * - moves all nodes but the first from `next` to `later` causes
  *   depth-first-search, as the processing of sibling nodes gets deferred;
  * - keeps all nodes within `next` causes breadth-first-search traversal,
  *   as siblings will be processed before child nodes.
  *
  * In addition to predefined functions for DFS and BFS, two mixed strategies
  * are shown that switch between BFS<->DFS by an internal/external criterion,
  * such as the overall number of nodes or the available amount of memory.
  *
  * Applications can define their own traversal strategy by providing a
  * function value of type `Repartition`.
  */
object Traversal {

  /** Functions that reshuffle a bipartition of nodes.
    *
    * Must not return an empty `next` while having nodes in `later`:
    *   repartition: (next, later) => (next1, later1)
    *   require: (next != null && later != null)
    *   ensuring: (!next1.isEmpty || later1.isEmpty)
    */
  type Repartition = (Seq[Node], Seq[Node]) => (Seq[Node], Seq[Node])

  // TODO: see ex_2_8
  // Note: repartition should preserve actual types of `next` and `later`
  // + (h +: next.take(0), t ++: later)  // preserves both types
  // - (List(h), t ++: later)  // not preserving next's type
  // - (later.iterableFactory(h), t ++: later)  // since 2.13, not in 2.12
  // - (later.companion(h), t ++: later)  // .companion deprecated since 2.13

  /** Depth-first-search traversal (moves nodes to `later` except first). */
  val dfs: Repartition =
    (next: Seq[Node], later: Seq[Node]) => {
      // preserve actual collection types (.take/.companion/.iterableFactory)
      val empty = next.take(-1)
      next match {
        case hn +: tn => (hn +: empty, tn ++: later)
        case _ => later match {
          case hl +: tl => (hl +: empty, tl)
          case e => (empty, e)
        }
      }
    }

  /** Breadth-first-search traversal (keeps all nodes in `next`). */
  val bfs: Repartition =
    (next: Seq[Node], later: Seq[Node]) => {
      // preserve actual collection types (.take/.companion/.iterableFactory)
      val empty = later.take(-1)
      (next ++ later, empty)
    }

  /** BFS while free memory is above a (fixed) percentage, otherwise DFS. */
  val bfsMemBound: Repartition =
    (n: Seq[Node], l: Seq[Node]) => {
      val minFreeMemRatioBfs = 0.8
      val rt = Runtime.getRuntime()
      val freeMemRatio = rt.freeMemory().toDouble / rt.maxMemory().toDouble
      if (freeMemRatio < minFreeMemRatioBfs) dfs(n, l) else bfs(n, l)
    }
}
