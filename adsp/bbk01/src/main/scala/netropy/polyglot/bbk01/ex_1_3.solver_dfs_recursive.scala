package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** A recursive, depth-first-search B&B solver.
  *
  * Idea: Extend the Backtracking code to Branch & Bound
  *       -> add heuristics, may yield new nodes: refined, proposed solution
  *       -> repeat some tests on refined/proposed with "early outs"
  *
  * Sketch:
  * ```
  *     if (node.isInfeasible) { ... return best }
  *     if (node.isFathomed) { ... return best|newBest }
  *     if (node.isSuboptimalBy(best)) { return best }
  *
  *     val (refined, proposed) = heuristics.bind(node)
  *     ...
  *     if (refined.isFathomed) { ... return best|newBest }
  *     if (refined.isSuboptimalBy(best)) { return best }
  *
  *     val nodes = branchNode(refined)
  *     ...
  *     recurse
  * ```
  *
  * Notes: .../ex_1_3.r.solver_dfs_recursive.md
  *
  * Summary:
  *
  * + Implemented B&B: testing, heuristics, pruning, branching, recursion
  * - only depth-first-search so far, function `go` not tail-recursive
  * - problem: clutter, size, loss of readability
  * - problem: ad-hoc control flow with 10..7 dispersed `if..return`s
  * - problem: code duplications, not apparent how to refactorize
  *   ~ 66 lines of code for fathom+branch flow (up from ~20)
  *   ~ 84 lines of code for recursive function `go` (up from ~35)
  *
  * - How to consolidate control flow?  `match`: patterns, conditions, flow
  *
  * ==> Refactor: try model control flow just with `match`, reduce clutter
  */
class BBSolver_v01_recursive_dfs
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated per side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    /** Solves a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be solved
      * @param best a start solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // not: @annotation.tailrec
    def go(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      if (node.isInfeasible) {
        stats.nNodesInfeasible += 1
        return best
      }

      // Note: code clutter
      if (node.isFathomed) {
        stats.nNodesFathomed += 1
        if (!node.isBetterThan(best))
          return best

        val newBest = node.closed
        stats.nNodesBest += 1
        onSolution(newBest) // notify caller, side-effect
        return newBest
      }

      if (node.isSuboptimalBy(best)) {
        stats.nNodesSuboptimal += 1
        return best
      }

      // try to advance node, current best
      val (refined, proposed) = heuristics.bind(node)

      // Note: code clutter
      if (refined ne node)
        stats.nNodesRefined += 1

      // check if proposed solution is new best
      //
      // Note: code clutter & duplication
      val best1 = proposed.fold(best) { s =>
        stats.nNodesProposed += 1
        if (!s.isBetterThan(best))
          best
        else {
          stats.nNodesBest += 1
          onSolution(s) // notify caller, side-effect
          s
        }
      }

      // check if refined node is fathomed
      //
      // Note: code clutter & duplication
      if ((refined ne node) && refined.isFathomed) {
        stats.nNodesFathomed += 1
        if (!refined.isBetterThan(best1))
          return best1

        val best2 = refined.closed
        stats.nNodesBest += 1
        onSolution(best2) // notify caller, side-effect
        return best2
      }

      // check if refined node is suboptimal
      //
      // Note: code clutter & duplication
      if ((refined ne node) || (best1 ne best)) {
        if (refined.isSuboptimalBy(best1)) {
          stats.nNodesSuboptimal += 1
          return best1
        }
      }

      // incomplete, branch
      val nodes = branchNode(refined)
      stats.nNodesBranched += nodes.size

      // recurse (illustrated for |nodes| = 2..0, n)
      nodes match {
        case Seq(n1, n0) => go(n0, go(n1, best1))  // most likely cases first
        case Seq(n0) => go(n0, best1)
        case Seq() => best1
        case nodes => nodes.foldLeft(best1)((b, n) => go(n, b))  // general
      }

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    val start = Node.start
    val best = go(start, start.closed)
    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v01_recursive_dfs_factory
  (heuristicsFactory: BBProblem => BBHeuristics)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v01_recursive_dfs(heuristicsFactory)(new BBProblem(problem))
}
