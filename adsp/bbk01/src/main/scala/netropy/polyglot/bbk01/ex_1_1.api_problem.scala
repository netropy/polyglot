package netropy.polyglot.bbk01

import collection.immutable.{IndexedSeq}
import collection.immutable.{Seq}  // default since Scala 2.13
import collection.mutable.{Map, LinkedHashMap}  // keeps insertion order


/** An instance of the 0/1 Knapsack Problem.
  *
  * The problem representation here is specific to the 0/1 Knapsack Problem
  * (beyond scope: general formulation as Mixed Inter Linear Programming).
  *
  * The variables of a 0/1 Knapsack instance are represented as indices in
  * sequence fields `values` and `weights`, storing the profit and cost per
  * variable.  The scalar value `maxWeight` holds the capacity constraint.
  *
  * Convenience methods provide for consistency checking, `toMap` conversion
  * and pretty printing.
  *
  * Notes: .../ex_1_1.r.api_problem.md
  *
  * Summary:
  *
  * - an instance of the 0/1 Knapsack Problem (no generalization as MILP)
  * - immutable, value-comparable, hashable, serializable, matchable
  * - variables (items) represented as indices into values, weights
  * - simplified: `Long` instead of `Double` for values, weights (<-> FPTAS)
  * - time-efficient: `IndexedSeq` lookup, cached `size`
  *
  * @constructor creates a problem instance from passed values
  * @param values the profit associated with each index (or 0/1 variable)
  * @param weights the cost associated with each index (or 0/1 variable)
  * @param maxWeight the capacity not to be exceeded by indices' sum weight
  */
case class Problem(
  values: IndexedSeq[Long],
  weights: IndexedSeq[Long],
  maxWeight: Long) {

  require(values != null)
  require(weights != null)

  /** The number of indices (0/1 variables) in this problem instance. */
  val size: Int = values.size
  assertIsConsistent

  /*
   * Consistency Checking
   */

  /** Ensures consistency of this problem instance.
    *
    * Some restrictions here help simplify coding of solver implementations
    * (for example, all values/weights > 0).  No loss of generality, since
    * problem instances can be preprocessed to meet these criteria.
    */
  def assertIsConsistent: Boolean = {
    assert(maxWeight > 0)
    assert(values.size == size)
    assert(values.size > 0)
    assert(values.forall(_ > 0))
    assert(weights.size == size)
    assert(weights.forall(_ > 0))
    assert(weights.forall(_ <= maxWeight),  // some test datasets = maxWeight
      s"weights.forall(_ <= maxWeight): "
        + s"maxWeight=$maxWeight, weights=$weights")
    true
  }

  /*
   * Pretty Printing
   */

  def toMap: Map[String, Any] = LinkedHashMap(
    "size" -> size,
    "values" -> values,
    "weights" -> weights,
    "maxWeight" -> maxWeight,
  )

  def mkString: String =
    mkString("Problem(\n    ", "\n    ", "\n)")

  def mkString(sep: String): String =
    mkString("Problem(", sep, ")")

  def mkString(start: String, sep: String, end: String): String =
    toMap.mkString(start, sep, end)
}
