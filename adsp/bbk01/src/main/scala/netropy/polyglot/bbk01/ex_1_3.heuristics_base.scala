package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** Base class for heuristics providing upper and lower bounds.
  *
  * Method `bind` runs approximate algorithms on a node, optionally
  * - solving a problem relaxation for a tighter bound (dual heuristic)
  * - guessing a feasible solution from the node (primal heuristics).
  *
  * Refresher:
  *
  * - A dual heuristic solves a problem relaxation to provide an upper bound
  *   on the maximation problem instance encoded by a node.
  *
  * - If a node's upper bound does not exceed the value of a currently known
  *   solution to the root problem, this node cannot possibly improve that
  *   solution and, hence, can be discarded (pruned).
  *
  * - A heuristic may also determine that some of the node's variables can
  *   be permanently fixed to `0` or `1`.  Either action, fixing variables
  *   or calculating a tighter bound, then produces a _refined_ node.
  *
  * - A primal heuristic tries to guess good, feasible solutions.  Since any
  *   solution to a subproblem is feasible to the root problem, the solution
  *   value provides a global lower bound (for maximization problems).
  *
  * - This heuristics component may therefore also try to complete a partial
  *   result, as encoded by a node, into a new, feasible solution, which is
  *   returned as an optional, _proposed_ node.
  *
  * @param problem the input problem instance, not null
  */
abstract class BBHeuristics(implicit problem: Problem) {

  require(problem != null)

  /** Tries to tighten a node's bound, fix variables, or guess a solution.
    *
    * @param node the subproblem instance, not: null, infeasible, fathomed
    * @return the same or a refined node, an optional feasible solution
    */
  final def bind(node: Node): (Node, Option[Node]) = {

    require(node != null)
    require(!node.isInfeasible)
    require(!node.isFathomed)

    bind1(node)

  } ensuring { r =>

    val (refined, proposed) = r  // dotty: (refined, proposed) => ...
    if (refined ne node) {
      refined.assertIsRefinedOf(node)
      assert(!refined.isInfeasible)  // reasonable to ask of heuristics
      //assert(!refined.isFathomed)  // solver's task to count fathomed nodes
    }
    proposed.forall(_.assertIsSolution)
    true
  }

  /** Implementation of `bind` w/o checking of pre-/post-conditions. */
  def bind1(node: Node): (Node, Option[Node])
}


/** Predefined factory method returning the "no-op" heuristics. */
object BBHeuristics {

  def none(problem: Problem): BBHeuristics =
    new BBNullHeuristic()(problem)
}


/** Dummy or "no-op" heuristic returning the node unchanged. */
class BBNullHeuristic
  (implicit problem: Problem)
    extends BBHeuristics {

  def bind1(node: Node): (Node, Option[Node]) = (node, None)
}
