package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** A recursive, DFS B&B solver based on partial functions.
  *
  * Idea: Utilize Scala's native support for Partial Functions
  *       -> refactor the match-clauses "cond => action" into a PFs
  *       -> use `orElse` to compose PFs for bail out / fallback
  *       -> use `andThen` to concatenate PFs for chaining
  *       => model the PF signatures that support `orElse`, `andThen`
  *
  * Refresher:
  *       Syntax for PF literals: `{ case ... => ... ; case ... }`
  *
  *       Type `scala.PartialFunction[-A, +B] extends A => B`
  *       - `isDefinedAt(a)` tests if argument `a` is in domain (=matched)
  *       - scaladoc: `apply(a)` undefined/may not throw on `!isDefined(a)`
  *       => caution: caller must check `isDefinedAt(a)` before `apply(a)`
  *       => pitfall: PFs pass as unary functions, can `apply` w/o knowing
  *
  *       Usage (simplified):
  *       - completion:        `pf0[A, B] orElse pf1[C, D] => pf2[A&C, B|D]`
  *       - concatenation:     `pf0[A, B] andThen pf1[B, C] => pf2[A, C]`
  *       - `lift`, `unlift`:  `pf[A, B]  <=>  f: A => Option[B]`
  *
  *       => composition: need compatible PF signatures!
  *       => lifting: do the computation on Option Monad level?
  *
  *       (Caveat: Scaladoc on [[PartialFunction]] inaccurate on 3 counts?)
  *
  * Task: How to model PF signatures that support composition?
  *
  *       pruning ops, conditional:        (node, best) =>            (best)
  *       refining op, unconditional:      (node, best) =>      (node, best)
  *
  *       branching, unconditional:        (node, best) => (Seq[node], best)
  *       recurse, unconditional:     (Seq[node], best) =>            (best)
  *       => branch andThen recurse:       (node, best) =>            (best)
  *
  *       CLUE: pruning ~ reduce, refining ~ map, branch_recurse ~ reduce
  *
  *       ==> Solved:
  *           + can `orElse`-compose pruning ops, branch_recurse
  *           + can `andThen`-compose refining op with pruning
  *
  * Sketch:
  * ```
  *     // Note: partial function literals require a target type
  *
  *     type BBPruneOp = PartialFunction[(Node, Node), Node]
  *     type BBRefineOp = PartialFunction[(Node, Node), (Node, Node)]
  *
  *     val handleIsInfeasible: BBPruneOp = {
  *       case (n, b) if n.isInfeasible => ... b    // conditional, partial
  *     }
  *     ...
  *
  *     val applyHeuristics: BBRefineOp = {
  *       case (n, b) => ... heuristics.bind(n)     // unconditional on (_,_)
  *     }
  *
  *     val applyBranchAndRecurse: BBPruneOp = {
  *       case (n, b) =>                            // unconditional on (_,_)
  *         branch(n).foldLeft(b)(... recurse ...)
  *     }
  *
  *     val applyFathomOrRecurse: BBPruneOp =       // composition
  *           handleIsInfeasible orElse             // try prune...
  *           ...
  *           applyHeuristics andThen               // refine
  *             handleIsFathomed orElse             // try prune again...
  *             ...
  *             applyBranchAndRecurse               // last resort
  *
  *     applyFathomOrRecurse(node, best)            // no need: isDefined()!
  * ```
  *
  * Notes: .../ex_2_2.r.solver_dfs_partial_functions.md
  *
  * Summary:
  *
  * - Only depth-first-search so far, function go() not tail-recursive
  *
  * + Greatly improved readability:
  *   + re-use of combined entity: `{ case <cond> => <action> }`
  *   + great gain in abstraction:
  *     + 2 function signatures for: pruning, refining, branching/recursing
  *     + 8 simple functions (6 partial + 2 total) plus recursive `go`
  *     + control + data flow uniformly expressed with `orElse`, `andThen`
  *   ~  8 lines of code for fathom+branch flow (down from ~46)
  *   ~ 25 lines of code for recursive function `go` (down from ~57)
  *
  * - Code might still be seen as difficult:
  *   - 2 "expert" types:
  *       `type BBPruneOp = PartialFunction[(Node, Node), Node]`
  *       `type BBRefineOp = PartialFunction[(Node, Node), (Node, Node)]`
  *   - the `Option`-lifted, version reads worse, ~35 lines of code for `go`
  *   - PF's domain? `case`s with _computational_ tests: `IsSuboptimal`
  *   => Use or Abuse of partial functions for control-flow purposes?
  *
  * - How "functional" is programming with Partial Functions?
  *   - FP's promise is _composability & robustness_ (see Note above)
  *   - monadic FP types for handling edge-cases, to make functions total:
  *       `Option`, `Either`, `Try` (+ types from FP libraries...)
  *   => strong preference for total functions over partial functions
  *
  * - For (total) functions: what "orElse"-style composition is available?
  *   => some monadic types offer chaining with "short-circuiting" logic
  *
  * ==> Try to model B&B processing using `Option`, `Either`
  */
class BBSolver_v03_partial_functions_dfs
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated per side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    // Refactored Helper Method

    /** Returns given node if its value exceeds `best`, otherwise `best`.
      *
      * Note: reduceSolution(s, o) is not commutative.
      * Invokes the callback `onSolution(s)` when `s` exceeds `best`.
      */
    def reduceSolution(s: Node, best: Node): Node = {

      //require(s.assertIsSolution)  // already checked by go(), bind()
      //require(best.assertIsSolution)  // already checked by go(), bind()

      if (!s.isBetterThan(best)) best
      else {
        stats.nNodesBest += 1
        onSolution(s) // notify caller, side-effect
        s
      }
    }

    // Partial Functions on a (node, best) pair

    type BBPruneOp = PartialFunction[(Node, Node), Node]
    type BBRefineOp = PartialFunction[(Node, Node), (Node, Node)]

    val handleIsInfeasible: BBPruneOp = {
      case (n, b) if n.isInfeasible =>
        stats.nNodesInfeasible += 1
        b
    }

    val handleIsFathomed: BBPruneOp = {
      case (n, b) if n.isFathomed =>
        stats.nNodesFathomed += 1
        reduceSolution(n.closed, b)  // not commutative
    }

    val handleIsSuboptimal: BBPruneOp = {
      case (n, b) if n.isSuboptimalBy(b) =>
        stats.nNodesSuboptimal += 1
        b
    }

    val applyHeuristics: BBRefineOp = {
      case (n, b) =>
        val (refined, proposed) = heuristics.bind(n)
        if (refined ne n)
          stats.nNodesRefined += 1
        val best1 = proposed.fold(b) { s =>
          stats.nNodesProposed += 1
          reduceSolution(s, b)  // not commutative
        }
        (refined, best1)
    }

    // Refactored Branching & Recursion

    /** Returns the branched child nodes of a node. */
    def branchedNodes(node: Node): Seq[Node] = {
      val nodes = branchNode(node)
      stats.nNodesBranched += nodes.size
      nodes
    }

    /** Solves a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be solved
      * @param best a start solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // select code version
    def go(node: Node, best: Node): Node = go_v0(node, best)  // _v0..1

    // not: @annotation.tailrec
    def go_v0(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      // must define this PF here to avoid forward reference:

      val applyBranchAndRecurse: BBPruneOp = {  // this PF always matches
        case (node1, best1) =>
          branchedNodes(node1).foldLeft(best1)((b, n) => go_v0(n, b))
      }

      // composed partial function:

      val applyFathomOrRecurse: BBPruneOp =
        ( handleIsInfeasible orElse
          handleIsFathomed orElse
          handleIsSuboptimal orElse
          ( applyHeuristics andThen
            ( handleIsFathomed orElse
              handleIsSuboptimal orElse
              applyBranchAndRecurse ) ) )

      applyFathomOrRecurse(node, best)  // safe, last PF always matches

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    // not: @annotation.tailrec
    def go_v1(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      // must define this PF here to avoid forward reference

      val applyBranchAndRecurse: BBPruneOp = {  // this PF always matches
        case (node1, best1) =>
          branchedNodes(node1).foldLeft(best1)((b, n) => go_v1(n, b))
      }

      // lifted partial functions: domain => Option[codomain]

      val handleIsInfeasibleF = (handleIsInfeasible.lift)(_)
      val handleIsFathomedF = (handleIsFathomed.lift)(_)
      val handleIsSuboptimalF = (handleIsSuboptimal.lift)(_)
      val applyHeuristicsF = (applyHeuristics.lift)(_)
      val applyBranchAndRecurseF = (applyBranchAndRecurse.lift)(_)

      // composed, lifted partial functions

      val applyFathomOrRecurse: ((Node, Node)) => Option[Node] =
        ( nb =>
          ( handleIsInfeasibleF(nb) orElse         // O.orElse <- PF.orElse
            handleIsFathomedF(nb) orElse
            handleIsSuboptimalF(nb) orElse
            ( applyHeuristicsF(nb) flatMap         // O.flatMap <- F.andThen
              ( nb1 =>
                ( handleIsFathomedF(nb1) orElse
                  handleIsSuboptimalF(nb1) orElse
                  applyBranchAndRecurseF(nb1) ) ) ) ) )

      applyFathomOrRecurse(node, best).get  // safe, always adds up to Some

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    val start = Node.start
    val best = go(start, start.closed)
    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v03_partial_functions_dfs_factory
  (heuristicsFactory: BBProblem => BBHeuristics)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v03_partial_functions_dfs(
      heuristicsFactory)(new BBProblem(problem))
}
