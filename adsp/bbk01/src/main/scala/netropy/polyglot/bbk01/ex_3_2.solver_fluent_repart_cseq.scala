package netropy.polyglot.bbk01

import collection.{Seq => CSeq}  // support mutable and immutable collections


// TODO: add scaladoc + design discussion + summary

/** A recursive B&B solver with fluent node-fathoming API.
  *
  * Summary:
  * + user-configurable traversal strategies
  * + function go() tailrecursive
  */
class BBSolver_v11_fluent_fathomer_repart_cseq
  (heuristicsFactory: BBProblem => BBHeuristics,
    emptyNodes: CSeq[Node],
    repartitioned: TraversalPF.Repartition)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  require(heuristicsFactory != null)
  require(emptyNodes != null && emptyNodes.isEmpty)
  require(repartitioned != null)

  // TODO: scaladoc
  def branchedNodes(nodes: CSeq[Node], stats: BBStatistics): CSeq[Node] = {
    val nodes1 = nodes.flatMap(branchNode)  // preserves collection type
    stats.nNodesBranched += nodes1.size
    nodes1
  }

  def solve(onSolution: Node => Unit): BBStatistics = {

    /** Solves the nodes in `next` and `later` by tail-recursion.
      *
      * @param next contains the nodes to be fathomed with current optimum
      * @param later the pool of nodes to be fathomed along with branched nodes
      * @return a fathomer with no nodes and the current optimum, statistics
      */
    @annotation.tailrec
    def go(next: Fathomer, later: CSeq[Node]): Fathomer = {

      // debug: incorrectly pruned node
      // import collection.immutable.{BitSet}
      // val s = Node(BitSet.empty, BitSet(...),.,.,.)
      // val has_s = next.nodes.exists(_.isAllowedBy(s))
      //
      // fathomed...branched...repartitioned
      //
      // val remaining = next1 ++ later1 ++ CSeq(fathomed.best)
      // if (has_s && !remaining.exists(_.isAllowedBy(s))) {
      //   println(s"----------------\n")
      //   println(s"s=${s}\n")
      //   println(s"next.best=${next.best}\n")
      //   println(s"next.nodes=${next.nodes}\n")
      //   println(s"!!!!!!!!!!!!!!!!\n")
      //   println(s"fathomed.best=${fathomed.best}\n")
      //   println(s"fathomed.nodes=${fathomed.nodes}\n")
      //   println(s"branched=${branched}\n")
      //   println(s"next1=${next1}\n")
      //   println(s"later1=${later1}\n")
      //   assert(false)
      // }

      val fathomed = next.fathom  // map operation
      val branched = branchedNodes(fathomed.nodes, fathomed.stats)  // map
      val (next1, later1) = repartitioned(branched, later)  // reshuffle
      assert(!next1.isEmpty || later1.isEmpty)

      if (next1.isEmpty) fathomed
      else go(fathomed.updated(next1), later1)
    }

    val stats = new BBStatistics(nVariables=problem.values.size)
    BBStatistics.start(stats)

    stats.nNodesBranched = 1
    val start = Node.start
    val best = start.closed
    val next = start +: emptyNodes
    val later = emptyNodes
    val done = go(Fathomer(next, best, stats, heuristics, onSolution), later)
    assert(done.nodes.isEmpty)

    BBStatistics.stop(stats)
    stats
  }
}

// TODO: cleanup API, names

/** Creates and initializes the solver to a problem instance. */
class BBSolver_v11_fluent_fathomer_repart_cseq_factory
  (heuristicsFactory: BBProblem => BBHeuristics,
    emptyNodes: CSeq[Node],
    repartitioned: TraversalPF.Repartition)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v11_fluent_fathomer_repart_cseq(
      heuristicsFactory, emptyNodes, repartitioned)(new BBProblem(problem))
}
