package netropy.polyglot.bbk01

import java.util.concurrent.{ForkJoinPool, ForkJoinTask, RecursiveTask}


// TODO: outdated code

/** A concurrent B&B Solver using `ForkJoinPool` with `RecursiveTask`s.
  */
class BBSolver_v22(implicit problem: BBProblem) extends BBSolver4 {

  /** TODO: document */
  val FORK_THRESHOLD_SIZE_OF_NODE_UNSET: Int = 8

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)
    stats.nNodesBranched = 1
    BBStatistics.start(stats)


    // currently best solution ("incumbent")
    //
    // Sufficient to use @volatile (or java.util.concurrent.AtomicReference):
    // - concurrent reads from RecursiveActionSolvers.go()
    // - atomic updates from critical section in compareAndUpdateBest()
    val start = Node.start
    @volatile
    var best = start.closed

    /** Atomically updates the incumbent and notifies caller of solve(). */
    def compareAndUpdateBest(node: Node): Unit = {
      this.synchronized {
        if (node.value > best.value) {  // new incumbent
          best = node
          onSolution(node) // side-effect
        }
      }
    }

    class RecursiveTaskSolver(node: Node, stats: BBStatistics)
        extends RecursiveTask[BBStatistics] {

      def compute(): BBStatistics = go(node)  // use recursive decomposition

      def go(node: Node): BBStatistics = {
        //node.assertIsConsistent(problem) // debug

        if (node.weight > problem.maxWeight) {  // infeasible
          stats.nNodesInfeasible += 1
          return stats
        }

        if (valueUpperBound(node) <= best.value) {  // suboptimal
          stats.nNodesSuboptimal += 1
          return stats
        }

        if (node.unset.isEmpty) {  // possibly new best
          compareAndUpdateBest(node)  // side-effect
          stats.nNodesBest += 1
          return stats
        }

        // incomplete, branch
        val refined = node
        val nodes = branchNode(refined)
        stats.nNodesBranched += nodes.size
        val h = nodes.headOption
        if (h.exists(_.unset.size < FORK_THRESHOLD_SIZE_OF_NODE_UNSET)) {
          nodes.foldLeft(())((u, n) => go(n))  // side-effect, ignoring return
          return stats
        }

        // TODO: could use
        // Scala 2.12
        //import collection.JavaConverters._
        // public static void invokeAll(ForkJoinTask<?>... tasks)
        // public static void invokeAll(ForkJoinTask<?> t1,ForkJoinTask<?> t2)

        // TODO: check task selection order, perhaps foldLeft, foldRight
        // Java use-site variance: RecursiveTask[V].fork(): ForkJoinTask[V]
        val empty = List.empty[ForkJoinTask[BBStatistics]]
        val tasks = nodes.foldRight(empty) { (n, l) =>
          val t = new RecursiveTaskSolver(n, new BBStatistics())
          t.fork() :: l
        }
        tasks.foldLeft(stats)((s, t) => s += t.join())

        // TODO: comment
        // performance: f1f0j1j0 ~ f0f1j1j0 < f1f0j0j1 < f0f1j0j1
        //val s1 = new RecursiveTaskSolver(n1, new BBStatistics()).fork()
        //val s0 = new RecursiveTaskSolver(n0, new BBStatistics()).fork()
        //stats += s1.join()
        //stats += s0.join()

        // performance: same as f1f0j1j0 ~ f0f1j1j0
        // example given in javadoc
        //  java.util.concurrent.RecursiveTask<V>
        //val s0 = new RecursiveTaskSolver(n0, new BBStatistics()).fork()
        //new RecursiveTaskSolver(n1, stats).compute() += s0.join()

        // performance: same as f1f0j1j0 ~ f0f1j1j0
        //val s0 = new RecursiveTaskSolver(n0, new BBStatistics()).fork()
        //go(n1).incrementBy(s0.join())
      }
    }

    val task = new RecursiveTaskSolver(start, new BBStatistics())
    stats += ForkJoinPool.commonPool.invoke(task)

    BBStatistics.stop(stats)
    stats
  }
}

object BBSolver_v22 extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v22()(new BBProblem(problem))
}
