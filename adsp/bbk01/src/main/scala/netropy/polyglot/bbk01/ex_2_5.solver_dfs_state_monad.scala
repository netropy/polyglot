package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13

import cats.data.State
//import cats.syntax.all._  // CATS implicits/givens, extension methods etc

// TODO: shorten Sketch


/** A recursive, DFS B&B solver based on the State Monad.
  *
  * Idea: Utilize the `State Monad` as context for B&B state transformations
  *       -> avoids the mutable state with `Option` or right-bias of `Either`
  *       -> model the B&B operations as functions mapping state vs result
  *       -> compose fathoming as series of (`flatMap`) transformations
  *
  * Refresher:
  *       `StateMonad[S, +A]` instances wrap an unary function `S => (S, A)`,
  *       which transforms a state `s0 => s1` and produces a value `a`.
  *
  *       Functor/monad functions `unit`, `map`, and `flatMap` lift values
  *       and computations on `A`, and also on `S`, into the state context.
  *
  *       SM provides: passing an updatable state through a computation
  *       without resorting to side-effects on variables or mutable objects.
  *
  * Sketch:
  * ```
  *     // monad type that models B&B state transformations as a function:
  *     //     best => (newBest, nodeOption)
  *     //   - the `best` solution is passed through as (hidden) state
  *     //   - the `nodeOption` is passed as a operation argument/result
  *     type BBState = State[Node, Option[Node]]
  *
  *     // B&B operations are then formulated as:
  *     //    nodeOption => ... State(best => ... (newBest, newNodeOption))
  *     //
  *     // i.e., return a SM that preserves/updates the state/node/or both
  *
  *     ...
  *     def checkIsFathomed(o: Option[Node]): BBState =
  *       o.fold(...) { n =>                                 // NEW: test
  *           if (!n.isFathomed)  State(b => (b, o))        // repack SM
  *           else { ... State(b => ... (newBest, None)) }  // update SM
  *       }
  *     ...
  *
  *     val fathom: Node => BBState = node =>  // compose B&B transformation
  *       State.unit(Some(node))
  *         .flatMap(...)                       // state passed implicitly
  *         .flatMap(checkIsFathomed(_))        // result passed explicitly
  *         .flatMap(...)
  *     ...
  *
  *     // apply the transformation to the initial node, and then state
  *     val (newBest, newNodeOpt) = fathom(node)(best)
  *
  *     // branch + recurse
  * ```
  *
  * Notes: .../ex_2_5.r.solver_dfs_state_monad.md
  *
  * Summary:
  *
  * - Only depth-first-search so far, function go() not tail-recursive
  *
  * + Minor improvements in readability:
  *   + no suspicious type: `Either[Node, (Node, Node)]`
  *   + no side-effect on mutable state as with `Option[Node]`
  *   + implicit passing of state, even of intermediate results
  *   + split recursion on child nodes from fathoming
  *   + fathom_v2(): readable and robust flat `.flatMap` chain
  *   ~ 14 lines of code for recursive function `go` (down from ~19)
  *   ~  8 lines of code for fathom flow (down from ~10)
  *
  * - Yet, the code's readability has worsened:
  *   - new obscure type: `BBState = State[Node, Option[Node]]`
  *   - not common knowledge: signature, usage pattern translate into
  *       `best => (newBest, nodeOption)`
  *       `nodeOption => ... State(best => ... (newBest, newNodeOption))`
  *   - with "naive/efficient" approach: non-uniform code for input & result
  *   - with "uniform-access" approach: boilerplate for unpacking, repacking
  *
  * - Loss of functionality: short-circuit behaviour of `Option`
  *   - each operation must now test for `None`
  *   + can factor out test+boilerplate into a wrapper method ("lift")
  *   - but what's the point, can as well use simpler monad/functor
  *   => This loss due to combining monads?  Overlooked something?
  *
  * ==> This solution, as it is: substantial costs, unclear benefits
  * ==> Formulate B&B as state transformations, try simpler monads/functors
  */
class BBSolver_v06_state_monad_dfs
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated by side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    // Helper Method

    /** Returns given node if its value exceeds `best`, otherwise `best`.
      *
      * Note: reduceSolution(s, o) is not commutative.
      * Invokes the callback `onSolution(s)` when `s` exceeds `best`.
      */
    def reduceSolution(s: Node, best: Node): Node = {

      //require(s.assertIsSolution)  // already checked by go(), bind()
      //require(best.assertIsSolution)  // already checked by go(), bind()

      if (!s.isBetterThan(best)) best
      else {
        stats.nNodesBest += 1
        onSolution(s) // notify caller, side-effect
        s
      }
    }

    /*
     * B&B processsing state
     */

    // monad type for state transformations: best => (newBest, nodeOption)
    type BBState = State[Node, Option[Node]]
    val BBState = State

    // TODO: How to add extension method to companion object?
    //def unit[S, A](a: A): State[S, A] = State.pure(a)

    /*
     * B&B Operations as Monad Functions #1: Naive/Efficient Approach
     *
     * Tries to avoid unpacking and repacking of `Option` and `BBState`.
     *
     * Poor Readibility:
     *
     * - loss of functionality: short-circuit behaviour of `Option`
     *
     * - boilerplate test:  `o.fold(doneState) { n => ... }`
     *
     * - non-uniform access to input, state:
     *     input node:     `def xxx(o: Option[Node]) = o.fold(...){ n => ...}`
     *     best:            `... => ... State(b => { ... })
     *     => embedded computations
     *
     * - different patterns for same return value
     *     delete node:                     update node:
     *     `doneState`                      `... BBState.pure(x)`
     *     `State(b => ... (b, None)`      `State(b => ... (b, x))`
     *
     * ==> Better unpack all and do processing from within lambda.
     */

    // for the simple case of node => None, this SM preserves the state
    val doneState: BBState = BBState.pure(None)  // = State(b => (b, None))

    def checkIsInfeasible_v0(o: Option[Node]): BBState =
      o.fold(doneState) { n =>                              // keep s, r
        if (!n.isInfeasible) BBState.pure(o)                 // keep s, r
        else {
          stats.nNodesInfeasible += 1
          doneState                                         // update r
        }
      }

    def checkIsFathomed_v0(o: Option[Node]): BBState =
      o.fold(doneState) { n =>                              // keep s, r
        if (!n.isFathomed) BBState.pure(o)                   // keep s, r
        else {
          stats.nNodesFathomed += 1
          State(b => (reduceSolution(n.closed, b), None))  // update s, r
        }
      }

    def checkIsSuboptimal_v0(o: Option[Node]): BBState =
      o.fold(doneState) { n =>                              // keep s, r
        State(b =>
          if (!n.isSuboptimalBy(b)) (b, o)                  // keep s, r
          else {
            stats.nNodesSuboptimal += 1
            (b, None)                                       // update r
          })
      }

    def applyHeuristics_v0(o: Option[Node]): BBState =
      o.fold(doneState) { n =>
        val (refined, proposed) = heuristics.bind(n)
        State(b => {
          if (refined ne n)
            stats.nNodesRefined += 1
          val best = proposed.fold(b) { s =>
            stats.nNodesProposed += 1
            reduceSolution(s, b)
          }
          (best, Some(refined))                             // update s, r
        })
      }

    /*
     * B&B Operations as Monad Functions #2: Uniform Access
     *
     * Allways unpacks, processes, and repacks `Option` and `BBState`.
     *
     * Readibility better but still poor:
     *
     * + uniform access from within lambda: (b0, n0) => (b1, n1)
     *
     * - boilerplate wrapper: `BBState { b => o.fold((b, o)) { n => ... } }`
     *
     * - could factor out boilerplate into helper method ("lift") or type
     *
     * - deferred execution: all computation when applying to initial state
     *
     * ==> But why then unpack separate state vs result and repack again?
     */

    def checkIsInfeasible(o: Option[Node]): BBState =
      BBState { b =>
        o.fold((b, o)) { n =>
          if (!n.isInfeasible) (b, Some(n))
          else {
            stats.nNodesInfeasible += 1
            (b, None)                                       // update r
          }
        }
      }

    def checkIsFathomed(o: Option[Node]): BBState =
      BBState { b =>
        o.fold((b, o)) { n =>
          if (!n.isFathomed) (b, Some(n))
          else {
            stats.nNodesFathomed += 1
            (reduceSolution(n.closed, b), None)             // update s, r
          }
        }
      }

    def checkIsSuboptimal(o: Option[Node]): BBState =
      BBState { b =>
        o.fold((b, o)) { n =>
          if (!n.isSuboptimalBy(b)) (b, Some(n))
          else {
            stats.nNodesSuboptimal += 1
            (b, None)                                       // update r
          }
        }
      }

    def applyHeuristics(o: Option[Node]): BBState =
      BBState { b =>
        o.fold((b, o)) { n =>
          val (refined, proposed) = heuristics.bind(n)
          if (refined ne n)
            stats.nNodesRefined += 1
          val best = proposed.fold(b) { s =>
            stats.nNodesProposed += 1
            reduceSolution(s, b)
          }
          (best, Some(refined))                             // update s, r
        }
      }

    /*
     * Composed Node Fathoming
     */

    val fathom_v0: Node => BBState = node =>
      for {
        o0 <- BBState.pure(Some(node))          // state passed implicitly
        o1 <- checkIsInfeasible(o0)            // result passed explicitly
        o2 <- checkIsFathomed(o1)
        o3 <- checkIsSuboptimal(o2)
        o4 <- applyHeuristics(o3)
        o5 <- checkIsFathomed(o4)
        o6 <- checkIsSuboptimal(o5)
      } yield o6

    val fathom_v1: Node => BBState = node =>  // desugared
      BBState.pure(Some(node))
        .flatMap(checkIsInfeasible(_)          // `(_)` required here
          .flatMap(checkIsFathomed(_)          // so, result still explicit
            .flatMap(checkIsSuboptimal(_)
              .flatMap(applyHeuristics(_)
                .flatMap(checkIsFathomed(_)
                  .flatMap(checkIsSuboptimal(_)
                    .map(o6 => o6)))))))

    val fathom_v2: Node => BBState = node =>  // per monad associativity
      BBState.pure(Some(node))
        .flatMap(checkIsInfeasible)            // `(_)` can be inferred
        .flatMap(checkIsFathomed)              // result & state implicit
        .flatMap(checkIsSuboptimal)
        .flatMap(applyHeuristics)
        .flatMap(checkIsFathomed)
        .flatMap(checkIsSuboptimal)

    /*
     * Branching & Recursion separate from node fathoming operations
     */

    /** Returns the branched child nodes of a node. */
    def branchedNodes(node: Option[Node]): Seq[Node] = {
      node.fold(Seq.empty[Node]) { n =>
        val nodes = branchNode(n)
        stats.nNodesBranched += nodes.size
        nodes
      }
    }

    /** Solves a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be solved
      * @param best a start solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // not: @annotation.tailrec
    def go(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      // select code version
      val fathomNodeSM: Node => BBState = fathom_v2  // _v0..2

      // apply the transformation to the initial node and state
      val (newBest, newNodeOpt) = fathomNodeSM(node).run(best).value

      // branch + recurse
      branchedNodes(newNodeOpt).foldLeft(newBest)((b, n) => go(n, b))

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    val start = Node.start
    val best = go(start, start.closed)

    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v06_state_monad_dfs_factory
  (heuristicsFactory: BBProblem => BBHeuristics)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v06_state_monad_dfs(
      heuristicsFactory)(new BBProblem(problem))
}
