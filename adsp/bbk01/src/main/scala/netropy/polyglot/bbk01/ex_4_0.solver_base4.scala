package netropy.polyglot.bbk01


// TODO: hopelessly outdated code

/** Base class with common methods for B&B Solver implementations.
  *
  * This class encapsulates two algorithmic parameters of Branch & Bound:
  * - `valueUpperBound()`: a dual heuristic estimating a node's max value
  * - `selectBranchVariable()`: on which index to branch a node
  *
  * A technical helper function common to all subclasses:
  * - `branchNode()`: creates a node pair using `selectBranchVariable()`
  *
  * Note that this base class does not declare methods that control the order
  * in which nodes are traversed (which is an algorithmic parameter of B&B).
  * Node selection is solely defined by subclasses, as they also provide the
  * collection/container (or callstack) for holding and accessing nodes.
  *
  * @param problem the input problem instance, not null
  */
abstract class BBSolver4(implicit problem: BBProblem)
    extends BBSolver(BBHeuristics.none) {

  // TODO:  delete
  /** Estimates a node's maximum feasible value (dual heuristic). */
  def valueUpperBound(node: Node): Long = {
  //override def valueUpperBound(node: Node): Double = {
    assert(node.weight <= problem.maxWeight)
    if (node.unset.isEmpty) return node.value  // solved

    val maxWeight = problem.maxWeight - node.weight
    var i = 0
    var j = -1
    var v: Long = 0
    var w: Long = 0
    while (i < problem.indicesByValueDensity.size && w <= maxWeight) {
      j = problem.indicesByValueDensity(i)
      i += 1
      if (node.unset.contains(j)) {
        v += problem.values(j)
        w += problem.weights(j)
      }
    }
    val wj = problem.weights(j)
    val vj = problem.values(j)

    //val r = node.value + v + ((maxWeight - w) / wj.toDouble) * vj
    val r0 = math.ceil(((maxWeight - w) / wj.toDouble) * vj).toLong
    val r = node.value + v + r0
    //if (node.packed == collection.immutable.BitSet(1, 3))
    //  assert(false)
    r

    // ineffective
    // the most simple estimate is to include all remaining variables
    //node.value + node.unset.toIndexedSeq.map(problem.values(_)).sum
    //
    // a better, yet still simple estimate is to eliminate single variables
    // whose inclusion would make this node infeasible
    //val unset = node.unset.filter(
    //  node.weight + problem.weights(_) <= problem.maxWeight)
    //node.value + unset.toIndexedSeq.map(problem.values(_)).sum
    //Long.MaxValue  // ineffective
    //node.value  // incorrect
    //Double.MaxValue  // ineffective
  }
}
