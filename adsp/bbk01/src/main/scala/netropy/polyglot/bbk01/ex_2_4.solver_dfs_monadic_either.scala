package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13
import util.{Either, Left, Right}


/** A recursive, DFS B&B solver based on monadic `Either`.
  *
  * Idea: Utilize [[util.Either]] to short-circuit the fathoming of a node
  *       -> model the processing state as a union of types
  *       -> compose fathoming as series of (`flatMap`) transformations
  *
  * Refresher:
  *       Type `Either[+A, +B]` forms a Tagged Union over types `A` and `B`
  *       as instances of subtypes `Left[A, Nothing]`, `Right[Nothing, B]`.
  *
  *       Right-Biased since Scala 2.12: `Either` itself became monadic by
  *       delegating to the `RightProjection`.  Mapping over an `Either` is:
  *       - a no-op if it actually holds an `A`  (i.e., is a `Left`)
  *       - effective if it actually holds a `B` (i.e., is a `Right`).
  *
  * Sketch:
  * ```
  *     // a union type whose instances represent
  *     // - the "done" state:         Left(best)
  *     // - the "incomplete" state:   Right((node, best))
  *     type BBState = Either[Node, (Node, Node)]
  *
  *     def checkIsInfeasible(r: (Node, Node)): BBState =
  *       if (r...) Right(r)                     // r needs more work
  *       else { ... Left(best) }                // pruned, r is done
  *     ...
  *
  *     val Left(best1) = for {
  *       r0   <- Right((node, best))            // initial values in a Right
  *       r1   <- checkIsInfeasible(r0)          // may inject a Left
  *       ...
  *       rm   <- applyHeuristics(rm-1)          // skipped on a Left
  *       rm+1 <- checkIsFathomed(rm)            // ...
  *       ...
  *       rn   <- Left(branchedNodes(rn-1), foldLeft and recurse)
  *     } yield rn
  * ```
  *
  * Notes: .../ex_2_4.r.solver_dfs_monadic_either.md
  *
  * Summary:
  *
  * - Only depth-first-search so far, function go() not tail-recursive
  *
  * + Improved readability:
  *   + no side-effect on mutable state
  *   + implicit control flow, expressed as data (return `Left`, `Right`)
  *   + integrates the recursion on child nodes as final action
  *   + go_v2(): readable and robust flat `.flatMap` chain
  *   ~ 19 lines of code for recursive function `go` (up from ~17)
  *   ~ 10 lines of code for fathom+branch flow (same as before ~10)
  *
  * - Code's readability not ideal
  *   - obscure type: `Either[Node, (Node, Node)]`
  *   - this usage of `Either` might seem confusing to some developers...
  *
  * - Use or Abuse of `Either` for control flow purposes?
  *   - `Left` is rather associated with "failure", not with a "done" state
  *   - relies upon right-biased `Either`, Scala <2.12 requires boilerplate
  *   - language experts' unhappiness with `Either`:
  *     Right-Bias since 2.12: confusing, ill-motivated ("Haskellism")
  *         Martin Odersky (2019-07-01):
  *         "We unfortunately got ourselves into a bad mess with Either."
  *         "The original sin for Either was right biasing it."
  *         "Before, Either could indeed be regarded as just a tagged union,
  *     -->  and any code that used it otherwise was dubious."  <--
  *   - still valid use cases: cascade of conditional operations? (see notes)
  *     => have support for both, biased and balanced, tagged union types?
  *
  * - How to improve data type modeling?
  *   - duplication, `best` in both `Left(best)` <-> `Right(node, best)`
  *   - factor out for product type: `(best: Node, node: Option[Node])`?
  *   - product types by themselves not monadic
  *
  * ==> Try use the `StateMonad` for passing `(Node, Option[Node])`
  *
  * @see https://contributors.scala-lang.org/t/pre-sip-proposal-of-introducing-a-rust-like-type-result/3497/31
  */
class BBSolver_v05_monadic_either_dfs
  (heuristicsFactory: BBProblem => BBHeuristics)
  (implicit problem: BBProblem)
    extends BBSolver(heuristicsFactory) {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated by side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    // Types modeling the B&B processsing states

    case class Unfathomed(node: Node, best: Node)  // the "incomplete" state
    type Optimum = Node                            // the "done" state
    type BBState = Either[Optimum, Unfathomed]

    // Refactored monadic tests and transformations on nodes

    def checkIsInfeasible(r: Unfathomed): BBState =
      if (!r.node.isInfeasible) Right(r)
      else {
        stats.nNodesInfeasible += 1
        Left(r.best)
      }

    def checkIsFathomed(r: Unfathomed): BBState =
      if (!r.node.isFathomed) Right(r)
      else {
        stats.nNodesFathomed += 1
        Left(reduceSolution(r.node.closed, r.best))  // not commutative
      }

    def checkIsSuboptimal(r: Unfathomed): BBState =
      if (!r.node.isSuboptimalBy(r.best)) Right(r)
      else {
        stats.nNodesSuboptimal += 1
        Left(r.best)
      }

    def applyHeuristics(r: Unfathomed): Right[Nothing, Unfathomed] = {
      val (refined, proposed) = heuristics.bind(r.node)
      if (refined ne r.node)
        stats.nNodesRefined += 1
      val best = proposed.fold(r.best) { s =>
        stats.nNodesProposed += 1
        reduceSolution(s, r.best)  // not commutative
      }
      Right(Unfathomed(refined, best))
    }

    /** Returns the branched child nodes of a node. */
    def branchedNodes(r: Unfathomed): Seq[Node] = {
      val nodes = branchNode(r.node)
      stats.nNodesBranched += nodes.size
      nodes
    }

    /** Returns given node if its value exceeds `best`, otherwise `best`.
      *
      * Note: reduceSolution(s, o) is not commutative.
      * Invokes the callback `onSolution(s)` when `s` exceeds `best`.
      */
    def reduceSolution(s: Node, best: Node): Node = {

      //require(s.assertIsSolution)  // already checked by go(), bind()
      //require(best.assertIsSolution)  // already checked by go(), bind()

      if (!s.isBetterThan(best)) best
      else {
        stats.nNodesBest += 1
        onSolution(s) // notify caller, side-effect
        s
      }
    }

    /** Solves a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be solved
      * @param best a start solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // select code version
    def go(node: Node, best: Node): Node = go_v0(node, best)  // _v0..2

    /** B&B-chain as `for`-comprehension. */
    // not: @annotation.tailrec
    def go_v0(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      val Left(best1) = (for {  // pattern-match
        r0 <- Right(Unfathomed(node, best))
        r1 <- checkIsInfeasible(r0)
        r2 <- checkIsFathomed(r1)
        r3 <- checkIsSuboptimal(r2)
        r4 <- applyHeuristics(r3)
        r5 <- checkIsFathomed(r4)
        r6 <- checkIsSuboptimal(r5)
        r7 <- Left(branchedNodes(r6).foldLeft(r6.best)((b, n) => go_v0(n, b)))
      } yield r7) : @unchecked  // always a Left (throw MatchError otherwise)

      best1

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    /** B&B-chain as desugared, nested `flatMap` operations. */
    // not: @annotation.tailrec
    def go_v1(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      // desugared version of go_v0()
      // dropped the innermost identity: .map(r7 => r7)
      // as example, captures result as Either, extracts value via Option
      val bestE: BBState =
        Right(Unfathomed(node, best))
          .flatMap(checkIsInfeasible(_)
            .flatMap(checkIsFathomed(_)
              .flatMap(checkIsSuboptimal(_)
                .flatMap(applyHeuristics(_)
                  .flatMap(checkIsFathomed(_)
                    .flatMap(checkIsSuboptimal(_)
                      .flatMap(r =>
                        Left(
                          branchedNodes(r)
                            .foldLeft(r.best)((b, n) => go_v1(n, b))))))))))

      bestE.swap.toOption.get  // no need for getOrElse(best) since a Left

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    /** B&B-chain as sequence of `flatMap` operations. */
    // not: @annotation.tailrec
    def go_v2(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      // flattened version of go_v1(), per monad associativity, identity laws
      val Left(best1) =
        Right(Unfathomed(node, best))
          .flatMap(checkIsInfeasible(_))
          .flatMap(checkIsFathomed(_))
          .flatMap(checkIsSuboptimal(_))
          .flatMap(applyHeuristics(_))
          .flatMap(checkIsFathomed(_))
          .flatMap(checkIsSuboptimal(_))
          .flatMap(r =>
            Left(branchedNodes(r).foldLeft(r.best)((b, n) => go_v1(n, b))))
            : @unchecked  // always a Left (throw MatchError otherwise)

      best1

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    val start = Node.start
    val best = go(start, start.closed)
    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
class BBSolver_v05_monadic_either_dfs_factory
  (heuristicsFactory: BBProblem => BBHeuristics)
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BBSolver_v05_monadic_either_dfs(
      heuristicsFactory)(new BBProblem(problem))
}
