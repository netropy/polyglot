package netropy.polyglot.bbk01

import collection.immutable.Seq  // default since Scala 2.13
import collection.mutable.{Map, LinkedHashMap}  // keeps insertion order

import java.text.NumberFormat


/** Base class for computational summary statistics.
  *
  * Features:
  * - for clients: methods for generic `toMap` conversion and pretty printing
  * - for solvers: methods `start/stop` with local variables for runtime data
  * - fields for reporting static JVM and execution environment information
  *
  * Subclasses may want to add counters for solver-specific statistics.
  *
  * Notes:
  *
  * `Statistics` not a `case class`:
  * - no need for field extraction, pattern matching, value equality
  * - subclasses want to add fields for solver-specific statistics
  * - serializability can be added externally or by subclasses
  *
  * @param nVariables #indices in the problem instance
  */
class Statistics(val nVariables: Long = 0) {

  // execution data (modified by start()/stop())
  var timeRealMs: Long = 0
  var jvmCompilationTimeMs: Long = 0
  var jvmGcCount: collection.mutable.Seq[Long] = collection.mutable.Seq()
  var jvmGcTimeMs: collection.mutable.Seq[Long] = collection.mutable.Seq()

  // static execution environment
  val jvmCpuCount: Int = Statistics.rt.availableProcessors
  val jvmMaxMemory: Long = Statistics.rt.maxMemory
  val jvmName: String = System.getProperty("java.vm.name")
  val jvmVersion: String = System.getProperty("java.vm.version")

  override def toString: String = mkString(", ")

  def mkString: String =
    mkString("Statistics(\n    ", "\n    ", "\n)")

  def mkString(sep: String): String =
    mkString("Statistics(", sep, ")")

  def mkString(start: String, sep: String, end: String): String = {
    import Statistics.formatter  // why not finding in companion?
    toMap.map {
      // overloaded NumberFormat.format
      case (k, v: Int) => s"$k -> ${formatter.format(v)}"
      case (k, v: Long) => s"$k -> ${formatter.format(v)}"
      case (k, v: Double) => s"$k -> ${formatter.format(v)}"
      case (k, v) => s"$k -> $v"
    }.mkString(start, sep, end)
  }

  def toMap: Map[String, Any] = {
    LinkedHashMap(
      "nVariables" -> nVariables,
      "timeRealS" -> (timeRealMs / 1000.0),
    )
  }

  def jvmExecutionMap: Map[String, Any] = {
    val m: Map[String, Any] = LinkedHashMap(
      "jvmCompilationTimeS" -> (jvmCompilationTimeMs / 1000.0),
    )
    m ++= (0 until jvmGcCount.length)
      .map(i => s"jvmGcCount_$i" -> jvmGcCount(i))
    m ++= (0 until jvmGcTimeMs.length)
      .map(i => s"jvmGcTimeS_$i" -> (jvmGcTimeMs(i) / 1000.0))
    m ++= Seq(
      "jvmGcCount_sum" -> jvmGcCount.sum,
      "jvmGcTimeS_sum" -> (jvmGcTimeMs.sum / 1000.0))
  }

  def jvmEnvironmentMap: Map[String, Any] = {
    LinkedHashMap(
      "jvmName" -> jvmName,
      "jvmVersion" -> jvmVersion,
      "jvmCpuCount" -> jvmCpuCount,
      "jvmMaxMemoryMiB" -> (jvmMaxMemory / (1 << 20)),
    )
  }
}

/** Helper methods for recording runtime statistics from the JVM. */
trait StatisticsHelper {

  import java.lang.Runtime
  import java.lang.management.{ManagementFactory, GarbageCollectorMXBean}


  import scala.jdk.CollectionConverters._  // Scala 2.13
  //import scala.collection.JavaConverters._  // Scala 2.12

  val gcs: Seq[GarbageCollectorMXBean] =
    ManagementFactory.getGarbageCollectorMXBeans().asScala.toSeq

  val rt = Runtime.getRuntime()

  val jvmCompMBean = ManagementFactory.getCompilationMXBean()  // -> ms

  /** Initializes `stats` with runtime statistics before a computation. */
  def start(stats: Statistics): Unit = {

    for (i <- 0 until 3) { rt.gc() }  // suggest run GC for clean slate

    stats.jvmGcTimeMs = collection.mutable.Seq.fill(gcs.length)(0)
    for (i <- 0 until gcs.length)
      stats.jvmGcTimeMs(i) = -gcs(i).getCollectionTime()

    stats.jvmGcCount = collection.mutable.Seq.fill(gcs.length)(0)
    for (i <- 0 until gcs.length)
      stats.jvmGcCount(i) = -gcs(i).getCollectionCount()

    stats.jvmCompilationTimeMs = -jvmCompMBean.getTotalCompilationTime()

    stats.timeRealMs = -System.currentTimeMillis()
  }

  /** Updates `stats` with runtime statistics after a computation. */
  def stop(stats: Statistics): Unit = {

    for (i <- 0 until 3) { rt.gc() }  // suggest run GC for collection cost

    for (i <- 0 until gcs.length)
      stats.jvmGcTimeMs(i) += gcs(i).getCollectionTime()

    for (i <- 0 until gcs.length)
      stats.jvmGcCount(i) += gcs(i).getCollectionCount()

    stats.jvmCompilationTimeMs += jvmCompMBean.getTotalCompilationTime()

    stats.timeRealMs += System.currentTimeMillis()
  }
}

/** Helper methods for recording runtime statistics from the JVM. */
object Statistics extends StatisticsHelper {

  val formatter = NumberFormat.getInstance
}
