package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** Cheap & simple LP bound with primal Greedy heuristic for 0/1 Knapsack.
  *
  * This heuristic: +updates upper bound, -fixes variables, +proposes solution
  *
  * Refresher:
  *
  * - The LP relaxation, also called _Continuous_ or _Fractional Knapsack_
  *   Problem, drops the boolean (integrality) constraint of `0` or `1`,
  *   which allows "to break" items for maximizing the total value.
  *
  * - This relaxation is also known as the "Dantzig" bound, as it may be
  *   be solved by a _Greedy Algorithm_ (published 1957 by George Dantzig).
  *
  * - It is cheap to compute: O(n) after sorting of the indices.
  *
  * - It is easy to code:
  *   1) sort items in decreasing order of value density (value/weight)
  *   2) starting at highest density, take items while not exceeding capacity
  *   3) fill up with the fraction of the item that did not fit the container
  *
  * - As added benefit: Steps 1) and 2) form the Greedy algorithm, which
  *   produces a feasible solution to the 0/1 Knapsack problem.
  *
  * - See [[https://en.wikipedia.org/wiki/Continuous_knapsack_problem]]
  *
  * Notes:
  *
  * - The Greedy algorithm can be viewed as a reduction (or fold) operation
  *   over the sorted, unset variables in a 0/1 Knapsack Problem instance.
  *   Ideally, this reduction is _lazy_ (it stops once a threshold is met).
  *
  * - Two code versions of a lazy reduction are given below:
  *
  *   - functional `greedy_v0`:
  *     uses the non-strict `takeWhile` on a lazy, filtered collection
  *     [[https://docs.scala-lang.org/tutorials/FAQ/stream-view-iterator.html]]
  *
  *   - imperative `greedy_v1`:
  *     uses a `return` for early exit from a `while` loop
  *
  * @param problem the (enriched) input problem instance, not null
  */
class BBKnapsackHeuristicGreedyLpBound
  (implicit problem: BBProblem)
    extends BBHeuristics {

  /** Computes the LP Bound and Greedy Solution for 0/1 Knapsack node. */
  def bind1(node: Node): (Node, Option[Node]) = {

    assert(node.weight < problem.maxWeight)  // not implied by !isFathomed
    assert(!node.unset.isEmpty)

    val (gn, gv, gw) = greedy(node)  // (node, excess value, excess weight)
    gn.assertIsSolution(problem)
    assert(gv != 0, gw != 0)

    // ceil() for conservative relaxation
    val resWeight = problem.maxWeight - gn.weight
    val lpBound =
      if (resWeight == 0) gn.value
      else gn.value + math.ceil(gv * resWeight.toDouble / gw).toLong

    (node.refined(lpBound), Some(gn))
  }

  /** Returns a feasible node and the value, weight of 1st excluded index. */
  def greedy(node: Node): (Node, Long, Long) =
    // select code version
    greedy_v0(node)  // v0..1

  def greedy_v0(node: Node): (Node, Long, Long) = {

    //assert(node.weight < problem.maxWeight)  // enforced by caller
    //assert(!node.unset.isEmpty)

    // lazy reduction using non-strict `takeWhile`
    var (n, v, w) = (node, 0L, 0L)
    problem.indicesByValueDensity
      .iterator  // lazy, slightly faster
      .filter(node.unset.contains)
      .takeWhile { i =>
        v = problem.values(i)
        w = problem.weights(i)
        if (n.weight + w <= problem.maxWeight) {
          n = n.included(i)
          true
        } else {
          false
        }
      }
      .size  // forces iterator, may return 0

    //assert(v != 0, w != 0)
    (n.closed, v, w)
  }

  def greedy_v1(node: Node): (Node, Long, Long) = {

    //assert(node.weight < problem.maxWeight)  // enforced by caller
    //assert(!node.unset.isEmpty)

    // lazy reduction using an early `return` from `while`
    var i = 0
    var (n, v, w) = (node, 0L, 0L)
    var weight = 0L
    val maxWeight = problem.maxWeight - node.weight
    while (i < problem.indicesByValueDensity.size) {
      val j = problem.indicesByValueDensity(i)
      i += 1
      if (n.unset.contains(j)) {
        v = problem.values(j)
        w = problem.weights(j)
        weight += w
        if (weight > maxWeight) {
          //assert(v != 0, w != 0)
          return (n.closed, v, w)
        }
        n = n.included(j)
      }
    }

    //assert(v != 0, w != 0)
    (n.closed, v, w)
  }
}
