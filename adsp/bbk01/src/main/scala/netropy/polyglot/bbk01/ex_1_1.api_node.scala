package netropy.polyglot.bbk01

import math.Ordering

import collection.immutable.{Seq, IndexedSeq, BitSet, List}
import collection.mutable.{Map, LinkedHashMap}  // keeps insertion order


/** A node in the B&B search tree representing a partial result/problem.
  *
  * This node representation is specific to the 0/1 Knapsack Problem.
  *
  * A node encodes in two status [[BitSet]]s which of the indices (variables)
  * - have been fixed to `0` or `1` (`packed` bitset) and
  * - have not been fixed yet (`unset` bitset).
  *
  * Given a 0/1 Knapsack `Problem` instance, a `Node` represents both
  * - a partial solution to it (`unset` where `0`, status in `packed`) and
  * - a sub-problem instance of it (variables where `unset` is `1`).
  *
  * Leaf nodes are said to be _fathomed_ in that all their indices are (or
  * could be) fixed to 0/1.  A leaf node is a solution to the problem instance
  * if the sum `weight` of its packed indices does not exceed the problem's
  * `maxWeight` constraint; otherwise, the node is said to be _infeasible_.
  *
  * Nodes can be _prunded_ or discarded as _suboptimal_ if their `maxValue`,
  * an upper bound on the node's best solution computed by a dual heuristic,
  * does not exceed a known solution or lower bound to the problem instance.
  *
  * Available methods and values on `Node`:
  * - factory methods (companion)
  * - `Ordering` values to compare nodes by their fields (companion)
  * - tests whether a node is _fathomed_, _infeasible_, or _suboptimal_
  * - "tigthening" operations returning a more constrained node
  * - branching operation creating new nodes with in-/excluded indices
  * - assertion methods for consistency checking
  * - utility methods for pretty printing
  *
  * Notes: .../ex_1_1.r.api_node.md
  *
  * Summary:
  *
  * - a node in the B&B search tree: partial result & sub-problem instance
  * - immutable, value-comparable, hashable, serializable, matchable
  * - space- & time-efficient: `BitSet` store/test/lookup, cached sums
  * - no stored reference to the immutable `Problem`: taken as `implicit`
  * - methods for creating, comparing, branching, and -> _testing_ of nodes
  *
  * @param unset the indices _not yet_ fixed to `0` or `1`
  * @param packed the indices _already_ fixed to `0` or `1`
  * @param value the sum of all packed `value`s
  * @param weight the sum of all packed `weight`s
  * @param maxValue an upper bound on the best `value`
  */
case class Node private (
  unset: BitSet,
  packed: BitSet,
  value: Long,
  weight: Long,
  maxValue: Long) {

  require(unset != null)
  require(packed != null)
  require(value >= 0L)
  require(weight >= 0L)
  require(maxValue >= value)
  require(maxValue != value ^ unset.isEmpty)  // by refined(), included()
  // allowed, requires problem: isInfeasible, isFathomed, !assertIsConsistent

  /*
   * Testing Nodes
   *
   * Note: preferring plain-english method names
   * - weight > problem.maxWeight
   *   => isInfeasible, isInvalid
   * - value == maxValue || unset.isEmpty || weight == problem.maxWeight
   *   => isFathomed, isProcessed, isComplete
   * - value > (bound):
   *   => isBetterThan, exceeds
   * - maxValue <= (lowerBound):
   *   => isSuboptimalBy, isSuboptimalAsTo, cannotExceed
   */

  /** Whether this node's weight exceeds the problem's capacity. */
  final def isInfeasible
      (implicit problem: Problem): Boolean =
    weight > problem.maxWeight

  /** Whether this node is complete or cannot be further improved. */
  final def isFathomed
      (implicit problem: Problem): Boolean = {

    assert(value != maxValue || unset.isEmpty)  // no need to require
    assert(weight < problem.maxWeight || unset.isEmpty)
    unset.isEmpty
  }

  /** Whether this node is a solution to the problem instance. */
  final def isSolution
      (implicit problem: Problem): Boolean =
    isFathomed && !isInfeasible

  /** Whether this solution's value exceeds the given solution. */
  final def isBetterThan(other: Node)
      (implicit problem: Problem): Boolean = {

    assert(isSolution)  // no need to require
    assert(other.isSolution)
    this.value > other.value
  }

  /** Whether this node cannot possibly exceed the given solution. */
  final def isSuboptimalBy(other: Node)
      (implicit problem: Problem): Boolean = {

    assert(other.isSolution)  // no need to require
    this.maxValue <= other.value
  }

  /*
   * Tigthening Nodes (idempotent, cheap to compute)
   */

  /** Returns this node or a clone having all unset indices excluded. */
  final def closed: Node =
    if (unset.isEmpty) this
    else Node(BitSet.empty, packed, value, weight, value)  // sets maxValue

  /** Returns this node or a clone with the given upper bound if tighter. */
  final def refined(newMaxValue: Long): Node = {

    require(newMaxValue >= value)

    if (newMaxValue == value) this.closed  // fathomed
    else if (newMaxValue >= maxValue) this  // not improving
    else Node(unset, packed, value, weight, newMaxValue)
  }

  /** Returns this node or a clone with given `unset` if a proper subset. */
  final def refined(newUnset: BitSet): Node = {

    require(newUnset.subsetOf(unset))

    if (newUnset.isEmpty) this.closed  // fathomed
    else if (newUnset.size == unset.size) this  // not improving
    else Node(newUnset, packed, value, weight, maxValue)
  }

  /*
   * Branching Nodes (cheap to compute)
   */

  /** Returns a clone with the given, unset index excluded. */
  final def excluded(i: Int)(implicit problem: Problem): Node = {

    require(0 <= i && i < problem.size)
    require(unset.contains(i))

    refined(unset - i)
  }

  /** Returns a clone with the given, unset index included. */
  final def included(i: Int)(implicit problem: Problem): Node = {

    require(0 <= i && i < problem.size)
    require(unset.contains(i))
    // allowed: weight + problem.weights(i) > problem.maxWeight

    // minor code duplication with isFathomed, closed
    val (u, p) = (unset - i, packed + i)
    val (v, w) = (value + problem.values(i), weight + problem.weights(i))
    if (u.isEmpty || w >= problem.maxWeight || v == maxValue)
      Node(BitSet.empty, p, v, w, v)  // sets maxValue
    else
      Node(u, p, v, w, maxValue)
  }

  /** Branches this node into nodes with included/excluded index.
    *
    * Note: not dropping infeasible nodes, not refining feasible nodes.
    * Algorithmic decisions, left to caller how to handle those nodes.
    */
  final def branch(i: Int)(implicit problem: Problem): Seq[Node] = {

    require(unset.contains(i))

    List(included(i), excluded(i))
  }

  /*
   * Checking Nodes for Consistency (expensive, require/ensure at boundaries)
   */

  /** Ensures this result is consistent under given problem instance. */
  def assertIsConsistent(implicit problem: Problem): Boolean = {
    assert((packed & unset) == BitSet.empty)
    assert(unset.size <= problem.size)
    assert(packed.size <= problem.size)
    assert(value == packed.toIndexedSeq.map(problem.values(_)).sum)
    assert(weight == packed.toIndexedSeq.map(problem.weights(_)).sum)
    assert(weight < problem.maxWeight || unset.isEmpty)
    // already ensured by constructor
    //assert(maxValue >= value)
    //assert(maxValue != value ^ unset.isEmpty)
    true
  }

  /** Ensures this result is feasible under given problem instance. */
  def assertIsSolution(implicit problem: Problem): Boolean = {
    assertIsConsistent
    assert(isSolution)
    true
  }

  /** Ensures this result is allowed by the given (partial) result. */
  def assertIsRefinedOf(node: Node)(implicit problem: Problem): Boolean = {
    assertIsConsistent
    assert(unset.subsetOf(node.unset))
    assert(node.packed.subsetOf(packed))
    assert(value >= node.value)
    assert(maxValue <= node.maxValue)
    true
  }

  // for debugging
  def isAllowedBy(node: Node)(implicit problem: Problem): Boolean =
    (true
      && (node.unset ++ node.packed).subsetOf(unset ++ packed)  // possible
      && packed.subsetOf(node.packed)  // actual
      && value <= node.value
      && weight <= node.weight
      && maxValue >= node.maxValue)

  /*
   * Pretty-Printing Nodes
   */

  def toMap: Map[String, Any] = LinkedHashMap(
    "unset" -> unset,
    "packed" -> packed,
    "value" -> value,
    "weight" -> weight,
    "maxValue" -> maxValue,
  )

  def mkString: String =
    mkString("Node(\n    ", "\n    ", "\n)")

  def mkString(sep: String): String =
    mkString("Node(", sep, ")")

  def mkString(start: String, sep: String, end: String): String =
    toMap.mkString(start, sep, end)
}

/** Convenience constructors and utilities. */
object Node {

  /*
   * Creating Nodes
   */

  /** Creates a (partial) result with given values w/o a problem instance. */
  def apply(
    unset: BitSet,
    packed: BitSet,
    value: Long,
    weight: Long,
    maxValue: Long): Node =
    new Node(unset, packed, value, weight, maxValue)

  /** Creates and empty result for a problem of `size` unfixed variables. */
  def empty: Node =
    Node(
      BitSet.empty, BitSet.empty,
      0L, 0L, 0L)  // there may be use cases for maxValue=Long.MaxValue

  /** Creates and empty result for a problem of `size` unfixed variables. */
  def start(implicit problem: Problem): Node =
    Node(
      BitSet.empty ++ (0 until problem.size), BitSet.empty,
      0L, 0L, problem.values.sum)

  /*
   * Comparing Nodes
   */

  /** A ordering on nodes by their `value`. */
  val cmpByValue: Ordering[Node] =
    Ordering.by((n: Node) => n.value)(Ordering.Long)

  /** A ordering on nodes by their `maxValue`. */
  val cmpByMaxValue: Ordering[Node] =
    Ordering.by((n: Node) => n.maxValue)(Ordering.Long)

  /** A ordering on nodes by their number of `unset` variables. */
  val cmpByUnsetSize: Ordering[Node] =
    Ordering.by((n: Node) => n.unset.size)(Ordering.Int)
}
