package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13
import math.Ordering  // redundant


/** An enriched instance of the 0/1 Knapsack Problem.
  *
  * Adds precomputed information for use by algorithmic components, such as
  * metrics and orderings on the indices (problem variables).
  *
  * Note:
  *
  * Extending `case class` is OK here, no change to `equals`/`hashCode`.
  * Added fields are computable from the superclass, hence, redundant.
  *
  * @param problem the input problem instance, not null
  */
class BBProblem(val problem: Problem)
    extends Problem(problem.values, problem.weights, problem.maxWeight) {

  // some useful metrics on the problem indices
  val byValueMetric =
    Ordering.by((i: Int) => -values(i))
  val byWeightMetric =
    Ordering.by((i: Int) => -weights(i))
  val byValueDensityMetric =
    Ordering.by((i: Int) => -values(i).toDouble / weights(i).toDouble)

  // sorted indexed sequences under above metrics
  val indicesByValue =
    (0 until size).sorted(byValueMetric).toVector
  val indicesByWeight =
    (0 until size).sorted(byWeightMetric).toVector
  val indicesByValueDensity =
    (0 until size).sorted(byValueDensityMetric).toVector
}
