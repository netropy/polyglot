package netropy.polyglot.bbk01

import collection.immutable.{Seq}  // default since Scala 2.13


/** A recursive, Backtracking solver.
  *
  * Idea: Recursive function, tests with "early outs", branching last resort
  *
  * Sketch:
  * ```
  *     if (node.isInfeasible) { ... return best }
  *
  *     if (node.isFathomed) { ... return best|newBest }
  *
  *     val nodes = branchNode(node)
  *     ...
  *     recurse
  * ```
  *
  * Notes: .../ex_1_2.r.solver_dfs_recursive.md
  *
  * Summary:
  *
  * + Implemented Backtracking: testing, pruning, branching, recursion
  * - no B&B, no heuristics, no pruning of suboptimal nodes
  * - only depth-first-search so far, function `go` not tail-recursive
  * - explicit control flow, nested tests with early `if-return`s
  * - poor readability, some code clutter:
  *   ~35 lines of code for recursive function `go`
  *
  * ==> Extend to B&B, add heuristics for upper/lower bounds
  */
class BTSolver_v00_recursive_dfs
  (implicit problem: BBProblem)
    extends BTSolver {

  def solve(onSolution: Node => Unit): BBStatistics = {

    val size = problem.values.size
    val stats = new BBStatistics(nVariables=size)  // updated per side-effect
    stats.nNodesBranched = 1
    BBStatistics.start(stats)

    /** Fathoms a node by recursive depth-first-search returning an optimum.
      *
      * @param node the subproblem instance to be fathomed
      * @param best a solution serving as (lower) bound
      * @return an optimum solution for the subproblem instance
      */
    // not: @annotation.tailrec
    def go(node: Node, best: Node): Node = {

      require(node.assertIsConsistent)
      require(best.assertIsSolution)

      if (node.isInfeasible) {
        stats.nNodesInfeasible += 1
        return best
      }

      // Note: code clutter
      if (node.isFathomed) {
        stats.nNodesFathomed += 1

        if (!node.isBetterThan(best))
          return best

        val newBest = node.closed
        stats.nNodesBest += 1
        onSolution(newBest)  // notify caller, side-effect
        return newBest
      }

      // incomplete, branch
      val nodes = branchNode(node)
      stats.nNodesBranched += nodes.size

      // recurse (illustrated for |nodes| = 0..2, n)
      nodes match {
        case Seq() => best
        case Seq(n0) => go(n0, best)
        case Seq(n1, n0) => go(n0, go(n1, best))
        case nodes => nodes.foldLeft(best)((b, n) => go(n, b))  // general
      }

    } ensuring { r =>
      if (r ne best) {
        r.assertIsSolution
        assert(r.value >= best.value)
      }
      true
    }

    val start = Node.start
    val best = go(start, start.closed)
    BBStatistics.stop(stats)
    stats
  }
}


/** Creates and initializes the solver to a problem instance. */
object BTSolver_v00_recursive_dfs_factory
    extends SolverFactory {

  def create(problem: Problem): Solver =
    new BTSolver_v00_recursive_dfs()(new BBProblem(problem))
}
