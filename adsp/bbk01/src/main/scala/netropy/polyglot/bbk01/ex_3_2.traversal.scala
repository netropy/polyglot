package netropy.polyglot.bbk01

import java.lang.Runtime  // need: freeMemory, maxMemory

import collection.{Seq => CSeq}
import collection.immutable.{Seq}  // need: head, tail, prepended, appended
import collection.mutable.{Buffer}  // need: remove, clear, prepend, append


/** Predefined search or tree traversal strategies.
  *
  * The order in which nodes are processed is determined by a bipartition
  * `(next, later)`, with nodes in `next` to be fathomed and branched before
  * those in `later`.
  *
  * This bipartition supports different tree traversal strategies by a
  * function that gets the chance to reshuffle (repartition) the nodes in
  * `(next, later)` before the next processing step.
  * For example, a function that
  * - moves all nodes but the first from `next` to `later` causes
  *   depth-first-search, as the processing of sibling nodes gets deferred;
  * - keeps all nodes within `next` causes breadth-first-search traversal,
  *   as siblings will be processed before child nodes.
  *
  * In addition to predefined functions for DFS and BFS, two mixed strategies
  * are shown that switch between BFS<->DFS by an internal/external criterion,
  * such as the overall number of nodes or the available amount of memory.
  *
  * Applications can define their own traversal strategy by providing a
  * function value of type `Repartition`.
  */
object TraversalPF {

  /** Functions that reshuffle a bipartition of nodes.
    *
    * Any implementation
    *     repartition: (next, later) => (next1, later1)
    *
    * - must not return an empty `next` while still holding nodes in `later`
    *     ensuring(!next1.isEmpty || later1.isEmpty)
    *
    * - for mutable collection types, should preserve object identity
    *     ensuring(next1 eq next)
    *     // same for `later`
    *
    * - for immutable collection types, should preserve their runtime types
    *     ensuring(
    *       next1.getClass == next.getClass ||
    *       next1.getClass == next.take(-1).getClass)
    *     // same for `later`
    */
  // TODO: discuss why partial function with non-exhaustive match appropriate
  //  (MatchError OK, PartialFunction vs @annotation.unchecked
  // TODO: discuss contra-/covariance, why static typing difficult here
  //type Repartition = (CSeq[Node], CSeq[Node]) => (CSeq[Node], CSeq[Node])
  type Repartition =
    PartialFunction[(CSeq[Node], CSeq[Node]), (CSeq[Node], CSeq[Node])]

  // TODO: elsewhere: discuss performance expectations:
  // immutable
  //   List: fast prepend (poor (linear) indexed access)
  //   Vector: fast append and prepend, good locality
  //   ArraySeq since >= 2.13: poor (linear) prepend
  //   Queue: implemented as a pair of Lists
  //   Stack < 2.13: deprecated, removed ("inelegant wrapper around List")
  //
  // mutable
  //   ArrayDeque: introduced in Scala 2.13, should excel
  //   ArrayBuffer: has poor (linear) prepend: +=: ++=:
  //   ListBuffer: might underperform due to cache-non-locality, GC overhead
  //   Stack, Queue < 2.13: deprecated, not a Buffer, based on MutableList
  //   Stack, Queue >= 2.13: extends ArrayDeque

  /** Depth-first-search on `Seq` (moves nodes to `later` except first). */
  val dfsSeq: Repartition = {
    case (next: Seq[Node], later: Seq[Node]) =>
      // TODO: code duplication, delegate to Traversal.dfs?
      // preserve actual collection types (.take/.companion/.iterableFactory)
      val empty = next.take(-1)
      next match {
        case hn +: tn => (hn +: empty, tn ++: later)
        case _ => later match {
          case hl +: tl => (hl +: empty, tl)
          case e => (empty, e)
        }
      }
  }

  /** Depth-first-search on `Buffer` (moves nodes to `later` except first). */
  val dfsBuffer: Repartition = {
    case (next: Buffer[Node], later: Buffer[Node]) =>
      // preserve collection object identity (no +:, ++:, .tail)
      // avoid copying of collections (no pattern matching)
      if (!next.isEmpty) {
        val n = next.remove(0)
        next ++=: later
        next.clear()
        n +=: next
      } else if (!later.isEmpty) {
        val n = later.remove(0)
        n +=: next
      }
      (next, later)

      // TODO: discuss ArrayBuffer: prepend has O(|later|) complexity
      // next ++=: later
      // consider relaxing identity+type preservation to allow for
      //   appending: (..., later ++= next)
      // Note, constant: n +=: next  // empty, same as append: n += next
  }

  /** Breadth-first-search on `Seq` (keeps all nodes in `next`). */
  val bfsSeq: Repartition = {
    case (next: Seq[Node], later: Seq[Node]) =>
      // TODO: code duplication, delegate to Traversal.bfs?
      // preserve actual collection types (.take/.companion/.iterableFactory)
      val empty = later.take(-1)
      (next ++ later, empty)
  }

  /** Breadth-first-search on `Buffer` (keeps all nodes in `next`). */
  val bfsBuffer: Repartition = {
    case (next: Buffer[Node], later: Buffer[Node]) =>
      // preserve collection object identity (no +:, ++:, .tail)
      later ++= next
      later.clear()
      (next, later)
  }

  /** Depth-first-search traversal (moves nodes to `later` except first). */
  val dfs: Repartition = dfsSeq orElse dfsBuffer

  /** Breadth-first-search traversal (keeps all nodes in `next`). */
  val bfs: Repartition = bfsSeq orElse bfsBuffer

  /** BFS while free memory is above a (fixed) percentage, otherwise DFS. */
  val bfsMemBound: Repartition = {
    case (n: CSeq[Node], l: CSeq[Node]) =>
      val minFreeMemRatioBfs = 0.8
      val rt = Runtime.getRuntime()
      val freeMemRatio = rt.freeMemory().toDouble / rt.maxMemory().toDouble
      if (freeMemRatio > minFreeMemRatioBfs) bfs(n, l) else dfs(n, l)
  }
}
