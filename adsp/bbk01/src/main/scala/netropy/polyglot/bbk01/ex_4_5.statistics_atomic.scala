package netropy.polyglot.bbk01

import java.util.concurrent.atomic.LongAdder


// TODO: outdated code

/** Atomic counters for computational statistics of a B&B-Solver run.
  *
  * This class duplicates the non-atomic counters `nXXX: Long` with an atomic
  * `nXXXA:Long` version here.  [[LongAdder]] is well suited and preferable
  * to `AtomicLong` for the purpose of collecting statistics.
  *
  * After a concurrent computation, method `copyAtomicLongs` can be used to
  * copy the atomics' values back to the non-atomic fields for display etc.
  *
  * @see [[https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/LongAdder.html]]
  */
class BBStatisticsAtomic(nVariables: Long = 0) extends
    BBStatistics(nVariables) {

  val nNodesBranchedA: LongAdder = new LongAdder
  val nNodesInfeasibleA: LongAdder = new LongAdder
  val nNodesFathomedA: LongAdder = new LongAdder
  val nNodesSuboptimalA: LongAdder = new LongAdder
  val nNodesBestA: LongAdder = new LongAdder

  def copyAtomicLongs(): Unit = {
    nNodesBranched = nNodesBranchedA.longValue
    nNodesInfeasible = nNodesInfeasibleA.longValue
    nNodesFathomed = nNodesFathomedA.longValue
    nNodesSuboptimal = nNodesSuboptimalA.longValue
    nNodesBest = nNodesBestA.longValue
  }
}
