package netropy.polyglot.bbk01


/** Predefined heuristics factory methods for the 0/1 Knapsack problem. */
object BBKnapsackHeuristics {

  def greedyLpBound(problem: BBProblem): BBHeuristics =
    new BBKnapsackHeuristicGreedyLpBound()(problem)

  def residualValue(problem: BBProblem): BBHeuristics =
    new BBKnapsackHeuristicResidualValue()(problem)

  def all(problem: BBProblem): BBHeuristics =
    new BBHeuristicsCombiner(
      new BBKnapsackHeuristicGreedyLpBound()(problem),
      new BBKnapsackHeuristicResidualValue()(problem)
    )(problem)
}
