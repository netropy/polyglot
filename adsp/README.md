# polyglot/adsp

### Algorithms, Data Structures, Patterns: Code Experiments, Exercises, Notes

Projects:
- [knapsack](knapsack/README.md) - coding exercise in Scala, Python (Backtracking)
- [bbk01](bbk01/README.md) - talk: lessons in idiomatic Scala (Branch&Bound)
- [trees (FP in Scala)](trees/README.md)

Quick links: \
[Scala sources](src/main/scala/netropy/polyglot/adsp/),
[Scala tests](src/test/scala/netropy/polyglot/adsp/)

Tools: \
[sbt](https://www.scala-sbt.org),
[Maven](https://maven.apache.org),
[Gradle](https://gradle.org),
[junit5](https://junit.org/junit5/)

Problem/language/library features: \
[tags](tags.md)

[Up](../README.md)
