package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

trait Ex_1_2_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_1.{Tree, Leaf, Branch}

  private type A = Int

  def test_size(size: Tree[A] => Int): Unit = {

    assertResult(1) {
      size(Leaf(1))
    }
    assertResult(3) {
      size(Branch(Leaf(1), Leaf(2)))
    }
    assertResult(5) {
      size(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)))
    }
    assertResult(5) {
      size(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))))
    }
    assertResult(7) {
      size(Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4)))
    }
    assertResult(7) {
      size(Branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4)))
    }
    assertResult(7) {
      size(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))
    }
    assertResult(7) {
      size(Branch(Leaf(1), Branch(Leaf(2), Branch(Leaf(3), Leaf(4)))))
    }
  }
}

class Ex_1_2_TestSuite extends Ex_1_2_Tests {

  import ex_1_2.{size_r, size_dfs_tr, size_dfs_scan1ahead_tr}

  test_size(size_r)
  test_size(size_dfs_tr)
  test_size(size_dfs_scan1ahead_tr)
}
