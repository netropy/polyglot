package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

trait Ex_2_1_Tests
    extends FunSuite
    with FunSuiteExt {

  // TODO: refactorize, split into separate files

  def test_toString[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    assert(t.toString != "")
  }

  def test_data[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {
    assert(t.dataOption.fold(true)(_ == t.data && t.isDataNode))
  }

  def test_forall_isDataNode[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {
    assert(t.forall(_.isDataNode))
  }

  def test_isBinary[A, T[+A] <: BinaryTree[A, T]](t: BinaryTree[A, T]) = {

    assert(t.forall(_.arity == 2))
    assert(t.forall(_.isBinary))
  }

  def test_t1_root[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    assert(!t.isBranch)
    assert(t.isLeaf)
    assertResult(0, t.degree)
    assertResult(0, t.children.size)
  }

  def test_t1_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_root(t)
    assert(t.children.forall(_.isLeaf))
    assert(!t.children.exists(_.isBranch))
    assertResult(1, t.size)
    assertResult(1, t.breadth)
    assertResult(0, t.height)
    assertResult(1, t.width(0))
    assertResult(0, t.width(1))
  }

  def test_t1_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_Tree(t)
    assertResult(2, t.arity)
    assert(t.isFull)
    assert(t.isComplete)
    assert(t.isPerfect)
    assert(t.isPathological)
  }

  def test_t1_1_root[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    assert(t.isBranch)
    assert(!t.isLeaf)
    assertResult(1, t.degree)
    assertResult(1, t.children.size)
  }

  def test_t1_1_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_1_root(t)
    assert(t.children.forall(_.isLeaf))
    assert(!t.children.exists(_.isBranch))
    assertResult(2, t.size)
    assertResult(1, t.breadth)
    assertResult(1, t.height)
    assertResult(1, t.width(0))
    assertResult(1, t.width(1))
    assertResult(0, t.width(2))
  }

  def test_t1_1_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_1_Tree(t)
    assertResult(2, t.arity)
    assert(!t.isFull)
    assert(!t.isComplete)
    assert(!t.isPerfect)
    assert(t.isPathological)
  }

  def test_t1_2_root[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    assert(t.isBranch)
    assert(!t.isLeaf)
    assertResult(2, t.degree)
    assertResult(2, t.children.size)
  }

  def test_t1_2_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_root(t)
    assert(t.children.forall(_.isLeaf))
    assert(!t.children.exists(_.isBranch))
    assertResult(3, t.size)
    assertResult(2, t.breadth)
    assertResult(1, t.height)
    assertResult(1, t.width(0))
    assertResult(2, t.width(1))
    assertResult(0, t.width(3))
  }

  def test_t1_2_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_Tree(t)
    assertResult(2, t.arity)
    assert(t.isFull)
    assert(t.isComplete)
    assert(t.isPerfect)
    assert(!t.isPathological)
  }

  def test_t1_1_1_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_1_root(t)
    assertResult(3, t.size)
    assertResult(1, t.breadth)
    assertResult(2, t.height)
    assertResult(1, t.width(0))
    assertResult(1, t.width(1))
    assertResult(1, t.width(2))
    assertResult(0, t.width(3))
  }

  def test_t1_1_1_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_1_1_Tree(t)
    assertResult(2, t.arity)
    assert(!t.isFull)
    assert(!t.isComplete)
    assert(!t.isPerfect)
    assert(t.isPathological)
  }

  def test_t1_2_1_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_root(t)
    assertResult(4, t.size)
    assertResult(2, t.breadth)
    assertResult(2, t.height)
    assertResult(1, t.width(0))
    assertResult(2, t.width(1))
    assertResult(1, t.width(2))
    assertResult(0, t.width(3))
  }

  def test_t1_2_1_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_1_Tree(t)
    assertResult(2, t.arity)
    assert(!t.isFull)
    assert(!t.isComplete)
    assert(!t.isPerfect)
    assert(!t.isPathological)
  }


  def test_t1_2_2full_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_root(t)
    assertResult(5, t.size)
    assertResult(3, t.breadth)
    assertResult(2, t.height)
    assertResult(1, t.width(0))
    assertResult(2, t.width(1))
    assertResult(2, t.width(2))
    assertResult(0, t.width(3))
  }

  def test_t1_2_2full_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_2full_Tree(t)
    assertResult(2, t.arity)
    assert(t.isFull)
    // left-full instance is complete, right-full is not
    //assert(t.isComplete)
    //assert(!t.isComplete)
    assert(!t.isPerfect)
    assert(!t.isPathological)
  }

  def test_t1_2_2_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_root(t)
    assertResult(5, t.size)
    assertResult(2, t.breadth)
    assertResult(2, t.height)
    assertResult(1, t.width(0))
    assertResult(2, t.width(1))
    assertResult(2, t.width(2))
    assertResult(0, t.width(3))
  }

  def test_t1_2_2_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_2_Tree(t)
    assertResult(2, t.arity)
    assert(!t.isFull)
    assert(!t.isComplete)
    assert(!t.isPerfect)
    assert(!t.isPathological)
  }

  def test_t1_2_3_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_root(t)
    assertResult(6, t.size)
    assertResult(3, t.breadth)
    assertResult(2, t.height)
    assertResult(1, t.width(0))
    assertResult(2, t.width(1))
    assertResult(3, t.width(2))
    assertResult(0, t.width(3))
  }

  def test_t1_2_3_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_3_Tree(t)
    assertResult(2, t.arity)
    assert(!t.isFull)
    assert(!t.isComplete)
    assert(!t.isPerfect)
    assert(!t.isPathological)
  }

  def test_t1_2_4_Tree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_root(t)
    assertResult(7, t.size)
    assertResult(4, t.breadth)
    assertResult(2, t.height)
    assertResult(1, t.width(0))
    assertResult(2, t.width(1))
    assertResult(4, t.width(2))
    assertResult(0, t.width(3))
  }

  def test_t1_2_4_BinaryTree[A, T[+A] <: Tree[A, T]](t: Tree[A, T]) = {

    test_t1_2_4_Tree(t)
    assertResult(2, t.arity)
    assert(t.isFull)
    assert(t.isComplete)
    assert(t.isPerfect)
    assert(!t.isPathological)
  }

  // ----------------------------------------------------------------------

  type A = Int
  type BT[+A, T[+A] <: BinaryTree[A, T]] = BinaryTree[A, T]

  def test_BinaryTree_t1[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))
    test_t1_BinaryTree(n1)
  }

  def test_BinaryTree_t1_1[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))

    test_t1_1_BinaryTree(n1
      .updatedLeft(n2))

    test_t1_1_BinaryTree(n1
      .updatedRight(n3))
  }

  def test_BinaryTree_t1_2[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))

    test_t1_2_BinaryTree(n1
      .updatedLeft(n2)
      .updatedRight(n3))
  }

  def test_BinaryTree_t1_1_1[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))

    test_t1_1_1_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4)))

    test_t1_1_1_BinaryTree(n1
      .updatedLeft(n2
         .updatedRight(n6)))

    test_t1_1_1_BinaryTree(n1
      .updatedRight(n3
         .updatedLeft(n5)))

    test_t1_1_1_BinaryTree(n1
      .updatedRight(n3
         .updatedRight(n7)))
  }

  def test_BinaryTree_t1_2_1[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))

    test_t1_2_1_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4))
      .updatedRight(n3))

    test_t1_2_1_BinaryTree(n1
      .updatedLeft(n2
         .updatedRight(n6))
      .updatedRight(n3))

    test_t1_2_1_BinaryTree(n1
      .updatedLeft(n2)
      .updatedRight(n3
         .updatedLeft(n5)))

    test_t1_2_1_BinaryTree(n1
      .updatedLeft(n2)
      .updatedRight(n3
         .updatedRight(n7)))
  }

  def test_BinaryTree_t1_2_2full[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))

    test_t1_2_2full_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4)
         .updatedRight(n6))
      .updatedRight(n3))

    test_t1_2_2full_BinaryTree(n1
      .updatedLeft(n2)
      .updatedRight(n3
         .updatedLeft(n5)
         .updatedRight(n7)))
  }

  def test_BinaryTree_t1_2_2[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))

    test_t1_2_2_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4))
      .updatedRight(n3
         .updatedLeft(n5)))

    test_t1_2_2_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4))
      .updatedRight(n3
         .updatedRight(n7)))

    test_t1_2_2_BinaryTree(n1
      .updatedLeft(n2
         .updatedRight(n6))
      .updatedRight(n3
         .updatedLeft(n5)))

    test_t1_2_2_BinaryTree(n1
      .updatedLeft(n2
         .updatedRight(n6))
      .updatedRight(n3
         .updatedRight(n7)))
  }

  def test_BinaryTree_t1_2_3[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))

    test_t1_2_3_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4))
      .updatedRight(n3
         .updatedLeft(n5)
         .updatedRight(n7)))

    test_t1_2_3_BinaryTree(n1
      .updatedLeft(n2
         .updatedRight(n6))
      .updatedRight(n3
         .updatedLeft(n5)
         .updatedRight(n7)))

    test_t1_2_3_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4)
         .updatedRight(n6))
      .updatedRight(n3
         .updatedLeft(n5)))

    test_t1_2_3_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4)
         .updatedRight(n6))
      .updatedRight(n3
         .updatedRight(n7)))
  }

  def test_BinaryTree_t1_2_4[T[+A] <: BT[A, T]](t: BT[A, T]) = {

    val Seq(n1, n2, n3, n4, n5, n6, n7) = (1 to 7).map(t.updatedData(_))

    test_t1_2_4_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4)
         .updatedRight(n6))
      .updatedRight(n3
         .updatedLeft(n5)
         .updatedRight(n7)))
  }
}

class Ex_2_1_LinkedBinaryTree_TestSuite extends Ex_2_1_Tests {

  test_BinaryTree_t1(LinkedBinaryTree.unit(0))

  test_BinaryTree_t1_1(LinkedBinaryTree.unit(0))

  test_BinaryTree_t1_2(LinkedBinaryTree.unit(0))

  test_BinaryTree_t1_1_1(LinkedBinaryTree.unit(0))

  test_BinaryTree_t1_2_1(LinkedBinaryTree.unit(0))

  test_BinaryTree_t1_2_2full(LinkedBinaryTree.unit(0))

  test_BinaryTree_t1_2_2(LinkedBinaryTree.unit(0))

  test_BinaryTree_t1_2_3(LinkedBinaryTree.unit(0))

  test_BinaryTree_t1_2_4(LinkedBinaryTree.unit(0))
}

class Ex_2_1_VectorBinaryTree_TestSuite extends Ex_2_1_Tests {

  test_BinaryTree_t1(VectorBinaryTree.unit(0))

  test_BinaryTree_t1_1(VectorBinaryTree.unit(0))

  test_BinaryTree_t1_2(VectorBinaryTree.unit(0))

  test_BinaryTree_t1_1_1(VectorBinaryTree.unit(0))

  test_BinaryTree_t1_2_1(VectorBinaryTree.unit(0))

  test_BinaryTree_t1_2_2full(VectorBinaryTree.unit(0))

  test_BinaryTree_t1_2_2(VectorBinaryTree.unit(0))

  test_BinaryTree_t1_2_3(VectorBinaryTree.unit(0))

  test_BinaryTree_t1_2_4(VectorBinaryTree.unit(0))
}
