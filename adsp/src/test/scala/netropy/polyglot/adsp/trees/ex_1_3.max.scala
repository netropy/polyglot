package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

trait Ex_1_3_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_1.{Tree, Leaf, Branch}

  private type A = Int

  def test_max(max: Tree[A] => Int): Unit = {

    assertResult(1) {
      max(Leaf(1))
    }
    assertResult(2) {
      max(Branch(Leaf(1), Leaf(2)))
    }
    assertResult(3) {
      max(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)))
    }
    assertResult(3) {
      max(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))))
    }
    assertResult(4) {
      max(Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4)))
    }
    assertResult(4) {
      max(Branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4)))
    }
    assertResult(4) {
      max(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))
    }
    assertResult(4) {
      max(Branch(Leaf(1), Branch(Leaf(2), Branch(Leaf(3), Leaf(4)))))
    }
  }
}

class Ex_1_3_TestSuite extends Ex_1_3_Tests {

  import ex_1_3.{max_r, max_dfs_tr, max_dfs_scan1ahead_tr}

  test_max(max_r)
  test_max(max_dfs_tr)
  test_max(max_dfs_scan1ahead_tr)
}
