package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

class Ex_1_7_TestSuite
    extends FunSuite
    with FunSuiteExt
    with Ex_1_2_Tests
    with Ex_1_3_Tests
    with Ex_1_4_Tests
    with Ex_1_5_Tests {

  import ex_1_7.{size_f, max_f, height_f, map_f, show_f}

  test_size(size_f)

  test_max(max_f)

  test_height(height_f)

  test_map(map_f)

  test_show(show_f)
}
