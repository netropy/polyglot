package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

import ex_1_1.{Tree, Leaf, Branch}

class Ex_1_1_Tests
    extends FunSuite
    with FunSuiteExt {

  test("Tree(|1..4|: Tree, Leaf, Branch") {
    assertEquals(
      Tree(1),
      Leaf(1) )
    assertEquals(
      Tree(1, 2),
      Branch(Leaf(1), Leaf(2)) )
    assertEquals(
      Tree(1, 2, 3),
      Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)) )
    assertEquals(
      Tree(1, 2, 3, 4),
      Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4)) )
  }
}
