package netropy.polyglot.adsp.trees

// A quick type hierarchy for testing:
trait Vegetable
trait Fruit extends Vegetable
class Carrot extends Vegetable
class Apple extends Fruit
class Lemon extends Fruit

class Ex_2_0_LinkedTreeTestData[+A <: Fruit](t: LinkedBinaryTree[A]) {

  type T[+A] = LinkedBinaryTree[A]

  val c: Carrot = new Carrot
  val a: Apple = new Apple
  val l: Lemon = new Lemon
  val t0: T[Fruit] = t.updatedData(a)

  val t1: T[Fruit] = t.updatedData(l)
  //val t2: T[Fruit] = t.updatedData(null)  // required: T[Fruit]
  val t2: T[Fruit] = t0.updatedData(null)
  val t2a: T[AnyRef] = t.updatedData(null)

  //val t3: T[Fruit] = t0.updatedData(c)  // required: Fruit
  val t3: T[Vegetable] = t0.updatedData(c)
  val t3a: T[Vegetable] = t.updatedData(c)

  val t4: T[Fruit] = t.updatedLeft(t)
  val t4a: T[Fruit] = t.updatedLeft(t0)
  val t4b: T[Fruit] = t0.updatedLeft(t)
  val t4c: T[Fruit] = t0.updatedLeft(t0)

  val t6: T[Fruit] = t0.updatedLeft(null)
  val t6a: T[Fruit] = t.updatedLeft(null)
  //val t7: T[Fruit] = t.updatedLeft(t3)  // required: T[Fruit]
  val t7: T[Vegetable] = t.updatedLeft(t3)
  val t8: T[_] = t.updatedLeft(t3)
}

class Ex_2_0_FruitTestData[T[+Fruit] <: BinaryTree[Fruit, T]](t: T[Fruit]) {

  val c: Carrot = new Carrot
  val a: Apple = new Apple
  val l: Lemon = new Lemon
  val t0: T[Fruit] = t.updatedData(a)
  val t1: T[Fruit] = t.updatedData(l)
  val t2: T[Fruit] = t.updatedData(null)
  //val t3: T[Fruit] = t.updatedData(c)  // Required: Fruit
  val t3: T[Vegetable] = t.updatedData(c)

  val t4: T[Fruit] = t.updatedLeft(t)
  val t5: T[Fruit] = t.updatedLeft(t1)
  //val t6: T[Fruit] = t.updatedLeft(null)  // required: T[Fruit]
  //val t7: T[Fruit] = t.updatedLeft(t3)  // required: T[Fruit]
  val t7: T[Vegetable] = t.updatedLeft(t3)
  //val t8: T[_] = t.updatedLeft(t3)  // dotty: unreducible application of higher-kinded type T to wildcard arguments
}

class Ex_2_0_TestData[A, T[+A] <: BinaryTree[A, T]](t: BinaryTree[A, T]) {

  val Seq(
    n01, n02, n03, n04, n05, n06, n07, n08, n09, n10, n11, n12, n13, n14, n15)
    = (1 to 15).map(t.updatedData(_))
/*
  val t0001 =
    (n01
      .updatedLeft(n02
         .updatedLeft(n04
           .updatedLeft(n08)
           .updatedRight(n09))
         .updatedRight(n05
           .updatedLeft(n10)
           .updatedRight(n11)))
      .updatedRight(n03
         .updatedLeft(n06
           .updatedLeft(n12)
           .updatedRight(n13))
         .updatedRight(n07
           .updatedLeft(n14)
           .updatedRight(n15))))
 */

/*
    test_t1_2_4_BinaryTree(n1
      .updatedLeft(n2
         .updatedLeft(n4)
         .updatedRight(n6))
      .updatedRight(n3
         .updatedLeft(n5)
         .updatedRight(n7)))
    Map(
      le -> Seq(1, 2, 3, 4, 6, 5, 7),
      pr -> Seq(1, 2, 4, 6, 3, 5, 7),
      po -> Seq(4, 6, 2, 5, 7, 3, 1),
      in -> Seq(4, 2, 6, 1, 5, 3, 7))
 */

  val Seq(le, pr, po, in) = (1 to 4).toSeq

  def genTree(depth: Int, start: A): Seq[T[A]] = {
    require(depth >= 0)

    val t0: T[A] = t.updatedData(start)
    val t1: T[Any] = t.updatedData(null)  // passing null widens to T[Any]
    //val t2 = t.updatedLeft(null: T[Any])  // cannot pass null, T not covariant
    if (depth == 0) Seq(t0)//, null)  // TODO: T[A] not nullable, redesign API
    else {
      for {
        c0 <- genTree(depth - 1, start)
        c1 <- genTree(depth - 1, start)
      } yield t0.updatedChildren(Seq(c0, c1))
    }
  }
}
