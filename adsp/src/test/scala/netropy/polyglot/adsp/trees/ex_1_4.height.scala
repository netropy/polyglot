package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

trait Ex_1_4_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_1.{Tree, Leaf, Branch}

  private type A = Int

  def test_height(height: Tree[A] => Int): Unit = {

    assertResult(0) {
      height(Leaf(1))
    }
    assertResult(1) {
      height(Branch(Leaf(1), Leaf(2)))
    }
    assertResult(2) {
      height(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)))
    }
    assertResult(2) {
      height(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))))
    }
    assertResult(3) {
      height(Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4)))
    }
    assertResult(3) {
      height(Branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4)))
    }
    assertResult(2) {
      height(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))
    }
    assertResult(3) {
      height(Branch(Leaf(1), Branch(Leaf(2), Branch(Leaf(3), Leaf(4)))))
    }
  }
}

class Ex_1_4_TestSuite extends Ex_1_4_Tests {

  import ex_1_4.{ height_r,
    height_dfs_pair_tr, height_dfs_null_tr, height_bfs_null_tr,
    height_dfs_scan1ahead_tr, height_dfs_scan1ahead_i }

  test_height(height_r)
  test_height(height_dfs_pair_tr)
  test_height(height_dfs_null_tr)
  test_height(height_bfs_null_tr)
  test_height(height_dfs_scan1ahead_tr)
  test_height(height_dfs_scan1ahead_i)
}
