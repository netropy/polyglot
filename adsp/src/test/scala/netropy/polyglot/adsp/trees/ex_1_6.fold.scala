package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

trait Ex_1_6_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_1.{Tree, Leaf, Branch}

  private type A = Int
  private type B = Tree[A]
  private val f: A => B = Leaf(_)
  private val g: (B, B) => B = Branch(_, _)

  def test_fold(fold: Tree[A] => (A => B) => ((B, B) => B) => B): Unit = {

    assertEquals(
      fold(Leaf(1))(f)(g),
      Leaf(1) )
    assertEquals(
      fold(Branch(Leaf(1), Leaf(2)))(f)(g),
      Branch(Leaf(1), Leaf(2)) )
    assertEquals(
      fold(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)))(f)(g),
      Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)) )
    assertEquals(
      fold(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))))(f)(g),
      Branch(Leaf(1), Branch(Leaf(2), Leaf(3))) )
    assertEquals(
      fold(Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4)))(f)(g),
      Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4)) )
    assertEquals(
      fold(Branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4)))(f)(g),
      Branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4)) )
    assertEquals(
      fold(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))(f)(g),
      Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))) )
    assertEquals(
      fold(Branch(Leaf(1), Branch(Leaf(2), Branch(Leaf(3), Leaf(4)))))(f)(g),
      Branch(Leaf(1), Branch(Leaf(2), Branch(Leaf(3), Leaf(4)))) )
  }
}

class Ex_1_6_TestSuite extends Ex_1_6_Tests {

  import ex_1_6.{fold_r, fold_dfs_null_tr}

  test_fold(fold_r)
  test_fold(fold_dfs_null_tr)
}
