package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

/** Tests API variance expectations for the abstract Tree type.
  *
  * Notes:
  *
  * Why Tree's covariance in +A, T[+_] but invariance in T?
  * See detailed discussion in .../...variance_api_design.md
  */
trait Ex_2_1_2_TreeType_Tests
    extends FunSuite
    with FunSuiteExt {

  /** Easiest: define a generic, bounded tree type as abstract type member.
    * Alternative: type parameter
    *   trait Ex_2_1_2_TreeType_Tests[T[+B] <: Tree[B, T]] {...}
    */
  type T[+B] <: Tree[B, T]

  /** Easiest: inject a factory by abstract method.
    * Alternatives: parametrized class/trait, self-type annotation
    *   abstract class ...[...](factory: TreeFactory[T]) {...}  // Scala2+3
    *   trait ...[...](factory: TreeFactory[T]) {...}  // Scala3
    *   trait ...[...] { this: HasTreeFactory[T] => ...}  // Scala2+3
    */
  def factory: TreeFactory[T]

  // Test values:
  val c: Carrot = new Carrot
  val a: Apple = new Apple
  val l: Lemon = new Lemon
  val tc: T[Carrot] = factory.unit(c)
  val ta: T[Apple] = factory.unit(a)
  val tl: T[Lemon] = factory.unit(l)
  val ti: T[Int] = factory.unit(1)
  val t: T[Any] = factory.unit(())

  // Tests for covariance in A

  def compile_test_T_is_covariant_in_A: Unit = {
    val t0: T[Vegetable] = tc
    val t1: T[Vegetable] = ta
    val t2: T[Fruit] = tl
    val t3: T[Any] = tc
    //val t4: T[Apple] = null  // correct compile error, T is invariant
  }

  // Tests for A in covariant position

  def compile_test_data_returns_A: Unit = {
    val d0: Carrot = tc.data
    val d1: Any = t.data
  }

  def compile_test_children_returns_most_specific_type: Unit = {
    val t0: Seq[T[Carrot]] = tc.children
    val t1: Seq[T[Any]] = t.children
  }

  def compile_test_iterator_returns_most_specific_type: Unit = {
    val t0: Iterator[T[Carrot]] = tc.iterator
    val t1: Iterator[T[Any]] = t.iterator
  }

  // Tests for A in contravariant position

  def compile_test_updatedData_widens_to_LUB_type: Unit = {
    //val t0: T[Fruit] = tc.updatedData(a)  // correct compile error
    val t0: T[Vegetable] = tc.updatedData(a)
    val t1: T[Fruit] = ta.updatedData(l)
    val t2: T[Lemon] = tl.updatedData(l)
    val t3: T[Any] = tl.updatedData(1)
    //val t4: T[Apple] = t.updatedData(a)  // correct compile error
    val t4: T[Any] = t.updatedData(a)
  }

  def compile_test_updatedData_accepts_null: Unit = {
    val t4: T[Any] = t.updatedData(null)
    val t5: T[Apple] = ta.updatedData(null)
  }

  def compile_test_updatedChildren_widens_to_LUB_type: Unit = {
    //val t0: T[Fruit] = tc.updatedChildren(Seq(ta))  // correct compile error
    val t0: T[Vegetable] = tc.updatedChildren(Seq(ta))
    val t1: T[Fruit] = ta.updatedChildren(Seq(tl))
    val t2: T[Lemon] = tl.updatedChildren(Seq(tl))
    val t4: T[Any] = tl.updatedChildren(Seq(ti))
    //val t3: T[Apple] = t.updatedChildren(Seq(ta))  // correct compile error
    val t3: T[Any] = t.updatedChildren(Seq(ta))
  }

  def compile_test_updatedChildren_accepts_null: Unit = {
    // correct compile error, invariant T, hence not: Null <: T
    //val t4: T[Any] = t.updatedChildren(Seq(null))
    //val t5: T[Apple] = ta.updatedChildren(Seq(null))
  }
}

/** Tests API variance expectations for the abstract BinaryTree type. */
trait Ex_2_1_2_BinaryTreeType_Tests
    extends Ex_2_1_2_TreeType_Tests {

  override type T[+B] <: BinaryTree[B, T]

  // Tests for A in covariant/contravariant position

  def compile_test_left_returns_most_specific_type: Unit = {
    val t0: T[Carrot] = tc.left
    val t1: T[Any] = t.left
  }

  def compile_test_updatedLeft_widens_to_LUB_type: Unit = {
    //val t0: T[Fruit] = tc.updatedLeft(ta)  // correct compile error
    val t0: T[Vegetable] = tc.updatedLeft(ta)
    val t1: T[Fruit] = ta.updatedLeft(tl)
    val t2: T[Lemon] = tl.updatedLeft(tl)
    //val t3: T[Apple] = t.updatedLeft(ta)  // correct compile error
    val t3: T[Any] = t.updatedLeft(ta)
  }

  def compile_test_updatedLeft_accepts_null: Unit = {
    // correct compile error, invariant T, hence not: Null <: T
    //val t4: T[Any] = t.updatedLeft(null)
    //val t5: T[Apple] = ta.updatedLeft(null)
  }
}


/** Tests API variance expectations for the concrete LinkedBinaryTree type. */
class Ex_2_1_2_LinkedBinaryTreeType_Tests
    extends Ex_2_1_2_BinaryTreeType_Tests {

  // define tree type, factory
  override type T[+B] = LinkedBinaryTree[B]
  override def factory: BinaryTreeFactory[LinkedBinaryTree] = LinkedBinaryTree

  override def compile_test_updatedChildren_accepts_null: Unit = {
    // supported now, T no longer abstract/parameter but alias, Null <: T
    val t4: T[Any] = t.updatedChildren(Seq(null))
    val t5: T[Apple] = ta.updatedChildren(Seq(null))
  }

  override def compile_test_updatedLeft_accepts_null: Unit = {
    // supported now, T no longer abstract/parameter but alias, Null <: T
    val t4: T[Any] = t.updatedLeft(null)
    val t5: T[Apple] = ta.updatedLeft(null)
  }
}
