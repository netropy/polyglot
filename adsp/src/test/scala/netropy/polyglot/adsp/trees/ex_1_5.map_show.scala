package netropy.polyglot.adsp.trees

import munit.FunSuite
import netropy.polyglot.adsp.FunSuiteExt  // sugar

trait Ex_1_5_Tests
    extends FunSuite
    with FunSuiteExt {

  import ex_1_1.{Tree, Leaf, Branch}

  private type A = Int
  private type B = Double
  private val f: A => B = _.toDouble

  def test_map(map: Tree[A] => (A => B) => Tree[B]): Unit = {

    assertEquals(
      map(Leaf(1))(f),
      Leaf(1.0) )
    assertEquals(
      map(Branch(Leaf(1), Leaf(2)))(f),
      Branch(Leaf(1.0), Leaf(2.0)) )
    assertEquals(
      map(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)))(f),
      Branch(Branch(Leaf(1.0), Leaf(2.0)), Leaf(3.0)) )
    assertEquals(
      map(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))))(f),
      Branch(Leaf(1.0), Branch(Leaf(2.0), Leaf(3.0))) )
    assertEquals(
      map(Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4)))(f),
      Branch(Branch(Branch(Leaf(1.0), Leaf(2.0)), Leaf(3.0)), Leaf(4.0)) )
    assertEquals(
      map(Branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4)))(f),
      Branch(Branch(Leaf(1.0), Branch(Leaf(2.0), Leaf(3.0))), Leaf(4.0)) )
    assertEquals(
      map(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))(f),
      Branch(Branch(Leaf(1.0), Leaf(2.0)), Branch(Leaf(3.0), Leaf(4.0))) )
    assertEquals(
      map(Branch(Leaf(1), Branch(Leaf(2), Branch(Leaf(3), Leaf(4)))))(f),
      Branch(Leaf(1.0), Branch(Leaf(2.0), Branch(Leaf(3.0), Leaf(4.0)))) )
  }

  def test_show(show: Tree[A] => String): Unit = {

    assertEquals(
      show(Leaf(1)),
      "(1)")
    assertEquals(
      show(Branch(Leaf(1), Leaf(2))),
      "[(1),(2)]")
    assertEquals(
      show(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))),
      "[[(1),(2)],(3)]")
    assertEquals(
      show(Branch(Leaf(1), Branch(Leaf(2), Leaf(3)))),
      "[(1),[(2),(3)]]")
    assertEquals(
      show(Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4))),
      "[[[(1),(2)],(3)],(4)]")
    assertEquals(
      show(Branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4))),
      "[[(1),[(2),(3)]],(4)]")
    assertEquals(
      show(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4)))),
      "[[(1),(2)],[(3),(4)]]")
    assertEquals(
      show(Branch(Leaf(1), Branch(Leaf(2), Branch(Leaf(3), Leaf(4))))),
      "[(1),[(2),[(3),(4)]]]")
  }
}

class Ex_1_5_TestSuite extends Ex_1_5_Tests {

  import ex_1_5.{map_r, map_dfs_null_tr, show_r, show_dfs_null_tr}

  test_map(map_r)
  test_map(map_dfs_null_tr)

  test_show(show_r)
  test_show(show_dfs_null_tr)
}
