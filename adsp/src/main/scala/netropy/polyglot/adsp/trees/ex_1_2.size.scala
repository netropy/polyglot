package netropy.polyglot.adsp.trees


object ex_1_2 {

  import ex_1_1.{Tree, Leaf, Branch}

  /* size: Returns the number of nodes (leaves and branches) in a tree. */

  /*
   * Notes: Algorithmic variants
   *
   * size_r:
   *     recursive decomposition with pre-order traversal (DFS)
   *
   * size_dfs_tr:
   *     tail-recursive, pre-order DFS with List as LIFO queue
   *
   * size_dfs_scan1ahead_tr:
   *     tail-recursive, pre-order DFS with Tree as LIFO queue
   *
   *     Scans ahead the left child and reduces it if a leaf; if a branch,
   *     recursively transforms the binary tree into a list: a _right comb_,
   *     in which all left childs are leaves (and thus can be reduced).
   *     Utilizes:
   *     - that lists are an edge-case of trees; can be generalized to n-way
   */

  // not: @annotation.tailrec
  def size_r[A](node: Tree[A]): Int = {
    require(node != null)

    node match {
      case Leaf(_) => 1
      case Branch(l, r) => size_r(l) + size_r(r) + 1
    }
  }

  def size_dfs_tr[A](node: Tree[A]): Int = {
    require(node != null)

    @annotation.tailrec
    def go(lt: List[Tree[A]], z: Int): Int =
      lt match {
        case Nil => z
        case Leaf(_) :: tt => go(tt, z + 1)
        case Branch(l, r) :: tt => go(l :: r :: tt, z + 1)
      }
    go(List(node), 0)
  }

  def size_dfs_scan1ahead_tr[A](node: Tree[A]): Int = {
    require(node != null)

    @annotation.tailrec
    def go(t: Tree[A], z: Int): Int =
      t match {
        case Leaf(_) => z + 1
        case Branch(Leaf(_), r) => go(r, z + 2)
        case Branch(Branch(ll, lr), r) => go(Branch(ll, Branch(lr, r)), z)
      }
    go(node, 0)
  }
}
