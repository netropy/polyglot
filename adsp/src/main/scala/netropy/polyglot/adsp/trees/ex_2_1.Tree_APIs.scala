package netropy.polyglot.adsp.trees

import scala.collection.{Iterable, Iterator}


/*
 * OO-style, f-bound design of abstract Tree types.
 *
 * Notes: Why Tree's covariance in +A, T[+_] but invariance in T?
 *        See detailed discussion in .../...variance_api_design.md
 *
 *
 * TODO: Notes:
 * - make covariant in +A, invariant in T[+A] ...
 * - cannot pass `null` for invariant type T[A]:
 *   def updatedLeft[A1 >: A](node: T[A1]): T[A1]
 * - only Scala3:
 *   def updatedLeft[A1 >: A](node: T[A1] | Null): T[A1]
 * - ambigous overload for `null`:
 *   def updatedLeft[A1 >: A](node: T[A1] | Null): T[A1]
 *   def updatedLeft[A1 >: A](node: Option[T[A1]]): T[A1]
 * - not a good idea
 *     def left: T[A] | Null
 *   caller would have to cast
 *     left.asInstanceOf[T[A1]]
 * - cannot overload, same type after erasure:
 *   def updateChildren[A1 >: A](nodes: Seq[T[A1]]): Unit
 *   def updateChildren[A1 >: A](nodes: Seq[Option[T[A1]]]): Unit
 * - too many issues with allowing for covariant +T[+A]
 *   would allow for passing null, though
 *
 * TODO: not helpful
 * cannot be implemented by subtype, conflicting methods, T is invariant
 * trait GenericBinaryTree[+A]
 *     extends BinaryTree[A, GenericBinaryTree] {
 * }
 * trait GenericBinaryTreeFactory {
 *   def unit[A](a: A): GenericBinaryTree[A]
 * }
 */

trait Tree[+A, T[+B] <: Tree[B, T]]
    extends Iterable[T[A]] {

  // TODO: fix endless loop with Iterable.toString
  override def toString: String =
    s"Tree($data, ${children.mkString("[", ",", "]")})"
    //s"Tree(data=$data, children=$children)"

  /** Whether this node type carries data.
    * For example, some tree types only store data in the leaves.
    */
  def isDataNode: Boolean

  /** Returns the data stored in this node.
    * Note: `null` is a valid value, since Tree is covariant in A.
    * @throws [[UnsupportedOperationException]] if !isDataNode
    */
  def data: A

  /** Returns the data stored in this node, or None if not carrying data.
    * Note: May return `Some(null)`, since Tree is covariant in A.
    * Note: t.dataOption == None  <=>  !t.isDataNode
    */
  def dataOption: Option[A]

  /** Returns a copy of the node having the given data.
    * Note: Accepts `null` as valid value, since Tree is covariant in A.
    */
  def updatedData[A1 >: A]
      (data: A1): T[A1]

  /** Returns all existing child nodes, left-to-right.
    * Note: children.size == degree && !children.contains(null)
    */
  def children: Seq[T[A]]

  /** Returns an option for each child node, left-to-right.
    * Note: childrenOption.size == arity && !childrenOption.contains(null)
    */
  def childrenOption: Seq[Option[T[A]]]

  /** Returns a copy of the node with given nodes as children, left-to-right.
    * Note: `nodes` cannot contain `null`, since Tree is invariant in T[_].
    * Note: t.updatedChildren(nodes).children == nodes
    * @throws [[IllegalArgumentException]] if nodes.size > arity
    */
  def updatedChildren[A1 >: A]
      (nodes: Seq[T[A1]]): T[A1]

  /** Returns a copy of the node with given nodes as children, left-to-right.
    * Note: t.updatedChildrenOption(nodes).childrenOption == nodes
    * @throws [[IllegalArgumentException]] if nodes.size != arity
    */
  def updatedChildrenOption[A1 >: A]
      (nodes: Seq[Option[T[A1]]]): T[A1]

  /** Returns the maximum number of possible children per node
    * (`k`- or `n`-way tree).
    */
  def arity: Int

  /** Returns the number of existing children.
    * Note: degree <= arity
    */
  def degree: Int = children.size

  /** Returns whether this node type is inner/internal (has children). */
  def isBranch: Boolean = !isLeaf

  /** Returns whether this node type is outer/external (no children). */
  def isLeaf: Boolean = children.isEmpty

  // invariants on non-final methods
  assert(degree <= arity)
  assert(degree == children.size)
  assert(arity == childrenOption.size)
  assert(isBranch == !isLeaf)
  assert(isLeaf == children.isEmpty)

  // ----------------------------------------------------------------------
  // Iterable and Subtree Functions
  //
  // Available from Iterable[T[A]], implemented via Iterator[T[A]]:
  //   def size: Int
  //   def exists(p: T[A] => Boolean): Boolean
  //   def forall(p: T[A] => Boolean): Boolean
  //   def find(p: T[A] => Boolean): Option[T[A]]
  //   ...
  // [[https://www.scala-lang.org/api/current/scala/collection/Iterable.html]]
  //
  // Implemented by trait TreeBreadthHeightWidth.
  // ----------------------------------------------------------------------

  /** Returns the number of leaves reachible from this node (>= 1). */
  def breadth: Int

  /** Returns the length of the longest downward path to a leaf. */
  def height: Int

  /** Returns the number of nodes in the level below (>= 0) this node,
    * or 0 if the tree does not have that level.
    */
  def width(level: Int): Int

  // ----------------------------------------------------------------------
  // Tree Traversal (External Iteration)
  //
  // Alternatively, could define a (Scala3) enum for tree walk order:
  //   enum Order { case LevelOrder, PreOrder, PostOrder }
  //
  // Iterators should iterate over instances of the actual, tree-implementing
  // subtype `T[A]`, not the generic tree trait `Tree[A, T]]` from which
  // callers would have to downcast.
  //
  // Implemented by trait TreeIterators.
  // [[https://en.wikipedia.org/wiki/Tree_traversal]]
  // ----------------------------------------------------------------------

  /** Visits each node once in a fixed but unspecified order. */
  def iterator: Iterator[T[A]]

  /** Visits each node once: BFS, top-to-bottom by levels, left-to-right. */
  def iterateLevelOrder: Iterator[T[A]]

  /** Visits each node once: DFS, parent before children, left-to-right. */
  def iteratePreOrder: Iterator[T[A]]

  /** Visits each node once: DFS, left-to-right, children before parent. */
  def iteratePostOrder: Iterator[T[A]]

  // ----------------------------------------------------------------------
  // Data Folds (Internal Iteration)
  // ----------------------------------------------------------------------

  // breadth-first traversal

  /** Visits each datum once: top-to-bottom by levels, left-to-right. */
  def foldLevelOrder[A, B]
    (z: B)
    (op: (B, A) => B): B = ???

  // depth-first traversal

  /** Visits each datum once: parent before children, left-to-right. */
  def foldPreOrder[A, B]
    (z: B)
    (op: (B, A) => B): B = ???

  /** Visits each datum once: left-to-right, children before parent. */
  def foldPostOrder[A, B]
    (z: B)
    (op: (B, A) => B): B = ???

  // ----------------------------------------------------------------------
  // shape properties
  // https://en.wikipedia.org/wiki/M-ary_tree
  // ----------------------------------------------------------------------

  /** Whether all nodes have a degree of exactly 0 or the tree's arity
    * (a.k.a. proper, plane, locally complete).
    */
  def isFull: Boolean

  /** Whether this tree is full on every level except for the last,
    * which is filled from left to right.
    */
  def isComplete: Boolean

  /** Whether this tree is filled on all levels. */
  def isPerfect: Boolean

  /** Whether this tree is a list (degree <= 1, a.k.a. degenerate). */
  def isPathological: Boolean
}


/** Creates a tree. */
trait TreeFactory[T[+B] <: Tree[B, T]] {

  // TODO: rename to apply?  make private and rename c'tor for impl classes?
  def unit[A](a: A): T[A]
}

// ----------------------------------------------------------------------
// Binary Trees
// https://en.wikipedia.org/wiki/Binary_tree
// ----------------------------------------------------------------------

/** A 2-way tree. */
trait BinaryTree[+A, T[+B] <: BinaryTree[B, T]]
    extends Tree[A, T] {

  override final def arity: Int = 2

  override def children: Seq[T[A]] =
    childrenOption.flatten  // discards None

  override def childrenOption: Seq[Option[T[A]]] =
    Vector(leftOption, rightOption)

  /** Whether this tree type is 2-way. */
  final def isBinary: Boolean = true

  /** Returns the left child node, or null if there is none. */
  def left: T[A]

  /** Returns the left child node. */
  def leftOption: Option[T[A]] = Option(left)

  /** Returns the right child node, or null if there is none. */
  def right: T[A]

  /** Returns the right child node. */
  def rightOption: Option[T[A]] = Option(right)

  /** Returns a copy of the node with the given node as left child.
    * Note: `node` cannot be `null`, since Tree is invariant in T[_].
    * Note: t.updatedLeft(node).left == node
    */
  def updatedLeft[A1 >: A](node: T[A1]): T[A1]

  /** Returns a copy of the node with the node option as left child.
    * Note: t.updatedLeftOption(node).leftOption == node
    */
  def updatedLeftOption[A1 >: A](node: Option[T[A1]]): T[A1]

  /** Returns a copy of the node with the given node as right child.
    * Note: `node` cannot be `null`, since Tree is invariant in T[_].
    * Note: t.updatedRight(node).right == node
    */
  def updatedRight[A1 >: A](node: T[A1]): T[A1]

  /** Returns a copy of the node with the node option as right child.
    * Note: t.updatedRightOption(node).rightOption == node
    */
  def updatedRightOption[A1 >: A](node: Option[T[A1]]): T[A1]

  // invariants on non-final methods
  assert(childrenOption == Seq(leftOption, rightOption))
  assert(children == childrenOption.flatten)

  // ----------------------------------------------------------------------
  // shape properties
  // ----------------------------------------------------------------------

  /** Whether this tree is a left comb (full and each right child a leaf). */
  def isLeftComb: Boolean

  /** Whether this tree is a right comb (full and each left child a leaf). */
  def isRightComb: Boolean

  // ----------------------------------------------------------------------
  // data folds
  // ----------------------------------------------------------------------

  // depth-first traversal

  /** Visits data-bearing nodes: left child, then parent, right child. */
  def foldInOrder[A, B]
    (z: B)
    (op: (B, A) => B): B = ???

  /** Visits data-bearing nodes: right child, then parent, left child. */
  def foldReverseInOrder[A, B]
    (z: B)
    (op: (B, A) => B): B = ???
}


/** Creates a 2-way tree. */
trait BinaryTreeFactory[T[+B] <: BinaryTree[B, T]]
    extends TreeFactory[T] {

  def unit[A](a: A): T[A]
}

// ----------------------------------------------------------------------
// Bidirectionally Traversable Trees
// ----------------------------------------------------------------------

/** A tree with navigation from a (non-root) node to its parent. */
trait UpTraversableTree[+A, T[+B] <: UpTraversableTree[B, T]]
    extends Tree[A, T] {

  /** Whether this tree has navigation from a node to its parent. */
  final def isUpTraversable: Boolean = true

  /** Returns the parent node, or null if root. */
  def parent: T[A]
  def parentOption: Option[T[A]] = Option(parent)

  /** Whether this node is the root. */
  def isRoot: Boolean = parent == null

  /** Returns the length of the path to the root. */
  def depth: Int = ???

  /* Returns the number of nodes in the level above (<0) or below (>0)
   * this node; or 0 if the tree does not have that level.
   */
  def width(level: Int): Int

  //TODO: comment on
  // covariant type +A in contravariant position
  // dotty: unreducible application of higher-kinded type T to wildcard arguments
  // def distance(n: T[_]): Int = ???
  // def distance(n: UpTraversableTree[B, T]): Int = ???
  type U[B] = UpTraversableTree[B, T]

  /** Returns the length of the path between this and the given node.
    * TODO: how to indicate error? IAE for unconnected nodes?
    */
  def distance(n: U[_]): Int = ???

  /** Whether this node is a parent or child of the given node. */
  def isNeighborOf(n: U[_]): Boolean = ???

  /** Whether the given node is reachable by proceeding upwards. */
  def isDescendantOf(n: U[_]): Boolean = ???

  /** Whether the given node is reachable by proceeding downwards. */
  def isAncestorOf(n: U[_]): Boolean =
    n.isDescendantOf(this)
}

/** Creates an up-traversable tree. */
trait UpTraversableTreeFactory[T[+B] <: UpTraversableTree[B, T]]
    extends TreeFactory[T] {

  def unit[A](a: A): T[A]
}

// ----------------------------------------------------------------------
// Mutable Tree Types
// ----------------------------------------------------------------------

/** A tree that allows to update the data in-place. */
trait MutableNode[+A, T[+B] <: MutableNode[B, T]]
    extends Tree[A, T] {

  /** Replaces the node's data. */
  def updateData[A1 >: A](data: A1): Unit
}

/** A tree that allows to update the child nodes in-place.
  *
  * Caution: This trait's in-place update methods
  * - do _not_ check for circular references (consider passing a copy);
  * - are _not_ thread-safe.
  *
  * Note: All in-place update methods support method chaining:
  * - t.updateXXX(xxx) eq t
  */
trait MutableTree[+A, T[+B] <: MutableTree[B, T]]
    extends Tree[A, T] {

  /** Replaces the children with the given nodes, left-to-right.
    * Note: `nodes` cannot contain `null`, since Tree is invariant in T[_].
    * Note: t.updateChildren(nodes).children == nodes
    * @throws [[IllegalArgumentException]] if nodes.size > arity
    */
  def updateChildren[A1 >: A](nodes: Seq[T[A1]]): T[A1]

  /** Replaces the children with the given node options, left-to-right.
    * Note: t.updateChildrenOption(nodes).childrenOption == nodes
    * @throws [[IllegalArgumentException]] if nodes.size != arity
    */
  def updateChildrenOption[A1 >: A](nodes: Seq[Option[T[A1]]]): T[A1]
}

/** A binary tree that allows to update the child nodes in-place. */
trait MutableBinaryTree[+A, T[+B] <: MutableBinaryTree[B, T]]
    extends BinaryTree[A, T]
    with MutableTree[A, T] {

  /** Replaces the left child node with the given node.
    * Caution: No check for circular references.  May want to pass a copy.
    */
  def updateLeft[A1 >: A](node: T[A1]): T[A1]

  /** Replaces the left child with the node option.
    * Note: t.updatedParent(node).parent == node
    */
  def updateLeftOption[A1 >: A](node: Option[T[A1]]): T[A1]

  /** Replaces the right child node with the given node.
    * Caution: No check for circular references.  May want to pass a copy.
    */
  def updateRight[A1 >: A](node: T[A1]): T[A1]

  /** Replaces the right child with the node option.
    * Note: t.updatedParent(node).parent == node
    */
  def updateRightOption[A1 >: A](node:Option[T[A1]]): T[A1]
}

/** A tree that allows to update the parent node in-place. */
trait MutableUpTraversableTree[+A, T[+B] <: MutableUpTraversableTree[B, T]]
    extends UpTraversableTree[A, T]
    with MutableTree[A, T] {

  /** Replaces the parent with the given node.
    * Note: `node` cannot be `null`, since Tree is invariant in T[_].
    * Note: t.updatedParent(node).parent == node
    */
  def updateParent[A1 >: A](node: T[A1]): T[A1]

  /** Replaces the parent with the node option.
    * Note: t.updatedParent(node).parent == node
    */
  def updateParentOption[A1 >: A](node: Option[T[A1]]): T[A1]
}


/** Creates a mutable node. */
trait MutableNodeFactory[T[+B] <: MutableNode[B, T]]
    extends TreeFactory[T] {

  def unit[A](a: A): T[A]
}

/** Creates a mutable tree. */
trait MutableTreeFactory[T[+B] <: MutableTree[B, T]]
    extends TreeFactory[T] {

  def unit[A](a: A): T[A]
}

/** Creates a mutable, 2-way tree. */
trait MutableBinaryTreeFactory[T[+B] <: MutableBinaryTree[B, T]]
    extends BinaryTreeFactory[T]
    with MutableTreeFactory[T] {

  def unit[A](a: A): T[A]
}

/** Creates a mutable, up-traversable tree. */
trait MutableUpTraversableTreeFactory[T[+B] <: MutableUpTraversableTree[B, T]]
    extends UpTraversableTreeFactory[T]
    with MutableTreeFactory[T] {

  def unit[A](a: A): T[A]
}

// ----------------------------------------------------------------------
// TODO: model these tree types in a better way (e.g. use Ordering)
// ----------------------------------------------------------------------

trait OrderedTree[+A, T[+B] <: OrderedTree[B, T]]
    extends Tree[A, T] {

  /** Whether this tree type maintains a total ordering on all nodes. */
  final def isOrdered: Boolean = true
}

trait BinarySearchTree[+A, T[+B] <: BinarySearchTree[B, T]]
    extends BinaryTree[A, T]
    with OrderedTree[A, T]

trait BalancedTree[+A, T[+B] <: BalancedTree[B, T]]
    extends Tree[A, T] {

  /** Returns whether this binary tree type maintains a balance.
    *
    * In a balanced binary tree, the left and right subtrees of every node
    * differ in height by no more than 1.
    */
  final def isBalanced: Boolean = true
}
