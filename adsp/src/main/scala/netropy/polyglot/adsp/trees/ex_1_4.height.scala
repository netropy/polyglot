package netropy.polyglot.adsp.trees


object ex_1_4 {

  import ex_1_1.{Tree, Leaf, Branch}

  /* height: Returns the length of the longest downward path to a leaf. */

  /*
   * Notes: Algorithmic variants
   *
   * height_r:
   *     recursive decomposition with pre-order traversal (DFS)
   *
   * height_dfs_pair_tr:
   *     tail-recursive, pre-order DFS with List as LIFO queue
   *
   *     Stores pairs (depth, node) so that the current depth is always known.
   *
   * height_dfs_null_tr:
   *     tail-recursive, pre-order DFS with List as LIFO queue
   *
   *     Stores `null` as sentinel indicating the completion of a branch.
   *     each non-null list element must then be a step into a level down.
   *     Utilizes:
   *     - availability of null as type-conform, distinct value
   *     - that List can store nulls and has an efficient prepend operation
   *
   * height_bfs_null_tr:
   *     tail-recursive, level-order BFS with Vector as FIFO queue
   *
   *     Stores `null` as sentinel indicating the completion of a level;
   *     each non-null list element must then be on the same level.
   *     Utilizes:
   *     - availability of null as type-conform, distinct value
   *     - that Vector can store nulls and has an efficient append operation
   *
   * height_dfs_scan1ahead_tr:
   *     tail-recursive, pre-order DFS with List as LIFO queue
   *
   *     Scans ahead for a left child leaf and never pushes it, so that any
   *     popped leaf must have been a right child, which therefore indicates
   *     the completion of a branch.
   *     Utilizes:
   *     - that this tree type is binary, cannot be generalized to n-way
   *
   * height_dfs_scan1ahead_i:
   *     iterative version of height_dfs_scan1ahead_tr
   *
   *     Demonstrates Scala's do-while syntax variants:
   *       do { body } while ( cond )   // scala 2 only
   *       while { body ; cond } do ()  // new scala 3
   *       while ({ body ; cond }) ()   // scala 2+3    <- picked this one
   *
   * See comments on implementation.
   */

  // not: @annotation.tailrec
  def height_r[A](node: Tree[A]): Int = {
    require(node != null)

    node match {
      case Leaf(_) => 0
      case Branch(l, r) => (height_r(l) max height_r(r)) + 1
    }
  }

  def height_dfs_pair_tr[A](node: Tree[A]): Int = {
    require(node != null)

    @annotation.tailrec
    def go(lt: List[(Int, Tree[A])], z: Int): Int =
      lt match {
        case Nil => z
        case (d, Leaf(_)) :: tt => go(tt, d max z)
        case (d, Branch(l, r)) :: tt => go((d + 1, l) :: (d + 1, r) :: tt, z)
      }
    go(List((0, node)), 0)
  }

  def height_dfs_null_tr[A](node: Tree[A]): Int = {
    require(node != null)

    @annotation.tailrec
    def go(lt: List[Tree[A]], d: Int, z: Int): Int =
      lt match {
        case Nil => z
        case null :: tt => go(tt, d - 1, z)  // completed branch, ->up
        case Leaf(_) :: tt => go(tt, d, d max z)
        case Branch(l, r) :: tt => go(l :: r :: null :: tt, d + 1, z)
      }
    go(List(node), 0, 0)
  }

  def height_bfs_null_tr[A](node: Tree[A]): Int = {
    require(node != null)

    @annotation.tailrec
    def go(vt: Vector[Tree[A]], d: Int, z: Int): Int =
      vt match {  // could use (t: @unchecked) to suppress warning
        case null +: Vector() => z
        case null +: tt => go(tt :+ null, d + 1, z)  // completed level, ->down
        case Leaf(_) +: tt => go(tt, d, d max z)
        case Branch(l, r) +: tt => go(tt :+ l :+ r, d, z)
        // dotty: incorrect [E029] Pattern Match Exhaustivity Warning
        case _ => throw new MatchError(node)
      }
    go(Vector(node, null), 0, 0)
  }

  def height_dfs_scan1ahead_tr(node: Tree[Int]): Int = {
    require(node != null)

   /* Avoids the need for a sentinel by utilizing that the tree is _binary_:
    * - never pushes a left leaf onto stack
    * - therefore, any popped leaf was a right leaf
    * - therefore, popping a leaf indicates the completion of a branch
    */
    @annotation.tailrec
    def go(lt: List[Tree[Int]], d: Int, z: Int): Int =
      lt match {
        case Branch(Leaf(a), r) :: tt =>
          go(r :: tt, d + 1, (d + 1) max z)
        case Branch(l, r) :: tt =>
          go(l :: r :: tt, d + 1, z)
        case Leaf(a) :: tt =>
          go(tt, d - 1, d max z)
        case Nil => z
      }
    go(List(node), 0, 0)
  }

  def height_dfs_scan1ahead_i[A](node: Tree[A]): Int = {

    // TODO: migrate to Scala3 while ... do () quiet syntax
    var height = 0
    var depth = 0
    var nodes = List(node)
    while (nodes match {  // scala 2+3 do-while
      case h :: tt =>
        h match {
          case Branch(Leaf(_), r) =>  // never push left leaf
            height = (depth + 1) max height
            depth += 1
            nodes = r :: tt
          case Branch(l, r) =>  // push both child nodes
            depth += 1
            nodes = l :: r :: tt
          case Leaf(a) =>  // pop this right leaf
            height = depth max height
            depth -= 1
            nodes = tt
        }
        true
      case Nil => false
    }) ()  // scala 2+3 do-while
    height
  }
}
