package netropy.polyglot.adsp.trees

import scala.collection.{Iterable, Iterator}


trait TreeBreadthHeightWidth[+A, T[+A] <: Tree[A, T]] {
  this: Tree[A, T] =>  // generic tree type sufficient

  /**
    * Notes: iterator-based or [non-]tail-recursive implementations
    *
    * - breadth: any order (DFS) traversal using iterator for lazy summation
    *   after filtering.
    *
    * - height: pre-order DFS with List as stack (LIFO queue) and null as
    *   sentinel indicating the completion of a branch (List can store null).
    *
    * - width: level-order BFS with Vector as FIFO queue and null as sentinel
    *   indicating the completion of a level (Vector can store null).
    *
    * - breadth_r, height_r, width_r: the directly recursive decompositions
    *   as functions in the companion for comparison.
    */

  def breadth: Int =
    iterator.filter(_.isLeaf).map(_ => 1).sum  // iterators are lazy :-)

  def height: Int = {

    @annotation.tailrec
    def go(t: List[Tree[A, T]], d: Int, z: Int): Int = {
      //assert(t != null && d >= 0 && z >= 0)
      t match {
        case Nil => z
        case null +: t =>
          go(t, d - 1, z)  // completed branch, ->up
        case h +: t if h.isLeaf =>
          go(t, d, d max z)
        case h +: t =>
          go(h.children ++: null +: t, d + 1, z)  // ->down
        // dotty: incorrect [E029] Pattern Match Exhaustivity Warning
        case _ => throw new MatchError(t)
      }
    }
    go(List(this), 0, 0)
  }

  def width(level: Int): Int = {
    require(level >= 0)

    @annotation.tailrec
    def go(t: Vector[Tree[A, T]], l: Int, z: Int): Int = {
      //assert(t != null && l >= 0 && z >= 0)
      t match {
        case null +: t if l == 0 => z  // done
        case null +: Vector() => assert(l > 0); 0  // done, out of levels
        case null +: t =>
          go(t :+ null, l - 1, 0)  // completed level, reset width, ->down
        case h +: t =>
          go(t :++ h.children, l, z + 1)
        // dotty: incorrect [E029] Pattern Match Exhaustivity Warning
        case _ => throw new MatchError(t)
      }
    }
    go(Vector(this, null), level, 0)
  }
}

object TreeBreadthHeightWidth {

  /**
    * As exercise: directly recursive decompositions of the trait's methods
    */

  // TODO: unit-test also these functions

  // not: @annotation.tailrec
  def breadth_r[A, T[+A] <: Tree[A, T]](t: Tree[A, T]): Int =
    if (t.isLeaf) 1
    else t.children.map(breadth_r(_)).sum

  // not: @annotation.tailrec
  def height_r[A, T[+A] <: Tree[A, T]](t: Tree[A, T]): Int =
    if (t.isLeaf) 0
    else t.children.map(height_r(_)).max + 1

  // not: @annotation.tailrec
  def width_r[A, T[+A] <: Tree[A, T]](t: Tree[A, T], level: Int): Int = {
    require(level >= 0)

    if (level == 0) 1
    else t.children.map(width_r(_, level - 1)).sum  // no need for isLeaf test
  }
}
