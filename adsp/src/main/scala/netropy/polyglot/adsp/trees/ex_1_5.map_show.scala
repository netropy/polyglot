package netropy.polyglot.adsp.trees


object ex_1_5 {

  import ex_1_1.{Tree, Leaf, Branch}

  /*
   * map: Modifies each leaf's value with a given function.
   *
   * show: Renders a tree like "(a)", "[(a),(b)]", "[(a),[(b),(c)]]" etc.
   */

  /*
   * Notes: Algorithmic variants
   *
   * map_r:
   * show_r:
   *     recursive decomposition with pre-order traversal (DFS)
   *
   *     Same algorithm as `height_r`, see ex_1_4 for description.
   *
   * map_dfs_null_tr:
   * show_dfs_null_tr:
   *     tail-recursive, pre-order DFS with List as LIFO queue
   *
   *     Same algorithm as `height_dfs_null_tr`, see ex_1_4 for description.
   */

  // not: @annotation.tailrec
  def map_r[A, B](node: Tree[A])(f: A => B): Tree[B] = {
    require(node != null)

    node match {
      case Leaf(a) => Leaf(f(a))
      case Branch(l, r) => Branch(map_r(l)(f), map_r(r)(f))
    }
  }

  // not: @annotation.tailrec
  def show_r[A](node: Tree[A]): String = {
    require(node != null)

    node match {
      case Leaf(a) => s"($a)"
      case Branch(l, r) => s"[${show_r(l)},${show_r(r)}]"
    }
  }

  def map_dfs_null_tr[A, B](node: Tree[A])(f: A => B): Tree[B] = {
    require(node != null)

    @annotation.tailrec
    def go(lt: List[Tree[A]], z: List[Tree[B]]): Tree[B] =
      lt match {
        case Nil =>
          val t :: Nil = z : @unchecked  // intentional narrowing
          t
        case null :: tt =>
          val r :: l :: zz = z : @unchecked  // intentional narrowing
          go(tt, Branch(l, r) :: zz)  // completed branch, ->up
        case Leaf(a) :: tt =>
          go(tt, Leaf(f(a)) :: z)
        case Branch(l, r) :: tt =>
          go(l :: r :: null :: tt, z)
      }
    go(List(node), Nil)
  }

  def show_dfs_null_tr[A, B](node: Tree[A]): String = {
    require(node != null)

    @annotation.tailrec
    def go(lt: List[Tree[A]], z: List[String]): String =
      lt match {
        case Nil =>
          val s :: Nil = z : @unchecked  // intentional narrowing
          s
        case null :: tt =>
          val r :: l :: zz = z : @unchecked  // intentional narrowing
          go(tt, s"[$l,$r]" :: zz)  // completed branch, ->up
        case Leaf(a) :: tt =>
          go(tt, s"($a)" :: z)
        case Branch(l, r) :: tt =>
          go(l :: r :: null :: tt, z)
      }
    go(List(node), Nil)
  }
}
