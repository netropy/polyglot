package netropy.polyglot.adsp.trees

import scala.collection.{Iterable, Iterator}


trait TreeIterators[+A, T[+A] <: Tree[A, T]] {
  this: T[A] =>  // iterators require the actual, tree-implementing subtype

  /**
    * Notes:
    *
    * - Generic tree traversal applicable to any `k`-way tree:
    *   - class LevelOrderIterator: BFS with Vector as FIFO queue.
    *   - class PreOrderIterator: DFS with List as LIFO queue.
    *   - class PostOrderIterator: DFS with 2x List as stacks (LIFO queues):
    *     for the child nodes (to be visited next) and the parent nodes.
    *
    * - External iterators have no direct-recursive decomposition (unlike
    *   internally traversing folds or breadth/height/width functions).
    *
    * - The Iterators[B] do not capture their outer `this` but take `root` as
    *   parameter (contravariant position).  Hence, they are defined invariant
    *   in [B] (sufficient here) and do not capture covariant, outer [+A].
    *   Interestingly, Dotty (0.27.0-RC1) is more lenient and allows for [+B]
    *   (more stringent escape-analysis?).
    */

  def iterator: Iterator[T[A]] = iteratePreOrder

  def iterateLevelOrder: Iterator[T[A]] = new LevelOrderIterator(this)

  def iteratePreOrder: Iterator[T[A]] = new PreOrderIterator(this)

  def iteratePostOrder: Iterator[T[A]] = new PostOrderIterator(this)

  private class LevelOrderIterator[B](root: T[B])
      extends Iterator[T[B]] {

    private var current = Vector(root)

    override def hasNext: Boolean = current.nonEmpty

    override def next(): T[B] = {
      require(hasNext)

      val head +: tail = current : @unchecked  // intentional narrowing
      current = tail :++ head.children  // fifo
      head
    }
  }

  private class PreOrderIterator[B](root: T[B])
      extends Iterator[T[B]] {

    private var current = List(root)

    override def hasNext: Boolean = current.nonEmpty

    override def next(): T[B] = {
      require(hasNext)

      val head +: tail = current : @unchecked  // intentional narrowing
      current = head.children ++: tail  // lifo
      head
    }
  }

  /** Advances internal state lazily for space-efficiency. */
  private class PostOrderIterator[B](root: T[B])
      extends Iterator[T[B]] {

    private var current = List(root)
    private var later = List.empty[T[B]]

    override def hasNext: Boolean = current.nonEmpty && later.nonEmpty

    override def next(): T[B] = {
      require(hasNext)

      while (current.nonEmpty) {  // && current.head.isBranch
        val head +: tail = current : @unchecked  // intentional narrowing
        if (head.isLeaf) {
          current = tail
          return head
        }

        assert(head.isBranch)
        current = head.children ++: current  // lifo
        later = head ++: later  // lifo
      }

      val head +: tail = later : @unchecked  // intentional narrowing
      assert(head.isBranch)
      later = tail
      head
    }
  }
}
