package netropy.polyglot.adsp.trees

trait BinaryTreeShapeProperties[+A, T[+A] <: BinaryTree[A, T]] {
  this: BinaryTree[A, T] =>

  // TODO: unit-test

  def isLeftComb: Boolean =
    forall(n => n.isLeaf || n.degree == 2 || n.right.isLeaf)

  def isRightComb: Boolean =
    forall(n => n.isLeaf || n.degree == 2 || n.left.isLeaf)
}
