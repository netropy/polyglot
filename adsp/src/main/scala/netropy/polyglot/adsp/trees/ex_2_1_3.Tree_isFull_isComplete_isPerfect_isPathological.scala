package netropy.polyglot.adsp.trees

trait TreeShapeProperties[+A, T[+A] <: Tree[A, T]] {
  this: Tree[A, T] =>

  // TODO: add recursive decompositions

  def isFull: Boolean =
    forall { n => val d = n.degree; (d == 0 || d == arity) }

  /*
   // TODO: inefficient & inelegant
  def isComplete: Boolean = {
    iterateLevelOrder.foldLeft((arity, true)) {
      case ((d, z), n) =>
        if (!z) (d, z)
        else if (n.degree == d) (d, z)
        else if (n.degree == 0) (0, z)
        else (0, false)
    }._2
  }
   */

  def isComplete: Boolean = {

    @annotation.tailrec
    def go(t: List[Tree[A, T]], d: Int, z: Int): Boolean = {
      //assert(t != null && d >= 0)
      // TODO: inelegant, document: 2 value transitions of z
      t match {
        case Nil => true
        case null +: t =>
          go(t, d - 1, z)  // completed branch, ->up
        case h +: t if h.isLeaf =>
          z match {
            case y if y > 0 => (d == z) && go(t, d, z)
            case 0 => go(t, d, d)
            case y if y < 0 => (-d == z + 1) && go(t, d, -z)
          }
        case h +: t =>
          (h.degree == arity) && go(h.children ++: null +: t, d + 1, z)
        // dotty: incorrect [E029] Pattern Match Exhaustivity Warning
        case _ => throw new MatchError(t)
      }
    }
    go(List(this), 0, 0)
  }

  def isPerfect: Boolean = {

    @annotation.tailrec
    def go(t: List[Tree[A, T]], d: Int, z: Int): Boolean = {
      //assert(t != null && d >= 0 && z >= 0)
      t match {
        case Nil => true
        case null +: t =>
          go(t, d - 1, z)  // completed branch, ->up
        case h +: t if h.isLeaf =>
          (d == z || z == 0) && go(t, d, d)
        case h +: t =>
          (h.degree == arity) && go(h.children ++: null +: t, d + 1, z)
        // dotty: incorrect [E029] Pattern Match Exhaustivity Warning
        case _ => throw new MatchError(t)
      }
    }
    go(List(this), 0, 0)
  }

  def isPathological: Boolean =
    forall(_.degree <= 1)
}
