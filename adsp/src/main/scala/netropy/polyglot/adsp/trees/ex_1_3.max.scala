package netropy.polyglot.adsp.trees


object ex_1_3 {

  import ex_1_1.{Tree, Leaf, Branch}

  /* max: Returns the maximum element in a Tree[Int]. */

  /*
   * Notes: Algorithmic variants
   *
   * max_r:
   *     recursive decomposition with pre-order traversal (DFS)
   *
   * max_dfs_tr:
   *     tail-recursive, pre-order DFS with List as LIFO queue
   *
   * max_dfs_scan1ahead_tr:
   *     tail-recursive, pre-order DFS with Tree as LIFO queue
   *
   *     Applies the same algorithm as `size_dfs_scan1ahead_tr`: transforms
   *     the binary tree into a _right comb_ list; otherwise, just reduces it.
   */

  // not: @annotation.tailrec
  def max_r(node: Tree[Int]): Int = {
    require(node != null)

    node match {
      case Leaf(a) => a
      case Branch(l, r) => max_r(l) max max_r(r)
    }
  }

  def max_dfs_tr(node: Tree[Int]): Int = {
    require(node != null)

    @annotation.tailrec
    def go(lt: List[Tree[Int]], z: Int): Int =
      lt match {
        case Nil => z
        case Leaf(a) :: tt => go(tt, a max z)
        case Branch(l, r) :: tt => go(l :: r :: tt, z)
      }
    go(List(node), Int.MinValue)
  }

  def max_dfs_scan1ahead_tr(node: Tree[Int]): Int = {
    require(node != null)

    @annotation.tailrec
    def go(t: Tree[Int], z: Int): Int =
      t match {
        case Leaf(a) => a max z
        case Branch(Leaf(a), r) => go(r, a max z)
        case Branch(Branch(ll, lr), r) => go(Branch(ll, Branch(lr, r)), z)
      }
    go(node, Int.MinValue)
  }
}
