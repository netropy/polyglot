package netropy.polyglot.adsp.trees


/*
 * FPiS Exercises Chapter 3: Tree Recursion & Folding
 */

object ex_1_1 {

  /** A binary, full, non-empty tree that stores data in leaf nodes only.
    *
    * Notes:
    *
    * - Binary:
    *   Tree's arity == 2, maximum degree, i.e., number of children.
    *
    * - Full (a.k.a. locally complete):
    *   All nodes have a degree of exactly 0 or the tree's arity.
    *
    * - Non-empty, (multi-way) Rose Tree:
    *   Always having a Node (Leaf/Branch) with stored data; may have children.
    *   - Here: required by fullness, branches cannot have empty children.
    *   - Generally: makes a comonad, counit/copoint always has data to return.
    *   - multi-way Rose Tree
    *     case class Tree[A](tip: A, sub: List[Tree[A]])
    *     https://gist.github.com/tonymorris/0e5f0c672c3d3394baee
    *     https://blog.higher-order.com/blog/2015/10/04/scala-comonad-tutorial-part-2/
    *
    * - Leaf Tree, Leaf-Labeled Rose Tree:
    *   Stores data in leaves only:
    *   - data iteration: no difference between pre-/in-/post-order traversal
    *   - has map and fold; many functions decompose easily over subtrees
    *   - can be used to implement other data types (list, set, bag)
    *     https://101wiki.softlang.org/Concept:Rose_tree
    *     https://doi.org/10.1016/0167-6423(95)00026-7
    */
  sealed trait Tree[+A]

  final case class Leaf[+A](data: A) extends Tree[A]

  final case class Branch[+A](left: Tree[A], right: Tree[A]) extends Tree[A] {
    require(left != null)
    require(right != null)
  }

  object Tree {

    /** Returns the values in a left-comb tree (all right nodes are leaves). */
    def apply[A](a: A, as: A*): Tree[A] =
      as.foldLeft(Leaf(a): Tree[A])((b, a) => new Branch(b, new Leaf(a)))
  }
}
