package netropy.polyglot.adsp.trees

import scala.collection.immutable.BitSet


/** An immutable, vector-based, traversable binary tree with data in all nodes.
  *
  * Notes:
  *
  * This implementation is a (failed) experiment:
  * + produce an immutable & down- and up-traversable, binary tree
  * + store binary tree in an array/vector, model a node mainly as an index
  * + gather a feel for _implementing_ an immutable data structure
  * + using immutable Vector with structural sharing and fast append/prepend
  * + presumably fast and somewhat memory-local traversal of values
  * + clever trick to use a 1-indexed vector to always have values(0) as unit
  * - storing binary trees in array is fast but space-inefficient (if sparse)
  * - space-inefficiency aggravated here by nodes not sharing the same vector
  * - so, bottom-line space-efficiency depends upon vector's ability to share
  * - also need a 2nd data structure: bitset to indicate which values are set
  * - some code complexity to grow and shrink the values vector
  * - significant code complexity to update data, and the left, right nodes
  * - immutability requires to create many intermediate nodes and vectors
  * - 3 methods that have exponential (+mutual) recursion (2x calls/call)
  *
  * @see [[https://en.wikipedia.org/wiki/Binary_tree#Arrays]]
  */
final case class VectorBinaryTree[+A]
    (values: Vector[A], set: BitSet, index: Int)
    extends BinaryTree[A, VectorBinaryTree]
    with UpTraversableTree[A, VectorBinaryTree]
    with TreeIterators[A, VectorBinaryTree]
    with TreeBreadthHeightWidth[A, VectorBinaryTree]
    with TreeShapeProperties[A, VectorBinaryTree]
    with BinaryTreeShapeProperties[A, VectorBinaryTree] {

  // index == 0 used for the unit A value
  require(values != null && values.size > 0)
  require(set != null && !set(0) && set(1))
  require(values.size > set.max)
  require(index > 0)

  type T[+A] = VectorBinaryTree[A]

  override def data: A = { assert(set(index)); values(index) }

  override def dataOption: Option[A] = Some(data)  // allow for Some(null)

  override def isDataNode: Boolean = true

  // use values as 1-indexed vector, keep values(0) as the unit value
  private def leftI(i: Int): Int = 2 * i
  private def rightI(i: Int): Int = 2 * i + 1
  private def parentI(i: Int): Int = i / 2

  override def left: T[A] = {

    val lIndex = leftI(index)
    if (!set(lIndex)) null
    else VectorBinaryTree(values, set, lIndex)
  }

  override def right: T[A] = {

    val rIndex = rightI(index)
    if (!set(rIndex)) null
    else VectorBinaryTree(values, set, rIndex)
  }

  override def parent: T[A] = {

    if (index == 1) null
    else VectorBinaryTree(values, set, parentI(index))
  }

  /** Returns the node for the given index i (whose value must not be set).
    * Grows the values vector if necessary.
    */
  private def getOrMake(i: Int): T[A] = {

    require(0 < i)
    require(i <= rightI(values.size - 1))
    assert(!set(i))

    if (i < values.size) {
      VectorBinaryTree(values, set, i)
    } else {
      val appended =
        values :++ Vector.fill(values.size + 1)(values(0))
      VectorBinaryTree(appended, set, i)
    }
  } ensuring { r =>
      (i < r.values.size)
  }

  /** Returns a copy of this node with data, left, right subtrees cleared.
    * Shrinks the values vector if possible.
    */
  // not: @annotation.tailrec, exponential recursion (2x calls)
  private def cleared: T[A] = {

    if (!set(index)) this
    else {
      val updated =
        VectorBinaryTree(values.updated(index, values(0)), set - index, index)
      val lUpdated =
        updated.leftOption.fold(updated)(_.cleared.parent)
      val lrUpdated =
        lUpdated.rightOption.fold(lUpdated)(_.cleared.parent)

      // shrinking lags behind growing (hysteresis)
      val allowance = rightI(set.max)
      if (values.size + 1 <= allowance) lrUpdated
      else {
        val shrunk =
          lrUpdated.values.take(allowance)
        VectorBinaryTree(shrunk, lrUpdated.set, lrUpdated.index)
      }
    }
  } ensuring { r =>
      (r.values.size > set.max)
  }

  override def updatedData[A1 >: A](data: A1): T[A1] =
    VectorBinaryTree(values.updated(index, data), set + index, index)

  override def updatedChildren[A1 >: A](nodes: Seq[T[A1]]): T[A1] = {

    require(nodes.size <= 2)
    nodes match {
      case Seq() => updatedLeft(null).updatedRight(null)
      case Seq(left) => updatedLeft(left).updatedRight(null)
      case Seq(left, right) => updatedLeft(left).updatedRight(right)
    }
  }

  override def updatedChildrenOption[A1 >: A]
      (nodes: Seq[Option[T[A1]]]): T[A1] = ???
    //updatedChildren(nodes.map(_.fold(null)(identity)))  TODO

  // not: @annotation.tailrec, mutual and exponential recursion (2x calls)
  override def updatedLeft[A1 >: A](node: T[A1]): T[A1] = {

    // scala2 needs explicit upcast, dotty upcasts automatically
    val lCleared: T[A1] =
      leftOption.fold(this)(_.cleared.parent)

    Option(node).fold(lCleared) { n =>
      val lMade =
        lCleared.getOrMake(leftI(index))
      val lUpdated =
        lMade.updatedData(node.data)
      val llUpdated =
        n.leftOption.fold(lUpdated)(n => lUpdated.updatedLeft(n))
      val llrUpdated =
        n.rightOption.fold(llUpdated)(n => llUpdated.updatedRight(n))
      llrUpdated.parent
    }
  } ensuring { r =>
    r != null && r.index == index
  }

  override def updatedLeftOption[A1 >: A](node: Option[T[A1]]): T[A1] = ???
    //updatedLeft(node.fold(null)(identity))   TODO

  // not: @annotation.tailrec, mutual and exponential recursion (2x calls)
  override def updatedRight[A1 >: A](node: T[A1]): T[A1] =  {

    // scala2 needs explicit upcast, dotty upcasts automatically
    val rCleared: T[A1] =
      rightOption.fold(this)(_.cleared.parent)

    Option(node).fold(rCleared) { n =>
      val rMade =
        rCleared.getOrMake(rightI(index))
      val rUpdated =
        rMade.updatedData(node.data)
      val rlUpdated =
        n.leftOption.fold(rUpdated)(n => rUpdated.updatedLeft(n))
      val rlrUpdated =
        n.rightOption.fold(rlUpdated)(n => rlUpdated.updatedRight(n))
      rlrUpdated.parent
    }
  } ensuring { r =>
    r != null && r.index == index
  }

  override def updatedRightOption[A1 >: A](node: Option[T[A1]]): T[A1] = ???
    //updatedRight(node.fold(null)(identity))   TODO

  override def isBranch: Boolean = !isLeaf

  override def isLeaf: Boolean = left == null && right == null
}


object VectorBinaryTree
    extends BinaryTreeFactory[VectorBinaryTree] {

  def unit(a: Int): VectorBinaryTree[Int] =
    VectorBinaryTree(Vector(0, a), BitSet(1), 1)

  def unit(a: Long): VectorBinaryTree[Long] =
    VectorBinaryTree(Vector(0L, a), BitSet(1), 1)

  def unit(a: Double): VectorBinaryTree[Double] =
    VectorBinaryTree(Vector(0.0d, a), BitSet(1), 1)

  // TODO: why is this not working (with dotty)?
  //def unit[A <: AnyRef | Null](a: A): VectorBinaryTree[A] =
  //  VectorBinaryTree(Vector[A](null, a), BitSet(1), 1)
  //                             ^^^^
  //   Found:    Null
  //   Required: A
  //   where:    A is a type in method unit with bounds <: AnyRef | Null

  /** Creates a binary tree with a Vector-element default and a node value.
    *
    * The internal Vector which holds all node data values requires a default
    * value `a: A` for when nodes are deleted or for when the vector grows.
    * This default value does not have to be distinct, but ideally it has
    * low memory requirements (such as `null`, `Nil` or a sentinel value).
    */
  // TODO: not working with Scala2
  // both method unit of type [A](a: A):... and [A](default: A)(a: A):...
  // match argument types (A)
  def unit0[A]
      (default: A)
      (a: A): VectorBinaryTree[A] =
    VectorBinaryTree(Vector(default, a), BitSet(1), 1)

  /** Creates a binary tree with a node and Vector-element default value.
    *
    * Use `unit(default)(a)` or `unit(default).updatedData(a)` if a value
    * other than `a` should be used as the Vector-element default.
    */
  def unit[A]
      (a: A): VectorBinaryTree[A] =
    unit0(a)(a)
}
