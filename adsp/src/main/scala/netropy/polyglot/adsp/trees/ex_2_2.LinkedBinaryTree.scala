package netropy.polyglot.adsp.trees


// TODO: write specialized LinkedBinaryLeaf,  LinkedBinaryBranch, apply() ?

/** An immutable, linked, binary tree with data in all nodes. */
final case class LinkedBinaryTree[+A]
    (data: A, left: LinkedBinaryTree[A], right: LinkedBinaryTree[A])
    extends BinaryTree[A, LinkedBinaryTree]
    with TreeIterators[A, LinkedBinaryTree]
    with TreeBreadthHeightWidth[A, LinkedBinaryTree]
    with TreeShapeProperties[A, LinkedBinaryTree]
    with BinaryTreeShapeProperties[A, LinkedBinaryTree] {

  type T[+A] = LinkedBinaryTree[A]

  override def dataOption: Option[A] = Some(data)

  override def isDataNode: Boolean = true

  override def updatedData[A1 >: A](data: A1): T[A1] =
    LinkedBinaryTree(data, left, right)

  override def updatedChildren[A1 >: A](nodes: Seq[T[A1]]): T[A1] = {

    require(nodes.size <= 2)
    nodes match {
      case Seq() => LinkedBinaryTree(data, null, null)
      case Seq(left) => LinkedBinaryTree(data, left, null)
      case Seq(left, right) => LinkedBinaryTree(data, left, right)
    }
  }

  override def updatedChildrenOption[A1 >: A]
      (nodes: Seq[Option[T[A1]]]): T[A1] = ???
    //updatedChildren(nodes.map(_.fold(null)(identity)))  TODO

  override def updatedLeft[A1 >: A](node: T[A1]): T[A1] =
    LinkedBinaryTree(data, node, right)

  override def updatedLeftOption[A1 >: A](node: Option[T[A1]]): T[A1] = ???
    //updatedLeft(node.fold(null)(identity))  TODO

  override def updatedRight[A1 >: A](node: T[A1]): T[A1] =
    LinkedBinaryTree(data, left, node)

  override def updatedRightOption[A1 >: A](node: Option[T[A1]]): T[A1] = ???
    // updatedRight(node.fold(null)(identity))  TODO

  override def isBranch: Boolean = !isLeaf

  override def isLeaf: Boolean = left == null && right == null
}

object LinkedBinaryTree
    extends BinaryTreeFactory[LinkedBinaryTree] {

  def unit[A]
      (a: A): LinkedBinaryTree[A] =
    LinkedBinaryTree(a, null, null)
}
