package netropy.polyglot.adsp.trees


object ex_1_6 {

  import ex_1_1.{Tree, Leaf, Branch}

  /*
   * fold: Combines the tree's data according to the tree's structure.
   *
   * See discussion of fold's signature and semantics:
   * .../ex_1_6.r.fold_for_trees.md
   */

  /*
   * Notes: Algorithmic variants
   *
   * fold_r:
   *     recursive decomposition with pre-order traversal (DFS)
   *
   * fold_dfs_null_tr:
   *     tail-recursive, pre-order DFS with List as LIFO queue
   *
   *     For traversal, applies the same algorithm as `height_dfs_null_tr`:
   *     stores `null` as sentinel indicating the completion of a branch.
   *     Holds the partial results, i.e., subtree folds, in another List.
   *     Utilizes:
   *     - availability of null as type-conform, distinct value
   *     - that List can store nulls and has an efficient prepend operation
   */

  // not: @annotation.tailrec
  def fold_r[A, B](node: Tree[A])(f: A => B)(g: (B, B) => B): B = {
    require(node != null)

    node match {
      case Leaf(a) => f(a)
      case Branch(l, r) => g(fold_r(l)(f)(g), fold_r(r)(f)(g))
    }
  }

  def fold_dfs_null_tr[A, B](node: Tree[A])(f: A => B)(g: (B, B) => B): B = {
    require(node != null)

    @annotation.tailrec
    def go(lt: List[Tree[A]], z: List[B]): B =
      lt match {
        case Nil =>
          val t :: Nil = z : @unchecked  // intentional narrowing
          t
        case null :: tt =>
          val r :: l :: zz = z : @unchecked  // intentional narrowing
          go(tt, g(l, r) :: zz)  // completed branch, ->up
        case Leaf(a) :: tt =>
          go(tt, f(a) :: z)
        case Branch(l, r) :: tt =>
          go(l :: r :: null :: tt, z)
      }
    go(List(node), Nil)
  }
}
