package netropy.polyglot.adsp.trees


object ex_1_7 {

  import ex_1_1.{Tree, Leaf, Branch}
  //import ex_1_6.{fold_r => fold}
  import ex_1_6.{fold_dfs_null_tr => fold}

  def size_f[A](node: Tree[A]): Int =
    fold(node)(_ => 1)((l, r) => l + r + 1)

  def max_f(node: Tree[Int]): Int =
    fold(node)(identity)((l, r) => l max r)

  def height_f[A](node: Tree[A]): Int =
    fold(node)(_ => 0)((l, r) => (l max r) + 1)

  def map_f[A, B](node: Tree[A])(f: A => B): Tree[B] =
    fold(node)(a => Tree(f(a)))(Branch(_, _))

  def show_f[A, B](node: Tree[A]): String =
    fold(node)(a => s"($a)")((l, r) => s"[$l,$r]")
}
