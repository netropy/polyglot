# polyglot/adsp/trees

#### 1 TODO...

##### 1.6 Design a combinator fold for trees

// Generalize size, maximum, depth, and map, writing a new function fold
// that abstracts over their similarities. Reimplement them in terms of
// this more general function. Can you draw an analogy between this fold
// function and the left and right folds for List?

fold: Maps a tree by operators given for the leaf and branch node types.

Notes: fold's signature <=> data constructors

- For lists, foldRight (foldLeft, foldMap) proved to be the powerful
  combinators, in terms of which most other operators can be expressed.

  The reason for this expressiveness is that by itself, `fold` preserves
  an input list's structure when mapping it.

  Proof:  When given Nil as start value and Cons as binary operator,
  foldRight reconstructs any input list:
    l == foldRight(l, Nil)(Cons)

- Likewise, a fold for trees should reconstruct a tree:
    t == fold(t)(...tree constructors...)

  Specifically, these operator arguments must be supported:
  ```
    (a: A) => Leaf(a)                         // called for leaf nodes
    (l: Tree[A], r: Tree[A]) => Branch(l, r)  // called for branch nodes
  ```

- Since fold should allow mapping to any type B (not just Tree[A]):
  ```
    def fold[A, B]
        (t: Tree[A])
        (ifLeaf: A => B)
        (ifBranch: (B, B) => B): B
  ```

- Semantics: `fold` recursively invokes the respective `ifLeaf` or
  `ifBranch` operators for each node in the tree, and so accumulates a
  result value.

=> The fold combinator can be generalized to an algebraic data type T:
   - `fold` takes an operator for each of the `T`'s data constructors
   - so that all domain values can be handled
   - in the operator's signature, `T` is replaced with a generic `B`
   - `fold` called with `T`'s data constructors is a fixpoint function.

- Category theory terms: _fold_ is a
  [Catamorphism](https://en.wikipedia.org/wiki/Catamorphism)
