# polyglot/adsp/Trees

### Trees: Functional Programming Exercises, Code Experiments, Notes

----------------------------------------------------------------------

TODO:

Motivation: Understanding of
- _functional LinkedTrees_ with inherited combinators
  from Foldable, Functor, Applicative, Traversal, and Monad.
- the usage, expressivenes and implementation of the core
  combinators.
- Data type design with Scala's OOP vs FP vs GADT Scala3 language features.

This project consists of a _tree of exercises_ that expand on chapter 3 (and
10, 11, 12) of the _"Red Book"_,
[Functional Programming in Scala](https://www.manning.com/books/functional-programming-in-scala),
written by Paul Chiusano and Rúnar Bjarnason.

The exercises from the first 9 chapters are also known as
[Scala Exercises](https://www.scala-exercises.org/fp_in_scala/),
published as by [47 Degrees](https://www.47deg.com) with the same numeration
(plus minor changes).

See also the _"Blue Book"_
[Companion Booklet to FPiS](http://blog.higher-order.com/blog/2015/03/06/a-companion-booklet-to-functional-programming-in-scala/)
for chapter notes, hints, answers.

---

The _Red Book_ served as a great _starting point_ for the exercises here.  The
given code solutions occasionally differ from the answers in the _Blue Book_.

Additions:
- Compared modeling LinkedTrees as: OOP vs FP vs FP-with-Nil-Punning vs Scala3
  `enum` data type.
- Mixed in exercises from chapters 10, 11, and 12 (on Foldable, Functor,
  Applicative, Traversal, Monad).
- Added combinators from the related CATS APIs.
- Alternative code versions with function name suffixes:
  - `_r`/`_tr`/`_i` [non-] vs tail-recursive vs iterative,
  - `_fm`/`_f` whether based on `fold{Map}`, `fold`
  - insisted on stacksafe (`@tailrec`) solutions almost everywhere.

Note that the core FP types (Foldable, Functor, etc) are only discussed here
in the specific _Tree context_.  Future exercises under [../../fp/](../../fp/)
will cover the Red Book's chapters 10, 11, 12.

[SE_ch3]: https://www.scala-exercises.org/fp_in_scala/functional_data_structures

----------------------------------------------------------------------

Quick links: \
[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/)

| FPiS / Scala Exercises | Here |
|---|---|
| | [___Q: 1 Tree Recursion & Folding___](ex_1.q.tree_recursion_and_folding.md) |
| | [ex_1_1.BinaryLeafLabeledTree.scala](../src/main/scala/netropy/polyglotxd/adsp/trees/ex_1_1.BinaryLeafLabeledRoseTree.scala) |
| [Ex 3.25 `size`][SE_ch3] | [ex_1_2.size.scala](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_2.size.scala) |
| [Ex 3.26 `max`][SE_ch3] | [ex_1_3.max.scala](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_3.max.scala) |
| [Ex 3.27 `depth`][SE_ch3] | [ex_1_4.height.scala](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_4.height.scala) |
| [Ex 3.28 `map`][SE_ch3] | [ex_1_5.map_show.scala](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_5.map_show.scala) |
| [Ex 3.29 `fold`][SE_ch3] | [ex_1_6.fold.scala](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_6.fold.scala) |
| [Ex 3.29 `fold`][SE_ch3] | [ex_1_7.size_max_height_map_show.scala](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_7.size_max_height_map_show.scala) |

Resources: _Blue Book_ and _Scala Exercises_ repositories \
<https://github.com/fpinscala/fpinscala> \
<https://github.com/scala-exercises/scala-exercises>

Problem/language/library features: \
[tags](tags.md)

[Up](./README.md)
