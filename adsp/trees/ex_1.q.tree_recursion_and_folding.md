# polyglot/adsp/Trees

### 1 Tree Recursion & Folding

----------------------------------------------------------------------

TODO:

___Motivation:___

Typically, the definition of a data type Tree (or other GADTs) is given as a
3-liner: a type constructor, two data constructors.

The design space is much larger, though -- which this exercise is to explore a
bit.

Starting with the big decisions:  Write Tree as a
- stand-alone type (i.e., not as a member of an existing OO type hierarchy or
  set of typeclasses),
- functional data structure (immutability, ideally with use of data sharing).

There's still to be decided:
- Model Tree as OO type or FP type?
  - Define subtypes for the values?  Expose them?
  - Model core features as methods or functions?  Utilize overriding or
    pattern-matching?
  - Confine value, function definitions to class/companion scope or export
    them at package-level?
  - Use Scala3's `enum` for singleton and parameterized values?  Use for
    values only or also add behaviour?
- Nil-Punning: Disallow, ignore, or utilize `null` as a value?
- Error handling: `require` arguments and throw exceptions or map to monadic
  values?  Which exception or error types?
- How to access data: directly call fields/methods, support for pattern
  matching, or through accessor functions, or all above?
- Expose type's primary constructor? Which constructors to add to companion?

Later, when adding more methods/functions:  Allow for
- non-strict APIs, utilize by-name parameters?
- partial application, multiple parameter trees?
- recursion, tail-recursion?

(Some Scala style guides forbid some of the above for the project or company.)

___Task:___

Exploring some of the design space, define a linked tree data type as:
1. Traditional OOP type with core accessor methods and constructors
2. Functional type & data constructors with a core set of external functions
3. Functional type, but all values/functions scoped, use of `null` as value
4. Scala3 `enum` type with scoped methods/functions

Provide essential scaladoc (i.e., document exceptions so that a simple
lines-of-code count also indicates usage complexity).

Discuss the designs 1..4).

Unit-test (and so document) the error behaviour of functions `tail`, `head`
for designs 1..4), see: \
[Exercise 3.2, FPiS](https://www.scala-exercises.org/fp_in_scala/functional_data_structures)

Quick links: \
[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/),
[Scala3 sources](../src/main/scala3/netropy/polyglot/adsp/trees/),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/),
[Scala3 tests](../src/test/scala3/netropy/polyglot/adsp/trees/)

----------------------------------------------------------------------

#### 1.1 Define a minimal, binary tree type for recursion and folding.

For simplicity, the tree instances should
- be immutable (functional data type with constructors and accessor methods);
- be full, that is, nodes are either leaves or have exactly two children;
- be non-empty, that is, consist of at least one leaf node (Rose Tree);
- store data in the leaf nodes only, which removes the difference between
  pre-/in-/post-order traversal strategies w.r.t. iterating over the data.

Provide some scaladoc, with links to Rose Trees, and unit tests.

[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_1.BinaryLeafLabeledTree.scala),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/ex_1_1.BinaryLeafLabeledTree.scala)

#### 1.2 Write a function `size`.

```
  /** Returns the number of nodes (leaves and branches) in a tree. */
  def size[A](node: Tree[A]): Int
```

Experiment with recursive and tail-recursive algorithms (as practical),
indicate versions by function name suffixes `_{r,tr}`.

[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_2.size.scala),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/ex_1_2.size.scala)

#### 1.3 Write a function `max` for `Tree[Int]`.

```
  /** Returns the maximum element in a Tree[Int]. */
  def max(node: Tree[Int]): Int
```

Experiment with recursive and tail-recursive algorithms (as practical),
indicate versions by function name suffixes `_{r,tr}`.

[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_3.max.scala),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/ex_1_3.max.scala)

#### 1.4 Write a function `height`.

(In FPiS exercise 3.27 this function is called `depth`, which is inaccurate,
since the _depth_ of a node is the length of its path to the root.)

```
  /** Returns the length of the longest downward path to a leaf. */
  def height[A](node: Tree[A]): Int
```

Experiment with recursive and tail-recursive algorithms (as practical),
indicate versions by function name suffixes `_{r,tr}`.

[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_4.height.scala),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/ex_1_4.height.scala)

#### 1.5 Write functions `map`, `show`.

```
  /** Modifies each leaf's value with a given function. */
  def map_r[A, B](node: Tree[A])(f: A => B): Tree[B]

  /** Renders a tree like "(a)", "[(a),(b)]", "[(a),[(b),(c)]]" etc. */
  def show_r[A](node: Tree[A]): String
```

Experiment with recursive and tail-recursive algorithms (as practical),
indicate versions by function name suffixes `_{r,tr}`.

[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_5.map_show.scala),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/ex_1_5.map_show.scala)

#### 1.6 Define a function `fold` analog to `foldRight` on _LinkedList_.

Discuss _fold's_ signature and semantics

[Remarks](ex_1_6.r.fold_for_trees.md),
[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_6.fold.scala),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/ex_1_6.fold.scala)

#### 1.7 Rewrite functions `size`, `max`, `height`, `map`, `show` in terms of a _fold_.

See Ex 1.2-1.5 for signatures.  Use function name suffix `_f`.

[Scala sources](../src/main/scala/netropy/polyglot/adsp/trees/ex_1_7.size_max_height_map_show.scala),
[Scala tests](../src/test/scala/netropy/polyglot/adsp/trees/ex_1_7.size_max_height_map_show.scala)

[Up](./README.md)
