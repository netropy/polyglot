name := "polyglot.testing.scalatest"
organization := "netropy"
description := "Example, Scala, Scala3, ScalaTest, sbt, gradle, maven"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

// OK: Scala 3, 2.13, 2.12
//scalaVersion := "2.12.20"
//scalaVersion := "2.13.15"
scalaVersion := "3.3.4"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  //"-Ytasty-reader",  // scala2.13
  //"-explaintypes",  // scala2
  //"-explain",  // scala3
)

// Resolvers (pre- or user-defined)
// https://www.scala-sbt.org/1.x/docs/Resolvers.html
//resolvers += DefaultMavenRepository  // inluded by default
//resolvers += Resolver.mavenLocal  // ~/.m2

// ScalaTest
// https://mvnrepository.com/artifact/org.scalatest/scalatest
//
// scalac 3/2.13/2.12 + scalatest_3/2.13/2.12
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.19" % "test"
//
// scalac 3 + scalatest_2.13
// 2022-11, 2021-11: PROBLEM: No given instance of type ... was found
//libraryDependencies += "org.scalatest" % "scalatest_2.13" % "3.2.14" % "test"
//
// scalac 2.13 + scalatest_3
// 2022-11, 2021-11: PROBLEM: Unsupported Scala 3 inline macro method
//libraryDependencies += "org.scalatest" % "scalatest_3" % "3.2.14" % "test"

// only use a single thread for building
//parallelExecution := false
// execute tests in the _current_ project serially (default parallel)
Test / parallelExecution := false

// fork a new JVM for 'run' and 'Test/run'
//fork := true
// fork a new JVM for 'Test/run', but not 'run'
//Test / fork := true
// add a JVM option to use when forking a JVM for 'run'
//javaOptions += "-Xmx8G"

//Test / testOptions := Seq(Tests.Filter(s => s.endsWith("Test")))

// print the output of tests immediately instead of buffering
//Test / logBuffered := false
