package netropy.polyglot.testing.scalatest

import netropy.polyglot.testing.Hello

// ScalaTest 3.1.0 reorganized names
import org.scalatest.Assertions._
import org.scalatest.GivenWhenThen
import org.scalatest.diagrams.Diagrams
import org.scalatest.featurespec.AnyFeatureSpec
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.propspec.AnyPropSpec
import org.scalatest.wordspec.AnyWordSpec

/** Unit-tests greet(): parametrized/property test with data from table. */
class HelloPropSpecTableData
    extends AnyPropSpec
    with TableDrivenPropertyChecks
    with Diagrams {

  val strings = Table("string",
    "", "a", "aa", "ሇ츨ᄕ", """AA"A"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA""")

  property("Hello's greeting starts with 'Hello'") {
    forAll(strings) { s =>
      assert(Hello.greet(s).startsWith("Hello"))
    }
  }

  property("Hello's greeting composes") {
    forAll(strings) { s =>
      assertResult(s"Hello $s") { Hello.greet(s) }
    }
  }
}

/** Unit-tests greet(): fixed data, flat, TDD/function-style test. */
class HelloFunSuite extends AnyFunSuite with Diagrams {

  test("Hello's greeting starts with 'Hello'") {
    assert(Hello.greet("You").startsWith("Hello"))
  }

  test("Hello's greeting composes") {
    assertResult(s"Hello You") { Hello.greet("You") }
  }
}

/** Unit-tests greet(): fixed data, flat, BDD/spec-style test with a DSL. */
class HelloFlatSpec extends AnyFlatSpec with Diagrams {

  "Hello's greeting" should "start with 'Hello'" in {
    assert(Hello.greet("You").startsWith("Hello"))
  }

  it must "compose" in {
    assertResult(s"Hello You") { Hello.greet("You") }
  }
}

/** Unit-tests greet(): fixed data, flat, acceptance-style test. */
class HelloFeatureSpec extends AnyFeatureSpec with GivenWhenThen with Diagrams {

  info("As a person, I'd always like to be greeted properly")

  Feature("The always friendly 'Hello' Greeter") {
    Scenario("Somebody calls the greet() function") {
      Given("any day of the week")
      When("passed any argument")
      Then("the greeting should start with 'Hello'")
      assert(Hello.greet("You").startsWith("Hello"))
      And("the greeting should compose")
      assertResult(s"Hello You") { Hello.greet("You") }
    }
  }
}

/** Unit-tests greet(): fixed data, nestable, BDD/spec-style test. */
class HelloFunSpec extends AnyFunSpec with Diagrams {

  describe("Hello's greeting") {
    describe("for any argument") {
      describe("on any given day") {
        it("should start with 'Hello'") {
          assert(Hello.greet("You").startsWith("Hello"))
        }

        it("should compose") {
          assertResult(s"Hello You") { Hello.greet("You") }
        }
      }
    }
  }
}

/** Unit-tests greet(): fixed data, nestable, BDD/spec-style test with a DSL. */
class HelloWordSpec extends AnyWordSpec with Diagrams {

  "Hello's greeting" when {
    "passed any argument" when {
      "the weekday ends in 'y'" should {
        "should start with 'Hello'" in {
          assert(Hello.greet("You").startsWith("Hello"))
        }

        "should compose" in {
          assertResult(s"Hello You") { Hello.greet("You") }
        }
      }
    }
  }
}
