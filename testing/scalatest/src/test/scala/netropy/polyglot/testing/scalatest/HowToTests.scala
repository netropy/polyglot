package netropy.polyglot.testing.scalatest

// ScalaTest 3.1.0 reorganized names
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.propspec.AnyPropSpec
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.diagrams.Diagrams

/** Code example: TDD/function-style unit test, also see HelloTests. */
class HowToFunSuite extends AnyFunSuite with Diagrams {

  test("some pending test...") (pending)

  ignore("some ignored test") { fail("too bad") }
}

/** Code example: BDD/spec-style unit test w/ functions, also see HelloTests. */
class HowToFunSpec extends AnyFunSpec with Diagrams {

  describe("some test") {
    it("defined as pending...") (pending)

    ignore("declared as ignored") { fail("too bad") }
  }
}

/** Code example: BDD/spec-style unit test using a DSL, also see HelloTests. */
class HowToFlatSpec extends AnyFlatSpec with Diagrams {

  "a test" can "be defined as pending..." in (pending)

  ignore can "be declared as ignored" in { fail("too bad") }
}

/** Code example: parametrized/property test with data from a table. */
class HowToPropSpecTableData
    extends AnyPropSpec
    with TableDrivenPropertyChecks
    with Diagrams {

  val strings = Table("string",
    "", "a", "aa", "ሇ츨ᄕ", """AA"A"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA""")

  property("Assertion: String concatenation is associative") {
    // cannot pass multiple arguments or tuple
    // forAll(strings, strings, strings) { (s, t, u)
    // forAll((strings, strings, strings)) { (s, t, u)
    forAll(strings) { s =>
      forAll(strings) { t =>
        forAll(strings) { u =>
          // cannot just evaluate to boolean, must state assertion
          // ((s + t) + u) == (s + (t + u))
          assert(((s + t) + u) == (s + (t + u)), "not associative")
        }
      }
    }
  }

  property("some pending test...") (pending)

  ignore("some ignored test") { fail("too bad") }
}
