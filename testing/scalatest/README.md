# polyglot/testing/scalatest

#### Topics: A small, cloneable, _standalone_ build project

- _Source + Unit Test Code_ checking out features and usage of
  [ScalaTest](https://www.scalatest.org)
- _Project Code_ for each build tool -
  [sbt](https://www.scala-sbt.org),
  [Maven](https://maven.apache.org),
  [Gradle](https://gradle.org) -
  any of which can be removed
- _Notes_ on the compatibility of build tools, language, and library versions
- _Notes + Code Experiments_ on the used libraries

Quick links: \
[scala sources](src/main/scala/netropy/polyglot/testing/),
[scala tests](src/test/scala/netropy/polyglot/testing/scalatest/)

Also see: \
[../scalatest\_scalacheck](../scalatest_scalacheck/README.md),
[../scalatest\_scalamock](../scalatest_scalamock/README.md),
[../Summary: ScalaTest](../Notes_unit_test_frameworks.md#summary-scalatest)

#### Summary: {sbt,maven,gradle} X scala{2,3} X {scalatest}

[ScalaTest](https://index.scala-lang.org/scalatest/scalatest)
is available Scala as 2.10..13 and 3.x binaries.

| 2022-11 | 2.13 scalac, scala-library | 3.2 scalac (1), scala3-library |
|:--|:--:|:--:|
| scalatest\_2.13 3.2.14 (2) | OK | Errors (5) |
| scalatest\_3 3.2.14 (2) | Errors (3)(4) | OK |

(1) 2022-11, 2021-11: PROBLEM: Gradle really runs _scalac 2.x <- [zinc] <-
scala plugin_.  No solution yet how to configure running _scalac 3.x_.

(2) 2022-11, 2021-11: ___Problems with use of ScalaTest from Gradle 6.x:___
- Only the _2.13 binary_ of the ScalaTest library can be imported,
- and only up to ScalaTest version _3.2.9 from 2019_, see details down below.

(3) 2022-11: scalac 2.13 can consume Scala3 libs via _-Ytasty-reader_, see
[TASTy](https://docs.scala-lang.org/scala3/guides/tasty-overview.html).

(4) 2022-11, 2021-11: Problems with cross-platform use 2.13 <- 3, see comments
in build.sbt.

(5) 2022-11, 2021-11: Problems with cross-platform use 2.13 -> 3, see comments
in build.sbt.

#### Notes: scalatest

Links: \
[ScalaTest](https://www.scalatest.org),
[@github](https://github.com/scalatest/scalatest),
[@scaladex](https://index.scala-lang.org/scalatest),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalatest),
[apidoc](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/index.html)
[quick start](https://www.scalatest.org/quick_start),
[user guide](https://www.scalatest.org/user_guide)

Most feature-rich framework unit- acceptance-style testing of Scala (and
Java) programs, see
[at a glance](https://www.scalatest.org/at_a_glance/FlatSpec).

ScalaTest has integrations with other libraries, like
[ScalaCheck](https://www.scalacheck.org),
[ScalaMock](https://scalamock.org),
also Mockito, EasyMock, jMock, JUnit via
[Scalatestplus](https://mvnrepository.com/artifact/org.scalatestplus)
libraries.

Problems with ScalaTest, in my view:
1. very large API + DSL surface, many design/usage/code choices; \
   despite excellent documentation there is just so much to look up...
1. problems with build tool integration, core and 3rd party plugins, \
   e.g., use of ScalaTest from Gradle 6.x is a nightmare, see below;
1. no cross-platform use, version dependencies with other libraries; \
   see [../scalatest_scalacheck](../scalatest_scalacheck/README.md),
   [../scalatest_scalamock](../scalatest_scalamock/README.md).

#### Notes: scalatest testing styles

ScalaTest offers ca. 8 different
[test notation styles](https://www.scalatest.org/user_guide/selecting_a_style)
to choose from in form of: APIs or DSLs, flat or nestable, function- or
property- or feature-oriented.

For a code example:
[HelloTests.scala](src/test/scala/netropy/polyglot/testing/scalatest/HelloTests.scala).

_My Preference:_
- _AnyPropSpec_ property tests over tests with fixed data;
- otherwise, _AnyFunSuite_ for function-style unit testing;
- otherwise, _AnyFeatureSpec_ for spec-style acceptance testing.

[AnyPropSpec](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/propspec/AnyPropSpec.html):
Flat, TDD/property-style unit tests with data from
- tables (_TableDrivenPropertyChecks_), i.e. parameterized tests, or
- random generators (_GeneratorDrivenPropertyChecks_, requires ScalaCheck in
  classpath).

[AnyFunSuite](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/funsuite/AnyFunSuite.html):
Flat, TDD/function-style unit tests (like JUnit, xUnit), takes test names,
generates specification-like output.

[AnyFlatSpec](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/flatspec/AnyFlatSpec.html):
Flat, BDD/spec-style unit tests using a DSL (_X should Y in, X must Y in_).

[AnyFeatureSpec](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/featurespec/AnyFeatureSpec.html):
Flat, BDD/acceptance-style tests using a DSL (_feature(X), scenario(Y)_).

[AnyFunSpec](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/funspec/AnyFunSpec.html):
Nestable, BDD/spec-style unit tests using functions (like _describe(X)_,
_it(Y)_ from Ruby's RSpec).

[AnyWordSpec](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/wordspec/AnyWordSpec.html):
Nestable, BDD/spec-style unit tests using a DSL that places words after
strings (_X when Y should Z in_, like specs2).

Ignored, here:
[AnyFreeSpec](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/freespec/AnyFreeSpec.html),
[RefSpec](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/refspec/RefSpec.html).

A Test-_Suite_ already has a rich set of
[Assertions](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/Assertions.html)
mixed in with, for example, explanatory _assertResult._

A Test-_Suite/Spec_ can be extended with, for example:
- [Diagrams](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/diagrams/index.html):
  failing asserts show the values and operation results in a diagram
- [Matchers](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/matchers/Matcher.html):
  to create custom matchers like _file should endWithExtension ("txt");_
- [GivenWhenThen](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/GivenWhenThen.html):
  for DSL spec methods _Given, When, Then, And;_
- [BeforeAndAfter](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/BeforeAndAfter.html):
  to execute code before and after running each test.

#### Notes: {sbt}+scalatest

The use from sbt is super easy, no plugins to be configured, just:
```
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % "test"
```

#### Notes: {maven}+scalatest

Alas, ScalaTest unit tests are not run via the _maven-surefire-plugin_.

The surefire core plugin is widely used (likely in the pom.xml anyway) and has
known configuration options for test name \<include\>/\<exclude\>_ patterns,
_\<parallel\>_ execution with _\<forkCount\>_, definition of a _\<runOrder\>_
etc.

ScalaTest provides its own test runner,
[scalatest-maven-plugin](https://www.scalatest.org/user_guide/using_the_scalatest_maven_plugin),
with its own configuration and execution options (which take up ~20 lines of
XML boilerplate code, here).

#### Notes: {gradle}+scalatest

2019..2022: Use of ScalaTest from Gradle 6.x has been fraught with problems.

1. The ScalaTest site
   [Running your tests](https://www.scalatest.org/user_guide/running_your_tests)
   lists many build tools and IDEs but not Gradle.
1. Erroneous claims (lost the source) that Gradle's JUnit4 runner could be
   used to run ScalaTest unit tests.  Alas, _tasks.test { useJUnit() }_ in
   build.gradle.kts has no effect in Gradle 6.x, no unit tests are picked up.
1. However, the Gradle site hosts a ScalaTest runner plugin
   [maiflai.scalatest](https://plugins.gradle.org/plugin/com.github.maiflai.scalatest)
   Alas, for Gradle 6.x, the last plugin version working is 0.30 from 2020.
1. The maiflai test runner plugin has a runtime dependency on the CommonMark
   parser
   [flexmark](https://mvnrepository.com/artifact/com.vladsch.flexmark/flexmark-all)
   Alas, the only version that satisfies this dependency and works is 0.35.10
   from 2019.
1. The maiflai plugin does not support recent
   [ScalaTest releases](https://mvnrepository.com/artifact/org.scalatest/scalatest).
   Alas, the last working version is ScalaTest 3.2.9 from 2019.
1. Only the 2.13 binary of ScalaTest can be consumed from Gradle 6.x
   (the _scala plugin_ currently runs _scalac 2.x_).  Import of the ScalaTest
   3 binary via the TASTy reader results in a TASTy signature error.
1. Alas, the maiflai test runner plugin might be an inactive project.  Its
   [development site](https://github.com/maiflai/gradle-scalatest)
   lists old Pull Requests and compatibility with ScalaTest 2.0 from 2013.

[Up](../README.md)
