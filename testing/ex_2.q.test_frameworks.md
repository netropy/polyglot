# polyglot/testing

### 2 Exercises: Usage of Unit- and Integration-Test Frameworks

#### 2.1 Exercise: Unit-test a simple function with common test frameworks.

Write tests for this trivial string function to show the basic usage of common
Scala or Java test frameworks, as their annotations, APIs, or DSLs:
```
    static public String greet(String s) {
        return "Hello " + s;
    }
```

In particular, check out a framework's support for
1. property testing,
1. BDD/spec-style or TDD/function-style testing,
1. parameterized tests, or
1. assertions.

(See [ex\_1\_2.r.md](ex_1_2.r.md) on terminology.)

[Answer](ex_2_1.r.md)

#### 2.2 Exercise: Write a minimal assertion API for (POJO) unit testing.

_Context:_

Some unit test frameworks and assertion libraries, see
[./Summaries & Notes](Notes_unit_test_frameworks.md),
come with a large API: tens of packages, tons of overloaded _assertXYZ_
methods, plus fluent _assertThat_ methods with _matchers_ for various data
types etc.

But some build tools support POJO (= Plain Old Java Object) tests, which do
not use an external assertion or test library, see
[./Summary: POJO Assertion Tests (Java, Scala)](../Notes_unit_test_frameworks.md#summary-pojo-assertion-tests-java-scala).

What would be a _minimal,_ most essential assertion API?  Which of the
_assertXYZ_ functions would we identify as most valuable or most often needed
in unit testing?

Perhaps, just 2 specialized functions are essential, in addition to the
language's pre-defined and general _assert_ feature:
- an _assertResult/asssertEqual_ for comparing an actual against expected
  value,
- an _assertThrows_ function for verifying an expected exception.

_Tasks:_
1. Write `assertResult`, `assertThrows` in Scala, Java (or other languages).
1. Discuss and document in code method design decisions (there will be!).
1. Write a unit test verifying these functions in terms of themselves.

[Answer](ex_2_2.r.md)

#### 2.3 Exercise: Mock-test a function that calls two stateful APIs.

Consider this "legacy" Java API (for convenience, defined in a Scala object).
It shows the interfaces of two _stateful services_ (for simplicity, just two
_java.util_ types):
```
object Events {

  import java.util.{Map,Iterator}
  import java.util.function.Consumer

  type Event = Map.Entry[Int, String]  // some (key, value) data
  type EventSource = Iterator[Event]  // a stateful interface
  type EventSink = Consumer[Event]  // a stateful interface
}
```

Assume that each interface represents a _remote service_ that is costly to
spin up or down.

_Tasks:_

1. Write a function _filter(EventSource, EventSink)_ that drops duplicate
   keys.  While there are elements, _filter_ should
   - _read_ a single _Event_ from the _EventSource_,
   - _drop_ it if its _key_ has been seen before,
   - otherwise, _forward_ it to the _EventSink_.
2. Write tests for function _filter_.  Try out and discuss mock-generators.

[Answer](ex_2_3.r.md)

[Up](./README.md)
