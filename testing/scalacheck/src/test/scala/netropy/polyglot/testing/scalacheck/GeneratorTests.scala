package netropy.polyglot.testing.scalacheck

import org.scalacheck.Properties
import org.scalacheck.Prop
import Prop.{forAll, exists, all, passed, falsified}


/** Code example: Custom Generators. */
object GeneratorTests
    extends Properties("GeneratorTests") {

  import org.scalacheck.Gen

  property("Gen.const(1)") =
    forAll(Gen.const(1)) { n => n == 1 }

  property("implicit Gen.const(1)") =
    forAll(1) { n => n == 1 }

  property("Gen.choose(1,10)") =
    forAll(Gen.choose(1,10)) { n => 1 <= n && n <= 10 }

  property("Gen.frequency(2 -> true, 1 -> false)") =
    forAll(Gen.frequency(2 -> true, 1 -> false)) { b => true }
    //forAll(Gen.frequency((2, true), (1, false))) { b => true }

  property("sequence only turns a seq of Gens into an ArrayList of values") = {
    forAll(Gen.sequence(Seq(Gen.const(1), Gen.const(2)))) { l =>
      l == new java.util.ArrayList(java.util.Arrays.asList(1,2))
    }
  }
}

/** Code experiment: Custom Generators. */
object StatefulGeneratorExperiment
    extends Properties("StatefulGeneratorExperiment") {

  import scala.collection.mutable.Buffer
  import org.scalacheck.Gen

  property("Hmm, properties/generators are somehow forgetful") = {
    val buf = Buffer[Boolean]()  // mutable
    val p = forAll(Gen.frequency(2 -> true, 1 -> false)) { b =>
      buf += b
      true
    }
    assert(buf.size == 0)  // passes, no elements added
    p
  }

  val buf = Buffer[Boolean]()  // mutable

  property("But properties can be \"made\" stateful (probabilistic test)") = {
    forAll(Gen.frequency(2 -> true, 1 -> false)) { b =>
      buf += b
      if (buf.size > 99) {  // mostly called 100x
        val (s, t) = (buf.size, buf.count(_ == true))
        assert(50 <= t && t <= 80, s"#true == $t, #total == $s")  // mostly
      }
      true
    }
  }
}
