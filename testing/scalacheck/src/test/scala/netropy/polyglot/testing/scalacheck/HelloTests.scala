package netropy.polyglot.testing.scalacheck

import netropy.polyglot.testing.Hello

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

/** Unit-tests the Hello.greet function. */
object HelloTests
    extends Properties("Hello") {

  property("Hello's greeting starts with 'Hello'") =
    forAll { (s: String) =>
      Hello.greet(s).startsWith("Hello")
    }

  property("Hello greeter composes") =
    forAll { (s: String) =>
      Hello.greet(s) == s"Hello $s"
    }
}
