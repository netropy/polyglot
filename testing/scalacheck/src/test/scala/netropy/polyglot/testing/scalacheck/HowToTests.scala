package netropy.polyglot.testing.scalacheck

import org.scalacheck.Properties
import org.scalacheck.Prop

/*
 2022-11, 2021-11: PROBLEM:
 The test runners of mvn, gradle do not run this class or object as test.
 Only sbt does.

 Probable cause: 'property(..) = ...' compiles to _static_ methods

 $ javap target/test-classes/netropy/polyglot/scala/testing/scalacheck/HowToTests.class
 Compiled from "HowToTests.scala"
 public final class netropy.polyglot.testing.scalacheck.HowToTests {
   public static void check(org.scalacheck.Test$Parameters);
   public static void include(org.scalacheck.Properties);
   public static void include(org.scalacheck.Properties, java.lang.String);
   public static void main(java.lang.String[]);
   public static java.lang.String name();
   public static org.scalacheck.Test$Parameters overrideParameters(org.scalacheck.Test$Parameters);
   public static scala.collection.Seq<scala.Tuple2<java.lang.String, org.scalacheck.Prop>> properties();
   public static org.scalacheck.Properties$PropertySpecifier property();
   public static org.scalacheck.Properties$PropertyWithSeedSpecifier propertyWithSeed();
   public static org.scalacheck.Test$Parameters check$default$1();
 }
 */

/** Code example: ScalaCheck properties tests, also see HelloTests.
  *
  * ScalaCheck property tests can be defined in a class or an object.
  * Prefer to define in an object:
  * - The test can be run by the built-in ScalaCheck command line test runner,
  *   which expects a main(): scala -cp ... HowToTests
  * - sbt recognizes this test whether defined as a class or an object.
  * - maven, gradle don't recognize this test even if defined as a class.
  */
object HowToTests
    extends Properties("HowTo") {

  // mvn, gradle do not recognize this class as test; only sbt does
  // assert(false, "in HowToTests")

  // define and run a property

  import Prop.{forAll, exists, all, passed, falsified}

  property("Boolean property makes a static test") =
    Prop("a".length > 0)

  property("String concatenation is associative") =
    forAll { (s: String, t: String, u: String) =>
      ((s + t) + u) == (s + (t + u))
    }

  // or define property values and group them for running together

  val propStringConcatStartsWith =
    forAll { (s: String, t: String) => (s + t).startsWith(s) }

  val propStringConcatEndsWith =
    forAll { (s: String, t: String) => (s + t).endsWith(t) }

  property("String concatenation starts/ends with") =
    all(propStringConcatStartsWith, propStringConcatEndsWith)

  // ScalaCheck only reports property outcomes: Failed, Error, Passed;
  // no support for declaring properties as Pending or Ignored.
  // Hence, moved into nested Properties to exclude them from running.
  object Skipped extends Properties("HowToSkipped") {

    property("some pending test...") = passed

    property("some ignored test") = falsified
  }
}
