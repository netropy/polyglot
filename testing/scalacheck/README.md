# polyglot/testing/scalacheck

#### Topics: A small, cloneable, _standalone_ build project

- _Source + Unit Test Code_ checking out features and usage of
  [ScalaCheck](https://www.scalacheck.org)
- _Project Code_ for each build tool -
  [sbt](https://www.scala-sbt.org),
  [Maven](https://maven.apache.org),
  [Gradle](https://gradle.org) -
  any of which can be removed;
- _Notes_ on the compatibility of build tools, language, and library versions
- _Notes + Code Experiments_ on the used libraries

Quick links: \
[scala sources](src/main/scala/netropy/polyglot/testing/),
[scala tests](src/test/scala/netropy/polyglot/testing/scalacheck/)

Also see: \
[../munit\_scalacheck](../munit_scalacheck/README.md),
[../scalatest\_scalacheck](../scalatest_scalacheck/README.md),
[../Summary: ScalaCheck](../Notes_unit_test_frameworks.md#summary-scalacheck)

#### Summary: {sbt,maven,gradle} X scala{2,3} X {scalacheck}

2022-11:
- __Maven, Gradle do not readily recognize pure ScalaCheck tests -- only sbt
  does.__
- To run ScalaCheck tests from Maven or Gradle requires a suitable _test
  runner._
- MUnit, ScalaTest provide such runners, see projects.
- For manual use, the built-in ScalaCheck command-line runner can run a single
  test.

[ScalaCheck](https://index.scala-lang.org/typelevel/scalacheck)
is available as Scala 2.11..13 and 3.x binaries.

| 2022-11 | 2.13 scalac, scala-library | 3.2 scalac (1), scala3-library |
|:--|:--:|:--:|
| scalacheck\_2.13 1.17.0 | OK | OK |
| scalacheck\_3 1.17.0 | OK (2) | OK |

(1) 2022-11, 2021-11: PROBLEM: Gradle really runs _scalac 2.x <- [zinc] <-
scala plugin_.  No solution yet how to configure running _scalac 3.x_.

(2) 2022-11: scalac 2.13 can consume Scala3 libs via _-Ytasty-reader_, see
[TASTy](https://docs.scala-lang.org/scala3/guides/tasty-overview.html).

#### Notes: scalacheck

Links: \
[ScalaCheck](https://www.scalacheck.org),
[@github](https://github.com/typelevel/scalacheck),
[@scaladex](https://index.scala-lang.org/typelevel/scalacheck),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalacheck),
[apidoc](https://javadoc.io/doc/org.scalacheck/scalacheck_3/latest/index.html),
[user guide](https://github.com/typelevel/scalacheck/blob/main/doc/UserGuide.md)

More code examples, quality tutorials:
[0](https://scalacheck.org/documentation.html)
[1](https://booksites.artima.com/scalacheck/examples/index.html),
[2](https://www.epfl.ch/labs/lamp/wp-content/uploads/2019/01/ScalacheckTutorial.html),
[3](https://alvinalexander.com/scala/scalacheck-custom-generator-examples),
[4](https://alvinalexander.com/scala/fp-book/scalacheck-2-advanced-example),
[5](https://www.baeldung.com/scala/scalacheck)

Library for property[-based] testing of Scala or Java programs with automatic
test data generation.

Properties:
- just Properties, no Assertions; can use MUnit, ScalaTest, or other assertion
  libraries for (slightly) better error reporting
- reported property outcomes: _failed, error, passed,_ and _undecided_ (for
  conditional properties)
- no support for declaring properties as _pending_ or _ignored;_ \
  but can move properties  into a nested object to exclude from running
- support for conditional properties, combining/grouping/labeling properties

Generators:
- support for various generators: user-defined, sized, conditional, arbitrary,
  containers
- support for collecting test data statistics to check the _test case
  distribution_

Automatic Test Case Minimisation: (Nice!)
- If an argument falsifies a property, ScalaCheck tries to minimise that
  argument before it is reported.
- Enabled by use of _Prop.forAll_ in properties vs _Prop.forAllNoShrink_.

Support for _Stateful Testing_: Check how methods (_commands_) affect the
system's state throughout time.

Tip: Prefer to define properties in an object instead of a class
- The test can be run by the built-in ScalaCheck command line test runner,
  which expects a main(): \
  _scala -cp ... HelloScalaCheckTests_
- sbt recognizes this test whether defined as a class or an object.
- Maven, Gradle don't recognize this test anyway, even if defined in a class.

Test runners: standalone (built-in), munit, ScalaTest, and specs2.

Example of using the built-in ScalaCheck command line test runner:
```
$ scala -cp ...:scalacheck_3-1.17.0.jar ...HelloScalaCheckTests
+ Hello.Hello's greeting starts with 'Hello': OK, passed 100 tests.
+ Hello.Hello's greeting ends with its argument: OK, passed 100 tests.
+ Hello.Hello greeter composes: OK, passed 100 tests.
```

Try out: Generic
[ScalaCheck Toolbox Combinators](https://mvnrepository.com/artifact/com.47deg/scalacheck-toolbox-combinators)

#### Notes: {sbt}+scalacheck

The use from sbt is super easy, no plugins to be configured, just:
```
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.17.0" % "test"
```

The sbt test runner recognizes ScalaCheck property tests defined as class or
object.

#### Notes: {maven,gradle}+scalacheck

2022-11: PROBLEM: \
The test runners of Maven, Gradle do not recognize (and never have) pure
ScalaCheck test classes, regardless of naming patterns or whether defined as
class or object.

Probable cause: _property(...)_ compiles to _static_ fields and methods:
```
class HelloScalaCheckTests
    extends Properties("Hello") {

  property("Hello's greeting starts with 'Hello'") =
    Prop.forAll { (s: String) =>
      Hello.greet(s).startsWith("Hello")
    }
	...
}

$ javap './target/.../HelloScalaCheckTests.class'

public class ...HelloScalaCheckTests extends org.scalacheck.Properties {

  public static final boolean $anonfun$new$2(java.lang.String);
  public static final org.scalacheck.Prop $anonfun$new$3(boolean);
  public static final org.scalacheck.util.Pretty $anonfun$new$4(java.lang.String);
  ...
}
```

[Up](../README.md)
