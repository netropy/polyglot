package netropy.polyglot.testing

/** Excercise: A given, stateful API for events. */
object Events {

  import java.util.{Map,Iterator}
  import java.util.function.Consumer

  type Event = Map.Entry[Int, String]  // some (key, value) data
  type EventSource = Iterator[Event]  // a stateful interface
  type EventSink = Consumer[Event]  // a stateful interface
}

/** Excercise: A function that pulls, filters, forwards events one-by-one. */
object EventFilter {

  import scala.collection.mutable.HashSet
  import Events._

  def filter(src: EventSource, snk: EventSink): Unit = {

    // pedestrian implementation (better ways to do this)
    val seen = HashSet.empty[Int]

    // using while-loop, problems with forEachRemaining() on generated mock
    while (src.hasNext) {
      val e = src.next()
      if (seen.add(e.getKey))
        snk.accept(e)
    }
    // this extra call is not expected by generated mock
    //assert(!src.hasNext());

    // Find out why forEachRemaining() does not iterate on generated mock
    // per javadoc, all it does is: while (hasNext()) action.accept(next());
    /*
    src.forEachRemaining { e =>
      assert(false)  // never thrown on generated mock, only handcoded mock
      if (seen.add(e.getKey))
        snk.accept(e)
     }
   */
  }
}
