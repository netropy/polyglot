package netropy.polyglot.testing.scalatest_scalamock

import netropy.polyglot.testing.Events._
import netropy.polyglot.testing.EventFilter

import java.util.AbstractMap.SimpleImmutableEntry

// ScalaTest 3.1.0 reorganized names
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.diagrams.Diagrams

/** Excercise: Unit-tests EventFilter with handwritten mocks. */
class EventFilterTests_HandcodedMock
    extends AnyFunSuite
    with Diagrams {

  /** Mock delivering data from a given list. */
  class MockEventSource(e: List[Event]) extends EventSource {

    val i = e.iterator
    def hasNext: Boolean = i.hasNext
    def next(): Event = i.next()
  }

  /** Mock checking data against a given list. */
  class MockEventSink(e: List[Event]) extends EventSink {

    val i = e.iterator
    def accept(e: Event): Unit = {
      // assert(i.hasNext && i.next() == e) }  // Diagrams fails this
      assert(i.hasNext)
      assert(i.next() == e)
    }
  }

  test("filter() reads, filters, and forwards Events in exact order") {
    def e(i: Int, s: String): Event = new SimpleImmutableEntry(i, s) // factory

    val src = new MockEventSource(List(e(1, "a"), e(1, "aa"), e(2, "b")))
    val snk = new MockEventSink(List(e(1, "a"), e(2, "b")))

    EventFilter.filter(src, snk)

    assert(!src.i.hasNext)
    assert(!snk.i.hasNext)
  }
}
