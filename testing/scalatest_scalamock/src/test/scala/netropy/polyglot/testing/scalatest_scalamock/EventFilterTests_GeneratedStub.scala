package netropy.polyglot.testing.scalatest_scalamock

import netropy.polyglot.testing.Events._
import netropy.polyglot.testing.EventFilter

import java.util.AbstractMap.SimpleImmutableEntry

import org.scalamock.scalatest.MockFactory

// ScalaTest 3.1.0 reorganized names
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.diagrams.Diagrams

/** Excercise: Unit-tests EventFilter using generated stubs only.
  *
  * Two unit tests here for how to specify the order of calls/return values:
  * - "filter()" should "read, filter, and forward Events in exact order"
  * - "filter()" should "read, filter, and forward Events in any order"
  */
class EventFilterTests_GeneratedStub
    extends AnyFlatSpec
    with Diagrams
    with MockFactory {

  behavior of "filter()"

  // stubs only
  val src = stub[EventSource]  // record-then-verify style with returns
  val snk = stub[EventSink]    // record-then-verify style with arguments

  def e(i: Int, s: String): Event = new SimpleImmutableEntry(i, s) // factory

  it should "read, filter, and forward Events in exact order" in {

    // Good! The code below defines a _total_ order only within inSequence{}:
    //
    // Expected:
    // inAnyOrder {
    //   inSequence {
    //     <stub-1> Iterator.hasNext() once (called once)
    //     <stub-1> Iterator.next() once (called once)
    //     <stub-1> Iterator.hasNext() once (called once)
    //     <stub-1> Iterator.next() once (called once)
    //     <stub-1> Iterator.hasNext() once (called once)
    //     <stub-1> Iterator.next() once (called once)
    //     <stub-1> Iterator.hasNext() once (called once)
    //   }
    //   inSequence {
    //     <stub-2> Consumer.accept(1=a) once (called once)
    //     <stub-2> Consumer.accept(2=b) once (called once)
    //   }
    // }

    // src stub must be told returns, we fixate the order:
    inSequence {
      (src.hasNext _).when().returns(true).once()  //.anyNumberOfTimes() default
      (src.next _).when().returns(e(1, "a")).once()
      (src.hasNext _).when().returns(true).once()
      (src.next _).when().returns(e(1, "aa")).once()
      (src.hasNext _).when().returns(true).once()
      (src.next _).when().returns(e(2, "b")).once()
      (src.hasNext _).when().returns(false).once()
    }

    EventFilter.filter(src, snk)

    // snk stub must be checked on calls & args, we fixate the order:
    inSequence {
      (snk.accept _).verify(e(1, "a"))  //.once() is default here
      (snk.accept _).verify(e(2, "b"))
    }
  }

  it should "read, filter, and forward Events in any order" in {

    // Good! The code below defines a _partial_ order under method+returns():
    //
    // Expected:
    // inAnyOrder {
    //   <stub-1> Iterator.hasNext() once (called once)
    //   <stub-1> Iterator.hasNext() once (called once)
    //   <stub-1> Iterator.hasNext() once (called once)
    //   <stub-1> Iterator.hasNext() once (called once)
    //   <stub-1> Iterator.next() once (called once)
    //   <stub-1> Iterator.next() once (called once)
    //   <stub-1> Iterator.next() once (called once)
    //   <stub-2> Consumer.accept(2=b) once (called once)
    //   <stub-2> Consumer.accept(1=a) once (called once)
    // }

    // src stub must be told returns, latter in right order:
    inAnyOrder {  // redundant, is default!
      (src.hasNext _).when().returns(true).once()     // order respected...
      (src.hasNext _).when().returns(true).once()     // ...
      (src.hasNext _).when().returns(true).once()     // ...
      (src.hasNext _).when().returns(false).once()    // ordered on src.hasNext
      (src.next _).when().returns(e(1, "a")).once()   // ...
      (src.next _).when().returns(e(1, "aa")).once()  // ...
      (src.next _).when().returns(e(2, "b")).once()   // ordered on src.next
    }

    EventFilter.filter(src, snk)

    // snk stub must be checked on calls & args, we allow any order:
    inAnyOrder {  // redundant, is default!
      (snk.accept _).verify(e(2, "b"))  // ...
      (snk.accept _).verify(e(1, "a"))  // swapped! any order OK on snk.accept
    }
  }
}
