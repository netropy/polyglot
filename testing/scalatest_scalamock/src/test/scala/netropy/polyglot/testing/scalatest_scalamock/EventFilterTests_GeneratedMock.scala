package netropy.polyglot.testing.scalatest_scalamock

import netropy.polyglot.testing.Events._
import netropy.polyglot.testing.EventFilter

import java.util.AbstractMap.SimpleImmutableEntry

import org.scalamock.scalatest.MockFactory

// ScalaTest 3.1.0 reorganized names
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.diagrams.Diagrams

/** Excercise: Unit-tests EventFilter using generated mocks only.
  *
  * Two unit tests here for how to specify the order of calls/return values:
  * - "filter()" should "read, filter, and forward Events in exact order"
  * - "filter()" should "read, filter, and forward Events in any order"
  */
class EventFilterTests_GeneratedMock
    extends AnyFlatSpec
    with Diagrams
    with MockFactory {

  behavior of "filter()"

  // mocks only
  val src = mock[EventSource]  // expectations-first style with returns
  val snk = mock[EventSink]    // expectations-first style with arguments

  def e(i: Int, s: String): Event = new SimpleImmutableEntry(i, s) // factory

  it should "read, filter, and forward Events in exact order" in {

    // Good! The code below defines a _total_ order only within inSequence{}:
    //
    // Expected:
    // inAnyOrder {
    //   inSequence {
    //     <mock-1> Iterator.hasNext() once (called once)
    //     <mock-1> Iterator.next() once (called once)
    //     <mock-2> Consumer.accept(1=a) once (called once)
    //     <mock-1> Iterator.hasNext() once (called once)
    //     <mock-1> Iterator.next() once (called once)
    //     <mock-1> Iterator.hasNext() once (called once)
    //     <mock-1> Iterator.next() once (called once)
    //     <mock-2> Consumer.accept(2=b) once (called once)
    //     <mock-1> Iterator.next() once (called once)
    //   }
    // }

    // src, snk mocks must be told calls & args & returns, we fixate the order:
    inSequence {
      (src.hasNext _).expects().returning(true)  // .once() is default here
      (src.next _).expects().returning(e(1, "a"))
      (snk.accept _).expects(e(1, "a"))
      (src.hasNext _).expects().returning(true)
      (src.next _).expects().returning(e(1, "aa"))
      (src.hasNext _).expects().returning(true)
      (src.next _).expects().returning(e(2, "b"))
      (snk.accept _).expects(e(2, "b"))
      (src.hasNext _).expects().returning(false)
    }

    EventFilter.filter(src, snk)
  }

  it should "read, filter, and forward Events in any order" in {

    // Good! The code below defines a _partial_ order under method+returning():
    //
    // Expected:
    // inAnyOrder {
    //   <mock-1> Iterator.hasNext() once (called once)
    //   <mock-1> Iterator.hasNext() once (called once)
    //   <mock-1> Iterator.hasNext() once (called once)
    //   <mock-1> Iterator.hasNext() once (called once)
    //   <mock-1> Iterator.next() once (called once)
    //   <mock-1> Iterator.next() once (called once)
    //   <mock-1> Iterator.next() once (called once)
    //   <mock-2> Consumer.accept(2=b) once (called once)
    //   <mock-2> Consumer.accept(1=a) once (called once)
    // }

    // src, snk mocks must be told calls & args & returns, latter in right order:
    inAnyOrder {  // redundant, is default!
      (src.hasNext _).expects().returning(true)     // order respected...
      (src.hasNext _).expects().returning(true)     // ...
      (src.hasNext _).expects().returning(true)     // ...
      (src.hasNext _).expects().returning(false)    // ordered on src.hasNext
      (src.next _).expects().returning(e(1, "a"))   // ...
      (src.next _).expects().returning(e(1, "aa"))  // ...
      (src.next _).expects().returning(e(2, "b"))   // ordered on src.next
      (snk.accept _).expects(e(2, "b"))  // ...
      (snk.accept _).expects(e(1, "a"))  // swapped! any order OK on snk.accept
    }

    EventFilter.filter(src, snk)
  }
}
