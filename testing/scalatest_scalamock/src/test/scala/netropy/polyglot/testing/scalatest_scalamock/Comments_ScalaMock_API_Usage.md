# polyglot/testing/scalatest_scalamock

# Comments: ScalaMock API Usage

### Discussion: The ScalaMock documentation is insufficient (2022-11).

It took too many code experiments to figure out how the stub/mock APIs work.

The [ScalaMock user guide](https://scalamock.org/user-guide)
and
[ScalaTest user guide](https://www.scalatest.org/user_guide/testing_with_mock_objects)
have not enough code examples on stub-style usage.

The
[apidoc](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/index.html)
mostly repeat the user guide; lacks 1-liners on most methods/classes
(like _matchers_).

Finally, some
[test/](https://github.com/paulbutcher/ScalaMock/tree/master/examples/src/test/scala/com/example),
[main/](https://github.com/paulbutcher/ScalaMock/tree/master/examples/src/main/scala/com/example)
code examples on ScalaMock's github turned out of some help.

### Q&A (as figured out from docs, examples, code experiments)

#### Q&A: What's the use of '_' when specifying nullary methods?

OK, this is standard Scala (w/o automatic eta expansion):
```
(src.hasNext _).when().returns(true).once()
(() => src.hasNext).when().returns(true).once()
```

#### Q&A: When should I use expects() vs when()?  Where in the API doc?

OK expects(...) is for mocks only, when(...) is for stubs only, see: \
[apidoc Mock](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/proxy/Mock.html),
[apidoc Stub](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/proxy/Stub.html)

#### Q&A: When should I use when() vs when(\*) vs when(...)?

OK, when() is for no args methods, other specify any/some argument:
```
(src.hasNext _).when().returns(true).once()        // for no arg only
(snk.accept _).when(*).returns(()).once()          // matchers.MatchAny
(snk.accept _).when(e(1, "a")).returns(()).once()  // matches this arg
((e: Event) => snk.accept(e)).when(e(1, "a")).returns(()).once()
```

#### Q&A: When should I use expects() vs expects(\*) vs expects(...)()?

OK, same as when(): expects() is for no args, other specify any/some argument.

#### Q&A: What's the difference between returns(), returning(), onCall()?

OK, only grammatical and whether taking a lambda; these are the same:
```
(src.hasNext _).when().returns(false).once()
(src.hasNext _).when().returning(false).once()
(src.hasNext _).when().onCall(() => false).once()
```
Perhaps, returns() reads nicer after when() vs returning() after expects().

[apidoc CallHandler](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/handlers/CallHandler.html)

#### Q&A: What's the difference between throws(), throwing(), onCall()?
OK, only grammatical and whether taking a lambda; these are the same:
```
(src.hasNext _).when().throws(new RuntimeException("err"))
(src.hasNext _).when().throwing(new RuntimeException("err"))
(src.hasNext _).when().onCall(() => throw new RuntimeException("err"))
```
Perhaps, throws() reads nicer after when() vs throwing() after expects().

[apidoc CallHandler](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/handlers/CallHandler.html)

#### Q&A: When should I specify once()? Sometimes default, sometimes not?

Seems .anyNumberOfTimes() is default after returns() vs once() after verify():
```
(src.hasNext _).when().returns(true).once() // else .anyNumberOfTimes()
EventFilter.filter(src, snk)
(snk.accept _).verify(e(1, "a"))            // .once() is default here
```

#### Q&A: Besides once() what else is available?  Where in the API doc?

OK, we have:
- once, twice, atLeastOnce, atLeastTwice, noMoreThanOnce, noMoreThanTwice,
- anyNumberOfTimes, never, times, <-- not sure what times() does
- repeat(count: Int|range: Range), repeated(count: Int|range: Range) <- just alias?

[apidoc CallHandler](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/handlers/CallHandler.html)

#### Q&A: How can I specify an order on returns() values within inAnyOrder{}?

OK, turns out returns()/onCall() is always ordered _per method name:_
```
inAnyOrder {  // redundant, is default!
  (src.hasNext _).when().returns(true).once()    // order respected
  (src.hasNext _).when().returns(true).once()    // ...
  (src.hasNext _).when().returns(false).once()   // order respected
}
```

#### Q&A: When should I specify inAnyOrder{}? Sometimes default, sometimes not?

OK, don't need inAnyOrder{} in top-level, only for nesting inSequence{}:
```
//inAnyOrder {  // redundant, is default!
  (snk.accept _).verify(e(2, "b"))
  (snk.accept _).verify(e(1, "a"))
//}
```

#### Q&A: When should I use inAnyOrder{} vs inSequence{}?

The semantics is slightly confusing but reflects what is needed:
- inSequence{...} == applies to all methods/blocks exactly as listed
- inAnyOrder{...} == does not apply to returns() on _same method names_

So, when we want to specify _total_ call order (for mocks/stubs):
```
inSequence {
  (src.hasNext _).expects().returning(true)
  (src.next _).expects().returning(e(1, "a"))
  (snk.accept _).expects(e(1, "a"))
  (src.hasNext _).expects().returning(true)
  (src.next _).expects().returning(e(2, "b"))
  (snk.accept _).expects(e(2, "b"))
  (src.hasNext _).expects().returning(false)
}
```

So, when we want to specify _partial_ call order (for mocks/stubs):
```
inAnyOrder {  // redundant, is default!
  (src.hasNext _).expects().returning(true)     // order respected...
  (src.hasNext _).expects().returning(true)     // ...
  (src.hasNext _).expects().returning(false)    // ordered on src.hasNext
  (src.next _).expects().returning(e(1, "a"))   // ...
  (src.next _).expects().returning(e(2, "b"))   // ordered on src.next
  (snk.accept _).expects(e(2, "b"))  // ...
  (snk.accept _).expects(e(1, "a"))  // swapped! any order OK on snk.accept
}
```

[Dir](.)
