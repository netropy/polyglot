# polyglot/testing/scalatest_scalamock

#### Topics: A small, cloneable, _standalone_ build project

- _Source + Unit Test Code_
  for excercise
  [../ex_2_3](../ex_2.q.test_frameworks.md#23-exercise-mocktest-a-function-that-calls-two-stateful-apis)
  checking out
  [ScalaMock](https://scalamock.org) from
  [ScalaTest](https://www.scalatest.org)
- _Project Code_ for each build tool -
  [sbt](https://www.scala-sbt.org),
  [Maven](https://maven.apache.org),
  [Gradle](https://gradle.org) -
  any of which can be removed
- _Notes_ on the compatibility of build tools, language, and library versions
- _Notes + Code Experiments_ on the used libraries

Quick links: \
[scala sources](src/main/scala/netropy/polyglot/testing/),
[scala tests](src/test/scala/netropy/polyglot/testing/scalatest_scalamock),
[Comments_ScalaMock_API_Usage](src/test/scala/netropy/polyglot/testing/scalatest_scalamock/Comments_ScalaMock_API_Usage.md)

Also see: \
[../ex_2_3](../ex_2.q.test_frameworks.md#23-exercise-mocktest-a-function-that-calls-two-stateful-apis),
[../scalatest](../scalatest/README.md),
[Summary: ScalaMock](../Notes_unit_test_frameworks.md#summary-scalamock),
[Summary: ScalaTest](../Notes_unit_test_frameworks.md#summary-scalatest)

#### Summary: {sbt,maven,gradle} X scala{2,3} X {scalatest+scalamock}

[ScalaMock](https://index.scala-lang.org/paulbutcher/scalamock)
is only available as Scala 2.11..13 binaries.

ScalaMock is typically used through
[ScalaTest](https://index.scala-lang.org/scalatest/scalatest) (or
[specs2](https://github.com/etorreborre/specs2)).

| 2022-11 | 2.13 scalac, scala-library | 3.2 scalac (1), scala3-library |
|:--|:--:|:--:|
| scalatest\_2.13 3.2.14, scalamock\_2.13 5.2.0 (2) | OK | Errors (3) |

(1) 2022-11, 2021-11: PROBLEM: Gradle really runs _scalac 2.x <- [zinc] <-
scala plugin_.  No solution yet how to configure running _scalac 3.x_.

(2) 2022-11, 2021-11: ___Problems with use of ScalaTest from Gradle 6.x:___
- Only the _2.13 binary_ of the ScalaTest library can be imported,
- and only up to ScalaTest version _3.2.9 from 2019_, see details in
 [../scalatest](../scalatest/README.md).

(3) 2022-11, 2021-11: ScalaMock uses Scala 2 macros, no cross-platform use
2.13 -> 3.

#### Notes: scalatest + scalamock (no scalatestplus)

Links: \
[ScalaTest](https://www.scalatest.org),
[@github](https://github.com/scalatest/scalatest),
[@scaladex](https://index.scala-lang.org/scalatest),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalatest),
[apidoc](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/index.html)
[quick start](https://www.scalatest.org/quick_start),
[user guide](https://www.scalatest.org/user_guide)

ScalaMock is _not_ integrated through
[ScalaTestPlus](https://www.scalatest.org/plus)
but ships its own base class for ScalaTest (and specs2).

A class then simply _extends ... with MockFactory,_ see user guides of
[ScalaMock](https://scalamock.org/user-guide/integration) and
[ScalaTest](https://www.scalatest.org/user_guide/testing_with_mock_objects).

#### Notes: scalamock

Links: \
[ScalaMock](https://scalamock.org),
[@github](https://github.com/paulbutcher/ScalaMock),
[@scaladex](https://index.scala-lang.org/paulbutcher/scalamock),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalamock)
[apidoc](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/index.html),
[quick start](https://scalamock.org/quick-start),
[user guide](https://scalamock.org/user-guide)

See discussion of ScalaMock in excercise
[../ex_2_3.r](./ex_2_3.r.md)

Support for Scala2 features:
- mocking of traits, Java interfaces, and non-final classes with default
  constructor
- with overloaded, curried, or polymorphic methods;
- not yet supported: mock any class and singleton/companion objects;
- _not sure:_ compiler-plugin needed to generate mocks for a _class_-based
  API.

Two usage styles:
- _expectations-first_ style (using
  [mock](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/proxy/Mock.html)):
  the request-response behaviour of the mock is configured beforehand;
- _record-then-verify_ (Mockito) style (using
  [stub](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/proxy/Stub.html)):
  the behaviour observed by the stub is checked afterwards;
- both mocks and stubs must have any return values configured _before_ the
  function under test is called;
- mocks and stubs can be mixed freely in code.

API Features:
- [mocks](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/proxy/Mock.html)
  are configured with _expects();_
  [stubs](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/proxy/Stub.html)
  are checked with _when(), verify();_
- nestable blocks _inSequence{...}_, _inAnyOrder{...}_ to specify expected / verify
  actual order of calls;
- configurable, expected call counts: _once, atLeastOnce,
  anyNumberOfTimes, ..._ see
  [CallHandler](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/handlers/CallHandler.html);
- argument matching support: wildcards, epsilon matching (floating points),
  predicate matching;
- configurable return values and exceptions: _onCall(), returns/returning(),
  throws/throwing();_
- thread-safe: but mock calls must be complete before the test completes.

[Up](../README.md)
