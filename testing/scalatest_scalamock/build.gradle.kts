/*
 * (This file was generated by the Gradle 'init' task.)
 */

group = "netropy"
version = "1.0.0-SNAPSHOT"
description = "Example, Scala, Scala3, ScalaTest, ScalaMock, sbt, gradle, maven"

// Tip: gradle skips tests on re-run, force to run all test cases:
// $ gradle cleanTest test

// Tip: find test reports under
// - build/test-results/test/TEST-*.xml
// - build/reports/tests/test/index.html
// - no .txt reports

plugins {
    // core plugin for Java (compile, test, bundle)
    // https://docs.gradle.org/current/userguide/java_plugin.html
    //java  // seems enabled by default

    // plugin for Java API and implementation separation
    // https://docs.gradle.org/current/userguide/java_library_plugin.html
    //`java-library`

    // plugin for mixed Scala and Java code (compile)
    // 2022-11: PROBLEM: still no support to run scalac 3.x
    // 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
    // https://docs.gradle.org/current/userguide/scala_plugin.html
    scala

    // ScalaTest runner plugin for gradle
    // https://mvnrepository.com/artifact/com.github.maiflai/gradle-scalatest
    // https://plugins.gradle.org/plugin/com.github.maiflai.scalatest
    // https://github.com/maiflai/gradle-scalatest
    //
    // 2022-11, 2021-11:
    // - mandatory plugin, otherwise no ScalaTest unit tests are run
    // - not a solution, no effect: use of ScalaTest's junit4 runner
    //   tasks.test { useJUnit() }
    // - plugin has a runtime dependency
    //   testRuntimeOnly("com.vladsch.flexmark:flexmark-all:0.35.10")
    //
    // 2022-11: PROBLEM: Gradle 6.9.3 fails 'test' with maiflai plugin 0.32
    // 2021-11: PROBLEM: Gradle 6.6.1 fails 'test' with maiflai plugin 0.31
    //    No signature of method ... is applicable for argument types ...
    //id("com.github.maiflai.scalatest") version "0.32"
    //id("com.github.maiflai.scalatest") version "0.31"
    id("com.github.maiflai.scalatest") version "0.30"
}

repositories {
    mavenCentral()
}

dependencies {
    // Scala-Lib version for code+tests
    // 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
    //val scala_compat_version = "2.12"
    //implementation("org.scala-lang:scala-library:2.12.20")
    val scala_compat_version = "2.13"
    implementation("org.scala-lang:scala-library:2.13.15")
    //val scala_compat_version = "3"
    //implementation("org.scala-lang:scala3-library_3:3.3.4")

    // ScalaTest, ScalaMock
    // https://mvnrepository.com/artifact/org.scalatest/scalatest
    // https://mvnrepository.com/artifact/org.scalamock/scalamock
    //
    // 2024-10, 2022-11, 2021-11: scalatest 3.2.9 is only working version!
    //   scala-library_2.12 + scalatest_2.12 + scalamock_2.12
    //   scala-library_2.13 + scalatest_2.13 + scalamock_2.13
    //testImplementation("org.scalatest:scalatest_2.12:3.2.9")
    //testImplementation("org.scalamock:scalamock_2.12:6.0.0")
    testImplementation("org.scalatest:scalatest_2.13:3.2.9")
    testImplementation("org.scalamock:scalamock_2.13:6.0.0")
    //
    // 2024-10: PROBLEM: Gradle 6.9.4 fails compile/test with ScalaTest 3.2.19
    // 2022-11: PROBLEM: Gradle 6.9.3 fails compile/test with ScalaTest 3.2.14
    // 2021-11: PROBLEM: Gradle 6.6.1 fails compile/test with ScalaTest 3.2.10
    //   An exception or error caused a run to abort. This may have been
    //   caused by a problematic custom reporter.
    //   java.lang.NoClassDefFoundError: com/vladsch/flexmark/util/ast/Node
    //testImplementation("org.scalatest:scalatest_$scala_compat_version:3.2.10")
    //testImplementation("org.scalatest:scalatest_$scala_compat_version:3.2.14")
    //testImplementation("org.scalatest:scalatest_$scala_compat_version:3.2.19")
    //
    // 2024-04: ScalaMock ported to Scala3
    // 2022-11, 2021-11: PROBLEM: ScalaMock not yet ported to Scala3
    //   scala3-library_3.2 + scalatest_3 + scalamock_3
    // 2022-11, 2021-11: PROBLEM: uses Scala2 macros, so no import from Scala3
    //   scala3-library_3.2 + scalatest_2.13 + scalamock_2.13
    //
    // 2022-11, 2021-11: PROBLEM: no cross-compile works
    //   scala-library_2.13 + scalatest_3 + -Ytasty-reader
    //     Unsupported Scala 3 inline macro method
    //   scala3-library_3.2 + scalatest_3 + -Ytasty-reader
    //     scala.tools.tasty.UnpickleException/TASTy signature has wrong version
    //testImplementation("org.scalatest:scalatest_3:3.2.9")

    // Flexmark, mandatory runtime dependency by maiflai.scalatest
    // CommonMark Java parser to generate html reports
    // https://mvnrepository.com/artifact/com.vladsch.flexmark/flexmark-all
    // https://github.com/vsch/flexmark-java
    //
    // 2022-11, 2021-11: PROBLEM:
    //   versions 0.35.10 or 0.36.8 last one working maiflai.scalatest
    //testRuntimeOnly("com.vladsch.flexmark:flexmark-all:0.62.2")  // 2020
    //testRuntimeOnly("com.vladsch.flexmark:flexmark-all:0.36.8")  // 2018
    testRuntimeOnly("com.vladsch.flexmark:flexmark-all:0.35.10") // 2019
}

tasks.withType<ScalaCompile> {
    scalaCompileOptions.additionalParameters = mutableListOf(
        "-feature",
        "-deprecation",
        "-unchecked",
	// 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
        //"-Ytasty-reader",  // scala2.13
        //"-explaintypes",  // scala2
        //"-explain",  // scala3
        ""
    )
}
