name := "polyglot.testing.scalatest_scalamock"
organization := "netropy"
description := "Example, Scala, Scala3, ScalaTest, ScalaMock, sbt, gradle, maven"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

// OK: Scala 3, 2.13, 2.12
//scalaVersion := "2.12.20"
//scalaVersion := "2.13.15"
scalaVersion := "3.3.4"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  //"-Ytasty-reader",  // scala2.13
  //"-explaintypes",  // scala2
  //"-explain",  // scala3
)

// Resolvers (pre- or user-defined)
// https://www.scala-sbt.org/1.x/docs/Resolvers.html
//resolvers += DefaultMavenRepository  // inluded by default
//resolvers += Resolver.mavenLocal  // ~/.m2

// ScalaTest, ScalaMock
// https://mvnrepository.com/artifact/org.scalatest/scalatest
// https://mvnrepository.com/artifact/org.scalamock/scalamock
// https://github.com/paulbutcher/ScalaMock
// https://index.scala-lang.org/paulbutcher/scalamock/scalamock
//
// scalac 3/2.13/2.12 + scalatest_3/2.13/2.12 + scalamock_3/2.13/2.12
// 2024-04: OK, 2022-11, 2021-11: ScalaMock not yet ported to Scala3
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.19" % "test"
libraryDependencies += "org.scalamock" %% "scalamock" % "6.0.0" % "test"
//
// scalac 3.2 + scalatest_2.13 + scalamock_2.13
// 2022-11, 2021-11: Scala 2 macro cannot be used in Dotty...
//
// scalac 3.2 + scalatest_3 + scalamock_3
// 2022-11, 2021-11: No scalamock_3 binary yet

// only use a single thread for building
//parallelExecution := false
// execute tests in the _current_ project serially (default parallel)
//Test / parallelExecution := false

// fork a new JVM for 'run' and 'Test/run'
//fork := true
// fork a new JVM for 'Test/run', but not 'run'
//Test / fork := true
// add a JVM option to use when forking a JVM for 'run'
//javaOptions += "-Xmx8G"

//Test / testOptions := Seq(Tests.Filter(s => s.endsWith("Test")))

// print the output of tests immediately instead of buffering
//Test / logBuffered := false
