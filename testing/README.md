# polyglot/testing

### Project Templates with Test Frameworks, Notes, Experiments

##### _TOC:_ Refcards, Notes
- [Notes: Compatibility {build tools} X scala{2.x,3} X {test libraries}](Notes_compatibility_build-tools_scala_test-libs.md)
- [Summaries & Notes: Unit Test Frameworks](Notes_unit_test_frameworks.md)
- [Refcard: Basic APIs, Annotations, DSLs of Unit Test Frameworks](Refcard_unit_test_frameworks_usage.md)

##### _TOC:_ Exercises, Q&As
- [1 Q&A: Basics of Software Verification Testing](ex_1.q.software_testing.md)
- [2 Exercises: Usage of Unit- and Integration-Test Frameworks](ex_2.q.test_frameworks.md)

##### _TOC:_ Small, cloneable, _standalone_ build projects

| Project | Libraries | Build Tools | Languages | Code |
|:--|:--|:--|:--|:--|
| [./pojo_assert](pojo_assert/README.md) | none, "POJO" tests | maven | Scala, Java | [ex_2_2 asserts](ex_2.q.test_frameworks.md), HelloTests |
| [./junit4](junit4/README.md) | JUnit 4 | sbt, maven, gradle | Scala, Java | HelloTests, HowToTests |
| [./junit5](junit5/README.md) | JUnit 5 | sbt, maven, gradle | Scala, Java | HelloTests, HowToTests |
| [./munit](munit/README.md) | MUnit | sbt, maven, gradle | Scala 2.13 + 3 | HelloTests, HowToTests |
| [./munit\_scalacheck](munit_scalacheck/README.md) | MUnit + ScalaCheck | sbt, maven, gradle | Scala 2.13 + 3 | HelloTests, HowToTests |
| [./scalacheck](scalacheck/README.md) | ScalaCheck | sbt, maven, gradle | Scala 2.13 + 3 | HelloTests, HowToTests |
| [./scalatest](scalatest/README.md) | ScalaTest | sbt, maven, gradle | Scala 2.13 + 3 | HelloTests, HowToTests |
| [./scalatest_scalacheck](scalatest_scalacheck/README.md) | ScalaTest + ScalaCheck | sbt, maven, gradle | Scala 2.13 + 3 | HelloTests, HowToTests |
| [./scalatest_scalamock](scalatest_scalamock/README.md) | ScalaTest + ScalaMock | sbt, maven, gradle | Scala 2.13 | [ex_2_3 mock-testing](ex_2.q.test_frameworks.md) |

Ideas bin:
- {[Mockito](https://github.com/mockito/mockito),
  [Mockito Scala](https://github.com/mockito/mockito-scala)}
  from {ScalaTest, MUnit}
- [GoogleTest](https://github.com/google/googletest)
  from {Gradle,
  [Bazel](https://github.com/bazelbuild/bazel),
  [CMake](https://github.com/Kitware/CMake)}

##### Refresher: Build Tools

See [../tools](../tools/README.md)

| | Test Reports | Tips |
|:---|:---|:---|
| sbt | target/test-reports/TEST-\*.xml | sbt:..> shell, _testQuick, testOnly \<pattern\>_ |
| Maven | target/surefire-reports/TEST-*.xml, \*.txt | _--fail-fast, test -Dsuites='\<pattern\>'_ |
| Gradle | build/test-results/test/TEST-\*.xml | _--fail-fast, cleanTest, test --tests \<pattern\>_ |
| Gradle | build/reports/tests/test/index.html | _--no-daemon, --status, --stop_ |

[Up](../README.md)
