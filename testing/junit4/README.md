# polyglot/testing/junit4

#### Topics: A small, cloneable, _standalone_ build project

- _Source + Unit Test Code_ checking out features and usage of
  [JUnit 4](https://junit.org/junit4)
- _Project Code_ for each build tool -
  [sbt](https://www.scala-sbt.org),
  [Maven](https://maven.apache.org),
  [Gradle](https://gradle.org) -
  any of which can be removed
- _Notes_ on the compatibility of build tools, language, and library versions
- _Notes + Code Experiments_ on the used libraries

Quick links: \
[java sources](src/main/java/netropy/polyglot/testing/),
[java tests](src/test/java/netropy/polyglot/testing/junit4/),
[scala tests](src/test/scala/netropy/polyglot/testing/junit4/)

Also see: \
[../junit5](../junit5/README.md),
[../Summary: JUnit 4 (Java)](../Notes_unit_test_frameworks.md#summary-junit-4-java)

#### Summary: {sbt,maven,gradle} X scala{2,3} X {junit4}

[JUnit 4](https://mvnrepository.com/artifact/junit/junit)
is a pure Java library, hence binary-compatible with any Scala version.

| 2022-11 | 2.13 scalac, scala-library | 3.2 scalac (1), scala3-library |
|:--|:--:|:--:|
| junit 4.13.2 (2) | OK | OK |

(1) 2022-11, 2021-11: PROBLEM: Gradle really runs _scalac 2.x <- [zinc] <-
scala plugin_.  No solution yet how to configure running _scalac 3.x_.

(2) 2022-11: for sbt: junit-interface 0.13.2 library.

#### Notes: junit4

Links: \
[JUnit 4](https://junit.org/junit4),
[@github](https://github.com/junit-team/junit4),
[@mvnrepo](https://mvnrepository.com/artifact/junit/junit),
[apidoc](https://junit.org/junit4/javadoc/latest),
[user wiki](https://github.com/junit-team/junit4/wiki),
[FAQ](https://junit.org/junit4/faq.html)

Features:
- _annotations_ for methods and classes to declare and configure tests
- annotations to configure and run test _suites_
- annotations for _parameterized_ tests with field injection
- annotations for test _fixtures_ (fixed baseline state for running tests)
- _assertion_ methods overloaded for all primitive types, objects, arrays
- fluent assertions (_assertThat_) with _matchers_ (core, 3rd party matchers)
- concurrency testing (_assertConcurrent_ takes a collection of Runnables)
- _assumption_ methods (failure causes test abortion)
- annotations for _rules_ (redefinition of behavior) and so-called _theories_
- 100% Java unit test library (hence compatible with all Scala versions)
- core and 3rd party test runners supported by most build tools and IDEs.

A bit of missing "sugar": see helper functions in
[AssertExtJUnit4J](src/test/java/netropy/polyglot/testing/junit4/AssertExtJUnit4J.java),
[AssertExtJUnit4S](src/test/scala/netropy/polyglot/testing/junit4/AssertExtJUnit4S.scala).

JUnit 4 has been superseded by _JUnit 5 "Jupiter"_, which introduced API and
packaging changes, see
[../junit5](../junit5/README.md).

JUnit 5, ScalaTest, and other tools ship a test runner for JUnit 4.

#### Notes: {sbt}+junit4

Tip: Increase junit-interface verbosity (_-v_) and show stacktraces (_-a_):
```
//Test / testOptions += Tests.Argument(TestFrameworks.JUnit, "-a", "-v") // =2
Test / testOptions += Tests.Argument(TestFrameworks.JUnit, "-a", "--verbosity=1")
```

The adapter
[sbt-junit-interface](https://github.com/sbt/junit-interface)
is pure Java, so no Scala version dependencies.

#### Notes: {maven}+junit4

2022-11: Fixed in Scala 3.2.0 \
2021-11: Problem still in Scala 3.1.0 \
2020-11: see
[Scala 3.0.0 Standard Lib breaks code that uses JUnit 4.13 from Maven](https://gitlab.com/netropy/issues-scala/tree/main/scala3-library_3.0.0_breaks_junit_4.13)

#### Notes: {gradle}+junit4

Must register JUnit 4 runner (not needed for sbt or maven):
```
tasks.test { useJUnit() }
```

Tip: JUnit 4/5 runner, default: no console output for passing/skipped tests
- configure: `testLogging { events("passed", "skipped", "failed") }`
- or use plugin for better console logs: `id("com.adarshr.test-logger")`

2022-11, 2021-11: PROBLEM: test runtime error with
[com.adarshr.test-logger 3.1.0](https://plugins.gradle.org/plugin/com.adarshr.test-logger/3.1.0)
```
//   Error: Failed to notify test listener.
//   Caused by: java.lang.AbstractMethodError: Receiver class
//   com.adarshr.gradle.testlogger.logger.SequentialTestLogger does not
//   define or inherit an implementation of the resolved method
//   'abstract java.lang.Object getProperty(java.lang.String)'
//   of interface groovy.lang.GroovyObject.
//id("com.adarshr.test-logger") version "3.1.0"
id("com.adarshr.test-logger") version "2.1.1"
```

[Up](../README.md)
