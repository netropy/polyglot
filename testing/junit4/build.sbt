name := "polyglot.testing.junit4"
organization := "netropy"
description := "Example, Scala, Scala3, Java, JUnit4, sbt, gradle, maven"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: in case of interleaved console output for JUnit tests, set
//   Test / parallelExecution := false
//   Test / logBuffered := true
// https://www.scala-sbt.org/1.x/docs/Testing.html

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

// OK: Scala 3, 2.13, 2.12
//scalaVersion := "2.12.20"
//scalaVersion := "2.13.15"
scalaVersion := "3.3.4"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  //"-Ytasty-reader",  // scala2.13
  //"-explaintypes",  // scala2
  //"-explain",  // scala3
)

// Resolvers (pre- or user-defined)
// https://www.scala-sbt.org/1.x/docs/Resolvers.html
//resolvers += DefaultMavenRepository  // inluded by default
//resolvers += Resolver.mavenLocal  // ~/.m2

// junit-interface: Implementation of sbt's test interface for JUnit 4
// https://mvnrepository.com/artifact/com.github.sbt/junit-interface
// https://github.com/sbt/junit-interface
// https://junit.org/junit4/
// junit <= 4.11:
//libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"
// junit >= 4.12:
libraryDependencies += "com.github.sbt" % "junit-interface" % "0.13.3" % Test

// JUnit4 framework options, better add this:
// -v verbosity=2, not documented what 2 means, may decrease verbosity
// -a make junit-interface show stacktraces for junit AssertionErrors
// Note: +a +v etc turns verbosity off
// sbt> testOnly -- -a *pattern*
Test / testOptions += Tests.Argument(TestFrameworks.JUnit, "-a", "--verbosity=1")

// https://www.scala-sbt.org/1.x/docs/Howto-Logging.html

// only use a single thread for building
//parallelExecution := false
// execute tests in the _current_ project serially (default parallel)
//Test / parallelExecution := false

// fork a new JVM for 'run' and 'Test/run'
//fork := true
// fork a new JVM for 'Test/run', but not 'run'
//Test / fork := true
// add a JVM option to use when forking a JVM for 'run'
//javaOptions += "-Xmx8G"

//Test / testOptions := Seq(Tests.Filter(s => s.endsWith("Test")))

// print the output of tests immediately instead of buffering
//Test / logBuffered := false
