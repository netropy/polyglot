package netropy.polyglot.testing;

public class Hello {

    static public String greet(String s) {
        return "Hello " + s;
    }

    static public void main(String[] args) {
        StringBuilder sb = new StringBuilder("[ ");
        for (String s : args) sb.append(s).append(" ");
        System.out.println(greet(sb.append("]").toString()));
    }
}
