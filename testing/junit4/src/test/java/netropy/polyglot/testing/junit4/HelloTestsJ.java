package netropy.polyglot.testing.junit4;

import org.junit.Test;
import static org.junit.Assert.*;

import netropy.polyglot.testing.Hello;

/** Unit-tests the Hello.greet function. */
public class HelloTestsJ {

    @Test
    public void test_propositions() {
        assertTrue("not greeted", Hello.greet("You").startsWith("Hello"));
        assertTrue("not named", Hello.greet("You").endsWith("You"));
    }

    @Test
    public void test_expect_values() {

        // https://junit.org/junit4/javadoc/latest/org/junit/Assert.html
        // assertEquals("msg", exp: Object, act: Object)
        assertEquals("not composed", "Hello You", Hello.greet("You"));
        assertNotEquals("is \"xxx\" ", "xxx", Hello.greet("You"));
        assertNull("expected null", null);
        assertNotNull("expected not-null", "xxx");
        assertSame("expected same", this, this);
        assertNotSame("expected not same", "", this);
    }

    // too much boilerplate for real parameterized tests in JUnit4
    // https://github.com/junit-team/junit4/wiki/Parameterized-tests
    // https://github.com/junit-team/junit4/wiki/Theories
    static public void testOn(String input) {
        assertEquals("Hello " + input, Hello.greet(input));
    }

    @Test
    public void test_parametrized_test() {
        testOn("You");
        testOn("Me");
    }
}
