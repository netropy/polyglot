package netropy.polyglot.testing.junit4;

import org.junit.Test;
import static netropy.polyglot.testing.junit4.AssertExtJUnit4J.*;

/** Unit-tests the assert* extensions. */
public class AssertExtJUnit4JTests {

    @Test
    public void test_assertThrowing0() {
        assertThrowing(
            Exception.class,
            () -> { throw new Exception(); });
    }

    @Test
    public void test_assertThrowing1() {
        assertThrowing(
            AssertionError.class,
            () -> { throw new AssertionError(); });
    }

    @Test
    public void test_assertThrowing2() throws Exception {
        try {
            assertThrowing(Exception.class, () -> {});
            throw new Exception();
        } catch (AssertionError e) {
        }
    }

    @Test
    public void test_assertThrowing3() throws Exception {
        try {
            assertThrowing(AssertionError.class, () -> {});
            throw new Exception();
        } catch (AssertionError e) {
        }
    }

    @Test
    public void test_assertThrowing4() {
        assertThrowing(
            AssertionError.class,
            () -> {
                assertThrowing(Exception.class, () -> {});
                throw new Exception();
            });
    }

    @Test
    public void test_assertThrowing5() {
        assertThrowing(
            AssertionError.class,
            () -> {
                assertThrowing(AssertionError.class, () -> {});
                throw new Exception();
            });
    }

    @Test
    public void test_assertThrowing6() {
        assertThrowing(
            Exception.class,
            () -> { throw new RuntimeException(); });
    }

    @Test
    public void test_assertThrowing7() {
        assertThrowing(
            AssertionError.class,
            () -> {
                assertThrowing(
                    RuntimeException.class,
                    () -> { throw new Exception(); });
            });
    }

    @Test
    public void test_assertResult0() {
        assertResult(0, 0);
    }

    @Test
    public void test_assertResult1() {
        assertThrowing(
            AssertionError.class,
            () -> { assertResult(0, 1); });
    }

    @Test
    public void test_assertResult2() {
        assertThrowing(
            AssertionError.class,
            () -> { assertResult(0, 1L); });
    }

    @Test
    public void test_assertResult3() {
        assertThrowing(
            AssertionError.class,
            () -> { assertResult(0, 1.0); });
    }

    @Test
    public void test_assertResult4() {
        assertThrowing(
            AssertionError.class,
            () -> { assertResult(0, "0"); });
    }

    @Test
    public void test_assertResult5() {
        assertThrowing(
            AssertionError.class,
            () -> { assertResult(null, 0); });
    }
}
