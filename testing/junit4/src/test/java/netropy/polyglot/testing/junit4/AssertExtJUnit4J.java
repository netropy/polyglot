package netropy.polyglot.testing.junit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import org.junit.function.ThrowingRunnable;

/** Assert utility for JUnit4 tests. */
public class AssertExtJUnit4J {

    static public <A> void
    assertResult(A exp, A act) {
        assertEquals(exp, act);
    }

    static public <T extends Throwable> void
    assertThrowing(Class<T> ex, ThrowingRunnable act) {
	assertThrows(ex, act);
    }

    // 2024-10: workaround for gradle 6.9.4 + JDK 17/21 + junit 4.13.2
    // org.junit.runners.model.InvalidTestClassError: no runnable methods
    // no effect to change package/visibility/class/abstract/interface
    @org.junit.Ignore
    @org.junit.Test
    public void test_dummy() {}
}
