package netropy.polyglot.testing.junit4;

import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.*;

/**
 * Code example: JUnit4 unit tests, also see HelloTests.
 *
 * Tests are just a class with @Test-annotated methods.
 * Optionally, suites can be assembled via @RunWith(Suite.class).
 * Test classes must be public, have a no-argument constructor.
 * Test methods must be non-static, no-arg.
 * @see [[https://junit.org/junit4/javadoc/4.13.2/org/junit/package-summary.html]]
 */
public class HowToTestsJ {

    // better, junit >= 4.13
    @Test
    public void test_expect_throwable_with_message() {

        Throwable t = assertThrows(
            java.lang.IllegalArgumentException.class,
            () -> { throw new IllegalArgumentException("usage error"); }
            );

        assertEquals("usage error", t.getMessage());
    }

    // deprecated in junit 4.13
    @Test(expected=java.lang.IllegalArgumentException.class)
    public void test_expect_throwable() {
        throw new IllegalArgumentException("usage error");
    }

    @Test
    public void test_console_output() {
        System.out.println("console output of a test");
    }

    // also needs @Test to be reported as ignored
    @Ignore("optional message")
    @Test
    public void test_ignored_failing_test() {
        fail("too bad");
    }
}
