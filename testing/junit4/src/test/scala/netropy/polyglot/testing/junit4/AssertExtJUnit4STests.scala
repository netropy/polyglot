package netropy.polyglot.testing.junit4

import org.junit.Test
import AssertExtJUnit4S.{assertResult, assertThrowing}

/** Unit-tests the assert* extensions. */
class AssertExtJUnit4STests {

  @Test
  def test_assertThrowing0: Unit =
    assertThrowing(classOf[Exception])(throw new Exception())

  @Test
  def test_assertThrowing1: Unit =
    assertThrowing(classOf[AssertionError])(throw new AssertionError())

  @Test
  def test_assertThrowing2: Unit =
    try {
      assertThrowing(classOf[Exception])(())
      throw new Exception()
    } catch { case e: AssertionError => () }

  @Test
  def test_assertThrowing3: Unit =
    try {
      assertThrowing(classOf[AssertionError])(())
      throw new Exception()
    } catch { case e: AssertionError => () }

  @Test
  def test_assertThrowing4: Unit =
    assertThrowing(classOf[AssertionError]) {
      assertThrowing(classOf[Exception])(())
      throw new Exception()
    }

  @Test
  def test_assertThrowing5: Unit =
    assertThrowing(classOf[AssertionError]) {
      assertThrowing(classOf[AssertionError])(())
      throw new Exception()
    }

  @Test
  def test_assertThrowing6: Unit =
    assertThrowing(classOf[Exception]) {
      throw new RuntimeException()
    }

  @Test
  def test_assertThrowing7: Unit =
    assertThrowing(classOf[AssertionError]) {
      assertThrowing(classOf[RuntimeException])(throw new Exception())
    }

  @Test
  def test_assertResult0: Unit =
    assertResult(0) { 0 }

  @Test
  def test_assertResult1: Unit =
    assertThrowing(classOf[AssertionError]) { assertResult(0) { 1 } }

  @Test
  def test_assertResult2: Unit =
    assertThrowing(classOf[AssertionError]) { assertResult(0) { 1L } }

  @Test
  def test_assertResult3: Unit =
    assertThrowing(classOf[AssertionError]) { assertResult(0) { 1.0 } }

  @Test
  def test_assertResult4: Unit =
    assertThrowing(classOf[AssertionError]) { assertResult(0) { "0" } }

  @Test
  def test_assertResult5: Unit =
    assertThrowing(classOf[AssertionError]) { assertResult(null) { 0 } }
}
