package netropy.polyglot.testing.junit4

import org.junit.Test
import org.junit.Assert._

import netropy.polyglot.testing.Hello

/** Unit-tests the Hello.greet function. */
class HelloTests {

  @Test
  def test_propositions: Unit = {

    assertTrue("not greeted", Hello.greet("You").startsWith("Hello"))
    assertTrue("not named", Hello.greet("You").endsWith("You"))
  }

  @Test
  def test_expect_values: Unit = {

    // https://junit.org/junit4/javadoc/latest/org/junit/Assert.html
    // assertEquals("msg", exp: Object, act: Object)
    assertEquals("not composed", "Hello You", Hello.greet("You"))
    assertNotEquals("""is "xxx" """, "xxx", Hello.greet("You"))
    // no support for multi-line strings by junit4, use hamcrest matchers?
  }

  // too much boilerplate for real parameterized tests in JUnit4
  // https://github.com/junit-team/junit4/wiki/Parameterized-tests
  // https://github.com/junit-team/junit4/wiki/Theories
  @Test
  def test_parametrized_test: Unit = {

    def testOn(input: String): Unit = {
      assertEquals(s"Hello $input", Hello.greet(input))
    }

    testOn("You")
    testOn("Me")
  }
}
