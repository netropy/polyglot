package netropy.polyglot.testing.junit4

import org.junit.{Test,Ignore}
import org.junit.Assert._

/** Code example: JUnit4 unit tests, also see HelloTests.
  *
  * Tests are just a class with @Test-annotated methods.
  * Optionally, suites can be assembled via @RunWith(Suite.class).
  * A test suite cannot be an `object` (as per junit).
  * Test classes must have a no-argument constructor.
  * Test methods must be parameterless or nullary.
  *
  * @see [[https://junit.org/junit4/javadoc/4.13.2/org/junit/package-summary.html]]
  */
class HowToTests {

  // better, junit >= 4.13
  @Test
  def test_expect_throwable_with_message: Unit = {

    val t: Throwable = assertThrows(
      classOf[java.lang.IllegalArgumentException],
      () => throw new IllegalArgumentException("usage error")
    )

    assertEquals("usage error", t.getMessage());
  }

  // deprecated in junit 4.13
  @Test(expected=classOf[java.lang.IllegalArgumentException])
  def test_expect_throwable: Unit = {
    throw new IllegalArgumentException("usage error")
  }

  @Test
  def test_console_output: Unit = {
    println("console output of a test")
  }

  // also needs @Test to be reported as ignored
  @Ignore("optional message")
  @Test
  def test_ignored_failing_test: Unit = {
    fail("too bad")
  }
}
