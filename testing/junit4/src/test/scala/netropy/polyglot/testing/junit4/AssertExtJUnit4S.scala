package netropy.polyglot.testing.junit4

import org.junit.Assert.{assertEquals, assertThrows}
import org.junit.function.ThrowingRunnable

/** Assert utility for JUnit4 tests. */
object AssertExtJUnit4S {

  // 2023-01: PROBLEM  gradle's scalac incorrectly reports compile errors:
  //   => type mismatch; found: String("0"), required: Int
  //      assertResult(0)("0")
  //      def assertResult[A](exp: => A)(act: => A)
  //   => of course, type parameter A should resolve to Any, not Int
  //   => workaround:
  //      def assertResult[A, B](exp: => A)(act: => B)
  def assertResult[A, B](exp: => A)(act: => B) =
   assertEquals(act, exp)

  def assertThrowing[T <: Throwable, A](ex: Class[T])(expr: => A): Unit =
    assertThrows(ex, () => expr)
}
