# polyglot/testing

### Refcard: Basic APIs, Annotations, DSLs of Unit Test Frameworks

Also see [Summaries & Notes](Notes_unit_test_frameworks.md).

| Project | Code | Test Suites | Tests (with static data) | Parameterized Tests (with specified data) | Property Tests (with generated data) | Assertions |
|:---|:---|:---|:---|:---|:---|:---|
| [./pojo\_assertion\_tests][10a] | [scala][10b] | No-arg Java/Scala _class_ | No-arg Java/Scala test method | Parameterized Java/Scala method, then call with data points from no-arg test method | Parameterized Java/Scala method, write data generators, and call from no-arg test method | Java/Scala _assert_ |
| [./junit4][20a] | [java][20b], [scala][20c] | optional [@FixMethodOrder][21d], No-arg Java/Scala _class_ | [@Test][21a] no-arg Java/Scala method | Parameterized Java/Scala method, then call with data points from a [@Test][21a] method | Parameterized Java/Scala method, write data generators, and call from a [@Test][21a] method | Java/Scala _assert_, [Assert][22a] see [API][21b], [Matchers][22b], [Assume][21c] |
| [./junit5][30a] | [java][30b], [scala][30c] | optional [@TestMethodOrder][31g] (or other), Java/Scala _class_, parameters resolved via [dependency injection][32d] | [@Test][31a] Java/Scala method, parameters resolved via [dependency injection][32d] | [@ParameterizedTest][31c] Java/Scala method, specify [data points][32c] with [@ValueSource][31d] or [other][31e]; can also write a [@TestFactory][31h] method for a [dynamic test][32e] | Parameterized Java/Scala method, write data generators, and call from a [@Test][31a] method, or write a [@TestFactory][31h] method for a [dynamic test][32e]; can try out experimental [RandomParametersExtension][33] | Java/Scala _assert_, [Assertions][32a] see [API][31b], [Matchers][32b], [Assumptions][31f] |
| [./munit][40a] | [scala][40b] | Java/Scala _class extends_ [FunSuite][41a] | [test("...") {...}][42a] | write a helper function with local test: [def testXYZ(...)(implicit loc: Location) = { test(...){...} }][42c] and call the helper on each data point | can use ScalaCheck, see [munit_scalacheck][60a] | Scala _assert_, [Assertions][42b] see [API][41b] |
| [./scalacheck][50a] | [scala][50b] | Scala _object/class extends_ [Properties][51a] | __typically not needed;__ [property("...") = exists {() => ...}][51b] | __typically not needed;__ [difficult][51c] _to write a non-arbitrary (non-random) custom data_ [Generator][52b] |  [property("...") = forAll {(...) => ...}][52a] or other constructors; predefined, composable data [Generators][51d] for various distributions | typically no use of asserts, returning _Boolean_ |
| [./munit\_scalacheck][60a] | [scala][60b] | Scala _class extends_ [ScalaCheckSuite][61] | __typically not needed;__ [test("...") {...}][42a] | __typically not needed;__ can write a helper function with local test: [def testXYZ(...)(implicit loc: Location) = { test(...){...} }][42c] and call the helper on each data point | [property("...") {forAll {(...) => ...}}][62] or other constructors; predefined, composable data [Generators][51d] for various distributions | Scala _assert_, [Assertions][42b] see [API][41b] |
| [./scalatest][70a] | [scala][70b] | Scala _class_ extends: [AnyFunSuite][71b], [AnyFlatSpec][71c], [AnyFeatureSpec][71d], [AnyFunSpec][71e], [AnyWordSpec][71f], [AnyPropSpec][71a], or [other][71j], see [guide][72a] | [test("...") {...}][72a], ["..." should "..." in {...}][72a], [feature("...") {scenario("...") {...}}][72a], [describe("...") {it("...") {...}}][72a], ["..." when { "..." should { "..." in {...}}}][72a], [Given("...")... When("...")... Then("...")... And("...")...][71g], or [other][71j] | Scala _class_ extends [AnyPropSpec][71a] with [TableDrivenPropertyChecks][71h], [property("...") {forAll(...: Table) {... => ...}}][72a] | can use ScalaCheck, see [scalatest_scalacheck][80a] | Scala _assert_, [Assertions][72b] see [API][71i], [Matchers][72c] |
| [./scalatest\_scalacheck][80a] | [scala][80b] | Scala _class_ extends: [AnyFunSuite][71b], [AnyFlatSpec][71c], [AnyFeatureSpec][71d], [AnyFunSpec][71e], [AnyWordSpec][71f], [AnyPropSpec][71a], or [other][71j], see [guide][72a] | __typically not needed;__ [test("...") {...}][72a], ["..." should "..." in {...}][72a], [feature("...") {scenario("...") {...}}][72a], [describe("...") {it("...") {...}}][72a], ["..." when { "..." should { "..." in {...}}}][72a], [Given("...")... When("...")... Then("...")... And("...")...][71g], or [other][71h] | __typically not needed;__ Scala _class_ extends [AnyPropSpec][71a] with [ScalaCheckPropertyChecks][81b], [property("...") {forAll(...: Table) {... => ...}}][72a] | Scala _class_ extends [AnyPropSpec][71a] with [ScalaCheckDrivenPropertyChecks][81a], [property("...") {forAll {(...) => ...}}][72a] | Scala _assert_, [Assertions][72b] see [API][71i], [Matchers][72c] |

[10a]: ./pojo_assert/README.md
[10b]: ./pojo_assert/src/test/scala/netropy/polyglot/testing/pojo_assert

[20a]: ./junit4/README.md
[20b]: ./junit4/src/test/java/netropy/polyglot/testing/junit4
[20c]: ./junit4/src/test/scala/netropy/polyglot/testing/junit4
[21a]: https://junit.org/junit4/javadoc/4.13/org/junit/Test.html
[21b]: https://junit.org/junit4/javadoc/4.13/org/junit/Assert.html
[21c]: https://junit.org/junit4/javadoc/4.13/org/junit/Assume.html
[21d]: https://junit.org/junit4/javadoc/4.13/org/junit/FixMethodOrder.html
[22a]: https://github.com/junit-team/junit4/wiki/Assertions
[22b]: https://github.com/junit-team/junit4/wiki/Matchers-and-assertthat

[30a]: ./junit5/README.md
[30b]: ./junit5/src/test/java/netropy/polyglot/testing/junit5
[30c]: ./junit5/src/test/scala/netropy/polyglot/testing/junit5
[31a]: https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/Test.html
[31b]: https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/Assertions.html
[31c]: https://junit.org/junit5/docs/current/api/org.junit.jupiter.params/org/junit/jupiter/params/ParameterizedTest.html
[31d]: https://junit.org/junit5/docs/current/api/org.junit.jupiter.params/org/junit/jupiter/params/provider/ValueSource.html
[31e]: https://junit.org/junit5/docs/current/api/org.junit.jupiter.params/org/junit/jupiter/params/provider/package-summary.html
[31f]: https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/Assumptions.html
[31g]: https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/TestMethodOrder.html
[31h]: https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/TestFactory.html
[32a]: https://junit.org/junit5/docs/current/user-guide/#writing-tests-assertions
[32b]: https://junit.org/junit5/docs/current/user-guide/#writing-tests-assertions-third-party
[32c]: https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests
[32d]: https://junit.org/junit5/docs/current/user-guide/#writing-tests-dependency-injection
[32e]: https://junit.org/junit5/docs/current/user-guide/#writing-tests-dynamic-tests
[33]: https://github.com/junit-team/junit5-samples/blob/r5.9.1/junit5-jupiter-extensions/src/main/java/com/example/random/RandomParametersExtension.java

[40a]: ./munit/README.md
[40b]: ./munit/src/test/scala/netropy/polyglot/testing/munit
[41a]: https://javadoc.io/static/org.scalameta/munit_3/0.7.29/api/munit/FunSuite.html
[41b]: https://javadoc.io/static/org.scalameta/munit_3/0.7.29/api/munit/Assertions.html
[41c]: https://javadoc.io/static/org.scalameta/munit_3/0.7.29/api/munit/Location.html
[42a]: https://scalameta.org/munit/docs/tests.html
[42b]: https://scalameta.org/munit/docs/assertions.html
[42c]: https://scalameta.org/munit/docs/tests.html#declare-tests-inside-a-helper-function

[50a]: ./scalacheck/README.md
[50b]: ./scalacheck/src/test/scala/netropy/polyglot/testing/scalacheck
[51a]: https://javadoc.io/doc/org.scalacheck/scalacheck_3/latest/org/scalacheck/Properties.html
[51b]: https://javadoc.io/doc/org.scalacheck/scalacheck_3/latest/org/scalacheck/Prop$.html
[51c]: https://javadoc.io/doc/org.scalacheck/scalacheck_3/latest/org/scalacheck/Gen.html
[51d]: https://javadoc.io/doc/org.scalacheck/scalacheck_3/latest/org/scalacheck/Gen$.html
[52a]: https://github.com/typelevel/scalacheck/blob/main/doc/UserGuide.md#properties
[52b]: https://github.com/typelevel/scalacheck/blob/main/doc/UserGuide.md#generators

[60b]: ./munit_scalacheck/src/test/scala/netropy/polyglot/testing/munit_scalacheck
[60a]: ./munit_scalacheck/README.md
[61]: https://scalameta.org/munit/docs/integrations/scalacheck.html
[62]: https://scalameta.org/munit/blog/2020/03/24/scalacheck.html

[70a]: ./scalatest
[70b]: ./scalatest/src/test/scala/netropy/polyglot/testing/scalatest
[71a]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/propspec/AnyPropSpec.html
[71b]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/funsuite/AnyFunSuite.html
[71c]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/flatspec/AnyFlatSpec.html
[71d]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/featurespec/AnyFeatureSpec.html
[71e]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/funspec/AnyFunSpec.html
[71f]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/wordspec/AnyWordSpec.html
[71g]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/GivenWhenThen.html
[71h]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/prop/TableDrivenPropertyChecks.html
[71i]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/Assertions.html
[71j]: https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/index.html
[72a]: https://www.scalatest.org/user_guide/selecting_a_style
[72b]: https://www.scalatest.org/user_guide/using_assertions
[72c]: https://www.scalatest.org/user_guide/using_matchers

[80a]: ./scalatest_scalacheck
[80b]: ./scalatest_scalacheck/src/test/scala/netropy/polyglot/testing/scalatest_scalacheck

[81a]: https://www.scalatest.org/scaladoc/plus-scalacheck-1.17/3.2.14.0/org/scalatestplus/scalacheck/ScalaCheckDrivenPropertyChecks.html
[81b]: https://www.scalatest.org/scaladoc/plus-scalacheck-1.17/3.2.14.0/org/scalatestplus/scalacheck/ScalaCheckPropertyChecks.html

[Up](./README.md)
