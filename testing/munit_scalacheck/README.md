# polyglot/testing/munit_scalacheck

#### Topics: A small, cloneable, _standalone_ build project

- _Source + Unit Test Code_ checking out features and usage of
  [MUnit](https://scalameta.org/munit) with
  [ScalaCheck](https://www.scalacheck.org)
- _Project Code_ for each build tool -
  [sbt](https://www.scala-sbt.org),
  [Maven](https://maven.apache.org),
  [Gradle](https://gradle.org) -
  any of which can be removed
- _Notes_ on the compatibility of build tools, language, and library versions
- _Notes + Code Experiments_ on the used libraries

Quick links: \
[scala sources](src/main/scala/netropy/polyglot/testing/),
[scala tests](src/test/scala/netropy/polyglot/testing/munit_scalacheck/)

Also see: \
[../scalacheck](../scalacheck/README.md),
[../munit](../munit/README.md),
[../Summary: ScalaCheck](../Notes_unit_test_frameworks.md#summary-scalacheck),
[../Summary: MUnit (Scala)](../Notes_unit_test_frameworks.md#summary-munit-scala)

#### Summary: {sbt,maven,gradle} X scala{2,3} X {munit_scalacheck}

The integration library
[MUnit-ScalaCheck](https://index.scala-lang.org/scalameta/munit/artifacts/munit-scalacheck),
which pulls
[MUnit](https://index.scala-lang.org/scalameta/munit) and
[ScalaCheck](https://index.scala-lang.org/typelevel/scalacheck),
is available as Scala 2.11..13 and 3.x binary.

| 2022-11 | 2.13 scalac, scala-library | 3.2 scalac (1), scala3-library |
|:--|:--:|:--:|
| munit-scalacheck\_2.13 0.7.29 | OK | Errors (5) |
| munit-scalacheck\_3 0.7.29 | Errors (2)(3)(4) | OK |

(1) 2022-11, 2021-11: PROBLEM: Gradle really runs _scalac 2.x <- [zinc] <-
scala plugin_.  No solution yet how to configure running _scalac 3.x_.

(2) 2022-11: scalac 2.13 requires additional import of
[scala-reflect 2.13](https://mvnrepository.com/artifact/org.scala-lang/scala-reflect).

(3) 2022-11: scalac 2.13 can consume Scala3 libs via _-Ytasty-reader_, see
[TASTy](https://docs.scala-lang.org/scala3/guides/tasty-overview.html).

(4) 2022-11, 2021-11: Problems with cross-platform use 2.13 <- 3, see comments
in build.sbt.

(5) 2022-11, 2021-11: Problems with cross-platform use 2.13 -> 3, see comments
in build.sbt.

#### Notes: munit-scalacheck

Links: \
[MUnit-ScalaCheck](https://scalameta.org/munit/docs/integrations/scalacheck.html),
[@github](https://github.com/scalameta/munit/tree/main/munit-scalacheck/shared/src/main/scala/munit),
[@scaladex](https://index.scala-lang.org/scalameta/munit/artifacts/munit-scalacheck),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalameta/munit-scalacheck),
[blog](https://scalameta.org/munit/blog/2020/03/24/scalacheck.html),
no API docs (browse github sources)

Links: \
[MUnit](https://scalameta.org/munit),
[@github](https://github.com/scalameta/munit),
[@scaladex](https://index.scala-lang.org/scalameta/munit),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalameta/munit),
[quick start](https://scalameta.org/munit/docs/getting-started.html),
no API docs (browse github sources)

Links: \
[ScalaCheck](https://www.scalacheck.org),
[@github](https://github.com/typelevel/scalacheck),
[@scaladex](https://index.scala-lang.org/typelevel/scalacheck),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalacheck),
[apidoc](https://javadoc.io/doc/org.scalacheck/scalacheck_3/latest/index.html),
[user guide](https://github.com/typelevel/scalacheck/blob/main/doc/UserGuide.md)

Base class _ScalaCheckSuite_ allows to write ScalaCheck property tests, which
can use MUnit assertions:
```
import munit.ScalaCheckSuite
import org.scalacheck.Prop._

class IntegerSuite extends ScalaCheckSuite {

  property("integer identities") {
    forAll { (n: Int) =>
	  (n + 0 == n) && (n * 1 == n)  // combined test expression
    }
  }

  property("integer identities with munit assertions") {
    forAll { (n: Int) =>
      assertEquals(n + 0, n)        // better error reporting
      assertEquals(n * 1, n)
    }
  }
}
```

_Practical:_ Existing ScalaCheck property definitions can be re-used
without porting (and alongside new MUnit properties). \
See _trait PropertiesAdapter_ in
[HelloTests.scala](src/test/scala/netropy/polyglot/testing/munit_scalacheck/HelloTests.scala)

#### Notes: {gradle}+munit-scalacheck

2022-11: no longer needed to register MUnit's JUnit 4 test runner
```
tasks.test { useJUnit() }
```

[Up](../README.md)
