package netropy.polyglot.testing.munit_scalacheck

import netropy.polyglot.testing.Hello

import munit.ScalaCheckSuite
import org.scalacheck.Prop.forAll

/*
 * Notes: MUnit + ScalaCheck  vs  MUnit + registered native/legacy ScalaCheck
 *
 * New MUnit + ScalaCheck + assertions property tests:
 *   class HelloTests extends ...
 *     property("...") { forAll { assert(...) } }
 *
 * Existing native/legacy ScalaCheck properties (propositions, no asserts):
 *   object HelloScalaCheckLegacyProperties extends ...
 *     property("...") = forAll { ... }
 *
 * MUnit + registered native/legacy ScalaCheck properties tests:
 *   class HelloScalaCheckPropertiesTests extends ...
 *     include(HelloScalaCheckLegacyProperties)  // adapter function, see below
 *
 * see: https://scalameta.org/munit/docs/integrations/scalacheck.html
 */

/** Unit-tests the Hello.greet function.
  * Property tests with MUnit + ScalaCheck + assertions.
  */
class HelloTests extends ScalaCheckSuite {

  property("Hello's greeting starts with 'Hello'") {
    forAll { (s: String) =>
      assert(Hello.greet(s).startsWith("Hello"), "not greeted")
    }
  }

  property("Hello greeter composes") {
    forAll { (s: String) =>
      assertEquals(Hello.greet(s), s"Hello $s", "not composed")
    }
  }
}

// Show how to register native/legacy ScalaCheck properties...
import org.scalacheck.Properties

/** Adapts native/legacy Properties.property ~> ScalaCheckSuite.property. */
trait PropertiesAdapter { self: ScalaCheckSuite =>

  def include(ps: Properties): Unit =
    for ((name, prop) <- ps.properties) property(name)(prop)
}

/** Unit-tests the Hello.greet function.
  * Native/legacy ScalaCheck properties (propositions, no asserts).
  */
object HelloScalaCheckLegacyProperties
    extends Properties("Hello") {

  property("ScalaCheckLegacy: Hello's greeting starts with 'Hello'") =
    forAll { (s: String) =>
      Hello.greet(s).startsWith("Hello")
    }

  property("ScalaCheckLegacy: Hello greeter composes") =
    forAll { (s: String) =>
      Hello.greet(s) == s"Hello $s"
    }
}

/** Unit-tests the Hello.greet function.
  * Property tests with MUnit + registered native/legacy ScalaCheck properties.
  */
class HelloScalaCheckPropertiesTests
    extends ScalaCheckSuite
    with PropertiesAdapter {

  include(HelloScalaCheckLegacyProperties)
}
