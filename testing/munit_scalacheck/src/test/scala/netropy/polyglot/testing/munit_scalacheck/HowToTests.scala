package netropy.polyglot.testing.munit_scalacheck

import munit.ScalaCheckSuite
import org.scalacheck.Prop
import org.scalacheck.Prop.all
import org.scalacheck.Prop.forAll
import org.scalacheck.Prop.falsified  // alternative: munit.Assertions.fail()
import org.scalacheck.Prop.passed  // scala3, no implicit: () => passed
//import munit.Clue.generate  // does not add implicit () => passed

// ----------------------------------------------------------------------
// Note: ScalaCheckSuite.property() behaves like test()
// ----------------------------------------------------------------------

/** Code example: ScalaCheck properties tests, also see HelloTests.
  *
  * Tests must be defined in a class, not an object.  Otherwise:
  * - maven: skips this test, doesn't recognize
  * - gradle: IllegalArgumentException:
  *   Class '...HowToTests$' is missing a public empty argument constructor
  * - sbt: org.junit.runners.model.InvalidTestClassError: '...HowToTests'
  */
class HowToTests extends ScalaCheckSuite {

  test("test() can behave like property()") {
    forAll { (s: String) => s.length >= 0 }
  }

  property("Boolean property behaves like static test()") {
    Prop("a".length > 0)
  }

  property("Proposition: String concatenation is associative") {
    forAll { (s: String, t: String, u: String) =>
      ((s + t) + u) == (s + (t + u))
    }
  }

  property("Assertion: String concatenation is associative") {
    forAll { (s: String, t: String, u: String) =>
      assert(((s + t) + u) == (s + (t + u)), "not associative")
    }
  }

  // or define property values and group them for running together

  val propStringConcatStartsWith =
    forAll { (s: String, t: String) => (s + t).startsWith(s) }

  val propStringConcatEndsWith =
    forAll { (s: String, t: String) => (s + t).endsWith(t) }

  property("String concatenation starts/ends with") {
    all(propStringConcatStartsWith, propStringConcatEndsWith)
  }
}

// some test reporters (gradle) seem to rely on (globally?) unique test names

// incorrect doc: test class can have more that one public constructor
class HowToHaveMultipleConstructors(i: Int)
    extends ScalaCheckSuite with Runnable {

  // test class must have a "public empty argument constructor"
  def this() = {
    this(0)
  }

  // incorrect doc: test class can have "runnable" (?) methods
  def run(): Unit = ()

  property("no op") { passed }
}

class HowToIgnoreTagSomePropertyTests extends ScalaCheckSuite {

  property("ignored property#0".ignore) { fail("bad") }  // gradle reports 2x?

  property("ignored property#1".ignore) { falsified }  // gradle reports 2x?

  property("tagged property#2".flaky) { passed }  // runs by default

  property("conditional property#3") {
    assume(false, "disable the rest of this test per dynamic condition")
    fail("too bad")
  }
}

class HowToSelectFailSomePropertyTests extends ScalaCheckSuite {

  property("selected property#4".only) { passed }  // scala2, just: ()

  property("failing property#5".fail) { passed }  // scala2, just: ()
}
