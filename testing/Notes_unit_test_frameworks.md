# polyglot/testing

### Summaries & Notes: Unit Test Frameworks

[Wikipedia](https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks)
already lists ~80 libraries and frameworks for _unit- and
integration-testing._

Here, are my notes and views on libraries that I've worked with, had a look
at, or for which I just collected links.

##### _TOC:_ Summaries of frameworks with which _I've worked:_

[Summary: POJO Assertion Tests (Java, Scala)](#summary-pojo-assertion-tests-java-scala) \
[Summary: JUnit 4 (Java)](#summary-junit-4-java) \
[Summary: JUnit 5 (Java)](#summary-junit-5-java) \
[Summary: MUnit (Scala)](#summary-munit-scala) \
[Summary: ScalaCheck](#summary-scalacheck) \
[Summary: ScalaTest](#summary-scalatest) \
[Summary: ScalaMock](#summary-scalamock) \
[Summary: GoogleTest (C++)](#summary-googletest-c)

Note that Java libraries can be easily used from Scala, though, not always
idiomatically.

##### _TOC:_ Notes on frameworks at which _I've taken a look:_

[Notes: Runtime Checks with Google Guava (Java)](#notes-runtime-checks-with-google-guava-java) \
[Notes: Fluent Assertions with Google Truth (Java)](#notes-fluent-assertions-with-google-truth-java) \
[Notes: Fluent Assertions with AssertJ (Java)](#notes-fluent-assertions-with-assertj-java) \
[Notes: specs2 Executable Software Specifications (Scala)](#notes-specs2-executable-software-specifications-scala) \
[Notes: µTest by Li Haoyi (Scala)](#notes-µtest-by-li-haoyi-scala)

##### _TOC:_ Other test frameworks, just listed here:

Assertions/matchers libraries:

[Hamcrest](https://github.com/hamcrest),
[@mvnrepo](https://mvnrepository.com/artifact/org.hamcrest):
Matchers that can be combined to create flexible expressions of intent (~13
languages) \
[Expecty](https://github.com/eed3si9n/expecty),
[@mvnrepo](https://mvnrepository.com/artifact/com.eed3si9n.expecty/expecty):
Power assertions (as known from Groovy and Spock) for Scala

Property testing frameworks:

[Hedgehog](https://github.com/hedgehogqa/scala-hedgehog),
[@mvnrepo](https://mvnrepository.com/artifact/qa.hedgehog):
A property-based testing system for Scala \
[Vavr-test](https://github.com/vavr-io/vavr-test),
[@mvnrepo](https://mvnrepository.com/artifact/io.vavr/vavr-test):
A Java property check framework for
[Vavr](https://www.vavr.io) \
[Scalaprops](https://github.com/scalaprops/scalaprops),
[@mvnrepo](https://mvnrepository.com/artifact/com.github.scalaprops):
Property-based testing library for Scala \
[discipline](https://github.com/typelevel/discipline),
[@mvnrepo](https://typelevel.org/blog/2013/11/17/discipline.html):
Flexible law checking for Scala (discipline-{core,munit,scalatest,specs2})

Mocking frameworks:

[Mockito](https://github.com/mockito/mockito),
[@mvnrepo](https://mvnrepository.com/artifact/org.mockito):
"Most popular mocking framework for Java", see
[Mockito vs EasyMock](https://github.com/mockito/mockito/wiki/Mockito-vs-EasyMock) \
[Mockito Scala](https://github.com/mockito/mockito-scala),
[@mvnrepo](https://mvnrepository.com/artifact/org.mockito/mockito-scala):
Mockito for Scala language by independent developers \
[EasyMock](https://github.com/easymock/easymock),
[@mvnrepo](https://mvnrepository.com/artifact/org.easymock):
A Java library that provides an easy way to use Mock Objects in unit testing \
[jMock](https://github.com/jmock-developers/jmock-library),
[@mvnrepo](https://mvnrepository.com/artifact/org.jmock):
A library that supports test-driven development of Java code with mock objects

Other test frameworks:

[Test-State](https://github.com/japgolly/test-state),
[@mvnrepo](https://mvnrepository.com/artifact/com.github.japgolly.test-state):
"Test stateful stuff statelessly, and reasonably" \
[Minitest](https://github.com/monix/minitest),
[@mvnrepo](https://mvnrepository.com/artifact/io.monix/minitest):
A Scala{2.x,3,.JS} test library by
[Monix](https://monix.io/) inspired by
[Ruby's Minitest](https://github.com/minitest/minitest)? \
[Verify](https://github.com/eed3si9n/verify)
[@mvnrepo](https://mvnrepository.com/artifact/com.eed3si9n.verify/verify):
Minimalist unit testing framework, origins in Minitest+Expecty+SourceCode

----------------------------------------------------------------------

#### Summary: "POJO" Assertion Tests (Java, Scala)

[POJO (plain old Java object) tests](https://maven.apache.org/surefire/maven-surefire-plugin/examples/pojo-test.html)
do not rely on an external unit testing framework.

POJO tests
- are plain methods that adhere to certain _naming and signature_ rules;
- indicate failure by throwing an exception, mostly by use of _assert_;
- __[+]__ avoid a consequential, 3rd-party, source + binary dependency;
- __[–]__ have to make do without the test library's assert functions;
- __[–]__ cannot rely on test framework features (e.g., parameterized tests);
- __[–]__ lack build tool support (e.g., sbt and Gradle lack test runners).

_My view of POJO tests:_ not worth it (lack of support, tool trouble).

For notes, code examples, and compatibility with build tools, see project: \
[./pojo_assert](pojo_assert/README.md)

#### Summary: JUnit 4 (Java)

[JUnit 4](https://junit.org/junit4/)
(or just _JUnit_),
[@github](https://github.com/junit-team/junit4),
[@mvnrepo](https://mvnrepository.com/artifact/junit/junit),
[apidoc](https://junit.org/junit4/javadoc/latest),
is a still widely used Java unit testing library supported by most build tools
and IDEs.

JUnit 4
- follows the object-oriented
  [xUnit](https://en.wikipedia.org/wiki/XUnit)
  design of: Test{ runner, case, fixtures, suites, execution, result formatter
  }, see
  [1](https://github.com/google/googletest/blob/main/docs/primer.md#basic-concepts),
  [2](https://github.com/google/googletest/blob/main/docs/primer.md#beware-of-the-nomenclature);
- has a medium-sized API of ~20 packages, of which ~6 are relevant to writing
  and configuring tests (including matchers);
- has a _small_ core package
  [org.junit](https://junit.org/junit4/javadoc/latest/org/junit/package-summary.html)
  with just 15 type members: 9 Java annotations, ~4 exception types, 2 helper
  classes;
- offers a basic (but sometimes sufficient) set of features, covering
  assertions and fixtures, but not parameterized tests;
- is supported by virtually all build tools and IDEs due to its long history
  and _widespread use;_
- has been superseded by JUnit 5 "Jupiter".

_My view of JUnit 4:_ limited feature set, outdated APIs.

For notes and Java + Scala code examples, see project: \
[./junit4](./junit4/README.md)

#### Summary: JUnit 5 (Java)

[JUnit 5](https://junit.org/junit5/)
"Jupiter",
[@github](https://github.com/junit-team/junit5/),
[@mvnrepo](https://mvnrepository.com/artifact/org.junit.jupiter),
[apidoc](https://junit.org/junit5/docs/current/api),
is the feature-rich, widely supported successor version of JUnit 4.

Junit 5
- has received many API improvements and functional upgrades over JUnit 4;
- introduced API and packaging changes that require migration of test source
  code and project build code;
- comes with a test runner that automatically picks up JUnit 4 tests, so that
  version 5 and 4 tests can be run together;
- has a _very large surface_ of ca. 20 modules, tens of packages, and
  hundreds of classes, annotations, or interfaces;
- has a rich set of features, covering assertions, fixtures, parameterized
  tests, test extensions, and much more;
- is supported by most build tools and IDEs.

_My view of JUnit 5:_ much improved over JUnit4, too large & complex.

For notes and Java + Scala code examples, see project: \
[./junit5](./junit5/README.md)

#### Summary: MUnit (Scala)

[MUnit](https://scalameta.org/munit),
[@github](https://github.com/scalameta/munit),
[@scaladex](https://index.scala-lang.org/scalameta/munit),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalameta/munit),
(no apidoc)
is a cross-platform unit testing library for Scala or Java programs.

MUnit
- has a _reasonably small & idiomatic_ Scala API, not based on annotations
  but ca. 30 classes, traits, and (companion) objects;
- covers the essential set of unit testing features, including assertions,
  fixtures, parameterized tests etc;
- re-uses JUnit APIs, so any build tool or IDE that knows how to run a JUnit
  test suite can run MUnit;
- has integrations with other tools, e.g.
  [ScalaCheck](https://www.scalacheck.org)
  for support of property testing;
- is based on and hosted on
  [Scalameta](https://scalameta.org/)
  (and in good shape).

_My view of MUnit:_ excellent, quick to learn, integration with ScalaCheck.

For notes, code examples, and compatibility with build tools, see project: \
[./munit](./munit/README.md),
[./munit\_scalacheck](./munit_scalacheck/README.md)

#### Summary: ScalaCheck

[ScalaCheck](https://www.scalacheck.org),
[@github](https://github.com/typelevel/scalacheck),
[@scaladex](https://index.scala-lang.org/typelevel/scalacheck),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalacheck)
[apidoc](https://javadoc.io/doc/org.scalacheck/scalacheck_3/latest/index.html)
is a library for property testing of Scala or Java programs.

ScalaCheck
- offers a large variety of test data generators with various distributions;
- has a _reasonably sized & idiomatic_ Scala API of ca. 30 type members, some
  with many value members (mostly combinators);
- follows a strict, functional programming design (with roots in the Haskell
  library QuickCheck);
- offers _automatic test case minimisation:_ tries to shrink argument failing
  a proposition before reporting;
- alas, has _limited build tool support,_ for example, only _sbt's_ test
  runner, and not _Maven's_ or _Gradle's,_ pick up pure ScalaCheck tests
  (probable cause: tests compile to _static_ fields and methods);
- has integrations with
  [MUnit](https://scalameta.org/munit) and
  [ScalaTest](https://www.scalatest.org),
  which adds support for property testing to these unit-testing frameworks.
- is maintained by
  [Typelevel](https://typelevel.org/)
  (and in good shape since).

_My view of ScalaCheck:_ excellent, feature-rich, best with MUnit.

For notes, code examples, and compatibility with build tools, see project: \
[./scalacheck](./scalacheck/README.md),
[./munit\_scalacheck](./munit_scalacheck/README.md),
[./scalatest\_scalacheck](./scalatest_scalacheck/README.md)

#### Summary: ScalaTest

[ScalaTest](https://www.scalatest.org),
[@github](https://github.com/scalatest/scalatest),
[@scaladex](https://index.scala-lang.org/scalatest),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalatest),
[apidoc](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/index.html)
is a Scala library for unit- and acceptance-style testing of Scala and Java
programs.

ScalaTest
- has a _very large & feature-rich_ Scala API consisting of more than 20
  packages, with hundreds of type and value members;
- covers the essential set of unit testing features, including assertions,
  matchers, fixtures, parameterized tests etc;
- offers ca. 8 different
  [test notation styles](https://www.scalatest.org/user_guide/selecting_a_style)
  to choose from in form of APIs and DSLs (domain-specific languages);
- these styles intend to cover the spectrum from function-style, non-nested
  suites for TDD (test-driven development)...
- ... to spec-style, feature-oriented, nestable DSLs for BDD (behavior-driven
  development) and acceptance testing;
- has own tool plugins (though, it reuses JUnit APIs?), which adds complexity
  to project build code;
- has integrations with other libraries, for example,
  [ScalaCheck](https://www.scalatest.org/plus/scalacheck),
  [Mockito](https://www.scalatest.org/plus/mockito),
  [EasyMock](https://www.scalatest.org/plus/easymock),
  [JMock](https://www.scalatest.org/plus/jmock),
  through _ScalaTestPlus._

ScalaTestPlus links:
[@github](https://github.com/scalatest?q=scalatestplus&type=all),
[@scaladex](https://index.scala-lang.org/search?q=scalatestplus),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalatestplus),
[apidoc](https://javadoc.io/doc/org.scalatestplus).

_My view of ScalaTest:_ most feature-rich, many useful integrations, excellent
documentation, too large & complex, problems with build tool integration
(e.g., ScalaTest from Gradle 6.x).

For notes, code examples, and compatibility with build tools, see project: \
[./scalatest](./scalatest/README.md),
[./scalatest\_scalacheck](./scalatest_scalacheck/README.md),
[./scalatest\_scalamock](./scalatest_scalamock/README.md)

#### Summary: ScalaMock

[ScalaMock](https://scalamock.org),
[@github](https://github.com/paulbutcher/ScalaMock),
[@scaladex](https://index.scala-lang.org/paulbutcher/scalamock),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalamock)
[apidoc](https://www.javadoc.io/doc/org.scalamock/scalamock_2.13/latest/org/scalamock/index.html)
is a stub- and mock-generator for Java and Scala programs.

ScalaMock
- allows to run Scala or Java code against _mocked_ implementations of
  _traits_, _interfaces_, and (certain) _classes_;
- this supports _rapid prototyping_ and unit- and _integration testing_ of
  component interaction;
- supports two usage styles, expectations-first style (using _mock_) and
  record-then-verify (Mockito) Style (using _stub_);
- alas, is _not available as Scala3 binary_ (as of 2022-11),
- alas, lacks integration with
  [MUnit](https://scalameta.org/munit)
  (as of 2022-11).

_My view of ScalaMock:_ feature-rich, largely intuitive, insufficient (API)
documentation, lack of a Scala3 port, no integration with MUnit; see
[./ex_2_3.r](./ex_2_3.r.md).

For notes, code examples, and compatibility with build tools, see project: \
[./scalatest\_scalamock](./scalatest_scalamock/README.md)

#### Summary: GoogleTest (C++)

[GoogleTest](https://google.github.io/googletest/),
[@github](https://github.com/google/googletest),
is an (in my view) _excellent_ library for unit testing and mocking of C++11
(or newer) programs.  It
- follows the object-oriented
  [xUnit](https://en.wikipedia.org/wiki/XUnit)
  design of: Test{ runner, case, fixtures, suites, execution, result formatter
  };
- [offers](https://google.github.io/googletest/reference/assertions.html)
  a rich set of paired _ASSERT\_\*_ (=fatal), _EXPECT\_\*_ (=non-fatal)
  macros, into which _<<_ streams messages;
- [provides](https://google.github.io/googletest/reference/matchers.html)
  fluent-style _ASSERT/EXPECT_THAT_ macros with matchers for floating points,
  strings, containers etc;
- [offers](https://google.github.io/googletest/reference/testing.html)
  _TEST*()_ macros to register tests and a _::testing::Test_ base class to
  derive _test fixtures_ and _value-parameterized tests;_
- [provides](https://google.github.io/googletest/primer.html)
  a _gtest_main()_ function to link with, or to write own _main()_ calling and
  returning the value of _RUN_ALL_TESTS();_
- [offers](https://google.github.io/googletest/gmock_for_dummies.html)
  a library for expectations-first style _mocks_ with
  [actions](https://google.github.io/googletest/reference/actions.html)
  for returns, side-effects, lambdas, composition etc;
- has more: _industrial quality,_
  [features](https://github.com/google/googletest#features)
  for production code (e.g., "death tests"), or related
  [open source projects](https://github.com/google/googletest#related-open-source-projects).

----------------------------------------------------------------------

#### Notes: Runtime Checks with Google Guava (Java)

[Guava](https://guava.dev/),
[@github](https://github.com/google/guava)
[@mvnrepo](https://mvnrepository.com/artifact/com.google.guava), see
[guide](https://github.com/google/guava/wiki),
is a Java library from Google that includes core data types and utilities.

Among these are _reasonable and well-documented_ helper classes for runtime
checks, offering overloaded functions:
- [Preconditions](https://guava.dev/releases/snapshot/api/docs/com/google/common/base/Preconditions.html):
  _check{Argument​, NotNull​, \*Index​*, State}​_ throwing mostly
  _IllegalArgumentException_, see
  [guide](https://github.com/google/guava/wiki/PreconditionsExplained);
- alas, _check*Index -> IndexOutOfBoundsException_ and
  _checkNotNull -> NullPointerException_, partially explained
  [here](https://github.com/google/guava/wiki/IdeaGraveyard#preconditionscheckargumentnotnull-throws-illegalargumentexception);
- [Verify](https://guava.dev/releases/snapshot/api/docs/com/google/common/base/Verify.html):
  _verify​[NotNull]​,_ throwing a _VerifyException_, see
  [guide](https://github.com/google/guava/wiki/ConditionalFailuresExplained)

_My impression of Guava:_ Worth the dependency; probably not for just the
runtime checks, but for other features and quality.

#### Notes: Fluent Assertions with Google Truth (Java)

[Truth](https://truth.dev)
[@github](https://github.com/google/truth),
[@mvnrepo](https://mvnrepository.com/artifact/com.google.truth),
[apidoc](https://truth.dev/api/latest/overview-summary.html),
is a Java library from Google Guava providing _fluent_ assertions for tests:

Yes, expressions from
[fluent APIs](https://en.wikipedia.org/wiki/Fluent_interface),
are much easier to read and faster to compose (autocompletion), see code
example from [1](https://truth.dev):
```
// typical JUnit assertEquals() with Guava collections:
assertEquals(
    ImmutableMultiset.of("guava", "dagger", "truth", "auto", "caliper"),
    HashMultiset.create(projectsByTeam().get("corelibs")));

// with Truth:
assertThat(projectsByTeam())
    .valuesForKey("corelibs")
    .containsExactly("guava", "dagger", "truth", "auto", "caliper");
```

Yes, what makes fluent assertions more readable:
- linear flow, uniformly indented beats parsing nested expressions;
- putting the actual value(s) first sets the context;
- less boilerplate due to richer operations.

_My impression of Truth:_ Worth trying out; but questions:
- How easy to support custom data types?
- How idiomatic the use from Scala?  How easy to work with Scala collections?

Other, interesting topics on the truth website: \
[2](https://truth.dev/):
Examples for how failure messages, test assertions, and complex assertions are
more readable. \
[1](https://truth.dev/), [3](https://truth.dev/comparison):
Discussions "Truth vs. AssertJ" and "Truth vs. Hamcrest", including a few
puzzlers. \
[4](https://truth.dev/known_types),
[5](https://truth.dev/floating_point),
[6](https://truth.dev/extension),
[7](https://truth.dev/faq):
More information on supported data types, comparing floating points, and
more.

#### Notes: Fluent Assertions with AssertJ (Java)

[AssertJ](https://assertj.github.io/doc),
[@github](https://github.com/assertj/assertj),
[@mvnrepo](https://mvnrepository.com/artifact/org.assertj),
[apidoc](https://javadoc.io/doc/org.assertj),
[old site](https://joel-costigliola.github.io/assertj),
is a library for _assertThat-style, fluent_ assertions in Java.

AssertJ is composed of several modules (currently 10 libraries):
- idea: "disposal assertions should be specific to the type of the objects we
  are checking when writing unit tests";
- intended use: "type _assertThat(underTest)._ and use code completion to
  show you all assertions available";
- support of JDK types, Guava types, Joda Time types, Neo4J, relational
  database types, Vavr, Swing;
- support for a custom assertions generator;
- _very large API surface:_
  [core module](https://www.javadoc.io/doc/org.assertj/assertj-core)
  consists of ~20 packages,
  [core.api](https://www.javadoc.io/doc/org.assertj/assertj-core/latest/org/assertj/core/api/package-summary.html)
  of estimated ~200 type members;
- _mega types:_ just the base class
  [AbstractAssert](https://javadoc.io/doc/org.assertj/assertj-core/latest/org/assertj/core/api/AbstractAssert.html)
  consists of estimated ~50 methods, most of them _not overloaded;_
- documentation appears to be mostly in the
  [apidoc](https://javadoc.io/doc/org.assertj),
  there's a tabular overview in the
  [Core assertions guide](https://assertj.github.io/doc/#assertj-core-assertions-guide);
- [AssertJ's goals](https://github.com/assertj/assertj#assertjs-goals)
  section mainly lists the packaging, not much the design philosophy or usage.

Compare with notes on Google Truth, above.

_My impression of AssertJ:_ Sorry, the huge API surface raises concerns.

#### Notes: specs2 Executable Software Specifications (Scala)

[specs2](https://etorreborre.github.io/specs2/),
[@github](https://github.com/etorreborre/specs2),
[@scaladex](https://index.scala-lang.org/etorreborre/specs2),
[@mvnrepo](https://mvnrepository.com/artifact/org.specs2),
[apidoc](https://www.javadoc.io/doc/org.specs2/specs2-core_3/latest/index.html),
is a library for writing BDD-/spec-style unit- and integration tests.

Features, see
[user guide](https://etorreborre.github.io/specs2/guide/SPECS2-5.2.0/org.specs2.guide.UserGuide.html):
- specifications for simple classes (_unit_ specifications) and full features
  (_acceptance_ specifications)
- different spec styles, e.g., unit style specs in Scala code: _"..." >> {
  "..." must ... }_
- specify in both (string-interpolated) text and in Scala code:
  _s2"""...$xyz...""" def xyz = ... must ..._
- hence, can document APIs with compiler-checked examples: _s2""" ${ ... ===
  ...}"""_
- integration with ScalaCheck for properties: _def ... = prop {... => ...}_
- support for contexts/fixtures for integration testing: _class ... extends
  Specification with BeforeSpec with BeforeEach_
- _large API surface:_
  [specs2-core](https://www.javadoc.io/doc/org.specs2/specs2-core_3/latest/index.html)
  consists of ~18 packages with hundreds of type members;
- _mega types:_ just the base class ("lightweight specification")
  [Spec](https://www.javadoc.io/doc/org.specs2/specs2-core_3/latest/org/specs2/Spec.html)
  has ~40 type members, ~250 methods, and ~45 _givens_.

In addition,
[@mvnrepo](https://mvnrepository.com/artifact/org.specs2)
lists 40+ packages for (optional) integrations with other libraries and tools,
for example:
- html/xml export of specifications
- scalamock: to use mocks in specifications
- specs2-junit: to run specifications as JUnit tests
- tons of matchers for various libraries: Cats, Scalaz etc

_My impression of specs2:_ Sorry, the huge API + DSL surface raises concerns.

#### Notes: µTest by Li Haoyi (Scala)

µTest on
[@github](https://github.com/lihaoyi/utest),
[@scaladex](https://index.scala-lang.org/com-lihaoyi/utest),
[@mvnrepo](https://mvnrepository.com/artifact/com.lihaoyi/utest),
is a unit-testing library for Scala, which aims to be small, simple, and
intuitive.

_Discussion:_ uTest introduces a
["simple, uniform syntax"](https://github.com/com-lihaoyi/utest#why-utest)
for various tasks:
- [Defining and grouping/nesting tests](https://github.com/com-lihaoyi/utest#nesting-tests):
  _test("test"){...}_
- Comment: ok, looks somewhat familiar to other property tests frameworks.
- [Running tests and suites](https://github.com/com-lihaoyi/utest#running-tests):
  _foo.BarTests.{baz,qux}_
- Comment: ok, looks familiar to _import_ path statements.
- [Smart Asserts](https://github.com/com-lihaoyi/utest#smart-asserts),
  [Arrow Asserts](https://github.com/com-lihaoyi/utest#arrow-asserts):
  _assert(x > 0, x == y)_ or _a ==> b_ (same as _assert(a == b)_)
- Comment: sorry, the benefits/readability of this new syntax eludes me.
- [Intercept](https://github.com/com-lihaoyi/utest#intercept):
  _val e = intercept[...some Throwable type...]{...action...} ..._
- Comment: ok, this looks familiar to an _assertThrows,_ except for returning
  the throwable to outside test code.
- [Eventually and Continually](https://github.com/com-lihaoyi/utest#eventually-and-continually):
  _eventually(tests: Boolean\*)_ and _continually(tests: Boolean\*)_
- Comment: sorry, wouldn't a retry-loop at configurable intervals introduce
  timing-dependencies into unit tests?
- [Assert Match](https://github.com/com-lihaoyi/utest#assert-match):
  _assertMatch(Seq(1, 2, 3)){case Seq(1, 2) =>}_ (fails, structurally unequal)
- Comment: hmm, not sure what the benefits are of the blended assert over an
  explicit _match_ within an _assert_.

Section
[Why uTest](https://github.com/com-lihaoyi/utest#why-utest)
argues to avoid the mistakes of frameworks that introduce wordy, "fluent
English-like" DSLs. \
Comment: Yes, in the case of
[Scalatest](https://www.scalatest.org);
otherwise, too broad.  Fluent APIs (and sparse DSLs) _can_ make code very
readable.

Other key features as stated on the website:
- nicely formatted, colored, easy-to-read command-line test output
- isolation-by-default for tests in the same suite
- supports every version of Scala, Scala.js and Scala-Native
- sbt or standalone test runner

_My impression of µTest:_ Sorry, \
__[-]__ I find some of the peculiar assertion syntax _not adding_ clarity and
readability; \
__[-]__ none of the above syntax example convinces me of the _costs/benefits
ratio_ of learning this new framework.

----------------------------------------------------------------------

[Up](./README.md)
