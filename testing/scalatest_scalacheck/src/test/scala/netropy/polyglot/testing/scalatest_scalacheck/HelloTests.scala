package netropy.polyglot.testing.scalatest_scalacheck

import netropy.polyglot.testing.Hello

// ScalaTest 3.1.0 reorganized names
import org.scalatest.propspec.AnyPropSpec
import org.scalatest.diagrams.Diagrams

// See HowToTests.scala:
//   runs checks against data from ScalaCheck generators
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
//   extends ScalaCheckDrivenPropertyChecks for data supplied by tables
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

/** Unit-tests Hello.greet: property test with generated data (<-ScalaCheck). */
class HelloPropSpecGenData
    extends AnyPropSpec
    with ScalaCheckDrivenPropertyChecks
    with Diagrams {

  property("Generated data: Hello's greeting starts with 'Hello'") {
    forAll { (s: String) =>
      assert(Hello.greet(s).startsWith("Hello"))
    }
  }

  property("Generated data: Hello's greeting composes") {
    forAll { (s: String) =>
      assertResult(s"Hello $s") { Hello.greet(s) }
    }
  }
}

/** Unit-tests Hello.greet: parametrized/property test with data from a table. */
class HelloPropSpecTableData
    extends AnyPropSpec
    with ScalaCheckPropertyChecks
    with Diagrams {

  val strings = Table("string",
    "", "a", "aa", "ሇ츨ᄕ", """AA"A"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA""")

  property("Table data: Hello's greeting starts with 'Hello'") {
    forAll(strings) { s =>
      assert(Hello.greet(s).startsWith("Hello"))
    }
  }

  property("Table data: Hello's greeting composes") {
    forAll(strings) { s =>
      assertResult(s"Hello $s") { Hello.greet(s) }
    }
  }
}
