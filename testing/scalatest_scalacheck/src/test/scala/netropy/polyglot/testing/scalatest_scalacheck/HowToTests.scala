package netropy.polyglot.testing.scalatest_scalacheck

// ScalaTest 3.1.0 reorganized names
import org.scalatest.propspec.AnyPropSpec
import org.scalatest.diagrams.Diagrams

// Runs checks against data from ScalaCheck generators.
// Methods:
//   forAll:   applies a check to data from implicit or specified generators
//   whenever: property need only hold whenever some condition is true
// API Doc hard to find:
//   https://www.scalatest.org/scaladoc/plus-scalacheck-1.17/3.2.14.0/org/scalatestplus/scalacheck/ScalaCheckDrivenPropertyChecks.html
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

// Extends ScalaCheckDrivenPropertyChecks for data supplied by tables.
// Methods:
//   exists:   assertion holds true for at least one element
//   forAll:   assertion holds true for every element (1st failure reported)
//   forEvery: same as forAll, but lists all failing elements
// API Doc hard to find:
//   https://www.scalatest.org/scaladoc/plus-scalacheck-1.17/3.2.14.0/org/scalatestplus/scalacheck/ScalaCheckPropertyChecks.html
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

/** Code example: property tests with generated data (<-ScalaCheck). */
class HowToPropSpecGenData
    extends AnyPropSpec
    with ScalaCheckDrivenPropertyChecks
    with Diagrams {

  property("Assertion: String concatenation is associative") {
    // cannot pass multiple arguments or tuple
    // forAll(strings, strings, strings) { (s: String, t: String, u: String)
    // forAll((strings, strings, strings)) { (s: String, t: String, u: String)
    forAll { (s: String) =>
      forAll { (t: String) =>
        forAll { (u: String) =>
          // cannot just evaluate to boolean, must state assertion
          // ((s + t) + u) == (s + (t + u))
          assert(((s + t) + u) == (s + (t + u)), "not associative")
        }
      }
    }
  }

  property("some pending test...") (pending)

  ignore("some ignored test") { fail("too bad") }
}

/** Code example: parametrized/property test with data from a table. */
class HowToPropSpecTableData
    extends AnyPropSpec
    with ScalaCheckPropertyChecks
    with Diagrams {

  val strings = Table("string",
    "", "a", "aa", "ሇ츨ᄕ", """AA"A"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA""")

  property("Assertion: String concatenation is associative") {
    // cannot pass multiple arguments or tuple
    // forAll(strings, strings, strings) { (s, t, u)
    // forAll((strings, strings, strings)) { (s, t, u)
    forAll(strings) { s =>
      forAll(strings) { t =>
        forAll(strings) { u =>
          // cannot just evaluate to boolean, must state assertion
          // ((s + t) + u) == (s + (t + u))
          assert(((s + t) + u) == (s + (t + u)), "not associative")
        }
      }
    }
  }

  property("some pending test...") (pending)

  ignore("some ignored test") { fail("too bad") }
}
