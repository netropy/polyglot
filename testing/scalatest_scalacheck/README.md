# polyglot/testing/scalatest_scalacheck

#### Topics: A small, cloneable, _standalone_ build project

- _Source + Unit Test Code_ checking out features and usage of
  [ScalaTest](https://www.scalatest.org) with
  [ScalaCheck](https://www.scalacheck.org) through
  [ScalaTestPlus ScalaCheck](https://github.com/scalatest/scalatestplus-scalacheck)
- _Project Code_ for each build tool -
  [sbt](https://www.scala-sbt.org),
  [Maven](https://maven.apache.org),
  [Gradle](https://gradle.org) -
  any of which can be removed
- _Notes_ on the compatibility of build tools, language, and library versions
- _Notes + Code Experiments_ on the used libraries

Quick links: \
[scala sources](src/main/scala/netropy/polyglot/testing/),
[scala tests](src/test/scala/netropy/polyglot/testing/scalatest_scalacheck/)

Also see: \
[../scalacheck](../scalacheck/README.md),
[../scalatest](../scalatest/README.md),
[../Summary: ScalaCheck](../Notes_unit_test_frameworks.md#summary-scalacheck),
[../Summary: ScalaTest](../Notes_unit_test_frameworks.md#summary-scalatest)

#### Summary: {sbt,maven,gradle} X scala{2,3} X {scalatest+scalatestplus_scalacheck}

[ScalaTest](https://index.scala-lang.org/scalatest/scalatest)
and the
[ScalaTestPlus ScalaCheck](https://index.scala-lang.org/scalatest/scalatestplus-scalacheck)
integration library, which pulls
[ScalaCheck](https://index.scala-lang.org/typelevel/scalacheck),
are available Scala as 2.12..13 and 3.x binaries.

| 2022-11 | 2.13 scalac, scala-library | 3.2 scalac (1), scala3-library |
|:--|:--:|:--:|
| scalatest\_2.13 3.2.14, scalacheck-1-17\_2.13 3.2.14.0 (2) | OK | Errors (5) |
| scalatest\_3 3.2.14, scalacheck-1-17\_3 3.2.14.0 (2) | Errors (3)(4) | OK |

(1) 2022-11, 2021-11: PROBLEM: Gradle really runs _scalac 2.x <- [zinc] <-
scala plugin_.  No solution yet how to configure running _scalac 3.x_.

(2) 2022-11, 2021-11: ___Problems with use of ScalaTest from Gradle 6.x:___
- Only the _2.13 binary_ of the ScalaTest library can be imported,
- and only up to ScalaTest version _3.2.9 from 2019_, see details in
 [../scalatest](../scalatest/README.md).
- This then freezes the (ScalaTestPlus) ScalaCheck version to _1.15 from 2021._

(3) 2022-11: scalac 2.13 can consume Scala3 libs via _-Ytasty-reader_, see
[TASTy](https://docs.scala-lang.org/scala3/guides/tasty-overview.html).

(4) 2022-11: Problems with cross-platform use 2.13 <- 3, see comments in
build.sbt.

(5) 2022-11: Problems with cross-platform use 2.13 -> 3, see comments in
build.sbt.

#### Notes: scalatestplus

Links: \
[ScalaTestPlus](https://www.scalatest.org/plus),
[@github](https://github.com/scalatest?q=scalatestplus&type=all),
[@scaladex](https://index.scala-lang.org/search?q=scalatestplus),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalatestplus),
[apidoc](https://javadoc.io/doc/org.scalatestplus)

ScalaTestPlus is a collection of libraries that extend ScalaTest with Mockito,
EasyMock, JMock, and other frameworks.

#### Notes: scalatestplus_scalacheck

Links: \
[ScalaTestPlus ScalaCheck](https://www.scalatest.org/plus/scalacheck),
[@github](https://github.com/scalatest/scalatestplus-scalacheck),
[@scaladex](https://index.scala-lang.org/scalatest/scalatestplus-scalacheck),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalatestplus/scalacheck-1-17)
[apidoc scalacheck-1-17](https://www.scalatest.org/scaladoc/plus-scalacheck-1.17/3.2.14.0/org/scalatestplus/scalacheck/index.html),
[user guide](https://www.scalatest.org/user_guide/property_based_testing),
[quick start](https://www.scalatest.org/plus/scalacheck)

Links: \
[ScalaTest](https://www.scalatest.org),
[@github](https://github.com/scalatest/scalatest),
[@scaladex](https://index.scala-lang.org/scalatest),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalatest),
[apidoc](https://www.scalatest.org/scaladoc/3.2.14/org/scalatest/index.html)
[quick start](https://www.scalatest.org/quick_start),
[user guide](https://www.scalatest.org/user_guide)

Links: \
[ScalaCheck](https://www.scalacheck.org),
[@github](https://github.com/typelevel/scalacheck),
[@scaladex](https://index.scala-lang.org/typelevel/scalacheck),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalacheck),
[apidoc](https://javadoc.io/doc/org.scalacheck/scalacheck_3/latest/index.html),
[user guide](https://github.com/typelevel/scalacheck/blob/main/doc/UserGuide.md)

_ScalaTestPlus ScalaCheck_ is the small library that pulls ScalaCheck and
provides integration support:
- ScalaTest provides the test runners and reporting,
- ScalaCheck brings the data generators for property checks;
- see below for the renamed _artifactId_ and _versioning_ scheme.

The "ScalaTestPlus ScalaCheck" API consists mainly of two _traits_ with a few
(but massively overloaded) methods:
```
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
```

_ScalaCheckDrivenPropertyChecks_ allows to run property checks against data
from ScalaCheck generators via methods:
- _forAll:_ applies a check to data from implicit or specified generators
- _whenever:_ property need only hold whenever some condition is true

_ScalaCheckPropertyChecks_ extends the above trait with overloads to also run
checks against data supplied by tables:
- _exists:_ assertion holds true for at least one element
- _forAll:_ assertion holds true for every element (1st failure reported)
- _forEvery:_ same as forAll, but lists all failing elements

#### 2022-11: OK; 2021-11, 2020-11: Problems

Since 2019, no newer version has been published of the original
`scalatestplus-scalacheck` integration library.

Eventually, I (and others) found out that the _artifactId_ had been changed
to`scalacheck-1-15`, `scalacheck-1-16` etc.

So, the ScalaCheck version (1.17) is now in the artifact's name.  The version
(3.2.14.0) should match the used version of ScalaTest (3.2.14):
```
<dependency>
    <groupId>org.scalatestplus</groupId>
    <artifactId>scalacheck-1-17_2.13</artifactId>
    <version>3.2.14.0</version>
    <scope>test</scope>
</dependency>
```

The new library _scalacheck-1-17_ solved a growing number of compatibility
problems, including availability of a Scala 3 binary.

[Up](../README.md)
