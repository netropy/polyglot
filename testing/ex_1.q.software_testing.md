# polyglot/testing

### 1 Q&A: Basics of Software Verification Testing

#### 1.1 Q&A: Describe the granulatity levels of software verification testing.

[Answer](ex_1_1.r.md)

#### 1.2 Q&A: Discuss some test-oriented software development techniques.

[Answer](ex_1_2.r.md)

#### 1.3 Q&A: List some Scala, Java, or C++ testing frameworks.

[Answer](ex_1_3.r.md)

[Up](./README.md)
