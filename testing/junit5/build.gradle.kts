/*
 * (This file was generated by the Gradle 'init' task.)
 */

group = "netropy"
version = "1.0.0-SNAPSHOT"
description = "Example, Scala, Scala3, Java, Junit5 Jupiter, sbt, gradle, maven"

// 2024-10: PROBLEM: JDK 21 fails with scala.reflect.internal.FatalError

// Tip: gradle skips tests on re-run, force to run all test cases:
// $ gradle cleanTest test

// Tip: find test reports under
// - build/test-results/test/TEST-*.xml
// - build/reports/tests/test/index.html
// - no .txt reports

// Tip: JUnit 4/5 runner, default: no console output for passing/skipped tests
// - configure: testLogging { events("passed", "skipped", "failed") }
// - or use plugin for better console logs: id("com.adarshr.test-logger")

plugins {
    // core plugin for Java (compile, test, bundle)
    // https://docs.gradle.org/current/userguide/java_plugin.html
    //java  // seems enabled by default

    // plugin for Java API and implementation separation
    // https://docs.gradle.org/current/userguide/java_library_plugin.html
    //`java-library`

    // plugin for mixed Scala and Java code (compile)
    // 2022-11: PROBLEM: still no support to run scalac 3.x
    // 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
    // https://docs.gradle.org/current/userguide/scala_plugin.html
    scala

    // better console log for tests
    // https://plugins.gradle.org/search?term=test+logger
    // https://plugins.gradle.org/plugin/com.adarshr.test-logger
    // https://github.com/radarsh/gradle-test-logger-plugin
    // 2024-11: PROBLEM: TypeNotPresentException, requires Gradle >=7.6
    //id("com.adarshr.test-logger") version "4.0.0"
    // 2022-11: PROBLEM: AbstractMethodError, requires Gradle >=6.5
    //id("com.adarshr.test-logger") version "3.2.0"
    id("com.adarshr.test-logger") version "2.1.1"
}

repositories {
    mavenCentral()
}

dependencies {
    // Scala-Lib version for code+tests
    // 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
    //val scala_compat_version = "2.12"
    //implementation("org.scala-lang:scala-library:2.12.20")
    val scala_compat_version = "2.13"
    implementation("org.scala-lang:scala-library:2.13.15")
    //val scala_compat_version = "3"
    //implementation("org.scala-lang:scala3-library_3:3.3.4")

    // JUnit Jupiter (Aggregator)
    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter
    // https://junit.org/junit5/
    testImplementation("org.junit.jupiter:junit-jupiter:5.11.3")
}

tasks.withType<ScalaCompile> {
    scalaCompileOptions.additionalParameters = mutableListOf(
        "-feature",
        "-deprecation",
        "-unchecked",
	// 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
        //"-Ytasty-reader",  // scala2.13
        //"-explaintypes",  // scala2
        //"-explain",  // scala3
        ""
    )
}

tasks.test {
    // use JUnit 5 runner (JUnit 4 runner doesn't find JUnit 5 tests)
    // default: no console output, reports under ./build/{reports,test-results}
    useJUnitPlatform()

    testLogging {
        // show test output, alternative to radarsh/gradle-test-logger-plugin:
        //   started, passed, skipped, failed, standard_out, standard_error
        //events("passed", "skipped", "failed")
        //showExceptions = true
        //showCauses = true
        //showStackTraces = true
    }
}
