name := "polyglot.testing.junit5"
organization := "netropy"
description := "Example, Scala, Scala3, Java, Junit5 Jupiter, sbt, gradle, maven"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: in case of interleaved console output for JUnit tests, set
//   Test / parallelExecution := false
//   Test / logBuffered := true
// https://www.scala-sbt.org/1.x/docs/Testing.html

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

// OK: Scala 3, 2.13, 2.12
//scalaVersion := "2.12.20"
//scalaVersion := "2.13.15"
scalaVersion := "3.3.4"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  //"-Ytasty-reader",  // scala2.13
  //"-explaintypes",  // scala2
  //"-explain",  // scala3
)

// Resolvers (pre- or user-defined)
// https://www.scala-sbt.org/1.x/docs/Resolvers.html
//resolvers += DefaultMavenRepository  // inluded by default
//resolvers += Resolver.mavenLocal  // ~/.m2

// Implementation of sbt's test interface for JUnit Jupiter
// https://mvnrepository.com/artifact/net.aichler/jupiter-interface
// https://github.com/maichler/sbt-jupiter-interface
// also requires ./project/plugins.sbt:
//   addSbtPlugin("net.aichler" % "sbt-jupiter-interface" % "0.11.1")
// also required: junit-jupiter classes (not part of sbt-jupiter-interface)
libraryDependencies += "net.aichler" % "jupiter-interface" % "0.11.1" % Test

// JUnit Jupiter (Aggregator)
// https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter
// https://junit.org/junit5/
libraryDependencies += "org.junit.jupiter" % "junit-jupiter" % "5.11.3" % Test

// JUnit5 framework options, better add this:
// -v decreases log level? "log events on log level info instead of debug"
// -s try decode Scala names in stack traces and test names
// -a make junit-interface show stacktraces for junit AssertionErrors
// sbt> testOnly -- -a *pattern*
// breaks build from parent dir, 'jupiterTestFramework' only defined here:
//Test / testOptions += Tests.Argument(jupiterTestFramework, "-a", "-s")

// only use a single thread for building
//parallelExecution := false
// execute tests in the _current_ project serially (default parallel)
//Test / parallelExecution := false

// fork a new JVM for 'run' and 'Test/run'
//fork := true
// fork a new JVM for 'Test/run', but not 'run'
//Test / fork := true
// add a JVM option to use when forking a JVM for 'run'
//javaOptions += "-Xmx8G"

//Test / testOptions := Seq(Tests.Filter(s => s.endsWith("Test")))

// print the output of tests immediately instead of buffering
//Test / logBuffered := true
