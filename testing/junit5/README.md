# polyglot/testing/junit5

#### Topics: A small, cloneable, _standalone_ build project

- _Source + Unit Test Code_ checking out features and usage of
  [JUnit 5](https://junit.org/junit5)
- _Project Code_ for each build tool -
  [sbt](https://www.scala-sbt.org),
  [Maven](https://maven.apache.org),
  [Gradle](https://gradle.org) -
  any of which can be removed
- _Notes_ on the compatibility of build tools, language, and library versions
- _Notes + Code Experiments_ on the used libraries

Quick links: \
[java sources](src/main/java/netropy/polyglot/testing/),
[java tests](src/test/java/netropy/polyglot/testing/junit5/),
[scala tests](src/test/scala/netropy/polyglot/testing/junit5/)

Also see: \
[../junit4](../junit4/README.md),
[../Summary: JUnit 5 (Java)](../Notes_unit_test_frameworks.md#summary-junit-5-java)

#### Summary: {sbt,maven,gradle} X scala{2,3} X {junit5}

[JUnit 5](https://mvnrepository.com/artifact/org.junit.jupiter)
is a pure Java library, hence binary-compatible with any Scala version.

| 2022-11 | 2.13 scalac, scala-library | 3.2 scalac (1), scala3-library |
|:--|:--:|:--:|
| junit-jupiter 5.9.1 (2) | OK | OK |

(1) 2022-11, 2021-11: PROBLEM: Gradle really runs _scalac 2.x <- [zinc] <-
scala plugin_.  No solution yet how to configure running _scalac 3.x_.

(2) 2022-11: for sbt: with jupiter-interface 0.11.1 library+plugin.

#### Notes: junit5

Links: \
[JUnit 5](https://junit.org/junit5),
[@github](https://github.com/junit-team/junit5),
[@mvnrepo](https://mvnrepository.com/artifact/org.junit.jupiter),
[apidoc](https://junit.org/junit5/docs/current/api),
[user guide](https://junit.org/junit5/docs/current/user-guide)

JUnit 5 "Jupiter" is the feature-rich successor version of JUnit 4:
- API and packaging changes, API improvements, source + tool migration needed
- core and 3rd party test runners, can run JUnit 4 tests
- supported by most build tools and IDEs

Modules (jar files):
- _JUnit Platform_: test execution, discovery, and reporting \
  (TestEngine API, Console Launcher, Platform Suite Engine can run multiple test engines).
- _JUnit Jupiter_: APIs for writing tests (programming model) and extensions
  (extension model).
- _JUnit Vintage_: backward-compatible TestEngine, which picks up JUnit 4
  tests \
  (just add junit-vintage-engine.jar to the test runtime path).

New _Annotations_ (org.junit.jupiter.api):
- configuring tests: \
  _@Test_, _@ParameterizedTest_, _@RepeatedTest_, \
  _@TestFactory_, _@TestTemplate_, _@TestInstance_, \
  _@Nested_, _@Tag_, _@Disabled_, _@Timeout_ etc
- fixtures, [f]initialization: \
  _@BeforeEach_, _@AfterEach_, _@BeforeAll_, _@AfterAll_
- support for meta-annotations and composed annotations

_Assertions_ (org.junit.jupiter.api.Assertions):
- extra methods taking Java 8 lambdas
- support for Kotlin assertions, 3rd-party assertion libraries, \
  e.g., for fluent _assertThat_-style assertions,
  [AssertJ](https://joel-costigliola.github.io/assertj/),
  [Hamcrest](http://hamcrest.org),
  [Truth](https://truth.dev)
- _assumptions_ (failure causes test abortion)

_Extension API_ for configuring test execution (test runners):
- _Extension, @ExtendWith, @RegisterExtension_
- test lifecycle callbacks, exception handlers
- use of multiple extensions (runners) at the same time

#### Notes: junit5 usage issues

- no style sheets for .xml output under _target/test-reports_
- interleaved console output or stacktraces for parallel tests
- await final summary output, try sbt: _logBuffered := true_ etc

#### Notes: source migration junit4 -> junit5

See [migration tips](https://junit.org/junit5/docs/current/user-guide/#migrating-from-junit4-tips)

| Junit 4 | Junit 5 |
|:---|:---|
| org.junit | org.junit.jupiter |
| Assert, msg is first argument | Assertions, msg is last argument |
| @Test(expected = XYZ.class) \* | assertThrows(XYZ.class, () -> ...) |
| @Ignore | @Disabled |
| @Before, @After | @BeforeEach, @AfterEach |
| @BeforeClass, @AfterClass | @BeforeAll, @AfterAll |
| @Category | @Tag |
| @RunWith | @ExtendWith |
| @Rule, @ClassRule | @ExtendWith, @RegisterExtension |

\* Junit 4.13 already introduced _assertThrows_

#### Notes: {sbt}+junit5

2022-11: fixed:
[sbt jupiter-interface](https://mvnrepository.com/artifact/net.aichler/jupiter-interface)
also available in default Maven repository.

Tip: Add JUnit5 framework options: _-s_ (try decode Scala names in stack
traces and test names) \
and _-a_ (make junit-interface show stacktraces).  sbt:
```
Test / testOptions += Tests.Argument(jupiterTestFramework, "-a", "-s")
```

Required: also add JUnit 5 classes and methods missing in
[sbt-jupiter-interface](https://github.com/maichler/sbt-jupiter-interface)
```
libraryDependencies += "net.aichler" % "jupiter-interface" % "0.9.1" % Test
libraryDependencies += "org.junit.jupiter" % "junit-jupiter" % "5.8.1" % Test
```

#### Notes: {gradle}+junit5

Must register JUnit 5 runner (not needed for sbt or maven):
```
  tasks.test { useJUnitPlatform() }
```

Tip: JUnit 4/5 runner, default: no console output for passing/skipped tests
- configure: `testLogging { events("passed", "skipped", "failed") }`
- or use plugin for better console logs: `id("com.adarshr.test-logger")`

2022-11, 2021-11: PROBLEM: Test runtime error with
[com.adarshr.test-logger 3.2.0](https://plugins.gradle.org/plugin/com.adarshr.test-logger/3.2.0)
```
//   Error: Failed to notify test listener.
//   Caused by: java.lang.AbstractMethodError: Receiver class
//   com.adarshr.gradle.testlogger.logger.SequentialTestLogger does not
//   define or inherit an implementation of the resolved method
//   'abstract java.lang.Object getProperty(java.lang.String)'
//   of interface groovy.lang.GroovyObject.
//id("com.adarshr.test-logger") version "3.2.0"
id("com.adarshr.test-logger") version "2.1.1"
```

[Up](../README.md)
