package netropy.polyglot.testing.junit5;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

/**
 * Code example: JUnit5 unit tests, also see HelloTests.
 *
 * Notes:
 *
 * - JUnit and build tool plugins tend to SILENTLY SKIP (not recognize) tests
 *   if any of the conditions below are not met.
 *
 * - Test Class: any top-level class, static member class, or @Nested class;
 *   must not be abstract and must have a single constructor;
 *   if constructor takes argument, must set up Parameter Resolution;
 *   have at least one test method.
 *
 * - Test Method: any method annotated with @Test, @RepeatedTest,
 *   @ParameterizedTest, @TestFactory, or @TestTemplate;
 *   if method takes argument, must set up Parameter Resolution;
 *   must not be abstract and must not return a value;
 *   must not be static.
 *
 * - Lifecycle Method: any method annotated with @BeforeAll, @AfterAll,
 *   @BeforeEach, or @AfterEach.
 *
 * - Not private: test classes, test methods, lifecycle methods; yet, some
 *   IDEs (also JUnitPlatform runner?) require test classes to be public.
 *
 * - @ParameterizedTest: values do increase the reported test's count
 *   argument sources: @NullSource, @EmptySource, @ValueSource, @EnumSource,
 *   @MethodSource, @CsvSource, @CsvFileSource, @ArgumentsSource;
 *   experimental feature in 5.6.2, requires dependency: junit-jupiter-params
 *
 * - @RepeatedTest(n) runs test method n times, increase test count by n.
 *
 * - Conditional Test Execution: Operating System, Java Runtime Environment,
 *   System Property, Environment Variable Conditions
 *
 * - Test Suites bundle test classes via @SuiteDisplayName, @SelectPackages.
 *
 * - @TestMethodOrder(...)
 *   MethodOrderer.{Alphanumeric, Random, OrderAnnotation per @Order(n)}
 *
 * - Tagging and Filtering: can select tagged tests per operators !, &, |, ( )
 */
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class HowToTestsJ {

    @Test
    public void test_expect_throwable_with_message() {

        Throwable t = assertThrows(
            java.lang.IllegalArgumentException.class,
            () -> { throw new IllegalArgumentException("usage error"); }
            );

        assertEquals(t.getMessage(), "usage error");
    }

    @Test
    public void test_console_output() {
        System.out.println("console output of a test");
    }

    // also needs @Test to be reported as ignored
    @Disabled("optional message")
    @Test
    public void test_ignored_failing_test() {
        fail("too bad", new IllegalArgumentException("can give a cause"));
    }
}
