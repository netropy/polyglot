package netropy.polyglot.testing.junit5;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import netropy.polyglot.testing.Hello;

/** Unit-tests the Hello.greet function. */
public class HelloTestsJ {

    public void test_propositions() {
        assertTrue(Hello.greet("You").startsWith("Hello"), "not greeted");
        assertTrue(Hello.greet("You").endsWith("You"), "not named");
    }

    @Test
    public void test_expect_values() {

        // assertEquals(expected, actual, message)
        assertEquals("Hello You", Hello.greet("You"), "not composed");
        assertNotEquals("xxx", Hello.greet("You"), "is \"xxx\" ");
        assertNull(null, "expected null");
        assertNotNull("xxx", "expected not-null");
        assertSame(this, this, "expected same");
        assertNotSame("", this, "expected not same");

        // JUnit recommends the use of 3rd party matchers with fluent API:
        // AssertJ: https://joel-costigliola.github.io/assertj/
        // Hamcrest: https://hamcrest.org/JavaHamcrest/
        // Truth: https://truth.dev/
    }

    @ParameterizedTest
    @NullSource  // null provider
    @EmptySource  // empty string provider, combination: @NullAndEmptySource
    // array of literals, 'strings' = method name, primitives + String + Class
    @ValueSource(strings = { "You", "Me" })
    public void test_parametrized_test(String input) {
        assertEquals("Hello " + input, Hello.greet(input));
    }
}
