package netropy.polyglot.testing.junit5

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.{Test,Disabled}
import org.junit.jupiter.api.{TestMethodOrder,MethodOrderer}

/** Code example: JUnit5 unit tests, also see HelloTests.
  *
  * Notes:
  *
  * - See: src/test/java/netropy/polyglot/java/testing/junit5/HelloTests.java
  *
  * - JUnit and build tool plugins tend to SILENTLY SKIP (not recognize) tests
  *   if any of the conditions below are not met.
  *
  * - Test class cannot be an object.
  */
@TestMethodOrder(classOf[MethodOrderer.Alphanumeric])
class HowToTests {

  @Test
  def test_expect_throwable_with_message: Unit = {

    val t: Throwable = assertThrows(
      classOf[java.lang.IllegalArgumentException],
      () => throw new IllegalArgumentException("usage error")
    )

    assertEquals(t.getMessage(), "usage error");
  }

  @Test
  def test_console_output: Unit = {
    println("console output of a test")
  }

  // also needs @Test to be reported as ignored
  @Disabled("optional message")
  @Test
  def test_ignored_failing_test: Unit = {
    fail("too bad", new IllegalArgumentException("can give a cause"));
  }
}
