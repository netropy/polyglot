package netropy.polyglot.testing.junit5

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.{NullSource,EmptySource,ValueSource}

import netropy.polyglot.testing.Hello

/** Unit-tests the Hello.greet function. */
class HelloTests {

  @Test
  def test_propositions: Unit = {
    assertTrue(Hello.greet("You").startsWith("Hello"), "not greeted")
    assertTrue(Hello.greet("You").endsWith("You"), "not named")
  }

  @Test
  def test_expect_values: Unit = {

    // assertEquals(expected, actual, message)
    assertEquals("Hello You", Hello.greet("You"), "not composed")
    assertNotEquals("xxx", Hello.greet("You"), """is "xxx" """)

    // no support for multi-line strings by junit4, use hamcrest matchers?
  }

  @ParameterizedTest
  @NullSource  // null provider
  @EmptySource  // empty string provider, combination: @NullAndEmptySource
  // array of literals, 'strings' = method name, primitives + String + Class
  @ValueSource(strings = Array("You", "Me"))
  def test_parametrized_test(input: String): Unit = {
    assertEquals(s"Hello $input", Hello.greet(input))
  }
}
