# polyglot/testing/munit

#### Topics: A small, cloneable, _standalone_ build project

- _Source + Unit Test Code_ checking out features and usage of
  [MUnit](https://scalameta.org/munit)
- _Project Code_ for each build tool -
  [sbt](https://www.scala-sbt.org),
  [Maven](https://maven.apache.org),
  [Gradle](https://gradle.org) -
  any of which can be removed
- _Notes_ on the compatibility of build tools, language, and library versions
- _Notes + Code Experiments_ on the used libraries

Quick links: \
[scala sources](src/main/scala/netropy/polyglot/testing/),
[scala tests](src/test/scala/netropy/polyglot/testing/munit/)

Also see: \
[../munit\_scalacheck](../munit_scalacheck/README.md),
[../Summary: MUnit (Scala)](../Notes_unit_test_frameworks.md#summary-munit-scala)

#### Summary: {sbt,maven,gradle} X scala{2,3} X {munit}

[MUnit](https://index.scala-lang.org/scalameta/munit)
is available as Scala 2.11..13 and 3.x binaries.

| 2022-11 | 2.13 scalac, scala-library | 3.2 scalac (1), scala3-library |
|:--|:--:|:--:|
| munit\_2.13 0.7.29 | OK | Errors (5) |
| munit\_3 0.7.29 | OK (2)(3)(4) | OK |

(1) 2022-11, 2021-11: PROBLEM: Gradle really runs _scalac 2.x <- [zinc] <-
scala plugin_.  No solution yet how to configure running _scalac 3.x_.

(2) 2022-11: scalac 2.13 requires additional import of
[scala-reflect 2.13](https://mvnrepository.com/artifact/org.scala-lang/scala-reflect).

(3) 2022-11: scalac 2.13 can consume Scala3 libs via _-Ytasty-reader_, see
[TASTy](https://docs.scala-lang.org/scala3/guides/tasty-overview.html).

(4) 2022-11: Ok. 2021-11: Problems with cross-platform use 2.13 <- 3.

(5) 2022-11, 2021-11: Problems with cross-platform use 2.13 -> 3, see comments
in build.sbt.

#### Notes: munit

See
[scala test](src/test/scala/netropy/polyglot/testing/munit/HowToParameterizeTestSuites.scala)

HowToDefineAbstractTestParams.scala
HowToDefineTestFixtures.scala
HowToTests.scala
MUnitApiLimitations.scala


Links: \
[MUnit](https://scalameta.org/munit),
[@github](https://github.com/scalameta/munit),
[@scaladex](https://index.scala-lang.org/scalameta/munit),
[@mvnrepo](https://mvnrepository.com/artifact/org.scalameta/munit),
[quick start](https://scalameta.org/munit/docs/getting-started.html),
no API docs (browse github sources)

Cross-platform unit testing library offering assertions, fixtures, and
extensible APIs.

Integrations: [ScalaCheck](https://www.scalacheck.org),
[Snapshot Testing](https://github.com/lolgab/munit-snapshot), and
[other/external](https://scalameta.org/munit/docs/integrations/external-integrations.html).

Assertions: see
[1](https://scalameta.org/munit/docs/assertions.html),
[2](https://github.com/scalameta/munit/blob/main/munit/shared/src/main/scala/munit/Assertions.scala)

Fixtures: see
[3](https://scalameta.org/munit/docs/fixtures.html)

FunSuite: see
[4](https://github.com/scalameta/munit/blob/main/munit/shared/src/main/scala/munit/FunSuite.scala)

Pretty-printed test reports with colors, diffs, and source locations.  The
prefix can be searched/grepped:
```
Test	  Prefix
Success	  +
Failed	  ==> X
Ignored	  ==> i
Skipped	  ==> s
```

#### Notes: {gradle}+munit

2022-11: no longer needed to register MUnit's JUnit 4 test runner
```
  tasks.test { useJUnit() }
```

#### Notes: MUnit API Limitations and Parameterized Tests

MUnit is somewhat particular _where_ tests can be defined:
- _not allowed:_ in an _object_ extending _FunSuite_ (InvalidTestClassError);
- _skipped/ignored:_ in a _trait_ self-typed as or extending _FunSuite_;
- _required:_ in a _top-level class_ extending _FunSuite_ and having a
  _no-argument_ constructor.

Practically, this means that test _classes_
- cannot truly be parameterized by taking constructor arguments;
- thus, cannot form type hierarchies passing up constructor arguments;
- therefore, form the _leaf_ nodes to a hierarchy/layer of test traits.

For tests _traits,_ this means that they
- cannot be parameterized by taking constructor arguments (Scala2);
- can take constructor arguments (Scala3) but cannot pass them on (flat layer);
- can take + pass implicit constructor arguments (Scala3) but that's a kludge;
- can be parameterized over an abstract resource injected by a bottom class;
- therefore, can form parameterized test suites as mixins for test classes.

Hence, MUnit (only) supports _parameterized tests_
- on the method level via an _(implicit: Location)_ argument;
- on the method level via ScalaCheck _forAll_ (property testing);
- on the method level via a _FunFixture_ ("loan" pattern);
- as traits over an abstract, test-class-injected resource ("cake" pattern);
- as stateful classes (or traits) via a _Fixture_.

Note that _FunFixture_ cannot be combined with _(implicit: Location)_ methods.

See \
[MUnitApiLimitations.scala](src/test/scala/netropy/polyglot/testing/munit/MUnitApiLimitations.scala),
[HowToDefineAbstractTestParams.scala](src/test/scala/netropy/polyglot/testing/munit/HowToDefineAbstractTestParams.scala),
[HowToDefineTestFixtures.scala](src/test/scala/netropy/polyglot/testing/munit/HowToDefineTestFixtures.scala)

[Up](../README.md)
