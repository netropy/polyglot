/*
 * (This file was generated by the Gradle 'init' task.)
 */

group = "netropy"
version = "1.0.0-SNAPSHOT"
description = "Example, Scala, Scala3, MUnit, sbt, gradle, maven"

// Tip: gradle skips tests on re-run, force to run all test cases:
// $ gradle cleanTest test

// Tip: find test reports under
// - build/test-results/test/TEST-*.xml
// - build/reports/tests/test/index.html
// - no .txt reports

plugins {
    // core plugin for Java (compile, test, bundle)
    // https://docs.gradle.org/current/userguide/java_plugin.html
    //java  // seems enabled by default

    // plugin for Java API and implementation separation
    // https://docs.gradle.org/current/userguide/java_library_plugin.html
    //`java-library`

    // plugin for mixed Scala and Java code (compile)
    // 2022-11: PROBLEM: still no support to run scalac 3.x
    // 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
    // https://docs.gradle.org/current/userguide/scala_plugin.html
    scala

    // better console log for tests
    // https://plugins.gradle.org/search?term=test+logger
    // https://plugins.gradle.org/plugin/com.adarshr.test-logger
    // https://github.com/radarsh/gradle-test-logger-plugin
    // 2024-11: PROBLEM: TypeNotPresentException, requires Gradle >=7.6
    //id("com.adarshr.test-logger") version "4.0.0"
    // 2022-11: PROBLEM: AbstractMethodError, requires Gradle >=6.5
    //id("com.adarshr.test-logger") version "3.2.0"
    id("com.adarshr.test-logger") version "2.1.1"
}

repositories {
    mavenCentral()
}

dependencies {
    // Scala-Lib version for code+tests
    // 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
    //val scala_compat_version = "2.12"
    //implementation("org.scala-lang:scala-library:2.12.20")
    val scala_compat_version = "2.13"
    implementation("org.scala-lang:scala-library:2.13.15")
    //val scala_compat_version = "3"
    //implementation("org.scala-lang:scala3-library_3:3.3.4")

    // MUnit
    // offered as Scala 3 and 2.11..13 binaries
    // https://mvnrepository.com/artifact/org.scalameta/munit
    // https://scalameta.org/munit/
    // https://index.scala-lang.org/scalameta/munit/munit
    //
    // FUTURE: switch automatically once gradle supports scalac_3
    //testImplementation("org.scalameta:munit_${scala_compat_version}:0.7.29")
    //
    // 2024-10, 2022-11, 2021-11: OK can always consume munit_2.13/2.12
    //   scala-library_2.12 + munit_2.12
    //testImplementation("org.scalameta:munit_2.12:1.0.2")
    //   scala-library_2.13 + munit_2.13
    //   scala3-library_3.3 + munit_2.13
    testImplementation("org.scalameta:munit_2.13:1.0.2")
    // 2022-11, 2021-11: PROBLEMS to consume munit_3
    //   scala-library_2.13 + scala-reflect_2.13 + munit_3 + -Ytasty-reader
    //   scala3-library_3.2 + scala-reflect_2.13 + munit_3 + -Ytasty-reader
    //   https://mvnrepository.com/artifact/org.scala-lang/scala-reflect
    //testImplementation("org.scala-lang:scala-reflect:2.13.10")
    //testImplementation("org.scalameta:munit_3:0.7.29")
}

tasks.withType<ScalaCompile> {
    scalaCompileOptions.additionalParameters = mutableListOf(
        "-feature",
        "-deprecation",
        "-unchecked",
	// 2021-11: PROBLEM: really runs scalac 2.x <- [zinc] <- scala plugin
        //"-Ytasty-reader",  // scala2.13
        //"-explaintypes",  // scala2
        //"-explain",  // scala3
        ""
    )
}

tasks.test {
    // 2022-11: no longer needed to register the JUnit 4 runner
    // munit provides a JUnit 4 runner:
    //useJUnit()

    // no need or no use to configure JUnit 4 runner options:
    //testLogging {
        //   started, passed, skipped, failed, standard_out, standard_error
        //events("passed", "skipped", "failed")
        //showExceptions = true
        //showCauses = true
        //showStackTraces = true
    //}
}
