name := "polyglot.testing.munit"
organization := "netropy"
description := "Example, Scala, Scala3, MUnit, sbt, gradle, maven"
version := "1.0.0-SNAPSHOT"

// Tip: find test reports under
// - target/test-reports/TEST-*.xml
// - no *.txt, *.html reports

// Tip: run 'sbt test:console' to import 'test' scope libs into REPL

// Tip: group %% artifact  ~~>  .../group/artifact_<scala compat version>/...

// OK: Scala 3, 2.13, 2.12
//scalaVersion := "2.12.20"
//scalaVersion := "2.13.15"
scalaVersion := "3.3.4"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  //"-Ytasty-reader",  // scala2.13
  //"-explaintypes",  // scala2
  //"-explain",  // scala3
)

// Resolvers (pre- or user-defined)
// https://www.scala-sbt.org/1.x/docs/Resolvers.html
//resolvers += DefaultMavenRepository  // inluded by default
//resolvers += Resolver.mavenLocal  // ~/.m2

// https://mvnrepository.com/artifact/org.scalameta/munit
// https://scalameta.org/munit/
// https://index.scala-lang.org/scalameta/munit/munit
//
// scalac 3/2.13/2.12 + munit_3/2.13/2.12
libraryDependencies += "org.scalameta" %% "munit" % "1.0.2" % Test
//
// scalac 3.2 + munit_2.13
// 2022-11: ERROR: no implicit argument of type munit.Location was found...
//libraryDependencies += "org.scalameta" % "munit_2.13" % "0.7.29" % Test
//
// scalac 2.13 + munit_3 + scala-reflect_2.13 + -Ytasty-reader
// 2022-11: OK, 2021-11: ERRORS
//libraryDependencies += "org.scalameta" % "munit_3" % "0.7.29" % Test
//libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.13.10" % Test

// 2022-11: no longer needed: make sbt recognize munit tests
//testFrameworks += new TestFramework("munit.Framework")

// only use a single thread for building
//parallelExecution := false
// execute tests in the _current_ project serially (default parallel)
//Test / parallelExecution := false

// fork a new JVM for 'run' and 'Test/run'
//fork := true
// fork a new JVM for 'Test/run', but not 'run'
//Test / fork := true
// add a JVM option to use when forking a JVM for 'run'
//javaOptions += "-Xmx8G"

//Test / testOptions := Seq(Tests.Filter(s => s.endsWith("Test")))

// print the output of tests immediately instead of buffering
//Test / logBuffered := false
