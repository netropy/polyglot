package netropy.polyglot.testing.munit

import munit.FunSuite

/*
 * Summary: MUnit Limitations, Parameterized Tests, Scala3 Traits
 *
 * MUnit is somewhat particular where tests can be defined:
 * - not allowed: in an object extending FunSuite (InvalidTestClassError)
 * - skipped/ignored: in a trait self-typed as or extending FunSuite
 * - required: in a top-level class extending FunSuite and having a
 *   no-argument constructor.
 *
 * => Support for parameterized test methods, see HelloFunSuite:
 *      def testOn(r: Resource)(implicit loc: Location) {
 *        test("...") { ... r ... }
 *      }
 *      testOn(...)
 *      testOn(...)
 *
 * How can we define parameterized test classes/suites?
 *
 * => No solution: Test _classes_ cannot truly take c'tor arguments:
 * - FunSuite is a class, so it cannot be mixed in freely.
 * - Classes extending FunSuite must have a public no-argument constructor.
 * - MUnit always invokes the no-arg c'tor, throwing IAE if unavailable.
 * - MUnit ignores other c'tors (including default or implicit arguments).
 * - Therefore, test classes_ cannot form hierarchies passing c'tor arguments.
 *
 * => No solution: Test classes cannot be nested (use a resource from outer).
 *
 * => No solution: Scala3 traits with parameters cannot form hierachies:
 * - Traits can have parameters but cannot pass arguments to super c'tor.
 * - Traits can pass implicit arguments to super c'tor, but this is a kludge.
 *
 * => Solution: "Cake" pattern.  See ./HowToDefineAbstractTestParams.scala
 *
 * => Alternative: Use MUnit fixtures.  See ./HowToDefineTestFixtures.scala
 *
 * @see [[https://scalameta.org/munit]]
 * @see [[https://github.com/scalameta/munit]]
 */

// ----------------------------------------------------------------------
// MUnit API Limitations
// ----------------------------------------------------------------------

/*
 * A test suite cannot be a trait, unless mixed into a class.
 */
trait TestTraitsAreIgnored extends FunSuite {

  //throw Exception()  // ignored, Scala3

  test("ignored test") { fail("too bad") }
}

/*
 * A test suite cannot be an object, must be class to be run.
 *
 * object TestCannotBeAnObject extends FunSuite
 * ~> org.junit.runners.model.InvalidTestClassError: Invalid test class ...:
 *    1. Test class should have exactly one public constructor
 *    2. No runnable methods
 *
 * But this exception text seems incorrect: This test class _has_
 * - more that one public constructor
 * - runnable methods
 * without problems.
 */
class TestClassCanHaveMultipleConstructors  // munit only calls default c'tor
  extends FunSuite with Runnable {

  def this(i: Int) = { this(); throw new Exception() }  // ignored

  def run(): Unit = { throw new Exception() }  // ignored

  test("executed test") { /* fail("too bad") */ }  // not ignored
}

class TestClassesCannotBeNested extends FunSuite {

  class Ignored_or_CtorError_FailingNestedSuite extends FunSuite {

    test("failing test#5") { fail("too bad") }
  }
}

class TestClassesSkipParameters(val resource: String)  // throws IAE...
    extends FunSuite {

  def this() = this("resource")  // ...unless have default c'tor used by munit

  test("no op") {}
}

class TestClassesSkipDefaultParameters(val resource: String = "")  // IAE...
    extends FunSuite {

  def this() = this("resource")  // ...unless have default c'tor used by munit

  test("no op") {}
}

/*  // no need to require Scala3 just for this code experiment

 class TestClassesSkipImplicitParameters(using val resource: String)  // IAE...
    extends FunSuite {

  def this() = this(using "resource")  // ...unless have a default c'tor

  test("no op") {}
}

 */

// ----------------------------------------------------------------------
// Scala3 Traits
// ----------------------------------------------------------------------

/*  // no need to require Scala3 just for this code experiment

// Scala 3 Traits may have parameters with certain limitations/relaxations:
// - Traits must never pass arguments to parent traits.
// - Traits with context parameters have inferred arguments passed to parent.
//
// https://docs.scala-lang.org/scala3/reference/other-new-features/trait-parameters.html

// So, can build a flat layer (no hierarchy) of test traits to be mixed in:

trait Scala3TraitsCanHaveParameters(resource: String) {
  this: FunSuite =>

  test("resource not null") { assert(resource != null) }
}

trait Scala3TraitsCannotPassArguments(resource: String)
  //extends Scala3TraitsCanHaveParameters(resource) {  // cannot pass args
  extends FunSuite {

  test("resource not null") { assert(resource != null) }
}

class TestClassesCanPassArgumentsToScala3Traits(resource: String)  // IAE...
  extends FunSuite  // must extend since a class, not a trait
  with Scala3TraitsCanHaveParameters(resource)
  with Scala3TraitsCannotPassArguments(resource) {  // can pass args

  def this() = this("resource")  // ...unless have default c'tor used by munit

  test("no op") {}
}

// But this kludge works: implicit c'tor arguments are passed implicitely:

trait Scala3TraitsCanHaveImplicitParameters(using val resource: String)
    extends FunSuite {

  test("resource not null") { assert(resource != null) }
}

trait Scala3TraitsMaySkipPassingImplicitArguments
    extends Scala3TraitsCanHaveImplicitParameters {  // compiles since implicit

  test("resource not null") { assert(resource != null) }
}

class Scala3ClassesCanPassImplicitArguments(using override val resource: String)
      extends FunSuite  // must extend since a class, not a trait
    with Scala3TraitsCanHaveImplicitParameters {  // compiles since implicit

  def this() = this(using "resource")  // ...but again, need a default c'tor

  test("no op") {}
}

 */
