package netropy.polyglot.testing.munit

import munit.{FunSuite, Compare}

/** Convenience methods for munit FunSuite tests as mixin.
  *
  * More curried and expectation-first equals-assertions to support usage:
  * ```
  *   assertEquals(actual, expected)  // = munit.Assertions
  *   assertEquals(actual) {
  *     ...expected...
  *   }
  *   assertResult(expected, actual)  // as in ScalaTest
  *   assertResult(expected) {
  *     ...actual...
  *   }
  * ```
  */
trait FunSuiteExt {
  this: FunSuite =>

  /** A _curried_ equals assertion taking the expected value _second_. */
  def assertEquals[A, B](act: A)(exp: B)(implicit c: Compare[A, B]): Unit =
    assertEquals(act, exp)

  /** An _uncurried_ equals assertion taking the expected value _first_. */
  def assertResult[A, B](exp: A, act: B)(implicit c: Compare[B, A]): Unit =
    assertEquals(act, exp)

  /** A _curried_ equals assertion taking the expected value _first_. */
  def assertResult[A, B](exp: => A)(act: B)(implicit c: Compare[B, A]): Unit =
    assertEquals(act, exp)

  /*
   * Design Notes:
   *
   * Here, the convenience functions are provided as mixin trait.
   * There's no benefit to define them as (Scala2/3) extension methods as
   * they would have to be qualified with 'this':
   *   object FunSuiteUtils {
   *     implicit class FunSuiteExt(fs: FunSuite) { ... }
   *   }
   *   import FunSuiteUtils.FunSuiteExt
   *   this.assertResult(...)
   *
   * Prevent compile error: double definition after erasure
   * Name clash of curried/uncurried versions after compile:
   *   def assertResult[A, B](exp: A, act: B)(implicit c: Compare[B, A]): Unit
   *   def assertResult[A, B](exp: A)(act: B)(implicit c: Compare[B, A]): Unit
   * Scala2 fix: disambiguate signatures in a call-invariant way (by-name etc)
   * Scala3 fix: @annotation.targetName("assertResult1")
   *
   * Also see discussion in netropy.polyglot.testing.pojo_assert.Assertions.
   */
}
