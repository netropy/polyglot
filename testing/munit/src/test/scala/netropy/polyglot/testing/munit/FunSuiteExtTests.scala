package netropy.polyglot.testing.munit

import munit.FunSuite

/** Unit-tests the convenience methods for munit.FunSuite. */
class FunSuiteExtTests
    extends FunSuite
    with FunSuiteExt {

  test("assertEquals(act, exp)") {
    assertEquals(1 + 1, 2)
    //assertEquals(1 + 1, 2L)  // no implicit Compare[Int, Long]
    assertEquals(1 + 1, 2L: AnyVal)
    assertEquals(1 + 1: AnyVal, 2L)
    //assertEquals(2, Integer.valueOf(2))  // no implicit Compare[Int, Integer]
    assertEquals(2, Integer.valueOf(2): Any)
    assertEquals(2: Any, Integer.valueOf(2))
    assertEquals(List(2), Seq(2))
    assertEquals(Seq(2), List(2))
    assertEquals(Seq(2), List(2))
  }

  test("assertEquals(act)(exp)") {
    assertEquals(1 + 1)(2)
    //assertEquals(1 + 1)(2L)  // no implicit Compare[Int, Long]
    assertEquals(1 + 1)(2L: AnyVal)
    assertEquals(1 + 1: AnyVal)(2L)
    //assertEquals(2)(Integer.valueOf(2))  // no implicit Compare[Int, Integer]
    assertEquals(2)(Integer.valueOf(2): Any)
    assertEquals(2: Any)(Integer.valueOf(2))
    assertEquals(List(2))(Seq(2))
    assertEquals(Seq(2))(List(2))
    assertEquals(Seq(2))(List(2))
  }

  test("assertResult(exp, act)") {
    assertResult(2, 1 + 1)
    //assertResult(2L, 1 + 1)  // no implicit Compare[Int, Long]
    assertResult(2L: AnyVal, 1 + 1)
    assertResult(2L, 1 + 1: AnyVal)
    //assertResult(Integer.valueOf(2), 2)  // no implicit Compare[Int, Integer]
    assertResult(Integer.valueOf(2): Any, 2)
    assertResult(Integer.valueOf(2), 2: Any)
    assertResult(Seq(2), List(2))
    assertResult(List(2), Seq(2))
    assertResult(List(2), Seq(2))
  }

  test("assertResult(exp)(act)") {
    assertResult(2)(1 + 1)
    assertResult(2)(1 + 1)
    //assertResult(2L)(1 + 1)  // no implicit Compare[Int, Long]
    assertResult(2L: AnyVal)(1 + 1)
    assertResult(2L)(1 + 1: AnyVal)
    //assertResult(Integer.valueOf(2))(2)  // no implicit Compare[Int, Integer]
    assertResult(Integer.valueOf(2): Any)(2)
    assertResult(Integer.valueOf(2))(2: Any)
    assertResult(Seq(2))(List(2))
    assertResult(List(2))(Seq(2))
    assertResult(List(2))(Seq(2))
 }
}
