package netropy.polyglot.testing.munit

import munit.FunSuite

/*
 * MUnit precludes parameterized test classes but allows dependency injection.
 *
 * => Solution: Use test traits with abstract resources ("cake" pattern).
 * - Test traits self-type or extend FunSuite, thus have test(){}, assert etc.
 * - Test traits declare resources as abstract value member (or lambda).
 * - Test traits can form inheritance hierarchies but there's no point.
 * - Test traits can use multiple abstract resources, mixed-in or declared.
 * - Bottom test classes then provide concrete resources.
 *
 * => Alternative: Use MUnit fixtures.  See ./HowToDefineTestFixtures.scala
 *
 * @see [[https://scalameta.org/munit]]
 * @see [[https://github.com/scalameta/munit]]
 */

// ----------------------------------------------------------------------
// Dependency Injection via Abstract Resource (Cake Pattern)
// ----------------------------------------------------------------------

trait HasResource {
  val resource: String  // abstract resource as value (or lambda)
  //assert(resource != null)  // unavailable at instantiation
}

trait TestTraitsCanExtendFunSuite
  extends FunSuite
  with HasResource {  // available in subtypes

  test("(1) has resource") { assert(resource != null) }
}

trait TestTraitsCanSelfTypeFunSuite
  extends HasResource {  // available in subtypes
  self: FunSuite =>  // hidden from subtypes

  test("(2) has resource") { assert(resource != null) }
}

trait TestTraitsCanSelfTypeFunSuiteWithX {
  self: FunSuite with HasResource =>  // hidden from subtypes

  test("(3) has resource") { assert(resource != null) }
}

trait TestTraitsCanFormHierarchyButWhatsThePoint
  extends TestTraitsCanExtendFunSuite
  with TestTraitsCanSelfTypeFunSuite {

  test("(4) has resource") { assert(resource != null) }
}

trait TestTraitsCanUseMultipleResouces {
  self: FunSuite with HasResource =>  // hidden from subtypes

  val other: String  // abstract resource
  //assert(other != null)  // unavailable at instantiation

  test("(5) has resources") { assert(resource != null && other != null) }
}

class TestClassProvidesResources
  extends FunSuite  // must come first since class
  with TestTraitsCanExtendFunSuite
  with TestTraitsCanSelfTypeFunSuite  // multiply inherited tests run once...
  with TestTraitsCanSelfTypeFunSuiteWithX  // ...due to linearization
  with TestTraitsCanFormHierarchyButWhatsThePoint
  with TestTraitsCanUseMultipleResouces {

  val resource = "resource"  // concrete resources, setUp only, no tearDown
  val other = "other"

  test("(6) has resources") { assert(resource != null && other != null) }
}
