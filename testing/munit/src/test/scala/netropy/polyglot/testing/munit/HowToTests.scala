package netropy.polyglot.testing.munit

// munit uses implicit conversion for extensions: .only, .ignore, .flaky
// they won't fix for dotty: //https://github.com/scalameta/munit/issues/96
import scala.language.implicitConversions

import munit.FunSuite

/*
 * Code example: MUnit tests, also see HelloTests.
 *
 * FunSuite provides assertions, fixtures, munitTimeout(), helpers etc.
 *
 * @see [[https://scalameta.org/munit/docs/tests.html]]
 */

class HowToInterceptTests extends FunSuite {

  test("expect throwables") {

    intercept[java.lang.IllegalArgumentException] {
      throw new IllegalArgumentException()
    }

    interceptMessage[java.lang.IllegalArgumentException]("usage error") {
      throw new IllegalArgumentException("usage error")
    }
  }
}

// some test reporters (gradle) seem to rely on (globally?) unique test names

class HowToRunOnlyThisTest extends FunSuite {

  test("run only this test of suite".only) {}

  test("failing test#1") { fail("too bad") }
}

class HowToIgnoreSomeTests extends FunSuite {

  test("failing test#2".ignore) { fail("too bad") }  // reported 2x by gradle

  test("conditional test") {
    assume(false, "disable the rest of this test per dynamic condition")
    fail("too bad")
  }

  // no support for reporting tests as 'pending' (ScalaTest)
  //test("some pending test...") (pending)
}

@munit.IgnoreSuite
class HowToIgnoreAnEntireSuite extends FunSuite {

  test("failing test#3") { fail("too bad") }
}

class HowToRunConditionalSuite extends FunSuite {

  override def munitIgnore = true  // dynamic condition

  test("failing test#4") { fail("too bad") }
}

class HowToDealWithFlakyTests extends FunSuite {

  override def munitFlakyOK = true  // or set env MUNIT_FLAKY_OK=true

  test("flaky failing".flaky) {
    println(s"this flaky test treated as skipped: munitFlakyOK=$munitFlakyOK")
    fail("too bad")
  }
}
