package netropy.polyglot.testing.munit

import netropy.polyglot.testing.Hello

import munit.{FunSuite, Location}


/** Unit-tests the Hello.greet function. */
class HelloFunSuite extends FunSuite {

  test("console output") {
    println("console output of a test")
  }

  test("test propositions") {

    assert(Hello.greet("You").startsWith("Hello"), "not greeted")
    assert(Hello.greet("You").endsWith("You"), "not named")
  }

  test("expect values") {

    // compare values as: (obtained, expected)
    //
    // `expected` must of subtype of `obtained`, but can always upcast:
    // -compiles: assertEquals(Some(1), Option(1))
    // +compiles: assertEquals[Any, Any](Some(1), Option(1))

    assertEquals(Hello.greet("You"), "Hello You", "not composed")

    assertNotEquals(Hello.greet("You"), "xxx", """is "xxx" """)

    assertNoDiff(Hello.greet("\n You"), "Hello \n You",
      "only assertNoDiff compares multi-line strings")

    assertNoDiff(Hello.greet("You \n "), " \n Hello You",
      "assertNoDiff also ignores leading/trailing whitespace")
  }

  def testOn(input: String)(implicit loc: Location): Unit = {

    test(s"parametrize, test on input: '$input'") {
      assertEquals(Hello.greet(input), s"Hello $input")
    }
  }

  testOn("You")
  testOn("Me")
}
