package netropy.polyglot.testing.munit

import munit.{FunSuite, Fixture, BeforeEach, AfterEach, Location}

/*
 * MUnit precludes parameterized test classes but allows dependency injection.
 *
 * => See "MUnit Limitations" in ./HowNotToParameterizeTestClasses.scala
 *
 * => See "Cake" pattern solution in ./HowToDefineAbstractTestParams.scala
 *
 * => Alternative: Use MUnit Fixtures.  See below.
 *
 * - ad-hoc test-/suite-local fixtures:
 *      var resource = null
 *      override def beforeEach/All() = { ... resource = ... }
 *      override def afterEach/All() = { ... resource ... }
 *      test("...") { ... resource ... }
 *
 *      => see: Stateful Dependency Management via Ad Hoc Before/After Methods
 *
 * - reusable test-/suite-local fixture objects:
 *      val resource = Fixture[R] {
 *        var r = null
 *        def apply() = r
 *        override def beforeEach/All() = { ... r = ...}
 *        override def afterEach/All() = { ... r ... }
 *      }
 *      override def munitFixtures = List(resource)
 *      test("...") { ... resource() ... }
 *
 *      => Stateful Dependency Management via Encapsulated, Re-usable Fixtures
 *
 * - functional test-local fixtures ("loan" pattern):
 *      val resource = Fixture[R](
 *        setup = t => { ... r },
 *        teardown = t => { ... r }
 *      )
 *      resource.test("...") { r => ... }
 *
 *      => see: Functional, Test-Local Dependency Management via Lambda
 *
 * Note: functional fixtures cannot be combined with parameterized test methods.
 *
 * @see [[https://scalameta.org/munit/docs/fixtures.html]]
 * @see [[https://scalameta.org/munit]]
 * @see [[https://github.com/scalameta/munit]]
 */

// ----------------------------------------------------------------------
// Stateful Dependency Management via Ad Hoc Before/After Methods
// ----------------------------------------------------------------------

trait StatefulTestTraitsCanHaveAbstractResources {
  self: FunSuite =>  // hidden from subtypes

  var suiteRes: String  // abstract resources
  var testRes: String

  //assert(suiteRes != null)  // unavailable at instantiation
  //assert(testresource != null)

  test("(1) has resources") { assert(suiteRes != null && testRes != null) }
}

class StatefulTestClassIsFixtureManagingResources
  extends FunSuite
  with StatefulTestTraitsCanHaveAbstractResources {

  var suiteRes: String = null  // concrete resources
  var testRes: String = null

  //assert(suiteRes != null)  // unavailable at instantiation
  //assert(testresource != null)

  override def beforeAll(): Unit = { suiteRes = "suiteRes" }
  override def beforeEach(ctx: BeforeEach): Unit = { testRes = ctx.test.name }
  override def afterEach(ctx: AfterEach): Unit = { testRes = null }
  override def afterAll(): Unit = { suiteRes = null }

  test("(2) has resources") { assert(suiteRes != null && testRes != null) }
}

// ----------------------------------------------------------------------
// Stateful Dependency Management via Encapsulated, Re-usable Fixtures
// ----------------------------------------------------------------------

trait StatelessTestTraitsCanHaveFixtures {
  self: FunSuite =>  // hidden from subtypes

  val suiteRes: Fixture[String]  // abstract or concrete resources
  val testRes: Fixture[String]

  //assert(suiteresource != null)  // unavailable at instantiation
  //assert(testresource != null)

  test("(1) has resources") { assert(suiteRes() != null && testRes() != null) }
}

class StatelessTestClassProvidesFixtures
  extends FunSuite
  with StatelessTestTraitsCanHaveFixtures {

  // a re-usable, suite-local fixture
  val suiteRes = new Fixture[String]("Suite Resource") {
    private var res: String = null

    def apply() = res
    override def beforeAll(): Unit = { res = "suiteRes" }
    override def afterAll(): Unit = { res = null }
  }

  // a re-usable, test-local fixture
  val testRes = new Fixture[String]("Test Resource") {
    private var res: String = null

    def apply() = res
    override def beforeEach(ctx: BeforeEach): Unit = { res = ctx.test.name }
    override def afterEach(ctx: AfterEach): Unit = { res = null }
  }

  // register fixtures for this suite
  override def munitFixtures = List(suiteRes, testRes)

  test("(2) has resources") { assert(suiteRes() != null && testRes() != null) }
}

// ----------------------------------------------------------------------
// Functional, Test-Local Dependency Management via Lambda (Loan Pattern)
// ----------------------------------------------------------------------

// FunFixture does not support defining a top-level type outside a FunSuite:
//   class TestResource
//     extends FunFixtures#FunFixture[String](setup = ..., teardown = ...){}
// Class FunFixture is defined inside trait FunFixtures with private c'tor.
//
// But FunFixture is meant for test-local, not suite-local, resources anyway.

trait StatelessTestTraitWithConcreteFunFixture {
  this: FunSuite =>

  // FunFixtures cannot define a class: deprecated FunFixture constructor
  // class TestRes extends FunFixture[String](
  //   setup = test => test.name,
  //   teardown = name => ()
  // )

  // FunFixtures cannot be abstract: NPE when used to define tests in body
  // val abstractTestRes: FunFixture[String]
  //
  // NPE unless doing tricks like deprecated DelayedInit
  // abstractTestRes.test("(1) has resource") { r => assert(r != null) }

  // a concrete, test-local, functional fixture (re-usable as value)
  val testRes = FunFixture[String](
    setup = test => test.name,
    teardown = name => ()
  )

  testRes.test("(1) has resource") { r => assert(r != null) }
}

class StatelessTestClassUsingFunFixture
  extends FunSuite
  with StatelessTestTraitWithConcreteFunFixture {

  testRes.test("(2) has resource") { r => assert(r != null) }
}

class FunFixtureDoesNotCallParameterizedTestMethods
  extends FunSuite {

  // a concrete, test-local, functional fixture (re-usable as value)
  val testRes = FunFixture[String](
    setup = test => test.name,
    teardown = name => ()
  )

  def testFail(r: String)(implicit loc: Location): Unit =
    test("failing parameterized test") { fail("too bad") }

  testRes.test("tests should have resource") { r =>
    testFail(r)  // ignored when called from FunFixture
    //fail("too bad")  // not ignored
  }

  //testFail("call parameterized test")  // not ignored from here
}
