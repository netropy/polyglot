package netropy.polyglot.testing

object Hello {

  def greet(s: String) = s"Hello $s"

  def main(args: Array[String]): Unit =
    println(greet(args.mkString(", ")))
}

object HelloApp extends App {

  // Scala2 runs this code as (deprecated) Delayedinit
  //   https://www.scala-lang.org/api/2.12.7/scala/DelayedInit.html
  //
  // Scala3 runs this code in the object's initializer
  //   https://dotty.epfl.ch/docs/reference/dropped/delayed-init.html
  // may observe fields as null when accessed asynchronously
  //
  println("Hello!")
}
