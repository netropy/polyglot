# polyglot/testing

### Notes: Compatibility {build tools} X scala{2.x,3} X {test libraries}

##### Problem: Pick a build tool, Scala version, and test library -- see if that combination works.

For example: a
[Gradle](https://gradle.org)
build project +
[Scala 3](https://docs.scala-lang.org/scala3/new-in-scala3.html)
code + unit/property tests by
[ScalaTest](https://www.scalatest.org) and
[ScalaCheck](https://www.scalacheck.org). \
... As of 2022, scala3 compilation from Gradle (6.x ~> zinc) is still a
nightmare, see
[./scalatest](scalatest/README.md).

Another example: an
[sbt](https://www.scala-sbt.org)
build project + mixed
[Scala 2.x/3](https://docs.scala-lang.org/tour/tour-of-scala.html)
codebase + tests by
[MUnit](https://scalameta.org/munit/) +
[ScalaMock](https://scalamock.org). \
... As of 2022, ScalaMock lacks a Scala3 port (no cross-platform use), see
[./scalatest_scalamock](scalatest_scalamock/README.md). \
... Also, ScalaMock has no integration with MUnit (only ScalaTest, specs2),
see
[user guide](https://scalamock.org/user-guide/integration).

What adds to compatibility/version dependencies:
- need for various build tool _plugins_ for java/scala compile, test runners,
  report generators etc;
- a mixed Scala 2.12/2.13/3, Java 8/11/17/21 code base, and 3rd party library
  dependencies.
- use of extra test libraries for assertions, property checking, mock
  generators etc.

Finally, the tool & library landscape is constantly changing.  In 2022, things
(Scala3 libs) improved.

Generally, it is not easy to predict which plugins/libraries will stop
receiving (timely) updates.

##### What to do?

Maintain a small set of small example projects with tools/plugins/langs/libs
combinations.

From time to time, bumb version numbers, try fix problems, update code and notes.

Over time, see which plugins/libraries become inactive and find replacement.

----------------------------------------------------------------------

#### 2022-11: {build tools} X scala{2.x,3} X {test frameworks}

##### Summary: Scala 2.13 + 3 compatibility much improved since 2021-11
- __[+]__ libraries available as 2.13 + 3: MUnit, ScalaTest, ScalaCheck
- __[\*]__ only some cross-platform use of these libraries, is bonus anyway
- __[–]__ ScalaMock still only available as 2.13, no cross-platform use
- __[–]__ Gradle 6.x still only runs _scalac 2.x <- [zinc] <- scala plugin_

Symbols: + = ok, (-) = errors but cross-platform use, - = errors, \* = other
versions with some tools due to plugins/errors, see details in READMEs and
build scripts

| __Test Libraries / Scala Version ->__ | 2.13.10 | 2.13.10 | 2.13.10 | 3.2.1 | 3.2.1 | 3.2.1 |
|:--|:--:|:--:|:--:|:--:|:--:|:--:|
| | sbt (1.8.0) | maven (3.8.6) | gradle (6.9.3) | sbt (1.8.0) | maven (3.8.6) | gradle (6.9.3) |
| junit4 (4.13.2\*, pure Java) | + | + | + | + | + | - |
| junit5 (5.9.1, pure Java) | + | + | + | + | + | - |
| munit\_2.13 (0.7.29) | + | + | + | (-) | (-) | - |
| munit\_3 (0.7.29) | + | + | + | + | + | - |
| munit-scalacheck\_2.13 (0.7.29) | + | + | + | (-) | (-) | - |
| munit-scalacheck\_3 (0.7.29) | (-) | (-) | (-) | + | + | - |
| scalacheck\_2.13 (1.17.0) | + | + | + | + | + | - |
| scalacheck\_3 (1.17.0) | + | + | + | + | + | - |
| scalatest\_2.13 (3.2.14\*) | + | + | + | (-) | (-) | - |
| scalatest\_3 (3.2.14\*) | (-) | (-) | (-) | + | + | - |
| scalatest\_2.13 (3.2.14\*) + scalatestplus-scalacheck-1-17\_2.13  (3.2.14.0) | + | + | + | (-) | (-) | - |
| scalatest\_3 (3.2.14\*) + scalatestplus-scalacheck-1-17\_3 (3.2.14.0) | (-) | (-) | (-) | + | + | - |
| scalatest\_2.13 (3.2.14\*) + scalamock\_2.13 (5.1.0) | + | + | + | (-) | (-) | - |
| scalatest\_3 (3.2.14\*) + scalamock\_2.13 (5.1.0) | (-) | (-) | (-) | (-) | (-) | - |

2022-11: PROBLEM: Still no solution how make Gradle 6.x run _scalac 3_.

----------------------------------------------------------------------

#### 2021-11: {build tools} X scala{2,3} X {test frameworks}

Symbols: + = ok, - = errors, (+/-) = issues, \* = other versions with some
tools due to plugins/errors, see details in READMEs and build scripts

| __Test Libraries / Scala Version ->__ | 2.13.7 | 2.13.7 | 2.13.7 | 3.1.0 | 3.1.0 | 3.1.0 |
|:--|:--:|:--:|:--:|:--:|:--:|:--:|
| | sbt (1.5.5) | maven (?) | gradle (6.6.1) | sbt (1.5.5) | maven (?) | gradle (6.6.1) |
| junit4 (4.13.2\*, pure Java) | + | + | + | + | (-) | - |
| junit5 (5.8.1\*, pure Java) | + | + | + | + | + | - |
| munit\_2.13 (0.7.29) | + | + | + | - | - | - |
| munit\_3 (0.7.29) | - | - | (+) | + | + | - |
| scalatest\_2.13 (3.2.10\*) | + | + | (+) | - | - | - |
| scalatest\_3 (3.2.10\*) | - | - | - | + | + | - |
| scalatest\_2.13 (3.2.10\*) + scalacheck\_2.13 (1.15.4) + scalatestplus-scalacheck\_2.13 (3.1.0.0-RC2) | + | + | (+) | - | - | - |
| scalatest\_3 (3.2.10\*) + scalacheck\_3 (1.15.4) + scalatestplus-scalacheck\_2.13 (3.1.0.0-RC2) | - | - | - | - | - | - |
| scalatest\_2.13 (3.2.10\*) + scalamock\_2.13 (5.1.0) | + | + | (+) | - | - | - |
| scalatest\_3 (3.2.10\*) + scalamock\_2.13 (5.1.0) | - | - | - | - | - | - |

2021-11: PROBLEM:

Gradle (6.6.1) always runs scalac 2.x (callchain: scalac <- zinc <- scala
plugin <- gradle).  Even if scala plugin is configured according to
[doc](https://docs.gradle.org/current/userguide/scala_plugin.html#sec:scala_dependency_management):
```
  implementation("org.scala-lang:scala3-library_3:3.1.0")
```

Proof: compile fails with scalac 3 option `-explain` but succeeds with scalac
2.x option `-explaintypes`, regardless of other settings (or `$PATH`):
```
tasks.withType<ScalaCompile> {
    scalaCompileOptions.additionalParameters = mutableListOf(
        // 2021-11: PROBLEM: gradle always runs scalac 2.x
        "-explain"  // FAILURE: bad option: '-explain'
        //"-explaintypes"  // OK
    )
}
```

2019..2021: Improvements

Build tool problems with old Dotty (especially maven, gradle) have improved
since renaming _dotc_ to _scalac_ and the official release of
[Scala 3](https://www.scala-lang.org/blog/2021/05/14/scala3-is-here.html).

Scala 2/3 library cross-compatibility has improved with the availability of
the _Scala 3 Unpickler_ and the _Scala 2.13 TASTy Reader_ (in 2.13.5), see
[classpath level migration guide](https://docs.scala-lang.org/scala3/guides/migration/compatibility-classpath.html).

----------------------------------------------------------------------

[Up](./README.md)
