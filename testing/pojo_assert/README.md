# polyglot/testing/pojo\_assertion\_tests

#### Topics: A small, cloneable, _standalone_ build project
- _Source and Unit Test Code_ for the coding excercise
  [../ex_2_2](../ex_2.q.test_frameworks.md#22-exercise-write-a-minimal-assertion-api-for-pojo-unit-testing)
  on test assertions
- _Notes_ on tool support for unit-testing without external libraries (POJO
  tests)
- _Project Code + Notes_ on the usage of the
  [maven-surefire-plugin][maven-surefire-pojo]
  for POJO tests

Quick links: \
[scala sources](src/main/scala/netropy/polyglot/testing/pojo_assert),
[java sources](src/main/java/netropy/polyglot/testing/pojo_assert),
[scala tests](src/test/scala/netropy/polyglot/testing/pojo_assert),
[java tests](src/test/java/netropy/polyglot/testing/pojo_assert),
[pom.xml](./pom.xml)

Also see: \
[../ex_2_2](../ex_2.q.test_frameworks.md#22-exercise-write-a-minimal-assertion-api-for-pojo-unit-testing),
[Summary: POJO Assertion Tests (Java, Scala)](../Notes_unit_test_frameworks.md#summary-pojo-assertion-tests-java-scala)

#### Summary: {sbt,maven,gradle} X {POJO testing}

POJO (Plain Old Java Object) tests do not rely on an external
[unit testing framework](https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks).
The main benefit is to avoid a 3rd-party, source + binary dependency.

Rather, a pojo unit test is a plain (un-annotated) method of a public class
that \
\- follows certain naming rules (e.g., name starts with lower-case "test"), \
\- has a prescribed signature (e.g., parameterless with _void/Unit_ return), \
\- indicates failure by throwing an exception (e.g., by use of _assert_).

To run pojo unit tests, the build tool needs to have a suitable _test runner_
that picks up the tests in the code for execution and reporting.

So far, only Maven has out-of-box support for pojo tests:
| 2022-11 | POJO support | test runner available/needed |
|:---|:---:|:---|
| [sbt](https://www.scala-sbt.org) | no | could write a plugin serving [sbt's test interface][sbt-test-interface] |
| [Maven](https://maven.apache.org) | __yes__ | the [maven-surefire-plugin][maven-surefire-plugin] offers [POJO support][maven-surefire-pojo] |
| [Gradle](https://gradle.org) | no | could write a runner mimicking [JUnit][junit] or [TestNG][testng] |

Note: The
[maven-failsafe-plugin](https://maven.apache.org/surefire/maven-failsafe-plugin/index.html)
is better suited for
[integration testing](https://en.wikipedia.org/wiki/Integration_testing).

[maven-surefire-plugin]: https://maven.apache.org/surefire/maven-surefire-plugin/index.html
[maven-surefire-pojo]: https://maven.apache.org/surefire/maven-surefire-plugin/examples/pojo-test.html
[sbt-test-interface]: https://github.com/sbt/test-interface
[junit]: https://junit.org
[testng]: https://testng.org

#### Notes: POJO unit testing - How to indicate test failure

For languages that offer exceptions: A failed assertion or any thrown
exception.

For C, Shell, Perl, and other programs: the
[Test Anything Protocol](https://testanything.org/).

TAP is an established, simple, text-based, language-agnostic output format,
for which various test harnesses (and language bindings) exist.

A pojo test simply prints output according to TAP (e.g., "ok", "not ok"),
which is parsed and reported by a TAP-compliant test harness.

#### Notes: POJO unit testing - Pros & Cons

The
[Summary: POJO Assertion Tests (Java, Scala)](../Notes_unit_test_frameworks.md#summary-assertion-pojo-tests-java-scala)
recaped the main pros & cons of pojo testing.  Here, an expanded list:

| rating | pro/con |
|:---:|:---|
| `[+++]` | one fewer ABI or platform dependency (Scala 3/2.x, native/cross-compile, Java 8/11/17 etc) |
| `[+++]` | one fewer API source code dependency (documentation, upgrading or switching the unit test framework etc) |
| `[++]` | one fewer API to learn (nomenclature, annotations, assertions, matchers, fixtures, parametrized tests etc) |
| `[+]` | one fewer, potential, 3rd party vulnerability (echoes of Apache Log4j; though, unit test code is rarely shipped) |
| `[+-]` | support by the [maven-surefire-plugin][maven-surefire-plugin], but only limited features available for POJO tests, see [feature matrix](https://maven.apache.org/surefire/maven-surefire-plugin/usage.html) |
| `[+-]` | limited support of test _fixtures_ for resource management (maven-surefire-plugin picks up basic _setUp(), tearDown()_) |
| `[--]` | lack of annotations/base classes: naming & signature conventions make it too easy for tests to be skipped _silently_ |
| `[--]` | lack of basic _assert_ functions (e.g., _assertThrows, assertResult, assertThat;_ though, can be written) |
| `[--]` | lack of basic data type _matchers_ (e.g., for collections, floating-points, date&time types; though, can be written) |
| `[---]` | lack of support for parametrized tests (e.g., with generated or external test data; though, can be written) |
| `[----]` | no support for POJO unit tests by [sbt](https://www.scala-sbt.org), [Gradle](https://gradle.org), and other build tools (though, test runner plugins could be written) |

#### Notes: maven-surefire-plugin - Methods [not] recognized as POJO unit tests

_Caution:_ The [maven-surefire-plugin][maven-surefire-pojo] only recognizes a
method as test if _all_ criteria are met:
1. method name starts with lower-case _test_
1. method is parameterless
1. method has return type _Unit_ / _void_
1. method is defined in a _class_, not a _trait_ or _interface_, or _object_
   or _top-level_ function
1. the class has a public, zero-arg constructor
1. the class is _public_ and in a _package_ that is exported by its containing
   _module_
1. the class name matches the test class pattern configured by _\<include\>_
   or _-Dtest_

Otherwise, maven-surefire will __silently skip__ running the method.

For example, _all_ of these (supposedly pojo test) methods are skipped:
```
  def test: Unit = throw new Exception()
  def test_0 = throw new Exception()
  def test_1: Int = throw new Exception()
  def tst_2: Unit = throw new Exception()
  def Test_3: Unit = throw new Exception()
  def test_4(u: Unit): Unit = throw new Exception()
```

#### Notes: maven-surefire-plugin - [Non-]support for parametrized tests

Methods need to be parameterless to be recognized as tests.  Therefore, one
needs to write a _testXYZ()_ method that then applies a parametrized function
to specific data.

This also means that any parametrized test is only reported once, as method
_testXYZ()_, and not per tested data point.

#### Notes: maven-surefire-plugin - Test selection by class name patterns

The [maven-surefire-plugin][maven-surefire-pojo] identifies the classes to run as
tests by an _include/exclude_ pattern match.

The class name is matched against:
1. the passed (or pre-configured) property `mvn -Dtest='...pattern...' test`
1. or the default `<include>` pattern `**/{Test*|*Test|*Tests|*TestCase}`
1. or any `<includes> <include>...</include> </includes>` surefire config
1. and any `<excludes> <exclude>...</exclude> </excludes>` surefire config

The surefire plugin does not support running individual pojo test methods,
only entire test classes (no effect of using _...#..._ in include patterns).

For the regular expression pattern syntax, see
[%regex\[expr\]](https://maven.apache.org/surefire/maven-surefire-plugin/examples/inclusion-exclusion.html).

#### Notes: maven-surefire-plugin - Test suites by pre-configured patterns

Passing a command-line property _-Dtest="pattern"_ can be tedious and
error-prone.  Maven _\<profiles\>_ can be used to offer pre-configured test
suites defined by their own _include/excludes_ patterns.

For example, profiles can be used as a substitute for unit-test annotations
like "slow" or "ignored":
```
  mvn test
  mvn test -Pfast-tests
  mvn test -Pslow-tests
  mvn test -Pskipped-tests
  mvn test -Pall-tests
```

For a profile code example, see
[./pom.xml](./pom.xml).

#### Notes: maven-surefire-plugin - Parallel execution of POJO test classes

The [maven-surefire-plugin][maven-surefire-pojo] does not support running
individual pojo test methods, only entire test classes.

Hence, surefire only runs entire test classes, not methods, in parallel.

Execution properties:
- `<parallel>` is `ignored`,
- `<forkCount>` configures the max #processes, e.g.: 1, 8, 2C (= 2 * #cpu
  cores),
- `<reuseForks>` (boolean) whether to reuse processes for the next tests,
- `<argLine>` adds JVM arguments,
- `<systemPropertyVariables>` adds system properties.

[Up](../README.md)
