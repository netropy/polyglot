package netropy.polyglot.testing.pojo_assert;

import java.util.Objects;

/** Excercise 2.2: A minimal set of assert functions for unit testing. */
public class AssertionsJ {

    /*
     * Discussion: What's the best method signature for assertResult(...)?
     *
     * Q: Should assertResult() be generic, if so over what type bounds?
     * A: Yes, but it does not matter much (except for readability).
     *
     * After type erasure, these signatures are the same:
     *   (1) void assertResult(Object expected, Object actual)
     *   (2) <V> void assertResult(V expected, V actual)
     *   (3) <E, A> void assertResult(E expected, A actual)
     *   (4) <E, A extends E> void assertResult(E expected, A actual)
     *
     * ...why any of these signatures compile: assertResult(1, "");
     *
     * My preference: (3) as it visually suggests unrelated, unbounded types.
     *
     * ------------------------------------------------------------
     *
     * Q: Should assertResult() be specialized / overloaded for unboxed types?
     * A: - Yes, as with java.util.function.*, overload for int, long, double.
     *    - No, this is about test code performance and the impact is minimal.
     *    - But the costs are minimal too, only 3 additional static functions.
     *    - OK either way, does not cost much and matter much.
     *
     * ------------------------------------------------------------
     *
     * Q: Can the function be curried as: assertResult(expected)(actual)?
     * A: Not really, in Java the usage becomes cluttered, unlike in Scala.
     *    See discussion of assertThrows(expected)(action) below.
     *
     * ------------------------------------------------------------
     *
     * Q: What argument types cannot be passed to assertResult()?
     * A: Lambdas won't match Object, which is not a functional type.
     *
     * We could overload assertResult() for java.util.function.* types.
     * But then, there's no point to compare lambdas without evaluating them.
     *
     * ------------------------------------------------------------
     *
     * Q: Should the arguments be passed unevaluated by-name, i.e., as thunks?
     * A: => No real benefit, assertResult is strict in both parameters.
     *    => Also: it's hard to evaluate arguments at most once [0 or 1].
     *    => Also: The usage becomes cluttered, must always pass a lambda.
     *    => These problems are hard in Java, non-existent or easy in Scala.
     *
     * // Code experiment:
     * // To emulate passing by-name, wrap arguments in a Supplier (thunk).
     * import java.util.function.Supplier;
     *
     * static public <E, A> void assertResult1(
     *     Supplier<E> expected, Supplier<A> actual) {
     *
     *     // Problem: We want the arguments evaluated at most once [0 or 1].
     *     //
     *     // We want arguments evaluated lazily, not force them outside assert,
     *     // since assert can be disabled:
     *     // assert Objects.equals(expected.get(), actual.get())
     *     // : ("expected: " + expected.get()  + ", actual: " + actual.get());
     *     //
     *     // But we also want them evaluated once where they are used twice:
     *     E e = expected.get();
     *     A a = actual.get();
     *     assert Objects.equals(e, a) : ("expected: " + e + ", actual: " + a);
     *     //
     *     // In Scala, we could use 'lazy val' for automatic memoization.
     *     // In Java, we'd have to wrap args in a Supplier that memoizes them.
     *     // => not worth it for the rare case that asserts are disabled
     * }
     *
     * // Usage: clutered, all arguments passed as lambdas (or method refs).
     * assertResult1(() -> 1, () -> 2);
     */

    /** Fails if the expected and actual values do not compare equal. */
    static public <E, A> void assertResult(E expected, A actual) {
        assert Objects.equals(expected, actual)
        : ("expected: " + expected + ", actual: " + actual);
    }

    /** Fails if the expected and actual values do not compare equal. */
    static public void assertResult(int expected, int actual) {
        assert expected == actual
        : ("expected: " + expected + ", actual: " + actual);
    }

    /** Fails if the expected and actual values do not compare equal. */
    static public void assertResult(long expected, long actual) {
        assert expected == actual
        : ("expected: " + expected + ", actual: " + actual);
    }

    /** Fails if the expected and actual values do not compare equal. */
    static public void assertResult(double expected, double actual) {
        assert expected == actual
        : ("expected: " + expected + ", actual: " + actual);
    }

    /*
     * Discussion: What's the best method signature for assertThrows(...)?
     *
     * Q: How to pass the expected Throwable runtime type into assertThrows?
     * A: <T extends Throwable> void assertThrows(Class<T> expected, ...);
     *
     * ------------------------------------------------------------
     *
     * Q: How to pass the throwing, executable action into assertThrows?
     * A: None of the standard Java types, Runnable or Callable<V>, work well.
     *    Best to define a special, functional interface: ThrowingRunnable.
     *
     *    The problem with: Runnable{ void run(); }
     *    - the action cannot throw checked Exceptions
     *
     *    import java.util.concurrent.Callable;
     *    The problems with: Callable<V>{ V call() throws Exception; }
     *    - the action must return an object or null
     *    - the action cannot throw an Error or Throwable, only Exceptions
     *
     *    => ... void assertThrows(..., ThrowingRunnable action);
     *
     * ------------------------------------------------------------
     *
     * Q: Can the function be curried as: assertThrows(expected)(action)?
     * A: Not really, in Java the usage becomes cluttered, unlike in Scala.
     *
     * // assertThrows as: Class<T> -> (Consumer<ThrowingRunnable> -> void)
     * import java.util.function.Consumer;
     * static public <T extends Throwable> Consumer<ThrowingRunnable>
     *     assertThrows(Class<T> expected) {
     *     return (ThrowingRunnable action) -> assertThrows(expected, action);
     * }
     *
     * // usage: must call accept() since Java knows no apply(), unlike Scala
     * assertThrows(RuntimeException.class).accept(() -> {});
     */

    /** A task that takes and returns nothing but may throw. */
    @FunctionalInterface
    public interface ThrowingRunnable {
        void run() throws Throwable;
    }

    /** Fails if the given expression does not throw the expected exception. */
    static public <T extends Throwable> void assertThrows(
        Class<T> expected,
        ThrowingRunnable action) {
        try {
            action.run();
        } catch (Throwable t) {
            if (expected.isInstance(t))
                return;  // t is assignment-compatible
            assert false : "expected throwable: " + expected + ", caught: " + t;
        }
        assert false : "expected throwable: " + expected;
    }
}
