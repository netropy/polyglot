package netropy.polyglot.testing.pojo_assert

import util.{Try, Success}

/** Excercise 2.2: A minimal set of assert functions for unit testing.
  *
  * Discussion: Where to provide functions assertResult, assertThrows
  *
  * Any solution is feasible.  My preference for (1):
  * (1) as function member of an Assertions object
  * (2) as Scala 2 package object functions
  * (3) as Scala 3 top-level functions
  */
object Assertions {

  /*
   * Discussion: Function signature variants for assertResult, assertThrows
   *
   * A) Curried Functions:
   * ---------------------
   *
   * My preference: offer both, single and multiple parameter lists overloads:
   * + support partial application with an expected value/exception
   * + support readability of short and long argument expressions:
   *   assertResult(exp, act)
   *   assertResult(
   *     ...exp...,
   *     ...act...)
   *   assertResult(exp) {
   *     ...act...
   *   }
   *
   * Problem: Avoid compile error: double definition after erasure
   * Name clash of curried/uncurried versions after compile:
   *   def assertResult[A, B](exp: A, act: B): Unit
   *   def assertResult[A, B](exp: A)(act: B): Unit
   * Scala2 fix: disambiguate signatures in a call-invariant way
   *             see B) By-Name Parameters
   * Scala3 fix: @annotation.targetName("assertResult1")
   *
   * B) By-Name Parameters:
   * ----------------------
   *
   * For assertThrows: non-strict in the 2nd parameter, must pass by name.
   *
   * For assertResult: strict in both parameters, easiest to pass by value.
   *
   * However, to avoid "double definition after erasure" compile error, we
   * disambiguate by having one overloaded method pass by-name:
   *   def assertResult[A, B](exp: A, act: B): Unit
   *   def assertResult[A, B](exp: => A)(act: => B): Unit
   * Note that the implementation must evaluate by-name arguments exactly once.
   *
   * C) Type Bounds:
   * ---------------
   *
   * Note that signatures 1..4 and 5..6 are the same after type erasure:
   *
   * (1) def assertResult(exp: Any)(act: Any): Unit
   * (2) def assertResult[A](exp: A)(act: A): Unit
   * (3) def assertResult[A, B](exp: A)(act: B): Unit
   * (4) def assertResult[A, B <: A](exp: A)(act: B): Unit
   *
   * (5) def assertThrows[T <: Throwable](ex: Class[T])(expr: => Any): Unit
   * (6) def assertThrows[T <: Throwable, A](ex: Class[T])(expr: => A): Unit
   *
   * Type bound (4) [A, B <: A] is ineffective due to upcasting, both compile:
   *   assertResult(1) { 1 }
   *   assertResult(1) { "1" }
   *
   * Type bound (5) [T <: Throwable] is necessary, since java.lang.Class<T>
   * is not covariant in _T_:
   *   def assertThrows(ex: Class[Throwable], A)(expr: => A): Unit =
   *       // runtime error passing classOf[Exception] as Class[Throwable]
   *       // Found:    (classOf[Exception] : Class[Exception])
   *       // Required: Class[Throwable]
   *
   * My signature preference: (3) and (6)
   * - parametrize over [A], [B], [T] to preserve compile type information
   *   (even if not further used in the method body)
   * - don't mislead with type bounds that are ineffective
   *
   * D) Multiversal Equality:
   * ------------------------
   *
   * In Scala (and Java), the default is Universal Equality: expressions
   * `a == b` and `a != b` compile for values `a`, `b` of any types.
   *
   * Scala3 supports multiversal equality via strictEquality, CanEqual etc.
   * @see [[https://docs.scala-lang.org/scala3/book/ca-multiversal-equality.html]]
   * @see [[https://docs.scala-lang.org/scala3/reference/contextual/multiversal-equality.html]]
   *
   * The Scala3 switch can enabled here (to cover assert(e == a)) by:
   *   import scala.language.strictEquality
   *
   * E) Code Experiment: Limited Multiversal Equality
   * ------------------------------------------------
   *
   * In absence of Scala3 multiversal equality or of another typeclass system,
   * what signature design of assertResult would at compile time restrict:
   *   assertResult(0, "0")                       // reject
   *   assertResult(Some(0), List(0).headOption)  // allow
   *
   * Note that a lower type bound on B
   *   def assertResult[A, B >: A](exp: A, act: B): Unit
   * has no bite as it gets erased to A = B = Any.
   *
   * But a type bound check by an implicit "evidence/witness" parameter
   *   def assertResult[A, B](exp: => A, act: B)(implicit ev: A <:< B): Unit
   * is effective as it is not satisfied by unrelated types:
   *   assertResult(0, "0")  // rejected
   *
   * The only unsupported case left: A >: B, unless we manually upcast
   *   assertResult(Seq(0), List(0))            // rejected
   *   assertResult(Seq(0), List(0): Seq[Int])  // allowed
   *
   * Note that adding an overload for A >: B
   *   def assertResult[A, B](exp: A, act: => B)(implicit ev: B <:< A): Unit
   * is no solution as it creates ambiguities reference.
   *
   * Here, we stop this code experiment with: def assertResultX[A, B] below.
   *
   * F) Type Specialization
   * ----------------------
   *
   * Q: Should assertResult() be specialized / overloaded for value types?
   * A: No, this is about test code performance and the impact is minimal.
   *    No, as we'd have to specialize over two type parameters:
   *      def assertResult[@specialized(Int, Long, Double) A,
   *                       @specialized(Int, Long, Double) B]
   *    No, as per javap: @specialized did _not_ generate any extra code.
   */

  /** Fails if the expected and actual values do not compare equal. */
  def assertResult[A, B](exp: A, act: B): Unit =
    assert(exp == act, s"expected value: $exp, actual value: $act")

  /** Fails if the expected and actual values do not compare equal. */
  def assertResult[A, B](exp: => A)(act: => B): Unit =
    assertResult(exp, act)

  /** Fails if the _related_ expected and actual values do not compare equal. */
  def assertResultX[A, B](exp: A, act: B)(implicit ev: A <:< B): Unit =
    assertResult(exp, act)

  /** Fails if the given expression does not throw the expected exception. */
  def assertThrows[T <: Throwable, A](ex: Class[T])(expr: => A): Unit =
    assertThrows_v1(ex)(expr)

  /*
   * Discussion: Code Variants for assertThrows
   *
   * assertThrows_v1: use procedural try-catch
   * assertThrows_v2: use monadic scala.util.Try
   *
   * Note: Variants v1 and v2 don't behave 100% the same.
   * Serious system errors (fatal exceptions) will be thrown by scala.util.Try
   * while try-catch will still attempt to wrap them in an AssertionError.
   *
   * My preference: use v1 = procedural try-catch
   * + easier to read, more familiar to most programmers
   * + using a Try still cannot avoid exception handling via if-then-else
   * - fatal exception behaviour
   */

  // use procedural try-catch
  private def assertThrows_v1[T <: Throwable, A]
      (ex: Class[T])(expr: => A): Unit = {
    val a = try {
      expr
    } catch {
      case e if ex.isInstance(e) =>
        return ()  // e is assignment-compatible
      case e: Throwable =>
        assert(false, s"expected throwable: $ex, caught: $e")
    }
    assert(false, s"expected throwable: $ex, obtained value: $a")
  }

  // use monadic scala.util.Try
  private def assertThrows_v2[T <: Throwable, A]
      (ex: Class[T])(expr: => A): Unit =
    Try(expr).transform(
      s =>
        Try(assert(false, s"expected throwable: $ex, obtained value: $s")),
      f => {
        if (ex.isInstance(f)) Success(f)  // f is assignment-compatible
        else Try(assert(false, s"expected throwable: $ex, caught: $f"))
      }
    ).get  // force a value
}
