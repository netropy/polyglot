package netropy.polyglot.testing

// Scala 2
object Main {

  def main(args: Array[String]): Unit = println(args.mkString(", "))
}

// Scala 2
object MainApp extends App {

  // Scala2 runs this code as (deprecated) Delayedinit
  //   https://www.scala-lang.org/api/2.12.7/scala/DelayedInit.html
  //
  // Scala3 runs this code in the object's initializer
  //   https://dotty.epfl.ch/docs/reference/dropped/delayed-init.html
  // may observe fields as null when accessed asynchronously
  //
  println("Hello!")
}
