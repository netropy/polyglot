package netropy.polyglot.testing.pojo_assert;

import java.util.Collection;
import java.util.Arrays;
import static netropy.polyglot.testing.pojo_assert.AssertionsJ.*;

/** Excercise 2.2: Unit-test the assert functions. */
public class AssertionsJTests {

    public void test_assertThrows0() {
        assertThrows(
            Exception.class,
            () -> { throw new Exception(); });
    }

    public void test_assertThrows1() {
        assertThrows(
            AssertionError.class,
            () -> { throw new AssertionError(); });
    }

    public void test_assertThrows2() throws Exception {
        try {
            assertThrows(Exception.class, () -> {});
            throw new Exception();
        } catch (AssertionError e) {
        }
    }

    public void test_assertThrows3() throws Exception {
        try {
            assertThrows(AssertionError.class, () -> {});
            throw new Exception();
        } catch (AssertionError e) {
        }
    }

    public void test_assertThrows4() {
        assertThrows(
            AssertionError.class,
            () -> {
                assertThrows(Exception.class, () -> {});
                throw new Exception();
            });
    }

    public void test_assertThrows5() {
        assertThrows(
            AssertionError.class,
            () -> {
                assertThrows(AssertionError.class, () -> {});
                throw new Exception();
            });
    }

    public void test_assertThrows6() {
        assertThrows(
            Exception.class,
            () -> { throw new RuntimeException(); });
    }

    public void test_assertThrows7() {
        assertThrows(
            AssertionError.class,
            () -> {
                assertThrows(
                    RuntimeException.class,
                    () -> { throw new Exception(); });
            });
    }

    public void test_assertResult0() {
        assertResult(0, 0);
    }

    public void test_assertResult1() {
        assertResult((Collection<Integer>)Arrays.asList​(0), Arrays.asList​(0));
    }

    public void test_assertResult2() {
        assertThrows(
            AssertionError.class,
            () -> { assertResult(0, 1); });
    }

    public void test_assertResult3() {
        assertThrows(
            AssertionError.class,
            () -> { assertResult(0, 1L); });
    }

    public void test_assertResult4() {
        assertThrows(
            AssertionError.class,
            () -> { assertResult(0, "0"); });
    }

    public void test_assertResult5() {
        assertThrows(
            AssertionError.class,
            () -> { assertResult(null, 0); });
    }
}
