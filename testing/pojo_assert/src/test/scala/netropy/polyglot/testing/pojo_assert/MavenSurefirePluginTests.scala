package netropy.polyglot.testing.pojo_assert

/** Unit tests for maven-surefire-plugin's support of POJO tests.
  *
  * Surefire only recognizes a method as test if _all_ criteria are met:
  * 1. method name starts with lower-case "test"
  * 2. method is parameterless
  * 3. method has return type Unit
  * 4. method is defined in a class, not a trait or object
  * 5. class has a public zero-arg constructor
  */
class MavenSurefirePluginTests {

  // sufire recognizes these test methods:

  def testOk: Unit = ()
  def test_ok: Unit = ()

  // CAUTION: surefire skips all these tests _silently_:

  // recognized by surefire >= 3.0.0
  // def test: Unit = throw new Exception()  // no name after "test" prefix

  def tst_1: Unit = throw new Exception()  // no "test" prefix

  def Test_2: Unit = throw new Exception()  // no lower-case "test" prefix

  def test_3 = throw new Exception()  // no Unit return (Nothing)

  def test_4: Int = throw new Exception()  // no Unit return

  def test_5(u: Unit): Unit = throw new Exception()  // no zero-arg method
}

// CAUTION: surefire skips all these tests _silently_:

object MavenSurefirePlugin1_Tests {  // no class

  def test_6: Unit = throw new Exception()
}

trait MavenSurefirePlugin2_Tests {  // no class

  def test_7: Unit = throw new Exception()
}

class MavenSurefirePlugin3_Tests(x: Int) {  // no public zero-arg constructor

  def test_8: Unit = throw new Exception()

  private def this() = this(0)  // no public constructor
}

class MavenSurefirePlugin_Tests4 {  // no suffix matching default "Tests|..."

  def test_9: Unit = throw new Exception()
}
