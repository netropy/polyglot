package netropy.polyglot.testing.pojo_assert

import Assertions._

/** Excercise 2.2: Unit-test the assert functions. */
class AssertionsTests {

  def test_assertThrows0: Unit =
    assertThrows(classOf[Exception])(throw new Exception())

  def test_assertThrows1: Unit =
    assertThrows(classOf[AssertionError])(throw new AssertionError())

  def test_assertThrows2: Unit =
    try {
      assertThrows(classOf[Exception])(())
      throw new Exception()
    } catch { case e: AssertionError => () }

  def test_assertThrows3: Unit =
    try {
      assertThrows(classOf[AssertionError])(())
      throw new Exception()
    } catch { case e: AssertionError => () }

  def test_assertThrows4: Unit =
    assertThrows(classOf[AssertionError]) {
      assertThrows(classOf[Exception])(())
      throw new Exception()
    }

  def test_assertThrows5: Unit =
    assertThrows(classOf[AssertionError]) {
      assertThrows(classOf[AssertionError])(())
      throw new Exception()
    }

  def test_assertThrows6: Unit =
    assertThrows(classOf[Exception]) {
      throw new RuntimeException()
    }

  def test_assertThrows7: Unit =
    assertThrows(classOf[AssertionError]) {
      assertThrows(classOf[RuntimeException])(throw new Exception())
    }

  def test_assertResult0: Unit = {
    assertResult(0, 0)
    assertResult(0, 0L)
    assertResult(List(0), Seq(0))
    assertResult(Seq(0), List(0))
  }

  def test_assertResult1: Unit = {
    assertThrows(classOf[AssertionError]) { assertResult(0, 1) }
    assertThrows(classOf[AssertionError]) { assertResult(0, 1L) }
    assertThrows(classOf[AssertionError]) { assertResult(0, "0") }
    assertThrows(classOf[AssertionError]) { assertResult(null, 0) }
  }

  def test_assertResult10: Unit = {
    assertResult(0) { 0 }
    assertResult(0) { 0L }
    assertResult(List(0)) { Seq(0) }
    assertResult(Seq(0)) { List(0) }
  }

  def test_assertResult11: Unit = {
    assertThrows(classOf[AssertionError]) { assertResult(0) { 1 } }
    assertThrows(classOf[AssertionError]) { assertResult(0) { 1L } }
    assertThrows(classOf[AssertionError]) { assertResult(0) { "0" } }
    assertThrows(classOf[AssertionError]) { assertResult(null) { 0 } }
  }

  def test_assertResult20: Unit = {
    assertResultX(0, 0)                       // OK, same type

    //assertResultX(0, "0")                   // rejected, unrelated types

    //assertResultX(0, 0L)                    // rejected, unrelated types
    assertResultX(0: AnyVal, 0L: AnyVal)      // OK after upcast

    assertResultX(List(0), Seq(0))            // OK, expected <: actual

    //assertResultX(Seq(0), List(0))          // rejected, expected >: actual
    assertResultX(Seq(0), List(0): Seq[Int])  // OK after upcast
  }
}
