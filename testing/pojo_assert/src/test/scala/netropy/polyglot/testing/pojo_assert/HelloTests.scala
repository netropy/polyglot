package netropy.polyglot.testing.pojo_assert

import netropy.polyglot.testing.Hello

import Assertions._

/** Unit-tests the Hello.greet function. */
class HelloTests {

  def testProposition =
    assert(Hello.greet("You").startsWith("Hello"), "not greeted")

  def testExpectValue =
    assertResult("Hello You")(Hello.greet("You"))

  // parametrized test
  def testExpectOn(input: String) =
    assertResult(s"Hello $input")(Hello.greet(input))

  def testExpectValues = {  // must wrap in a no-arg test method
    testExpectOn("You")
    testExpectOn("Me")
  }
}
