# polyglot

### Languages, Code Experiments, Exercises, Refcards, Notes

Top-Level Projects:
- [adsp](adsp/README.md) - algorithms, data structures, patterns
- [bigdata](bigdata/README.md) - cluster, cloud, streaming, batch
- [c](c/README.md) - C lang
- [fp](fp/README.md) - functional programming topics
- [java](java/README.md) - Java lang
- [math](math/README.md) - math & statistics
- [misc](misc/README.md) - general programming topics, style guides
- [python](python/README.md) - Python lang
- [rust](rust/README.md) - Rust lang
- [scala](scala/README.md) - Scala lang
- [testing](testing/README.md) - test frameworks, project templates
- [tools](tools/README.md) - build tools

How to build projects from here: \
[Root Project Code Example: Multi-Project Build with sbt, Gradle, Maven](Multi-Project_Build_with_sbt_Gradle_Maven.md)
